FROM debian:buster

# install tools
RUN apt-get update
RUN apt-get install aria2 xz-utils -y

# jenkins deploy utils
RUN apt-get -y install rpm
RUN apt-get install openssh-client -y

# developer utils
RUN apt-get install ruby vim -y
#RUN gem install irb pry pry-nav

# user information TODO: change this to container ENV
ARG username=builder
ARG user_id=1000

# set up versions
ARG node=8.11.3
ARG maven=3.6.3

# download java, maven and node
RUN aria2c -x 16 \
    https://nodejs.org/dist/v$node/node-v$node-linux-x64.tar.xz \
    -o /opt/node.tar.xz
RUN aria2c -x 16 \
    http://ftp.unicamp.br/pub/apache/maven/maven-3/$maven/binaries/apache-maven-$maven-bin.tar.gz \
    -o /opt/maven.tar.gz
RUN aria2c -x 16 \
    https://repo.huaweicloud.com/java/jdk/8u201-b09/jdk-8u201-linux-x64.tar.gz \
    http://liveupdate.infomedica.it/java/jdk-8u201-linux-x64.tar.gz \
    http://gate.bitel.ru/pub/soft/java/j2se/1.8.x/jdk-8u201-linux-x64.tar.gz \
    https://packages.baidu.com/app/jdk-8/jdk-8u201-linux-x64.tar.gz \
    -o /opt/java.tar.gz

# extract files
RUN tar -xvf /opt/maven.tar.gz -C /opt --one-top-level --strip-components=1
RUN tar -xJf /opt/node.tar.xz -C /opt
RUN tar -xvf /opt/java.tar.gz -C /opt

# remove downloaded files
RUN rm /opt/*.tar.*

# fix folder names
RUN mv /opt/$(ls /opt | grep node) /opt/node
RUN mv /opt/$(ls /opt | grep jdk) /opt/java

# create maven repository and code shortcut
ENV M2_REPO /m
RUN mkdir $M2_REPO
RUN mkdir /c

# create user and fix permissions
RUN useradd -m --uid $user_id --shell /bin/sh $username
RUN chown -R $username:$username /c
RUN chown -R $username:$username /m

RUN echo "America/Sao_Paulo" > /etc/timezone

USER $username
RUN ln -s $M2_REPO ~/.m2

# add bins in path
ENV PATH $PATH:/opt/maven/bin
ENV PATH $PATH:/opt/node/bin
ENV PATH $PATH:/opt/java/bin