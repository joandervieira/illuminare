FROM debian:buster

# install tools
RUN apt-get update
RUN apt-get install aria2 xz-utils -y

# jenkins deploy utils
RUN apt-get -y install rpm
RUN apt-get install openssh-client -y

# user information TODO: change this to container ENV
ARG username=tomcat
ARG user_id=1000

# set up versions
ARG java=7u80
ARG maven=3.6.3

ARG TOMCAT_URL=http://mirror.nbtelecom.com.br/apache/tomcat/tomcat-7/v7.0.107/bin/apache-tomcat-7.0.107.tar.gz

# download java, maven and node
RUN aria2c -x 16 $TOMCAT_URL \
    -o /opt/tomcat.tar.gz
RUN aria2c -x 16 \
    https://mirrors.yangxingzhen.com/jdk/jdk-$java-linux-x64.tar.gz \
    http://cdn-files.evildayz.com/mirror/java/jdk_$java/jdk-$java-linux-x64.tar.gz \
    http://mirror.cnop.net/jdk/linux/jdk-$java-linux-x64.tar.gz \
    https://ftp.weheartwebsites.de/linux/java/jdk/jdk-$java-linux-x64.tar.gz \
    http://repo-install.vtech.fr/java/jdk-$java-linux-x64.tar.gz \
    -o /opt/java.tar.gz
RUN aria2c -x 16 \
    http://ftp.unicamp.br/pub/apache/maven/maven-3/$maven/binaries/apache-maven-$maven-bin.tar.gz \
    -o /opt/maven.tar.gz

# extract files
RUN tar -xvf /opt/tomcat.tar.gz -C /opt
RUN tar -xvf /opt/java.tar.gz -C /opt
RUN tar -xvf /opt/maven.tar.gz -C /opt

# remove downloaded files
RUN rm /opt/*.tar.*

# fix folder names
RUN mv /opt/$(ls /opt | grep tomcat) /tomcat
RUN mv /opt/$(ls /opt | grep jdk) /opt/java
RUN mv /opt/$(ls /opt | grep maven) /opt/maven

# create user and fix permissions
RUN useradd -m --uid $user_id --shell /bin/sh $username
RUN chown -R $username:$username /tomcat

# add bins in path
ENV PATH $PATH:/opt/java/bin
ENV PATH $PATH:/tomcat/bin/
ENV PATH $PATH:/opt/maven/bin

ENV CATALINA_HOME /tomcat
RUN chmod +x $CATALINA_HOME/bin/catalina.sh

ENV DEBUGPORT=8000
ENV SERVICEMIXSPECSDEBUG=false
ENV JMXREMOTEPORT=3002
ENV CATALINA_HOME /tomcat
ENV THREADSIZE=15
ENV CONTEXT_PATH illuminare

ENV CATALINA_OPTS "-Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=\$JMXREMOTEPORT -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"

RUN echo "America/Sao_Paulo" > /etc/timezone

USER $username

ENV M2_REPO /m

RUN ln -s $M2_REPO ~/.m2

COPY server.sh /entrypoint.sh
#RUN chmod 755 /entrypoint.sh

WORKDIR $CATALINA_HOME
ENTRYPOINT ["/entrypoint.sh"]
CMD ["catalina.sh","run"]