#!/bin/sh

export JAVA_OPTS="
	-Dorg.apache.servicemix.specs.debug=$SERVICEMIXSPECSDEBUG
	-XX:MaxPermSize=512m -Xms256m -Xmx4096m
	-Xdebug -Xrunjdwp:transport=dt_socket,address=$DEBUGPORT,server=y,suspend=n
	-Dapp.devMode=true
	-Dfile.encoding=UTF-8
	-Dinner.jetty.maxThreadPool=$THREADSIZE -Dinner.jetty.minThreadPool=$THREADSIZE"

rm -rf /tomcat/webapps/* || true

mvn clean install -T 1C -f /c/pom.xml -nsu -Pdev

ln -s /c/target/illuminare.war /tomcat/webapps/illuminare.war

exec "$@"