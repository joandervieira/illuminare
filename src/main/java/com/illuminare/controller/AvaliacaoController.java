package com.illuminare.controller;


import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.AvaliacaoService;
import com.illuminare.view.AppError;
import com.illuminare.view.Paginator;


@Controller
@Path( "/avaliacao" )
public class AvaliacaoController extends GenericController {

    Logger log = Logger.getLogger( AvaliacaoController.class );

    @Autowired
    AvaliacaoService service;


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response list( @QueryParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            return Response.ok( service.listAll( idEmpresa, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error list():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById( @PathParam( "id" ) Long id, @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch ) {

        try {
            return Response.ok( service.findById( id, fieldsToFetch ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{idPesquisa}/{idAvaliador}/{idAvaliado}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findAvaliacao(
        @PathParam( "idPesquisa" ) Long idPesquisa,
        @PathParam( "idAvaliador" ) Long idAvaliador,
        @PathParam( "idAvaliado" ) Long idAvaliado ) {

        try {
            return Response.ok( service.findAvaliacao( idPesquisa, idAvaliador, idAvaliado, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( QuestionarioAvaliacao entity ) {

        try {
            return Response.ok( service.save( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/associar" )
    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response associar( QuestionarioAvaliacao entity ) {

        try {
            return Response.ok( service.associar( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error associar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error associar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( QuestionarioAvaliacao entity )
        throws MessageException {

        try {
            service.update( entity, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            service.deleteLogic( id, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/pesquisar" )
    public Response pesquisar( Paginator< QuestionarioAvaliacao > paginator ) {

        try {
            return Response.ok( service.pesquisar( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error pesquisar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    @Path( "/avaliacoesEmAberto/{idFuncionarioAvaliador}" )
    public Response findAvaliacoesEmAberto( @PathParam( "idFuncionarioAvaliador" ) Long idFuncionarioAvaliador ) {

        try {
            return Response.ok( service.findAvaliacoesEmAberto( idFuncionarioAvaliador ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findAvaliacoesEmAberto():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    @Path( "/avaliacoesByPesquisasEmAberto" )
    public Response findAvaliacoesByPesquisasEmAberto( @QueryParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            return Response.ok( service.findAvaliacoesByPesquisasEmAberto( idEmpresa, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findAvaliacoesByPesquisasEmAberto():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/avaliacoesByPesquisasEmAbertoPageable" )
    public Response avaliacoesByPesquisasEmAbertoPageable( Paginator< QuestionarioAvaliacao > paginator ) {

        try {
            return Response.ok( service.avaliacoesByPesquisasEmAbertoPageable( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findAvaliacoesByPesquisasEmAberto():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }

}
