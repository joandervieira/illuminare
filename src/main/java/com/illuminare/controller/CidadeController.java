package com.illuminare.controller;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.illuminare.entity.Cidade;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.CidadeService;
import com.illuminare.util.Constantes;
import com.illuminare.view.AppError;


@Controller
@Path( "/cidade" )
public class CidadeController extends GenericController {

    @Autowired
    CidadeService service;


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findAll( @QueryParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            return Response.ok( service.listAll( idEmpresa, Constantes.USER_ROOT ) ).build();
        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById( @PathParam( "id" ) Long id ) {

        try {
            return Response.ok( service.findById( id ) ).build();
        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/findByEstado/{idEstado}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findByEstado( @PathParam( "idEstado" ) Long idEstado ) {

        try {
            return Response.ok( service.findByEstado( idEstado ) ).build();
        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( Cidade entity ) {

        try {
            return Response.ok( service.save( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( Cidade entity )
        throws MessageException {

        try {
            service.update( entity, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            service.deleteLogic( id, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }

}
