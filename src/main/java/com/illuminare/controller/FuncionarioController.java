package com.illuminare.controller;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.illuminare.entity.Funcionario;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.FuncionarioService;
import com.illuminare.view.AppError;
import com.illuminare.view.Paginator;
import com.illuminare.view.PreviewFuncionariosImportacao;
import com.sun.jersey.multipart.FormDataParam;


@Controller
@Path( "/funcionario" )
public class FuncionarioController extends GenericController {

    Logger log = Logger.getLogger( FuncionarioController.class );

    @Autowired
    FuncionarioService service;


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response list(
        @QueryParam( "idEmpresa" ) Long idEmpresa,
        @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch,
        @QueryParam( "orderBy" ) String orderBy ) {

        try {
            if ( StringUtils.isBlank( orderBy ) ) {
                orderBy = "+pessoa.nome";
            }
            return Response.ok( service.listAllOrderBy( idEmpresa, fieldsToFetch, orderBy, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error list():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById( @PathParam( "id" ) Long id, @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch ) {

        try {

            return Response.ok( service.findById( id, new LinkedHashSet< String >( fieldsToFetch ) ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( Funcionario entity ) {

        try {
            return Response.ok( service.save( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( Funcionario entity )
        throws MessageException {

        try {
            service.update( entity, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            service.deleteLogic( id, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/pesquisar" )
    public Response pesquisar( Paginator< Funcionario > paginator ) {

        try {
            return Response.ok( service.pesquisar( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error pesquisar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    /**
     * Upload a File
     */

    @Path( "/importFromXls" )
    @POST
    @Consumes( MediaType.MULTIPART_FORM_DATA )
    // public Response importFromXls(@FormDataParam("file") InputStream
    // fileInputStream, @FormDataParam("file") FormDataContentDisposition
    // contentDispositionHeader) {
    public Response importFromXls( @FormDataParam( "file" ) InputStream fileInputStream ) {

        try {
            log.info( "importFromXls()" );
            // log.debug("contentDispositionHeader:" +
            // contentDispositionHeader);
            log.debug( "fileInputStream:" + fileInputStream );

            return Response.ok( service.importFromXls( fileInputStream ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error importFromXls():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error importFromXls():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }

    }


    @Path( "/confirmarImportacao" )
    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response confirmarImportacao( PreviewFuncionariosImportacao previewFuncionariosImportacao ) {

        try {
            service.confirmarImportacao( previewFuncionariosImportacao, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error confirmarImportacao():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error confirmarImportacao():" + e.getMessage(), e );
            log.error( e );
            e.printStackTrace();
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/preConfirmarImportacao" )
    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response preConfirmarImportacao( PreviewFuncionariosImportacao previewFuncionariosImportacao ) {

        try {

            return Response.ok( service.preConfirmarImportacao( previewFuncionariosImportacao, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error confirmarImportacao():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();

        } catch ( Exception e ) {
            log.error( "error confirmarImportacao():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    // save uploaded file to a defined location on the server
    private void saveFile( InputStream uploadedInputStream, String serverLocation ) {

        try {
            OutputStream outpuStream = new FileOutputStream( new File( serverLocation ) );
            int read = 0;
            byte[] bytes = new byte[ 1024 ];

            outpuStream = new FileOutputStream( new File( serverLocation ) );
            while ( ( read = uploadedInputStream.read( bytes ) ) != -1 ) {
                outpuStream.write( bytes, 0, read );
            }
            outpuStream.flush();
            outpuStream.close();
        } catch ( IOException e ) {

            e.printStackTrace();
        }

    }

}
