package com.illuminare.controller;


import javax.ws.rs.WebApplicationException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.illuminare.entity.User;


public class GenericController {

    protected String getUserNameLoggedUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();

        if ( principal instanceof String && ( (String) principal ).equals( "anonymousUser" ) ) {
            throw new WebApplicationException( 401 );
        }

        if ( principal instanceof User ) {
            User user = (User) principal;
            return user.getName();
        } else {
            return null;
        }
    }


    protected User getLoggedUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();

        if ( principal instanceof String && ( (String) principal ).equals( "anonymousUser" ) ) {
            throw new WebApplicationException( 401 );
        }

        if ( principal instanceof User ) {
            return (User) principal;
        } else {
            return null;
        }
    }
}
