package com.illuminare.controller;


import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.view.AppError;
import com.illuminare.view.Paginator;


@Controller
@Path( "/grupo-ocupacional" )
public class GrupoOcupacionalController extends GenericController {

    @Autowired
    GrupoOcupacionalService service;


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findAll(
        @QueryParam( "idEmpresa" ) Long idEmpresa,
        @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch,
        @QueryParam( "orderBy" ) String orderBy ) {

        try {
            if ( StringUtils.isBlank( orderBy ) ) {
                orderBy = "+nome";
            }
            return Response.ok( service.listAllOrderBy( idEmpresa, fieldsToFetch, orderBy, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById( @PathParam( "id" ) Long id ) {

        try {
            return Response.ok( service.findById( id ) ).build();
        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( GrupoOcupacional entity ) {

        try {
            return Response.ok( service.save( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( GrupoOcupacional entity )
        throws MessageException {

        try {
            service.update( entity, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            service.deleteLogic( id, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/pesquisar" )
    public Response pesquisar( Paginator< GrupoOcupacional > paginator ) {

        try {
            return Response.ok( service.pesquisar( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }

}
