package com.illuminare.controller;


import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.Questionario;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoMetaService;
import com.illuminare.view.AppError;
import com.illuminare.view.Paginator;
import com.illuminare.view.PesquisaView;


@Controller
@Path( "/pesquisa" )
public class PesquisaController extends GenericController {

    Logger log = Logger.getLogger( PesquisaController.class );

    @Autowired
    PesquisaService service;

    @Autowired
    QuestaoMetaService questaoMetaService;


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findAll( @QueryParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            String orderBy = "+nome";
            return Response.ok( service.listAllOrderBy( idEmpresa, orderBy, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error list():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById(
        @PathParam( "id" ) Long id,
        @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch,
        @QueryParam( "findMetas" ) Boolean findMetas ) {

        try {
            Pesquisa pesquisa = service.findById( id, fieldsToFetch, findMetas );
            return Response.ok( pesquisa ).build();

        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/full" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findByIdFull( @QueryParam( "idPesquisa" ) Long id ) {

        try {

            Pesquisa pesquisa = service.findById( id, Arrays.asList( "questionarios" ) );

            if ( pesquisa != null && pesquisa.getQuestionarios() != null ) {
                Set< Questionario > newQuestionarios = new LinkedHashSet<>();
                for ( Questionario questionario : pesquisa.getQuestionarios() ) {
                    Questionario newQuestionario = questaoMetaService.fillMetasQuestionario( questionario, pesquisa );
                    newQuestionarios.add( newQuestionario );
                }

                pesquisa.setQuestionarios( newQuestionarios );
            }

            return Response.ok( pesquisa ).build();

        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( Pesquisa entity ) {

        try {
            return Response.ok( service.save( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( Pesquisa entity )
        throws MessageException {

        try {
            service.update( entity, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            service.deleteLogic( id, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/pesquisar" )
    public Response pesquisar( Paginator< Pesquisa > paginator ) {

        try {
            return Response.ok( service.pesquisar( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error pesquisar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    @Path( "/fechadas/{idAvaliado}" )
    public Response fechadas( @PathParam( "idAvaliado" ) Long idAvaliado ) {

        try {
            return Response.ok( service.findPesquisasFechadasByAvaliado( idAvaliado ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error fechadas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error fechadas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    @Path( "/abertas" )
    public Response abertas( @QueryParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            return Response.ok( service.findPesquisasEmAberto( idEmpresa, null ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    @Path( "/{idPesquisa}/avaliados" )
    public Response avaliados( @PathParam( "idPesquisa" ) Long idPesquisa ) {

        try {
            return Response.ok( service.findAvaliados( idPesquisa ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    @Path( "/temAvaliacaoRespondida" )
    public Response temAvaliacaoRespondida( @QueryParam( "idPesquisa" ) Long idPesquisa ) {

        try {

            boolean avaliacaoRespondida = service.existeAvaliacaoRespondida( idPesquisa );
            Map< String, Object > result = new HashMap<>();
            result.put( "temAvaliacaoRespondida", avaliacaoRespondida );

            return Response.ok( result ).build();

        } catch ( ServiceException e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @DELETE
    @Path( "/removerAvaliacoesEResponstas" )
    public Response removerAvaliacoesEResponstas( @QueryParam( "idPesquisa" ) Long idPesquisa ) {

        try {

            service.removerAvaliacoesEResponstas( idPesquisa, getUserNameLoggedUser() );

            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @DELETE
    @Path( "/removerRespostas" )
    public Response removerRespostas( @QueryParam( "idPesquisa" ) Long idPesquisa ) {

        try {

            service.removerRespostas( idPesquisa, getUserNameLoggedUser() );

            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/buscarMetas" )
    public Response buscarMetas( PesquisaView pesquisa ) {

        try {

            questaoMetaService.fillMetaNasQuestoesTransient( pesquisa );

            return Response.ok( pesquisa ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/clonarMetas" )
    public Response clonarMetas( PesquisaView pesquisa ) {

        try {

            questaoMetaService.clonarMetasTransient( pesquisa );

            return Response.ok( pesquisa ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/clonar" )
    public Response clonar( Map< String, Long > payload ) {

        try {

            service.clonarPesquisa( payload.get( "idPesquisa" ), getUserNameLoggedUser() );

            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error abertas():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    private ObjectMapper getObjectMapper() {

        return new ObjectMapper().disable( SerializationFeature.FAIL_ON_EMPTY_BEANS );
    }


    private Questao copyQuestao( Questao questao ) {

        Questao newQuestao = new Questao();

        newQuestao.setId( questao.getId() );
        newQuestao.setNomeQuestao( questao.getNomeQuestao() );
        newQuestao.setPerguntaQuestao( questao.getPerguntaQuestao() );
        newQuestao.setIsTexto( questao.getIsTexto() );
        newQuestao.setIsLista( questao.getIsLista() );
        newQuestao.setIsNumerico( questao.getIsNumerico() );
        // newQuestao.setListaValores( questao.getListaValores() );
        newQuestao.setNotaPropria( questao.getNotaPropria() );
        newQuestao.setNotaLideres( questao.getNotaLideres() );
        newQuestao.setNotaSubordinados( questao.getNotaSubordinados() );
        newQuestao.setNotaOutros( questao.getNotaOutros() );
        newQuestao.setResposta( questao.getResposta() );
        newQuestao.setRespostaTexto( questao.getRespostaTexto() );
        newQuestao.setRespostaNumerico( questao.getRespostaNumerico() );
        newQuestao.setRespostaValorLista( questao.getRespostaValorLista() );
        newQuestao.setRespostaId( questao.getRespostaId() );
        newQuestao.setQuestoesMetasGruposOcupacionais( questao.getQuestoesMetasGruposOcupacionais() );
        newQuestao.setQuestoesMetasCargos( questao.getQuestoesMetasCargos() );
        newQuestao.setQuestoesMetasSetores( questao.getQuestoesMetasSetores() );
        newQuestao.setClassificacao( questao.getClassificacao() );
        newQuestao.setQuestaoClassificacao( questao.getQuestaoClassificacao() );
        newQuestao.setMeta( questao.getMeta() );
        newQuestao.setNegativo( questao.getNegativo() );
        newQuestao.setValorObtido( questao.getValorObtido() );
        newQuestao.setPercentualMeta( questao.getPercentualMeta() );
        newQuestao.setMedia( questao.getMedia() );

        return newQuestao;
    }

}
