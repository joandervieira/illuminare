package com.illuminare.controller;


import java.util.List;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Resposta;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.RespostaService;
import com.illuminare.view.AppError;
import com.illuminare.view.Paginator;


@Controller
@Path( "/resposta" )
public class RespostaController extends GenericController {

    Logger log = Logger.getLogger( RespostaController.class );

    @Autowired
    RespostaService service;


    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findAll( @QueryParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            return Response.ok( service.listAll( idEmpresa, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error list():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById( @PathParam( "id" ) Long id, @QueryParam( "fieldsToFetch" ) Set< String > fieldsToFetch ) {

        try {
            return Response.ok( service.findById( id, fieldsToFetch ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( Resposta entity ) {

        try {
            return Response.ok( service.saveUpdate( entity, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/all" )
    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvarAll( List< Resposta > entities ) {

        try {
            return Response.ok( service.saveUpdateAll( entities, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( Resposta entity )
        throws MessageException {

        try {
            service.update( entity, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/all" )
    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizarAll( List< Resposta > entities )
        throws MessageException {

        try {
            service.updateAll( entities, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            service.deleteLogic( id, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/deleteByAvaliacao/{idAvaliacao}" )
    @DELETE
    public Response deleteByAvaliacao( @PathParam( "idAvaliacao" ) Long idAvaliacao )
        throws MessageException {

        try {
            service.deleteByAvaliacao( idAvaliacao, getUserNameLoggedUser() );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/pesquisar" )
    public Response pesquisar( Paginator< Resposta > paginator ) {

        try {
            return Response.ok( service.pesquisar( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error pesquisar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/findByAvaliacao/{idAvaliacao}" )
    @GET
    public Response findByAvaliacao( @PathParam( "idAvaliacao" ) Long idAvaliacao )
        throws MessageException {

        try {

            return Response.ok( service.findByAvaliacao( idAvaliacao ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/findByAvaliacaoAgrupado/{idAvaliacao}" )
    @GET
    public Response findByAvaliacaoAgrupado( @PathParam( "idAvaliacao" ) Long idAvaliacao )
        throws MessageException {

        try {

            return Response.ok( service.findByAvaliacaoAgrupado( idAvaliacao ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error findByAvaliacaoAgrupado():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/questionarioRespondido" )
    @GET
    public Response questionarioRespondido(
        @QueryParam( "idQuestionario" ) Long idQuestionario,
        @QueryParam( "idPesquisa" ) Long idPesquisa,
        @QueryParam( "idAvaliado" ) Long idAvaliado,
        @QueryParam( "idAvaliador" ) Long idAvaliador )
        throws MessageException {

        try {

            Pesquisa pesquisa = new Pesquisa( idPesquisa );
            Questionario questionario = new Questionario( idQuestionario );
            Funcionario avaliado = new Funcionario( idAvaliado );
            Funcionario avaliador = new Funcionario( idAvaliador );

            return Response.ok( service.questionarioRespondido( pesquisa, questionario, avaliado, avaliador ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/deleteDuplicadas" )
    @GET
    public Response deleteDuplicadas() {

        try {
            return Response.ok( service.removerDuplicadas( null ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }

}
