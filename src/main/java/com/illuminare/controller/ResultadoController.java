package com.illuminare.controller;


import static com.illuminare.util.NumberUtil.notNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.illuminare.entity.ApplicationProperty;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.resultado.Resultado;
import com.illuminare.resultado.Resultados;
import com.illuminare.service.ApplicationPropertyService;
import com.illuminare.service.ResultadoService2;
import com.illuminare.util.DateTimeUtils;
import com.illuminare.view.AppError;
import com.illuminare.view.CacheEntry;
import com.illuminare.view.PesquisaResultView;


@Controller
@Path( "/resultado" )
public class ResultadoController extends GenericController {

    Logger log = Logger.getLogger( ResultadoController.class );

    // @Autowired
    // ResultadoService service;

    @Autowired
    private ResultadoService2 service;

    @Autowired
    private ApplicationPropertyService applicationPropertyService;

    private Map< String, CacheEntry > cache = new HashMap();


    private List< Long > getIds( String idsString ) {

        List< Long > ids = new ArrayList<>();

        if ( idsString != null ) {
            for ( String id : idsString.trim().split( "," ) ) {
                ids.add( Long.valueOf( id ) );
            }

        }

        return ids;
    }


    @Path( "/{idAvaliado}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response gerarResultadoPorFuncionarioAvaliado( @QueryParam( "idsPesquisa" ) String idsPesquisa, @PathParam( "idAvaliado" ) Long idAvaliado ) {

        try {

            Assert.notNull( idsPesquisa );
            Assert.notNull( idAvaliado );

            List< Resultado > resultados = new ArrayList<>();

            for ( Long idPesquisa : getIds( idsPesquisa ) ) {

                Resultado resultado = service.resultadoPorFuncionarioAvaliado( idPesquisa, idAvaliado, getUserNameLoggedUser() );
                resultados.add( resultado );
            }

            return Response.ok( sumarizarResultados( resultados ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error gerarResultadoPorFuncionarioAvaliado():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    private ObjectMapper getObjectMapper() {

        return new ObjectMapper().disable( SerializationFeature.FAIL_ON_EMPTY_BEANS );
    }


    @Path( "/byGrupoOcupacional/{idGrupoOcupacional}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response gerarResultadoPorGrupoOcupacional(
        @QueryParam( "idsPesquisa" ) String idsPesquisa,
        @PathParam( "idGrupoOcupacional" ) Long idGrupoOcupacional ) {

        try {
            Assert.notNull( idsPesquisa );
            Assert.notNull( idGrupoOcupacional );

            List< Resultado > resultados = new ArrayList<>();

            for ( Long idPesquisa : getIds( idsPesquisa ) ) {
                Resultado resultado = service.resultadoPorGrupoOcupacional( idPesquisa, idGrupoOcupacional, getUserNameLoggedUser() );
                resultados.add( resultado );
            }

            return Response.ok( sumarizarResultados( resultados ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error gerarResultadoPorGrupoOcupacional():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/byCargo/{idCargo}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response gerarResultadoPorCargo( @QueryParam( "idsPesquisa" ) String idsPesquisa, @PathParam( "idCargo" ) Long idCargo ) {

        try {

            Assert.notNull( idsPesquisa );
            Assert.notNull( idCargo );

            List< Resultado > resultados = new ArrayList<>();

            for ( Long idPesquisa : getIds( idsPesquisa ) ) {
                Resultado resultado = service.resultadoPorCargo( idPesquisa, idCargo, getUserNameLoggedUser() );
                resultados.add( resultado );
            }

            return Response.ok( sumarizarResultados( resultados ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error gerarResultadoPorCargo():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/bySetor/{idSetor}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response gerarResultadoPorSetor( @QueryParam( "idsPesquisa" ) String idsPesquisa, @PathParam( "idSetor" ) Long idSetor ) {

        try {
            Assert.notNull( idsPesquisa );
            Assert.notNull( idSetor );

            List< Resultado > resultados = new ArrayList<>();

            for ( Long idPesquisa : getIds( idsPesquisa ) ) {
                Resultado resultado = service.resultadoPorSetor( idPesquisa, idSetor, getUserNameLoggedUser() );
                resultados.add( resultado );
            }

            return Response.ok( sumarizarResultados( resultados ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error gerarResultadoPorSetor():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Path( "/byEmpresa/{idEmpresa}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response gerarResultadoPorEmpresa( @QueryParam( "idsPesquisa" ) String idsPesquisa, @PathParam( "idEmpresa" ) Long idEmpresa ) {

        try {
            Assert.notNull( idsPesquisa );
            Assert.notNull( idEmpresa );

            // String cacheId = "byEmpresa" + idsPesquisa + idEmpresa;
            // Map[] cache = getCache( cacheId );
            // if ( cache != null ) {
            // return Response.ok( cache ).build();
            // }

            List< Resultado > resultados = new ArrayList<>();

            for ( Long idPesquisa : getIds( idsPesquisa ) ) {
                Resultado resultado = service.resultadoPorEmpresa( idPesquisa, idEmpresa, getUserNameLoggedUser() );
                resultados.add( resultado );
            }

            // Map[] resultMap = getObjectMapper().convertValue( resultados, Map[].class );
            // putInCache( cacheId, resultMap );
            return Response.ok( sumarizarResultados( resultados ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error gerarResultadoPorSetor():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    private Resultados sumarizarResultados( List< Resultado > resultadosList ) {

        Resultados resultados = new Resultados();
        if ( isNotEmpty( resultadosList ) ) {
            resultados.setResultadoPesquisas( resultadosList );
            for ( Resultado resultado : resultadosList ) {
                somarNotasResultados( resultado.getPesquisa(), resultados );
            }
            gerarMediaResultados( resultados );
        }

        return resultados;
    }


    private void gerarMediaResultados( Resultados resultados ) {

        if ( notNull( resultados.getMediaPropria() ) && resultados.qtdeTotalPropria > 0 ) {
            resultados.setMediaPropria( resultados.getMediaPropria() / resultados.qtdeTotalPropria );
        }

        if ( notNull( resultados.getMediaLideres() ) && resultados.qtdeTotalLider > 0 ) {
            resultados.setMediaLideres( resultados.getMediaLideres() / resultados.qtdeTotalLider );
        }

        if ( notNull( resultados.getMediaSubordinados() ) && resultados.qtdeTotalSubordinados > 0 ) {
            resultados.setMediaSubordinados( resultados.getMediaSubordinados() / resultados.qtdeTotalSubordinados );
        }

        if ( notNull( resultados.getMediaOutros() ) && resultados.qtdeTotalOutros > 0 ) {
            resultados.setMediaOutros( resultados.getMediaOutros() / resultados.qtdeTotalOutros );
        }

        if ( notNull( resultados.getMeta() ) && resultados.qtdeTotalMeta > 0 ) {
            resultados.setMeta( resultados.getMeta() / resultados.qtdeTotalMeta );
        }

        if ( notNull( resultados.getValorObtido() ) && resultados.qtdeTotalValorObtido > 0 ) {
            resultados.setValorObtido( resultados.getValorObtido() / resultados.qtdeTotalValorObtido );
        }

        if ( notNull( resultados.getPercentualMeta() ) && resultados.qtdeTotalPercentualMeta > 0 ) {
            resultados.setPercentualMeta( resultados.getPercentualMeta() / resultados.qtdeTotalPercentualMeta );
        }
    }


    private void somarNotasResultados( PesquisaResultView pesquisa, Resultados resultados ) {

        if ( notNull( pesquisa.getMediaPropria() ) ) {
            if ( resultados.getMediaPropria() == null ) {
                resultados.setMediaPropria( 0d );
            }
            resultados.setMediaPropria( resultados.getMediaPropria() + pesquisa.getMediaPropria() );
            resultados.qtdeTotalPropria++;
        }

        if ( notNull( pesquisa.getMediaLideres() ) ) {
            if ( resultados.getMediaLideres() == null ) {
                resultados.setMediaLideres( 0d );
            }
            resultados.setMediaLideres( resultados.getMediaLideres() + pesquisa.getMediaLideres() );
            resultados.qtdeTotalLider++;
        }

        if ( notNull( pesquisa.getMediaSubordinados() ) ) {
            if ( resultados.getMediaSubordinados() == null ) {
                resultados.setMediaSubordinados( 0d );
            }
            resultados.setMediaSubordinados( resultados.getMediaSubordinados() + pesquisa.getMediaSubordinados() );
            resultados.qtdeTotalSubordinados++;
        }

        if ( notNull( pesquisa.getMediaOutros() ) ) {
            if ( resultados.getMediaOutros() == null ) {
                resultados.setMediaOutros( 0d );
            }
            resultados.setMediaOutros( resultados.getMediaOutros() + pesquisa.getMediaOutros() );
            resultados.qtdeTotalOutros++;
        }

        if ( notNull( pesquisa.getMeta() ) ) {
            if ( resultados.getMeta() == null ) {
                resultados.setMeta( 0d );
            }
            resultados.setMeta( resultados.getMeta() + pesquisa.getMeta() );
            resultados.qtdeTotalMeta++;
        }

        if ( notNull( pesquisa.getValorObtido() ) ) {
            if ( resultados.getValorObtido() == null ) {
                resultados.setValorObtido( 0d );
            }
            resultados.setValorObtido( resultados.getValorObtido() + pesquisa.getValorObtido() );
            resultados.qtdeTotalValorObtido++;
        }

        if ( notNull( pesquisa.getPercentualMeta() ) ) {
            if ( resultados.getPercentualMeta() == null ) {
                resultados.setPercentualMeta( 0d );
            }
            resultados.setPercentualMeta( resultados.getPercentualMeta() + pesquisa.getPercentualMeta() );
            resultados.qtdeTotalPercentualMeta++;
        }

    }


    private void putInCache( String cacheId, Map[] resultMap ) {

        if ( cache.get( cacheId ) == null ) {
            cache.put( cacheId, new CacheEntry( resultMap ) );
        }
    }


    private Map[] getCache( String cacheId )
        throws ServiceException {

        CacheEntry cacheEntry = cache.get( cacheId );
        if ( cacheEntry != null ) {
            Long expireHoursCache = getExpireHoursCache();
            if ( expireHoursCache > 0L && DateTimeUtils.getDifferenceHours( cacheEntry.getDate(), new Date() ) <= expireHoursCache ) {
                return cacheEntry.getData();
            } else {
                cache.put( cacheId, null );
            }
        }
        return null;
    }


    private Long getExpireHoursCache()
        throws ServiceException {

        ApplicationProperty prop = applicationPropertyService.findByKey( "expireHoursCache" );
        if ( prop != null ) {
            String val = prop.getValue();
            if ( isNotBlank( val ) ) {
                return Long.valueOf( val );
            } else {
                prop.setValue( "6" );
                prop.setUpdatedAt( new Date() );
                prop.setUpdatedBy( "system" );
                applicationPropertyService.update( prop, "system" );
            }
        } else {
            prop = new ApplicationProperty();
            prop.setKeyProp( "expireHoursCache" );
            prop.setValue( "6" );
            prop.setCreatedAt( new Date() );
            prop.setCreatedBy( "system" );
            prop.setActive( true );
            applicationPropertyService.save( prop, "system" );
        }

        return 6L;
    }

}
