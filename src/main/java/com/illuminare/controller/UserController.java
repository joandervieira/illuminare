package com.illuminare.controller;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.illuminare.entity.User;
import com.illuminare.exceptions.MessageException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.security.TokenUtils;
import com.illuminare.security.UserTransfer;
import com.illuminare.service.UserService;
import com.illuminare.view.AppError;
import com.illuminare.view.Paginator;


@Component
@Path( "/user" )
public class UserController extends GenericController {

    Logger log = Logger.getLogger( UserController.class );

    @Autowired
    private UserDetailsService userService;

    @Autowired
    private UserService appUserService;

    @Autowired
    @Qualifier( "authenticationManager" )
    private AuthenticationManager authManager;

    @Autowired
    @Qualifier( "passwordEncoder" )
    private PasswordEncoder passwordEncoder;


    /**
     * Retrieves the currently logged in user.
     *
     * @return A transfer containing the username and the roles.
     */
    @GET
    @Produces( MediaType.APPLICATION_JSON )
    public Response getUser() {

        // try {
        log.debug( "######## getUser()" );
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.debug( "authentication:" + authentication );
        Object principal = authentication.getPrincipal();
        log.debug( "principal:" + principal );

        if ( principal instanceof String && ( (String) principal ).equals( "anonymousUser" ) ) {
            throw new WebApplicationException( 401 );
        }
        UserDetails userDetails = (UserDetails) principal;

        // return new UserTransfer(userDetails.getUsername(),
        // this.createRoleMap(userDetails));
        return Response.ok( userDetails ).build();

        /*
         * } catch (ServiceException e) { return
         * Response.status(Status.BAD_REQUEST).entity(new
         * AppError(e.getMessage())).build(); }
         */
    }


    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    @GET
    public Response findById( @PathParam( "id" ) Long id, @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch ) {

        try {
            return Response.ok( appUserService.findById( id, fieldsToFetch ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error findById():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    /**
     * Authenticates a user and creates an authentication token.
     *
     * @param username
     *            The name of the user.
     * @param password
     *            The password of the user.
     * @return A transfer containing the authentication token.
     */
    @Path( "authenticate" )
    @POST
    @Produces( MediaType.APPLICATION_JSON )
    public Response authenticate( @FormParam( "username" ) String username, @FormParam( "password" ) String password ) {

        log.debug( "######## authenticate()" );

        try {
            log.debug( "password use:" + passwordEncoder.encode( password ) );

            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken( username, password );
            Authentication authentication = this.authManager.authenticate( authenticationToken );
            SecurityContextHolder.getContext().setAuthentication( authentication );

            appUserService.salvarUltimoAcesso( username );

            /*
             * Reload user as password of authentication principal will be null after
             * authorization and password is needed for token generation
             */
            // UserDetails userDetails =
            // this.userService.loadUserByUsername(username);
            User user = this.appUserService.findByName( username, Arrays.asList( "roles" ) );
            UserTransfer userT = new UserTransfer();
            userT.setName( username );
            userT.setRoles( this.createRoleMap( user ) );
            userT.setToken( TokenUtils.createToken( user ) );
            userT.setSenhaAtualizada( user.getSenhaAtualizada() );
            userT.setId( user.getId() );
            // userT.setFuncionario(user.getFuncionario());

            // TokenTransfer tokenTransfer = new
            // TokenTransfer(TokenUtils.createToken(userDetails));

            // return Response.ok(tokenTransfer).build();
            log.info( "usando logado:" + user );
            return Response.ok( userT ).build();

        } catch ( BadCredentialsException e ) {
            // e.printStackTrace();
            log.error( "error authenticate():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( "Usuário ou Senha inválido" ) ).build();

        } catch ( ServiceException e ) {
            // e.printStackTrace();
            log.error( "error authenticate():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    private Map< String, Boolean > createRoleMap( UserDetails userDetails ) {

        Map< String, Boolean > roles = new HashMap< String, Boolean >();
        for ( GrantedAuthority authority : userDetails.getAuthorities() ) {
            roles.put( authority.getAuthority(), Boolean.TRUE );
        }

        return roles;
    }


    @GET
    @Produces( MediaType.APPLICATION_JSON )
    @Path( "todos" )
    public Response todos( @QueryParam( "fieldsToFetch" ) List< String > fieldsToFetch )
        throws ServiceException {

        log.debug( "######## user todos()" );
        return Response.ok( appUserService.listAll( fieldsToFetch ) ).build();

    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    public Response salvar( User user ) {

        try {
            return Response.ok( appUserService.save( user, getUserNameLoggedUser() ) ).build();

        } catch ( ServiceException e ) {
            log.error( "error salvar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage(), e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }

    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/enviarEmailRecuperacao" )
    public Response enviarEmailRecuperacao( User user ) {

        try {
            appUserService.enviarEmailRecuperacao( user );
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error enviarEmailRecuperacao():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @PUT
    public Response atualizar( User user )
        throws MessageException {

        try {
            appUserService.update( user, getUserNameLoggedUser() );
            // return Response.ok().entity(user).build();
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/atualizarSenha" )
    @PUT
    public Response atualizarSenha( User user )
        throws MessageException {

        try {
            appUserService.atualizarSenha( user, getUserNameLoggedUser() );
            // return Response.ok().entity(user).build();
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error atualizarSenha():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @Path( "/{id}" )
    @DELETE
    public Response excluir( @PathParam( "id" ) Long id )
        throws MessageException {

        try {
            appUserService.deleteLogic( id, getUserNameLoggedUser() );
            // return Response.ok().entity(user).build();
            return Response.ok().build();

        } catch ( ServiceException e ) {
            log.error( "error excluir():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }


    @Produces( MediaType.APPLICATION_JSON )
    @POST
    @Path( "/pesquisar" )
    public Response pesquisar( Paginator< User > paginator ) {

        try {
            return Response.ok( appUserService.pesquisar( paginator, getUserNameLoggedUser() ) ).build();
        } catch ( ServiceException e ) {
            log.error( "error pesquisar():" + e.getMessage() );
            log.error( e );
            return Response.status( Status.BAD_REQUEST ).entity( new AppError( e.getMessage() ) ).build();
        } catch ( Exception e ) {
            log.error( "error :" + e.getMessage() );
            log.error( e );
            return Response.status( Status.INTERNAL_SERVER_ERROR ).entity( new AppError( e.getMessage() ) ).build();
        }
    }

}