package com.illuminare.dao;


import com.illuminare.entity.ApplicationProperty;


public interface ApplicationPropertyDAO extends GenericDAO< ApplicationProperty > {

}
