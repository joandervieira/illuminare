package com.illuminare.dao;


import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface AvaliacaoDAO extends GenericDAO< QuestionarioAvaliacao > {

    Set< QuestionarioAvaliacao > findAvaliacoesByPesquisas( Long idFuncionarioAvaliador, List< Pesquisa > pesquisas );


    Set< QuestionarioAvaliacao > findAvaliacao( Long idPesquisa, Long idFuncionarioAvaliador, Long idFuncionarioAvaliado, Date dataInicio, Date dataFim )
        throws ServiceException;


    List< QuestionarioAvaliacao > findByAvaliador( Long idFuncionarioAvaliador );


    List< QuestionarioAvaliacao > findByAvaliado( Long idFuncionarioAvaliado );


    Paginator< QuestionarioAvaliacao > findAvaliacoesByPesquisasPageable( Paginator< QuestionarioAvaliacao > paginator, boolean distinctFuncionarios );


    List< QuestionarioAvaliacao > findAvaliacao( List< String > fieldsToFetch, Map< String, Object > fieldsToSearch );


    List< QuestionarioAvaliacao > findByFuncionarioAvaliadoAndAvaliadorAndPesquisa(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa );


    QuestionarioAvaliacao findByFuncionarioAvaliadoAndAvaliadorAndPesquisaAndQuestionario(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa,
        Questionario questionario );


    List< QuestionarioAvaliacao > findByPesquisa( Pesquisa pesquisa );


    void deleteByPesquisa( Pesquisa pesquisa );


    void deleteByPesquisaAndQuestionarioAndFuncionarioAvaliado( Pesquisa pesquisa, Questionario questionario, Funcionario funcionarioAvaliado );


    void deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador );


    void deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior );


    List< Funcionario > findFuncionariosByPesquisaAndQuestionario( Pesquisa pesquisa, Questionario questionario );


    List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador );


    List< QuestionarioAvaliacao > findByPesquisaAndQuestionario( Pesquisa pesquisa, Questionario questionario );


    List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior );


    List< QuestionarioAvaliacao > findByPesquisaAndAvaliado( Long idPesquisa, List< Long > idsFuncionarioAvaliado );
}
