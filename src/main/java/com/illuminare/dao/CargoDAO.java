package com.illuminare.dao;


import com.illuminare.entity.Cargo;


public interface CargoDAO extends GenericDAO< Cargo > {

}
