package com.illuminare.dao;


import com.illuminare.entity.Empresa;


public interface EmpresaDAO extends GenericDAO< Empresa > {

}
