package com.illuminare.dao;


import com.illuminare.entity.Estado;


public interface EstadoDAO extends GenericDAO< Estado > {

}
