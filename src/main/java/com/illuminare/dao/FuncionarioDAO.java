package com.illuminare.dao;


import java.util.List;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Setor;


public interface FuncionarioDAO extends GenericDAO< Funcionario > {

    Funcionario findByCpf( Long idEmpresa, String cpf, List< String > fieldsToFetch );


    List< Funcionario > findByGrupoOcupacional( GrupoOcupacional grupoOcupacional );


    List< Funcionario > findByCargo( Cargo cargo );


    List< Funcionario > findBySetor( Setor setor );


    List< Funcionario > findByEmpresa( Empresa empresa );
}
