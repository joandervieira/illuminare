package com.illuminare.dao;


import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;

import com.illuminare.exceptions.DAOException;
import com.illuminare.view.Paginator;


public interface GenericDAO< T > {

    T findById( Long id );


    int deleteLogic( Long id );


    void flush();


    T save( T entity );


    void update( T entity );


    List< T > saveAll( List< T > entities );


    List< T > updateAll( List< T > entities );


    @SuppressWarnings( "unchecked" )
    // public static < T > T unProxy( T entity ) {
    //
    // if ( entity instanceof HibernateProxy ) {
    // entity = (T) ( (HibernateProxy) entity
    // ).getHibernateLazyInitializer().getImplementation();
    // }
    //
    // return entity;
    // }
    < T > T unproxyInitialize( T entity );


    List< T > findByAttr( Map< String, Object > attrs );


    List< T > listAllOrderBy( Long idEmpresa, String orderBy );


    List< T > listAll( Long idEmpresa );


    List< T > listAll( Long idEmpresa, Set< String > fieldsToFetch );


    List< T > listAll( Long idEmpresa, Set< String > fieldsToFetch, String orderBy );


    // Paginator<T> listAllOrderBy(Paginator<T> pagination);

    void delete( T entity );


    void refresh( T entity );


    List< T > findByIdEmpresa( Long idEmpresa );


    T findById( Object pk );


    Session getSession();


    int executeNativeUpdate( String sql )
        throws DAOException;


    void detach( T entity );


    Paginator< T > pesquisar( Paginator< T > paginator );


    T findById( Long id, Set< String > fieldsToFetch );


    List< T > findByJpql( String jpql, Map< String, Object > attrs );


    Set< T > saveAll( Set< T > entities );


    @SuppressWarnings( "rawtypes" )
    List findByJpql( String jpql, Map< String, Object > attrs, Class clazz );


    @SuppressWarnings( "rawtypes" )
    Object findByJpqlSingle( String jpql, Map< String, Object > attrs, Class clazz );


    List< T > findByJpql( String jpql, Map< String, Object > attrs, Integer firstResult, Integer maxResult );


    List< T > findByAttr( Map< String, Object > attrs, List< String > fieldsToFetch );


    int updateByJpql( String jpql, Map< String, Object > attrs );

}