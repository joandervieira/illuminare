package com.illuminare.dao;


import com.illuminare.entity.GrupoQuestao;


public interface GrupoQuestaoDAO extends GenericDAO< GrupoQuestao > {

    Long totalQuestoesDoGrupo( Long idGrupo );
}
