package com.illuminare.dao;


import java.util.List;

import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;


public interface PesquisaDAO extends GenericDAO< Pesquisa > {

    List< Pesquisa > findByQuestionario( List< Questionario > questionarios );


    List< Pesquisa > findPesquisasEmAberto( Long idEmpresa );


    List< Pesquisa > findPesquisasFechadas( Long idEmpresa );


    List< Pesquisa > findPesquisasEmAberto( Long idEmpresa, List< String > fieldsToFetch );

}
