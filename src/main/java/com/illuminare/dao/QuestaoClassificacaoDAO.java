package com.illuminare.dao;


import java.util.List;

import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.Questionario;


public interface QuestaoClassificacaoDAO extends GenericDAO< QuestaoClassificacao > {

    List< QuestaoClassificacao > findByQuestionarioAndQuestao( Questionario questionario, Questao questao, GrupoQuestao grupoQuestao );


    void deleteByQuestionario( Questionario questionario );
}
