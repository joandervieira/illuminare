package com.illuminare.dao;


import java.util.List;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Setor;


public interface QuestaoMetaDAO extends GenericDAO< QuestaoMeta > {

    QuestaoMeta findByQuestaoAndPesquisa( Questao questao, GrupoQuestao grupoQuestao, Pesquisa pesquisa );


    QuestaoMeta findByQuestaoAndPesquisaAndGrupoOcupacional(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        GrupoOcupacional grupoOcupacional );


    QuestaoMeta findByQuestaoAndPesquisaAndCargo( Questao questao, GrupoQuestao grupoQuestao, Pesquisa pesquisa, Questionario questionario, Cargo cargo );


    List< QuestaoMeta > findByQuestionario( Long idQuestionario );


    List< QuestaoMeta > findByPesquisa( Long idPesquisa );


    QuestaoMeta findByQuestaoAndPesquisaAndSetor( Questao questao, GrupoQuestao grupoQuestao, Pesquisa pesquisa, Questionario questionario, Setor setor );


    void deleteByPesquisa( Pesquisa pesquisa, String user );
}
