package com.illuminare.dao;


import java.util.List;

import com.illuminare.entity.Questao;
import com.illuminare.entity.Questionario;


public interface QuestionarioDAO extends GenericDAO< Questionario > {

    List< Questionario > findByQuestao( List< Questao > questoes );

}
