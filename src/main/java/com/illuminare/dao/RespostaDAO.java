package com.illuminare.dao;


import java.util.List;
import java.util.Map;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Resposta;
import com.illuminare.exceptions.DAOException;
import com.illuminare.exceptions.ServiceException;


public interface RespostaDAO extends GenericDAO< Resposta > {

    List< Resposta > findByAvaliado( Long idAvaliado );


    List< Resposta > findByAvaliado( Long idAvaliado, List< Long > idsPesquisas )
        throws ServiceException;


    List< Resposta > findByAvaliacao( Long idQuestionarioAvaliacao );


    List< Resposta > findByAvaliado( List< Funcionario > avaliados, Pesquisa pesquisa );


    List< Resposta > findByAvaliado( Funcionario funcionarioAvaliado, Pesquisa pesquisa );


    Boolean checkIsRespnodidoByAvaliacao( Long idQuestionarioAvaliacao );


    List< Resposta > findByAvaliacaoAndQuestao( Long idQuestionarioAvaliacao, Long idQuestao, Long idGrupoQuestao );


    int deleteByAvaliacao( Long idAvaliacao )
        throws DAOException;


    int deleteById( Long id );


    List< Map< String, Object > > find( Long idQuestionarioAvaliacao, Long idQuestao, Long idGrupoQuestao );
}
