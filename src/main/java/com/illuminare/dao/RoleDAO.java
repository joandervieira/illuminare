package com.illuminare.dao;


import com.illuminare.entity.Role;


public interface RoleDAO extends GenericDAO< Role > {

    Role findByName( String name );

}
