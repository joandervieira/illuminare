package com.illuminare.dao;


import com.illuminare.entity.TipoEndereco;


public interface TipoEnderecoDAO extends GenericDAO< TipoEndereco > {

}
