package com.illuminare.dao;


import com.illuminare.entity.TipoEnderecoEletronico;


public interface TipoEnderecoEletronicoDAO extends GenericDAO< TipoEnderecoEletronico > {

}
