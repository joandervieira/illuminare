package com.illuminare.dao;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.illuminare.entity.User;


public interface UserDAO extends GenericDAO< User >, UserDetailsService {

    String getSenhaUsuario( Long id );


    Boolean usuarioJaExiste( String userName );


    List< User > listarAdministradores();


    User findByName( String name, List< String > fieldsToFetch );


    List< User > listAll( List< String > fieldsToFetch );


    User findByFuncionario( Long idFuncionario );

}
