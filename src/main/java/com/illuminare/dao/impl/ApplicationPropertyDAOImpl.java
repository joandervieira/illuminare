package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.ApplicationPropertyDAO;
import com.illuminare.entity.ApplicationProperty;


@Component
public class ApplicationPropertyDAOImpl extends GenericDAOImpl< ApplicationProperty > implements ApplicationPropertyDAO {

    public ApplicationPropertyDAOImpl() {

        super( ApplicationProperty.class );
    }

}
