package com.illuminare.dao.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.illuminare.dao.AvaliacaoDAO;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.RespostaService;
import com.illuminare.view.Paginator;


@Component
public class AvaliacaoDAOImpl extends GenericDAOImpl< QuestionarioAvaliacao > implements AvaliacaoDAO {

    @Autowired
    RespostaService respostaService;

    public AvaliacaoDAOImpl() {

        super( QuestionarioAvaliacao.class );
    }

    @Override
    public Set< QuestionarioAvaliacao > findAvaliacoesByPesquisas( Long idFuncionarioAvaliador, List< Pesquisa > pesquisas ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql +=
            " left join fetch e.funcionarioAvaliado avaliado   left join fetch avaliado.cargo cargoAvaliado left join fetch cargoAvaliado.grupoOcupacional left join fetch avaliado.pessoa pessoaAvaliado ";
        jpql +=
            " left join fetch e.funcionarioAvaliador avaliador left join fetch avaliador.cargo cargoAvaliador left join fetch cargoAvaliador.grupoOcupacional left join fetch avaliador.pessoa pessoaAvaliador ";
        jpql += " left join fetch e.questionario questionario ";
        jpql += " left join fetch e.pesquisa pesquisa ";

        jpql += " where pesquisa.id in (:idsPesquisas) ";
        if ( idFuncionarioAvaliador != null ) {
            jpql += " and avaliador.id = :idFuncionarioAvaliador ";
        }

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (questionario.excluido = false or questionario.excluido = null) ";

        // jpql +=
        // " and e.id not in (select id from Resposta resp left join fetch
        // questionarioAvaliacao avaliacaoResp left join fetch
        // avaliacao.funcionarioAvaliado avaliadoResp where avaliadoResp.id =
        // avaliado.id) ";

        List< Long > ids = new ArrayList< Long >();
        for ( Pesquisa p : pesquisas ) {
            if ( p.getId() != null ) {
                ids.add( p.getId() );
            }
        }
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idsPesquisas", ids );
        if ( idFuncionarioAvaliador != null ) {
            attrs.put( "idFuncionarioAvaliador", idFuncionarioAvaliador );
        }

        return new HashSet<>( findByJpql( jpql, attrs ) );

    }


    @SuppressWarnings( "unchecked" )
    @Override
    public Paginator< QuestionarioAvaliacao > findAvaliacoesByPesquisasPageable( Paginator< QuestionarioAvaliacao > paginator, boolean distinctFuncionarios ) {

        if ( paginator == null ) {
            paginator = new Paginator<>();
        }
        String sqlSelect = "select e from QuestionarioAvaliacao e ";
        String sqlSelectTotalClause = "select count(e.id) from QuestionarioAvaliacao e ";

        String sqlFetches = " ", sqlWhere = " ";

        sqlFetches +=
            " left join fetch e.funcionarioAvaliado avaliado   left join fetch avaliado.cargo cargoAvaliado left join fetch cargoAvaliado.grupoOcupacional left join fetch avaliado.pessoa pessoaAvaliado ";
        sqlFetches +=
            " left join fetch e.funcionarioAvaliador avaliador left join fetch avaliador.cargo cargoAvaliador left join fetch cargoAvaliador.grupoOcupacional left join fetch avaliador.pessoa pessoaAvaliador ";
        sqlFetches += " left join fetch e.questionario questionario ";
        sqlFetches += " left join fetch e.pesquisa pesquisa ";
        sqlFetches += " left join fetch e.respostas respostas ";

        sqlWhere += " where 1 = 1 ";

        if ( paginator.getFieldsToSearch() != null ) {
            for ( Map.Entry< String, Object > field : paginator.getFieldsToSearch().entrySet() ) {

                String column, paramKey, comparator;

                if ( "respondido".equals( field.getKey() ) ) {
                    sqlWhere += " and respostas.id is ";
                    if ( (Boolean) field.getValue() ) {
                        sqlWhere += "not ";
                    }
                    sqlWhere += "null ";
                    continue;
                }

                if ( field.getKey().contains( "[" ) && field.getKey().contains( "]" ) ) {
                    column = field.getKey();
                    column = column.replace( "[", "" ).replace( "]", "" );
                } else {
                    column = "e." + field.getKey();
                }
                paramKey = generateParamKey( field.getKey(), field.getValue(), true );

                comparator = " = ";
                if ( field.getValue() instanceof Collection ) {
                    comparator = " in ";
                    sqlWhere += " and " + column + comparator + " (" + paramKey + " ) ";
                } else {
                    sqlWhere += " and " + column + comparator + paramKey;
                }
            }
        }

        sqlWhere += " and (e.excluido = false or e.excluido = null) ";
        if ( distinctFuncionarios ) {
            sqlWhere += " group by  pesquisa, avaliado, avaliador ";
        }

        if ( StringUtils.isNotBlank( paginator.getOrderBy() ) ) {
            sqlWhere += generetedOrderBy( paginator.getOrderBy() );
        }

        Map< String, Object > attrs = new HashMap<>();
        if ( paginator.getFieldsToSearch() != null ) {
            for ( Map.Entry< String, Object > field : paginator.getFieldsToSearch().entrySet() ) {
                if ( "respondido".equals( field.getKey() ) ) {
                    continue;
                }
                String paramKey = generateParamKey( field.getKey(), field.getValue(), false );
                attrs.put( paramKey, field.getValue() );
            }
        }

        List< Long > totalList = findByJpql( sqlSelectTotalClause + sqlFetches.replace( "fetch", "" ) + sqlWhere, attrs, Long.class );
        Long total = totalList != null ? totalList.size() : 0L;

        List< QuestionarioAvaliacao > result;
        if ( paginator.getPageNumber() != null && paginator.getPageSize() != null ) {
            Integer firstResult = Long.valueOf( ( paginator.getPageNumber() - 1 ) * paginator.getPageSize() ).intValue();
            Integer maxResult = paginator.getPageSize().intValue();
            result = findByJpql( sqlSelect + sqlFetches + sqlWhere, attrs, firstResult, maxResult );
        } else {
            result = findByJpql( sqlSelect + sqlFetches + sqlWhere, attrs );
        }

        paginator.setResult( result );
        paginator.setTotal( total );

        return paginator;

    }


    @Override
    public List< QuestionarioAvaliacao > findByAvaliador( Long idFuncionarioAvaliador ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " left join fetch e.funcionarioAvaliado avaliado ";
        jpql += " left join fetch e.funcionarioAvaliador avaliador ";
        jpql += " left join fetch e.questionario questionario";

        jpql += " where avaliador.id = : idFuncionarioAvaliador";

        jpql += " and (e.excluido = false or e.excluido = null)";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idFuncionarioAvaliador", idFuncionarioAvaliador );
        return findByJpql( jpql, attrs );

    }


    @Override
    public List< QuestionarioAvaliacao > findByAvaliado( Long idFuncionarioAvaliado ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " left join fetch e.funcionarioAvaliado avaliado ";
        jpql += " left join fetch e.funcionarioAvaliador avaliador ";
        jpql += " left join fetch e.questionario questionario";

        jpql += " where avaliado.id = :idFuncionarioAvaliado ";

        jpql += " and (e.excluido = false or e.excluido = null)";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idFuncionarioAvaliado", idFuncionarioAvaliado );

        return findByJpql( jpql, attrs );

    }


    @Override
    public Set< QuestionarioAvaliacao > findAvaliacao( Long idPesquisa, Long idFuncionarioAvaliador, Long idFuncionarioAvaliado, Date dataInicio, Date dataFim )
        throws ServiceException {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " left join fetch e.pesquisa pesquisa ";
        // jpql +=
        // " left join fetch e.questionario questionario left join fetch
        // questionario.gruposQuestoes grupoQuestao left join fetch
        // grupoQuestao.questoesOrdem questaoOrdem left join fetch
        // questaoOrdem.questao questao left join fetch questao.listaValores";
        jpql += " left join fetch e.questionario questionario ";
        jpql += " left join fetch e.funcionarioAvaliador avaliador left join fetch avaliador.pessoa pessoaAvaliador ";
        jpql += " left join fetch e.funcionarioAvaliado avaliado left join fetch avaliado.pessoa pessoaAvaliado ";

        jpql += " where ";
        jpql += " avaliador.id = :idFuncionarioAvaliador ";
        jpql += " and avaliado.id = :idFuncionarioAvaliado ";
        jpql += " and pesquisa.id = :idPesquisa ";

        if ( dataInicio != null ) {
            jpql += " and pesquisa.dtaInicio <= :dataInicio ";
        }
        if ( dataFim != null ) {
            jpql += " and pesquisa.dtaFim >= :dataFim ";
        }

        // List< Resposta > respostasAvaliacoesRespondidas =
        // respostaService.findByAvaliado( idFuncionarioAvaliado );
        // if ( CollectionUtils.isNotEmpty( respostasAvaliacoesRespondidas ) ) {
        // jpql += " and e.id not in (:idsAvaliacoesRespondidas) ";
        // }

        jpql += " and (e.excluido = false or e.excluido = null)";
        jpql += " and (e.ativo = true or e.ativo = null)";

        jpql += " and (questionario.excluido = false or questionario.excluido = null)";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idFuncionarioAvaliador", idFuncionarioAvaliador );
        attrs.put( "idFuncionarioAvaliado", idFuncionarioAvaliado );
        attrs.put( "idPesquisa", idPesquisa );

        if ( dataInicio != null ) {
            attrs.put( "dataInicio", dataInicio );
        }
        if ( dataFim != null ) {
            attrs.put( "dataFim", dataFim );
        }
        // if ( CollectionUtils.isNotEmpty( respostasAvaliacoesRespondidas ) ) {
        // Set< Long > idsAvaliacoes = new HashSet<>();
        // for ( Resposta resp : respostasAvaliacoesRespondidas ) {
        // if ( resp.getQuestionarioAvaliacao() != null ) {
        // idsAvaliacoes.add( resp.getQuestionarioAvaliacao().getId() );
        // }
        // }
        // attrs.put( "idsAvaliacoesRespondidas", idsAvaliacoes );
        // }

        return new HashSet<>( findByJpql( jpql, attrs ) );

    }


    @Override
    public List< QuestionarioAvaliacao > findAvaliacao( List< String > fieldsToFetch, Map< String, Object > fieldsToSearch ) {

        String sqlFrom = "select e from QuestionarioAvaliacao e ";

        String sqlWhere = " where 1 = 1 ";

        if ( fieldsToFetch != null ) {
            for ( String field : fieldsToFetch ) {
                String leftJoinFech = " left join fetch ";
                if ( !field.contains( "[" ) && !field.contains( "]" ) ) {
                    leftJoinFech += " e.";
                }
                field = field.replace( "[", "" ).replace( "]", "" );
                sqlFrom += leftJoinFech + field;
            }
        }
        sqlWhere += " and (e.excluido = false or e.excluido = null)";
        sqlWhere += " and (e.ativo = true or e.ativo = null)";

        if ( fieldsToSearch != null ) {
            for ( Map.Entry< String, Object > field : fieldsToSearch.entrySet() ) {
                String column, paramKey, comparator;
                if ( field.getKey().contains( "[" ) && field.getKey().contains( "]" ) ) {
                    column = field.getKey();
                    column = column.replace( "[", "" ).replace( "]", "" );
                } else {
                    column = "e." + field.getKey();
                }
                paramKey = generateParamKey( field.getKey(), field.getValue(), true );

                comparator = " = ";
                if ( field.getValue() instanceof Collection ) {
                    comparator = " in ";
                    sqlWhere += " and " + column + comparator + " (" + paramKey + " ) ";
                } else {
                    sqlWhere += " and " + column + comparator + paramKey;
                }
            }
        }

        Map< String, Object > attrs = new HashMap<>();
        if ( fieldsToSearch != null ) {
            for ( Map.Entry< String, Object > field : fieldsToSearch.entrySet() ) {
                String paramKey = generateParamKey( field.getKey(), field.getValue(), false );
                attrs.put( paramKey, field.getValue() );
            }
        }

        return findByJpql( sqlFrom + sqlWhere, attrs );

    }


    @Override
    public List< QuestionarioAvaliacao > findByFuncionarioAvaliadoAndAvaliadorAndPesquisa(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " inner join fetch e.funcionarioAvaliado avaliado ";
        jpql += " inner join fetch e.funcionarioAvaliador avaliador ";
        jpql += " inner join fetch e.pesquisa pesquisa ";

        jpql += " where 1 = 1 ";
        jpql += " and avaliado = :avaliado ";
        jpql += " and avaliador = :avaliador ";
        jpql += " and pesquisa = :pesquisa ";

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "avaliado", funcionarioAvaliado );
        attrs.put( "avaliador", funcionarioAvaliador );
        attrs.put( "pesquisa", pesquisa );

        return findByJpql( jpql, attrs );

    }


    @Override
    public QuestionarioAvaliacao findByFuncionarioAvaliadoAndAvaliadorAndPesquisaAndQuestionario(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa,
        Questionario questionario ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " inner join fetch e.funcionarioAvaliado avaliado ";
        jpql += " inner join fetch e.funcionarioAvaliador avaliador ";
        jpql += " inner join fetch e.pesquisa pesquisa ";
        jpql += " inner join fetch e.questionario questionario ";

        jpql += " where 1 = 1 ";
        jpql += " and avaliado = :avaliado ";
        jpql += " and avaliador = :avaliador ";
        jpql += " and pesquisa = :pesquisa ";
        jpql += " and questionario = :questionario ";

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "avaliado", funcionarioAvaliado );
        attrs.put( "avaliador", funcionarioAvaliador );
        attrs.put( "pesquisa", pesquisa );
        attrs.put( "questionario", questionario );

        // return findByJpql( jpql, attrs );
        return (QuestionarioAvaliacao) findByJpqlSingle( jpql, attrs, QuestionarioAvaliacao.class );

    }


    @Override
    public List< QuestionarioAvaliacao > findByPesquisa( Pesquisa pesquisa ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " inner join e.pesquisa pesquisa ";

        jpql += " where 1 = 1 ";
        jpql += " and pesquisa = :pesquisa ";

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "pesquisa", pesquisa );

        return findByJpql( jpql, attrs );

    }


    @Override
    public void deleteByPesquisa( Pesquisa pesquisa ) {

        String jpql = "update QuestionarioAvaliacao e set e.excluido = true ";
        jpql += " where 1 = 1 ";
        jpql += " and e.pesquisa = :pesquisa ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.executeUpdate();
    }


    @Override
    public void deleteByPesquisaAndQuestionarioAndFuncionarioAvaliado( Pesquisa pesquisa, Questionario questionario, Funcionario funcionarioAvaliado ) {

        String jpql = "update QuestionarioAvaliacao e set e.excluido = true ";
        jpql += " where 1 = 1 ";
        jpql += " and e.pesquisa = :pesquisa ";
        jpql += " and e.questionario = :questionario ";
        jpql += " and e.funcionarioAvaliado = :funcionarioAvaliado ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.setParameter( "questionario", questionario );
        query.setParameter( "funcionarioAvaliado", funcionarioAvaliado );
        query.executeUpdate();
    }


    @Override
    public void deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador ) {

        String jpql = "update QuestionarioAvaliacao e set e.excluido = true ";
        jpql += " where 1 = 1 ";
        jpql += " and e.pesquisa = :pesquisa ";
        if ( questionario != null ) {
            jpql += " and e.questionario = :questionario ";
        }
        jpql += " and e.funcionarioAvaliado = :funcionarioAvaliado ";
        jpql += " and e.funcionarioAvaliador = :funcionarioAvaliador ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        if ( questionario != null ) {
            query.setParameter( "questionario", questionario );
        }
        query.setParameter( "funcionarioAvaliado", funcionarioAvaliado );
        query.setParameter( "funcionarioAvaliador", funcionarioAvaliador );
        query.executeUpdate();
    }


    @Override
    public void deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior ) {

        String jpql = "update QuestionarioAvaliacao e set e.excluido = true ";
        jpql += " where 1 = 1 ";
        jpql += " and e.pesquisa = :pesquisa ";
        jpql += " and e.questionario = :questionario ";
        jpql += " and e.funcionarioAvaliado = :funcionarioAvaliado ";
        if ( autoAvaliacao != null ) {
            jpql += " and e.autoAvaliacao = :autoAvaliacao ";
        }
        if ( avaliacaoSubordinado != null ) {
            jpql += " and e.avaliacaoSubordinado = :avaliacaoSubordinado ";
        }
        if ( avaliacaoSuperior != null ) {
            jpql += " and e.avaliacaoSuperior = :avaliacaoSuperior ";
        }

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.setParameter( "questionario", questionario );
        query.setParameter( "funcionarioAvaliado", funcionarioAvaliado );
        if ( autoAvaliacao != null ) {
            query.setParameter( "autoAvaliacao", autoAvaliacao );
        }
        if ( avaliacaoSubordinado != null ) {
            query.setParameter( "avaliacaoSubordinado", avaliacaoSubordinado );
        }
        if ( avaliacaoSuperior != null ) {
            query.setParameter( "avaliacaoSuperior", avaliacaoSuperior );
        }
        query.executeUpdate();
    }


    @Override
    public List< Funcionario > findFuncionariosByPesquisaAndQuestionario( Pesquisa pesquisa, Questionario questionario ) {

        String jpql = "select distinct e.funcionarioAvaliado from QuestionarioAvaliacao e ";
        jpql += " inner join e.pesquisa pesquisa ";
        jpql += " inner join e.questionario questionario ";

        jpql += " where 1 = 1 ";
        jpql += " and pesquisa = :pesquisa ";
        jpql += " and questionario = :questionario ";

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.setParameter( "questionario", questionario );

        return query.getResultList();
    }


    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " inner join e.pesquisa pesquisa ";
        jpql += " inner join e.questionario questionario ";
        jpql += " inner join e.funcionarioAvaliado funcionarioAvaliado ";
        jpql += " inner join e.funcionarioAvaliador funcionarioAvaliador ";

        jpql += " where 1 = 1 ";
        jpql += " and pesquisa = :pesquisa ";
        jpql += " and questionario = :questionario ";
        if ( funcionarioAvaliado != null ) {
            jpql += " and funcionarioAvaliado = :funcionarioAvaliado ";
        }
        if ( funcionarioAvaliador != null ) {
            jpql += " and funcionarioAvaliador = :funcionarioAvaliador ";
        }

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.setParameter( "questionario", questionario );
        if ( funcionarioAvaliado != null ) {
            query.setParameter( "funcionarioAvaliado", funcionarioAvaliado );
        }
        if ( funcionarioAvaliador != null ) {
            query.setParameter( "funcionarioAvaliador", funcionarioAvaliador );
        }

        return query.getResultList();
    }


    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionario( Pesquisa pesquisa, Questionario questionario ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " inner join e.pesquisa pesquisa ";
        jpql += " inner join e.questionario questionario ";

        jpql += " where 1 = 1 ";
        jpql += " and pesquisa = :pesquisa ";
        jpql += " and questionario = :questionario ";

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.setParameter( "questionario", questionario );

        return query.getResultList();
    }


    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior ) {

        String jpql = "select e from QuestionarioAvaliacao e ";
        jpql += " inner join e.pesquisa pesquisa ";
        jpql += " inner join e.questionario questionario ";
        jpql += " inner join e.funcionarioAvaliado funcionarioAvaliado ";
        jpql += " inner join e.funcionarioAvaliador funcionarioAvaliador ";

        jpql += " where 1 = 1 ";
        jpql += " and pesquisa = :pesquisa ";
        jpql += " and questionario = :questionario ";
        if ( funcionarioAvaliado != null ) {
            jpql += " and funcionarioAvaliado = :funcionarioAvaliado ";
        }
        if ( funcionarioAvaliador != null ) {
            jpql += " and funcionarioAvaliador = :funcionarioAvaliador ";
        }
        if ( autoAvaliacao != null ) {
            jpql += " and e.autoAvaliacao = :autoAvaliacao ";
        }
        if ( avaliacaoSubordinado != null ) {
            jpql += " and e.avaliacaoSubordinado = :avaliacaoSubordinado ";
        }
        if ( avaliacaoSuperior != null ) {
            jpql += " and e.avaliacaoSuperior = :avaliacaoSuperior ";
        }

        jpql += " and (e.excluido = false or e.excluido = null) ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa );
        query.setParameter( "questionario", questionario );
        if ( funcionarioAvaliado != null ) {
            query.setParameter( "funcionarioAvaliado", funcionarioAvaliado );
        }
        if ( funcionarioAvaliador != null ) {
            query.setParameter( "funcionarioAvaliador", funcionarioAvaliador );
        }

        if ( autoAvaliacao != null ) {
            query.setParameter( "autoAvaliacao", autoAvaliacao );
        }
        if ( avaliacaoSubordinado != null ) {
            query.setParameter( "avaliacaoSubordinado", avaliacaoSubordinado );
        }
        if ( avaliacaoSuperior != null ) {
            query.setParameter( "avaliacaoSuperior", avaliacaoSuperior );
        }

        return query.getResultList();
    }


    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndAvaliado( Long idPesquisa, List< Long > idsFuncionarioAvaliado ) {

        String jpql = "select avaliacao from QuestionarioAvaliacao avaliacao ";
        jpql += " where avaliacao.pesquisa.id = :idPesquisa ";
        jpql += " and avaliacao.funcionarioAvaliado.id in (:idsFuncionarioAvaliado) ";

        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null)";

        org.hibernate.Query query = getSession().createQuery( jpql.toString() );

        query.setParameter( "idPesquisa", idPesquisa );
        query.setParameterList( "idsFuncionarioAvaliado", idsFuncionarioAvaliado );

        return query.list();

    }

}
