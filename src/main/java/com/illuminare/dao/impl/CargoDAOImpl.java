package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.CargoDAO;
import com.illuminare.entity.Cargo;


@Component
public class CargoDAOImpl extends GenericDAOImpl< Cargo > implements CargoDAO {

    public CargoDAOImpl() {

        super( Cargo.class );
    }

}
