package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.CidadeDAO;
import com.illuminare.entity.Cidade;


@Component
public class CidadeDAOImpl extends GenericDAOImpl< Cidade > implements CidadeDAO {

    public CidadeDAOImpl() {

        super( Cidade.class );
    }

}
