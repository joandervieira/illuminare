package com.illuminare.dao.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.illuminare.entity.ApplicationProperty;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.EstadoCivil;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Role;
import com.illuminare.entity.Setor;
import com.illuminare.entity.TipoEndereco;
import com.illuminare.entity.TipoEnderecoEletronico;
import com.illuminare.entity.TipoTelefone;
import com.illuminare.entity.User;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.ApplicationPropertyService;
import com.illuminare.service.CargoService;
import com.illuminare.service.EstadoCivilService;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.service.RoleService;
import com.illuminare.service.SetorService;
import com.illuminare.service.TipoEnderecoEletronicoService;
import com.illuminare.service.TipoEnderecoService;
import com.illuminare.service.TipoTelefoneService;
import com.illuminare.service.UserService;
import com.illuminare.util.Constantes;


/**
 * Initialize the database with some test entries.
 * 
 */
@Service
public class DataBaseInitializer {

    Logger log = Logger.getLogger( DataBaseInitializer.class );

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private EstadoCivilService estadoCivilService;

    @Autowired
    private ApplicationPropertyService applicationPropertyService;

    @Autowired
    private TipoEnderecoEletronicoService tipoEnderecoEletronicoService;

    @Autowired
    private TipoEnderecoService tipoEnderecoService;

    @Autowired
    private TipoTelefoneService tipoTelefoneService;

    @Autowired
    private CargoService cargoService;

    @Autowired
    private SetorService setorService;

    @Autowired
    private GrupoOcupacionalService grupoOcupacionalService;

    @Autowired
    @Qualifier( "passwordEncoder" )
    private PasswordEncoder passwordEncoder;


    public void initDataBase()
        throws ServiceException {

        log.debug( "============== initDataBase" );

        ApplicationProperty initDataBase = applicationPropertyService.findByKey( Constantes.APP_PROPERTY_INIT_DATA_BASE );
        if ( initDataBase == null ) {
            initDataBase = initApplicationProperty();
        }

        if ( initDataBase.getValue().equals( "true" ) ) {
            initSecurity();
            initEstadoCivil();
            initTipoEnderecoEletronico();
            initTipoEndereco();
            initTipoTelefone();
            initCargo();

            initDataBase.setValue( "false" );
            applicationPropertyService.update( initDataBase, Constantes.USER_SYSTEM );
        }

        log.debug( "============== fim initDataBase" );

    }


    private ApplicationProperty initApplicationProperty() {

        try {
            ApplicationProperty initDataBase = new ApplicationProperty();
            initDataBase.setKeyProp( Constantes.APP_PROPERTY_INIT_DATA_BASE );
            initDataBase.setValue( "true" );
            initDataBase.setDescription( "Flag para indicar que a massa de dados inicial deve ser inserida" );
            initDataBase.setActive( true );
            initDataBase.setCreatedAt( new Date() );
            initDataBase.setCreatedBy( Constantes.USER_SYSTEM );
            initDataBase.setUpdatedAt( new Date() );
            initDataBase.setUpdatedBy( Constantes.USER_SYSTEM );
            return applicationPropertyService.save( initDataBase, Constantes.USER_SYSTEM );
        } catch ( ServiceException e ) {
            e.printStackTrace();
        }
        return null;
    }


    private void initSecurity()
        throws ServiceException {

        /*** ROLES ***/
        Role rootRole = roleService.findByName( Constantes.ROLE_ROOT );
        Role userRole = roleService.findByName( Constantes.ROLE_USER );
        Role adminRole = roleService.findByName( Constantes.ROLE_ADMIN );
        Role funcionarioRole = roleService.findByName( Constantes.ROLE_FUNCIONARIO );

        if ( rootRole == null ) {
            log.debug( "######### salvando role_root" );
            rootRole = new Role();
            rootRole.setName( Constantes.ROLE_ROOT );
            rootRole.setNomeExibir( "Master" );
            rootRole.setAtivo( true );
            rootRole = roleService.save( rootRole, "system" );
        }
        if ( userRole == null ) {
            log.debug( "######### salvando role_user" );
            userRole = new Role();
            userRole.setNomeExibir( "Usuário" );
            userRole.setName( Constantes.ROLE_USER );
            userRole.setAtivo( true );
            userRole = roleService.save( userRole, "system" );
        }

        if ( adminRole == null ) {
            log.debug( "######### salvando adminRole" );
            adminRole = new Role();
            adminRole.setName( Constantes.ROLE_ADMIN );
            adminRole.setNomeExibir( "Administrador" );
            adminRole.setAtivo( true );
            adminRole = roleService.save( adminRole, "system" );
        }

        if ( funcionarioRole == null ) {
            log.debug( "######### salvando adminFuncionario" );
            funcionarioRole = new Role();
            funcionarioRole.setName( Constantes.ROLE_FUNCIONARIO );
            funcionarioRole.setNomeExibir( "Funcionário" );
            funcionarioRole.setAtivo( true );
            funcionarioRole = roleService.save( funcionarioRole, "system" );
        }

        // roleService.flush();

        /*** FIM ROLES ***/

        // if (userService.findByName(Constantes.USER_ROOT, null) == null) {
        // log.debug("######### salvando user");
        // User user = new User(Constantes.USER_USER, Constantes.USER_USER);
        // user.setNomeExibir("Usuário Teste");
        // user.addRole(userRole);
        // user.setAtivo(true);
        // user.setTipoUser(Constantes.TIPO_USER_FUNCIONARIO);
        // log.debug("salvando usuario user:" + user);
        // this.userService.save(user, "root");
        // }

        User uRoot = userService.findByName( Constantes.USER_ROOT, null );
        if ( uRoot == null ) {
            log.debug( "######### salvando root" );
            User rootUser = new User( Constantes.USER_ROOT, "root" );
            rootUser.setNomeExibir( "Usuário Administrador" );
            rootUser.addRole( rootRole );
            rootUser.addRole( userRole );
            rootUser.addRole( adminRole );
            rootUser.setAtivo( true );
            rootUser.setTipoUser( Constantes.TIPO_USER_ADMIN );
            log.debug( "salvando usuario root:" + rootUser );
            this.userService.save( rootUser, Constantes.USER_SYSTEM );
        } else {
            uRoot.setPassword( passwordEncoder.encode( "root" ) );
            this.userService.update( uRoot, Constantes.USER_SYSTEM );
        }
    }


    private void initEstadoCivil()
        throws ServiceException {

        List< EstadoCivil > entities = new ArrayList< EstadoCivil >();
        entities.add( new EstadoCivil( "Solteiro(a)", true ) );
        entities.add( new EstadoCivil( "Casado(a)", true ) );
        entities.add( new EstadoCivil( "Divorciado(a)", true ) );
        entities.add( new EstadoCivil( "Viúvo(a)", true ) );
        entities.add( new EstadoCivil( "Amasiado", true ) );

        Map< String, Object > attr = new HashMap< String, Object >();
        for ( EstadoCivil entity : entities ) {
            attr.put( "nome", entity.getNome() );
            List< EstadoCivil > busca = estadoCivilService.findByAttr( attr );
            if ( CollectionUtils.isEmpty( busca ) ) {
                estadoCivilService.save( entity, "root" );
            }
        }
    }


    private void initTipoEnderecoEletronico()
        throws ServiceException {

        List< TipoEnderecoEletronico > entities = new ArrayList< TipoEnderecoEletronico >();
        entities.add( new TipoEnderecoEletronico( "E-mail", true ) );
        entities.add( new TipoEnderecoEletronico( "Site Pessoal", true ) );
        entities.add( new TipoEnderecoEletronico( "Site Comercial", true ) );
        entities.add( new TipoEnderecoEletronico( "Facebook", true ) );
        entities.add( new TipoEnderecoEletronico( "Twitter", true ) );
        entities.add( new TipoEnderecoEletronico( "Instagram", true ) );
        entities.add( new TipoEnderecoEletronico( "YouTube", true ) );
        entities.add( new TipoEnderecoEletronico( "Qzone", true ) );
        entities.add( new TipoEnderecoEletronico( "WhatsApp", true ) );
        entities.add( new TipoEnderecoEletronico( "Google+", true ) );
        entities.add( new TipoEnderecoEletronico( "Tumblr", true ) );
        entities.add( new TipoEnderecoEletronico( "WeChat", true ) );
        entities.add( new TipoEnderecoEletronico( "Line", true ) );

        Map< String, Object > attr = new HashMap< String, Object >();
        for ( TipoEnderecoEletronico entity : entities ) {
            attr.put( "tipo", entity.getTipo() );
            List< TipoEnderecoEletronico > busca = tipoEnderecoEletronicoService.findByAttr( attr );
            if ( CollectionUtils.isEmpty( busca ) ) {
                tipoEnderecoEletronicoService.save( entity, "root" );
            }
        }
    }


    private void initTipoEndereco()
        throws ServiceException {

        List< TipoEndereco > entities = new ArrayList< TipoEndereco >();
        entities.add( new TipoEndereco( "Residencial", true ) );
        entities.add( new TipoEndereco( "Comercial", true ) );
        entities.add( new TipoEndereco( "Entrega", true ) );

        Map< String, Object > attr = new HashMap< String, Object >();
        for ( TipoEndereco entity : entities ) {
            attr.put( "tipo", entity.getTipo() );
            List< TipoEndereco > busca = tipoEnderecoService.findByAttr( attr );
            if ( CollectionUtils.isEmpty( busca ) ) {
                tipoEnderecoService.save( entity, "root" );
            }
        }
    }


    private void initTipoTelefone()
        throws ServiceException {

        List< TipoTelefone > entities = new ArrayList< TipoTelefone >();
        entities.add( new TipoTelefone( "Residencial", true ) );
        entities.add( new TipoTelefone( "Comercial", true ) );
        entities.add( new TipoTelefone( "Pessoal", true ) );

        Map< String, Object > attr = new HashMap< String, Object >();
        for ( TipoTelefone entity : entities ) {
            attr.put( "tipo", entity.getTipo() );
            List< TipoTelefone > busca = tipoTelefoneService.findByAttr( attr );
            if ( CollectionUtils.isEmpty( busca ) ) {
                tipoTelefoneService.save( entity, "root" );
            }
        }
    }


    private void initCargo()
        throws ServiceException {

        List< GrupoOcupacional > gruposOcupacionais = new ArrayList< GrupoOcupacional >();
        GrupoOcupacional gerentes = new GrupoOcupacional( "Gerentes", true );
        gruposOcupacionais.add( gerentes );
        gruposOcupacionais.add( new GrupoOcupacional( "Líderes", true ) );
        gruposOcupacionais.add( new GrupoOcupacional( "Operacional", true ) );
        gruposOcupacionais.add( new GrupoOcupacional( "Outros", true ) );

        Map< String, Object > attr = new HashMap< String, Object >();
        for ( GrupoOcupacional entity : gruposOcupacionais ) {
            attr.put( "nome", entity.getNome() );
            List< GrupoOcupacional > busca = grupoOcupacionalService.findByAttr( attr );
            if ( CollectionUtils.isEmpty( busca ) ) {
                grupoOcupacionalService.save( entity, "root" );
            }
        }

        attr = new HashMap< String, Object >();
        attr.put( "nome", gerentes.getNome() );
        List< GrupoOcupacional > gerentesList = grupoOcupacionalService.findByAttr( attr );
        if ( gerentesList != null ) {
            gerentes = gerentesList.get( 0 );
        }

        Cargo gerente = new Cargo( "Gerente", true );
        gerente.setGrupoOcupacional( gerentes );
        cargoService.save( gerente, Constantes.USER_SYSTEM );

        Setor administracao = new Setor( "Administração", true );
        setorService.save( administracao, Constantes.USER_SYSTEM );
    }

}
