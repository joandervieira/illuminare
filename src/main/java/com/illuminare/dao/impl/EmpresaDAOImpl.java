package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.EmpresaDAO;
import com.illuminare.entity.Empresa;


@Component
public class EmpresaDAOImpl extends GenericDAOImpl< Empresa > implements EmpresaDAO {

    public EmpresaDAOImpl() {

        super( Empresa.class );
    }

}
