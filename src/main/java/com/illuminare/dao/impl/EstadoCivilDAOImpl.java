package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.EstadoCivilDAO;
import com.illuminare.entity.EstadoCivil;


@Component
public class EstadoCivilDAOImpl extends GenericDAOImpl< EstadoCivil > implements EstadoCivilDAO {

    public EstadoCivilDAOImpl() {

        super( EstadoCivil.class );
    }

}
