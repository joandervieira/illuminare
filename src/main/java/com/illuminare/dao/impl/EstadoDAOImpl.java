package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.EstadoDAO;
import com.illuminare.entity.Estado;


@Component
public class EstadoDAOImpl extends GenericDAOImpl< Estado > implements EstadoDAO {

    public EstadoDAOImpl() {

        super( Estado.class );
    }

}
