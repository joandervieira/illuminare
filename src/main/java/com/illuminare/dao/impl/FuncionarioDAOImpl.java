package com.illuminare.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.illuminare.dao.FuncionarioDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Setor;


@Component
public class FuncionarioDAOImpl extends GenericDAOImpl< Funcionario > implements FuncionarioDAO {

    public FuncionarioDAOImpl() {

        super( Funcionario.class );
    }


    @Override
    public Funcionario findByCpf( Long idEmpresa, String cpf, List< String > fieldsToFetch ) {

        if ( StringUtils.isNotBlank( cpf ) ) {

            String jpql = "select e from Funcionario e left join fetch e.pessoa pessoa ";

            if ( fieldsToFetch != null ) {
                for ( String field : fieldsToFetch ) {
                    if ( field.contains( "pessoa" ) && !field.contains( "[" ) && !field.contains( "]" ) ) {
                        continue;
                    }
                    String leftJoinFech = " left join fetch ";
                    if ( !field.contains( "[" ) && !field.contains( "]" ) ) {
                        leftJoinFech += " e.";
                    }
                    field = field.replace( "[", "" ).replace( "]", "" );
                    jpql += leftJoinFech + field;
                }
            }

            jpql += " where pessoa.documento = :cpf ";
            if ( idEmpresa != null ) {
                jpql += " and e.idEmpresa = :idEmpresa ";
            }

            Map< String, Object > attrs = new HashMap<>();
            attrs.put( "cpf", cpf );
            if ( idEmpresa != null ) {
                attrs.put( "idEmpresa", idEmpresa );
            }
            List< Funcionario > funcionarios = findByJpql( jpql, attrs );
            if ( CollectionUtils.isNotEmpty( funcionarios ) ) {
                return funcionarios.get( 0 );
            }
        }
        return null;
    }


    @Override
    public List< Funcionario > findByGrupoOcupacional( GrupoOcupacional grupoOcupacional ) {

        Assert.notNull( grupoOcupacional );
        Assert.notNull( grupoOcupacional.getId() );

        String jpql =
            "select e from Funcionario e left join fetch e.cargo c left join fetch c.grupoOcupacional g where g.id = :idGrupoOcupacional and e.ativo = true and (e.excluido = false or e.excluido = null) ";
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idGrupoOcupacional", grupoOcupacional.getId() );
        return findByJpql( jpql, attrs );
    }


    @Override
    public List< Funcionario > findByCargo( Cargo cargo ) {

        Assert.notNull( cargo );
        Assert.notNull( cargo.getId() );

        String jpql = "select e from Funcionario e where e.cargo = :cargo and e.ativo = true and (e.excluido = false or e.excluido = null) ";
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "cargo", cargo );
        return findByJpql( jpql, attrs );
    }


    @Override
    public List< Funcionario > findBySetor( Setor setor ) {

        Assert.notNull( setor );
        Assert.notNull( setor.getId() );

        String jpql = "select e from Funcionario e where e.setor = :setor and e.ativo = true and (e.excluido = false or e.excluido = null) ";
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "setor", setor );
        return findByJpql( jpql, attrs );
    }


    @Override
    public List< Funcionario > findByEmpresa( Empresa empresa ) {

        Assert.notNull( empresa );
        Assert.notNull( empresa.getId() );

        String jpql = "select e from Funcionario e where e.idEmpresa = :idEmpresa and e.ativo = true and (e.excluido = false or e.excluido = null) ";
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idEmpresa", empresa.getId() );
        return findByJpql( jpql, attrs );
    }

    /*
     * @Override public List<Funcionario>
     * findByGrupoOcupacional(Set<GrupoOcupacional> gruposOcupacionais) { if
     * (CollectionUtils.isNotEmpty(gruposOcupacionais)) {
     * 
     * Set<Long> ids = new HashSet<>(); for (GrupoOcupacional go :
     * gruposOcupacionais) { ids.add(go.getId()); }
     * 
     * String jpql =
     * "select distinct e from Funcionario e left join fetch e.cargo c left join fetch c.grupoOcupacional g where g.id in (:idsGruposOcupacionais)"
     * ; Map<String, Object> attrs = new HashMap<>();
     * attrs.put("idsGruposOcupacionais", ids); return findByJpql(jpql, attrs); }
     * return null; }
     */

}
