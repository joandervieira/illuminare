package com.illuminare.dao.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.GenericEntity;
import com.illuminare.exceptions.DAOException;
import com.illuminare.view.Paginator;


@Repository
public abstract class GenericDAOImpl< T > implements GenericDAO< T > {

    Logger log = Logger.getLogger( GenericDAOImpl.class );

    private Class< T > persistentClass;

    @PersistenceContext
    private EntityManager entityManager;


    public GenericDAOImpl( Class< T > persistentClass ) {

        super();
        this.persistentClass = persistentClass;
    }


    public EntityManager getEntityManager() {

        return this.entityManager;
    }


    public Class< T > getPersistentClass() {

        return persistentClass;
    }


    @Override
    @Transactional
    public void flush() {

        this.getEntityManager().flush();
    }


    @Override
    public void detach( T entity ) {

        this.getEntityManager().detach( entity );
        getSession().evict( entity );
    }


    @Override
    public Session getSession() {

        Session session = entityManager.unwrap( Session.class );
        return session;
    }


    @Override
    public int executeNativeUpdate( String sql )
        throws DAOException {

        try {
            log.debug( "################ EXECUTANDO NATIVE UPDATE ################" );
            Assert.notNull( sql );
            log.debug( sql );
            sql = sql.replace( ";", "" );
            return this.getEntityManager().createNativeQuery( sql ).executeUpdate();
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }


    @Override
    public T save( T entity ) {

        log.debug( "################ SALVANDO ################" );
        log.debug( entity );
        return this.getEntityManager().merge( entity );
    }


    @Override
    public List< T > saveAll( List< T > entities ) {

        log.debug( "################ SALVANDO TODOS ################" );
        log.debug( entities );
        List< T > entitiesSaved = new ArrayList< T >();
        for ( T entity : entities ) {
            entitiesSaved.add( this.getEntityManager().merge( entity ) );
        }
        return entitiesSaved;
    }


    @Override
    public Set< T > saveAll( Set< T > entities ) {

        log.debug( "################ SALVANDO TODOS ################" );
        log.debug( entities );
        Set< T > entitiesSaved = new HashSet<>();
        for ( T entity : entities ) {
            entitiesSaved.add( this.getEntityManager().merge( entity ) );
        }
        return entitiesSaved;
    }


    @Override
    public List< T > updateAll( List< T > entities ) {

        log.debug( "################ ATUALIZANDO TODOS ################" );
        log.debug( entities );
        List< T > entitiesSaved = new ArrayList< T >();
        for ( T entity : entities ) {
            update( entity );
        }
        return entitiesSaved;
    }


    @Override
    public void update( T entity ) {

        log.debug( "################ ATUALIZANDO ################" );
        log.debug( entity );
        this.getEntityManager().merge( entity );
    }


    @Override
    public int deleteLogic( Long id ) {

        Query query = this.getEntityManager().createQuery( "Update " + persistentClass.getSimpleName() + " e set e.excluido = true where e.id = :id" );
        query.setParameter( "id", id );
        return query.executeUpdate();

    }


    @Override
    public void delete( T entity ) {

        this.getEntityManager().remove( entity );

    }


    @Override
    public void refresh( T entity ) {

        this.getEntityManager().refresh( entity );

    }


    @Override
    public List< T > listAll( Long idEmpresa ) {

        Paginator< T > paginator = new Paginator< T >();
        GenericEntity entity = new GenericEntity();
        entity.setIdEmpresa( idEmpresa );
        paginator.setEntity( (T) entity );
        paginator = pesquisar( paginator );
        return paginator.getResult();
    }


    @Override
    public List< T > listAllOrderBy( Long idEmpresa, String orderBy ) {

        Paginator< T > paginator = new Paginator< T >();
        paginator.setOrderBy( orderBy );
        GenericEntity entity = new GenericEntity();
        entity.setIdEmpresa( idEmpresa );
        paginator.setEntity( (T) entity );
        paginator = pesquisar( paginator );
        return paginator.getResult();
    }


    @Override
    public List< T > listAll( Long idEmpresa, Set< String > fieldsToFetch ) {

        Paginator< T > paginator = new Paginator< T >();
        paginator.setFieldsToFetch( new ArrayList<>( fieldsToFetch ) );
        GenericEntity entity = new GenericEntity();
        entity.setIdEmpresa( idEmpresa );
        paginator.setEntity( (T) entity );
        paginator = pesquisar( paginator );
        return paginator.getResult();
    }


    @Override
    public List< T > listAll( Long idEmpresa, Set< String > fieldsToFetch, String orderBy ) {

        Paginator< T > paginator = new Paginator< T >();
        paginator.setOrderBy( orderBy );
        paginator.setFieldsToFetch( new ArrayList<>( fieldsToFetch ) );
        GenericEntity entity = new GenericEntity();
        entity.setIdEmpresa( idEmpresa );
        paginator.setEntity( (T) entity );
        paginator = pesquisar( paginator );
        return paginator.getResult();
    }


    protected Long executeCountQuery( String sql ) {

        TypedQuery< Long > query = getEntityManager().createQuery( sql, Long.class );
        return (Long) query.getSingleResult();
    }


    @Override
    public T findById( Long id ) {

        try {

            return this.getEntityManager().find( this.persistentClass, id );

        } catch ( NoResultException e ) {
            return null;
        }
    }


    @Override
    public T findById( Long id, Set< String > fieldsToFetch ) {

        try {

            StringBuilder sql = new StringBuilder( "select e from " + persistentClass.getSimpleName() + " e " );

            if ( fieldsToFetch != null ) {
                for ( String field : fieldsToFetch ) {
                    String leftJoinFech = " left join fetch ";
                    if ( !field.contains( "[" ) && !field.contains( "]" ) ) {
                        leftJoinFech += " e.";
                    }
                    field = field.replace( "[", "" ).replace( "]", "" );
                    sql.append( leftJoinFech + field );
                }
            }

            sql.append( " where e.id = :id " );

            TypedQuery< T > query = getEntityManager().createQuery( sql.toString(), persistentClass );

            query.setParameter( "id", id );

            return query.getSingleResult();

        } catch ( NoResultException e ) {
            return null;
        }
    }


    @Override
    public List< T > findByIdEmpresa( Long idEmpresa ) {

        StringBuilder sql = new StringBuilder( "select e from " + persistentClass.getSimpleName() + " e " );

        sql.append( " where e.idEmpresa = :idEmpresa " );

        TypedQuery< T > query = getEntityManager().createQuery( sql.toString(), persistentClass );

        query.setParameter( "idEmpresa", idEmpresa );

        return query.getResultList();

    }


    @Override
    public T findById( Object pk ) {

        try {

            return this.getEntityManager().find( this.persistentClass, pk );

        } catch ( NoResultException e ) {
            return null;
        }
    }


    /**
     * Transforms a hibernate proxy to the entity's class
     * 
     * @param entity
     * @return entity
     */
    @SuppressWarnings( "unchecked" )
    // public static < T > T unProxy( T entity ) {
    //
    // if ( entity instanceof HibernateProxy ) {
    // entity = (T) ( (HibernateProxy) entity
    // ).getHibernateLazyInitializer().getImplementation();
    // }
    //
    // return entity;
    // }

    @Override
    public < T > T unproxyInitialize( T entity ) {

        if ( entity != null ) {

            Hibernate.initialize( entity );
            if ( entity instanceof HibernateProxy ) {
                entity = (T) ( (HibernateProxy) entity ).getHibernateLazyInitializer().getImplementation();
            }
            return entity;
        }

        return null;
    }


    @Override
    public List< T > findByAttr( Map< String, Object > attrs ) {

        // try {
        // StringBuilder sql = new StringBuilder();
        // sql.append("select e from " + persistentClass.getSimpleName() + " e
        // where 1 = 1 ");
        // for (Map.Entry<String, Object> attr : attrs.entrySet()) {
        // sql.append(" and ");
        // sql.append(" e." + attr.getKey() + " = :" +
        // attr.getKey().replace(".", "_"));
        // }
        //
        // TypedQuery<T> query = getEntityManager().createQuery(sql.toString(),
        // persistentClass);
        //
        // for (Map.Entry<String, Object> attr : attrs.entrySet()) {
        // query.setParameter(attr.getKey().replace(".", "_"), attr.getValue());
        // }
        //
        // return query.getResultList();
        //
        // } catch (NoResultException e) {
        // return null;
        // }

        return findByAttr( attrs, null );

    }


    @Override
    public List< T > findByAttr( Map< String, Object > attrs, List< String > fieldsToFetch ) {

        try {
            StringBuilder sql = new StringBuilder();

            sql.append( "select e from " + persistentClass.getSimpleName() + " e " );

            if ( fieldsToFetch != null ) {
                for ( String field : fieldsToFetch ) {
                    String leftJoinFech = " left join fetch ";
                    if ( !field.contains( "[" ) && !field.contains( "]" ) ) {
                        leftJoinFech += " e.";
                    }
                    field = field.replace( "[", "" ).replace( "]", "" );
                    sql.append( leftJoinFech + field );
                }
            }

            sql.append( " where 1 = 1 " );

            if ( attrs != null ) {
                for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                    sql.append( " and " );
                    sql.append( " e." + attr.getKey() + " = :" + attr.getKey().replace( ".", "_" ) );
                }
            }
            sql.append( " and (e.excluido = false or e.excluido = null) " );

            TypedQuery< T > query = getEntityManager().createQuery( sql.toString(), persistentClass );

            if ( attrs != null ) {
                for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                    query.setParameter( attr.getKey().replace( ".", "_" ), attr.getValue() );
                }
            }

            return query.getResultList();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @Override
    public List< T > findByJpql( String jpql, Map< String, Object > attrs ) {

        try {
            if ( StringUtils.isBlank( jpql ) ) {
                return null;
            }

            TypedQuery< T > query = getEntityManager().createQuery( jpql, persistentClass );

            if ( attrs != null ) {
                for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                    query.setParameter( attr.getKey(), attr.getValue() );
                }
            }

            return query.getResultList();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @Override
    public List< T > findByJpql( String jpql, Map< String, Object > attrs, Integer firstResult, Integer maxResult ) {

        try {
            if ( StringUtils.isBlank( jpql ) ) {
                return null;
            }

            TypedQuery< T > query = getEntityManager().createQuery( jpql, persistentClass );

            for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                query.setParameter( attr.getKey(), attr.getValue() );
            }

            if ( firstResult != null && maxResult != null ) {
                query.setFirstResult( firstResult );
                query.setMaxResults( maxResult );
            }

            return query.getResultList();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @SuppressWarnings( { "unchecked", "rawtypes" } )
    @Override
    public List findByJpql( String jpql, Map< String, Object > attrs, Class clazz ) {

        try {
            if ( StringUtils.isBlank( jpql ) ) {
                return null;
            }

            TypedQuery query = getEntityManager().createQuery( jpql, clazz );

            if ( attrs != null ) {
                for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                    query.setParameter( attr.getKey(), attr.getValue() );
                }
            }

            return query.getResultList();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @SuppressWarnings( { "unchecked", "rawtypes" } )
    @Override
    public Object findByJpqlSingle( String jpql, Map< String, Object > attrs, Class clazz ) {

        try {
            if ( StringUtils.isBlank( jpql ) ) {
                return null;
            }

            TypedQuery query = getEntityManager().createQuery( jpql, clazz );

            if ( attrs != null ) {
                for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                    query.setParameter( attr.getKey(), attr.getValue() );
                }
            }

            return query.getSingleResult();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @Override
    public int updateByJpql( String jpql, Map< String, Object > attrs ) {

        if ( StringUtils.isBlank( jpql ) ) {

            Query query = getEntityManager().createQuery( jpql );

            if ( attrs != null ) {
                for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                    query.setParameter( attr.getKey(), attr.getValue() );
                }
            }

            return query.executeUpdate();

        }

        return 0;

    }


    protected String generateParamKey( String param, Object value, boolean withPrefix ) {

        String paramSafe = "";
        if ( param != null ) {
            if ( withPrefix ) {
                paramSafe = " :";
            }
            paramSafe += param.replace( ".", "_" ).replace( "[date>=]", "" ).replace( "[date<=]", "" ).replace( "[", "" ).replace( "]", "" );
            if ( value != null ) {
                paramSafe = paramSafe + System.identityHashCode( value );
            }
        }
        return paramSafe;
    }


    protected String generetedOrderBy( String orderBy ) {

        String orderByGenereted = "";

        if ( StringUtils.isNotBlank( orderBy ) ) {
            String fieldOrder = orderBy.substring( 1 );
            String direction = orderBy.substring( 0, 1 );
            if ( direction.equals( "+" ) ) {
                direction = " asc ";
            } else {
                direction = " desc ";
            }
            String orderByClause = " order by ";
            if ( fieldOrder.contains( "[" ) && fieldOrder.contains( "]" ) ) {
                fieldOrder = fieldOrder.replace( "[", "" ).replace( "]", "" );
            } else {
                orderByClause += " e.";
            }
            orderByGenereted += orderByClause + fieldOrder + direction;
        }

        return orderByGenereted;
    }


    @Override
    public Paginator< T > pesquisar( Paginator< T > paginator ) {

        if ( paginator == null ) {
            paginator = new Paginator< T >();
        }

        StringBuilder sqlSelect = new StringBuilder( "select distinct e " );
        StringBuilder sqlSelectIdsClause = new StringBuilder( "select distinct e.id " );
        StringBuilder sqlSelectTotalClause = new StringBuilder( "select count(distinct e.id) " );
        StringBuilder sqlFrom = new StringBuilder( "from " + persistentClass.getSimpleName() + " e " );
        StringBuilder sqlFetches = new StringBuilder( 200 );
        StringBuilder sqlWhere = new StringBuilder( 200 );
        StringBuilder sqlOrderBy = new StringBuilder( 40 );

        GenericEntity entity = (GenericEntity) paginator.getEntity();

        boolean isPaginable = false;

        if ( paginator.getPageNumber() != null && paginator.getPageSize() != null ) {
            isPaginable = true;
        }

        // JOINS FETCH
        if ( paginator.getFieldsToFetch() != null ) {
            for ( String field : paginator.getFieldsToFetch() ) {
                String leftJoinFech = " left join fetch ";
                if ( !field.contains( "[" ) && !field.contains( "]" ) ) {
                    leftJoinFech += " e.";
                }
                field = field.replace( "[", "" ).replace( "]", "" );
                sqlFetches.append( leftJoinFech + field );
            }
        }
        sqlFetches.append( " " );

        if ( paginator.getFieldsToSearch() == null ) {
            paginator.setFieldsToSearch( new HashMap< String, Object >() );
        }
        if ( entity != null && entity.getIdEmpresa() != null ) {
            // sqlWhere.append( " and e.idEmpresa = " + entity.getIdEmpresa() );
            paginator.getFieldsToSearch().put( "idEmpresa", entity.getIdEmpresa() );
        }

        // WHERE
        sqlWhere.append( " where 1 = 1 " );
        for ( Map.Entry< String, Object > attr : paginator.getFieldsToSearch().entrySet() ) {
            sqlWhere.append( " and " );

            String column = "", condition = "", paramKey = "", originalKey = "";

            condition = " = ";
            originalKey = attr.getKey();
            // paramKey = " :" + originalKey.replace(".",
            // "_").replace("[date>=]", "").replace("[date<=]", "").replace("[",
            // "").replace("]", "");
            // paramKey = paramKey + System.identityHashCode(attr.getValue());
            paramKey = generateParamKey( originalKey, attr.getValue(), true );

            if ( originalKey.contains( "[date>=]" ) ) {
                condition = " >= ";
                originalKey = originalKey.replace( "[date>=]", "" );

            } else if ( originalKey.contains( "[date<=]" ) ) {
                condition = " <= ";
                originalKey = originalKey.replace( "[date<=]", "" );
            }

            if ( originalKey.contains( " in " ) ) {
                condition = "";
                paramKey = paramKey.replace( "in", "" ).replace( " ", "" );
                paramKey = "(" + paramKey + ")";
            }

            if ( originalKey.contains( "[" ) && originalKey.contains( "]" ) ) {
                column = originalKey;
                column = column.replace( "[", "" ).replace( "]", "" );
            } else {
                column = "e." + originalKey;
            }

            if ( attr.getValue() instanceof String ) {
                column = "upper(";

                if ( originalKey.contains( "[" ) && originalKey.contains( "]" ) ) {
                    column += originalKey;
                    column = column.replace( "[", "" ).replace( "]", "" );
                } else {
                    column += "e." + originalKey;
                }

                // column = "upper(e." + originalKey + ") ";
                column += ") ";
                condition = " like ";
            }

            sqlWhere.append( column + condition + paramKey );
        }

        sqlWhere.append( " and (e.excluido = false or e.excluido = null) " );

        if ( StringUtils.isBlank( paginator.getOrderBy() ) ) {
            sqlOrderBy.append( " order by e.dataCadastro desc " );
        } else {
            String fieldOrder = paginator.getOrderBy().substring( 1 );
            String direction = paginator.getOrderBy().substring( 0, 1 );
            if ( direction.equals( "+" ) ) {
                direction = " asc ";
            } else {
                direction = " desc ";
            }
            String orderByClause = " order by ";
            if ( fieldOrder.contains( "[" ) && fieldOrder.contains( "]" ) ) {
                fieldOrder = fieldOrder.replace( "[", "" ).replace( "]", "" );
            } else {
                orderByClause += " e.";
            }
            sqlOrderBy.append( orderByClause + fieldOrder + direction );
        }

        // String finalSql =
        // sqlSelectIdsClause.append(sqlFrom).append(sqlWhere).append(sqlOrderBy).toString();
        String finalSql =
            sqlSelectIdsClause.append( sqlFrom ).append( sqlFetches.toString().replace( "fetch", "" ) ).append( sqlWhere ).append( sqlOrderBy ).toString();
        String finalSqlTotal = "";
        if ( isPaginable ) {
            finalSqlTotal = sqlSelectTotalClause.append( sqlFrom ).append( sqlFetches.toString().replace( "fetch", "" ) ).append( sqlWhere )
                .append( sqlOrderBy ).toString();
        }
        TypedQuery< Long > queryIds = getEntityManager().createQuery( finalSql, Long.class );
        TypedQuery< Long > queryTotal = null;
        if ( isPaginable ) {
            queryTotal = getEntityManager().createQuery( finalSqlTotal, Long.class );
        }

        if ( paginator.getPageNumber() != null && paginator.getPageSize() != null ) {
            queryIds.setFirstResult( Long.valueOf( ( paginator.getPageNumber() - 1 ) * paginator.getPageSize() ).intValue() );
            queryIds.setMaxResults( paginator.getPageSize().intValue() );
        }

        for ( Map.Entry< String, Object > attr : paginator.getFieldsToSearch().entrySet() ) {
            Object value = attr.getValue();

            // String paramKey = attr.getKey().replace(".",
            // "_").replace("[date>=]", "").replace("[date<=]", "").replace("[",
            // "").replace("]",
            // "").replace("in", "").replace(" ", "");
            // paramKey = paramKey + System.identityHashCode(value);
            String paramKey = generateParamKey( attr.getKey(), attr.getValue(), false );

            if ( value instanceof String ) {
                value = "%" + ( (String) value ).toUpperCase() + "%";
            }

            if ( value instanceof Date ) {
                queryIds.setParameter( paramKey, (Date) value, TemporalType.DATE );
                if ( isPaginable ) {
                    queryTotal.setParameter( paramKey, (Date) value, TemporalType.DATE );
                }
            } else {
                paramKey = paramKey.replace( "in", "" ).replace( " ", "" );
                queryIds.setParameter( paramKey, value );
                if ( isPaginable ) {
                    queryTotal.setParameter( paramKey, value );
                }
            }

        }

        List< Long > idsResult = queryIds.getResultList();
        Long totalResult = null;
        try {
            if ( isPaginable ) {
                totalResult = queryTotal.getSingleResult();
            } else {
                totalResult = 0L;
            }
        } catch ( NoResultException e ) {
            totalResult = 0L;
        }

        if ( paginator.getPageSize() != null && idsResult != null && idsResult.size() < paginator.getPageSize() ) {
            paginator.setHasNext( false );
        } else {
            paginator.setHasNext( true );
        }

        paginator.setTotal( totalResult );

        if ( CollectionUtils.isNotEmpty( idsResult ) ) {
            finalSql = sqlSelect.append( sqlFrom ).append( sqlFetches ).append( " where e.id in (:idsResult) " ).append( sqlOrderBy ).toString();
            TypedQuery< T > query = getEntityManager().createQuery( finalSql, persistentClass );
            query.setParameter( "idsResult", idsResult );
            paginator.setResult( query.getResultList() );
        } else {
            paginator.setResult( new ArrayList< T >() );
        }

        return paginator;

    }

}