package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.GrupoOcupacionalDAO;
import com.illuminare.entity.GrupoOcupacional;


@Component
public class GrupoOcupacionalDAOImpl extends GenericDAOImpl< GrupoOcupacional > implements GrupoOcupacionalDAO {

    public GrupoOcupacionalDAOImpl() {

        super( GrupoOcupacional.class );
    }

}
