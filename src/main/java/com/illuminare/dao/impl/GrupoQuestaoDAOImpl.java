package com.illuminare.dao.impl;


import java.math.BigInteger;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.illuminare.dao.GrupoQuestaoDAO;
import com.illuminare.entity.GrupoQuestao;


@Component
public class GrupoQuestaoDAOImpl extends GenericDAOImpl< GrupoQuestao > implements GrupoQuestaoDAO {

    public GrupoQuestaoDAOImpl() {

        super( GrupoQuestao.class );
    }


    @Override
    public Long totalQuestoesDoGrupo( Long idGrupo ) {

        String hql = "select COUNT(*) as total from questao_ordem WHERE id_grupo_questao = :idGrupo";

        Query query = getEntityManager().createNativeQuery( hql );

        query.setParameter( "idGrupo", idGrupo );

        return (Long) ( (BigInteger) query.getSingleResult() ).longValue();

    }

}
