package com.illuminare.dao.impl;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class JdbcUtil {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    static final String DB_URL = "jdbc:mysql://localhost/illuminare";

    // Database credentials
    static final String USER = "root";

    static final String PASS = "root";


    public int executeUpdate( String sql ) {

        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();

            return stmt.executeUpdate( sql );

        } catch ( SQLException se ) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch ( Exception e ) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if ( stmt != null )
                    conn.close();
            } catch ( SQLException se ) {
            } // do nothing
            try {
                if ( conn != null )
                    conn.close();
            } catch ( SQLException se ) {
                se.printStackTrace();
            } // end finally try
        } // end try

        return 0;
    }


    private Connection getConnection()
        throws ClassNotFoundException,
        SQLException {

        // STEP 2: Register JDBC driver
        Class.forName( "com.mysql.jdbc.Driver" );

        // STEP 3: Open a connection
        System.out.println( "Connecting to a selected database..." );
        return DriverManager.getConnection( DB_URL, USER, PASS );

    }
}
