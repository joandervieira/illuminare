package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.ListaValoresDAO;
import com.illuminare.entity.ListaValores;


@Component
public class ListaValoresDAOImpl extends GenericDAOImpl< ListaValores > implements ListaValoresDAO {

    public ListaValoresDAOImpl() {

        super( ListaValores.class );
    }

}
