package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.MetaDAO;
import com.illuminare.entity.Meta;


@Component
public class MetaDAOImpl extends GenericDAOImpl< Meta > implements MetaDAO {

    public MetaDAOImpl() {

        super( Meta.class );
    }

}
