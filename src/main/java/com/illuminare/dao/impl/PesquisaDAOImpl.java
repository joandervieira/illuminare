package com.illuminare.dao.impl;


import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.illuminare.dao.PesquisaDAO;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.util.DateUtil;


@Component
public class PesquisaDAOImpl extends GenericDAOImpl< Pesquisa > implements PesquisaDAO {

    public PesquisaDAOImpl() {

        super( Pesquisa.class );
    }


    @Override
    public List< Pesquisa > findByQuestionario( List< Questionario > questionarios ) {

        if ( CollectionUtils.isEmpty( questionarios ) ) {
            return null;
        }
        String jpql =
            "select e from Pesquisa e left join fetch e.questionarios quest where quest.id in (:questionarios) and (e.excluido = false or e.excluido = null)";
        Map< String, Object > attrs = new HashMap<>();
        Set< Long > ids = new HashSet<>();
        for ( Questionario q : questionarios ) {
            if ( q.getId() != null ) {
                ids.add( q.getId() );
            }
        }
        attrs.put( "questionarios", ids );
        return findByJpql( jpql, attrs );
    }


    @Override
    public List< Pesquisa > findPesquisasEmAberto( Long idEmpresa ) {

        // String jpql =
        // "select distinct e from Pesquisa e left join fetch e.questionarios
        // quest where e.dtaInicio <= :hoje and e.dtaFim >= :hoje and
        // (e.excluido = false or e.excluido = null)";
        // Map<String, Object> attrs = new HashMap<>();
        // attrs.put("hoje", DateUtil.getStartOfToday());
        // return findByJpql(jpql, attrs);
        return findPesquisasEmAberto( idEmpresa, Arrays.asList( "questionarios" ) );
    }


    @Override
    public List< Pesquisa > findPesquisasEmAberto( Long idEmpresa, List< String > fieldsToFetch ) {

        String jpql = "select distinct e from Pesquisa e ";

        if ( fieldsToFetch != null ) {
            for ( String field : fieldsToFetch ) {
                String leftJoinFech = " left join fetch ";
                if ( !field.contains( "[" ) && !field.contains( "]" ) ) {
                    leftJoinFech += " e.";
                }
                field = field.replace( "[", "" ).replace( "]", "" );
                jpql += ( leftJoinFech + field );
            }
        }

        jpql += " where e.dtaInicio <= :hoje and e.dtaFim >= :hoje and (e.excluido = false or e.excluido = null) ";
        if ( idEmpresa != null ) {
            jpql += " and e.idEmpresa = " + idEmpresa + " ";
        }
        jpql += " order by e.nome asc ";
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "hoje", DateUtil.getStartOfToday() );
        return findByJpql( jpql, attrs );
    }


    @Override
    public List< Pesquisa > findPesquisasFechadas( Long idEmpresa ) {

        String jpql = "select distinct e from Pesquisa e where e.dtaEntrega <= :hoje and (e.excluido = false or e.excluido = null) ";
        if ( idEmpresa != null ) {
            jpql += " and e.idEmpresa = " + idEmpresa;
        }
        jpql += " order by e.dtaEntrega desc";
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "hoje", DateUtil.getStartOfToday() );
        return findByJpql( jpql, attrs );
    }

}
