package com.illuminare.dao.impl;


import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.illuminare.dao.QuestaoClassificacaoDAO;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.Questionario;


@Component
public class QuestaoClassificacaoDAOImpl extends GenericDAOImpl< QuestaoClassificacao > implements QuestaoClassificacaoDAO {

    public QuestaoClassificacaoDAOImpl() {

        super( QuestaoClassificacao.class );
    }


    @Override
    public List< QuestaoClassificacao > findByQuestionarioAndQuestao( Questionario questionario, Questao questao, GrupoQuestao grupoQuestao ) {

        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );

        try {

            StringBuilder hql = new StringBuilder();

            hql.append( "select qc from QuestaoClassificacao qc " );
            hql.append( "where qc.idQuestionario = :idQuestionario " );
            if ( questao != null && questao.getId() != null ) {
                hql.append( "and qc.idQuestao = :idQuestao " );
            }
            if ( grupoQuestao != null && grupoQuestao.getId() != null ) {
                hql.append( "and qc.idGrupoQuestao = :idGrupoQuestao " );
            }

            Query query = getEntityManager().createQuery( hql.toString() );

            query.setParameter( "idQuestionario", questionario.getId() );

            if ( questao != null && questao.getId() != null ) {
                query.setParameter( "idQuestao", questao.getId() );
            }
            if ( grupoQuestao != null && grupoQuestao.getId() != null ) {
                query.setParameter( "idGrupoQuestao", grupoQuestao.getId() );
            }

            return query.getResultList();

        } catch ( NoResultException e ) {
            return null;
        }
    }


    @Override
    public void deleteByQuestionario( Questionario questionario ) {

        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );

        Query query = getEntityManager().createQuery( "delete QuestaoClassificacao entity where entity.idQuestionario = :idQuestionario " );
        query.setParameter( "idQuestionario", questionario.getId() );
        query.executeUpdate();

    }

}
