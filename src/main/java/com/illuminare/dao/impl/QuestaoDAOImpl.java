package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.QuestaoDAO;
import com.illuminare.entity.Questao;


@Component
public class QuestaoDAOImpl extends GenericDAOImpl< Questao > implements QuestaoDAO {

    public QuestaoDAOImpl() {

        super( Questao.class );
    }

}
