package com.illuminare.dao.impl;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.illuminare.dao.QuestaoMetaDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Setor;


@Component
public class QuestaoMetaDAOImpl extends GenericDAOImpl< QuestaoMeta > implements QuestaoMetaDAO {

    public QuestaoMetaDAOImpl() {

        super( QuestaoMeta.class );
    }


    @Override
    public QuestaoMeta findByQuestaoAndPesquisa( Questao questao, GrupoQuestao grupoQuestao, Pesquisa pesquisa ) {

        Assert.notNull( questao );
        Assert.notNull( questao.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        StringBuilder jpql = new StringBuilder( 300 );

        jpql.append( "select qm from QuestaoMeta qm where 1=1 " );
        jpql.append( "and (excluido is null or excluido = false) " );
        jpql.append( "and idQuestao = :idQuestao " );
        jpql.append( "and idPesquisa = :idPesquisa " );
        jpql.append( "and idGrupoQuestao = :idGrupoQuestao " );

        Map< String, Object > attrs = new HashMap<>();

        attrs.put( "idQuestao", questao.getId() );
        attrs.put( "idPesquisa", pesquisa.getId() );
        attrs.put( "idGrupoQuestao", grupoQuestao.getId() );

        List< QuestaoMeta > result = findByJpql( jpql.toString(), attrs );

        if ( CollectionUtils.isNotEmpty( result ) ) {
            return result.get( 0 );
        }

        return null;

    }


    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndGrupoOcupacional(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        GrupoOcupacional grupoOcupacional ) {

        Assert.notNull( questao );
        Assert.notNull( questao.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( grupoOcupacional );
        Assert.notNull( grupoOcupacional.getId() );
        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );
        Assert.notNull( grupoQuestao );
        Assert.notNull( grupoQuestao.getId() );

        StringBuilder jpql = new StringBuilder( 300 );

        jpql.append( "select qm from QuestaoMeta qm where 1=1 " );
        jpql.append( "and (excluido is null or excluido = false) " );
        jpql.append( "and idQuestao = :idQuestao " );
        jpql.append( "and idPesquisa = :idPesquisa " );
        jpql.append( "and idGrupoOcupacional = :idGrupoOcupacional " );
        jpql.append( "and idQuestionario = :idQuestionario " );
        jpql.append( "and idGrupoQuestao = :idGrupoQuestao " );

        Map< String, Object > attrs = new HashMap<>();

        attrs.put( "idQuestao", questao.getId() );
        attrs.put( "idPesquisa", pesquisa.getId() );
        attrs.put( "idGrupoOcupacional", grupoOcupacional.getId() );
        attrs.put( "idQuestionario", questionario.getId() );
        attrs.put( "idGrupoQuestao", grupoQuestao.getId() );

        List< QuestaoMeta > result = findByJpql( jpql.toString(), attrs );

        if ( CollectionUtils.isNotEmpty( result ) ) {
            return result.get( 0 );
        }

        return null;

    }


    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndCargo(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        Cargo cargo ) {

        Assert.notNull( questao );
        Assert.notNull( questao.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( cargo );
        Assert.notNull( cargo.getId() );
        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );
        Assert.notNull( grupoQuestao );
        Assert.notNull( grupoQuestao.getId() );

        StringBuilder jpql = new StringBuilder( 300 );

        jpql.append( "select qm from QuestaoMeta qm where 1=1 " );
        jpql.append( "and (excluido is null or excluido = false) " );
        jpql.append( "and idQuestao = :idQuestao " );
        jpql.append( "and idPesquisa = :idPesquisa " );
        jpql.append( "and idCargo = :idCargo " );
        jpql.append( "and idQuestionario = :idQuestionario " );
        jpql.append( "and idGrupoQuestao = :idGrupoQuestao " );

        Map< String, Object > attrs = new HashMap<>();

        attrs.put( "idQuestao", questao.getId() );
        attrs.put( "idPesquisa", pesquisa.getId() );
        attrs.put( "idCargo", cargo.getId() );
        attrs.put( "idQuestionario", questionario.getId() );
        attrs.put( "idGrupoQuestao", grupoQuestao.getId() );

        List< QuestaoMeta > result = findByJpql( jpql.toString(), attrs );

        if ( CollectionUtils.isNotEmpty( result ) ) {
            return result.get( 0 );
        }

        return null;

    }


    @Override
    public List< QuestaoMeta > findByQuestionario( Long idQuestionario ) {

        StringBuilder hql = new StringBuilder( 300 );

        hql.append( " select qm from QuestaoMeta qm " );
        hql.append( " where qm.idQuestionario = :idQuestionario " );
        hql.append( "and (qm.excluido is null or qm.excluido = false) " );
        hql.append( " order by qm.id " );

        org.hibernate.Query query = getSession().createQuery( hql.toString() );
        query.setParameter( "idQuestionario", idQuestionario );

        return query.list();
    }


    @Override
    public List< QuestaoMeta > findByPesquisa( Long idPesquisa ) {

        StringBuilder hql = new StringBuilder( 300 );

        hql.append( " select qm from QuestaoMeta qm " );
        hql.append( " where qm.idPesquisa = :idPesquisa " );
        hql.append( "and (qm.excluido is null or qm.excluido = false) " );
        hql.append( " order by qm.id " );

        org.hibernate.Query query = getSession().createQuery( hql.toString() );
        query.setParameter( "idPesquisa", idPesquisa );

        return query.list();
    }


    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndSetor(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        Setor setor ) {

        Assert.notNull( questao );
        Assert.notNull( questao.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( setor );
        Assert.notNull( setor.getId() );
        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );
        Assert.notNull( grupoQuestao );
        Assert.notNull( grupoQuestao.getId() );

        StringBuilder jpql = new StringBuilder( 300 );

        jpql.append( "select qm from QuestaoMeta qm where 1=1 " );
        jpql.append( "and (excluido is null or excluido = false) " );
        jpql.append( "and idQuestao = :idQuestao " );
        jpql.append( "and idPesquisa = :idPesquisa " );
        jpql.append( "and idSetor = :idSetor " );
        jpql.append( "and idQuestionario = :idQuestionario " );
        jpql.append( "and idGrupoQuestao = :idGrupoQuestao " );

        Map< String, Object > attrs = new HashMap<>();

        attrs.put( "idQuestao", questao.getId() );
        attrs.put( "idPesquisa", pesquisa.getId() );
        attrs.put( "idSetor", setor.getId() );
        attrs.put( "idQuestionario", questionario.getId() );
        attrs.put( "idGrupoQuestao", grupoQuestao.getId() );

        List< QuestaoMeta > result = findByJpql( jpql.toString(), attrs );

        if ( CollectionUtils.isNotEmpty( result ) ) {
            return result.get( 0 );
        }

        return null;

    }


    @Override
    public void deleteByPesquisa( Pesquisa pesquisa, String user ) {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        // Query query = getEntityManager().createQuery( "delete QuestaoMeta qm where
        // qm.idPesquisa = :idPesquisa " );
        // query.setParameter( "idPesquisa", pesquisa.getId() );
        // query.executeUpdate();

        String jpql = "update QuestaoMeta e set e.excluido = true ";
        jpql += " , e.usuarioAtualizacao = :usuarioAtualizacao ";
        jpql += " , e.dataAtualizacao = :dataAtualizacao ";
        jpql += " where 1 = 1 ";
        jpql += " and e.idPesquisa = :pesquisa ";

        Query query = getEntityManager().createQuery( jpql );
        query.setParameter( "pesquisa", pesquisa.getId() );
        query.setParameter( "usuarioAtualizacao", user );
        query.setParameter( "dataAtualizacao", new Date() );
        query.executeUpdate();

    }

}
