package com.illuminare.dao.impl;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.illuminare.dao.QuestionarioDAO;
import com.illuminare.entity.Questao;
import com.illuminare.entity.Questionario;


@Component
public class QuestionarioDAOImpl extends GenericDAOImpl< Questionario > implements QuestionarioDAO {

    public QuestionarioDAOImpl() {

        super( Questionario.class );
    }


    @Override
    public List< Questionario > findByQuestao( List< Questao > questoes ) {

        if ( CollectionUtils.isEmpty( questoes ) ) {
            return null;
        }
        String jpql =
            "select e from Questionario e left join fetch e.questoes quest where quest.id in (:questoes) and (e.excluido = false or e.excluido = null)";
        Map< String, Object > attrs = new HashMap<>();
        Set< Long > ids = new HashSet<>();
        for ( Questao q : questoes ) {
            if ( q.getId() != null ) {
                ids.add( q.getId() );
            }
        }
        attrs.put( "questoes", ids );
        return findByJpql( jpql, attrs );
    }

}
