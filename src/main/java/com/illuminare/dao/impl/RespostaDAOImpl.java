package com.illuminare.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.illuminare.dao.RespostaDAO;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Resposta;
import com.illuminare.exceptions.DAOException;
import com.illuminare.service.AvaliacaoService;


@Component
public class RespostaDAOImpl extends GenericDAOImpl< Resposta > implements RespostaDAO {

    @Autowired
    AvaliacaoService avaliacaoService;


    public RespostaDAOImpl() {

        super( Resposta.class );
    }


    @Override
    public List< Resposta > findByAvaliado( Long idAvaliado ) {

        String jpql = "select e from Resposta e ";
        jpql += " left join fetch e.questionarioAvaliacao avaliacao left join fetch avaliacao.funcionarioAvaliado avaliado ";

        jpql += " where avaliado.id = :idAvaliado ";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idAvaliado", idAvaliado );

        return findByJpql( jpql, attrs );

    }

    // @Override
    // public List<Resposta> findByAvaliado(Long idAvaliado, List<Long>
    // idsPesquisas) {
    //
    // String jpql = "select e from Resposta e ";
    // jpql += " left join fetch e.questionarioAvaliacao avaliacao ";
    // jpql += " left join fetch avaliacao.pesquisa pesq ";
    // jpql += " left join fetch avaliacao.funcionarioAvaliado avaliado ";
    // jpql += " left join fetch avaliacao.funcionarioAvaliador avaliador ";
    // jpql += " left join fetch e.questao questao left join fetch
    // questao.listaValores ";
    //
    // jpql += " where avaliado.id = :idAvaliado ";
    // jpql += " and pesq.id in (:idsPesquisas) ";
    //
    // jpql += " and (e.excluido = false or e.excluido = null) ";
    //
    // Map<String, Object> attrs = new HashMap<>();
    // attrs.put("idAvaliado", idAvaliado);
    // attrs.put("idsPesquisas", idsPesquisas);
    //
    // return findByJpql(jpql, attrs);
    //
    // }

    // @Override
    // public List<Resposta> findByAvaliado(Long idAvaliado, List<Long>
    // idsPesquisas) {
    //
    // String jpql = "select e from Resposta e ";
    // jpql += " left join fetch e.questionarioAvaliacao avaliacao ";
    // jpql += " left join fetch avaliacao.pesquisa pesq ";
    // jpql += " left join fetch avaliacao.funcionarioAvaliado avaliado ";
    // jpql += " left join fetch avaliacao.funcionarioAvaliador avaliador ";
    // jpql += " left join fetch e.questao questao left join fetch
    // questao.listaValores ";
    //
    // jpql += " where avaliado.id = :idAvaliado ";
    // jpql += " and pesq.id in (:idsPesquisas) ";
    //
    // jpql += " and (e.excluido = false or e.excluido = null) ";
    //
    // Map<String, Object> attrs = new HashMap<>();
    // attrs.put("idAvaliado", idAvaliado);
    // attrs.put("idsPesquisas", idsPesquisas);
    //
    // return findByJpql(jpql, attrs);
    //
    // }


    // @Override
    // public List<Resposta> findByAvaliado(Long idAvaliado, List<Long>
    // idsPesquisas) throws ServiceException {
    //
    // //List<QuestionarioAvaliacao> avaliacoes =
    // avaliacaoService.findByAvaliado(idAvaliado);
    //
    // String jpql = "select e from Resposta e ";
    // jpql += " left join fetch e.questionarioAvaliacao avaliacao ";
    // // jpql += " left join fetch avaliacao.pesquisa pesq ";
    // // jpql += " left join fetch avaliacao.funcionarioAvaliado avaliado ";
    // // jpql += " left join fetch avaliacao.funcionarioAvaliador avaliador ";
    // // jpql += " left join fetch e.questao questao left join fetch
    // questao.listaValores ";
    //
    // jpql += " where 1 =1 ";
    // // jpql += " and avaliado.id = :idAvaliado ";
    // // jpql += " and pesq.id in (:idsPesquisas) ";
    // //jpql += " and e.questionarioAvaliacao in (:avaliacoes) ";
    // jpql += " and avaliacao.id = 151 ";
    //
    // // jpql += " and (e.excluido = false or e.excluido = null) ";
    //
    // Map<String, Object> attrs = new HashMap<>();
    // // attrs.put("idAvaliado", idAvaliado);
    // // attrs.put("idsPesquisas", idsPesquisas);
    // attrs.put("avaliacoes", 37);
    //
    // // List<Resposta> respostas = findByJpql(jpql, attrs);
    //
    // TypedQuery<Resposta> query = getEntityManager().createQuery(jpql,
    // Resposta.class);
    // //query.setParameter("avaliacoes", avaliacoes);
    //
    // List<Resposta> respostas = query.getResultList();
    //
    // return respostas;
    //
    // }

    @Override
    public List< Resposta > findByAvaliado( Long idAvaliado, List< Long > idsPesquisas ) {

        String jpql = "select e from Resposta e ";
        jpql += " left join fetch e.questionarioAvaliacao avaliacao    ";
        jpql += " left join fetch avaliacao.pesquisa pesq ";
        jpql += " left join fetch avaliacao.funcionarioAvaliado avaliado ";
        jpql += " left join fetch avaliacao.funcionarioAvaliador avaliador ";
        jpql += " left join fetch e.questao questao left join fetch questao.listaValores ";

        jpql += " where avaliado.id = :idAvaliado ";
        jpql += " and  pesq.id in (:idsPesquisas) ";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idAvaliado", idAvaliado );
        attrs.put( "idsPesquisas", idsPesquisas );

        return findByJpql( jpql, attrs );

    }


    @Override
    public List< Resposta > findByAvaliado( Funcionario funcionarioAvaliado, Pesquisa pesquisa ) {

        String jpql = "select e from Resposta e ";
        jpql += " left join fetch e.questionarioAvaliacao avaliacao    ";
        // jpql += " left join fetch avaliacao.pesquisa pesq ";
        // jpql += " left join fetch avaliacao.funcionarioAvaliado avaliado ";
        // jpql += " left join fetch avaliacao.funcionarioAvaliador avaliador ";
        // jpql += " left join fetch e.questao questao left join fetch
        // questao.listaValores ";

        jpql += " where avaliacao.funcionarioAvaliado = :funcionarioAvaliado ";
        jpql += " and  avaliacao.pesquisa = :pesquisa ";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "funcionarioAvaliado", funcionarioAvaliado );
        attrs.put( "pesquisa", pesquisa );

        return findByJpql( jpql, attrs );

    }


    @Override
    public List< Resposta > findByAvaliado( List< Funcionario > avaliados, Pesquisa pesquisa ) {

        String jpql = "select e from Resposta e ";
        jpql += " left join fetch e.questionarioAvaliacao avaliacao    ";
        jpql += " left join fetch avaliacao.pesquisa pesq ";
        jpql += " left join fetch avaliacao.funcionarioAvaliado avaliado ";
        jpql += " left join fetch avaliacao.funcionarioAvaliador avaliador ";
        jpql += " left join fetch e.questao questao left join fetch questao.listaValores ";

        jpql += " where avaliado in (:avaliados) ";
        jpql += " and  pesq = :pesquisa ";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "avaliados", avaliados );
        attrs.put( "pesquisa", pesquisa );

        return findByJpql( jpql, attrs );

    }


    @Override
    public List< Resposta > findByAvaliacao( Long idQuestionarioAvaliacao ) {

        String jpql = "select e from Resposta e ";
        jpql += " left join fetch e.questionarioAvaliacao avaliacao ";

        jpql += " where avaliacao.id = :idQuestionarioAvaliacao ";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idQuestionarioAvaliacao", idQuestionarioAvaliacao );

        return findByJpql( jpql, attrs );

    }


    @Override
    public Boolean checkIsRespnodidoByAvaliacao( Long idQuestionarioAvaliacao ) {

        String jpql = "select count(*) from Resposta e ";
        jpql += " join e.questionarioAvaliacao avaliacao ";

        jpql += " where avaliacao.id = :idQuestionarioAvaliacao ";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idQuestionarioAvaliacao", idQuestionarioAvaliacao );

        Long count = (Long) findByJpqlSingle( jpql, attrs, Long.class );
        if ( count != null && count > 0 ) {
            return true;
        }
        return false;

    }


    @Override
    public List< Resposta > findByAvaliacaoAndQuestao( Long idQuestionarioAvaliacao, Long idQuestao, Long idGrupoQuestao ) {

        String jpql = "select e from Resposta e ";
        jpql += " left join fetch e.questionarioAvaliacao avaliacao ";

        jpql += " where avaliacao.id = :idQuestionarioAvaliacao ";
        jpql += " and e.questao.id = :idQuestao";
        jpql += " and e.idGrupoQuestao = :idGrupoQuestao";

        jpql += " and (e.excluido = false or e.excluido = null) ";
        jpql += " and (avaliacao.excluido = false or avaliacao.excluido = null) ";
        jpql += " order by e.dataCadastro desc ";

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "idQuestionarioAvaliacao", idQuestionarioAvaliacao );
        attrs.put( "idQuestao", idQuestao );
        attrs.put( "idGrupoQuestao", idGrupoQuestao );

        // return (Resposta) findByJpqlSingle( jpql, attrs, Resposta.class );

        TypedQuery query = getEntityManager().createQuery( jpql, Resposta.class );

        if ( attrs != null ) {
            for ( Map.Entry< String, Object > attr : attrs.entrySet() ) {
                query.setParameter( attr.getKey(), attr.getValue() );
            }
        }

        // return query.getSingleResult();
        return query.getResultList();

    }


    @Override
    public int deleteByAvaliacao( Long idAvaliacao )
        throws DAOException {

        String sql = "delete from resposta where id_questionario_avaliacao = " + idAvaliacao;
        // Map< String, Object > attrs = new HashMap<>();
        // attrs.put( "idAvaliacao", idAvaliacao );

        // executeNativeUpdate( sql );

        SQLQuery query = getSession().createSQLQuery( sql );
        return query.executeUpdate();

    }


    @Override
    public int deleteById( Long id ) {

        String sql = "delete from resposta where id = " + id;
        SQLQuery query = getSession().createSQLQuery( sql );
        return query.executeUpdate();

    }


    @Override
    public List< Map< String, Object > > find( Long idQuestionarioAvaliacao, Long idQuestao, Long idGrupoQuestao ) {

        StringBuilder sql = new StringBuilder();
        // ;
        sql.append( " select " );
        sql.append( " id, " );
        sql.append( " id_questao, " );
        sql.append( " id_grupo_questao, " );
        sql.append( " id_questionario_avaliacao, " );
        sql.append( " data_cadastro " );
        sql.append( " from resposta " );
        sql.append( " WHERE 1=1 " );
        if ( idQuestao != null ) {
            sql.append( " and id_questao = :id_questao " );
        }
        if ( idGrupoQuestao != null ) {
            sql.append( " and id_grupo_questao = :id_grupo_questao " );
        }
        if ( idQuestionarioAvaliacao != null ) {
            sql.append( " and id_questionario_avaliacao = :id_questionario_avaliacao " );
        }
        sql.append( "and (excluido is null or excluido = false) " );
        sql.append( " order by data_cadastro desc " );

        SQLQuery query = getSession().createSQLQuery( sql.toString() );

        if ( idQuestao != null ) {
            query.setParameter( "id_questao", idQuestao );
        }
        if ( idGrupoQuestao != null ) {
            query.setParameter( "id_grupo_questao", idGrupoQuestao );
        }
        if ( idQuestionarioAvaliacao != null ) {
            query.setParameter( "id_questionario_avaliacao", idQuestionarioAvaliacao );
        }

        query.addScalar( "id", LongType.INSTANCE );
        query.addScalar( "id_questao", LongType.INSTANCE );
        query.addScalar( "id_grupo_questao", LongType.INSTANCE );
        query.addScalar( "id_questionario_avaliacao", LongType.INSTANCE );

        query.setResultTransformer( Transformers.ALIAS_TO_ENTITY_MAP );

        return query.list();
    }

}
