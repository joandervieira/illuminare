package com.illuminare.dao.impl;


import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import com.illuminare.dao.RoleDAO;
import com.illuminare.entity.Role;


@Component
public class RoleDAOImpl extends GenericDAOImpl< Role > implements RoleDAO {

    public RoleDAOImpl() {

        super( Role.class );
    }


    @Override
    // @Transactional(readOnly = true)
    public Role findByName( String name ) {

        try {
            String sql = "select e from Role e where e.name = :name " //
                + "and (e.excluido = false or e.excluido = null)";

            TypedQuery< Role > query = getEntityManager().createQuery( sql, Role.class );
            query.setParameter( "name", name );

            return query.getSingleResult();

            /*
             * String sql = "select e from Role e where e.name = :name " +
             * "and (e.excluido = false or e.excluido = null)";
             * 
             * Query query = getSession().createQuery(sql); query.setParameter("name",
             * name); return (Role) query.uniqueResult();
             */

        } catch ( NoResultException e ) {
            return null;
        }
    }

}
