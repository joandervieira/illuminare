package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.SetorDAO;
import com.illuminare.entity.Setor;


@Component
public class SetorDAOImpl extends GenericDAOImpl< Setor > implements SetorDAO {

    public SetorDAOImpl() {

        super( Setor.class );
    }

}
