package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.TipoEnderecoDAO;
import com.illuminare.entity.TipoEndereco;


@Component
public class TipoEnderecoDAOImpl extends GenericDAOImpl< TipoEndereco > implements TipoEnderecoDAO {

    public TipoEnderecoDAOImpl() {

        super( TipoEndereco.class );
    }

}
