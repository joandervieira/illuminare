package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.TipoEnderecoEletronicoDAO;
import com.illuminare.entity.TipoEnderecoEletronico;


@Component
public class TipoEnderecoEletronicoDAOImpl extends GenericDAOImpl< TipoEnderecoEletronico > implements TipoEnderecoEletronicoDAO {

    public TipoEnderecoEletronicoDAOImpl() {

        super( TipoEnderecoEletronico.class );
    }

}
