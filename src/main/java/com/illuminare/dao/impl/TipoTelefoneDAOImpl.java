package com.illuminare.dao.impl;


import org.springframework.stereotype.Component;

import com.illuminare.dao.TipoTelefoneDAO;
import com.illuminare.entity.TipoTelefone;


@Component
public class TipoTelefoneDAOImpl extends GenericDAOImpl< TipoTelefone > implements TipoTelefoneDAO {

    public TipoTelefoneDAOImpl() {

        super( TipoTelefone.class );
    }

}
