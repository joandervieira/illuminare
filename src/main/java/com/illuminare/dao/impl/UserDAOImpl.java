package com.illuminare.dao.impl;


import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.UserDAO;
import com.illuminare.entity.User;
import com.illuminare.util.Constantes;


@Component
public class UserDAOImpl extends GenericDAOImpl< User > implements UserDAO {

    Logger log = Logger.getLogger( UserDAOImpl.class );


    public UserDAOImpl() {

        super( User.class );
    }


    @Override
    @Transactional( readOnly = true )
    public UserDetails loadUserByUsername( String username )
        throws UsernameNotFoundException {

        log.debug( "####### loadUserByUsername():UserDetails" );
        User user = this.findByName( username, Arrays.asList( "roles" ) );
        if ( null == user ) {
            throw new UsernameNotFoundException( "The user with name " + username + " was not found" );
        }

        /*
         * log.debug("Usario logado:" + user); log.debug("permissoes:"); for
         * (GrantedAuthority role : user.getAuthorities()) { log.debug(role); }
         */

        return user;
    }


    @Override
    @Transactional( readOnly = true )
    public User findByName( String name, List< String > fieldsToFetch ) {

        try {
            StringBuilder sql = new StringBuilder( "select e from User e " );
            if ( fieldsToFetch != null ) {
                for ( String field : fieldsToFetch ) {
                    sql.append( " left join fetch e." + field );
                }
            }

            sql.append( " where e.name = :name and (e.excluido = false or e.excluido = null) " );

            TypedQuery< User > query = getEntityManager().createQuery( sql.toString(), User.class );
            query.setParameter( "name", name );

            return query.getSingleResult();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @Override
    public List< User > listAll( List< String > fieldsToFetch ) {

        StringBuilder sql = new StringBuilder( "select distinct e from User e " );
        if ( fieldsToFetch != null ) {
            for ( String field : fieldsToFetch ) {
                sql.append( " left join fetch e." + field );

            }
        }

        sql.append( " where  e.excluido = false or e.excluido is null order by e.dataCadastro desc " );

        TypedQuery< User > query = getEntityManager().createQuery( sql.toString(), User.class );

        return query.getResultList();

    }


    @Override
    @Transactional( readOnly = true )
    public String getSenhaUsuario( Long id ) {

        try {

            TypedQuery< String > query = getEntityManager().createQuery( "select e.password from User e where e.id = :id", String.class );
            query.setParameter( "id", id );

            return query.getSingleResult();

        } catch ( NoResultException e ) {
            return null;
        }

    }


    @Override
    @Transactional( readOnly = true )
    public Boolean usuarioJaExiste( String userName ) {

        try {

            TypedQuery< String > query =
                getEntityManager().createQuery( "select e.name from User e where e.name = :name and (e.excluido = false or e.excluido = null) ", String.class );
            query.setParameter( "name", userName );

            return query.getSingleResult() != null;

        } catch ( NoResultException e ) {
            return false;
        }

    }


    @Override
    @Transactional( readOnly = true )
    public List< User > listarAdministradores() {

        TypedQuery< User > query = getEntityManager()
            .createQuery( "select u from User u join u.roles r where r.name = :name and (u.excluido = false or u.excluido = null) ", User.class );
        query.setParameter( "name", Constantes.ROLE_ADMIN );

        return query.getResultList();

    }


    @Override
    public User findByFuncionario( Long idFuncionario ) {

        try {

            TypedQuery< User > query = getEntityManager().createQuery(
                "select e from User e left join fetch e.funcionario f where f.id = :idFuncionario and (e.excluido = false or e.excluido = null) ",
                User.class );
            query.setParameter( "idFuncionario", idFuncionario );

            return query.getSingleResult();

        } catch ( NoResultException e ) {
            return null;
        }

    }

}
