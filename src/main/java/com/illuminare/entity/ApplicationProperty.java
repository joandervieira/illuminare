package com.illuminare.entity;


import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table( name = "application_property" )
public class ApplicationProperty {

    @Id
    @GeneratedValue
    private Long id;

    @Column( name = "key_prop" )
    private String keyProp;

    @Column( name = "value", length = 4000 )
    private String value;

    @Lob
    @Basic( fetch = FetchType.LAZY )
    @Column( name = "value_lob" )
    private String valueLob;

    @Column( name = "description", length = 500 )
    private String description;

    @Column( name = "active" )
    private Boolean active;

    @Column( name = "excluido" )
    private Boolean excluido = false;

    @Column( name = "id_empresa" )
    private Long idEmpresa = 0L;

    @Column( name = "created_at" )
    @Temporal( TemporalType.TIMESTAMP )
    private Date createdAt;

    @Column( name = "updated_at" )
    @Temporal( TemporalType.TIMESTAMP )
    private Date updatedAt;

    @Column( name = "created_by" )
    private String createdBy;

    @Column( name = "updated_by" )
    private String updatedBy;


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getKeyProp() {

        return keyProp;
    }


    public void setKeyProp( String keyProp ) {

        this.keyProp = keyProp;
    }


    public String getValue() {

        return value;
    }


    public void setValue( String value ) {

        this.value = value;
    }


    public String getValueLob() {

        return valueLob;
    }


    public void setValueLob( String valueLob ) {

        this.valueLob = valueLob;
    }


    public String getDescription() {

        return description;
    }


    public void setDescription( String description ) {

        this.description = description;
    }


    public Boolean getActive() {

        return active;
    }


    public void setActive( Boolean active ) {

        this.active = active;
    }


    public Date getCreatedAt() {

        return createdAt;
    }


    public void setCreatedAt( Date createdAt ) {

        this.createdAt = createdAt;
    }


    public Date getUpdatedAt() {

        return updatedAt;
    }


    public void setUpdatedAt( Date updatedAt ) {

        this.updatedAt = updatedAt;
    }


    public String getCreatedBy() {

        return createdBy;
    }


    public void setCreatedBy( String createdBy ) {

        this.createdBy = createdBy;
    }


    public String getUpdatedBy() {

        return updatedBy;
    }


    public void setUpdatedBy( String updatedBy ) {

        this.updatedBy = updatedBy;
    }


    @Override
    public String toString() {

        return "ApplicationProperty [id=" + id + ", keyProp=" + keyProp + ", value=" + value + ", description=" + description + ", active=" + active
            + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + "]";
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }
}
