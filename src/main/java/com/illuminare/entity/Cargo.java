package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.illuminare.view.CargoView;


@Entity
@Table( name = "cargo" )
public class Cargo extends GenericEntity {

    @Column( name = "nome" )
    private String nome;

    @Column( name = "descricao_sumaria" )
    private String descricaoSumaria;

    @Column( name = "descricao_detalhada", length = 1000 )
    private String descricaoDetalhada;

    @Column( name = "carga_horaria" )
    private Long cargaHoraria;

    @Column( name = "obs", length = 1000 )
    private String obs;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_grupo_ocupacional" )
    private GrupoOcupacional grupoOcupacional;


    public Cargo() {

    }


    public Cargo( String nome ) {

        setNome( nome );
    }


    public Cargo( String nome, Boolean ativo ) {

        setNome( nome );
        setAtivo( ativo );
    }


    public Cargo( Long id ) {

        setId( id );
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDescricaoSumaria() {

        return descricaoSumaria;
    }


    public void setDescricaoSumaria( String descricaoSumaria ) {

        this.descricaoSumaria = descricaoSumaria;
    }


    public String getDescricaoDetalhada() {

        return descricaoDetalhada;
    }


    public void setDescricaoDetalhada( String descricaoDetalhada ) {

        this.descricaoDetalhada = descricaoDetalhada;
    }


    public Long getCargaHoraria() {

        return cargaHoraria;
    }


    public void setCargaHoraria( Long cargaHoraria ) {

        this.cargaHoraria = cargaHoraria;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public GrupoOcupacional getGrupoOcupacional() {

        return grupoOcupacional;
    }


    public void setGrupoOcupacional( GrupoOcupacional grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }


    @Override
    public String toString() {

        return "Cargo [nome=" + nome + ", getId()=" + getId() + "]";
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        private boolean excludeGrupoOcupacional = false;

        public CargoView format() {

            CargoView view = new CargoView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setNome( getNome() );
            view.setDescricaoSumaria( getDescricaoSumaria() );
            view.setDescricaoDetalhada( getDescricaoDetalhada() );
            view.setCargaHoraria( getCargaHoraria() );
            view.setObs( getObs() );
            if ( !excludeGrupoOcupacional ) {
                if ( getGrupoOcupacional() != null ) {
                    view.setGrupoOcupacional( getGrupoOcupacional().formatToWebPage().format() );
                }
            }
            return view;
        }

        public FormatBuilder excludeGrupoOcupacional() {

            excludeGrupoOcupacional = true;
            return this;
        }

    }

}
