package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table( name = "cidade" )
public class Cidade extends GenericEntity {

    @Column( name = "nome" )
    private String nome;

    @ManyToOne
    @JoinColumn( name = "id_estado" )
    private Estado estado;


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public Estado getEstado() {

        return estado;
    }


    public void setEstado( Estado estado ) {

        this.estado = estado;
    }


    @Override
    public String toString() {

        return "Cidade [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
