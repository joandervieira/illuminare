package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "empresa" )
public class Empresa extends GenericEntity {

    @Column( name = "nome" )
    private String nome;

    @Column( name = "cnpj" )
    private String cnpj;


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getCnpj() {

        return cnpj;
    }


    public void setCnpj( String cnpj ) {

        this.cnpj = cnpj;
    }
}
