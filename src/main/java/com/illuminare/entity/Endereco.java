package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table( name = "endereco" )
public class Endereco extends GenericEntity {

    @Column( name = "logradouro" )
    private String logradouro;

    @Column( name = "numero" )
    private String numero;

    @Column( name = "cep" )
    private String cep;

    @Column( name = "bairro" )
    private String bairro;

    @Column( name = "complemento" )
    private String complemento;

    @Column( name = "referencia" )
    private String referencia;

    @Column( name = "is_principal" )
    private Boolean isPrincipal;

    @Column( name = "obs", length = 1000 )
    private String obs;

    @ManyToOne( fetch = FetchType.EAGER )
    @JoinColumn( name = "id_cidade" )
    private Cidade cidade;

    @Transient
    private String nomeCidade;

    @Transient
    private String siglaEstado;

    @ManyToOne
    @JoinColumn( name = "id_tipo_endereco" )
    private TipoEndereco tipoEndereco;


    public String getLogradouro() {

        return logradouro;
    }


    public void setLogradouro( String logradouro ) {

        this.logradouro = logradouro;
    }


    public String getNumero() {

        return numero;
    }


    public void setNumero( String numero ) {

        this.numero = numero;
    }


    public String getCep() {

        return cep;
    }


    public void setCep( String cep ) {

        this.cep = cep;
    }


    public String getBairro() {

        return bairro;
    }


    public void setBairro( String bairro ) {

        this.bairro = bairro;
    }


    public String getComplemento() {

        return complemento;
    }


    public void setComplemento( String complemento ) {

        this.complemento = complemento;
    }


    public String getReferencia() {

        return referencia;
    }


    public void setReferencia( String referencia ) {

        this.referencia = referencia;
    }


    public Boolean getIsPrincipal() {

        return isPrincipal;
    }


    public void setIsPrincipal( Boolean isPrincipal ) {

        this.isPrincipal = isPrincipal;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public Cidade getCidade() {

        return cidade;
    }


    public void setCidade( Cidade cidade ) {

        this.cidade = cidade;
    }


    public TipoEndereco getTipoEndereco() {

        return tipoEndereco;
    }


    public void setTipoEndereco( TipoEndereco tipoEndereco ) {

        this.tipoEndereco = tipoEndereco;
    }


    public String getNomeCidade() {

        return nomeCidade;
    }


    public void setNomeCidade( String nomeCidade ) {

        this.nomeCidade = nomeCidade;
    }


    public String getSiglaEstado() {

        return siglaEstado;
    }


    public void setSiglaEstado( String siglaEstado ) {

        this.siglaEstado = siglaEstado;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( numero == null ) ? 0 : numero.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Endereco other = (Endereco) obj;
        if ( numero == null ) {
            if ( other.numero != null )
                return false;
        } else if ( !numero.equals( other.numero ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Endereco [logradouro=" + logradouro + ", getId()=" + getId() + "]";
    }

}
