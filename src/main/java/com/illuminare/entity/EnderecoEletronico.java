package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table( name = "endereco_eletronico" )
public class EnderecoEletronico extends GenericEntity {

    @Column( name = "endereco", length = 4000 )
    private String endereco;

    @Column( name = "obs", length = 4000 )
    private String obs;

    @ManyToOne
    @JoinColumn( name = "id_tipo_endereco_eletronico" )
    private TipoEnderecoEletronico tipoEnderecoEletronico;


    public String getEndereco() {

        return endereco;
    }


    public void setEndereco( String endereco ) {

        this.endereco = endereco;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public TipoEnderecoEletronico getTipoEnderecoEletronico() {

        return tipoEnderecoEletronico;
    }


    public void setTipoEnderecoEletronico( TipoEnderecoEletronico tipoEnderecoEletronico ) {

        this.tipoEnderecoEletronico = tipoEnderecoEletronico;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( endereco == null ) ? 0 : endereco.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        EnderecoEletronico other = (EnderecoEletronico) obj;
        if ( endereco == null ) {
            if ( other.endereco != null )
                return false;
        } else if ( !endereco.equals( other.endereco ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "EnderecoEletronico [endereco=" + endereco + ", getId()=" + getId() + "]";
    }

}
