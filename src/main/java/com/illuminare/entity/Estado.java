package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "estado" )
public class Estado extends GenericEntity {

    @Column( name = "nome" )
    private String nome;

    @Column( name = "uf" )
    private String uf;


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getUf() {

        return uf;
    }


    public void setUf( String uf ) {

        this.uf = uf;
    }


    @Override
    public String toString() {

        return "Estado [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
