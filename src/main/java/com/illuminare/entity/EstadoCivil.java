package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.illuminare.util.AppStringUtils;


@Entity
@Table( name = "estado_civil" )
public class EstadoCivil extends GenericEntity {

    @Column( name = "nome" )
    private String nome;


    public EstadoCivil() {

    }


    public EstadoCivil( String nome ) {

        setNome( nome );
    }


    public EstadoCivil( String nome, Boolean ativo ) {

        setNome( nome );
        setAtivo( ativo );
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = AppStringUtils.capitalizeFully( nome );
    }

}
