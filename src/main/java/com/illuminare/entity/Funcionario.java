package com.illuminare.entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.illuminare.view.FuncionarioView;


@Entity
@Table( name = "funcionario" )
public class Funcionario extends GenericEntity {

    @Transient
    GrupoOcupacional grupoOcupacional;

    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "id_pessoa" )
    private Pessoa pessoa;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_cargo" )
    private Cargo cargo;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_setor" )
    private Setor setor;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_lider" )
    // Funcionario lider;
    private Lider lider;

    @Column( name = "id_lider", insertable = false, updatable = false )
    private Long idLider;

    @Column( name = "matricula" )
    private String matricula;


    public Funcionario() {

    }


    public Funcionario( Long id ) {

        setId( id );
    }

    public static List< Long > getIds( List< Funcionario > entities ) {

        List< Long > ids = new ArrayList<>();

        if ( entities != null ) {
            for ( Funcionario entity : entities ) {
                if ( entity.getId() != null ) {
                    ids.add( entity.getId() );
                }
            }

        }

        return ids;
    }

    public Pessoa getPessoa() {

        return pessoa;
    }

    public void setPessoa( Pessoa pessoa ) {

        this.pessoa = pessoa;
    }

    public Cargo getCargo() {

        return cargo;
    }

    public void setCargo( Cargo cargo ) {

        this.cargo = cargo;
    }

    public Setor getSetor() {

        return setor;
    }

    public void setSetor( Setor setor ) {

        this.setor = setor;
    }

    public GrupoOcupacional getGrupoOcupacional() {

        return grupoOcupacional;
    }

    public void setGrupoOcupacional( GrupoOcupacional grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }

    public Lider getLider() {

        return lider;
    }

    public void setLider( Lider lider ) {

        this.lider = lider;
    }

    public String getMatricula() {

        return matricula;
    }

    public void setMatricula( String matricula ) {

        this.matricula = matricula;
    }

    public void preencherGrupoOcupacional() {

        if ( getCargo() != null ) {
            setGrupoOcupacional( getCargo().getGrupoOcupacional() );
        }
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( matricula == null ) ? 0 : matricula.hashCode() );
        result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Funcionario other = (Funcionario) obj;
        if ( matricula == null ) {
            if ( other.matricula != null )
                return false;
        } else if ( !matricula.equals( other.matricula ) )
            return false;
        if ( getId() == null ) {
            if ( other.getId() != null )
                return false;
        } else if ( !getId().equals( other.getId() ) )
            return false;
        return true;
    }

    @Override
    public String toString() {

        return "Funcionario [matricula=" + matricula + ", getId()=" + getId() + "]";
    }

    public Long getIdLider() {

        return idLider;
    }

    public void setIdLider( Long idLider ) {

        this.idLider = idLider;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public FuncionarioView format() {

            FuncionarioView view = new FuncionarioView();

            if ( getGrupoOcupacional() != null ) {
                view.setGrupoOcupacional( getGrupoOcupacional().formatToWebPage().format() );
            }
            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            if ( getPessoa() != null ) {
                view.setPessoa( getPessoa().formatToWebPage().format() );
            }
            if ( getCargo() != null ) {
                view.setCargo( getCargo().formatToWebPage().format() );
            }
            if ( getSetor() != null ) {
                view.setSetor( getSetor().formatToWebPage().format() );
            }
            if ( getLider() != null ) {
                view.setLider( getLider().formatToWebPage().format() );
            }
            view.setMatricula( getMatricula() );

            return view;
        }

    }

}
