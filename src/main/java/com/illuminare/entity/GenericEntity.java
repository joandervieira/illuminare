package com.illuminare.entity;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;


@MappedSuperclass
// @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,
// property="id")
// @JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class,
// property="$UUID")
// @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class,
// property="$idSerialization")
@JsonIgnoreProperties( ignoreUnknown = true )
public class GenericEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column( name = "ativo" )
    private Boolean ativo;

    @Column( name = "excluido" )
    private Boolean excluido = false;

    @Column( name = "user_cadastro" )
    private String usuarioCadastro;

    @Column( name = "user_atualizacao" )
    private String usuarioAtualizacao;

    @Column( name = "data_cadastro" )
    private Date dataCadastro;

    @Column( name = "data_atualizacao" )
    private Date dataAtualizacao;

    @Column( name = "id_empresa" )
    private Long idEmpresa;

    @Transient
    @JsonProperty
    private List< String > fieldsToFetch;

    @Transient
    @JsonProperty
    private Empresa empresa;


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Boolean getAtivo() {

        return ativo;
    }


    public void setAtivo( Boolean ativo ) {

        this.ativo = ativo;
    }


    public Boolean getExcluido() {

        return excluido;
    }


    public void setExcluido( Boolean excluido ) {

        this.excluido = excluido;
    }


    public String getUsuarioCadastro() {

        return usuarioCadastro;
    }


    public void setUsuarioCadastro( String usuarioCadastro ) {

        this.usuarioCadastro = usuarioCadastro;
    }


    public String getUsuarioAtualizacao() {

        return usuarioAtualizacao;
    }


    public void setUsuarioAtualizacao( String usuarioAtualizacao ) {

        this.usuarioAtualizacao = usuarioAtualizacao;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDataCadastro() {

        return dataCadastro;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDataCadastro( Date dataCadastro ) {

        this.dataCadastro = dataCadastro;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDataAtualizacao() {

        return dataAtualizacao;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDataAtualizacao( Date dataAtualizacao ) {

        this.dataAtualizacao = dataAtualizacao;
    }


    public List< String > getFieldsToFetch() {

        return fieldsToFetch;
    }


    public void setFieldsToFetch( List< String > fieldsToFetch ) {

        this.fieldsToFetch = fieldsToFetch;
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        GenericEntity other = (GenericEntity) obj;
        if ( id == null ) {
            if ( other.id != null )
                return false;
        } else if ( !id.equals( other.id ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "GenericEntity{" + "id=" + id + ", ativo=" + ativo + ", excluido=" + excluido + ", usuarioCadastro='" + usuarioCadastro + '\''
            + ", usuarioAtualizacao='" + usuarioAtualizacao + '\'' + ", dataCadastro=" + dataCadastro + ", dataAtualizacao=" + dataAtualizacao + ", idEmpresa="
            + idEmpresa + ", fieldsToFetch=" + fieldsToFetch + '}';
    }


    public Empresa getEmpresa() {

        return empresa;
    }


    public void setEmpresa( Empresa empresa ) {

        this.empresa = empresa;
    }

}
