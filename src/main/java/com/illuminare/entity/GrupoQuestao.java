package com.illuminare.entity;


import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.illuminare.dao.GenericDAO;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.QuestaoOrdemView;


@Entity
@Table( name = "grupo_questao" )
public class GrupoQuestao extends GenericEntity {

    @Column( name = "nome" )
    private String nome;

    @Column( name = "descricao" )
    private String descricao;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "id_grupo_questao" )
    private Set< QuestaoOrdem > questoesOrdem;

    @Column( name = "obs", length = 1000 )
    private String obs;

    @Transient
    private Double mediaPropria;

    @Transient
    private Double mediaLideres;

    @Transient
    private Double mediaSubordinados;

    @Transient
    private Double mediaOutros;

    @Transient
    private Double meta;

    @Transient
    private Double valorObtido;

    @Transient
    private Double percentualMeta;


    public GrupoQuestao() {

    }


    public GrupoQuestao( Long idGrupoQuestao ) {

        setId( idGrupoQuestao );
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDescricao() {

        return descricao;
    }


    public void setDescricao( String descricao ) {

        this.descricao = descricao;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public Set< QuestaoOrdem > getQuestoesOrdem() {

        return questoesOrdem;
    }


    public void setQuestoesOrdem( Set< QuestaoOrdem > questoesOrdem ) {

        this.questoesOrdem = questoesOrdem;
    }


    public Double getMediaPropria() {

        return mediaPropria;
    }


    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }


    public Double getMediaLideres() {

        return mediaLideres;
    }


    public void setMediaLideres( Double mediaLideres ) {

        this.mediaLideres = mediaLideres;
    }


    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }


    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }


    public Double getMediaOutros() {

        return mediaOutros;
    }


    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }


    @Override
    public String toString() {

        return "GrupoQuestao [nome=" + nome + ", getId()=" + getId() + "]";
    }


    public void unproxy( GenericDAO dao ) {

        dao.unproxyInitialize( getQuestoesOrdem() );
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public GrupoQuestaoView format() {

            GrupoQuestaoView view = new GrupoQuestaoView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setNome( getNome() );
            view.setDescricao( getDescricao() );
            view.setQuestoesOrdem( getQuestoesOrdemView() );
            view.setObs( getObs() );
            view.setMediaPropria( getMediaPropria() );
            view.setMediaLideres( getMediaLideres() );
            view.setMediaSubordinados( getMediaSubordinados() );
            view.setMediaOutros( getMediaOutros() );
            view.setMeta( getMeta() );
            view.setValorObtido( getValorObtido() );
            view.setPercentualMeta( getPercentualMeta() );

            return view;
        }


        private Set< QuestaoOrdemView > getQuestoesOrdemView() {

            if ( getQuestoesOrdem() != null ) {
                Set< QuestaoOrdemView > views = new LinkedHashSet<>();
                for ( QuestaoOrdem questaoOrdem : getQuestoesOrdem() ) {
                    views.add( questaoOrdem.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

    }

}
