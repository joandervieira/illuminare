package com.illuminare.entity;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.illuminare.view.LiderView;


@Entity
@Table( name = "funcionario" )
public class Lider extends GenericEntity {

    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "id_pessoa" )
    Pessoa pessoa;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_cargo" )
    Cargo cargo;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_setor" )
    Setor setor;

    @Transient
    GrupoOcupacional grupoOcupacional;

    @Column( name = "matricula" )
    private String matricula;


    public Lider() {

    }


    public Lider( Long id, String matricula ) {

        setId( id );
        this.matricula = matricula;
    }


    public Pessoa getPessoa() {

        return pessoa;
    }


    public void setPessoa( Pessoa pessoa ) {

        this.pessoa = pessoa;
    }


    public Cargo getCargo() {

        return cargo;
    }


    public void setCargo( Cargo cargo ) {

        this.cargo = cargo;
    }


    public Setor getSetor() {

        return setor;
    }


    public void setSetor( Setor setor ) {

        this.setor = setor;
    }


    public GrupoOcupacional getGrupoOcupacional() {

        return grupoOcupacional;
    }


    public void setGrupoOcupacional( GrupoOcupacional grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }


    public String getMatricula() {

        return matricula;
    }


    public void setMatricula( String matricula ) {

        this.matricula = matricula;
    }


    @Override
    public String toString() {

        return "Funcionario [getId()=" + getId() + "]";
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public LiderView format() {

            LiderView view = new LiderView();

            if ( getGrupoOcupacional() != null ) {
                view.setGrupoOcupacional( getGrupoOcupacional().formatToWebPage().format() );
            }
            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            if ( getPessoa() != null ) {
                view.setPessoa( getPessoa().formatToWebPage().format() );
            }
            if ( getCargo() != null ) {
                view.setCargo( getCargo().formatToWebPage().format() );
            }
            if ( getSetor() != null ) {
                view.setSetor( getSetor().formatToWebPage().format() );
            }
            view.setMatricula( getMatricula() );

            return view;
        }

    }

}
