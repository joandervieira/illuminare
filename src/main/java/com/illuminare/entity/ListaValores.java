package com.illuminare.entity;


import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.illuminare.view.ListaValoresView;
import com.illuminare.view.ValorListaView;


@Entity
@Table( name = "lista_valores" )
public class ListaValores extends GenericEntity {

    @Column( name = "nome", length = 100 )
    private String nome;

    @Column( name = "tipo_lista" )
    private String tipoLista;

    @OneToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "id_lista_valores" )
    private Set< ValorLista > valoresLista = new LinkedHashSet< ValorLista >();


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public Set< ValorLista > getValoresLista() {

        return valoresLista;
    }


    public void setValoresLista( Set< ValorLista > valoresLista ) {

        this.valoresLista = valoresLista;
    }


    public String getTipoLista() {

        return tipoLista;
    }


    public void setTipoLista( String tipoLista ) {

        this.tipoLista = tipoLista;
    }


    @Override
    public String toString() {

        return "ListaValores{" + "nome='" + nome + '\'' + ", tipoLista='" + tipoLista + '\'' + ", valoresLista=" + valoresLista + '}';
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public ListaValoresView format() {

            ListaValoresView view = new ListaValoresView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setNome( getNome() );
            view.setTipoLista( getTipoLista() );
            view.setValoresLista( getValoresListaView() );

            return view;
        }


        public Set< ValorListaView > getValoresListaView() {

            if ( getValoresLista() != null ) {
                Set< ValorListaView > valoresListaView = new LinkedHashSet<>();
                for ( ValorLista valorLista : getValoresLista() ) {
                    valoresListaView.add( valorLista.formatToWebPage().format() );
                }
                return valoresListaView;
            }
            return null;
        }

    }

}
