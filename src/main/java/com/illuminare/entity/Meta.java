package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table( name = "meta" )
public class Meta extends GenericEntity {

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_funcionario" )
    private Funcionario funcionario;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_pesquisa" )
    private Pesquisa pesquisa;

    @Column( name = "meta", length = 4000 )
    private String meta;


    public Meta() {

    }


    public Funcionario getFuncionario() {

        return funcionario;
    }


    public void setFuncionario( Funcionario funcionario ) {

        this.funcionario = funcionario;
    }


    public Pesquisa getPesquisa() {

        return pesquisa;
    }


    public void setPesquisa( Pesquisa pesquisa ) {

        this.pesquisa = pesquisa;
    }


    public String getMeta() {

        return meta;
    }


    public void setMeta( String meta ) {

        this.meta = meta;
    }


    @Override
    public String toString() {

        return "Meta [funcionario=" + funcionario + ", pesquisa=" + pesquisa + ", meta=" + meta + "]";
    }

}
