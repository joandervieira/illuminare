package com.illuminare.entity;


import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;
import com.illuminare.view.PesquisaView;
import com.illuminare.view.QuestionarioView;


@Entity
@Table( name = "pesquisa" )
public class Pesquisa extends GenericEntity {

    @Column( name = "nome", length = 100 )
    private String nome;

    @Column( name = "dta_inicio" )
    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaInicio;

    @Column( name = "dta_fim" )
    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaFim;

    @Column( name = "dta_entrega" )
    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaEntrega;

    @Column( name = "obs", length = 4000 )
    private String obs;

    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable( name = "pesquisa_questionario", joinColumns = { @JoinColumn( name = "id_pesquisa" ) },
        inverseJoinColumns = { @JoinColumn( name = "id_questionario" ) } )
    private Set< Questionario > questionarios;

    @Column( name = "auto_avaliacao" )
    private Boolean autoAvaliacao;

    @Column( name = "avaliacao_subordinados" )
    private Boolean avaliacaoSubordinados;

    // private Set<Questionario> questionarios = new
    // LinkedHashSet<Questionario>();
    @Column( name = "avaliacao_superiores" )
    private Boolean avaliacaoSuperiores;

    @Column( name = "por_tipo" )
    private Boolean porTipo;

    @Column( name = "calculo_por_meta" )
    private Boolean calculoPorMeta;

    @Column( name = "calculo_valor_obtido_superior" )
    private Boolean calculoValorObtidoSuperior = true;

    @Column( name = "calculo_valor_obtido_propria" )
    private Boolean calculoValorObtidoPropria = true;

    @Column( name = "calculo_valor_obtido_subordinados" )
    private Boolean calculoValorObtidoSubordinados = true;

    @Column( name = "calculo_valor_obtido_outros" )
    private Boolean calculoValorObtidoOutros = true;

    @Transient
    private Date dtaInicioMaiorQue;

    @Transient
    private Date dtaInicioMenorQue;

    @Transient
    private Date dtaFimMaiorQue;

    @Transient
    private Date dtaFimMenorQue;

    @Transient
    private Date dtaEntregaMaiorQue;

    @Transient
    private Date dtaEntregaMenorQue;

    @Transient
    private boolean salvarMetas;


    public Pesquisa() {

    }


    public Pesquisa( Long id ) {

        setId( id );
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaInicio() {

        return dtaInicio;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaInicio( Date dtaInicio ) {

        this.dtaInicio = dtaInicio;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaFim() {

        return dtaFim;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaFim( Date dtaFim ) {

        this.dtaFim = dtaFim;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaEntrega() {

        return dtaEntrega;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaEntrega( Date dtaEntrega ) {

        this.dtaEntrega = dtaEntrega;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public Set< Questionario > getQuestionarios() {

        return questionarios;
    }


    public void setQuestionarios( Set< Questionario > questionarios ) {

        this.questionarios = questionarios;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaInicioMaiorQue() {

        return dtaInicioMaiorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaInicioMaiorQue( Date dtaInicioMaiorQue ) {

        this.dtaInicioMaiorQue = dtaInicioMaiorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaInicioMenorQue() {

        return dtaInicioMenorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaInicioMenorQue( Date dtaInicioMenorQue ) {

        this.dtaInicioMenorQue = dtaInicioMenorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaFimMaiorQue() {

        return dtaFimMaiorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaFimMaiorQue( Date dtaFimMaiorQue ) {

        this.dtaFimMaiorQue = dtaFimMaiorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaFimMenorQue() {

        return dtaFimMenorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaFimMenorQue( Date dtaFimMenorQue ) {

        this.dtaFimMenorQue = dtaFimMenorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaEntregaMaiorQue() {

        return dtaEntregaMaiorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaEntregaMaiorQue( Date dtaEntregaMaiorQue ) {

        this.dtaEntregaMaiorQue = dtaEntregaMaiorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaEntregaMenorQue() {

        return dtaEntregaMenorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaEntregaMenorQue( Date dtaEntregaMenorQue ) {

        this.dtaEntregaMenorQue = dtaEntregaMenorQue;
    }


    public boolean getAutoAvaliacao() {

        if ( autoAvaliacao == null ) {
            autoAvaliacao = false;
        }

        return autoAvaliacao;
    }


    public void setAutoAvaliacao( boolean autoAvaliacao ) {

        this.autoAvaliacao = autoAvaliacao;
    }


    public Boolean getAvaliacaoSubordinados() {

        if ( avaliacaoSubordinados == null ) {
            avaliacaoSubordinados = false;
        }
        return avaliacaoSubordinados;
    }


    public void setAvaliacaoSubordinados( Boolean avaliacaoSubordinados ) {

        this.avaliacaoSubordinados = avaliacaoSubordinados;
    }


    public Boolean getAvaliacaoSuperiores() {

        if ( avaliacaoSuperiores == null ) {
            avaliacaoSuperiores = false;
        }

        return avaliacaoSuperiores;
    }


    public void setAvaliacaoSuperiores( Boolean avaliacaoSuperiores ) {

        this.avaliacaoSuperiores = avaliacaoSuperiores;
    }


    @Override
    public String toString() {

        return "Pesquisa [nome=" + nome + ", getId()=" + getId() + "]";
    }


    public Boolean getPorTipo() {

        if ( porTipo == null ) {
            porTipo = false;
        }

        return porTipo;
    }


    public void setPorTipo( Boolean porTipo ) {

        this.porTipo = porTipo;
    }


    public boolean getSalvarMetas() {

        return salvarMetas;
    }


    public void setSalvarMetas( boolean salvarMetas ) {

        this.salvarMetas = salvarMetas;
    }


    public Boolean getCalculoPorMeta() {

        if ( calculoPorMeta == null ) {
            return false;
        }

        return calculoPorMeta;
    }


    public void setCalculoPorMeta( Boolean calculoPorMeta ) {

        this.calculoPorMeta = calculoPorMeta;
    }


    public Boolean getCalculoValorObtidoSuperior() {

        if ( calculoValorObtidoSuperior == null ) {
            calculoValorObtidoSuperior = true;
        }

        return calculoValorObtidoSuperior;
    }


    public void setCalculoValorObtidoSuperior( Boolean calculoValorObtidoSuperior ) {

        this.calculoValorObtidoSuperior = calculoValorObtidoSuperior;
    }


    public Boolean getCalculoValorObtidoPropria() {

        if ( calculoValorObtidoPropria == null ) {
            calculoValorObtidoPropria = true;
        }

        return calculoValorObtidoPropria;
    }


    public void setCalculoValorObtidoPropria( Boolean calculoValorObtidoPropria ) {

        this.calculoValorObtidoPropria = calculoValorObtidoPropria;
    }


    public Boolean getCalculoValorObtidoSubordinados() {

        if ( calculoValorObtidoSubordinados == null ) {
            calculoValorObtidoSubordinados = true;
        }

        return calculoValorObtidoSubordinados;
    }


    public void setCalculoValorObtidoSubordinados( Boolean calculoValorObtidoSubordinados ) {

        this.calculoValorObtidoSubordinados = calculoValorObtidoSubordinados;
    }


    public Boolean getCalculoValorObtidoOutros() {

        if ( calculoValorObtidoOutros == null ) {
            calculoValorObtidoOutros = true;
        }

        return calculoValorObtidoOutros;
    }


    public void setCalculoValorObtidoOutros( Boolean calculoValorObtidoOutros ) {

        this.calculoValorObtidoOutros = calculoValorObtidoOutros;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public Pesquisa copy() {

        Pesquisa newEntity = new Pesquisa();
        BeanUtils.copyProperties( this, newEntity );
        return newEntity;
    }

    public class FormatBuilder {

        private boolean excludeQuestionarios = false;

        public PesquisaView format() {

            PesquisaView pesquisaView = new PesquisaView();
            pesquisaView.setId( getId() );
            pesquisaView.setIdEmpresa( getIdEmpresa() );
            pesquisaView.setNome( getNome() );
            pesquisaView.setDtaInicio( getDtaInicio() );
            pesquisaView.setDtaFim( getDtaFim() );
            pesquisaView.setDtaEntrega( getDtaEntrega() );
            pesquisaView.setObs( getObs() );
            if ( !excludeQuestionarios ) {
                pesquisaView.setQuestionarios( getQuestionariosView() );
            }
            pesquisaView.setAutoAvaliacao( getAutoAvaliacao() );
            pesquisaView.setAvaliacaoSubordinados( getAvaliacaoSubordinados() );
            pesquisaView.setAvaliacaoSuperiores( getAvaliacaoSuperiores() );
            pesquisaView.setPorTipo( getPorTipo() );
            pesquisaView.setCalculoPorMeta( getCalculoPorMeta() );
            pesquisaView.setCalculoValorObtidoSuperior( getCalculoValorObtidoSuperior() );
            pesquisaView.setCalculoValorObtidoPropria( getCalculoValorObtidoPropria() );
            pesquisaView.setCalculoValorObtidoSubordinados( getCalculoValorObtidoSubordinados() );
            pesquisaView.setCalculoValorObtidoOutros( getCalculoValorObtidoOutros() );
            pesquisaView.setDtaInicioMaiorQue( getDtaInicioMaiorQue() );
            pesquisaView.setDtaInicioMenorQue( getDtaInicioMenorQue() );
            pesquisaView.setDtaFimMaiorQue( getDtaFimMaiorQue() );
            pesquisaView.setDtaFimMenorQue( getDtaFimMenorQue() );
            pesquisaView.setDtaEntregaMaiorQue( getDtaEntregaMaiorQue() );
            pesquisaView.setDtaEntregaMenorQue( getDtaEntregaMenorQue() );
            pesquisaView.setSalvarMetas( getSalvarMetas() );
            pesquisaView.setAtivo( getAtivo() );
            pesquisaView.setExcluido( getExcluido() );

            return pesquisaView;
        }

        private Set< QuestionarioView > getQuestionariosView() {

            if ( getQuestionarios() != null ) {
                Set< QuestionarioView > views = new LinkedHashSet<>();
                for ( Questionario questionario : getQuestionarios() ) {
                    views.add( questionario.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

        public FormatBuilder excludeQuestionarios() {

            this.excludeQuestionarios = true;

            return this;
        }

    }

}
