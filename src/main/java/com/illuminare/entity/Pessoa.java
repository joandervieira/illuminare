package com.illuminare.entity;


import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;
import com.illuminare.view.PessoaView;


@Entity
@Table( name = "pessoa" )
public class Pessoa extends GenericEntity {

    @Column( name = "tipo_pessoa" )
    private String tipoPessoa;

    @Column( name = "nome", length = 200 )
    private String nome;

    @Column( name = "documento" )
    private String documento; // cpf ou cnpj

    @Column( name = "rg" )
    private String rg;

    @Column( name = "sexo" )
    private String sexo;

    @Column( name = "dta_nascimento" )
    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaNascimento;

    @Column( name = "inscricao_estadual" )
    private String inscricaoEstadual;

    @Column( name = "razao_social" )
    private String razaoSocial;

    @Column( name = "obs", length = 4000 )
    private String obs;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_estado_civil" )
    private EstadoCivil estadoCivil;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "id_pessoa" )
    private Set< Telefone > telefones;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "id_pessoa" )
    private Set< Endereco > enderecos;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "id_pessoa" )
    private Set< EnderecoEletronico > enderecosEletronicos;


    public String getTipoPessoa() {

        return tipoPessoa;
    }


    public void setTipoPessoa( String tipoPessoa ) {

        this.tipoPessoa = tipoPessoa;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDocumento() {

        return documento;
    }


    public void setDocumento( String documento ) {

        this.documento = documento;
    }


    public String getSexo() {

        return sexo;
    }


    public void setSexo( String sexo ) {

        this.sexo = sexo;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaNascimento() {

        return dtaNascimento;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaNascimento( Date dtaNascimento ) {

        this.dtaNascimento = dtaNascimento;
    }


    public String getInscricaoEstadual() {

        return inscricaoEstadual;
    }


    public void setInscricaoEstadual( String inscricaoEstadual ) {

        this.inscricaoEstadual = inscricaoEstadual;
    }


    public String getRazaoSocial() {

        return razaoSocial;
    }


    public void setRazaoSocial( String razaoSocial ) {

        this.razaoSocial = razaoSocial;
    }


    public EstadoCivil getEstadoCivil() {

        return estadoCivil;
    }


    public void setEstadoCivil( EstadoCivil estadoCivil ) {

        this.estadoCivil = estadoCivil;
    }


    public Set< Telefone > getTelefones() {

        return telefones;
    }


    public void setTelefones( Set< Telefone > telefones ) {

        this.telefones = telefones;
    }


    public Set< Endereco > getEnderecos() {

        return enderecos;
    }


    public void setEnderecos( Set< Endereco > enderecos ) {

        this.enderecos = enderecos;
    }


    public Set< EnderecoEletronico > getEnderecosEletronicos() {

        return enderecosEletronicos;
    }


    public void setEnderecosEletronicos( Set< EnderecoEletronico > enderecosEletronicos ) {

        this.enderecosEletronicos = enderecosEletronicos;
    }


    public String getRg() {

        return rg;
    }


    public void setRg( String rg ) {

        if ( rg != null ) {
            rg = rg.toUpperCase().trim();
        }
        this.rg = rg;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
        result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Pessoa other = (Pessoa) obj;
        if ( nome == null ) {
            if ( other.nome != null )
                return false;
        } else if ( !nome.equals( other.nome ) )
            return false;
        if ( getId() == null ) {
            if ( other.getId() != null )
                return false;
        } else if ( !getId().equals( other.getId() ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Pessoa [nome=" + nome + ", getId()=" + getId() + "]";
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public PessoaView format() {

            PessoaView view = new PessoaView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setTipoPessoa( getTipoPessoa() );
            view.setNome( getNome() );
            view.setDocumento( getDocumento() );
            view.setRg( getRg() );
            view.setSexo( getSexo() );
            view.setDtaNascimento( getDtaNascimento() );
            view.setInscricaoEstadual( getInscricaoEstadual() );
            view.setRazaoSocial( getRazaoSocial() );
            view.setObs( getObs() );
            // TODO format view
            // view.setEstadoCivil(getEstadoCivil());
            // view.setTelefones(getTelefones());
            // view.setEnderecos(getEnderecos());
            // view.setEnderecosEletronicos(getEnderecosEletronicos());

            return view;
        }

    }

}
