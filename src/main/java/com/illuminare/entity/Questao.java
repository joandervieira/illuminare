package com.illuminare.entity;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.illuminare.dao.GenericDAO;
import com.illuminare.resultado.Nota;
import com.illuminare.view.QuestaoMetaView;
import com.illuminare.view.QuestaoView;


@Entity
@Table( name = "questao" )
public class Questao extends GenericEntity {

    @Column( name = "nome_questao", length = 100 )
    private String nomeQuestao;

    @Column( name = "pergunta_questao", length = 1000 )
    private String perguntaQuestao;

    @Column( name = "is_texto" )
    private Boolean isTexto;

    @Column( name = "is_lista" )
    private Boolean isLista;

    @Column( name = "is_numerico" )
    private Boolean isNumerico;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_lista_valores" )
    private ListaValores listaValores;

    @Transient
    private Nota notaPropria;

    @Transient
    private Nota notaLideres;

    @Transient
    private Nota notaSubordinados;

    @Transient
    private Nota notaOutros;

    @Transient
    private Resposta resposta;

    @Transient
    private String respostaTexto;

    @Transient
    private Double respostaNumerico;

    @Transient
    private ValorLista respostaValorLista;

    @Transient
    private Long respostaId;

    @Transient
    private List< QuestaoMeta > questoesMetasGruposOcupacionais = new ArrayList<>();

    @Transient
    private List< QuestaoMeta > questoesMetasCargos = new ArrayList<>();

    @Transient
    private List< QuestaoMeta > questoesMetasSetores = new ArrayList<>();

    @Transient
    private Long classificacao;

    @Transient
    private QuestaoClassificacao questaoClassificacao;

    @Transient
    private Double meta;

    @Transient
    private Boolean negativo = false;

    @Transient
    private Double valorObtido;

    @Transient
    private Double percentualMeta;

    @Transient
    private Double media;

    @Transient
    private List< Resposta > respostasProprias = new ArrayList<>();

    @Transient
    private List< Resposta > respostasLider = new ArrayList<>();

    @Transient
    private List< Resposta > respostasSubordinados = new ArrayList<>();

    @Transient
    private List< Resposta > respostasOutros = new ArrayList<>();


    public Questao() {

    }


    public Questao( Long id ) {

        setId( id );
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public String getNomeQuestao() {

        return nomeQuestao;
    }


    public void setNomeQuestao( String nomeQuestao ) {

        this.nomeQuestao = nomeQuestao;
    }


    public Boolean getIsTexto() {

        if ( isTexto == null ) {
            isTexto = false;
        }

        return isTexto;
    }


    public void setIsTexto( Boolean isTexto ) {

        this.isTexto = isTexto;
    }


    public Boolean getIsLista() {

        if ( isLista == null ) {
            isLista = false;
        }

        return isLista;
    }


    public void setIsLista( Boolean isLista ) {

        this.isLista = isLista;
    }


    public ListaValores getListaValores() {

        return listaValores;
    }


    public void setListaValores( ListaValores listaValores ) {

        this.listaValores = listaValores;
    }


    public String getPerguntaQuestao() {

        return perguntaQuestao;
    }


    public void setPerguntaQuestao( String perguntaQuestao ) {

        this.perguntaQuestao = perguntaQuestao;
    }


    public Nota getNotaPropria() {

        return notaPropria;
    }


    public void setNotaPropria( Nota notaPropria ) {

        this.notaPropria = notaPropria;
    }


    public Nota getNotaLideres() {

        return notaLideres;
    }


    public void setNotaLideres( Nota notaLideres ) {

        this.notaLideres = notaLideres;
    }


    public Nota getNotaSubordinados() {

        return notaSubordinados;
    }


    public void setNotaSubordinados( Nota notaSubordinados ) {

        this.notaSubordinados = notaSubordinados;
    }


    public Nota getNotaOutros() {

        return notaOutros;
    }


    public void setNotaOutros( Nota notaOutros ) {

        this.notaOutros = notaOutros;
    }


    public void setTexto( Boolean isTexto ) {

        this.isTexto = isTexto;
    }


    public void setLista( Boolean isLista ) {

        this.isLista = isLista;
    }


    public Resposta getResposta() {

        return resposta;
    }


    public void setResposta( Resposta resposta ) {

        this.resposta = resposta;
    }


    public String getRespostaTexto() {

        return respostaTexto;
    }


    public void setRespostaTexto( String respostaTexto ) {

        this.respostaTexto = respostaTexto;
    }


    public ValorLista getRespostaValorLista() {

        return respostaValorLista;
    }


    public void setRespostaValorLista( ValorLista respostaValorLista ) {

        this.respostaValorLista = respostaValorLista;
    }


    @Override
    public String toString() {

        return "Questao [nomeQuestao=" + nomeQuestao + ", getId()=" + getId() + "]";
    }


    public Long getRespostaId() {

        return respostaId;
    }


    public void setRespostaId( Long respostaId ) {

        this.respostaId = respostaId;
    }


    public Boolean getIsNumerico() {

        if ( isNumerico == null ) {
            isNumerico = false;
        }

        return isNumerico;
    }


    public void setIsNumerico( Boolean numerico ) {

        isNumerico = numerico;
    }


    public Double getRespostaNumerico() {

        return respostaNumerico;
    }


    public void setRespostaNumerico( Double respostaNumerico ) {

        this.respostaNumerico = respostaNumerico;
    }


    public Long getClassificacao() {

        return classificacao;
    }


    public void setClassificacao( Long classificacao ) {

        this.classificacao = classificacao;
    }


    public QuestaoClassificacao getQuestaoClassificacao() {

        return questaoClassificacao;
    }


    public void setQuestaoClassificacao( QuestaoClassificacao questaoClassificacao ) {

        if ( questaoClassificacao != null ) {
            this.classificacao = questaoClassificacao.getClassificacao();
        }

        this.questaoClassificacao = questaoClassificacao;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( !( o instanceof Questao ) )
            return false;
        if ( !super.equals( o ) )
            return false;
        Questao questao = (Questao) o;

        return Objects.equals( getNomeQuestao(), questao.getNomeQuestao() ) && Objects.equals( getPerguntaQuestao(), questao.getPerguntaQuestao() )
            && Objects.equals( getId(), questao.getId() );
    }


    @Override
    public int hashCode() {

        return Objects.hash( super.hashCode(), getNomeQuestao(), getPerguntaQuestao(), getId() );
    }


    public Boolean getNegativo() {

        return negativo;
    }


    public void setNegativo( Boolean negativo ) {

        this.negativo = negativo;
    }


    public Double getMedia() {

        return media;
    }


    public void setMedia( Double media ) {

        this.media = media;
    }


    public List< QuestaoMeta > getQuestoesMetasGruposOcupacionais() {

        return questoesMetasGruposOcupacionais;
    }


    public void setQuestoesMetasGruposOcupacionais( List< QuestaoMeta > questoesMetasGruposOcupacionais ) {

        this.questoesMetasGruposOcupacionais = questoesMetasGruposOcupacionais;
    }


    public List< QuestaoMeta > getQuestoesMetasCargos() {

        return questoesMetasCargos;
    }


    public void setQuestoesMetasCargos( List< QuestaoMeta > questoesMetasCargos ) {

        this.questoesMetasCargos = questoesMetasCargos;
    }


    public List< QuestaoMeta > getQuestoesMetasSetores() {

        return questoesMetasSetores;
    }


    public void setQuestoesMetasSetores( List< QuestaoMeta > questoesMetasSetores ) {

        this.questoesMetasSetores = questoesMetasSetores;
    }


    public void unproxy( GenericDAO dao ) {

        dao.unproxyInitialize( getListaValores() );
    }


    public List< Resposta > getRespostasProprias() {

        return respostasProprias;
    }


    public void setRespostasProprias( List< Resposta > respostasProprias ) {

        this.respostasProprias = respostasProprias;
    }


    public List< Resposta > getRespostasLider() {

        return respostasLider;
    }


    public void setRespostasLider( List< Resposta > respostasLider ) {

        this.respostasLider = respostasLider;
    }


    public List< Resposta > getRespostasSubordinados() {

        return respostasSubordinados;
    }


    public void setRespostasSubordinados( List< Resposta > respostasSubordinados ) {

        this.respostasSubordinados = respostasSubordinados;
    }


    public List< Resposta > getRespostasOutros() {

        return respostasOutros;
    }


    public void setRespostasOutros( List< Resposta > respostasOutros ) {

        this.respostasOutros = respostasOutros;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public QuestaoView format() {

            QuestaoView view = new QuestaoView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setNomeQuestao( getNomeQuestao() );
            view.setPerguntaQuestao( getPerguntaQuestao() );
            view.setIsTexto( getIsTexto() );
            view.setIsLista( getIsLista() );
            view.setIsNumerico( getIsNumerico() );
            if ( getListaValores() != null ) {
                view.setListaValores( getListaValores().formatToWebPage().format() );
            }
            view.setNotaPropria( getNotaPropria() );
            view.setNotaLideres( getNotaLideres() );
            view.setNotaSubordinados( getNotaSubordinados() );
            view.setNotaOutros( getNotaOutros() );
            if ( getResposta() != null ) {
                view.setResposta( getResposta().formatToWebPage().excludeQuestao().excludeQuestionarioAvaliacao().excludeRespostaValorLista().format() );
            }
            view.setRespostaTexto( getRespostaTexto() );
            view.setRespostaNumerico( getRespostaNumerico() );
            if ( getRespostaValorLista() != null ) {
                view.setRespostaValorLista( getRespostaValorLista().formatToWebPage().format() );
            }
            view.setRespostaId( getRespostaId() );
            view.setQuestoesMetasGruposOcupacionais( getQuestoesMetasGruposOcupacionaisView() );
            view.setQuestoesMetasCargos( getQuestoesMetasCargosView() );
            view.setQuestoesMetasSetores( getQuestoesMetasSetoresView() );
            view.setClassificacao( getClassificacao() );
            if ( getQuestaoClassificacao() != null ) {
                view.setQuestaoClassificacao( getQuestaoClassificacao().formatToWebPage().format() );
            }
            view.setMeta( getMeta() );
            view.setNegativo( getNegativo() );
            view.setValorObtido( getValorObtido() );
            view.setPercentualMeta( getPercentualMeta() );
            view.setMedia( getMedia() );

            return view;
        }


        private List< QuestaoMetaView > getQuestoesMetasSetoresView() {

            if ( getQuestoesMetasSetores() != null ) {
                List< QuestaoMetaView > views = new ArrayList<>();
                for ( QuestaoMeta questaoMeta : getQuestoesMetasSetores() ) {
                    views.add( questaoMeta.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }


        private List< QuestaoMetaView > getQuestoesMetasCargosView() {

            if ( getQuestoesMetasCargos() != null ) {
                List< QuestaoMetaView > views = new ArrayList<>();
                for ( QuestaoMeta questaoMeta : getQuestoesMetasCargos() ) {
                    views.add( questaoMeta.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }


        private List< QuestaoMetaView > getQuestoesMetasGruposOcupacionaisView() {

            if ( getQuestoesMetasGruposOcupacionais() != null ) {
                List< QuestaoMetaView > views = new ArrayList<>();
                for ( QuestaoMeta questaoMeta : getQuestoesMetasGruposOcupacionais() ) {
                    views.add( questaoMeta.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

    }
}
