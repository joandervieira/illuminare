package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.illuminare.view.QuestaoClassificacaoView;


@Entity
@Table( name = "questao_classificacao" )
public class QuestaoClassificacao extends GenericEntity {

    @Column( name = "id_questionario" )
    private Long idQuestionario;

    @Column( name = "id_grupo_questao" )
    private Long idGrupoQuestao;

    @Column( name = "id_questao" )
    private Long idQuestao;

    @Column( name = "classificacao" )
    private Long classificacao;


    public Long getClassificacao() {

        return classificacao;
    }


    public void setClassificacao( Long classificacao ) {

        this.classificacao = classificacao;
    }


    public Long getIdQuestao() {

        return idQuestao;
    }


    public void setIdQuestao( Long idQuestao ) {

        this.idQuestao = idQuestao;
    }


    public Long getIdQuestionario() {

        return idQuestionario;
    }


    public void setIdQuestionario( Long idQuestionario ) {

        this.idQuestionario = idQuestionario;
    }


    @Override
    public String toString() {

        return "QuestaoClassificacao{" + "idQuestionario=" + idQuestionario + ", idQuestao=" + idQuestao + ", classificacao=" + classificacao + '}';
    }


    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }


    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public QuestaoClassificacaoView format() {

            QuestaoClassificacaoView view = new QuestaoClassificacaoView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setIdQuestionario( getIdQuestionario() );
            view.setIdGrupoQuestao( getIdGrupoQuestao() );
            view.setIdQuestao( getIdQuestao() );
            view.setClassificacao( getClassificacao() );

            return view;
        }

    }
}
