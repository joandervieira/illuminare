package com.illuminare.entity;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.illuminare.view.QuestaoMetaView;


@Entity
@Table( name = "questao_meta" )
public class QuestaoMeta extends GenericEntity {

    @Column( name = "id_questao" )
    private Long idQuestao;

    @Column( name = "id_grupo_questao" )
    private Long idGrupoQuestao;

    @Column( name = "id_pesquisa" )
    private Long idPesquisa;

    @Column( name = "meta" )
    private Double meta;

    @Column( name = "auto_calculada" )
    private Boolean autoCalculada;

    @Column( name = "negativo" )
    private Boolean negativo = false;

    @Column( name = "id_grupo_ocupacional" )
    private Long idGrupoOcupacional;

    @Column( name = "id_cargo" )
    private Long idCargo;

    @Column( name = "id_setor" )
    private Long idSetor;

    @Column( name = "id_questionario" )
    private Long idQuestionario;

    public static List< QuestaoMetaView > formatToWebPage( List< QuestaoMeta > questaoMetas ) {

        List< QuestaoMetaView > newList = new ArrayList<>();
        if ( questaoMetas != null ) {
            for ( QuestaoMeta questaoMeta : questaoMetas ) {
                newList.add( questaoMeta.formatToWebPage().format() );
            }

        }
        return newList;
    }

    public Long getIdQuestao() {

        return idQuestao;
    }

    public void setIdQuestao( Long idQuestao ) {

        this.idQuestao = idQuestao;
    }

    public Long getIdPesquisa() {

        return idPesquisa;
    }

    public void setIdPesquisa( Long idPesquisa ) {

        this.idPesquisa = idPesquisa;
    }

    public Double getMeta() {

        return meta;
    }

    public void setMeta( Double meta ) {

        this.meta = meta;
    }

    public Boolean getNegativo() {

        return negativo;
    }

    public void setNegativo( Boolean negativo ) {

        this.negativo = negativo;
    }

    public Long getIdGrupoOcupacional() {

        return idGrupoOcupacional;
    }

    public void setIdGrupoOcupacional( Long idGrupoOcupacional ) {

        this.idGrupoOcupacional = idGrupoOcupacional;
    }

    public Long getIdCargo() {

        return idCargo;
    }

    public void setIdCargo( Long idCargo ) {

        this.idCargo = idCargo;
    }

    public Long getIdSetor() {

        return idSetor;
    }

    public void setIdSetor( Long idSetor ) {

        this.idSetor = idSetor;
    }

    public Long getIdQuestionario() {

        return idQuestionario;
    }

    public void setIdQuestionario( Long idQuestionario ) {

        this.idQuestionario = idQuestionario;
    }

    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        if ( !super.equals( o ) )
            return false;
        QuestaoMeta that = (QuestaoMeta) o;
        return Objects.equals( getIdQuestao(), that.getIdQuestao() ) && Objects.equals( getIdPesquisa(), that.getIdPesquisa() )
            && Objects.equals( getMeta(), that.getMeta() ) && Objects.equals( getNegativo(), that.getNegativo() )
            && Objects.equals( getIdGrupoOcupacional(), that.getIdGrupoOcupacional() ) && Objects.equals( getIdCargo(), that.getIdCargo() )
            && Objects.equals( getIdSetor(), that.getIdSetor() ) && Objects.equals( getIdQuestionario(), that.getIdQuestionario() );
    }

    @Override
    public int hashCode() {

        return Objects.hash(
            super.hashCode(),
            getIdQuestao(),
            getIdPesquisa(),
            getMeta(),
            getNegativo(),
            getIdGrupoOcupacional(),
            getIdCargo(),
            getIdSetor(),
            getIdQuestionario() );
    }

    public Boolean getAutoCalculada() {

        return autoCalculada;
    }

    public void setAutoCalculada( Boolean autoCalculada ) {

        this.autoCalculada = autoCalculada;
    }

    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }

    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public QuestaoMeta copy() {

        QuestaoMeta newEntity = new QuestaoMeta();
        BeanUtils.copyProperties( this, newEntity );
        return newEntity;
    }

    public class FormatBuilder {

        public QuestaoMetaView format() {

            QuestaoMetaView view = new QuestaoMetaView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setIdQuestao( getIdQuestao() );
            view.setIdGrupoQuestao( getIdGrupoQuestao() );
            view.setIdPesquisa( getIdPesquisa() );
            view.setMeta( getMeta() );
            view.setAutoCalculada( getAutoCalculada() );
            view.setNegativo( getNegativo() );
            view.setIdGrupoOcupacional( getIdGrupoOcupacional() );
            view.setIdCargo( getIdCargo() );
            view.setIdSetor( getIdSetor() );
            view.setIdQuestionario( getIdQuestionario() );

            return view;
        }

    }
}
