package com.illuminare.entity;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.illuminare.dao.GenericDAO;
import com.illuminare.view.QuestaoOrdemView;


@Entity
@Table( name = "questao_ordem" )
public class QuestaoOrdem extends GenericEntity implements Comparable< QuestaoOrdem > {

    @ManyToOne
    @JoinColumn( name = "id_questao" )
    private Questao questao;

    @Column( name = "ordem" )
    private Integer ordem;


    public Questao getQuestao() {

        return questao;
    }


    public void setQuestao( Questao questao ) {

        this.questao = questao;
    }


    public Integer getOrdem() {

        return ordem;
    }


    public void setOrdem( Integer ordem ) {

        this.ordem = ordem;
    }


    @Override
    public String toString() {

        return "QuestaoOrdem [questao=" + questao + ", ordem=" + ordem + "]";
    }


    @Override
    public int compareTo( QuestaoOrdem other ) {

        if ( this.ordem == null || other == null ) {
            return -1;
        }
        if ( this.ordem < other.ordem ) {
            return -1;
        }
        if ( this.ordem > other.ordem ) {
            return 1;
        }
        return 0;
    }

    // implementar o equals comparar por id questao


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( !( o instanceof QuestaoOrdem ) )
            return false;
        if ( !super.equals( o ) )
            return false;
        QuestaoOrdem that = (QuestaoOrdem) o;
        return Objects.equals( getQuestao(), that.getQuestao() );
    }


    @Override
    public int hashCode() {

        return Objects.hash( super.hashCode(), getQuestao() );
    }


    public void unproxy( GenericDAO dao ) {

        dao.unproxyInitialize( getQuestao() );

        getQuestao().unproxy( dao );
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public QuestaoOrdemView format() {

            QuestaoOrdemView view = new QuestaoOrdemView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            if ( getQuestao() != null ) {
                view.setQuestao( getQuestao().formatToWebPage().format() );
            }
            view.setOrdem( getOrdem() );

            return view;
        }

    }
}
