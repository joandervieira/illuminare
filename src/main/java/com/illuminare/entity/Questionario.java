package com.illuminare.entity;


import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.illuminare.dao.GenericDAO;
import com.illuminare.view.CargoView;
import com.illuminare.view.GrupoOcupacionalView;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.QuestionarioView;
import com.illuminare.view.SetorView;


@Entity
@Table( name = "questionario" )
public class Questionario extends GenericEntity {

    @Column( name = "nome", length = 100 )
    private String nome;

    @Column( name = "obs", length = 4000 )
    private String obs;

    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable( name = "questionario_grupo_questao", joinColumns = { @JoinColumn( name = "id_questionario" ) },
        inverseJoinColumns = { @JoinColumn( name = "id_grupo_questao" ) } )
    // private List<GrupoQuestao> gruposQuestoes ;
    private Set< GrupoQuestao > gruposQuestoes = new LinkedHashSet< GrupoQuestao >();

    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable( name = "questionario_grupo_ocupacional", joinColumns = { @JoinColumn( name = "id_questionario" ) },
        inverseJoinColumns = { @JoinColumn( name = "id_grupo_ocupacional" ) } )
    // private List<GrupoOcupacional> gruposOcupacionais;
    private Set< GrupoOcupacional > gruposOcupacionais = new LinkedHashSet< GrupoOcupacional >();

    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable( name = "questionario_cargo", joinColumns = { @JoinColumn( name = "id_questionario" ) },
        inverseJoinColumns = { @JoinColumn( name = "id_cargo" ) } )
    private Set< Cargo > cargos = new LinkedHashSet<>();

    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable( name = "questionario_setor", joinColumns = { @JoinColumn( name = "id_questionario" ) },
        inverseJoinColumns = { @JoinColumn( name = "id_setor" ) } )
    private Set< Setor > setores = new LinkedHashSet<>();

    @Transient
    private Double mediaPropria;

    @Transient
    private Double mediaLideres;

    @Transient
    private Double mediaSubordinados;

    @Transient
    private Double mediaOutros;

    @Transient
    private String pesquisaNome;

    @Transient
    private Long pesquisaId;

    @Transient
    private String avaliadoNome;

    @Transient
    private Long avaliadoId;

    @Transient
    private String avaliadorNome;

    @Transient
    private Long avaliadorId;

    @Transient
    private Double meta;

    @Transient
    private Double valorObtido;

    @Transient
    private Double percentualMeta;

    @Transient
    private List< Resposta > respostasProprias = new ArrayList<>();

    @Transient
    private List< Resposta > respostasLider = new ArrayList<>();

    @Transient
    private List< Resposta > respostasSubordinados = new ArrayList<>();

    @Transient
    private List< Resposta > respostasOutros = new ArrayList<>();


    public Questionario() {

    }


    public Questionario( Long id ) {

        setId( id );
    }

    public static Set< Questionario > copy( Set< Questionario > oldQuestionarios ) {

        Set< Questionario > newQuestionarios = new LinkedHashSet<>();
        if ( isNotEmpty( oldQuestionarios ) ) {
            for ( Questionario oldQuestionario : oldQuestionarios ) {
                newQuestionarios.add( oldQuestionario.copy() );
            }

        }
        return newQuestionarios;
    }

    public Double getMeta() {

        return meta;
    }

    public void setMeta( Double meta ) {

        this.meta = meta;
    }

    public Double getValorObtido() {

        return valorObtido;
    }

    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }

    public Double getPercentualMeta() {

        return percentualMeta;
    }

    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }

    public String getNome() {

        return nome;
    }

    public void setNome( String nome ) {

        this.nome = nome;
    }

    public String getObs() {

        return obs;
    }

    public void setObs( String obs ) {

        this.obs = obs;
    }

    public Set< GrupoQuestao > getGruposQuestoes() {

        return gruposQuestoes;
    }

    public void setGruposQuestoes( Set< GrupoQuestao > gruposQuestoes ) {

        this.gruposQuestoes = gruposQuestoes;
    }

    public Set< GrupoOcupacional > getGruposOcupacionais() {

        return gruposOcupacionais;
    }

    public void setGruposOcupacionais( Set< GrupoOcupacional > gruposOcupacionais ) {

        this.gruposOcupacionais = gruposOcupacionais;
    }

    public Double getMediaPropria() {

        return mediaPropria;
    }

    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }

    public Double getMediaLideres() {

        return mediaLideres;
    }

    public void setMediaLideres( Double mediaLideres ) {

        this.mediaLideres = mediaLideres;
    }

    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }

    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }

    public Double getMediaOutros() {

        return mediaOutros;
    }

    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }

    public String getPesquisaNome() {

        return pesquisaNome;
    }

    public void setPesquisaNome( String pesquisaNome ) {

        this.pesquisaNome = pesquisaNome;
    }

    public Long getPesquisaId() {

        return pesquisaId;
    }

    public void setPesquisaId( Long pesquisaId ) {

        this.pesquisaId = pesquisaId;
    }

    public String getAvaliadoNome() {

        return avaliadoNome;
    }

    public void setAvaliadoNome( String avaliadoNome ) {

        this.avaliadoNome = avaliadoNome;
    }

    public Long getAvaliadoId() {

        return avaliadoId;
    }

    public void setAvaliadoId( Long avaliadoId ) {

        this.avaliadoId = avaliadoId;
    }

    public String getAvaliadorNome() {

        return avaliadorNome;
    }

    public void setAvaliadorNome( String avaliadorNome ) {

        this.avaliadorNome = avaliadorNome;
    }

    public Long getAvaliadorId() {

        return avaliadorId;
    }

    public void setAvaliadorId( Long avaliadorId ) {

        this.avaliadorId = avaliadorId;
    }

    @Override
    public String toString() {

        return "Questionario [nome=" + nome + ", getId()=" + getId() + "]";
    }

    public Set< Cargo > getCargos() {

        return cargos;
    }

    public void setCargos( Set< Cargo > cargos ) {

        this.cargos = cargos;
    }

    public Set< Setor > getSetores() {

        return setores;
    }

    public void setSetores( Set< Setor > setores ) {

        this.setores = setores;
    }

    public void unproxy( GenericDAO dao ) {

        dao.unproxyInitialize( getGruposOcupacionais() );
        dao.unproxyInitialize( getGruposQuestoes() );
        dao.unproxyInitialize( getCargos() );
        dao.unproxyInitialize( getSetores() );

    }

    public List< Resposta > getRespostasProprias() {

        return respostasProprias;
    }

    public void setRespostasProprias( List< Resposta > respostasProprias ) {

        this.respostasProprias = respostasProprias;
    }

    public List< Resposta > getRespostasLider() {

        return respostasLider;
    }

    public void setRespostasLider( List< Resposta > respostasLider ) {

        this.respostasLider = respostasLider;
    }

    public List< Resposta > getRespostasSubordinados() {

        return respostasSubordinados;
    }

    public void setRespostasSubordinados( List< Resposta > respostasSubordinados ) {

        this.respostasSubordinados = respostasSubordinados;
    }

    public List< Resposta > getRespostasOutros() {

        return respostasOutros;
    }

    public void setRespostasOutros( List< Resposta > respostasOutros ) {

        this.respostasOutros = respostasOutros;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }


    public Questionario copy() {

        Questionario newEntity = new Questionario();
        BeanUtils.copyProperties( this, newEntity );
        return newEntity;
    }

    public class FormatBuilder {

        private boolean excludeGruposQuestoes = false;
        private boolean excludeGruposOcupacionais = false;
        private boolean excludeCargos = false;
        private boolean excludeSetores = false;

        public QuestionarioView format() {

            QuestionarioView view = new QuestionarioView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setNome( getNome() );
            view.setObs( getObs() );
            if ( !excludeGruposQuestoes ) {
                view.setGruposQuestoes( getGruposQuestoesView() );
            }
            if ( !excludeGruposOcupacionais ) {
                view.setGruposOcupacionais( getGruposOcupacionaisView() );
            }
            if ( !excludeCargos ) {
                view.setCargos( getCargosView() );
            }
            if ( !excludeSetores ) {
                view.setSetores( getSetoresView() );
            }
            view.setMediaPropria( getMediaPropria() );
            view.setMediaLideres( getMediaLideres() );
            view.setMediaSubordinados( getMediaSubordinados() );
            view.setMediaOutros( getMediaOutros() );
            view.setPesquisaNome( getPesquisaNome() );
            view.setPesquisaId( getPesquisaId() );
            view.setAvaliadoNome( getAvaliadoNome() );
            view.setAvaliadoId( getAvaliadoId() );
            view.setAvaliadorNome( getAvaliadorNome() );
            view.setAvaliadorId( getAvaliadorId() );
            view.setMeta( getMeta() );
            view.setValorObtido( getValorObtido() );
            view.setPercentualMeta( getPercentualMeta() );

            return view;
        }

        private Set< SetorView > getSetoresView() {

            if ( getSetores() != null ) {
                Set< SetorView > views = new LinkedHashSet<>();
                for ( Setor setor : getSetores() ) {
                    views.add( setor.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

        private Set< CargoView > getCargosView() {

            if ( getCargos() != null ) {
                Set< CargoView > views = new LinkedHashSet<>();
                for ( Cargo cargo : getCargos() ) {
                    views.add( cargo.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

        private Set< GrupoOcupacionalView > getGruposOcupacionaisView() {

            if ( getGruposOcupacionais() != null ) {
                Set< GrupoOcupacionalView > views = new LinkedHashSet<>();
                for ( GrupoOcupacional grupoOcupacional : getGruposOcupacionais() ) {
                    views.add( grupoOcupacional.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

        private Set< GrupoQuestaoView > getGruposQuestoesView() {

            if ( getGruposQuestoes() != null ) {
                Set< GrupoQuestaoView > views = new LinkedHashSet<>();
                for ( GrupoQuestao grupoQuestao : getGruposQuestoes() ) {
                    views.add( grupoQuestao.formatToWebPage().format() );
                }
                return views;
            }
            return null;
        }

        public FormatBuilder excludeGruposQuestoes() {

            excludeGruposQuestoes = true;
            return this;
        }


        public FormatBuilder excludeGruposOcupacionais() {

            excludeGruposOcupacionais = true;
            return this;
        }


        public FormatBuilder excludeCargos() {

            excludeCargos = true;
            return this;
        }


        public FormatBuilder excludeSetores() {

            excludeSetores = true;
            return this;
        }

    }
}
