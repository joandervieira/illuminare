package com.illuminare.entity;


import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.illuminare.dao.GenericDAO;
import com.illuminare.view.QuestionarioAvaliacaoView;
import com.illuminare.view.QuestionarioRespondido;
import com.illuminare.view.RespostaView;


@Entity
@Table( name = "questionario_avaliacao" )
public class QuestionarioAvaliacao extends GenericEntity {

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_pesquisa" )
    private Pesquisa pesquisa;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_questionario" )
    private Questionario questionario;

    @Column( name = "id_questionario", updatable = false, insertable = false )
    private Long idQuestionario;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_funcionario_avaliado" )
    private Funcionario funcionarioAvaliado;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_funcionario_avaliador" )
    private Funcionario funcionarioAvaliador;

    @Column( name = "auto_avaliacao" )
    private Boolean autoAvaliacao;

    @Column( name = "avaliacao_subordinado" )
    private Boolean avaliacaoSubordinado;

    @Column( name = "avaliacao_superior" )
    private Boolean avaliacaoSuperior;

    @OneToMany( mappedBy = "questionarioAvaliacao" )
    private List< Resposta > respostas;

    @Transient
    private Boolean respondido;

    @Transient
    private QuestionarioRespondido questionarioRespondido;

    public static List< QuestionarioAvaliacao > copy( List< QuestionarioAvaliacao > oldQuestionarios ) {

        List< QuestionarioAvaliacao > newQuestionarios = new ArrayList();
        if ( isNotEmpty( oldQuestionarios ) ) {
            for ( QuestionarioAvaliacao oldQuestionario : oldQuestionarios ) {
                newQuestionarios.add( oldQuestionario.copy() );
            }

        }
        return newQuestionarios;
    }

    public Questionario getQuestionario() {

        return questionario;
    }

    public void setQuestionario( Questionario questionario ) {

        this.questionario = questionario;
    }

    public Funcionario getFuncionarioAvaliado() {

        return funcionarioAvaliado;
    }

    public void setFuncionarioAvaliado( Funcionario funcionarioAvaliado ) {

        this.funcionarioAvaliado = funcionarioAvaliado;
    }

    public Funcionario getFuncionarioAvaliador() {

        return funcionarioAvaliador;
    }


    /*
     * @Override public int hashCode() { final int prime = 31; int result =
     * super.hashCode(); result = prime * result + ((funcionarioAvaliado == null) ?
     * 0 : funcionarioAvaliado.hashCode()); result = prime * result +
     * ((funcionarioAvaliador == null) ? 0 : funcionarioAvaliador.hashCode());
     * result = prime * result + ((pesquisa == null) ? 0 : pesquisa.hashCode());
     * result = prime * result + ((questionario == null) ? 0 :
     * questionario.hashCode()); return result; }
     * 
     * @Override public boolean equals(Object obj) { if (this == obj) return true;
     * if (!super.equals(obj)) return false; if (getClass() != obj.getClass())
     * return false; QuestionarioAvaliacao other = (QuestionarioAvaliacao) obj; if
     * (funcionarioAvaliado == null) { if (other.funcionarioAvaliado != null) return
     * false; } else if (!funcionarioAvaliado.equals(other.funcionarioAvaliado))
     * return false; if (funcionarioAvaliador == null) { if
     * (other.funcionarioAvaliador != null) return false; } else if
     * (!funcionarioAvaliador.equals(other.funcionarioAvaliador)) return false; if
     * (pesquisa == null) { if (other.pesquisa != null) return false; } else if
     * (!pesquisa.equals(other.pesquisa)) return false; if (questionario == null) {
     * if (other.questionario != null) return false; } else if
     * (!questionario.equals(other.questionario)) return false; return true; }
     */

    public void setFuncionarioAvaliador( Funcionario funcionarioAvaliador ) {

        this.funcionarioAvaliador = funcionarioAvaliador;
    }

    public Pesquisa getPesquisa() {

        return pesquisa;
    }

    public void setPesquisa( Pesquisa pesquisa ) {

        this.pesquisa = pesquisa;
    }

    @Override
    public String toString() {

        String idAvaliador = "", nomeAvaliador = "", idAvaliado = "", nomeAvaliado = "", idPesquisa = "", idQuestionario = "";
        if ( getFuncionarioAvaliador() != null ) {
            idAvaliador = "" + getFuncionarioAvaliador().getId();
            if ( getFuncionarioAvaliador().getPessoa() != null ) {
                nomeAvaliador = getFuncionarioAvaliador().getPessoa().getNome();
            }
        }
        if ( getFuncionarioAvaliador() != null ) {
            idAvaliado = "" + getFuncionarioAvaliado().getId();
            if ( getFuncionarioAvaliado().getPessoa() != null ) {
                nomeAvaliado = getFuncionarioAvaliado().getPessoa().getNome();
            }
        }
        if ( getPesquisa() != null ) {
            idPesquisa = "" + getPesquisa().getId();
        }
        if ( getQuestionario() != null ) {
            idQuestionario = "" + getQuestionario().getId();
        }

        return String.format(
            "Funcionário [%s-%s] avaliando [%s-%s] -- [pesquisa:%s, questionario:%s]",
            idAvaliador,
            nomeAvaliador,
            idAvaliado,
            nomeAvaliado,
            idPesquisa,
            idQuestionario );

    }

    public List< Resposta > getRespostas() {

        return respostas;
    }

    public void setRespostas( List< Resposta > respostas ) {

        this.respostas = respostas;
    }

    public Boolean getRespondido() {

        return respondido;
    }

    public void setRespondido( Boolean respondido ) {

        this.respondido = respondido;
    }

    public Boolean getAutoAvaliacao() {

        if ( autoAvaliacao == null ) {
            autoAvaliacao = false;
        }

        return autoAvaliacao;
    }

    public void setAutoAvaliacao( Boolean autoAvaliacao ) {

        this.autoAvaliacao = autoAvaliacao;
    }

    public Boolean getAvaliacaoSubordinado() {

        if ( avaliacaoSubordinado == null ) {
            avaliacaoSubordinado = false;
        }

        return avaliacaoSubordinado;
    }

    public void setAvaliacaoSubordinado( Boolean avaliacaoSubordinado ) {

        this.avaliacaoSubordinado = avaliacaoSubordinado;
    }

    public Boolean getAvaliacaoSuperior() {

        if ( avaliacaoSuperior == null ) {
            avaliacaoSuperior = false;
        }

        return avaliacaoSuperior;
    }

    public void setAvaliacaoSuperior( Boolean avaliacaoSuperior ) {

        this.avaliacaoSuperior = avaliacaoSuperior;
    }

    public QuestionarioRespondido getQuestionarioRespondido() {

        return questionarioRespondido;
    }

    public void setQuestionarioRespondido( QuestionarioRespondido questionarioRespondido ) {

        this.questionarioRespondido = questionarioRespondido;
    }

    public void unproxy( GenericDAO dao ) {

        dao.unproxyInitialize( getRespostas() );
        dao.unproxyInitialize( getFuncionarioAvaliado() );
        dao.unproxyInitialize( getFuncionarioAvaliador() );
        dao.unproxyInitialize( getPesquisa() );
        dao.unproxyInitialize( getQuestionario() );
        getQuestionario().unproxy( dao );

    }

    public Long getIdQuestionario() {

        return idQuestionario;
    }

    public void setIdQuestionario( Long idQuestionario ) {

        this.idQuestionario = idQuestionario;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }


    public QuestionarioAvaliacao copy() {

        QuestionarioAvaliacao newEntity = new QuestionarioAvaliacao();
        BeanUtils.copyProperties( this, newEntity );
        return newEntity;
    }

    public class FormatBuilder {

        public QuestionarioAvaliacaoView format() {

            QuestionarioAvaliacaoView view = new QuestionarioAvaliacaoView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            if ( getPesquisa() != null ) {
                view.setPesquisa( getPesquisa().formatToWebPage().excludeQuestionarios().format() );
            }
            if ( getQuestionario() != null ) {
                view.setQuestionario( getQuestionario().formatToWebPage().excludeCargos().excludeGruposOcupacionais().excludeSetores().format() );
            }
            if ( getFuncionarioAvaliado() != null ) {
                view.setFuncionarioAvaliado( getFuncionarioAvaliado().formatToWebPage().format() );
            }
            if ( getFuncionarioAvaliador() != null ) {
                view.setFuncionarioAvaliador( getFuncionarioAvaliador().formatToWebPage().format() );
            }
            view.setAutoAvaliacao( getAutoAvaliacao() );
            view.setAvaliacaoSubordinado( getAvaliacaoSubordinado() );
            view.setAvaliacaoSuperior( getAvaliacaoSuperior() );
            view.setRespostas( getRespostasView() );
            view.setRespondido( getRespondido() );
            view.setQuestionarioRespondido( getQuestionarioRespondido() );

            return view;
        }


        private List< RespostaView > getRespostasView() {

            if ( getRespostas() != null ) {
                List< RespostaView > views = new ArrayList<>();
                for ( Resposta resposta : getRespostas() ) {
                    views.add( resposta.formatToWebPage().excludeQuestionarioAvaliacao().excludeQuestao().format() );
                }
                return views;
            }
            return null;
        }

    }
}
