package com.illuminare.entity;


import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.illuminare.dao.GenericDAO;
import com.illuminare.util.NumberUtil;
import com.illuminare.view.RespostaView;


@Entity
@Table( name = "resposta" )
public class Resposta extends GenericEntity {

    @Column( name = "texto_resposta", length = 4000 )
    private String respostaTexto;

    @Column( name = "resposta_numerico" )
    private Double respostaNumerico;

    @ManyToOne( fetch = FetchType.EAGER )
    @JoinColumn( name = "id_valor_lista_resposta" )
    private ValorLista respostaValorLista;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_questao" )
    private Questao questao;

    @Column( name = "id_questao", insertable = false, updatable = false )
    private Long idQuestao;

    @Column( name = "id_grupo_questao" )
    private Long idGrupoQuestao;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_questionario_avaliacao" )
    private QuestionarioAvaliacao questionarioAvaliacao;


    /*
     * @ManyToOne(fetch=FetchType.LAZY)
     *
     * @JoinColumn(name="id_questionario") private Questionario questionario;
     */

    public static List< Resposta > copy( List< Resposta > oldEntities ) {

        List< Resposta > newEntities = new ArrayList<>();
        if ( isNotEmpty( oldEntities ) ) {
            for ( Resposta oldEntity : oldEntities ) {
                newEntities.add( oldEntity.copy() );
            }

        }
        return newEntities;
    }

    public String getRespostaTexto() {

        return respostaTexto;
    }

    public void setRespostaTexto( String respostaTexto ) {

        this.respostaTexto = respostaTexto;
    }

    public ValorLista getRespostaValorLista() {

        return respostaValorLista;
    }

    public void setRespostaValorLista( ValorLista respostaValorLista ) {

        this.respostaValorLista = respostaValorLista;
    }

    public Questao getQuestao() {

        return questao;
    }

    public void setQuestao( Questao questao ) {

        this.questao = questao;
    }

    public QuestionarioAvaliacao getQuestionarioAvaliacao() {

        return questionarioAvaliacao;
    }

    public void setQuestionarioAvaliacao( QuestionarioAvaliacao questionarioAvaliacao ) {

        this.questionarioAvaliacao = questionarioAvaliacao;
    }

    public Double getRespostaNumerico() {

        return respostaNumerico;
    }

    public void setRespostaNumerico( Double respostaNumerico ) {

        this.respostaNumerico = respostaNumerico;
    }

    @Override
    public String toString() {

        return "Resposta{" + "respostaTexto='" + respostaTexto + '\'' + ", respostaNumerico=" + respostaNumerico + ", respostaValorLista=" + respostaValorLista
            + ", questao=" + questao + ", questionarioAvaliacao=" + questionarioAvaliacao + '}';
    }

    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }

    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }

    public void unproxy( GenericDAO dao ) {

        dao.unproxyInitialize( getRespostaValorLista() );
        dao.unproxyInitialize( getQuestao() );
        dao.unproxyInitialize( getQuestionarioAvaliacao() );

    }

    public Long getIdQuestao() {

        return idQuestao;
    }

    public void setIdQuestao( Long idQuestao ) {

        this.idQuestao = idQuestao;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }


    public Double getValorNumerico() {

        if ( getRespostaValorLista() != null && NumberUtil.notNullNumber( getRespostaValorLista().getValor() ) ) {
            return NumberUtil.getDouble( getRespostaValorLista().getValor() );
        }
        if ( NumberUtil.notNull( getRespostaNumerico() ) ) {
            return getRespostaNumerico();
        }
        return null;
    }


    public String getValorTexto() {

        if ( isNotBlank( getRespostaTexto() ) ) {
            return getRespostaTexto();
        }
        return null;
    }


    public Resposta copy() {

        Resposta newEntity = new Resposta();
        BeanUtils.copyProperties( this, newEntity );
        return newEntity;
    }

    public class FormatBuilder {

        private boolean excludeRespostaValorLista = false;
        private boolean excludeQuestionarioAvaliacao = false;
        private boolean excludeQuestao = false;

        public RespostaView format() {

            RespostaView view = new RespostaView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setRespostaTexto( getRespostaTexto() );
            view.setRespostaNumerico( getRespostaNumerico() );
            view.setIdGrupoQuestao( getIdGrupoQuestao() );
            if ( !excludeRespostaValorLista ) {
                if ( getRespostaValorLista() != null ) {
                    view.setRespostaValorLista( getRespostaValorLista().formatToWebPage().format() );
                }
            }
            if ( !excludeQuestao ) {
                if ( getQuestao() != null ) {
                    view.setQuestao( getQuestao().formatToWebPage().format() );
                }
            }
            if ( !excludeQuestionarioAvaliacao ) {
                if ( getQuestionarioAvaliacao() != null ) {
                    view.setQuestionarioAvaliacao( getQuestionarioAvaliacao().formatToWebPage().format() );
                }
            }

            return view;
        }

        public FormatBuilder excludeRespostaValorLista() {

            excludeRespostaValorLista = true;
            return this;
        }


        public FormatBuilder excludeQuestionarioAvaliacao() {

            excludeQuestionarioAvaliacao = true;
            return this;
        }


        public FormatBuilder excludeQuestao() {

            excludeQuestao = true;
            return this;
        }

    }
}
