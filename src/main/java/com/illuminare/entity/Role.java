package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "role" )
public class Role extends GenericEntity {

    public static final Role ROLE_ROOT = new Role( "role_root" );

    public static final Role ROLE_USER = new Role( "role_user" );

    public static final Role ROLE_ADMIN = new Role( "role_admin" );

    public static final Role ROLE_FUNCIONARIO = new Role( "role_funcionario" );

    @Column( name = "name", unique = true, nullable = false )
    private String name;

    @Column( name = "nome_exibir" )
    private String nomeExibir;


    public Role() {

    }


    public Role( String name ) {

        setName( name );
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public String getNomeExibir() {

        return nomeExibir;
    }


    public void setNomeExibir( String nomeExibir ) {

        this.nomeExibir = nomeExibir;
    }


    @Override
    public String toString() {

        return "Role [id=" + getId() + ", name=" + name + "]";
    }

}
