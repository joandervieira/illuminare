package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.illuminare.view.SetorView;


@Entity
@Table( name = "setor" )
public class Setor extends GenericEntity {

    @Column( name = "nome" )
    private String nome;

    @Column( name = "descricao" )
    private String descricao;

    @Column( name = "obs", length = 1000 )
    private String obs;


    public Setor() {

    }


    public Setor( String nome ) {

        setNome( nome );
    }


    public Setor( String nome, Boolean ativo ) {

        setNome( nome );
        setAtivo( ativo );
    }


    public Setor( Long id ) {

        setId( id );
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDescricao() {

        return descricao;
    }


    public void setDescricao( String descricao ) {

        this.descricao = descricao;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
        result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Setor other = (Setor) obj;
        if ( getId() == null ) {
            if ( other.getId() != null )
                return false;
        } else if ( !getId().equals( other.getId() ) )
            return false;
        if ( nome == null ) {
            if ( other.nome != null )
                return false;
        } else if ( !nome.equals( other.nome ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Setor [nome=" + nome + ", getId()=" + getId() + "]";
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public SetorView format() {

            SetorView view = new SetorView();

            view.setId( getId() );
            view.setNome( getNome() );
            view.setDescricao( getDescricao() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setObs( getObs() );

            return view;
        }

    }
}
