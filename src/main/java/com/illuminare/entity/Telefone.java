package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table( name = "telefone" )
public class Telefone extends GenericEntity {

    @Column( name = "numero" )
    private String numero;

    @Column( name = "ddd" )
    private String ddd;

    @Column( name = "ramal" )
    private String ramal;

    @Column( name = "obs", length = 1000 )
    private String obs;

    @ManyToOne
    @JoinColumn( name = "id_telefone" )
    private TipoTelefone tipoTelefone;


    public String getNumero() {

        return numero;
    }


    public void setNumero( String numero ) {

        this.numero = numero;
    }


    public String getDdd() {

        return ddd;
    }


    public void setDdd( String ddd ) {

        this.ddd = ddd;
    }


    public String getRamal() {

        return ramal;
    }


    public void setRamal( String ramal ) {

        this.ramal = ramal;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public TipoTelefone getTipoTelefone() {

        return tipoTelefone;
    }


    public void setTipoTelefone( TipoTelefone tipoTelefone ) {

        this.tipoTelefone = tipoTelefone;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( ddd == null ) ? 0 : ddd.hashCode() );
        result = prime * result + ( ( numero == null ) ? 0 : numero.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Telefone other = (Telefone) obj;
        if ( ddd == null ) {
            if ( other.ddd != null )
                return false;
        } else if ( !ddd.equals( other.ddd ) )
            return false;
        if ( numero == null ) {
            if ( other.numero != null )
                return false;
        } else if ( !numero.equals( other.numero ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Telefone [numero=" + numero + ", getId()=" + getId() + "]";
    }

}
