package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "tipo_endereco" )
public class TipoEndereco extends GenericEntity {

    @Column( name = "tipo" )
    private String tipo;

    @Column( name = "obs", length = 1000 )
    private String obs;


    public TipoEndereco() {

    }


    public TipoEndereco( String tipo ) {

        setTipo( tipo );
    }


    public TipoEndereco( String tipo, Boolean ativo ) {

        setTipo( tipo );
        setAtivo( ativo );
    }


    public String getTipo() {

        return tipo;
    }


    public void setTipo( String tipo ) {

        this.tipo = tipo;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public String toString() {

        return "TipoEndereco [tipo=" + tipo + ", getId()=" + getId() + "]";
    }

}
