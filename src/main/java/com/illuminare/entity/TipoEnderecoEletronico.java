package com.illuminare.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "tipo_endereco_eletronico" )
public class TipoEnderecoEletronico extends GenericEntity {

    @Column( name = "tipo" )
    private String tipo;

    @Column( name = "obs", length = 1000 )
    private String obs;


    public TipoEnderecoEletronico() {

    }


    public TipoEnderecoEletronico( String tipo ) {

        setTipo( tipo );
    }


    public TipoEnderecoEletronico( String tipo, Boolean ativo ) {

        setTipo( tipo );
        setAtivo( ativo );
    }


    public String getTipo() {

        return tipo;
    }


    public void setTipo( String tipo ) {

        this.tipo = tipo;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public String toString() {

        return "TipoEnderecoEletronico [tipo=" + tipo + ", getId()=" + getId() + "]";
    }

}
