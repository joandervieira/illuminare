package com.illuminare.entity;


import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.util.AppStringUtils;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;


@Entity
@Table( name = "user" )
public class User extends GenericEntity implements UserDetails {

    /**
     *
     */
    private static final long serialVersionUID = -1483955829057616212L;

    /*** SPRING SECURITY **/
    @Column( name = "name", unique = true, nullable = false )
    private String name;

    @Column( name = "password", nullable = false )
    private String password;

    // @ElementCollection(fetch = FetchType.EAGER)
    @ManyToMany( fetch = FetchType.EAGER )
    @JoinTable( name = "user_role", //
        joinColumns = { @JoinColumn( name = "user_id", nullable = false ) }, //
        inverseJoinColumns = { @JoinColumn( name = "role_id", nullable = false ) } )
    private Set< Role > roles;

    /*** FIM SPRING SECURITY **/

    @Column( name = "nome_exibir" )
    private String nomeExibir;

    @Column( name = "senha_atualizada" )
    private Boolean senhaAtualizada = false;

    @Column( name = "email" )
    private String email;

    @Column( name = "ultimo_acesso" )
    private Date ultimoAcesso;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_funcionario" )
    private Funcionario funcionario;

    @Transient
    private String tipoUser;


    public User() {

        /* Reflection instantiation */
    }


    public User( String name, String passwordHash ) {

        this.name = name;
        this.password = passwordHash;
    }


    /*** SPRING SECURITY **/

    public String getName() {

        return this.name;
    }


    public void setName( String name ) {

        // this.name = AppStringUtils.capitalizeFully(name);
        this.name = name;
    }


    public Set< Role > getRoles() {

        return this.roles;
    }


    public void setRoles( Set< Role > roles ) {

        this.roles = roles;
    }


    public Boolean getSenhaAtualizada() {

        return senhaAtualizada;
    }


    public void setSenhaAtualizada( Boolean senhaAtualizada ) {

        this.senhaAtualizada = senhaAtualizada;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail( String email ) {

        this.email = StringUtils.deleteWhitespace( StringUtils.lowerCase( email ) );
    }


    @Override
    public String getPassword() {

        return this.password;
    }


    public void setPassword( String password ) {

        if ( password != null ) {
            this.password = AppStringUtils.normalizeSpace( password );
        } else {
            this.password = password;
        }
    }


    @JsonIgnore
    @Override
    public Collection< ? extends GrantedAuthority > getAuthorities() {

        Set< Role > roles = this.getRoles();

        if ( roles == null ) {
            return Collections.emptyList();
        }

        Set< GrantedAuthority > authorities = new HashSet< GrantedAuthority >();
        for ( Role role : roles ) {
            authorities.add( new SimpleGrantedAuthority( role.getName() ) );
        }

        return authorities;
    }


    @Override
    public String getUsername() {

        return this.name;
    }


    @Override
    public boolean isAccountNonExpired() {

        return true;
    }


    @Override
    public boolean isAccountNonLocked() {

        return true;
    }


    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }


    @Override
    public boolean isEnabled() {

        return true;
    }


    /*** FIM SPRING SECURITY **/

    public String getNomeExibir() {

        return nomeExibir;
    }


    public void setNomeExibir( String nomeExibir ) {

        // this.nomeExibir = nomeExibir;
        this.nomeExibir = AppStringUtils.capitalizeFully( nomeExibir );
    }


    /*** FUNCOES AUXILIARES **/

    public void addRole( Role role ) {

        if ( this.roles == null ) {
            this.roles = new HashSet< Role >();
        }
        this.roles.add( role );
    }


    public Boolean containsRole( String role ) {

        Set< Role > roles = getRoles();
        if ( role != null && roles != null ) {
            for ( Role r : roles ) {
                if ( r.getName().equals( role ) ) {
                    return true;
                }
            }
        }
        return false;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getUltimoAcesso() {

        return ultimoAcesso;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setUltimoAcesso( Date ultimoAcesso ) {

        this.ultimoAcesso = ultimoAcesso;
    }


    public Funcionario getFuncionario() {

        return funcionario;
    }


    public void setFuncionario( Funcionario funcionario ) {

        this.funcionario = funcionario;
    }


    public String getTipoUser() {

        return tipoUser;
    }


    public void setTipoUser( String tipoUser ) {

        this.tipoUser = tipoUser;
    }


    @Override
    public String toString() {

        return "User [name=" + name + ", getId()=" + getId() + "]";
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        User other = (User) obj;
        if ( name == null ) {
            if ( other.name != null )
                return false;
        } else if ( !name.equals( other.name ) )
            return false;
        return true;
    }

}