package com.illuminare.entity;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.illuminare.view.ValorListaView;


@Entity
@Table( name = "valor_lista" )
public class ValorLista extends GenericEntity implements Comparable< ValorLista > {

    @Column( name = "nome", length = 2000 )
    private String nome;

    @Column( name = "valor" )
    private String valor;

    @Column( name = "ordem" )
    private Integer ordem;


    public static void main( String[] args ) {

        ValorLista v1 = new ValorLista();
        v1.setOrdem( 0 );
        v1.setNome( "teste1" );

        ValorLista v2 = new ValorLista();
        v2.setOrdem( 1 );
        v2.setNome( "teste2" );

        ValorLista v3 = new ValorLista();
        v3.setOrdem( 2 );
        v3.setNome( "teste3" );

        Set< ValorLista > l = new HashSet< ValorLista >();
        l.add( v2 );
        l.add( v1 );
        l.add( v3 );
        for ( ValorLista v : l ) {
            System.out.println( v.getOrdem() + "=" + v.getNome() );
        }
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getValor() {

        return valor;
    }


    public void setValor( String valor ) {

        this.valor = valor;
    }


    public Integer getOrdem() {

        return ordem;
    }


    public void setOrdem( Integer ordem ) {

        this.ordem = ordem;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
        result = prime * result + ( ( valor == null ) ? 0 : valor.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        ValorLista other = (ValorLista) obj;
        if ( nome == null ) {
            if ( other.nome != null )
                return false;
        } else if ( !nome.equals( other.nome ) )
            return false;
        if ( valor == null ) {
            if ( other.valor != null )
                return false;
        } else if ( !valor.equals( other.valor ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "ValorLista [nome=" + nome + ", valor=" + valor + ", ordem=" + ordem + ", getId()=" + getId() + "]";
    }


    @Override
    public int compareTo( ValorLista other ) {

        if ( this.ordem == null || other == null ) {
            return -1;
        }
        if ( this.ordem < other.ordem ) {
            return -1;
        }
        if ( this.ordem > other.ordem ) {
            return 1;
        }
        return 0;
    }

    public FormatBuilder formatToWebPage() {

        return new FormatBuilder();
    }

    public class FormatBuilder {

        public ValorListaView format() {

            ValorListaView view = new ValorListaView();

            view.setId( getId() );
            view.setIdEmpresa( getIdEmpresa() );
            view.setNome( getNome() );
            view.setValor( getValor() );
            view.setOrdem( getOrdem() );

            return view;
        }

    }

}
