package com.illuminare.enums;


public enum RoleEnum {

    ADMIN( "role_admin" ),
    FUNCIONARIO( "role_funcionario" ),
    GESTOR_RH( "role_gestor" ),
    GESTOR_SETOR( "role_gestor_setor" );

    private String name;


    RoleEnum( String name ) {

        this.name = name;
    }


    public String getName() {

        return this.name;
    }
}
