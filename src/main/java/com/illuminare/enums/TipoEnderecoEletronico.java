package com.illuminare.enums;


public enum TipoEnderecoEletronico {

    EMAIL( "E-mail" );

    private String tipo;


    private TipoEnderecoEletronico( String tipo ) {

        this.tipo = tipo;
    }


    public String getTipo() {

        return this.tipo;
    }
}
