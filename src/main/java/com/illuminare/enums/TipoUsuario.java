package com.illuminare.enums;


public enum TipoUsuario {

    ADMIN( "admin" ),
    FUNCIONARIO( "funcionario" ),
    GESTOR_RH( "gestor" ),
    GESTOR_SETOR( "gestor_setor" );

    private String tipo;


    private TipoUsuario( String tipo ) {

        this.tipo = tipo;
    }


    public String getTipo() {

        return this.tipo;
    }
}
