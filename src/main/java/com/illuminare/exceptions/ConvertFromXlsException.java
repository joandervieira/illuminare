package com.illuminare.exceptions;


public class ConvertFromXlsException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -1901819158774753728L;


    public ConvertFromXlsException() {

    }


    public ConvertFromXlsException( String message ) {

        super( message );
    }


    public ConvertFromXlsException( Throwable cause ) {

        super( cause );
    }


    public ConvertFromXlsException( String message, Throwable cause ) {

        super( message, cause );
    }


    public ConvertFromXlsException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {

        super( message, cause, enableSuppression, writableStackTrace );
    }

}
