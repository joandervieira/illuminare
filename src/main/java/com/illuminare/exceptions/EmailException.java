package com.illuminare.exceptions;


public class EmailException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -1901819158774753728L;


    public EmailException() {

    }


    public EmailException( String message ) {

        super( message );
    }


    public EmailException( Throwable cause ) {

        super( cause );
    }


    public EmailException( String message, Throwable cause ) {

        super( message, cause );
    }


    public EmailException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {

        super( message, cause, enableSuppression, writableStackTrace );
    }

}
