package com.illuminare.exceptions;


public class EntityNotFoundException extends ServiceException {

    /**
     * 
     */
    private static final long serialVersionUID = -1901819158774753728L;


    public EntityNotFoundException() {

    }


    public EntityNotFoundException( String message ) {

        super( message );
    }


    public EntityNotFoundException( Throwable cause ) {

        super( cause );
    }


    public EntityNotFoundException( String message, Throwable cause ) {

        super( message, cause );
    }


    public EntityNotFoundException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {

        super( message, cause, enableSuppression, writableStackTrace );
    }

}
