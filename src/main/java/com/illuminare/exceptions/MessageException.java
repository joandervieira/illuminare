package com.illuminare.exceptions;


public class MessageException extends Exception {

    /**
    * 
    */
    private static final long serialVersionUID = 1L;

    private final String msg;


    public MessageException( String msg ) {

        this.msg = msg;
    }


    public String getMsg() {

        return msg;
    }

}
