package com.illuminare.exceptions;


public class QuestionarioSemRespostasException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -1901819158774753728L;


    public QuestionarioSemRespostasException() {

    }


    public QuestionarioSemRespostasException( String message ) {

        super( message );
    }


    public QuestionarioSemRespostasException( Throwable cause ) {

        super( cause );
    }


    public QuestionarioSemRespostasException( String message, Throwable cause ) {

        super( message, cause );
    }


    public QuestionarioSemRespostasException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {

        super( message, cause, enableSuppression, writableStackTrace );
    }

}
