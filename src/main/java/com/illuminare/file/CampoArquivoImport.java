package com.illuminare.file;


public enum CampoArquivoImport {
    CNPJ( 0, "CNPJ" ), //
    MATRICULA( 1, "Matrícula" ), //
    NOME( 2, "Nome" ), //
    CPF( 3, "CPF" ), //
    EMAIL( 4, "E-mail" ), //
    SEXO( 5, "Sexo" ), //
    FONE( 6, "Fone" ), //
    END_LOGRADOURO( 7, "Endereço Logradouro" ), //
    END_NUMERO( 8, "Endereço Número" ), //
    END_CEP( 9, "Endereço Número" ), //
    END_BAIRRO( 10, "Endereço Bairro" ), //
    END_COMPLEMENTO( 11, "Endereço Complemento" ), //
    END_SIGLA_ESTADO( 12, "Endereço Estado" ), //
    END_NOME_CIDADE( 13, "Endereço Cidade" ),
    NOME_CARGO( 14, "Cargo" ),
    NOME_GRUPO_OCUPACIONAL( 15, "Grupo Ocupacional" ),
    NOME_SETOR( 16, "Setor" ),
    MATRICULA_LIDER( 17, "Líder" ),;

    private int index;

    private String nome;


    private CampoArquivoImport( int index, String nome ) {

        this.index = index;
        this.nome = nome;
    }


    public int getIndex() {

        return this.index;
    }


    public String getNome() {

        return this.nome;
    }
}
