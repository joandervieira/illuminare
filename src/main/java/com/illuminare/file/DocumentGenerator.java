package com.illuminare.file;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;


/**
 * Classe responsavel por gerar documentos do tipo XLS, DOC, PDF, etc.
 * 
 */
public class DocumentGenerator {

    private final static Logger log = Logger.getLogger( DocumentGenerator.class );

    SXSSFWorkbook workbook;

    Sheet sheet;

    CreationHelper createHelper;

    CellStyle cellStyle;

    int rowIdx = 0;

    int colIdx = 0;

    int totalRows = 0;
    boolean headerIsWrite = false;
    String fullyQualificationFileName;
    private int totalColumn = 0;


    public DocumentGenerator() {

        initObjects();
    }

    public static double getFileSizeInMB( String fullyQualificationName ) {

        File file = new File( fullyQualificationName );

        if ( file.exists() ) {

            double bytes = file.length();
            double kilobytes = ( bytes / 1024 );
            double megabytes = ( kilobytes / 1024 );
            // double gigabytes = ( megabytes / 1024 );
            // double terabytes = ( gigabytes / 1024 );
            // double petabytes = ( terabytes / 1024 );
            // double exabytes = ( petabytes / 1024 );
            // double zettabytes = ( exabytes / 1024 );
            // double yottabytes = ( zettabytes / 1024 );

            // System.out.println( "bytes : " + bytes );
            // System.out.println( "kilobytes : " + kilobytes );
            // System.out.println( "megabytes : " + megabytes );
            // System.out.println( "gigabytes : " + gigabytes );
            // System.out.println( "terabytes : " + terabytes );
            // System.out.println( "petabytes : " + petabytes );
            // System.out.println( "exabytes : " + exabytes );
            // System.out.println( "zettabytes : " + zettabytes );
            // System.out.println( "yottabytes : " + yottabytes );

            return megabytes;
        } else {
            return 0;
        }
    }

    public static String zipFile( String fullyQualificationName )
        throws IOException {

        if ( fullyQualificationName != null ) {
            String fileName = "";
            if ( fullyQualificationName.contains( File.separator ) ) {
                fileName = getFileNameFromFullyQualificationName( fullyQualificationName );
            } else {
                fileName = fullyQualificationName;
            }

            byte[] buffer = new byte[ 1024 ];

            log.debug( "ziping file:" + fullyQualificationName );
            log.debug( "file size:" + getFileSizeInMB( fullyQualificationName ) + " MB" );
            FileOutputStream fos = new FileOutputStream( fullyQualificationName + ".zip" );
            ZipOutputStream zos = new ZipOutputStream( fos );
            ZipEntry ze = new ZipEntry( fileName );
            zos.putNextEntry( ze );
            FileInputStream in = new FileInputStream( fullyQualificationName );

            int len;
            while ( ( len = in.read( buffer ) ) > 0 ) {
                zos.write( buffer, 0, len );
            }

            in.close();
            zos.closeEntry();

            // remember close it
            zos.close();
            log.debug( "finish zip file. New size: " + getFileSizeInMB( fullyQualificationName + ".zip" ) + " MB. File:" + fileName + ".zip" );
            deleteFile( fullyQualificationName );
            return fullyQualificationName + ".zip";
        }

        return null;

    }

    public static String getFileNameFromFullyQualificationName( String fullyQualificationName ) {

        if ( fullyQualificationName != null ) {
            String s = fullyQualificationName;
            return s.substring( s.lastIndexOf( File.separator ) + 1, s.length() );
        } else {
            return null;
        }
    }

    private static boolean deleteFile( String fileLocation ) {

        if ( fileLocation != null ) {
            File file = new File( fileLocation );
            return file.delete();
        } else {
            return false;
        }
    }

    /**
     * Testes
     */
    public static void main( String[] args )
        throws DocumentGeneratorException {

        System.out.println( "init" );

        DocumentGenerator dg = new DocumentGenerator();

        Map< String, Object > rowMap = new LinkedHashMap< String, Object >();
        rowMap.put( "coluna1Double", (Double) 10.5 );
        rowMap.put( "coluna2Integer string", "32469709" );
        rowMap.put( "coluna2Integer integer", Integer.valueOf( 10 ).intValue() );
        rowMap.put( "coluna3Number", (Number) 10.5 );
        rowMap.put( "coluna4Double Primitive", (double) 10.5 );
        rowMap.put( "coluna Primitive", ( Short.valueOf( "10" ) ) );
        rowMap.put( "coluna float", ( Float.valueOf( "10.6" ) ) );
        rowMap.put( "coluna blank", null );
        dg.writeRowOnXls( rowMap );

        dg.writeXlsOnFile( "teste_tipos" );

        System.out.println( "finish" );
    }

    private void initObjects() {

        if ( workbook == null ) {
            /**
             * SXSSF (Streaming Usermodel API) SXSSF (package:
             * org.apache.poi.xssf.streaming) is an API-compatible streaming extension of
             * XSSF to be used when very large spreadsheets have to be produced, and heap
             * space is limited. SXSSF achieves its low memory footprint by limiting access
             * to the rows that are within a sliding window, while XSSF gives access to all
             * rows in the document. Older rows that are no longer in the window become
             * inaccessible, as they are written to the disk.
             *
             * In this case, keep Constantes.BUFFER_ROW_TO_WRITE_XLS numbers of rows in
             * memory, exceeding these rows, they will be flushed to disk, and will not be
             * accessible
             */
            workbook = new SXSSFWorkbook( Constantes.BUFFER_ROW_TO_WRITE_XLS );
            createHelper = workbook.getCreationHelper();
            sheet = workbook.createSheet( "data" );
            cellStyle = workbook.createCellStyle();
            cellStyle.setDataFormat( createHelper.createDataFormat().getFormat( "dd/mm/yy hh:mm:ss" ) );
        }
    }

    private Sheet writeHeader( Map< String, Object > rowMap ) {

        colIdx = 0;
        totalColumn = 0;
        Row row = sheet.createRow( 0 );
        for ( String columnName : rowMap.keySet() ) {
            row.createCell( colIdx ).setCellValue( columnName );
            colIdx++;
            totalColumn++;
        }
        colIdx = 0;
        rowIdx++;
        headerIsWrite = true;
        return sheet;
    }

    /**
     * Escreve os dados em uma linha da planilha. Chamar este metodo para cada linha
     * a ser inserida.<br>
     * Para obter a planilha preenchida, chamar o metodo getWorkbook(). <br>
     * Para gravar a planilha em um arquivo no disco, chamar o metodo
     * writeXlsOnFile()
     *
     * @param rowMap
     *            Dados a serem inseridos na proxima linha
     * @throws DocumentGeneratorException
     */
    public void writeRowOnXls( Map< String, Object > rowMap )
        throws DocumentGeneratorException {

        try {
            initObjects();

            colIdx = 0;

            // CABECALHO
            if ( !headerIsWrite ) {
                writeHeader( rowMap );
            }

            if ( rowMap != null && rowMap.size() > 0 ) {

                // DADOS
                Row rowData = sheet.createRow( rowIdx );
                for ( String columnName : rowMap.keySet() ) {
                    Object val = rowMap.get( columnName );
                    if ( val != null ) {
                        if ( val instanceof Date ) {
                            Cell c = rowData.createCell( colIdx );
                            c.setCellValue( (Date) val );
                            c.setCellStyle( cellStyle );

                        } else if ( isNumber( val ) ) {
                            Cell c = rowData.createCell( colIdx );
                            c.setCellValue( Double.valueOf( val.toString() ) );
                            c.setCellType( SXSSFCell.CELL_TYPE_NUMERIC );
                        } else {
                            Cell c = rowData.createCell( colIdx );
                            c.setCellValue( val.toString() );
                            c.setCellType( SXSSFCell.CELL_TYPE_STRING );
                        }
                    } else {
                        Cell c = rowData.createCell( colIdx );
                        c.setCellValue( "" );
                        c.setCellType( SXSSFCell.CELL_TYPE_BLANK );
                    }
                    colIdx++;
                }

                rowIdx++;
            }

        } catch ( Exception e ) {
            throw new DocumentGeneratorException( "documentgenerator.erros.errorOnGenerateXls", e );
        }

    }

    /**
     * Salva a Planilha Excel XLS no diretorio temporario do sistema operacional.
     * <br>
     * Para obter o caminho completo do arquivo, chamar o metodo:
     * getFullyQualificationFileName()
     *
     * @param fileName
     *            - nome do arquivo a ser salvo
     * @throws DocumentGeneratorException
     */
    public void writeXlsOnFile( String fileName )
        throws DocumentGeneratorException {

        try {
            if ( workbook != null ) {

                Date dateStart = new Date();
                autoSizeColumns();

                // String reportPath = File.listRoots()[ 0 ].getAbsolutePath() +
                // File.separator + "tmp" + File.separator;
                String path = getTmpDir() + File.separator;

                if ( fileName == null ) {
                    fileName = getDefaultReportName();
                } else {
                    fileName = fileName.replaceAll( "[^a-zA-Z0-9.-]", "_" );
                }
                setFullyQualificationFileName( path + fileName );
                log.debug( "writing file on disk... [" + getFullyQualificationFileName() + "]" );
                FileOutputStream fileOut = new FileOutputStream( getFullyQualificationFileName() );
                workbook.write( fileOut );
                fileOut.close();
                workbook.dispose();
                log.debug( "finish write file on disk... [" + getFullyQualificationFileName() + "]" );
                Date dateStop = new Date();
                log.debug( "time to write file: " + getDiffTime( dateStart, dateStop ) );

            }

        } catch ( Exception e ) {
            throw new DocumentGeneratorException( "documentgenerator.erros.errorOnGenerateXls", e );
        }
    }

    private String getDiffTime( Date dateStart, Date dateStop ) {

        long diff = dateStop.getTime() - dateStart.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / ( 60 * 1000 ) % 60;
        // long diffHours = diff / (60 * 60 * 1000) % 24;
        // long diffDays = diff / (24 * 60 * 60 * 1000);

        return ( diffMinutes + " minutes, " + diffSeconds + " seconds, " + diff + " milliseconds" );

    }

    /**
     * Salva a Planilha Excel XLS no diretorio temporario do sistema operacional
     *
     * @throws DocumentGeneratorException
     */
    public void writeXlsOnFile()
        throws DocumentGeneratorException {

        writeXlsOnFile( getDefaultReportName() );
    }

    public boolean deleteReport() {

        if ( getFullyQualificationFileName() != null ) {
            return deleteFile( getFullyQualificationFileName() );
        } else {
            return false;
        }
    }

    public String getTmpDir() {

        String propTmpDir = "java.io.tmpdir";
        String tmpDir = System.getProperty( propTmpDir );
        return tmpDir;
    }

    private String getDefaultReportName() {

        SimpleDateFormat format = new SimpleDateFormat( "dd-MM-yy_HH:mm:ss:S" );
        // return "report_" + ( format.format( new Date() ) ) + ".xls";
        return "report_" + ( format.format( new Date() ) ) + "_" + UUID.randomUUID().toString().replaceAll( "-", "" ) + ".xls";
    }

    private boolean isNumber( Object val ) {

        if ( val != null ) {
            if ( val instanceof Number ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void autoSizeColumns() {

        if ( sheet != null ) {
            for ( int c = 0; c <= totalColumn; c++ ) {
                sheet.autoSizeColumn( c );
            }
        }
    }

    public SXSSFWorkbook getWorkbook() {

        return workbook;
    }

    public void setWorkbook( SXSSFWorkbook workbook ) {

        this.workbook = workbook;
    }

    public String getFullyQualificationFileName() {

        return fullyQualificationFileName;
    }

    public void setFullyQualificationFileName( String fullyQualificationFileName ) {

        if ( fullyQualificationFileName != null ) {
            // fullyQualificationFileName =
            // fullyQualificationFileName.replaceAll( "[^a-zA-Z0-9.-]", "_" );
            if ( !fullyQualificationFileName.endsWith( ".xlsx" ) ) {
                fullyQualificationFileName = fullyQualificationFileName + ".xlsx";
            }
        }

        this.fullyQualificationFileName = fullyQualificationFileName;
    }

    public int getTotalRows() {

        return totalRows;
    }


    public void setTotalRows( int totalRows ) {

        this.totalRows = totalRows;
    }

}
