package com.illuminare.file;


public class DocumentGeneratorException extends Exception {

    private static final long serialVersionUID = 1L;


    public DocumentGeneratorException() {

    }


    public DocumentGeneratorException( String message ) {

        super( message );
    }


    public DocumentGeneratorException( Throwable cause ) {

        super( cause );
    }


    public DocumentGeneratorException( String message, Throwable cause ) {

        super( message, cause );
    }


    public DocumentGeneratorException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {

        super( message, cause, enableSuppression, writableStackTrace );
    }

}
