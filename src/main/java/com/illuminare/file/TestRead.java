package com.illuminare.file;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class TestRead {

    public static void main( String[] args )
        throws EncryptedDocumentException,
        InvalidFormatException,
        IOException {

        test();
    }


    public static void test()
        throws IOException,
        EncryptedDocumentException,
        InvalidFormatException {

        InputStream inp = new FileInputStream( "/home/joander/developer/joander/sources/illuminare/illuminare/src/main/java/com/illuminare/file/teste.xlsx" );
        // InputStream inp = new FileInputStream("workbook.xlsx");

        Workbook wb = WorkbookFactory.create( inp );

        if ( wb != null ) {
            Sheet sheet = wb.getSheetAt( 0 );
            int totalRow = sheet.getLastRowNum();// start 0

            int i = 1;
            for ( Row row : sheet ) {
                if ( i == 1 ) {
                    i++;
                    continue;
                }
                Cell cell = row.getCell( CampoArquivoImport.MATRICULA.getIndex() );
                boolean matriculaNull = false;
                if ( cell != null ) {
                    // String matricula = (String) (cell.getCellType() ==
                    // Cell.CELL_TYPE_STRING ? cell.getStringCellValue() :
                    // cell.getNumericCellValue());
                    String matricula = String.valueOf( cell.getNumericCellValue() );
                    matricula = StringUtils.deleteWhitespace( matricula );
                    if ( StringUtils.isBlank( matricula ) ) {
                        matriculaNull = true;
                        matricula = null;
                    }
                    System.out.println( matricula );
                } else {
                    matriculaNull = true;
                }

                if ( matriculaNull ) {
                    // erro matricula obrigatorio
                }

                i++;
            }

        }

    }
}
