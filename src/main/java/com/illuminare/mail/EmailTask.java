package com.illuminare.mail;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import com.illuminare.util.Constantes;


public class EmailTask implements Runnable {

    Logger log = Logger.getLogger( EmailTask.class );

    private String titulo;

    private String html;

    private List< String > para;


    @Override
    public void run() {

        try {
            SimpleDateFormat f = new SimpleDateFormat( "dd/MM/yy HH:mm:ss" );
            Date dateStart = new Date();
            log.debug( "enviando email..." + f.format( dateStart ) );

            // send the email
            HtmlEmail email = new HtmlEmail();
            email.setHostName( "smtp.gmail.com" );
            email.setSmtpPort( 465 );
            email.setAuthenticator( new DefaultAuthenticator( Constantes.EMAIL_SISTEMA, Constantes.SENHA_EMAIL_SISTEMA ) );
            email.setSSLOnConnect( true );
            email.setFrom( Constantes.EMAIL_SISTEMA, "Illuminare" );

            log.debug( "Enviando email para:" );
            for ( String p : para ) {
                log.debug( p );
                email.addBcc( p );
            }

            email.setSubject( titulo );
            email.setHtmlMsg( html );

            // send the email
            email.send();

            log.debug( "email enviado:" + f.format( new Date() ) + " - Tempo: " + getDiffTime( dateStart, new Date() ) );

        } catch ( Exception e ) {
            log.error( "Erro ao enviar email:" + e.getMessage() );
            e.printStackTrace();
        }

    }


    private String getDiffTime( Date dateStart, Date dateStop ) {

        long diff = dateStop.getTime() - dateStart.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / ( 60 * 1000 ) % 60;
        // long diffHours = diff / (60 * 60 * 1000) % 24;
        // long diffDays = diff / (24 * 60 * 60 * 1000);
        return ( diffMinutes + " minutes, " + diffSeconds + " seconds, " + diff + " milliseconds" );
    }


    public String getTitulo() {

        return titulo;
    }


    public void setTitulo( String titulo ) {

        this.titulo = titulo;
    }


    public String getHtml() {

        return html;
    }


    public void setHtml( String html ) {

        this.html = html;
    }


    public List< String > getPara() {

        return para;
    }


    public void setPara( List< String > para ) {

        this.para = para;
    }

}
