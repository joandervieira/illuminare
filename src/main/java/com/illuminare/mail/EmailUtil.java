package com.illuminare.mail;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import com.illuminare.exceptions.EmailException;
import com.illuminare.util.Constantes;


public class EmailUtil {

    Logger log = Logger.getLogger( EmailUtil.class );


    public void enviarEmail( String titulo, String html, List< String > para )
        throws EmailException {

        try {
            SimpleDateFormat f = new SimpleDateFormat( "dd/MM/yy HH:mm:ss" );
            Date dateStart = new Date();
            log.debug( "enviando email..." + f.format( dateStart ) );

            // send the email
            HtmlEmail email = new HtmlEmail();
            email.setHostName( "smtp.gmail.com" );
            email.setSmtpPort( 465 );
            email.setAuthenticator( new DefaultAuthenticator( Constantes.EMAIL_SISTEMA, Constantes.SENHA_EMAIL_SISTEMA ) );
            email.setSSLOnConnect( true );
            email.setFrom( Constantes.EMAIL_SISTEMA, Constantes.EMAIL_FROM_TITLE );

            log.debug( "Enviando email de: " + Constantes.EMAIL_SISTEMA + " - para:" );
            for ( String p : para ) {
                log.debug( p );
                email.addBcc( p );
            }

            email.setSubject( titulo );
            email.setHtmlMsg( html );

            // send the email
            email.send();

            log.debug( "email enviado para:" + para + " em " + f.format( new Date() ) + " - Tempo: " + getDiffTime( dateStart, new Date() ) );

        } catch ( Exception e ) {
            log.error( "Erro ao enviar email: " + e.getMessage() );
            e.printStackTrace();
            throw new EmailException( e.getMessage(), e );
        }

    }


    private String getDiffTime( Date dateStart, Date dateStop ) {

        long diff = dateStop.getTime() - dateStart.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / ( 60 * 1000 ) % 60;
        // long diffHours = diff / (60 * 60 * 1000) % 24;
        // long diffDays = diff / (24 * 60 * 60 * 1000);
        return ( diffMinutes + " minutes, " + diffSeconds + " seconds, " + diff + " milliseconds" );
    }
}
