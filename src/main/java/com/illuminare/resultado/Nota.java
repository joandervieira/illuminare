package com.illuminare.resultado;


import java.util.List;


public class Nota {

    private Double media;

    private String mediaExibicao;

    private List< String > respostasTextos;


    public Double getMedia() {

        return media;
    }


    public void setMedia( Double media ) {

        this.media = media;
    }


    public String getMediaExibicao() {

        return mediaExibicao;
    }


    public void setMediaExibicao( String mediaExibicao ) {

        this.mediaExibicao = mediaExibicao;
    }


    public List< String > getRespostasTextos() {

        return respostasTextos;
    }


    public void setRespostasTextos( List< String > respostasTextos ) {

        this.respostasTextos = respostasTextos;
    }


    @Override
    public String toString() {

        return "Nota [media=" + media + ", mediaExibicao=" + mediaExibicao + ", respostasTextos.getSize="
            + ( respostasTextos != null ? respostasTextos.size() : null ) + "]";
    }

}
