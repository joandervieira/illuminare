package com.illuminare.resultado;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Questao;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.ValorLista;


public class QuestaoRespostas {

    private Questao questao;

    private Long idGrupoQuestao;

    private List< Resposta > respostas;

    private Double media;

    private ValorLista valorListaMedia;

    private Questionario questionario;

    private Funcionario funcionarioAvaliado;

    private Long totalQuestoesDoGrupo;


    public Questao getQuestao() {

        return questao;
    }


    public void setQuestao( Questao questao ) {

        this.questao = questao;
    }


    public List< Resposta > getRespostas() {

        if ( respostas == null ) {
            respostas = new ArrayList<>();
        }
        return respostas;
    }


    public void setRespostas( List< Resposta > respostas ) {

        this.respostas = respostas;
    }


    public Double getMedia() {

        return media;
    }


    public void setMedia( Double media ) {

        this.media = media;
    }


    public ValorLista getValorListaMedia() {

        return valorListaMedia;
    }


    public void setValorListaMedia( ValorLista valorListaMedia ) {

        this.valorListaMedia = valorListaMedia;
    }


    public Questionario getQuestionario() {

        return questionario;
    }


    public void setQuestionario( Questionario questionario ) {

        this.questionario = questionario;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        QuestaoRespostas that = (QuestaoRespostas) o;
        return Objects.equals( getQuestao(), that.getQuestao() ) && Objects.equals( getIdGrupoQuestao(), that.getIdGrupoQuestao() )
            && Objects.equals( getQuestionario(), that.getQuestionario() );
    }


    @Override
    public int hashCode() {

        return Objects.hash( getQuestao(), getIdGrupoQuestao(), getQuestionario() );
    }


    @Override
    public String toString() {

        return "QuestaoRespostas [questao=" + questao + ", media=" + media + ", questionario=" + questionario + "]";
    }


    public Funcionario getFuncionarioAvaliado() {

        return funcionarioAvaliado;
    }


    public void setFuncionarioAvaliado( Funcionario funcionarioAvaliado ) {

        this.funcionarioAvaliado = funcionarioAvaliado;
    }


    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }


    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }

    public Long getTotalQuestoesDoGrupo() {

        return totalQuestoesDoGrupo;
    }

    public void setTotalQuestoesDoGrupo( Long totalQuestoesDoGrupo ) {

        this.totalQuestoesDoGrupo = totalQuestoesDoGrupo;

    }
}
