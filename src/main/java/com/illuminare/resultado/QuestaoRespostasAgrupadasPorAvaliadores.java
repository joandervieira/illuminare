package com.illuminare.resultado;


import java.util.List;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Resposta;


public class QuestaoRespostasAgrupadasPorAvaliadores {

    private Pesquisa pesquisa;

    private Funcionario avaliado;

    private List< QuestaoRespostas > respostasPropria;

    private List< QuestaoRespostas > respostasLideres;

    private List< QuestaoRespostas > respostasSubordinados;

    private List< QuestaoRespostas > respostasOutros;

    private List< Resposta > respostas;


    public Pesquisa getPesquisa() {

        return pesquisa;
    }


    public void setPesquisa( Pesquisa pesquisa ) {

        this.pesquisa = pesquisa;
    }


    public Funcionario getAvaliado() {

        return avaliado;
    }


    public void setAvaliado( Funcionario avaliado ) {

        this.avaliado = avaliado;
    }


    public List< QuestaoRespostas > getRespostasPropria() {

        return respostasPropria;
    }


    public void setRespostasPropria( List< QuestaoRespostas > respostasPropria ) {

        this.respostasPropria = respostasPropria;
    }


    public List< QuestaoRespostas > getRespostasLideres() {

        return respostasLideres;
    }


    public void setRespostasLideres( List< QuestaoRespostas > respostasLideres ) {

        this.respostasLideres = respostasLideres;
    }


    public List< QuestaoRespostas > getRespostasSubordinados() {

        return respostasSubordinados;
    }


    public void setRespostasSubordinados( List< QuestaoRespostas > respostasSubordinados ) {

        this.respostasSubordinados = respostasSubordinados;
    }


    public List< QuestaoRespostas > getRespostasOutros() {

        return respostasOutros;
    }


    public void setRespostasOutros( List< QuestaoRespostas > respostasOutros ) {

        this.respostasOutros = respostasOutros;
    }


    public List< Resposta > getRespostas() {

        return respostas;
    }


    public void setRespostas( List< Resposta > respostas ) {

        this.respostas = respostas;
    }
}
