package com.illuminare.resultado;


import java.util.ArrayList;
import java.util.List;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Resposta;


public class RespostasAgrupadasPorAvaliadores {

    private Funcionario avaliado;

    private List< Resposta > respostasPropria;

    private List< Resposta > respostasLideres;

    private List< Resposta > respostasSubordinados;

    private List< Resposta > respostasOutros;

    private List< Resposta > respostas;


    // public Funcionario getAvaliado() {
    // return avaliado;
    // }
    //
    // public void setAvaliado(Funcionario avaliado) {
    // this.avaliado = avaliado;
    // }

    public List< Resposta > getRespostasPropria() {

        if ( respostasPropria == null ) {
            respostasPropria = new ArrayList<>();
        }
        return respostasPropria;
    }


    public void setRespostasPropria( List< Resposta > respostasPropria ) {

        this.respostasPropria = respostasPropria;
    }


    public List< Resposta > getRespostasLideres() {

        if ( respostasLideres == null ) {
            respostasLideres = new ArrayList<>();
        }
        return respostasLideres;
    }


    public void setRespostasLideres( List< Resposta > respostasLideres ) {

        this.respostasLideres = respostasLideres;
    }


    public List< Resposta > getRespostasSubordinados() {

        if ( respostasSubordinados == null ) {
            respostasSubordinados = new ArrayList<>();
        }
        return respostasSubordinados;
    }


    public void setRespostasSubordinados( List< Resposta > respostasSubordinados ) {

        this.respostasSubordinados = respostasSubordinados;
    }


    public List< Resposta > getRespostasOutros() {

        if ( respostasOutros == null ) {
            respostasOutros = new ArrayList<>();
        }
        return respostasOutros;
    }


    public void setRespostasOutros( List< Resposta > respostasOutros ) {

        this.respostasOutros = respostasOutros;
    }


    public List< Resposta > getRespostas() {

        return respostas;
    }


    public void setRespostas( List< Resposta > respostas ) {

        this.respostas = respostas;
    }
}
