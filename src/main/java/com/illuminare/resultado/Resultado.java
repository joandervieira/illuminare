package com.illuminare.resultado;


import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.List;

import com.illuminare.entity.Empresa;
import com.illuminare.view.CargoView;
import com.illuminare.view.FuncionarioView;
import com.illuminare.view.GrupoOcupacionalView;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.PesquisaResultView;
import com.illuminare.view.QuestaoOrdemView;
import com.illuminare.view.QuestionarioView;
import com.illuminare.view.SetorView;


public class Resultado {

    private PesquisaResultView pesquisa;

    private FuncionarioView avaliado;

    private GrupoOcupacionalView grupoOcupacional;

    private CargoView cargo;

    private SetorView setor;

    private Empresa empresa;

    private List< QuestionarioView > questionarios;


    public PesquisaResultView getPesquisa() {

        return pesquisa;
    }


    public void setPesquisa( PesquisaResultView pesquisa ) {

        this.pesquisa = pesquisa;
    }


    public FuncionarioView getAvaliado() {

        return avaliado;
    }


    public void setAvaliado( FuncionarioView avaliado ) {

        this.avaliado = avaliado;
    }


    public List< QuestionarioView > getQuestionarios() {

        return questionarios;
    }


    public void setQuestionarios( List< QuestionarioView > questionarios ) {

        this.questionarios = questionarios;
    }


    public GrupoOcupacionalView getGrupoOcupacional() {

        return grupoOcupacional;
    }


    public void setGrupoOcupacional( GrupoOcupacionalView grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }


    public CargoView getCargo() {

        return cargo;
    }


    public void setCargo( CargoView cargo ) {

        this.cargo = cargo;
    }


    public SetorView getSetor() {

        return setor;
    }


    public void setSetor( SetorView setor ) {

        this.setor = setor;
    }


    public Empresa getEmpresa() {

        return empresa;
    }


    public void setEmpresa( Empresa empresa ) {

        this.empresa = empresa;
    }


    public void cleanResult() {

        if ( getPesquisa() != null && getPesquisa().getEntity() != null ) {
            getPesquisa().setEntity( null );
        }

        if ( getQuestionarios() != null ) {
            for ( QuestionarioView questionario : getQuestionarios() ) {
                questionario.setRespostasProprias( null );
                questionario.setRespostasLider( null );
                questionario.setRespostasSubordinados( null );
                questionario.setRespostasOutros( null );
                if ( isNotEmpty( questionario.getGruposQuestoes() ) ) {
                    for ( GrupoQuestaoView grupoQuestaoView : questionario.getGruposQuestoes() ) {
                        if ( isNotEmpty( grupoQuestaoView.getQuestoesOrdem() ) ) {
                            for ( QuestaoOrdemView questaoOrdemView : grupoQuestaoView.getQuestoesOrdem() ) {
                                questaoOrdemView.getQuestao().setRespostasProprias( null );
                                questaoOrdemView.getQuestao().setRespostasLider( null );
                                questaoOrdemView.getQuestao().setRespostasSubordinados( null );
                                questaoOrdemView.getQuestao().setRespostasOutros( null );
                            }

                        }
                    }

                }
            }
        }
    }
}
