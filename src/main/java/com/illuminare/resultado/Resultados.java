package com.illuminare.resultado;


import java.util.ArrayList;
import java.util.List;


public class Resultados {

    public int qtdeTotalPropria;
    public int qtdeTotalLider;
    public int qtdeTotalSubordinados;
    public int qtdeTotalOutros;
    public int qtdeTotalMeta;
    public int qtdeTotalValorObtido;
    public int qtdeTotalPercentualMeta;
    private List< Resultado > resultadoPesquisas = new ArrayList<>();
    private Double mediaPropria;
    private Double mediaLideres;
    private Double mediaSubordinados;
    private Double mediaOutros;
    private Double meta;
    private Double valorObtido;
    private Double percentualMeta;

    public Double getMediaPropria() {

        return mediaPropria;
    }


    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }


    public Double getMediaLideres() {

        return mediaLideres;
    }


    public void setMediaLideres( Double mediaLideres ) {

        this.mediaLideres = mediaLideres;
    }


    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }


    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }


    public Double getMediaOutros() {

        return mediaOutros;
    }


    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public List< Resultado > getResultadoPesquisas() {

        return resultadoPesquisas;
    }


    public void setResultadoPesquisas( List< Resultado > resultadoPesquisas ) {

        this.resultadoPesquisas = resultadoPesquisas;
    }
}
