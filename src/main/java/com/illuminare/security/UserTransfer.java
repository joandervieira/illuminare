package com.illuminare.security;


import java.util.Map;

import com.illuminare.entity.Funcionario;


public class UserTransfer {

    private Long id;

    private String name;

    private Map< String, Boolean > roles;

    private String token;

    private Boolean senhaAtualizada = false;

    private String email;

    private String password;

    private Funcionario Funcionario;


    public UserTransfer( String userName, Map< String, Boolean > roles ) {

        this.name = userName;
        this.roles = roles;
    }


    public UserTransfer() {

    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getName() {

        return this.name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    public Map< String, Boolean > getRoles() {

        return this.roles;
    }

    public void setRoles( Map< String, Boolean > roles ) {

        this.roles = roles;
    }

    public String getToken() {

        return token;
    }

    public void setToken( String token ) {

        this.token = token;
    }

    public Boolean getSenhaAtualizada() {

        return senhaAtualizada;
    }


    public void setSenhaAtualizada( Boolean senhaAtualizada ) {

        this.senhaAtualizada = senhaAtualizada;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail( String email ) {

        this.email = email;
    }


    public String getPassword() {

        return password;
    }


    public void setPassword( String password ) {

        this.password = password;
    }


    public Funcionario getFuncionario() {

        return Funcionario;
    }


    public void setFuncionario( Funcionario funcionario ) {

        Funcionario = funcionario;
    }

}