package com.illuminare.service;


import com.illuminare.entity.ApplicationProperty;
import com.illuminare.exceptions.ServiceException;


public interface ApplicationPropertyService extends GenericService< ApplicationProperty > {

    ApplicationProperty findByKey( String key )
        throws ServiceException;

}
