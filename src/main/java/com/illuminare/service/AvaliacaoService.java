package com.illuminare.service;


import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;
import com.illuminare.view.QuestionarioAvaliacaoView;


public interface AvaliacaoService extends GenericService< QuestionarioAvaliacao > {

    Paginator< QuestionarioAvaliacao > pesquisar( Paginator< QuestionarioAvaliacao > paginator, String usuarioLogado )
        throws ServiceException;


    Set< QuestionarioAvaliacao > findAvaliacoesEmAberto( Long idFuncionarioAvaliador )
        throws ServiceException;


    Set< QuestionarioAvaliacaoView > findAvaliacao( Long idPesquisa, Long idFuncionarioAvaliador, Long idFuncionarioAvaliado, String usuarioLogado )
        throws ServiceException;


    List< QuestionarioAvaliacao > findByAvaliado( Long idFuncionarioAvaliado )
        throws ServiceException;


    List< QuestionarioAvaliacao > findByAvaliador( Long idFuncionarioAvaliador )
        throws ServiceException;


    Set< QuestionarioAvaliacao > findAvaliacoesByPesquisasEmAberto( Long idEmpresa, String userName )
        throws ServiceException;


    Paginator< QuestionarioAvaliacao > avaliacoesByPesquisasEmAbertoPageable( Paginator< QuestionarioAvaliacao > paginator, String userName )
        throws ServiceException;


    List< QuestionarioAvaliacao > associar( QuestionarioAvaliacao avaliacao, String usuarioCadastro )
        throws ServiceException;


    List< QuestionarioAvaliacao > findByPesquisa( Long idPesquisa )
        throws ServiceException;


    List< QuestionarioAvaliacao > findByFuncionarioAvaliadoAndAvaliadorAndPesquisa(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa );


    @Transactional( readOnly = true )
    List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador );


    @Transactional( readOnly = true )
    List< QuestionarioAvaliacao > findByPesquisaAndQuestionario( Pesquisa pesquisa, Questionario questionario );


    @Transactional( readOnly = true )
    List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior );


    @Transactional( readOnly = true )
    List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndAvaliadorAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior );


    @Transactional( readOnly = true )
    QuestionarioAvaliacao findByFuncionarioAvaliadoAndAvaliadorAndPesquisaAndQuestionario(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa,
        Questionario questionario );


    List< QuestionarioAvaliacao > findByPesquisa( Pesquisa pesquisa );


    void deleteByPesquisa( Pesquisa pesquisa );


    void deleteByPesquisaAndQuestionarioAndFuncionarioAvaliado( Pesquisa pesquisa, Questionario questionario, Funcionario funcionarioAvaliado );


    void deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador );


    @Transactional
    void deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior );


    @Transactional( readOnly = true )
    List< QuestionarioAvaliacao > findByPesquisaAndAvaliado( Long idPesquisa, Long idFuncionarioAvaliado );


    @Transactional( readOnly = true )
    List< QuestionarioAvaliacao > findByPesquisaAndAvaliado( Long idPesquisa, List< Long > idsFuncionarioAvaliado );
}
