package com.illuminare.service;


import java.util.List;

import com.illuminare.entity.Cargo;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface CargoService extends GenericService< Cargo > {

    Paginator< Cargo > pesquisar( Paginator< Cargo > paginator, String usuarioLogado )
        throws ServiceException;


    List< Cargo > findByNome( Long idEmpresa, String nome )
        throws ServiceException;


    List< Cargo > findByNome( Long idEmpresa, String nome, List< String > fieldsToFetch )
        throws ServiceException;

}
