package com.illuminare.service;


import java.util.List;

import com.illuminare.entity.Cidade;
import com.illuminare.exceptions.ServiceException;


public interface CidadeService extends GenericService< Cidade > {

    List< Cidade > findByEstado( Long idEstado )
        throws ServiceException;


    List< Cidade > findByNome( String nome )
        throws ServiceException;


    List< Cidade > findByNomeCidadeAndEstado( String nome, Long idEstado )
        throws ServiceException;

}
