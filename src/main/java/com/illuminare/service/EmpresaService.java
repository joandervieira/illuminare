package com.illuminare.service;


import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.Empresa;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface EmpresaService extends GenericService< Empresa > {

    Paginator< Empresa > pesquisar( Paginator< Empresa > paginator, String usuarioLogado )
        throws ServiceException;


    @Transactional( readOnly = true )
    Empresa findByCNPJ( String cnpj );
}
