package com.illuminare.service;


import com.illuminare.entity.Estado;
import com.illuminare.exceptions.ServiceException;


public interface EstadoService extends GenericService< Estado > {

    Estado findByUf( String uf )
        throws ServiceException;

}
