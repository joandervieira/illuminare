package com.illuminare.service;


import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;
import com.illuminare.view.PreviewFuncionariosImportacao;


public interface FuncionarioService extends GenericService< Funcionario > {

    Paginator< Funcionario > pesquisar( Paginator< Funcionario > paginator, String usuarioLogado )
        throws ServiceException;


    Funcionario findByCpf( Long idEmpresa, String cpf )
        throws ServiceException;


    @Transactional( readOnly = true )
    Funcionario findByCpf( Long idEmpresa, String cpf, List< String > fieldsToFetch );


    List< Funcionario > findSubordinados( Funcionario funcionario )
        throws ServiceException;


    List< Funcionario > findByGrupoOcupacional( Funcionario funcionario )
        throws ServiceException;


    List< Funcionario > findByGrupoOcupacional( GrupoOcupacional grupoOcupacional )
        throws ServiceException;


    Funcionario findByMatricula( Long idEmpresa, String matricula, List< String > fieldsToFetch )
        throws ServiceException;


    @Transactional( propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class )
    PreviewFuncionariosImportacao preConfirmarImportacao( PreviewFuncionariosImportacao previewFuncionariosImportacao, String usuarioCadastro )
        throws ServiceException;


    PreviewFuncionariosImportacao importFromXls( InputStream inputStream )
        throws ServiceException,
        IllegalAccessException,
        InvocationTargetException;


    @Transactional( readOnly = true )
    List< Funcionario > findByEmpresa( Empresa empresa )
        throws ServiceException;


    Funcionario findByMatricula( Long idEmpresa, String matricula )
        throws ServiceException;


    void confirmarImportacao( PreviewFuncionariosImportacao previewFuncionariosImportacao, String usuarioCadastro )
        throws ServiceException;


    List< Long > getIds( List< Funcionario > entities );


    List< Funcionario > findBySetor( Setor setor )
        throws ServiceException;


    List< Funcionario > findByCargo( Cargo cargo )
        throws ServiceException;

}
