package com.illuminare.service;


import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.illuminare.exceptions.ServiceException;


public interface GenericService< T > {

    // GenericDAO<T> getDao();

    List< T > listAll( Long idEmpresa, String userName )
        throws ServiceException;


    List< T > listAllOrderBy( Long idEmpresa, String orderBy, String userName )
        throws ServiceException;


    List< T > listAll( Long idEmpresa, Set< String > fieldsToFetch, String userName )
        throws ServiceException;


    @Transactional( readOnly = true )
    List< T > listAll( Long idEmpresa, List< String > fieldsToFetch, String userName )
        throws ServiceException;


    List< T > listAllOrderBy( Long idEmpresa, Set< String > fieldsToFetch, String orderBy, String userName )
        throws ServiceException;


    List< T > listAllOrderBy( Long idEmpresa, List< String > fieldsToFetch, String orderBy, String userName )
        throws ServiceException;


    T findById( Long id )
        throws ServiceException;


    void flush()
        throws ServiceException;


    @Transactional
    void update( T entity, String usuarioAtualizacao )
        throws ServiceException;


    @Transactional
    void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException;


    @Transactional
    void delete( Long id, String usuarioAtualizacao )
        throws ServiceException;


    @Transactional( readOnly = true )
    List< T > findByIdEmpresa( Long idEmpresa );


    @Transactional
    T save( T entity, String usuarioCadastro )
        throws ServiceException;


    @Transactional
    List< T > saveAll( List< T > entities, String usuarioCadastro )
        throws ServiceException;


    @Transactional
    List< T > saveUpdateAll( List< T > entities, String usuarioCadastro )
        throws ServiceException;


    @Transactional
    T saveUpdate( T entity, String usuarioCadastro )
        throws ServiceException;


    @Transactional
    Set< T > saveAll( Set< T > entities, String usuarioCadastro )
        throws ServiceException;


    @Transactional
    List< T > updateAll( List< T > entities, String usuarioAtualizacao )
        throws ServiceException;


    List< T > findByAttr( Map< String, Object > attrs )
        throws ServiceException;


    int executeNativeUpdate( String sql )
        throws ServiceException;


    T findByIdCurrent( Long id );


    T findById( Long id, Set< String > fieldsToFetch )
        throws ServiceException;


    T findById( Long id, List< String > fieldsToFetch )
        throws ServiceException;

}
