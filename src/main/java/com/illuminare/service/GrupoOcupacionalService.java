package com.illuminare.service;


import java.util.List;

import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface GrupoOcupacionalService extends GenericService< GrupoOcupacional > {

    Paginator< GrupoOcupacional > pesquisar( Paginator< GrupoOcupacional > paginator, String usuarioLogado )
        throws ServiceException;


    List< GrupoOcupacional > findByNome( Long idEmpresa, String nome )
        throws ServiceException;

}
