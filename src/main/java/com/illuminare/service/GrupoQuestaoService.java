package com.illuminare.service;


import com.illuminare.entity.GrupoQuestao;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface GrupoQuestaoService extends GenericService< GrupoQuestao > {

    Paginator< GrupoQuestao > pesquisar( Paginator< GrupoQuestao > paginator, String usuarioLogado )
        throws ServiceException;

}
