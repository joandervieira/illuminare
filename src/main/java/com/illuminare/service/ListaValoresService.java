package com.illuminare.service;


import com.illuminare.entity.ListaValores;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface ListaValoresService extends GenericService< ListaValores > {

    void validarEstaAssociadoQuestionario( Long idListaValores )
        throws ServiceException;


    void validarEstaAssociadoQuestionario( ListaValores entity )
        throws ServiceException;


    Paginator< ListaValores > pesquisar( Paginator< ListaValores > paginator, String usuarioLogado )
        throws ServiceException;

}
