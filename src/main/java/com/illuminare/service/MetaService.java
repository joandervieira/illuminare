package com.illuminare.service;


import com.illuminare.entity.Meta;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface MetaService extends GenericService< Meta > {

    Paginator< Meta > pesquisar( Paginator< Meta > paginator, String usuarioLogado )
        throws ServiceException;


    Meta findByPesquisaAndFuncionario( Long idPesquisa, Long idAvaliado )
        throws ServiceException;

}
