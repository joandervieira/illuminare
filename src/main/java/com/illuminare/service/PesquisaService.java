package com.illuminare.service;


import java.util.List;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface PesquisaService extends GenericService< Pesquisa > {

    Pesquisa findById( Long id, List< String > fieldsToFetch, Boolean findMetas )
        throws ServiceException;


    Paginator< Pesquisa > pesquisar( Paginator< Pesquisa > paginator, String usuarioLogado )
        throws ServiceException;


    List< Pesquisa > findByQuestionario( List< Questionario > questionarios )
        throws ServiceException;


    List< Pesquisa > findPesquisasEmAberto( Long idEmpresa )
        throws ServiceException;


    List< Pesquisa > findPesquisasEmAberto( Long idEmpresa, List< String > fieldsToFetch )
        throws ServiceException;


    boolean existeAvaliacaoRespondida( Long pesquisaId )
        throws ServiceException;


    void removerAvaliacoesEResponstas( Long pesquisaId, String userName )
        throws ServiceException;


    void removerRespostas( Long pesquisaId, String userName )
        throws ServiceException;


    void clonarPesquisa( Long oldIdPesquisa, String user )
        throws ServiceException;


    List< Long > getIds( List< Pesquisa > pesquisas );


    List< Pesquisa > findPesquisasFechadasByAvaliado( Long idAvaliado )
        throws ServiceException;


    List< Pesquisa > findPesquisasFechadas( Long idEmpresa )
        throws ServiceException;


    List< Funcionario > findAvaliados( Long idPesquisa )
        throws ServiceException;

}
