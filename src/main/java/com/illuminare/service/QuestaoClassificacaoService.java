package com.illuminare.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.Questionario;
import com.illuminare.exceptions.ServiceException;


public interface QuestaoClassificacaoService extends GenericService< QuestaoClassificacao > {

    QuestaoClassificacao findByQuestionarioAndQuestao( Questionario questionario, Questao questao, GrupoQuestao grupoQuestao );


    @Transactional( readOnly = true )
    List< QuestaoClassificacao > findByQuestionario( Questionario questionario );


    void salvarAtualizarTodos( List< QuestaoClassificacao > entities, String user )
        throws ServiceException;
}
