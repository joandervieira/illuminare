package com.illuminare.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.CargoView;
import com.illuminare.view.GrupoOcupacionalView;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.PesquisaView;
import com.illuminare.view.QuestaoMetaView;
import com.illuminare.view.QuestaoView;
import com.illuminare.view.QuestionarioView;
import com.illuminare.view.SetorView;


public interface QuestaoMetaService extends GenericService< QuestaoMeta > {

    @Transactional( readOnly = true )
    QuestaoMeta findByQuestaoAndPesquisaAndGrupoOcupacional( Long questao, Long grupoQuestao, Long pesquisa, Long questionario, Long grupoOcupacional );


    @Transactional( readOnly = true )
    QuestaoMeta findByQuestaoAndPesquisaAndGrupoOcupacional(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        GrupoOcupacional grupoOcupacional );


    @Transactional( readOnly = true )
    QuestaoMeta findByQuestaoAndPesquisaAndCargo( Long questao, Long grupoQuestao, Long pesquisa, Long questionario, Long cargo );


    @Transactional( readOnly = true )
    QuestaoMeta findByQuestaoAndPesquisaAndCargo( Questao questao, GrupoQuestao grupoQuestao, Pesquisa pesquisa, Questionario questionario, Cargo cargo );


    @Transactional( readOnly = true )
    QuestaoMeta findByQuestaoAndPesquisaAndSetor( Long questao, Long grupoQuestao, Long pesquisa, Long questionario, Long setor );


    @Transactional( readOnly = true )
    QuestaoMeta findByQuestaoAndPesquisaAndSetor( Questao questao, GrupoQuestao grupoQuestao, Pesquisa pesquisa, Questionario questionario, Setor setor );


    void deleteByPesquisa( Pesquisa pesquisa, String user );


    @Transactional( readOnly = true )
    Questionario fillMetasQuestionario( Questionario questionario, Pesquisa pesquisa )
        throws ServiceException;


    @Transactional( readOnly = true )
    Questao findMetasQuestao( Long idQuestao, Long idQuestionario, Long idPesquisa, Long idGrupoQuestao )
        throws ServiceException;


    @Transactional( readOnly = true )
    void fillMetaNasQuestoesTransient( PesquisaView pesquisa )
        throws ServiceException;


    @Transactional( readOnly = true )
    void clonarMetasTransient( PesquisaView pesquisa )
        throws ServiceException;


    QuestaoMetaView getQuestaoMeta(
        List< QuestaoMetaView > questaoMetasDoQuestionario,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa,
        GrupoOcupacionalView grupoOcupacionalView );


    QuestaoMetaView getQuestaoMeta(
        List< QuestaoMetaView > questaoMetasDoQuestionario,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa,
        SetorView setor );


    QuestaoMetaView getQuestaoMeta(
        List< QuestaoMetaView > questaoMetasDoQuestionario,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa,
        CargoView cargo );


    @Transactional( readOnly = true )
    List< QuestaoMeta > findByQuestionario( Long idQuestionario );


    @Transactional( readOnly = true )
    List< QuestaoMeta > findByPesquisa( Long idPesquisa );
}
