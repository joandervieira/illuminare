package com.illuminare.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.Questao;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;
import com.illuminare.view.QuestaoView;


public interface QuestaoService extends GenericService< Questao > {

    @Transactional( readOnly = true )
    Boolean temClassificacao( QuestaoView questao );


    Paginator< Questao > pesquisar( Paginator< Questao > paginator, String usuarioLogado )
        throws ServiceException;


    List< Questao > findByListaValores( Long idListaValores )
        throws ServiceException;

}
