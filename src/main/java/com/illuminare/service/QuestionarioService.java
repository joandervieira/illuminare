package com.illuminare.service;


import java.util.List;

import com.illuminare.entity.Questao;
import com.illuminare.entity.Questionario;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface QuestionarioService extends GenericService< Questionario > {

    Paginator< Questionario > pesquisar( Paginator< Questionario > paginator, String usuarioLogado )
        throws ServiceException;


    List< Questionario > findByListaValores( Long idListaValores )
        throws ServiceException;


    List< Questionario > findByQuestao( List< Questao > questoes )
        throws ServiceException;

}
