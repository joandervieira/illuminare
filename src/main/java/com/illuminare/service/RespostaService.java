package com.illuminare.service;


import java.util.List;

import com.illuminare.view.QuestionarioView;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;
import com.illuminare.view.QuestionarioRespondido;


public interface RespostaService extends GenericService< Resposta > {

    Paginator< Resposta > pesquisar( Paginator< Resposta > paginator, String usuarioLogado )
        throws ServiceException;


    List< Resposta > findByAvaliado( Long idAvaliado )
        throws ServiceException;


    List< Resposta > findByAvaliado( Long idAvaliado, List< Long > idsPesquisas )
        throws ServiceException;


    @Transactional( readOnly = true )
    List< Resposta > findByEmpresa( Pesquisa pesquisa, Empresa empresa )
        throws ServiceException;


    List< Resposta > findByAvaliacao( Long idQuestionarioAvaliacao )
        throws ServiceException;


    List< Resposta > findByGrupoOcupacional( Pesquisa pesquisa, GrupoOcupacional grupoOcupacional )
        throws ServiceException;


    List< Resposta > findByCargo( Pesquisa pesquisa, Cargo cargo )
        throws ServiceException;


    List< Resposta > findBySetor( Pesquisa pesquisa, Setor setor )
        throws ServiceException;


    List< Resposta > findByAvaliado( Funcionario funcionarioAvaliado, Pesquisa pesquisa )
        throws ServiceException;


    @Transactional( readOnly = true )
    Boolean checkIsRespnodidoByAvaliacao( Long idQuestionarioAvaliacao );


    @Transactional( readOnly = true )
    Resposta findByAvaliacaoAndQuestao( Long idQuestionarioAvaliacao, Long idQuestao, Long idGrupoQuestao )
        throws ServiceException;


    @Transactional( readOnly = true )
    List<QuestionarioView> findByAvaliacaoAgrupado(Long idQuestionarioAvaliacao )
        throws ServiceException;


    @Transactional
    void deleteByAvaliacao( Long idAvaliacao, String user )
        throws ServiceException;


    @Transactional( readOnly = true )
    QuestionarioRespondido questionarioRespondido( Pesquisa pesquisa, Questionario questionario, Funcionario avaliado, Funcionario avaliador )
        throws ServiceException;


    @Transactional
    int removerDuplicadas( Long idEmpresa )
        throws ServiceException;
}
