package com.illuminare.service;


import org.springframework.transaction.annotation.Transactional;

import com.illuminare.exceptions.ServiceException;
import com.illuminare.resultado.Resultado;


public interface ResultadoService2 {

    Resultado resultadoPorFuncionarioAvaliado( Long idPesquisa, Long idAvaliado, String user )
        throws ServiceException;


    Resultado resultadoPorGrupoOcupacional( Long idPesquisa, Long idGrupoOcupacional, String user )
        throws ServiceException;


    Resultado resultadoPorCargo( Long idPesquisa, Long idCargo, String user )
        throws ServiceException;


    Resultado resultadoPorSetor( Long idPesquisa, Long idSetor, String user )
        throws ServiceException;


    @Transactional( readOnly = true )
    Resultado resultadoPorEmpresa( Long idPesquisa, Long idEmpresa, String user )
        throws ServiceException;
}
