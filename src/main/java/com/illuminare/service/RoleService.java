package com.illuminare.service;


import com.illuminare.entity.Role;


public interface RoleService extends GenericService< Role > {

    Role findByName( String name );

}
