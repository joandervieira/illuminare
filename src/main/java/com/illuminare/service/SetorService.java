package com.illuminare.service;


import java.util.List;

import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface SetorService extends GenericService< Setor > {

    Paginator< Setor > pesquisar( Paginator< Setor > paginator, String usuarioLogado )
        throws ServiceException;


    List< Setor > findByNome( Long idEmpresa, String nome )
        throws ServiceException;

}
