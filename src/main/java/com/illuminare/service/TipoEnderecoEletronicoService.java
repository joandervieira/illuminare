package com.illuminare.service;


import com.illuminare.entity.TipoEnderecoEletronico;
import com.illuminare.exceptions.ServiceException;


public interface TipoEnderecoEletronicoService extends GenericService< TipoEnderecoEletronico > {

    TipoEnderecoEletronico getTypeEmail()
        throws ServiceException;

}
