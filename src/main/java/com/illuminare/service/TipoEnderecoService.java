package com.illuminare.service;


import com.illuminare.entity.TipoEndereco;
import com.illuminare.exceptions.ServiceException;


public interface TipoEnderecoService extends GenericService< TipoEndereco > {

    TipoEndereco getResidencial()
        throws ServiceException;

}
