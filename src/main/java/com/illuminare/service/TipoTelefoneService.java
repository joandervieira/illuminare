package com.illuminare.service;


import com.illuminare.entity.TipoTelefone;
import com.illuminare.exceptions.ServiceException;


public interface TipoTelefoneService extends GenericService< TipoTelefone > {

    TipoTelefone getResidencial()
        throws ServiceException;


    TipoTelefone getPessoal()
        throws ServiceException;

}
