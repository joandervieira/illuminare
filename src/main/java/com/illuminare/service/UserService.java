package com.illuminare.service;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.illuminare.entity.Funcionario;
import com.illuminare.entity.User;
import com.illuminare.exceptions.EntityNotFoundException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.view.Paginator;


public interface UserService extends GenericService< User > {

    UserDetails loadUserByUsername( String username )
        throws UsernameNotFoundException;


    List< User > listarAdministradores();


    void atualizarSenha( User entity, String usuarioAtualizacao )
        throws ServiceException;


    User findByName( String name, List< String > fieldsToFetch )
        throws EntityNotFoundException;


    List< User > listAll( List< String > fieldsToFetch );


    void salvarUltimoAcesso( String userName )
        throws ServiceException;


    void enviarEmailRecuperacao( User user )
        throws ServiceException;


    Paginator< User > pesquisar( Paginator< User > paginator, String usuarioLogado )
        throws ServiceException;


    void cadastrarUsuarioFuncionario( Funcionario funcionario, String usuarioCadastro )
        throws ServiceException;


    User findByFuncionario( Long idFuncionario )
        throws ServiceException;


    User findByName( String name )
        throws EntityNotFoundException;


    boolean isAdmin( String userName )
        throws EntityNotFoundException;


    boolean isAdmin( User user );


    boolean isGestor( String userName )
        throws EntityNotFoundException;


    boolean isGestor( User user );
}
