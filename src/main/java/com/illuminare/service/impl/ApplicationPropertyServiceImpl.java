package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.ApplicationPropertyDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.ApplicationProperty;
import com.illuminare.exceptions.DAOException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.ApplicationPropertyService;
import com.illuminare.service.UserService;


@Service
public class ApplicationPropertyServiceImpl extends GenericServiceImpl< ApplicationProperty > implements ApplicationPropertyService {

    @Autowired
    ApplicationPropertyDAO dao;

    @Autowired
    UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< ApplicationProperty > getDao() {

        return dao;
    }


    private ApplicationProperty fillAuditoriaSave( ApplicationProperty entity, String usuarioCadastro ) {

        entity.setCreatedAt( new Date() );
        entity.setCreatedBy( usuarioCadastro );
        return entity;
    }


    private ApplicationProperty fillAuditoriaUpdate( ApplicationProperty entity, String usuarioAtualizacao ) {

        entity.setUpdatedAt( new Date() );
        entity.setUpdatedBy( usuarioAtualizacao );
        return entity;
    }


    @Override
    @Transactional
    public ApplicationProperty save( ApplicationProperty entity, String usuarioCadastro )
        throws ServiceException {

        if ( entity != null && usuarioCadastro != null ) {
            return getDao().save( fillAuditoriaSave( entity, usuarioCadastro ) );
        } else {
            return null;
        }

    }


    @Transactional
    @Override
    public List< ApplicationProperty > saveAll( List< ApplicationProperty > entities, String usuarioCadastro ) {

        if ( CollectionUtils.isNotEmpty( entities ) && usuarioCadastro != null ) {
            List< ApplicationProperty > entitiesApplicationPropertyoSave = new ArrayList< ApplicationProperty >();

            for ( ApplicationProperty entity : entities ) {
                entitiesApplicationPropertyoSave.add( fillAuditoriaSave( entity, usuarioCadastro ) );
            }
            return getDao().saveAll( entitiesApplicationPropertyoSave );
        } else {
            return null;
        }
    }


    @Transactional
    @Override
    public List< ApplicationProperty > updateAll( List< ApplicationProperty > entities, String usuarioAtualizacao ) {

        if ( CollectionUtils.isNotEmpty( entities ) && usuarioAtualizacao != null ) {
            List< ApplicationProperty > entitiesApplicationPropertyoSave = new ArrayList< ApplicationProperty >();

            for ( ApplicationProperty entity : entities ) {
                entitiesApplicationPropertyoSave.add( fillAuditoriaSave( entity, usuarioAtualizacao ) );
            }
            return getDao().saveAll( entitiesApplicationPropertyoSave );
        } else {
            return null;
        }
    }


    @Override
    @Transactional
    public void update( ApplicationProperty entity, String usuarioAtualizacao )
        throws ServiceException {

        if ( entity != null && usuarioAtualizacao != null ) {
            getDao().update( fillAuditoriaUpdate( entity, usuarioAtualizacao ) );
        }

    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        if ( id != null && usuarioAtualizacao != null ) {
            ApplicationProperty e = findById( id );
            e.setUpdatedAt( new Date() );
            e.setUpdatedBy( usuarioAtualizacao );
            update( (ApplicationProperty) e, usuarioAtualizacao );
            if ( getDao().deleteLogic( id ) <= 0 ) {
                throw new ServiceException( "Falha ao excluir entidade" );
            }
        }

    }


    @Override
    @Transactional( readOnly = true )
    public List< ApplicationProperty > listAll( Long idEmpresa, String userName )
        throws ServiceException {

        if ( !userService.isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        return getDao().listAll( idEmpresa );
    }


    @Override
    @Transactional( readOnly = true )
    public List< ApplicationProperty > listAllOrderBy( Long idEmpresa, String orderBy, String userName ) {

        return getDao().listAllOrderBy( idEmpresa, orderBy );
    }


    @Override
    @Transactional( readOnly = true )
    public ApplicationProperty findById( Long id ) {

        if ( id != null ) {
            return getDao().findById( id );
        } else {
            return null;
        }
    }


    @Override
    @Transactional( readOnly = true )
    public List< ApplicationProperty > findByAttr( Map< String, Object > attrs ) {

        return getDao().findByAttr( attrs );
    }


    @Override
    @Transactional
    public int executeNativeUpdate( String sql )
        throws ServiceException {

        try {
            return getDao().executeNativeUpdate( sql );
        } catch ( DAOException e ) {
            throw new ServiceException( e );
        }
    }


    @Override
    @Transactional( readOnly = true )
    public ApplicationProperty findByKey( String key )
        throws ServiceException {

        Map< String, Object > attr = new HashMap< String, Object >();
        attr.put( "keyProp", key );
        List< ApplicationProperty > list = findByAttr( attr );
        if ( CollectionUtils.isNotEmpty( list ) ) {
            return list.get( 0 );
        } else {
            return null;
        }
    }
}
