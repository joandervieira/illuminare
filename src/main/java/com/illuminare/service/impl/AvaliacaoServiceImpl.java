package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.AvaliacaoDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.QuestaoDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.Setor;
import com.illuminare.entity.User;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.AvaliacaoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoClassificacaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.RespostaService;
import com.illuminare.service.UserService;
import com.illuminare.util.DateUtil;
import com.illuminare.view.Paginator;
import com.illuminare.view.QuestionarioAvaliacaoView;
import com.illuminare.view.QuestionarioRespondido;


@Service
public class AvaliacaoServiceImpl extends GenericServiceImpl< QuestionarioAvaliacao > implements AvaliacaoService {

    Logger log = Logger.getLogger( AvaliacaoServiceImpl.class );

    @Autowired
    AvaliacaoDAO dao;
    @Autowired
    PesquisaService pesquisaService;
    @Autowired
    UserService userService;
    @Autowired
    FuncionarioService funcionarioService;
    @Autowired
    RespostaService respostaService;
    @Autowired
    QuestionarioService questionarioService;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    QuestaoClassificacaoService questaoClassificacaoService;
    @Autowired
    QuestaoDAO questaoDAO;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< QuestionarioAvaliacao > getDao() {

        return dao;
    }


    @Transactional( readOnly = true )
    @Override
    public Paginator< QuestionarioAvaliacao > pesquisar( Paginator< QuestionarioAvaliacao > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< QuestionarioAvaliacao >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( QuestionarioAvaliacao entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( QuestionarioAvaliacao entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getPesquisa() != null && entity.getPesquisa().getId() != null ) {
                fields.put( "[pesquisa].id", entity.getPesquisa().getId() );
            }
            if ( entity.getFuncionarioAvaliado() != null && entity.getFuncionarioAvaliado().getId() != null ) {
                fields.put( "[avaliado].id", entity.getFuncionarioAvaliado().getId() );
            }
            if ( entity.getFuncionarioAvaliador() != null && entity.getFuncionarioAvaliador().getId() != null ) {
                fields.put( "[avaliador].id", entity.getFuncionarioAvaliador().getId() );
            }
            if ( entity.getRespondido() != null ) {
                fields.put( "respondido", entity.getRespondido() );
            }
        }

        return fields;
    }


    @Transactional( readOnly = true )
    @Override
    public Set< QuestionarioAvaliacao > findAvaliacoesEmAberto( Long idFuncionarioAvaliador )
        throws ServiceException {

        Assert.notNull( idFuncionarioAvaliador, "Parameter idFuncionarioAvaliador is required." );

        Funcionario funcionarioAvaliador = funcionarioService.findById( idFuncionarioAvaliador );
        if ( funcionarioAvaliador == null ) {
            throw new ServiceException( "Funcionário não encontrado com id=" + idFuncionarioAvaliador );
        }

        List< Pesquisa > pesquisas = pesquisaService.findPesquisasEmAberto( funcionarioAvaliador.getIdEmpresa() );

        if ( CollectionUtils.isNotEmpty( pesquisas ) ) {

            Set< QuestionarioAvaliacao > avaliacoes = dao.findAvaliacoesByPesquisas( idFuncionarioAvaliador, pesquisas );

            return makeResultFindAvaliacoesEmAberto( avaliacoes );

        } else {
            return new HashSet<>();
        }

    }


    public Set< QuestionarioAvaliacao > makeResultFindAvaliacoesEmAberto( Set< QuestionarioAvaliacao > avaliacoes )
        throws ServiceException {

        Set< QuestionarioAvaliacao > avaliacoesResult = new HashSet<>();

        if ( avaliacoes != null ) {
            for ( QuestionarioAvaliacao qa : avaliacoes ) {

                if ( qa.getFuncionarioAvaliado() != null ) {
                    if ( qa.getFuncionarioAvaliado().getPessoa() != null ) {
                        qa.getFuncionarioAvaliado().setPessoa( dao.unproxyInitialize( qa.getFuncionarioAvaliado().getPessoa() ) );
                        qa.getFuncionarioAvaliado().setCargo( dao.unproxyInitialize( qa.getFuncionarioAvaliado().getCargo() ) );
                    }
                }
                if ( qa.getFuncionarioAvaliador() != null ) {
                    if ( qa.getFuncionarioAvaliador().getPessoa() != null ) {
                        qa.getFuncionarioAvaliador().setPessoa( dao.unproxyInitialize( qa.getFuncionarioAvaliador().getPessoa() ) );
                        qa.getFuncionarioAvaliador().setCargo( dao.unproxyInitialize( qa.getFuncionarioAvaliador().getCargo() ) );
                    }
                }

                QuestionarioAvaliacao avaliacao = new QuestionarioAvaliacao();
                avaliacao.setId( qa.getId() );
                avaliacao.setFuncionarioAvaliado( qa.getFuncionarioAvaliado() );
                avaliacao.setFuncionarioAvaliador( qa.getFuncionarioAvaliador() );

                QuestionarioRespondido questionarioRespondido =
                    respostaService.questionarioRespondido( qa.getPesquisa(), qa.getQuestionario(), qa.getFuncionarioAvaliado(), qa.getFuncionarioAvaliador() );
                avaliacao.setQuestionarioRespondido( questionarioRespondido );

                Pesquisa p = new Pesquisa();
                p.setId( qa.getPesquisa().getId() );
                p.setNome( qa.getPesquisa().getNome() );
                p.setDtaInicio( qa.getPesquisa().getDtaInicio() );
                p.setDtaFim( qa.getPesquisa().getDtaFim() );
                p.setDtaEntrega( qa.getPesquisa().getDtaEntrega() );
                p.setObs( qa.getPesquisa().getObs() );
                avaliacao.setPesquisa( p );

                Questionario q = new Questionario();
                q.setId( qa.getQuestionario().getId() );
                q.setNome( qa.getQuestionario().getNome() );
                q.setGruposOcupacionais( qa.getQuestionario().getGruposOcupacionais() );
                avaliacao.setQuestionario( q );

                avaliacoesResult.add( avaliacao );
            }
        }

        return avaliacoesResult;
    }


    private void unProxyQuestionarioAvaliacao( QuestionarioAvaliacao qa ) {

        if ( qa != null ) {
            if ( qa.getFuncionarioAvaliado() != null ) {
                if ( qa.getFuncionarioAvaliado().getPessoa() != null ) {
                    qa.getFuncionarioAvaliado().setPessoa( dao.unproxyInitialize( qa.getFuncionarioAvaliado().getPessoa() ) );
                }
            }
            if ( qa.getFuncionarioAvaliador() != null ) {
                if ( qa.getFuncionarioAvaliador().getPessoa() != null ) {
                    qa.getFuncionarioAvaliador().setPessoa( dao.unproxyInitialize( qa.getFuncionarioAvaliador().getPessoa() ) );
                }
            }
        }
    }


    @Transactional( readOnly = true )
    @Override
    public Set< QuestionarioAvaliacao > findAvaliacoesByPesquisasEmAberto( Long idEmpresa, String userName )
        throws ServiceException {

        if ( !userService.isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }

        List< Pesquisa > pesquisas = pesquisaService.findPesquisasEmAberto( idEmpresa, null );

        Set< QuestionarioAvaliacao > avaliacoes = null;

        if ( CollectionUtils.isNotEmpty( pesquisas ) ) {
            avaliacoes = dao.findAvaliacoesByPesquisas( null, pesquisas );
        }

        return avaliacoes;
    }


    @Transactional( readOnly = true )
    @Override
    public Paginator< QuestionarioAvaliacao > avaliacoesByPesquisasEmAbertoPageable( Paginator< QuestionarioAvaliacao > paginator, String userName )
        throws ServiceException {

        paginator.setFieldsToSearch( new HashMap< String, Object >() );

        Long idEmpresa = null;
        if ( paginator.getEntity() != null ) {
            idEmpresa = paginator.getEntity().getIdEmpresa();
        }
        if ( !userService.isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }

        paginator.setResult( null );
        paginator.setTotal( 0L );

        List< Pesquisa > pesquisas = null;
        Boolean filteredByPesquisa = true;

        // if ( paginator.getEntity() == null ||
        // paginator.getEntity().getPesquisa() == null ||
        // paginator.getEntity().getPesquisa().getId() == null ) {
        // filteredByPesquisa = false;
        // pesquisas = pesquisaService.findPesquisasEmAberto( idEmpresa, null );
        // if ( CollectionUtils.isNotEmpty( pesquisas ) ) {
        // paginator.getFieldsToSearch().put( "[pesquisa].id",
        // pesquisaService.getIds( pesquisas ) );
        // }
        //
        // }
        if ( paginator.getEntity() == null || paginator.getEntity().getPesquisa() == null || paginator.getEntity().getPesquisa().getId() == null ) {
            filteredByPesquisa = false;
            pesquisas = pesquisaService.listAll( idEmpresa, userName );
            if ( CollectionUtils.isNotEmpty( pesquisas ) ) {
                paginator.getFieldsToSearch().put( "[pesquisa].id", pesquisaService.getIds( pesquisas ) );
            }

        }

        if ( CollectionUtils.isNotEmpty( pesquisas ) || filteredByPesquisa ) {
            paginator.getFieldsToSearch().putAll( mapFields( paginator.getEntity() ) );

            paginator = dao.findAvaliacoesByPesquisasPageable( paginator, true );

            if ( paginator.getResult() != null ) {
                for ( QuestionarioAvaliacao qa : paginator.getResult() ) {
                    if ( qa.getRespostas() != null ) {
                        for ( Resposta r : qa.getRespostas() ) {
                            r.setQuestionarioAvaliacao( null );
                        }
                    }
                    if ( qa.getPesquisa() != null && qa.getPesquisa().getIdEmpresa() != null ) {
                        Empresa empresa = empresaService.findById( qa.getPesquisa().getIdEmpresa() );
                        qa.setEmpresa( empresa );
                    }
                }
            }
        }

        return paginator;
    }


    @Transactional( readOnly = true, propagation = Propagation.REQUIRES_NEW )
    @Override
    public Set< QuestionarioAvaliacaoView > findAvaliacao( Long idPesquisa, Long idFuncionarioAvaliador, Long idFuncionarioAvaliado, String usuarioLogado )
        throws ServiceException {

        Assert.notNull( idPesquisa );
        Assert.notNull( idFuncionarioAvaliador );
        Assert.notNull( idFuncionarioAvaliado );
        Assert.notNull( usuarioLogado );

        User usuarioAvaliador = userService.findByFuncionario( idFuncionarioAvaliador );
        if ( usuarioAvaliador == null ) {
            throw new ServiceException( "Erro ao processar Avaliação: Usuário do Funcionário Avaliador não foi encontrado" );
        }
        if ( !usuarioAvaliador.getName().equals( usuarioLogado ) ) {
            throw new ServiceException( "Erro ao processar Avaliação: Usuário logado não é o mesmo do Funcionário Avaliador" );
        }

        Set< QuestionarioAvaliacao > avaliacoes =
            dao.findAvaliacao( idPesquisa, idFuncionarioAvaliador, idFuncionarioAvaliado, DateUtil.getStartOfToday(), DateUtil.getStartOfToday() );

        if ( CollectionUtils.isEmpty( avaliacoes ) ) {
            throw new ServiceException( "Avaliação não encontrada" );
        }
        dao.unproxyInitialize( avaliacoes );
        for ( QuestionarioAvaliacao avaliacao : avaliacoes ) {
            if ( avaliacao.getQuestionario() != null ) {
                Set< String > fetchQuestionarioFields = new LinkedHashSet<>();
                fetchQuestionarioFields.add( "gruposQuestoes grupoQuestao" );
                fetchQuestionarioFields.add( "[grupoQuestao].questoesOrdem qOrdem" );
                fetchQuestionarioFields.add( "[qOrdem].questao questao" );
                fetchQuestionarioFields.add( "[questao].listaValores" );
                avaliacao.setQuestionario( questionarioService.findById( avaliacao.getQuestionario().getId(), fetchQuestionarioFields ) );

            }
            if ( avaliacao.getFuncionarioAvaliador() != null ) {
                avaliacao.setFuncionarioAvaliador(
                    funcionarioService.findById( avaliacao.getFuncionarioAvaliador().getId(), Arrays.asList( "pessoa", "cargo c", "[c].grupoOcupacional" ) ) );
            }
            if ( avaliacao.getFuncionarioAvaliador() != null ) {
                avaliacao.setFuncionarioAvaliado(
                    funcionarioService.findById( avaliacao.getFuncionarioAvaliado().getId(), Arrays.asList( "pessoa", "cargo c", "[c].grupoOcupacional" ) ) );
            }

        }

        // find respostas e classificacao
        for ( QuestionarioAvaliacao questionarioAvaliacao : avaliacoes ) {
            questionarioAvaliacao.unproxy( dao );
            for ( GrupoQuestao grupoQuestao : questionarioAvaliacao.getQuestionario().getGruposQuestoes() ) {
                grupoQuestao.unproxy( dao );
                for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                    questaoOrdem.unproxy( dao );
                    Questao questao = questaoOrdem.getQuestao();

                    // RESPOSTAS
                    Resposta resposta = respostaService.findByAvaliacaoAndQuestao( questionarioAvaliacao.getId(), questao.getId(), grupoQuestao.getId() );
                    if ( resposta != null ) {
                        resposta.unproxy( dao );
                        questao.setRespostaValorLista( resposta.getRespostaValorLista() );
                        questao.setRespostaTexto( resposta.getRespostaTexto() );
                        questao.setRespostaNumerico( resposta.getRespostaNumerico() );
                        questao.setRespostaId( resposta.getId() );
                        questionarioAvaliacao.setRespondido( true );
                    }

                    // CLASSIFICACOES
                    QuestaoClassificacao classificacao =
                        questaoClassificacaoService.findByQuestionarioAndQuestao( questionarioAvaliacao.getQuestionario(), questao, grupoQuestao );
                    if ( classificacao != null ) {
                        questao.setClassificacao( classificacao.getClassificacao() );
                    }
                }
            }
        }

        return toView( avaliacoes );
    }


    private Set< QuestionarioAvaliacaoView > toView( Set< QuestionarioAvaliacao > avaliacoes ) {

        if ( avaliacoes != null ) {
            Set< QuestionarioAvaliacaoView > views = new LinkedHashSet<>();
            for ( QuestionarioAvaliacao avaliacao : avaliacoes ) {
                views.add( avaliacao.formatToWebPage().format() );
            }
            return views;
        }

        return null;
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByAvaliado( Long idFuncionarioAvaliado )
        throws ServiceException {

        return dao.findByAvaliado( idFuncionarioAvaliado );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByAvaliador( Long idFuncionarioAvaliador )
        throws ServiceException {

        return dao.findByAvaliador( idFuncionarioAvaliador );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        Assert.notNull( id, "Parâmetro id nulo" );

        List< Resposta > respostas = respostaService.findByAvaliacao( id );

        if ( CollectionUtils.isNotEmpty( respostas ) ) {
            throw new ServiceException( "Impossível Excluir. Associação já possuí respostas cadastradas " );
        }

        super.deleteLogic( id, usuarioAtualizacao );
    }


    @Override
    @Transactional( rollbackFor = Exception.class )
    public List< QuestionarioAvaliacao > associar( QuestionarioAvaliacao avaliacao, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( avaliacao );
        Assert.notNull( avaliacao.getPesquisa() );
        Assert.notNull( avaliacao.getPesquisa().getId() );
        Assert.notNull( avaliacao.getFuncionarioAvaliado() );
        Assert.notNull( avaliacao.getFuncionarioAvaliado().getId() );
        Assert.notNull( avaliacao.getFuncionarioAvaliador() );
        Assert.notNull( avaliacao.getFuncionarioAvaliador().getId() );

        // if ( associacaoJaExiste( avaliacao ) ) {
        // throw new ServiceException( "Impossível Gerar Associação. Associação já
        // existe" );
        // }

        removerAssociacoesExistententes( avaliacao );

        Pesquisa pesquisa = pesquisaService.findById( avaliacao.getPesquisa().getId(), Arrays.asList( "questionarios q", "[q].gruposOcupacionais go" ) );

        if ( CollectionUtils.isEmpty( pesquisa.getQuestionarios() ) ) {
            throw new ServiceException( "Impossível Gerar Associação. Pesquisa não possuí questionários" );
        }

        // avaliador = funcionarioService.findById(avaliador.getId(),
        // Arrays.asList("cargo c", "[c].grupoOcupacional go"));
        Funcionario avaliado = funcionarioService.findById( avaliacao.getFuncionarioAvaliado().getId(), Arrays.asList( "cargo c", "[c].grupoOcupacional go" ) );

        if ( avaliado.getCargo() == null || avaliado.getCargo().getGrupoOcupacional() == null ) {
            throw new ServiceException( "Impossível gerar associação. O funcionário avaliado não possuí Cargou ou o Cargo não possuí Grupo Ocupacional" );
        }

        List< QuestionarioAvaliacao > avaliacoes = new ArrayList<>();

        for ( Questionario questionario : pesquisa.getQuestionarios() ) {

            if ( questionario.getExcluido() == null || !questionario.getExcluido() ) {

                boolean hasGrupoOcupacional = false;
                boolean hasCargo = false;
                boolean hasSetor = false;

                if ( CollectionUtils.isNotEmpty( questionario.getGruposOcupacionais() ) && avaliado.getCargo() != null
                    && avaliado.getCargo().getGrupoOcupacional() != null ) {
                    for ( GrupoOcupacional go : questionario.getGruposOcupacionais() ) {
                        if ( go.getId().equals( avaliado.getCargo().getGrupoOcupacional().getId() ) ) {
                            hasGrupoOcupacional = true;
                            break;
                        }
                    }
                }
                if ( CollectionUtils.isNotEmpty( questionario.getCargos() ) && avaliado.getCargo() != null ) {
                    for ( Cargo cargo : questionario.getCargos() ) {
                        if ( cargo.getId().equals( avaliado.getCargo().getId() ) ) {
                            hasCargo = true;
                            break;
                        }
                    }
                }
                if ( CollectionUtils.isNotEmpty( questionario.getSetores() ) && avaliado.getSetor() != null ) {
                    for ( Setor setor : questionario.getSetores() ) {
                        if ( setor.getId().equals( avaliado.getSetor().getId() ) ) {
                            hasSetor = true;
                            break;
                        }
                    }
                }

                if ( hasGrupoOcupacional || hasCargo || hasSetor ) {
                    QuestionarioAvaliacao avaliacaoGerada = new QuestionarioAvaliacao();
                    avaliacaoGerada.setQuestionario( questionario );
                    avaliacaoGerada.setPesquisa( pesquisa );
                    avaliacaoGerada.setFuncionarioAvaliador( avaliacao.getFuncionarioAvaliador() );
                    avaliacaoGerada.setFuncionarioAvaliado( avaliado );
                    avaliacoes.add( avaliacaoGerada );
                }

            }

        }

        if ( avaliacoes.isEmpty() ) {
            throw new ServiceException(
                "Impossível gerar associação. O Grupo Ocupacional, Cargo, Setor do Funcionário avaliado não é compatível com nenhum questionário desta pesquisa " );
        }

        return saveAll( avaliacoes, usuarioCadastro );

    }


    @Transactional( readOnly = true )
    public boolean associacaoJaExiste( QuestionarioAvaliacao avaliacao ) {

        Assert.notNull( avaliacao );
        Assert.notNull( avaliacao.getPesquisa() );
        Assert.notNull( avaliacao.getPesquisa().getId() );
        Assert.notNull( avaliacao.getFuncionarioAvaliado() );
        Assert.notNull( avaliacao.getFuncionarioAvaliado().getId() );
        Assert.notNull( avaliacao.getFuncionarioAvaliador() );
        Assert.notNull( avaliacao.getFuncionarioAvaliador().getId() );

        List< String > fieldsToFetch = Arrays.asList( "pesquisa pesquisa", "funcionarioAvaliador avaliador", "funcionarioAvaliado avaliado" );
        Map< String, Object > fieldsToSearch = new HashMap<>();
        fieldsToSearch.put( "[pesquisa].id", avaliacao.getPesquisa().getId() );
        fieldsToSearch.put( "[avaliado].id", avaliacao.getFuncionarioAvaliado().getId() );
        fieldsToSearch.put( "[avaliador].id", avaliacao.getFuncionarioAvaliador().getId() );
        List< QuestionarioAvaliacao > avaliacoes = dao.findAvaliacao( fieldsToFetch, fieldsToSearch );

        return CollectionUtils.isNotEmpty( avaliacoes );
    }


    public void removerAssociacoesExistententes( QuestionarioAvaliacao avaliacao ) {

        Assert.notNull( avaliacao );
        Assert.notNull( avaliacao.getPesquisa() );
        Assert.notNull( avaliacao.getPesquisa().getId() );
        Assert.notNull( avaliacao.getFuncionarioAvaliado() );
        Assert.notNull( avaliacao.getFuncionarioAvaliado().getId() );
        Assert.notNull( avaliacao.getFuncionarioAvaliador() );
        Assert.notNull( avaliacao.getFuncionarioAvaliador().getId() );

        dao.deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
            avaliacao.getPesquisa(),
            null,
            avaliacao.getFuncionarioAvaliado(),
            avaliacao.getFuncionarioAvaliador() );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByPesquisa( Long idPesquisa )
        throws ServiceException {

        if ( idPesquisa == null ) {
            throw new ServiceException( "findByPesquisa() Pârametro idPesquisa nulo" );
        }

        List< String > fieldsToFetch = Arrays.asList( "pesquisa pesquisa", "funcionarioAvaliador avaliador", "funcionarioAvaliado avaliado" );
        Map< String, Object > fieldsToSearch = new HashMap<>();
        fieldsToSearch.put( "[pesquisa].id", idPesquisa );
        return dao.findAvaliacao( fieldsToFetch, fieldsToSearch );

    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByPesquisa( Pesquisa pesquisa ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( pesquisa.getId(), "Parameter pesquisa.id is required." );

        return dao.findByPesquisa( pesquisa );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByFuncionarioAvaliadoAndAvaliadorAndPesquisa(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa ) {

        Assert.notNull( funcionarioAvaliado, "Parameter funcionarioAvaliado is required." );
        Assert.notNull( funcionarioAvaliador, "Parameter funcionarioAvaliador is required." );
        Assert.notNull( pesquisa, "Parameter pesquisa is required." );

        return dao.findByFuncionarioAvaliadoAndAvaliadorAndPesquisa( funcionarioAvaliado, funcionarioAvaliador, pesquisa );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( questionario, "Parameter questionario is required." );

        return dao.findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador( pesquisa, questionario, funcionarioAvaliado, funcionarioAvaliador );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionario( Pesquisa pesquisa, Questionario questionario ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( questionario, "Parameter questionario is required." );

        return dao.findByPesquisaAndQuestionario( pesquisa, questionario );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( questionario, "Parameter questionario is required." );

        return dao.findByPesquisaAndQuestionarioAndAvaliadoAndTipo(
            pesquisa,
            questionario,
            funcionarioAvaliado,
            null,
            autoAvaliacao,
            avaliacaoSubordinado,
            avaliacaoSuperior );
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioAvaliacao > findByPesquisaAndQuestionarioAndAvaliadoAndAvaliadorAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( questionario, "Parameter questionario is required." );

        return dao.findByPesquisaAndQuestionarioAndAvaliadoAndTipo(
            pesquisa,
            questionario,
            funcionarioAvaliado,
            funcionarioAvaliador,
            autoAvaliacao,
            avaliacaoSubordinado,
            avaliacaoSuperior );
    }


    @Transactional( readOnly = true )
    @Override
    public QuestionarioAvaliacao findByFuncionarioAvaliadoAndAvaliadorAndPesquisaAndQuestionario(
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador,
        Pesquisa pesquisa,
        Questionario questionario ) {

        Assert.notNull( funcionarioAvaliado, "Parameter funcionarioAvaliado is required." );
        Assert.notNull( funcionarioAvaliador, "Parameter funcionarioAvaliador is required." );
        Assert.notNull( pesquisa, "Parameter pesquisa is required." );

        return dao.findByFuncionarioAvaliadoAndAvaliadorAndPesquisaAndQuestionario( funcionarioAvaliado, funcionarioAvaliador, pesquisa, questionario );
    }


    @Transactional
    @Override
    public void deleteByPesquisa( Pesquisa pesquisa ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( pesquisa.getId(), "Parameter pesquisa.id is required." );

        dao.deleteByPesquisa( pesquisa );
    }


    @Transactional
    @Override
    public void deleteByPesquisaAndQuestionarioAndFuncionarioAvaliado( Pesquisa pesquisa, Questionario questionario, Funcionario funcionarioAvaliado ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( pesquisa.getId(), "Parameter pesquisa.id is required." );

        Assert.notNull( questionario, "Parameter questionario is required." );
        Assert.notNull( questionario.getId(), "Parameter questionario.id is required." );

        Assert.notNull( funcionarioAvaliado, "Parameter funcionarioAvaliado is required." );
        Assert.notNull( funcionarioAvaliado.getId(), "Parameter funcionarioAvaliado.id is required." );

        dao.deleteByPesquisaAndQuestionarioAndFuncionarioAvaliado( pesquisa, questionario, funcionarioAvaliado );
    }


    @Transactional
    @Override
    public void deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Funcionario funcionarioAvaliador ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( pesquisa.getId(), "Parameter pesquisa.id is required." );

        Assert.notNull( questionario, "Parameter questionario is required." );
        Assert.notNull( questionario.getId(), "Parameter questionario.id is required." );

        Assert.notNull( funcionarioAvaliado, "Parameter funcionarioAvaliado is required." );
        Assert.notNull( funcionarioAvaliado.getId(), "Parameter funcionarioAvaliado.id is required." );

        Assert.notNull( funcionarioAvaliador, "Parameter funcionarioAvaliador is required." );
        Assert.notNull( funcionarioAvaliador.getId(), "Parameter funcionarioAvafuncionarioAvaliadorliado.id is required." );

        dao.deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador( pesquisa, questionario, funcionarioAvaliado, funcionarioAvaliador );
    }


    @Transactional
    @Override
    public void deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo(
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado,
        Boolean autoAvaliacao,
        Boolean avaliacaoSubordinado,
        Boolean avaliacaoSuperior ) {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( pesquisa.getId(), "Parameter pesquisa.id is required." );

        Assert.notNull( questionario, "Parameter questionario is required." );
        Assert.notNull( questionario.getId(), "Parameter questionario.id is required." );

        Assert.notNull( funcionarioAvaliado, "Parameter funcionarioAvaliado is required." );
        Assert.notNull( funcionarioAvaliado.getId(), "Parameter funcionarioAvaliado.id is required." );

        dao.deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo(
            pesquisa,
            questionario,
            funcionarioAvaliado,
            autoAvaliacao,
            avaliacaoSubordinado,
            avaliacaoSuperior );
    }


    @Override
    @Transactional( readOnly = true )
    public List< QuestionarioAvaliacao > findByPesquisaAndAvaliado( Long idPesquisa, Long idFuncionarioAvaliado ) {

        Assert.notNull( idPesquisa );
        Assert.notNull( idFuncionarioAvaliado );

        return findByPesquisaAndAvaliado( idPesquisa, Collections.singletonList( idFuncionarioAvaliado ) );

    }


    @Override
    @Transactional( readOnly = true )
    public List< QuestionarioAvaliacao > findByPesquisaAndAvaliado( Long idPesquisa, List< Long > idsFuncionarioAvaliado ) {

        Assert.notNull( idPesquisa );
        Assert.notEmpty( idsFuncionarioAvaliado );

        return dao.findByPesquisaAndAvaliado( idPesquisa, idsFuncionarioAvaliado );

    }
}
