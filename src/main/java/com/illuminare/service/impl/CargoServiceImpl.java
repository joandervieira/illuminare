package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.CargoDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.CargoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class CargoServiceImpl extends GenericServiceImpl< Cargo > implements CargoService {

    @Autowired
    CargoDAO dao;

    @Autowired
    UserService userService;

    @Autowired
    EmpresaService empresaService;
    @Autowired
    FuncionarioService funcionarioService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< Cargo > getDao() {

        return dao;
    }


    @Override
    public Paginator< Cargo > pesquisar( Paginator< Cargo > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Cargo >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( Cargo entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( Cargo entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getObs() != null ) {
                fields.put( "obs", entity.getObs() );
            }
            if ( entity.getDescricaoSumaria() != null ) {
                fields.put( "descricaoSumaria", entity.getDescricaoSumaria() );
            }
            if ( entity.getDescricaoDetalhada() != null ) {
                fields.put( "descricaoDetalhada", entity.getDescricaoDetalhada() );
            }
            if ( entity.getCargaHoraria() != null ) {
                fields.put( "cargaHoraria", entity.getCargaHoraria() );
            }
            if ( entity.getGrupoOcupacional() != null && entity.getGrupoOcupacional().getId() != null ) {
                fields.put( "grupoOcupacional.id", entity.getGrupoOcupacional().getId() );
            }
        }

        return fields;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Cargo > findByNome( Long idEmpresa, String nome )
        throws ServiceException {

        return findByNome( idEmpresa, nome, null );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Cargo > findByNome( Long idEmpresa, String nome, List< String > fieldsToFetch )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "nome", nome );
        if ( idEmpresa != null ) {
            attrs.put( "idEmpresa", idEmpresa );
        }
        return dao.findByAttr( attrs, fieldsToFetch );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap<>();
        Cargo cargo = new Cargo();
        cargo.setId( id );
        attrs.put( "cargo", cargo );
        List< Funcionario > funcionarios = funcionarioService.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( funcionarios ) ) {
            for ( Funcionario e : funcionarios ) {
                if ( e.getExcluido() == null || !e.getExcluido() ) {
                    throw new ServiceException( "Impossível Excluir. Cargo está associado a um funcionário" );
                }
            }
        }
        super.deleteLogic( id, usuarioAtualizacao );
    }

}
