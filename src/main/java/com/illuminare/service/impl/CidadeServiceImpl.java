package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.CidadeDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.Cidade;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.CidadeService;
import com.illuminare.service.UserService;


@Service
public class CidadeServiceImpl extends GenericServiceImpl< Cidade > implements CidadeService {

    @Autowired
    CidadeDAO dao;

    @Autowired
    private UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< Cidade > getDao() {

        return dao;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Cidade > findByEstado( Long idEstado )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "estado.id", idEstado );
        return dao.findByAttr( attrs );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Cidade > findByNome( String nome )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "nome", nome );
        return dao.findByAttr( attrs );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Cidade > findByNomeCidadeAndEstado( String nome, Long idEstado )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "estado.id", idEstado );
        attrs.put( "nome", nome );
        return dao.findByAttr( attrs );
    }

}
