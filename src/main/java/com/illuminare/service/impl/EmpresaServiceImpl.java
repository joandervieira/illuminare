package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.EmpresaDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.Empresa;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.CargoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.service.GrupoQuestaoService;
import com.illuminare.service.ListaValoresService;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.SetorService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class EmpresaServiceImpl extends GenericServiceImpl< Empresa > implements EmpresaService {

    @Autowired
    EmpresaDAO dao;

    @Autowired
    private UserService userService;

    @Autowired
    private CargoService cargoService;

    @Autowired
    private FuncionarioService funcionarioService;

    @Autowired
    private GrupoOcupacionalService grupoOcupacionalService;

    @Autowired
    private GrupoQuestaoService grupoQuestaoService;

    @Autowired
    private ListaValoresService listaValoresService;

    @Autowired
    private PesquisaService pesquisaService;

    @Autowired
    private QuestaoService questaoService;

    @Autowired
    private QuestionarioService questionarioService;

    @Autowired
    private SetorService setorService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< Empresa > getDao() {

        return dao;
    }


    @Transactional
    @Override
    public void deleteLogic( Long id, String userName )
        throws ServiceException {

        if ( CollectionUtils.isNotEmpty( funcionarioService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( cargoService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( grupoOcupacionalService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( grupoQuestaoService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( listaValoresService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( pesquisaService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( questaoService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( questionarioService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }
        if ( CollectionUtils.isNotEmpty( setorService.findByIdEmpresa( id ) ) ) {
            throw new ServiceException( "Impossível excluir. Empresa já está associada a outro registro. " );
        }

        super.deleteLogic( id, userName );
    }


    @Transactional( readOnly = true )
    @Override
    public Paginator< Empresa > pesquisar( Paginator< Empresa > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Empresa >();
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        return paginator;
    }


    private Map< String, Object > mapFields( Empresa entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getCnpj() != null ) {
                fields.put( "cnpj", entity.getCnpj() );
            }
        }

        return fields;
    }


    @Transactional( readOnly = true )
    @Override
    public Empresa findByCNPJ( String cnpj ) {

        Assert.isTrue( StringUtils.isNotBlank( cnpj ), "Parametro CNPJ é obrigatório" );

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "cnpj", cnpj );

        List< Empresa > empresas = dao.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( empresas ) ) {
            return empresas.get( 0 );
        }
        return null;
    }

}
