package com.illuminare.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.illuminare.dao.EstadoCivilDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.EstadoCivil;
import com.illuminare.service.EstadoCivilService;
import com.illuminare.service.UserService;


@Service
public class EstadoCivilServiceImpl extends GenericServiceImpl< EstadoCivil > implements EstadoCivilService {

    // private static final Logger log =
    // Logger.getLogger(EstadoCivilServiceImpl.class);

    @Autowired
    EstadoCivilDAO dao;

    @Autowired
    private UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< EstadoCivil > getDao() {

        return dao;
    }

}
