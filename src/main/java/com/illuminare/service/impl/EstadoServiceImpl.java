package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.EstadoDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.Estado;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.EstadoService;
import com.illuminare.service.UserService;


@Service
public class EstadoServiceImpl extends GenericServiceImpl< Estado > implements EstadoService {

    @Autowired
    EstadoDAO dao;

    @Autowired
    UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< Estado > getDao() {

        return dao;
    }


    @Override
    @Transactional( readOnly = true )
    public Estado findByUf( String uf )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "uf", uf );
        List< Estado > list = dao.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( list ) ) {
            return list.get( 0 );
        } else {
            return null;
        }
    }

}
