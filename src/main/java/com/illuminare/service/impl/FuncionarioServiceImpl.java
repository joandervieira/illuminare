package com.illuminare.service.impl;


import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.FuncionarioDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Cidade;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Endereco;
import com.illuminare.entity.EnderecoEletronico;
import com.illuminare.entity.Estado;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Lider;
import com.illuminare.entity.Pessoa;
import com.illuminare.entity.Setor;
import com.illuminare.entity.Telefone;
import com.illuminare.entity.User;
import com.illuminare.enums.RoleEnum;
import com.illuminare.exceptions.ConvertFromXlsException;
import com.illuminare.exceptions.DAOException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.CargoService;
import com.illuminare.service.CidadeService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.EstadoService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.service.SetorService;
import com.illuminare.service.TipoEnderecoEletronicoService;
import com.illuminare.service.TipoEnderecoService;
import com.illuminare.service.TipoTelefoneService;
import com.illuminare.service.UserService;
import com.illuminare.util.AppStringUtils;
import com.illuminare.util.Constantes;
import com.illuminare.util.ImportUtils;
import com.illuminare.view.ImportFuncionarioView;
import com.illuminare.view.Paginator;
import com.illuminare.view.PreviewFuncionariosImportacao;


@Service
public class FuncionarioServiceImpl extends GenericServiceImpl< Funcionario > implements FuncionarioService {

    Logger log = Logger.getLogger( FuncionarioServiceImpl.class );

    @Autowired
    FuncionarioDAO dao;
    @Autowired
    UserService userService;
    @Autowired
    TipoEnderecoEletronicoService tipoEnderecoEletronicoService;
    @Autowired
    TipoEnderecoService tipoEnderecoService;
    @Autowired
    TipoTelefoneService tipoTelefoneService;
    @Autowired
    CidadeService cidadeService;
    @Autowired
    EstadoService estadoService;
    @Autowired
    CargoService cargoService;
    @Autowired
    GrupoOcupacionalService grupoOcupacionalService;
    @Autowired
    SetorService setorService;
    @Autowired
    private EmpresaService empresaService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< Funcionario > getDao() {

        return dao;
    }


    @Override
    @Transactional( readOnly = true )
    public Paginator< Funcionario > pesquisar( Paginator< Funcionario > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Funcionario >();
        }

        User user = userService.findByName( usuarioLogado );
        Funcionario userFuncionario = user.getFuncionario();
        if ( userFuncionario != null ) {
            user.setFuncionario( dao.unproxyInitialize( user.getFuncionario() ) );
            userFuncionario = user.getFuncionario();
        }
        if ( !userService.isAdmin( user ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }

        if ( user.containsRole( RoleEnum.GESTOR_SETOR.getName() ) ) {
            // pegar setor do usuario logado (do objeto funcionario)
            if ( userFuncionario == null ) {
                throw new ServiceException( "Usuário não possuí funcionário associado." );
            }
            if ( userFuncionario.getSetor() == null ) {
                throw new ServiceException( "Usuário não possuí setor associado." );
            }
            paginator.getEntity().setSetor( userFuncionario.getSetor() );
        }

        // FIELDS TO SEARCH
        paginator.setFieldsToSearch( mapFieldsToSearch( paginator.getEntity() ) );

        // FIND
        paginator = dao.pesquisar( paginator );

        // problema com o proxy do hibernate quando o resultado retorna um
        // funcionario igual ao do usuario logado.
        if ( userFuncionario != null ) {
            if ( CollectionUtils.isNotEmpty( paginator.getResult() ) ) {
                ArrayList< Funcionario > result = new ArrayList<>();
                for ( Funcionario funcionario : paginator.getResult() ) {
                    if ( userFuncionario.getId().equals( funcionario.getId() ) ) {
                        funcionario = unProxyFuncionarioPesquisa( funcionario );
                    }

                    result.add( funcionario );
                }
                paginator.setResult( result );
            }
        }

        if ( paginator.getResult() != null ) {
            for ( Funcionario entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }

                if ( entity.getSetor() != null ) {
                    entity.setSetor( dao.unproxyInitialize( entity.getSetor() ) );
                }
            }

        }

        return paginator;
    }


    private Funcionario unProxyFuncionarioPesquisa( Funcionario funcionario ) {

        if ( funcionario != null ) {
            funcionario.setPessoa( dao.unproxyInitialize( funcionario.getPessoa() ) );
            if ( funcionario.getCargo() != null ) {
                funcionario.getCargo().setGrupoOcupacional( dao.unproxyInitialize( funcionario.getCargo().getGrupoOcupacional() ) );
                funcionario.setCargo( dao.unproxyInitialize( funcionario.getCargo() ) );
            }
            funcionario.setSetor( dao.unproxyInitialize( funcionario.getSetor() ) );
            funcionario = dao.unproxyInitialize( funcionario );
            return funcionario;
        }
        return null;
    }


    private Map< String, Object > mapFieldsToSearch( Funcionario entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getAtivo() != null ) {
                fields.put( "ativo", entity.getAtivo() );
            }
            if ( entity.getMatricula() != null ) {
                fields.put( "matricula", entity.getMatricula() );
            }
            if ( entity.getSetor() != null && entity.getSetor().getId() != null ) {
                fields.put( "setor.id", entity.getSetor().getId() );
            }
            if ( entity.getCargo() != null && entity.getCargo().getId() != null ) {
                fields.put( "cargo.id", entity.getCargo().getId() );
            }
            if ( entity.getGrupoOcupacional() != null && entity.getGrupoOcupacional().getId() != null ) {
                fields.put( "[c].grupoOcupacional.id", entity.getGrupoOcupacional().getId() );
            }
            if ( entity.getPessoa() != null ) {
                if ( entity.getCargo() != null && entity.getCargo().getId() != null ) {
                    fields.put( "pessoa.cargo.id", entity.getCargo().getId() );
                }
                if ( entity.getSetor() != null && entity.getSetor().getId() != null ) {
                    fields.put( "pessoa.setor.id", entity.getSetor().getId() );
                }
                if ( entity.getPessoa().getNome() != null ) {
                    fields.put( "pessoa.nome", entity.getPessoa().getNome() );
                }
                if ( entity.getPessoa().getAtivo() != null ) {
                    fields.put( "pessoa.ativo", entity.getPessoa().getAtivo() );
                }
                if ( entity.getPessoa().getExcluido() != null ) {
                    fields.put( "pessoa.excluido", entity.getPessoa().getExcluido() );
                }
                if ( entity.getPessoa().getDocumento() != null ) {
                    fields.put( "pessoa.documento", entity.getPessoa().getDocumento() );
                }
                if ( entity.getPessoa().getDtaNascimento() != null ) {
                    fields.put( "pessoa.dtaNascimento", entity.getPessoa().getDtaNascimento() );
                }
                if ( entity.getPessoa().getSexo() != null ) {
                    fields.put( "pessoa.sexo", entity.getPessoa().getSexo() );
                }
                if ( entity.getPessoa().getEstadoCivil() != null && entity.getPessoa().getEstadoCivil().getId() != null ) {
                    fields.put( "pessoa.estadoCivil.id", entity.getPessoa().getEstadoCivil().getId() );
                }
                if ( entity.getPessoa().getEnderecos() != null ) {
                    for ( Endereco end : entity.getPessoa().getEnderecos() ) {
                        if ( end.getLogradouro() != null ) {
                            fields.put( "pessoa.enderecos.logradouro", end.getLogradouro() );
                        }
                        if ( end.getNumero() != null ) {
                            fields.put( "pessoa.enderecos.numero", end.getNumero() );
                        }
                        if ( end.getBairro() != null ) {
                            fields.put( "pessoa.enderecos.bairro", end.getBairro() );
                        }
                    }
                }
            }

        }

        return fields;
    }


    @Override
    @Transactional( rollbackFor = Exception.class )
    public Funcionario save( Funcionario funcionario, String usuarioCadastro )
        throws ServiceException {

        if ( funcionario != null && funcionario.getPessoa() != null ) {

            funcionario.getPessoa().setTipoPessoa( Constantes.TIPO_PESSOA_PF );
            funcionario.setDataCadastro( new Date() );
            funcionario.setUsuarioCadastro( usuarioCadastro );

            if ( funcionario.getIdEmpresa() == null ) {
                throw new ServiceException( "Erro ao salvar funcionário. Empresa é obrigatório" );
            }

            funcionario = validarSaveUpdate( funcionario );

            Funcionario f = findByCpf( funcionario.getIdEmpresa(), funcionario.getPessoa().getDocumento() );
            if ( f != null ) {
                throw new ServiceException( "CPF já está cadastrado para o funcionário: " + f.getPessoa().getNome() );
            }

            if ( funcionario.getMatricula() != null ) {

                Funcionario fMatricula = findByMatricula( funcionario.getIdEmpresa(), funcionario.getMatricula(), Arrays.asList( "pessoa" ) );
                if ( fMatricula != null ) {
                    throw new ServiceException( "Já existe um funcionário cadastrado com a matrícula: [" + fMatricula.getMatricula() + "] - ["
                        + fMatricula.getPessoa().getNome() + "]" );
                }
            }

            funcionario = verificarCargo( funcionario, usuarioCadastro );

            funcionario = verificarLider( funcionario, usuarioCadastro );

            funcionario = verificarSetor( funcionario, usuarioCadastro );

            funcionario = dao.save( funcionario );

            try {
                userService.cadastrarUsuarioFuncionario( funcionario, usuarioCadastro );
            } catch ( ServiceException e ) {
                throw new ServiceException( "Erro ao cadastrar Usuário do Funcionário de Acesso ao Sistema: " + e.getMessage() );
            }

            return funcionario;

        } else {
            throw new ServiceException( "Campos obrigatórios não preenchidos" );
        }
    }


    private Funcionario verificarCargo( Funcionario funcionario, String usuarioCadastro )
        throws ServiceException {

        if ( funcionario.getCargo() != null && funcionario.getCargo().getId() != null ) {
            Cargo cargo = cargoService.findById( funcionario.getCargo().getId() );
            cargo.setIdEmpresa( funcionario.getIdEmpresa() );
            GrupoOcupacional grupoOcupacional = cargo.getGrupoOcupacional();
            cargoService.update( cargo, usuarioCadastro );
            if ( grupoOcupacional != null ) {
                grupoOcupacional.setIdEmpresa( funcionario.getIdEmpresa() );
                grupoOcupacionalService.update( grupoOcupacional, usuarioCadastro );
            }
        }

        if ( funcionario.getCargo() != null && funcionario.getCargo().getId() == null ) {
            GrupoOcupacional grupoOcupacional = null;
            if ( funcionario.getCargo().getGrupoOcupacional() != null ) {
                if ( funcionario.getCargo().getGrupoOcupacional().getId() != null ) {
                    grupoOcupacional = funcionario.getCargo().getGrupoOcupacional();
                    grupoOcupacional.setIdEmpresa( funcionario.getIdEmpresa() );
                    grupoOcupacionalService.update( grupoOcupacional, usuarioCadastro );
                } else {
                    if ( funcionario.getCargo().getGrupoOcupacional().getNome() != null ) {
                        List< GrupoOcupacional > grupos =
                            grupoOcupacionalService.findByNome( funcionario.getIdEmpresa(), funcionario.getCargo().getGrupoOcupacional().getNome() );
                        if ( grupos != null && grupos.size() == 1 ) {
                            grupoOcupacional = grupos.get( 0 );
                            grupoOcupacional.setIdEmpresa( funcionario.getIdEmpresa() );
                            grupoOcupacionalService.update( grupoOcupacional, usuarioCadastro );
                        } else {
                            grupoOcupacional = funcionario.getCargo().getGrupoOcupacional();
                            grupoOcupacional.setIdEmpresa( funcionario.getIdEmpresa() );
                            grupoOcupacional = grupoOcupacionalService.save( grupoOcupacional, usuarioCadastro );
                        }
                    } else {
                        throw new ServiceException( "Erro ao salvar funcionário " + funcionario.getPessoa().getNome() + ", Grupo Ocupacional não informado." );
                    }
                }
                if ( funcionario.getCargo().getNome() == null ) {
                    throw new ServiceException( "Erro ao salvar funcionário " + funcionario.getPessoa().getNome() + ", Cargo não informado." );
                } else {
                    Cargo cargo = null;
                    List< Cargo > cargos = cargoService.findByNome( funcionario.getIdEmpresa(), funcionario.getCargo().getNome() );
                    if ( cargos != null && cargos.size() == 1 ) {
                        cargo = cargos.get( 0 );
                        cargo.setIdEmpresa( funcionario.getIdEmpresa() );
                        cargoService.update( cargo, usuarioCadastro );
                    } else {
                        cargo = funcionario.getCargo();
                        cargo.setGrupoOcupacional( grupoOcupacional );
                        cargo.setIdEmpresa( funcionario.getIdEmpresa() );
                        cargo = cargoService.save( cargo, usuarioCadastro );
                    }
                    funcionario.setCargo( cargo );
                }
            }
        }

        return funcionario;
    }


    private Funcionario verificarLider( Funcionario funcionario, String usuarioCadastro )
        throws ServiceException {

        if ( funcionario.getLider() != null && funcionario.getLider().getId() == null ) {
            if ( funcionario.getLider().getMatricula() != null ) {
                if ( funcionario.getIdEmpresa() == null ) {
                    throw new ServiceException( "Erro ao verificarLider(). Empresa é obrigatório" );
                }
                Funcionario funcLider = findByMatricula( funcionario.getIdEmpresa(), funcionario.getLider().getMatricula() );
                if ( funcLider != null ) {
                    funcionario.setLider( new Lider( funcLider.getId(), funcLider.getMatricula() ) );
                } else {
                    if ( funcionario.getLider().getPessoa() != null ) {
                        if ( funcionario.getLider().getPessoa().getNome() != null && funcionario.getLider().getPessoa().getDocumento() != null ) {
                            if ( funcionario.getLider().getMatricula() != null && funcionario.getLider().getCargo() != null ) {
                                funcLider = liderToFuncionario( funcionario.getLider() );
                                funcLider = save( funcLider, usuarioCadastro );
                                funcionario.setLider( new Lider( funcLider.getId(), funcLider.getMatricula() ) );
                            }
                        }
                    }
                }
            }
        }
        return funcionario;
    }


    private Funcionario verificarSetor( Funcionario funcionario, String usuarioCadastro )
        throws ServiceException {

        if ( funcionario != null && funcionario.getSetor() != null && funcionario.getSetor().getId() != null ) {
            Setor setor = setorService.findById( funcionario.getSetor().getId() );
            setor.setIdEmpresa( funcionario.getIdEmpresa() );
            setorService.update( setor, usuarioCadastro );
        }

        if ( funcionario != null && funcionario.getSetor() != null ) {
            Setor setor = null;
            if ( funcionario.getSetor().getId() != null ) {
                setor = setorService.findById( funcionario.getSetor().getId() );
            } else {
                if ( funcionario.getSetor().getNome() != null ) {
                    List< Setor > setores = setorService.findByNome( funcionario.getIdEmpresa(), funcionario.getSetor().getNome() );
                    if ( setores != null && setores.size() == 1 ) {
                        setor = setores.get( 0 );
                        setor.setIdEmpresa( funcionario.getIdEmpresa() );
                        setorService.update( setor, usuarioCadastro );
                    } else {
                        setor = funcionario.getSetor();
                        setor.setIdEmpresa( funcionario.getIdEmpresa() );
                        setor = setorService.save( setor, usuarioCadastro );
                    }
                }
            }

            funcionario.setSetor( setor );
        }

        return funcionario;
    }


    private Funcionario liderToFuncionario( Lider lider ) {

        if ( lider != null ) {
            Funcionario func = new Funcionario();
            func.setId( lider.getId() );
            func.setIdEmpresa( lider.getIdEmpresa() );
            func.setMatricula( lider.getMatricula() );
            func.setPessoa( lider.getPessoa() );
            func.setSetor( lider.getSetor() );
            func.setCargo( lider.getCargo() );
            func.setAtivo( lider.getAtivo() );
            func.setGrupoOcupacional( lider.getGrupoOcupacional() );
            func.setDataAtualizacao( lider.getDataAtualizacao() );
            func.setDataCadastro( lider.getDataCadastro() );
            func.setExcluido( lider.getExcluido() );
            func.setUsuarioAtualizacao( lider.getUsuarioAtualizacao() );
            func.setUsuarioCadastro( lider.getUsuarioCadastro() );
            func.setFieldsToFetch( lider.getFieldsToFetch() );
            return func;
        }
        return null;
    }


    @Override
    @Transactional( rollbackFor = Exception.class )
    public void update( Funcionario funcionario, String usuarioAtualizacao )
        throws ServiceException {

        if ( funcionario != null ) {
            funcionario.setDataAtualizacao( new Date() );
            funcionario.setUsuarioAtualizacao( usuarioAtualizacao );
            funcionario = validarSaveUpdate( funcionario );

            if ( funcionario.getId() == null ) {
                throw new ServiceException( "Parâmetro nulo. [id]" );
            }
            if ( funcionario.getIdEmpresa() == null ) {
                String nome = "";
                if ( funcionario.getPessoa() != null ) {
                    nome = funcionario.getPessoa().getNome();
                }
                throw new ServiceException( "Erro ao atualizar funcionário. Empresa é obrigatório. " + funcionario.getMatricula() + nome );
            }

            Funcionario entityBase = findById( funcionario.getId(), new HashSet<>( Arrays.asList( "pessoa" ) ) );

            if ( entityBase.getPessoa() != null && entityBase.getPessoa().getDocumento() != null ) {
                if ( !entityBase.getPessoa().getDocumento().equals( funcionario.getPessoa().getDocumento() ) ) {
                    Funcionario f = findByCpf( funcionario.getIdEmpresa(), funcionario.getPessoa().getDocumento() );
                    if ( f != null ) {
                        throw new ServiceException( "CPF já está cadastrado para o funcionário: " + f.getPessoa().getNome() );
                    }
                }
            }

            if ( funcionario.getMatricula() != null && !funcionario.getMatricula().equals( entityBase.getMatricula() ) ) {
                Funcionario fMatricula = findByMatricula( funcionario.getIdEmpresa(), funcionario.getMatricula(), Arrays.asList( "pessoa" ) );
                if ( fMatricula != null && !fMatricula.getId().equals( funcionario.getId() ) ) {
                    throw new ServiceException( "Já existe um funcionário cadastrado com a matrícula: [" + fMatricula.getMatricula() + "] - ["
                        + fMatricula.getPessoa().getNome() + "]" );
                }
            }

            funcionario = verificarCargo( funcionario, usuarioAtualizacao );

            funcionario = verificarLider( funcionario, usuarioAtualizacao );

            funcionario = verificarSetor( funcionario, usuarioAtualizacao );

            dao.update( funcionario );
        }
    }


    private Funcionario validarSaveUpdate( Funcionario entity )
        throws ServiceException {

        if ( entity.getAtivo() == null ) {
            entity.setAtivo( true );
        }

        String salvarAtualizar = entity.getId() != null ? "atualizar" : "salvar";

        if ( entity.getPessoa() == null ) {
            throw new ServiceException( "Parâmetro nulo. [pessoa]" );
        }
        if ( StringUtils.isEmpty( entity.getPessoa().getDocumento() ) ) {
            throw new ServiceException( "Erro ao " + salvarAtualizar + " Funcionário " + entity.getPessoa().getNome() + ", CPF é obrigatório" );
        }
        if ( StringUtils.isEmpty( entity.getMatricula() ) ) {
            throw new ServiceException( "Erro ao " + salvarAtualizar + " Funcionário " + entity.getPessoa().getNome() + ", Matrícula é obrigatório" );
        }

        if ( entity.getLider() != null && entity.getLider().getId() != null ) {

            Funcionario funcionarioLider = dao.findById( entity.getLider().getId(), new LinkedHashSet<>( Arrays.asList( "lider l", "[l].pessoa" ) ) );

            if ( funcionarioLider.getId().equals( entity.getId() ) ) {
                throw new ServiceException( "Erro ao " + salvarAtualizar + " Funcionário " + entity.getPessoa().getNome()
                    + ", o Líder não pode ser o próprio funcionário, por favor, selecionar outro Líder" );
            }

            if ( funcionarioLider.getLider() != null && funcionarioLider.getLider().getId() != null ) {
                if ( funcionarioLider.getLider().getId().equals( entity.getId() ) ) {

                    String msgErro = "Erro ao " + salvarAtualizar + " " + getTratamentoFuncionarioA( entity ) + " " + entity.getPessoa().getNome() + ". ";
                    msgErro +=
                        funcionarioLider.getPessoa().getNome() + " não pode ser líder " + getTratamentoDoDa( entity ) + " " + entity.getPessoa().getNome();
                    msgErro += ", porque " + entity.getPessoa().getNome() + " já é lider " + getTratamentoDoDa( funcionarioLider ) + " "
                        + funcionarioLider.getPessoa().getNome();

                    throw new ServiceException( msgErro );
                }
            }

        }

        if ( entity.getPessoa().getEnderecos() != null ) {
            Set< Endereco > enderecos = new HashSet< Endereco >();
            for ( Endereco end : entity.getPessoa().getEnderecos() ) {
                if ( StringUtils.isNotBlank( end.getLogradouro() ) ) {
                    enderecos.add( end );
                }
            }
            entity.getPessoa().setEnderecos( enderecos );
        }

        if ( entity.getPessoa().getTelefones() != null ) {
            Set< Telefone > telefones = new HashSet< Telefone >();
            for ( Telefone tel : entity.getPessoa().getTelefones() ) {
                if ( StringUtils.isNotBlank( tel.getNumero() ) || StringUtils.isNotBlank( tel.getRamal() ) ) {
                    telefones.add( tel );
                }
            }
            entity.getPessoa().setTelefones( telefones );
        }

        if ( entity.getPessoa().getEnderecosEletronicos() != null ) {
            Set< EnderecoEletronico > endEletronicos = new HashSet< EnderecoEletronico >();
            for ( EnderecoEletronico endEletronico : entity.getPessoa().getEnderecosEletronicos() ) {
                if ( StringUtils.isNotBlank( endEletronico.getEndereco() ) ) {
                    endEletronicos.add( endEletronico );
                }
            }
            entity.getPessoa().setEnderecosEletronicos( endEletronicos );
        }

        return entity;
    }


    @Override
    @Transactional( readOnly = true )
    public Funcionario findByCpf( Long idEmpresa, String cpf ) {

        if ( StringUtils.isNotBlank( cpf ) ) {
            return dao.findByCpf( idEmpresa, cpf, null );
        }
        return null;
    }


    @Override
    @Transactional( readOnly = true )
    public Funcionario findByCpf( Long idEmpresa, String cpf, List< String > fieldsToFetch ) {

        if ( StringUtils.isNotBlank( cpf ) ) {
            return dao.findByCpf( idEmpresa, cpf, fieldsToFetch );
        }
        return null;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findSubordinados( Funcionario funcionario ) {

        Assert.notNull( funcionario );
        Assert.notNull( funcionario.getId() );
        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "lider.id", funcionario.getId() );
        return findByAttr( attrs );

    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findByGrupoOcupacional( Funcionario funcionario )
        throws ServiceException {

        if ( funcionario == null ) {
            throw new ServiceException( "Parâmetros nulo" );
        }

        if ( funcionario.getCargo() == null || funcionario.getCargo().getGrupoOcupacional() == null
            || funcionario.getCargo().getGrupoOcupacional().getId() == null ) {

            if ( funcionario.getId() == null ) {
                return null;
            }

            funcionario = findById( funcionario.getId(), new HashSet<>( Arrays.asList( "cargo" ) ) );

            if ( funcionario.getCargo() == null || funcionario.getCargo().getId() == null ) {
                return null;
            }
        }

        return dao.findByGrupoOcupacional( funcionario.getCargo().getGrupoOcupacional() );

    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findByGrupoOcupacional( GrupoOcupacional grupoOcupacional )
        throws ServiceException {

        if ( grupoOcupacional == null || grupoOcupacional.getId() == null ) {
            throw new ServiceException( "Parâmetros nulo" );
        }

        return dao.findByGrupoOcupacional( grupoOcupacional );

    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findByCargo( Cargo cargo )
        throws ServiceException {

        Assert.notNull( cargo );
        Assert.notNull( cargo.getId() );

        return dao.findByCargo( cargo );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findBySetor( Setor setor )
        throws ServiceException {

        Assert.notNull( setor );
        Assert.notNull( setor.getId() );

        return dao.findBySetor( setor );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findByEmpresa( Empresa empresa )
        throws ServiceException {

        Assert.notNull( empresa );
        Assert.notNull( empresa.getId() );

        return dao.findByEmpresa( empresa );
    }


    @Override
    @Transactional( readOnly = true )
    public Funcionario findByMatricula( Long idEmpresa, String matricula )
        throws ServiceException {

        Assert.notNull( matricula );
        return findByMatricula( idEmpresa, matricula, null );
    }


    @Override
    @Transactional( readOnly = true, propagation = Propagation.REQUIRES_NEW )
    public Funcionario findByMatricula( Long idEmpresa, String matricula, List< String > fieldsToFetch )
        throws ServiceException {

        Assert.notNull( matricula );

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "matricula", matricula );
        attrs.put( "idEmpresa", idEmpresa );
        List< Funcionario > funcionarios = dao.findByAttr( attrs, fieldsToFetch );
        if ( CollectionUtils.isNotEmpty( funcionarios ) ) {
            if ( funcionarios.size() > 1 ) {
                log.error( "findByMatricula() MAIS DE UM FUNCIONARIO ENCONTRADO COM A MESMA MATRICULA" );
            }
            return funcionarios.get( 0 );
        } else {
            return null;
        }

    }


    @Override
    @Transactional( propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class )
    public void confirmarImportacao( PreviewFuncionariosImportacao previewFuncionariosImportacao, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( usuarioCadastro, "Parameter funcionarios is required." );
        Assert.notNull( previewFuncionariosImportacao, "Parameter previewFuncionariosImportacao is required." );

        if ( CollectionUtils.isEmpty( previewFuncionariosImportacao.getFuncionariosImportados() ) ) {
            throw new ServiceException( "Parâmetro previewFuncionariosImportacao.funcionariosImportados é obrigatório" );
        }

        if ( previewFuncionariosImportacao.getIdEmpresa() == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }

        Long idEmpresa = previewFuncionariosImportacao.getIdEmpresa();
        Empresa empresa = empresaService.findById( idEmpresa );
        if ( empresa == null ) {
            throw new ServiceException( "Empresa não encontrada com ID=" + idEmpresa );
        }

        // limparCamposEInativandoTodosFuncionarios();
        limparCampoLiderTodosFuncionarios( idEmpresa );
        inativarTodosFuncionarios( idEmpresa );
        // excluirLogicamenteTodosFuncionarios();

        log.info( "Listando funcionarios para importar" );
        List< String > erros = new ArrayList<>();
        for ( Funcionario funcionario : previewFuncionariosImportacao.getFuncionariosImportados() ) {

            String nome = funcionario.getPessoa() != null ? funcionario.getPessoa().getNome() : null;
            log.info( String.format( "Funcionario id=%s, nome=%s, matricula=%s", funcionario.getId(), nome, funcionario.getMatricula() ) );
            salvarAtualizarFuncionarioImportado( funcionario, erros, idEmpresa, usuarioCadastro );

        }
        if ( !erros.isEmpty() ) {
            String msgsErros = "Erros de validação:<br>";
            for ( String erro : erros ) {
                msgsErros += erro + "<br>";
            }
            throw new ServiceException( msgsErros );
        }
    }


    private void salvarAtualizarFuncionarioImportado( Funcionario funcionario, List< String > erros, Long idEmpresa, String usuarioCadastro ) {

        log.info( "salvarAtualizarFuncionarioImportado()" );
        try {
            if ( funcionario.getId() != null ) {
                log.info( String.format( "Atualizando1 funcionario id=%s", funcionario.getId() ) );

                checkSalvarLider( funcionario, usuarioCadastro, idEmpresa );
                update( funcionario, usuarioCadastro );

            } else {
                Funcionario jaExiste = findByMatricula( idEmpresa, funcionario.getMatricula() );
                if ( jaExiste == null && funcionario.getPessoa() != null && funcionario.getPessoa().getDocumento() != null ) {
                    jaExiste = findByCpf( idEmpresa, funcionario.getPessoa().getDocumento() );
                }
                if ( jaExiste != null && jaExiste.getId() != null ) {
                    funcionario.setId( jaExiste.getId() );
                    log.info( String.format( "Atualizando2 funcionario id=%s", funcionario.getId() ) );
                    checkSalvarLider( funcionario, usuarioCadastro, idEmpresa );
                    update( funcionario, usuarioCadastro );
                } else {
                    log.info(
                        String.format(
                            "Salvando funcionario matricula %s, pessoa=%s, lider=%s",
                            funcionario.getMatricula(),
                            funcionario.getPessoa(),
                            funcionario.getLider() ) );

                    checkSalvarLider( funcionario, usuarioCadastro, idEmpresa );

                    log.info(
                        String.format(
                            "Salvando APOS CHECK LIDER  funcionario matricula %s, pessoa=%s, lider=%s",
                            funcionario.getMatricula(),
                            funcionario.getPessoa(),
                            funcionario.getLider() ) );

                    save( funcionario, usuarioCadastro );
                }
            }

        } catch ( ServiceException e ) {
            erros.add( e.getMessage() );
        }
    }


    private void checkSalvarLider( Funcionario funcionario, String usuarioCadastro, Long idEmpresa )
        throws ServiceException {

        boolean salvarLider = false;
        if ( funcionario != null && funcionario.getLider() != null && funcionario.getLider().getId() == null ) {
            log.info( "checkSalvarLider() do funcionario =" + funcionario );
            Lider lider = funcionario.getLider();

            if ( StringUtils.isNotBlank( lider.getMatricula() ) ) {

                if ( lider.getPessoa() != null && StringUtils.isNotBlank( lider.getPessoa().getDocumento() )
                    && StringUtils.isNotBlank( lider.getPessoa().getNome() ) ) {

                    salvarLider = true;
                }

            }

            log.info( "salvarLider=" + salvarLider );
            if ( salvarLider ) {

                Funcionario liderJaExiste = findByMatricula( idEmpresa, lider.getMatricula() );
                if ( liderJaExiste == null ) {
                    liderJaExiste = findByCpf( idEmpresa, lider.getPessoa().getDocumento() );
                }

                log.info( String.format( "liderJaExiste=%s", liderJaExiste ) );
                if ( liderJaExiste != null ) {

                    lider.setId( liderJaExiste.getId() );

                } else {

                    Funcionario newFuncLider = new Funcionario();
                    newFuncLider.setPessoa( lider.getPessoa() );
                    newFuncLider.setCargo( lider.getCargo() );
                    newFuncLider.setGrupoOcupacional( lider.getGrupoOcupacional() );
                    newFuncLider.setMatricula( lider.getMatricula() );
                    newFuncLider.setSetor( lider.getSetor() );
                    newFuncLider.setIdEmpresa( idEmpresa );

                    newFuncLider = save( newFuncLider, usuarioCadastro );
                    log.info( String.format( "newFuncLider=%s", newFuncLider ) );

                    lider.setId( newFuncLider.getId() );
                }

                funcionario.setLider( lider );

            } else {
                funcionario.setLider( null );
            }
        }
    }


    @Override
    @Transactional( propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class )
    public PreviewFuncionariosImportacao preConfirmarImportacao( PreviewFuncionariosImportacao previewFuncionariosImportacao, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( usuarioCadastro, "Parameter funcionarios is required." );
        Assert.notNull( previewFuncionariosImportacao, "Parameter previewFuncionariosImportacao is required." );

        if ( previewFuncionariosImportacao.getIdEmpresa() == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        if ( CollectionUtils.isEmpty( previewFuncionariosImportacao.getFuncionariosImportados() ) ) {
            throw new ServiceException( "Parametro funcionários é obrigatório" );
        }

        Long idEmpresa = previewFuncionariosImportacao.getIdEmpresa();
        Empresa empresa = empresaService.findById( idEmpresa );

        // VERIFICAR SE O FUNCIONARIO ESTA EM OUTRA EMPRESA
        // for ( Funcionario funcImport :
        // previewFuncionariosImportacao.getFuncionariosImportados() ) {
        // if ( funcImport.getId() != null ) {
        // if ( funcImport.getIdEmpresa() != null &&
        // !funcImport.getIdEmpresa().equals( idEmpresa ) ) {
        //
        // throw new ServiceException( "O funcionário " +
        // funcImport.getMatricula() + "-" + funcImport.getPessoa().getNome()
        // + " já está cadastrado na empresa " + empresa.getNome() + " (" +
        // empresa.getCnpj() + ")" );
        // }
        // }
        // }

        // VERIFICAR DESABILITADOS

        List< Funcionario > funcionariosDesabilitados = new ArrayList<>();

        List< Funcionario > funcionariosDb = listAll(
            idEmpresa,
            Arrays.asList(
                "pessoa pessoa",
                "lider",
                "setor",
                "cargo cargo",
                "[cargo].grupoOcupacional",
                "[pessoa].estadoCivil",
                "[pessoa].telefones",
                "[pessoa].enderecos",
                "[pessoa].enderecosEletronicos" ),
            usuarioCadastro );

        for ( Funcionario funcDb : funcionariosDb ) {
            boolean found = false;
            for ( Funcionario funcImport : previewFuncionariosImportacao.getFuncionariosImportados() ) {
                if ( funcImport.getId() != null ) {
                    if ( funcDb.getId().equals( funcImport.getId() ) ) {
                        found = true;
                        break;
                    }
                }
            }
            if ( !found ) {
                funcionariosDesabilitados.add( funcDb );
            }
        }

        PreviewFuncionariosImportacao previewFuncionariosImportacaoResult = new PreviewFuncionariosImportacao();
        previewFuncionariosImportacaoResult.setFuncionariosDesativados( funcionariosDesabilitados );
        previewFuncionariosImportacaoResult.setIdEmpresa( idEmpresa );

        return previewFuncionariosImportacaoResult;

    }


    @Transactional
    public void limparCampoLiderTodosFuncionarios( Long idEmpresa )
        throws ServiceException {

        Assert.notNull( idEmpresa, "Parameter idEmpresa is not required." );
        try {
            log.info( "LIMPANDO O CAMPO LIDER DE TODOS OS FUNCIONARIOS DA EMPRESA=" + idEmpresa );
            // int updated = dao.updateByJpql("update Funcionario set lider =
            // null", null);
            int updated = dao.executeNativeUpdate( "update funcionario set id_lider = null where id_empresa = " + idEmpresa );
            log.info( "REGISTROS ATUALIZADOS:" + updated );
        } catch ( DAOException e ) {
            throw new ServiceException( e.getMessage() );
        }
    }


    @Transactional
    public void inativarTodosFuncionarios( Long idEmpresa )
        throws ServiceException {

        try {
            log.info( "INATIVANDO TODOS OS FUNCIONARIOS DA EMPRESA=" + idEmpresa );
            // int updated = dao.updateByJpql("update Funcionario f set f.ativo
            // = false", null);
            int updated = dao.executeNativeUpdate( "update funcionario set ativo = 0 where id_empresa = " + idEmpresa );
            log.info( "REGISTROS ATUALIZADOS:" + updated );
        } catch ( DAOException e ) {
            throw new ServiceException( e.getMessage() );
        }
    }


    @Transactional
    public void excluirLogicamenteTodosFuncionarios()
        throws ServiceException {

        try {
            log.info( "EXCLUINDO LOGICAMENTE TODOS OS FUNCIONARIOS" );
            // int updated = dao.updateByJpql("update Funcionario f set
            // f.excluido = true", null);
            int updated = dao.executeNativeUpdate( "update funcionario set excluido = 1" );
            log.info( "REGISTROS ATUALIZADOS:" + updated );
        } catch ( DAOException e ) {
            throw new ServiceException( e.getMessage() );
        }
    }


    @Override
    @Transactional( readOnly = true, propagation = Propagation.REQUIRES_NEW )
    public PreviewFuncionariosImportacao importFromXls( InputStream inputStream )
        throws ServiceException,
        IllegalAccessException,
        InvocationTargetException {

        ImportUtils importUtils = new ImportUtils();
        try {
            ImportFuncionarioView importFuncionarioView = importUtils.xlxToFuncionario( inputStream );
            List< Funcionario > funcionariosImportados = importFuncionarioView.getFuncionarios();
            String cnpjEmpresa = importFuncionarioView.getCnpj();
            if ( StringUtils.isBlank( cnpjEmpresa ) ) {
                throw new ServiceException( "CNPJ da Empresa é obrigatório" );
            }
            Empresa empresa = empresaService.findByCNPJ( cnpjEmpresa );
            if ( empresa == null ) {
                throw new ServiceException( "Empresa não encontrada com CNPJ=" + cnpjEmpresa );
            }

            String erros = "";
            if ( CollectionUtils.isNotEmpty( importUtils.getMsgsErro() ) ) {
                erros += "Erros de validação:<br>";
                for ( String erro : importUtils.getMsgsErro() ) {
                    erros += erro + "<br>";
                }
            }
            // for (Funcionario func : funcionariosImportados) {
            // Funcionario funcExistente = findByMatricula(func.getMatricula(),
            // null);
            // if (funcExistente == null) {
            // erros += "Matricula [" + func.getMatricula() + "] não encontrada.
            // <br>";
            // }
            // }

            if ( !erros.isEmpty() ) {
                throw new ServiceException( erros );
            }

            List< Funcionario > funcionariosResult = prepararRegistros( empresa.getId(), funcionariosImportados, importUtils, "root" );

            // VERIFICAR FUNCIONARIOS DE OUTRA EMPRESA
            erros = "";
            for ( Funcionario func : funcionariosResult ) {
                if ( !empresa.getId().equals( func.getIdEmpresa() ) ) {
                    Empresa emp = empresaService.findById( func.getIdEmpresa() );
                    String nome = "";
                    String cpf = "";
                    if ( func.getPessoa() != null ) {
                        nome = func.getPessoa().getNome();
                        cpf = func.getPessoa().getDocumento();
                    }
                    erros += "O funcionário matrícula: " + func.getMatricula() + ", nome: " + nome + ", cpf: " + cpf + ", já está cadastrado na empresa: "
                        + emp.getNome() + " - CNPJ: " + emp.getCnpj() + "";
                    erros += "<br>";
                }
            }
            if ( !erros.isEmpty() ) {
                String msg = "Erros de validação:<br>";
                msg += erros;
                throw new ServiceException( msg );
            }

            // TODO PROBLEMAS COM PROXY DO HIBERNATE, VERIFICAR.
            if ( funcionariosResult != null ) {
                for ( Funcionario funcionario : funcionariosResult ) {
                    if ( funcionario != null ) {
                        if ( funcionario.getPessoa() != null ) {
                            Pessoa pes = new Pessoa();
                            BeanUtils.copyProperties( pes, funcionario.getPessoa() );
                            funcionario.setPessoa( pes );
                        }
                        if ( funcionario.getCargo() != null ) {
                            Cargo cargo = new Cargo();
                            BeanUtils.copyProperties( cargo, funcionario.getCargo() );
                            funcionario.setCargo( cargo );
                        }
                        if ( funcionario.getSetor() != null ) {
                            Setor setor = new Setor();
                            BeanUtils.copyProperties( setor, funcionario.getSetor() );
                            funcionario.setSetor( setor );
                        }
                        if ( funcionario.getLider() != null ) {
                            Lider lider = new Lider();
                            BeanUtils.copyProperties( lider, funcionario.getLider() );
                            funcionario.setLider( lider );
                        }
                    }
                }
            }

            PreviewFuncionariosImportacao previewFuncionariosImportacao = new PreviewFuncionariosImportacao();
            previewFuncionariosImportacao.setFuncionariosImportados( funcionariosResult );
            previewFuncionariosImportacao.setIdEmpresa( empresa.getId() );

            return previewFuncionariosImportacao;

        } catch ( ConvertFromXlsException e ) {
            throw new ServiceException( e.getMessage(), e );
        } catch ( Exception e ) {
            log.error( "Erro ao importar arquivo XLS de funcionários.Error=" + e.getMessage(), e );
            throw new ServiceException( e.getMessage(), e );
        }
    }


    private List< Funcionario > prepararRegistros( Long idEmpresa, List< Funcionario > funcionariosImportados, ImportUtils importUtils, String userName )
        throws ServiceException,
        IllegalAccessException,
        InvocationTargetException {

        List< Funcionario > funcionarios = new ArrayList<>();
        List< String > erros = new ArrayList<>();

        if ( funcionariosImportados != null ) {

            for ( Funcionario funcImportado : funcionariosImportados ) {

                Funcionario funcionario = null;

                if ( funcImportado.getMatricula() != null ) {
                    funcionario = findByMatricula(
                        idEmpresa,
                        funcImportado.getMatricula(),
                        Arrays.asList(
                            "pessoa pessoa",
                            "lider",
                            "setor",
                            "cargo cargo",
                            "[cargo].grupoOcupacional",
                            "[pessoa].estadoCivil",
                            "[pessoa].telefones",
                            "[pessoa].enderecos",
                            "[pessoa].enderecosEletronicos" ) );
                }

                if ( funcionario == null ) {
                    if ( funcImportado.getPessoa() != null && funcImportado.getPessoa().getDocumento() != null ) {
                        funcionario = findByCpf(
                            idEmpresa,
                            funcImportado.getPessoa().getDocumento(),
                            Arrays.asList(
                                "pessoa pessoa",
                                "lider",
                                "setor",
                                "cargo cargo",
                                "[cargo].grupoOcupacional",
                                "[pessoa].estadoCivil",
                                "[pessoa].telefones",
                                "[pessoa].enderecos",
                                "[pessoa].enderecosEletronicos" ) );
                    }
                }

                if ( funcionario == null ) {
                    funcionario = new Funcionario();
                }
                if ( funcionario.getIdEmpresa() == null ) {
                    funcionario.setIdEmpresa( idEmpresa );
                }

                funcionario.setLider( null );

                if ( funcionario.getPessoa() == null ) {
                    funcionario.setPessoa( new Pessoa() );
                }

                funcionario.setMatricula( funcImportado.getMatricula() );
                funcionario.getPessoa().setNome( funcImportado.getPessoa().getNome() );
                funcionario.getPessoa().setDocumento( funcImportado.getPessoa().getDocumento() );
                funcionario.getPessoa().setSexo( funcImportado.getPessoa().getSexo() );
                funcionario.setAtivo( true );
                funcionario.setExcluido( false );

                funcionario = prepararEmail( funcImportado, funcionario );

                funcionario = prepararEndereco( funcImportado, funcionario );

                funcionario = prepararTelefone( funcImportado, funcionario );

                funcionario = prepararCargo( idEmpresa, funcImportado, funcionario, userName );

                funcionario = prepararSetor( idEmpresa, funcImportado, funcionario );

                try {

                    funcionario = prepararLider( idEmpresa, funcImportado, funcionario, funcionariosImportados, importUtils, userName );

                } catch ( ServiceException e ) {
                    erros.add( e.getMessage() );
                }

                funcionarios.add( funcionario );

            }

            if ( !erros.isEmpty() ) {
                String msgErros = "Erros de validação:<br>";
                for ( String erro : erros ) {
                    msgErros += erro + "<br>";
                }
                throw new ServiceException( msgErros );
            }

        }

        return funcionarios;

    }


    private Funcionario prepararCargo( Long idEmpresa, Funcionario funcImportado, Funcionario funcionario, String user )
        throws ServiceException {

        if ( funcImportado.getCargo() != null && funcImportado.getCargo().getNome() != null ) {
            Cargo cargo = null;
            List< Cargo > cargos = cargoService.findByNome( idEmpresa, funcImportado.getCargo().getNome(), Arrays.asList( "grupoOcupacional" ) );
            if ( cargos != null && cargos.size() == 1 ) {
                funcionario.setCargo( cargos.get( 0 ) );
                if ( funcionario.getCargo().getGrupoOcupacional() != null && idEmpresa != null ) {
                    if ( !idEmpresa.equals( funcionario.getCargo().getGrupoOcupacional().getIdEmpresa() ) ) {
                        funcionario.getCargo().getGrupoOcupacional().setIdEmpresa( idEmpresa );
                        grupoOcupacionalService.update( funcionario.getCargo().getGrupoOcupacional(), user );
                    }
                }
            } else {
                GrupoOcupacional grupoOcupacional = null;
                if ( funcImportado.getCargo() != null && funcImportado.getCargo().getGrupoOcupacional() != null
                    && funcImportado.getCargo().getGrupoOcupacional().getNome() != null ) {
                    List< GrupoOcupacional > gruposOcupacionais =
                        grupoOcupacionalService.findByNome( idEmpresa, funcImportado.getCargo().getGrupoOcupacional().getNome() );
                    if ( gruposOcupacionais != null && gruposOcupacionais.size() == 1 ) {
                        grupoOcupacional = gruposOcupacionais.get( 0 );
                    } else {
                        grupoOcupacional = new GrupoOcupacional();
                        grupoOcupacional.setIdEmpresa( idEmpresa );
                        grupoOcupacional.setNome( funcImportado.getCargo().getGrupoOcupacional().getNome() );
                        grupoOcupacional.setDataCadastro( new Date() );
                        // grupoOcupacional =
                        // grupoOcupacionalService.save(grupoOcupacional,
                        // Constantes.USER_SYSTEM); //SALVAR SOMENTE APOS
                        // CONFIRMACAO

                    }
                }
                cargo = new Cargo();
                cargo.setNome( funcImportado.getCargo().getNome() );
                cargo.setGrupoOcupacional( grupoOcupacional );
                // cargo = cargoService.save(cargo, Constantes.USER_SYSTEM);
                // //SALVAR SOMENTE APOS CONFIRMACAO
                funcionario.setCargo( cargo );
            }
        }

        if ( funcionario.getCargo() != null ) {
            funcionario.getCargo().setIdEmpresa( idEmpresa );
            if ( funcionario.getCargo().getGrupoOcupacional() != null ) {
                funcionario.getCargo().getGrupoOcupacional().setIdEmpresa( idEmpresa );
            }
        }

        return funcionario;
    }


    private Funcionario prepararEndereco( Funcionario funcImportado, Funcionario funcionario )
        throws ServiceException {

        if ( CollectionUtils.isNotEmpty( funcImportado.getPessoa().getEnderecos() ) ) {
            Endereco enderecoResidencial = funcImportado.getPessoa().getEnderecos().iterator().next();
            if ( enderecoResidencial != null ) {
                if ( enderecoResidencial.getCep() != null ) {
                    String cep = StringUtils.deleteWhitespace( enderecoResidencial.getCep() );
                    cep = AppStringUtils.somenteNumeros( cep );
                    enderecoResidencial.setCep( cep );
                }
            }
            if ( enderecoResidencial.getNomeCidade() != null & enderecoResidencial.getSiglaEstado() != null ) {

                Estado estado = estadoService.findByUf( enderecoResidencial.getSiglaEstado() );
                if ( estado != null ) {
                    List< Cidade > cidades = cidadeService.findByNomeCidadeAndEstado( enderecoResidencial.getNomeCidade(), estado.getId() );
                    if ( cidades != null && cidades.size() == 1 ) {
                        enderecoResidencial.setTipoEndereco( tipoEnderecoService.getResidencial() );
                        enderecoResidencial.setCidade( cidades.get( 0 ) );
                        funcionario.getPessoa().setEnderecos( new HashSet< Endereco >() );
                        funcionario.getPessoa().getEnderecos().add( enderecoResidencial );
                    }
                }
            }
        }

        return funcionario;
    }


    private Funcionario prepararTelefone( Funcionario funcImportado, Funcionario funcionario )
        throws ServiceException {

        if ( CollectionUtils.isNotEmpty( funcImportado.getPessoa().getTelefones() ) ) {
            Telefone telefonePessoal = funcImportado.getPessoa().getTelefones().iterator().next();
            telefonePessoal.setTipoTelefone( tipoTelefoneService.getPessoal() );

            if ( telefonePessoal.getNumero() != null ) {
                String numero = telefonePessoal.getNumero();
                numero = StringUtils.deleteWhitespace( numero );
                numero = AppStringUtils.somenteNumeros( numero );
                telefonePessoal.setNumero( numero );
            }

            funcionario.getPessoa().setTelefones( new HashSet< Telefone >() );
            funcionario.getPessoa().getTelefones().add( telefonePessoal );
        }

        return funcionario;
    }


    private Funcionario prepararEmail( Funcionario funcImportado, Funcionario funcionario )
        throws ServiceException {

        if ( CollectionUtils.isNotEmpty( funcImportado.getPessoa().getEnderecosEletronicos() ) ) {
            EnderecoEletronico email = funcImportado.getPessoa().getEnderecosEletronicos().iterator().next();
            email.setTipoEnderecoEletronico( tipoEnderecoEletronicoService.getTypeEmail() );

            funcionario.getPessoa().setEnderecosEletronicos( new HashSet< EnderecoEletronico >() );
            funcionario.getPessoa().getEnderecosEletronicos().add( email );
        }

        return funcionario;
    }


    private Funcionario prepararSetor( Long idEmpresa, Funcionario funcImportado, Funcionario funcionario )
        throws ServiceException {

        if ( funcImportado.getSetor() != null && funcImportado.getSetor().getNome() != null ) {
            List< Setor > setores = setorService.findByNome( idEmpresa, funcImportado.getSetor().getNome() );
            if ( setores != null && setores.size() == 1 ) {
                funcionario.setSetor( setores.get( 0 ) );
            } else {
                Setor setor = new Setor();
                setor.setNome( funcImportado.getSetor().getNome() );
                // setor = setorService.save(setor, Constantes.USER_SYSTEM);
                // //SALVAR SOMENTE APOS CONFIRMACAO
                funcionario.setSetor( setor );
            }
        }

        if ( funcionario.getSetor() != null ) {
            funcionario.getSetor().setIdEmpresa( idEmpresa );
        }

        return funcionario;
    }


    private Funcionario prepararLider(
        Long idEmpresa,
        Funcionario funcImportado,
        Funcionario funcionario,
        List< Funcionario > funcionariosImportados,
        ImportUtils importUtils,
        String userName )
        throws ServiceException {

        if ( funcImportado.getLider() != null && funcImportado.getLider().getMatricula() != null
            && !Double.valueOf( funcImportado.getLider().getMatricula() ).equals( 0D ) ) {
            Funcionario fLider = findByMatricula( idEmpresa, funcImportado.getLider().getMatricula(), Arrays.asList( "pessoa" ) );
            if ( fLider != null ) {
                Lider lider = new Lider();
                lider.setId( fLider.getId() );
                lider.setMatricula( fLider.getMatricula() );
                lider.setPessoa( fLider.getPessoa() );
                lider.setCargo( fLider.getCargo() );
                funcionario.setLider( lider );
            } else {
                boolean encontrado = false;
                for ( Funcionario funcLider : funcionariosImportados ) {

                    if ( funcLider.getMatricula() != null //
                        && !funcLider.getMatricula().equals( funcImportado.getMatricula() ) //
                        && funcLider.getMatricula().equals( funcImportado.getLider().getMatricula() ) ) {

                        Lider lider = new Lider();
                        lider.setId( funcLider.getId() );
                        lider.setMatricula( funcLider.getMatricula() );
                        lider.setPessoa( funcLider.getPessoa() );
                        funcLider = prepararCargo( idEmpresa, funcLider, funcLider, userName );
                        lider.setCargo( funcLider.getCargo() );
                        funcionario.setLider( lider );
                        encontrado = true;
                        break;
                    }

                }
                if ( !encontrado ) {
                    String msgError = "";
                    if ( funcImportado.getMatricula() != null ) {
                        Integer rowNum = importUtils.getIndexMatriculasLinhas().get( funcImportado.getMatricula() );
                        msgError += "Linha " + ( rowNum + 1 ) + " - ";
                    }
                    throw new ServiceException( msgError + "Matrícula Líder [" + funcImportado.getLider().getMatricula()
                        + "] não foi encontrada na base de dados nem no documento importado" );
                }
            }
            if ( funcionario.getLider() != null && funcionario.getLider().getMatricula() != null ) {
                Funcionario funcLider = null;
                for ( Funcionario funcImport : funcionariosImportados ) {
                    if ( funcImport.getMatricula().equals( funcionario.getLider().getMatricula() ) ) {
                        funcLider = funcImport;
                        break;
                    }
                }
                if ( funcLider != null && funcLider.getLider() != null ) {
                    if ( funcLider.getLider().getMatricula().equals( funcionario.getMatricula() ) ) {
                        String msgErro = "";
                        String funcLidernomeOuMatricula = funcLider.getMatricula();
                        if ( funcLider.getPessoa() != null && funcLider.getPessoa().getNome() != null ) {
                            funcLidernomeOuMatricula = funcLider.getPessoa().getNome();
                        }
                        msgErro += funcLidernomeOuMatricula + " não pode ser líder de " + funcionario.getPessoa().getNome();
                        msgErro += ", porque " + funcionario.getPessoa().getNome() + " já é lider de " + funcLider.getPessoa().getNome();

                        throw new ServiceException( msgErro );

                    }
                }
            }
        }

        return funcionario;
    }


    private String getTratamentoDoDa( Funcionario func ) {

        String doDa = "do";
        if ( func != null && func.getPessoa() != null ) {
            if ( func.getPessoa().getSexo() != null ) {
                doDa = func.getPessoa().getSexo().equals( "M" ) ? "do" : "da";
            }
        }
        return doDa;
    }


    private String getTratamentoFuncionarioA( Funcionario func ) {

        String funcionarioA = "Funcionário";
        if ( func != null && func.getPessoa() != null ) {
            if ( func.getPessoa().getSexo() != null ) {
                funcionarioA = func.getPessoa().getSexo().equals( "M" ) ? "Funcionário" : "Funcionária";
            }
        }
        return funcionarioA;
    }


    @Override
    public List< Long > getIds( List< Funcionario > entities ) {

        List< Long > ids = new ArrayList<>();
        if ( entities != null ) {
            for ( Funcionario pes : entities ) {
                if ( pes.getId() != null ) {
                    ids.add( pes.getId() );
                }
            }
        }
        return ids;
    }

}
