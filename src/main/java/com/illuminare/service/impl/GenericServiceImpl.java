package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.entity.GenericEntity;
import com.illuminare.exceptions.DAOException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.GenericService;
import com.illuminare.service.UserService;


@Service
public abstract class GenericServiceImpl< T > implements GenericService< T > {

    public abstract GenericDAO< T > getDao();


    public abstract UserService getUserService();


    @Override
    public void flush() {

        getDao().flush();
    }


    @SuppressWarnings( "unchecked" )
    private T fillAuditoriaSave( T entity, String usuarioCadastro ) {

        GenericEntity e = (GenericEntity) entity;
        e.setDataCadastro( new Date() );
        e.setUsuarioCadastro( usuarioCadastro );
        return (T) e;
    }


    @SuppressWarnings( "unchecked" )
    private T fillAuditoriaUpdate( T entity, String usuarioAtualizacao ) {

        GenericEntity e = (GenericEntity) entity;
        e.setDataAtualizacao( new Date() );
        e.setUsuarioAtualizacao( usuarioAtualizacao );
        return (T) e;
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > findByIdEmpresa( Long idEmpresa ) {

        if ( idEmpresa != null ) {
            return getDao().findByIdEmpresa( idEmpresa );
        }
        return null;
    }


    @Override
    @Transactional
    public T save( T entity, String usuarioCadastro )
        throws ServiceException {

        if ( entity != null && usuarioCadastro != null ) {
            if ( ( (GenericEntity) entity ).getIdEmpresa() == null ) {
                if ( !getUserService().isAdmin( usuarioCadastro ) ) {
                    throw new ServiceException( "Empresa é obrigatório" );
                }
            }
            return getDao().save( fillAuditoriaSave( entity, usuarioCadastro ) );
        } else {
            return null;
        }

    }


    @Transactional
    @Override
    public List< T > saveAll( List< T > entities, String usuarioCadastro ) {

        if ( CollectionUtils.isNotEmpty( entities ) && usuarioCadastro != null ) {
            List< T > entitiesToSave = new ArrayList< T >();

            for ( T entity : entities ) {
                entitiesToSave.add( fillAuditoriaSave( entity, usuarioCadastro ) );
            }
            return getDao().saveAll( entitiesToSave );
        } else {
            return null;
        }
    }


    @Transactional
    @Override
    public List< T > saveUpdateAll( List< T > entities, String usuarioCadastro )
        throws ServiceException {

        if ( CollectionUtils.isNotEmpty( entities ) && usuarioCadastro != null ) {

            List< T > entitiesToSave = new ArrayList< T >();
            List< T > entitiesToUpdate = new ArrayList< T >();

            for ( T entity : entities ) {
                GenericEntity e = (GenericEntity) entity;
                if ( e.getId() == null ) {
                    entitiesToSave.add( fillAuditoriaSave( entity, usuarioCadastro ) );
                } else {
                    entitiesToUpdate.add( fillAuditoriaUpdate( entity, usuarioCadastro ) );
                }
            }

            List< T > entitiesResult = new ArrayList<>();

            List< T > savedEntities = getDao().saveAll( entitiesToSave );
            List< T > updatedEntities = getDao().updateAll( entitiesToUpdate );

            if ( CollectionUtils.isNotEmpty( savedEntities ) ) {
                entitiesResult.addAll( savedEntities );
            }
            if ( CollectionUtils.isNotEmpty( updatedEntities ) ) {
                entitiesResult.addAll( updatedEntities );
            }

            return entitiesResult;

        } else {
            return null;
        }
    }


    @Transactional
    @Override
    public T saveUpdate( T entity, String usuarioCadastro )
        throws ServiceException {

        GenericEntity e = (GenericEntity) entity;
        if ( e.getId() == null ) {
            return save( entity, usuarioCadastro );
        } else {
            update( entity, usuarioCadastro );
        }

        return entity;

    }


    @Transactional
    @Override
    public Set< T > saveAll( Set< T > entities, String usuarioCadastro ) {

        if ( CollectionUtils.isNotEmpty( entities ) && usuarioCadastro != null ) {
            Set< T > entitiesToSave = new HashSet<>();

            for ( T entity : entities ) {
                entitiesToSave.add( fillAuditoriaSave( entity, usuarioCadastro ) );
            }
            return getDao().saveAll( entitiesToSave );
        } else {
            return null;
        }
    }


    @Transactional
    @Override
    public List< T > updateAll( List< T > entities, String usuarioAtualizacao ) {

        if ( CollectionUtils.isNotEmpty( entities ) && usuarioAtualizacao != null ) {
            List< T > entitiesToSave = new ArrayList< T >();

            for ( T entity : entities ) {
                entitiesToSave.add( fillAuditoriaSave( entity, usuarioAtualizacao ) );
            }
            return getDao().saveAll( entitiesToSave );
        } else {
            return null;
        }
    }


    @Override
    @Transactional
    public void update( T entity, String usuarioAtualizacao )
        throws ServiceException {

        if ( entity != null && usuarioAtualizacao != null ) {
            if ( ( (GenericEntity) entity ).getIdEmpresa() == null ) {
                if ( !getUserService().isAdmin( usuarioAtualizacao ) ) {
                    throw new ServiceException( "Empresa é obrigatório" );
                }
            }
            getDao().update( fillAuditoriaUpdate( entity, usuarioAtualizacao ) );
        }

    }


    @SuppressWarnings( "unchecked" )
    @Override
    @Transactional
    public void deleteLogic( Long id, String userName )
        throws ServiceException {

        if ( id != null && userName != null ) {
            GenericEntity e = (GenericEntity) findById( id );

            e.setDataAtualizacao( new Date() );
            e.setUsuarioAtualizacao( userName );

            // update((T) e, usuarioAtualizacao);
            e.setDataAtualizacao( new Date() );
            e.setUsuarioAtualizacao( userName );

            getDao().update( (T) e );

            int result = getDao().deleteLogic( id );
            if ( result <= 0 ) {
                throw new ServiceException( "Falha ao excluir entidade" );
            }
        }

    }


    @Override
    @Transactional
    public void delete( Long id, String usuarioAtualizacao )
        throws ServiceException {

        if ( id != null && usuarioAtualizacao != null ) {
            GenericEntity e = (GenericEntity) findById( id );
            if ( e == null ) {
                throw new ServiceException( "Falha ao excluir entidade. Registro não encontrado" );
            }
            getDao().deleteLogic( id );
        }

    }


    @Override
    @Transactional( readOnly = true )
    public List< T > listAll( Long idEmpresa, String userName )
        throws ServiceException {

        if ( !getUserService().isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        return getDao().listAll( idEmpresa );
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > listAllOrderBy( Long idEmpresa, String orderBy, String userName )
        throws ServiceException {

        if ( !getUserService().isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        return getDao().listAllOrderBy( idEmpresa, orderBy );
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > listAll( Long idEmpresa, Set< String > fieldsToFetch, String userName )
        throws ServiceException {

        if ( !getUserService().isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        return getDao().listAll( idEmpresa, fieldsToFetch );
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > listAll( Long idEmpresa, List< String > fieldsToFetch, String userName )
        throws ServiceException {

        if ( !getUserService().isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        return getDao().listAll( idEmpresa, new HashSet< String >( fieldsToFetch ) );
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > listAllOrderBy( Long idEmpresa, Set< String > fieldsToFetch, String orderBy, String userName )
        throws ServiceException {

        if ( !getUserService().isAdmin( userName ) && idEmpresa == null ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        return getDao().listAll( idEmpresa, fieldsToFetch, orderBy );
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > listAllOrderBy( Long idEmpresa, List< String > fieldsToFetch, String orderBy, String userName )
        throws ServiceException {

        Set< String > fieldsToFetchSet = null;
        if ( fieldsToFetch != null ) {
            fieldsToFetchSet = new LinkedHashSet<>( fieldsToFetch );
        }
        return listAllOrderBy( idEmpresa, fieldsToFetchSet, orderBy, userName );
    }


    @Override
    // @Transactional( readOnly = true, propagation = Propagation.REQUIRES_NEW )
    @Transactional( readOnly = true )
    public T findById( Long id ) {

        if ( id != null ) {
            return getDao().findById( id );
        } else {
            return null;
        }
    }


    @Override
    @Transactional( readOnly = true )
    public T findByIdCurrent( Long id ) {

        if ( id != null ) {
            return getDao().findById( id );
        } else {
            return null;
        }
    }


    @Override
    // @Transactional( readOnly = true, propagation = Propagation.REQUIRES_NEW )
    @Transactional( readOnly = true )
    public T findById( Long id, Set< String > fieldsToFetch ) {

        if ( id != null ) {
            return getDao().findById( id, fieldsToFetch );
        } else {
            return null;
        }
    }


    @Override
    // @Transactional( readOnly = true, propagation = Propagation.REQUIRES_NEW )
    @Transactional( readOnly = true )
    public T findById( Long id, List< String > fieldsToFetch ) {

        Set< String > fieldsToFetchSet = null;
        if ( fieldsToFetch != null ) {
            fieldsToFetchSet = new LinkedHashSet<>( fieldsToFetch );
        }
        return findById( id, fieldsToFetchSet );
    }


    @Override
    @Transactional( readOnly = true )
    public List< T > findByAttr( Map< String, Object > attrs ) {

        return getDao().findByAttr( attrs );
    }


    @Override
    @Transactional
    public int executeNativeUpdate( String sql )
        throws ServiceException {

        try {
            return getDao().executeNativeUpdate( sql );
        } catch ( DAOException e ) {
            throw new ServiceException( e );
        }
    }

}
