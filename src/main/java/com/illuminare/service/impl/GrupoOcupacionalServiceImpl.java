package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.GrupoOcupacionalDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.CargoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class GrupoOcupacionalServiceImpl extends GenericServiceImpl< GrupoOcupacional > implements GrupoOcupacionalService {

    @Autowired
    GrupoOcupacionalDAO dao;

    @Autowired
    UserService userService;

    @Autowired
    EmpresaService empresaService;
    @Autowired
    CargoService cargoService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< GrupoOcupacional > getDao() {

        return dao;
    }


    @Override
    public Paginator< GrupoOcupacional > pesquisar( Paginator< GrupoOcupacional > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< GrupoOcupacional >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( GrupoOcupacional entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( GrupoOcupacional entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getObs() != null ) {
                fields.put( "obs", entity.getObs() );
            }
            if ( entity.getDescricao() != null ) {
                fields.put( "descricao", entity.getDescricao() );
            }
        }

        return fields;
    }


    @Override
    @Transactional( readOnly = true )
    public List< GrupoOcupacional > findByNome( Long idEmpresa, String nome )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "nome", nome );
        if ( idEmpresa != null ) {
            attrs.put( "idEmpresa", idEmpresa );
        }
        return dao.findByAttr( attrs );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap<>();
        GrupoOcupacional grupoOcupacional = new GrupoOcupacional();
        grupoOcupacional.setId( id );
        attrs.put( "grupoOcupacional", grupoOcupacional );
        List< Cargo > entities = cargoService.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( entities ) ) {
            for ( Cargo e : entities ) {
                if ( e.getExcluido() == null || !e.getExcluido() ) {
                    throw new ServiceException( "Impossível Excluir. Grupo Ocupacional está associado a um Cargo" );
                }
            }
        }
        super.deleteLogic( id, usuarioAtualizacao );
    }

}
