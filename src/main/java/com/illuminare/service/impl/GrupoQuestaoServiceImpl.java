package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.GrupoQuestaoDAO;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.GrupoQuestaoService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class GrupoQuestaoServiceImpl extends GenericServiceImpl< GrupoQuestao > implements GrupoQuestaoService {

    @Autowired
    GrupoQuestaoDAO dao;
    @Autowired
    UserService userService;
    @Autowired
    EmpresaService empresaService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< GrupoQuestao > getDao() {

        return dao;
    }


    @Override
    public Paginator< GrupoQuestao > pesquisar( Paginator< GrupoQuestao > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< GrupoQuestao >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( GrupoQuestao entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( GrupoQuestao entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getObs() != null ) {
                fields.put( "obs", entity.getObs() );
            }
        }

        return fields;
    }

}
