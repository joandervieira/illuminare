package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.ListaValoresDAO;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.ListaValores;
import com.illuminare.entity.Questionario;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.ListaValoresService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class ListaValoresServiceImpl extends GenericServiceImpl< ListaValores > implements ListaValoresService {

    @Autowired
    ListaValoresDAO dao;
    @Autowired
    QuestionarioService questionarioService;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    private UserService userService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< ListaValores > getDao() {

        return dao;
    }


    @Override
    @Transactional( readOnly = true )
    public Paginator< ListaValores > pesquisar( Paginator< ListaValores > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< ListaValores >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            List< ListaValores > listOrdered = new ArrayList< ListaValores >();
            for ( ListaValores entity : paginator.getResult() ) {
                // entity = ordernarValores(entity);

                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }

                listOrdered.add( entity );
            }
            paginator.setResult( listOrdered );
        }

        return paginator;
    }


    private Map< String, Object > mapFields( ListaValores entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }

        }

        return fields;
    }


    // @Override
    // public List<ListaValores> listAllOrderBy() {
    // List<ListaValores> listEntities = super.listAllOrderBy();
    // if (listEntities != null) {
    // Set<ListaValores> listOrdered = new HashSet<ListaValores>();
    // for (ListaValores entity : listEntities) {
    // // entity = ordernarValores(entity);
    // listOrdered.add(entity);
    // }
    // return listOrdered;
    // } else {
    // return null;
    // }
    // }
    //
    // @Override
    // public List<ListaValores> listAllOrderBy(String orderBy) {
    // List<ListaValores> listEntities = super.listAllOrderBy(orderBy);
    // if (listEntities != null) {
    // Set<ListaValores> listOrdered = new HashSet<ListaValores>();
    // for (ListaValores entity : listEntities) {
    // // entity = ordernarValores(entity);
    // listOrdered.add(entity);
    // }
    // return listOrdered;
    // } else {
    // return null;
    // }
    // }

    @Override
    public ListaValores findById( Long id ) {

        ListaValores entity = super.findById( id );
        // entity = ordernarValores(entity);
        return entity;
    }


    @Override
    public ListaValores findById( Long id, List< String > fieldsToFetch ) {

        ListaValores entity = super.findById( id, fieldsToFetch );
        // entity = ordernarValores(entity);
        return entity;
    }


    // private ListaValores ordernarValores(ListaValores listaValores) {
    // if (listaValores != null) {
    // if (listaValores.getValoresLista() != null) {
    // listaValores.setValoresLista(new
    // TreeList<ValorLista>(listaValores.getValoresLista()));
    // }
    // return listaValores;
    // } else {
    // return null;
    // }
    // }

    @Override
    @Transactional
    public void update( ListaValores entity, String usuarioAtualizacao )
        throws ServiceException {

        // validarEstaAssociadoQuestionario(entity);
        super.update( entity, usuarioAtualizacao );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        // validarEstaAssociadoQuestionario(id);
        super.deleteLogic( id, usuarioAtualizacao );
    }


    @Override
    public void validarEstaAssociadoQuestionario( ListaValores entity )
        throws ServiceException {

        if ( entity == null || entity.getId() == null ) {
            throw new ServiceException( "Parâmetros nulos. [idListaValores]" );
        }
        validarEstaAssociadoQuestionario( entity.getId() );
    }


    @Override
    public void validarEstaAssociadoQuestionario( Long idListaValores )
        throws ServiceException {

        if ( idListaValores == null ) {
            throw new ServiceException( "Parâmetros nulos. [idListaValores]" );
        }

        List< Questionario > questionarios = questionarioService.findByListaValores( idListaValores );
        if ( CollectionUtils.isNotEmpty( questionarios ) ) {
            String questionariosNomes = "";
            int c = 0;
            for ( Questionario q : questionarios ) {
                if ( c == 3 ) {
                    break;
                }
                if ( c > 0 ) {
                    questionariosNomes += ", ";
                }
                questionariosNomes += q.getNome();
                c++;
            }
            String aoAos = questionarios.size() > 1 ? "aos questionários: " : "ao questionário: ";
            String entreOutros = questionarios.size() > 3 ? ", entre outros ..." : "";
            throw new ServiceException( "Escala de Valores está associada " + aoAos + questionariosNomes + entreOutros );
        }

    }

}
