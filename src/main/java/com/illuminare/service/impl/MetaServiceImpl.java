package com.illuminare.service.impl;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.MetaDAO;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Meta;
import com.illuminare.entity.Pesquisa;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.MetaService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class MetaServiceImpl extends GenericServiceImpl< Meta > implements MetaService {

    @Autowired
    MetaDAO dao;

    @Autowired
    UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< Meta > getDao() {

        return dao;
    }


    @Override
    public Paginator< Meta > pesquisar( Paginator< Meta > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Meta >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        return paginator;
    }


    private Map< String, Object > mapFields( Meta entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getFuncionario() != null ) {
                fields.put( "funcionario", entity.getFuncionario() );
            }
            if ( entity.getPesquisa() != null ) {
                fields.put( "pesquisa", entity.getPesquisa() );
            }
        }

        return fields;
    }


    @Override
    @Transactional
    public Meta save( Meta entity, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( entity );
        Assert.notNull( usuarioCadastro );

        if ( entity.getFuncionario() == null || entity.getFuncionario().getId() == null ) {
            throw new ServiceException( "Funcionário é obrigatório" );
        }
        if ( entity.getPesquisa() == null || entity.getPesquisa().getId() == null ) {
            throw new ServiceException( "Pesquisa é obrigatório" );
        }
        if ( StringUtils.isBlank( entity.getMeta() ) ) {
            throw new ServiceException( "Meta é obrigatório" );
        }

        Meta meta = findByPesquisaAndFuncionario( entity.getPesquisa().getId(), entity.getFuncionario().getId() );
        if ( meta == null ) {
            return super.save( entity, usuarioCadastro );
        } else {
            meta.setMeta( entity.getMeta() );
            super.update( meta, usuarioCadastro );
            return meta;
        }

    }


    @Override
    @Transactional( readOnly = true )
    public Meta findByPesquisaAndFuncionario( Long idPesquisa, Long idAvaliado )
        throws ServiceException {

        Assert.notNull( idPesquisa, "idPesquisa é obrigatório" );
        Assert.notNull( idAvaliado, "idAvaliado é obrigatório" );
        Paginator< Meta > paginator = new Paginator<>();
        Map< String, Object > fieldsToSearch = new HashMap<>();
        Funcionario funcionario = new Funcionario();
        funcionario.setId( idAvaliado );
        fieldsToSearch.put( "funcionario", funcionario );
        fieldsToSearch.put( "pesquisa", new Pesquisa( idPesquisa ) );
        paginator.setFieldsToSearch( fieldsToSearch );
        paginator = dao.pesquisar( paginator );
        if ( CollectionUtils.isNotEmpty( paginator.getResult() ) ) {
            return paginator.getResult().get( 0 );
        } else {
            return null;
        }

    }
}
