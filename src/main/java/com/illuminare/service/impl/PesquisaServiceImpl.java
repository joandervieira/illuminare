package com.illuminare.service.impl;


import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.illuminare.controller.ResultadoController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.AvaliacaoDAO;
import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.PesquisaDAO;
import com.illuminare.dao.QuestaoMetaDAO;
import com.illuminare.dao.RespostaDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Lider;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.AvaliacaoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoClassificacaoService;
import com.illuminare.service.QuestaoMetaService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.RespostaService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class PesquisaServiceImpl extends GenericServiceImpl< Pesquisa > implements PesquisaService {

    Logger log = Logger.getLogger( PesquisaServiceImpl.class );

    @Autowired
    private PesquisaDAO dao;

    @Autowired
    private UserService userService;

    @Autowired
    private FuncionarioService funcionarioService;

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Autowired
    private RespostaService respostaService;

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private QuestaoService questaoService;

    @Autowired
    private QuestaoMetaService questaoMetaService;

    @Autowired
    private QuestaoMetaDAO questaoMetaDAO;

    @Autowired
    private AvaliacaoDAO avaliacaoDAO;

    @Autowired
    private QuestaoClassificacaoService questaoClassificacaoService;

    @Autowired
    private RespostaDAO respostaDAO;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< Pesquisa > getDao() {

        return dao;
    }


    @Override
    public Pesquisa findById( Long id, List< String > fieldsToFetch, Boolean findMetas )
        throws ServiceException {

        Pesquisa pesquisa = super.findById( id, fieldsToFetch );

        if ( pesquisa != null && fieldsToFetch != null ) {
            if ( findMetas ) {

            }
        }

        return pesquisa;
    }


    @Override
    @Transactional( readOnly = true )
    public Paginator< Pesquisa > pesquisar( Paginator< Pesquisa > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Pesquisa >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( Pesquisa entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( Pesquisa entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        // SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getDtaInicio() != null ) {
                fields.put( "dtaInicio", entity.getDtaInicio() );
            }
            if ( entity.getDtaFim() != null ) {
                fields.put( "dtaFim", entity.getDtaFim() );
            }
            if ( entity.getDtaEntrega() != null ) {
                fields.put( "dtaEntrega", entity.getDtaEntrega() );
            }
            if ( entity.getObs() != null ) {
                fields.put( "obs", entity.getObs() );
            }

            if ( entity.getDtaInicioMaiorQue() != null ) {
                fields.put( "[date>=]dtaInicio", entity.getDtaInicioMaiorQue() );
            }
            if ( entity.getDtaInicioMenorQue() != null ) {
                fields.put( "[date<=]dtaInicio", entity.getDtaInicioMenorQue() );
            }
            if ( entity.getDtaFimMaiorQue() != null ) {
                fields.put( "[date>=]dtaFim", entity.getDtaFimMaiorQue() );
            }
            if ( entity.getDtaFimMenorQue() != null ) {
                fields.put( "[date<=]dtaFim", entity.getDtaFimMenorQue() );
            }
            if ( entity.getDtaEntregaMaiorQue() != null ) {
                fields.put( "[date>=]dtaEntrega", entity.getDtaEntregaMaiorQue() );
            }
            if ( entity.getDtaEntregaMenorQue() != null ) {
                fields.put( "[date<=]dtaEntrega", entity.getDtaEntregaMenorQue() );
            }

            // fields.put("data [date<=]dtaEntrega",
            // entity.getDtaEntregaMenorQue());

        }

        return fields;
    }


    @Override
    @Transactional( rollbackFor = Exception.class )
    public Pesquisa save( Pesquisa pesquisa, String usuarioCadastro )
        throws ServiceException {

        Set< Questionario > questionarios = pesquisa.getQuestionarios();
        pesquisa.setPorTipo( true );
        pesquisa = super.save( pesquisa, usuarioCadastro );
        salvarMetas( questionarios, pesquisa, usuarioCadastro );
        gerarQuestionariosAvaliacao( pesquisa, usuarioCadastro );
        return pesquisa;
    }


    @Override
    @Transactional( rollbackFor = Exception.class )
    public void update( Pesquisa pesquisa, String userName )
        throws ServiceException {

        // TODO Auto-generated method stub
        // super.update( entity, usuarioAtualizacao );
        Assert.notNull( pesquisa, "Parameter entity can not be null." );
        Assert.notNull( pesquisa.getId(), "Parameter entity.id can not be null." );
        Assert.notNull( userName, "Parameter userName can not be null." );

        Long idEmpresa = pesquisa.getIdEmpresa();
        if ( idEmpresa == null ) {
            if ( !getUserService().isAdmin( userName ) ) {
                throw new ServiceException( "Empresa é obrigatório" );
            }
        }

        if ( !pesquisa.getPorTipo() ) {
            List< QuestionarioAvaliacao > questionariosExistentes = avaliacaoService.findByPesquisa( pesquisa );
            if ( isNotEmpty( questionariosExistentes ) ) {
                for ( QuestionarioAvaliacao questionariosExistente : questionariosExistentes ) {
                    if ( isNotEmpty( questionariosExistente.getRespostas() ) ) {
                        throw new ServiceException( "Impossível editar esta pesquisa. Os questionários já estão sendo respondidos." );
                    }
                }

            }
        }

        mergeGerarQuestionariosAvaliacao( pesquisa, userName );

        if ( pesquisa.getSalvarMetas() ) {
            questaoMetaService.deleteByPesquisa( pesquisa, userName );
            salvarMetas( pesquisa.getQuestionarios(), pesquisa, userName );
        }

        Pesquisa pesquisaDb = findById( pesquisa.getId() );
        pesquisaDb.setIdEmpresa( idEmpresa );
        pesquisaDb.setDtaEntrega( pesquisa.getDtaEntrega() );
        pesquisaDb.setDtaFim( pesquisa.getDtaFim() );
        pesquisaDb.setDtaInicio( pesquisa.getDtaInicio() );
        pesquisaDb.setNome( pesquisa.getNome() );
        pesquisaDb.setObs( pesquisa.getObs() );
        pesquisaDb.setQuestionarios( pesquisa.getQuestionarios() );
        pesquisaDb.setAutoAvaliacao( pesquisa.getAutoAvaliacao() );
        pesquisaDb.setAvaliacaoSuperiores( pesquisa.getAvaliacaoSuperiores() );
        pesquisaDb.setAvaliacaoSubordinados( pesquisa.getAvaliacaoSubordinados() );
        pesquisaDb.setCalculoPorMeta( pesquisa.getCalculoPorMeta() );

        pesquisaDb.setDataAtualizacao( new Date() );
        pesquisaDb.setUsuarioAtualizacao( userName );

        pesquisaDb.setCalculoValorObtidoOutros( pesquisa.getCalculoValorObtidoOutros() );
        pesquisaDb.setCalculoValorObtidoPropria( pesquisa.getCalculoValorObtidoPropria() );
        pesquisaDb.setCalculoValorObtidoSubordinados( pesquisa.getCalculoValorObtidoSubordinados() );
        pesquisaDb.setCalculoValorObtidoSuperior( pesquisa.getCalculoValorObtidoSuperior() );

        dao.update( pesquisaDb );

    }


    public Set< Funcionario > getFuncionarios( Questionario questionario )
        throws ServiceException {

        Set< Funcionario > funcionarios = new HashSet<>();

        if ( questionario.getGruposOcupacionais() != null ) {

            for ( GrupoOcupacional go : questionario.getGruposOcupacionais() ) {

                List< Funcionario > f = funcionarioService.findByGrupoOcupacional( go );
                if ( f != null ) {
                    funcionarios.addAll( f );
                }
            }

        }

        if ( questionario.getCargos() != null ) {

            for ( Cargo cargo : questionario.getCargos() ) {

                List< Funcionario > f = funcionarioService.findByCargo( cargo );
                if ( f != null ) {
                    funcionarios.addAll( f );
                }
            }

        }

        if ( questionario.getSetores() != null ) {

            for ( Setor setor : questionario.getSetores() ) {

                List< Funcionario > f = funcionarioService.findBySetor( setor );
                if ( f != null ) {
                    funcionarios.addAll( f );
                }
            }

        }

        return funcionarios;

    }


    @Transactional( rollbackFor = Exception.class )
    private void gerarQuestionariosAvaliacao( Pesquisa pesquisa, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( pesquisa.getQuestionarios() );

        List< QuestionarioAvaliacao > avaliacoes = new ArrayList<>();

        for ( Questionario questionario : pesquisa.getQuestionarios() ) {

            Set< Funcionario > funcionarios = getFuncionarios( questionario );

            for ( Funcionario avaliado : funcionarios ) {

                Lider lider = null;
                if ( avaliado.getLider() == null ) {
                    avaliado = funcionarioService.findById( avaliado.getId(), new ArrayList<>( Arrays.asList( "lider" ) ) );
                    lider = avaliado.getLider();
                } else {
                    lider = avaliado.getLider();
                }

                List< Funcionario > subordinados = funcionarioService.findSubordinados( avaliado );

                QuestionarioAvaliacao avaliacaoPropria = new QuestionarioAvaliacao();
                avaliacaoPropria.setAutoAvaliacao( true );
                avaliacaoPropria.setQuestionario( questionario );
                avaliacaoPropria.setPesquisa( pesquisa );
                avaliacaoPropria.setFuncionarioAvaliador( avaliado );
                avaliacaoPropria.setFuncionarioAvaliado( avaliado );

                if ( pesquisa.getAutoAvaliacao() ) {
                    avaliacoes.add( avaliacaoPropria );
                }

                if ( lider != null ) {
                    QuestionarioAvaliacao avaliacaoLider = new QuestionarioAvaliacao();
                    avaliacaoLider.setQuestionario( questionario );
                    avaliacaoLider.setAvaliacaoSuperior( true );
                    avaliacaoLider.setPesquisa( pesquisa );
                    avaliacaoLider.setFuncionarioAvaliado( avaliado );
                    avaliacaoLider.setFuncionarioAvaliador( new Funcionario( lider.getId() ) );
                    if ( pesquisa.getAvaliacaoSubordinados() ) {
                        avaliacoes.add( avaliacaoLider );
                    }
                }
                if ( pesquisa.getAvaliacaoSuperiores() ) {
                    if ( isNotEmpty( subordinados ) ) {
                        for ( Funcionario subordinado : subordinados ) {
                            QuestionarioAvaliacao avaliacaoSubordinado = new QuestionarioAvaliacao();
                            avaliacaoSubordinado.setAvaliacaoSubordinado( true );
                            avaliacaoSubordinado.setQuestionario( questionario );
                            avaliacaoSubordinado.setPesquisa( pesquisa );
                            avaliacaoSubordinado.setFuncionarioAvaliado( avaliado );
                            avaliacaoSubordinado.setFuncionarioAvaliador( subordinado );
                            avaliacoes.add( avaliacaoSubordinado );
                        }
                    }
                }
            }

        }

        avaliacaoService.saveAll( avaliacoes, usuarioCadastro );

    }


    private void mergeGerarQuestionariosAvaliacao( Pesquisa pesquisa, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( pesquisa.getQuestionarios() );

        removerAvaliacoesNaoVinculadasComPesquisa( pesquisa );

        for ( Questionario questionario : pesquisa.getQuestionarios() ) {

            Set< Funcionario > funcionariosAtuais = getFuncionarios( questionario );

            for ( Funcionario avaliado : funcionariosAtuais ) {

                Lider lider = null;
                if ( avaliado.getLider() == null ) {
                    avaliado = funcionarioService.findById( avaliado.getId(), new ArrayList<>( Arrays.asList( "lider" ) ) );
                    lider = avaliado.getLider();
                } else {
                    lider = avaliado.getLider();
                }

                List< Funcionario > subordinados = funcionarioService.findSubordinados( avaliado );

                QuestionarioAvaliacao avaliacaoPropria = new QuestionarioAvaliacao();
                avaliacaoPropria.setAutoAvaliacao( true );
                avaliacaoPropria.setQuestionario( questionario );
                avaliacaoPropria.setPesquisa( pesquisa );
                avaliacaoPropria.setFuncionarioAvaliador( avaliado );
                avaliacaoPropria.setFuncionarioAvaliado( avaliado );

                if ( pesquisa.getAutoAvaliacao() ) {
                    List< QuestionarioAvaliacao > avaliacaoPropriaBd =
                        avaliacaoService.findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador( pesquisa, questionario, avaliado, avaliado );
                    if ( isEmpty( avaliacaoPropriaBd ) ) {
                        avaliacaoService.save( avaliacaoPropria, usuarioCadastro );
                    }
                } else {
                    avaliacaoService.deleteByPesquisaAndQuestionarioAndAvaliadoAndAvaliador( pesquisa, questionario, avaliado, avaliado );
                }

                if ( lider != null ) {
                    QuestionarioAvaliacao avaliacaoLider = new QuestionarioAvaliacao();
                    avaliacaoLider.setAvaliacaoSuperior( true );
                    avaliacaoLider.setQuestionario( questionario );
                    avaliacaoLider.setPesquisa( pesquisa );
                    avaliacaoLider.setFuncionarioAvaliado( avaliado );
                    avaliacaoLider.setFuncionarioAvaliador( new Funcionario( lider.getId() ) );
                    if ( pesquisa.getAvaliacaoSubordinados() ) {

                        List< QuestionarioAvaliacao > avaliacaoLiderBd =
                            avaliacaoService.findByPesquisaAndQuestionarioAndAvaliadoAndTipo( pesquisa, questionario, avaliado, null, null, true );

                        if ( isEmpty( avaliacaoLiderBd ) ) {
                            avaliacaoService.save( avaliacaoLider, usuarioCadastro );
                        } else {
                            if ( !lider.getId().equals( avaliacaoLiderBd.get( 0 ).getFuncionarioAvaliador().getId() ) ) {
                                QuestionarioAvaliacao avaliacaoEdit = avaliacaoLiderBd.get( 0 );
                                avaliacaoEdit.setFuncionarioAvaliador( new Funcionario( lider.getId() ) );
                                avaliacaoService.update( avaliacaoEdit, usuarioCadastro );
                                respostaService.deleteByAvaliacao( avaliacaoEdit.getId(), usuarioCadastro );
                            }
                        }

                    } else {
                        avaliacaoService.deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo( pesquisa, questionario, avaliado, null, null, true );
                    }
                }

                if ( pesquisa.getAvaliacaoSuperiores() ) {
                    if ( isNotEmpty( subordinados ) ) {

                        for ( Funcionario subordinado : subordinados ) {
                            QuestionarioAvaliacao avaliacaoSubordinado = new QuestionarioAvaliacao();
                            avaliacaoSubordinado.setAvaliacaoSubordinado( true );
                            avaliacaoSubordinado.setQuestionario( questionario );
                            avaliacaoSubordinado.setPesquisa( pesquisa );
                            avaliacaoSubordinado.setFuncionarioAvaliado( avaliado );
                            avaliacaoSubordinado.setFuncionarioAvaliador( subordinado );

                            List< QuestionarioAvaliacao > avaliacaoSubordinadoBd = avaliacaoService
                                .findByPesquisaAndQuestionarioAndAvaliadoAndAvaliadorAndTipo( pesquisa, questionario, avaliado, subordinado, null, true, null );

                            if ( isEmpty( avaliacaoSubordinadoBd ) ) {
                                avaliacaoService.save( avaliacaoSubordinado, usuarioCadastro );
                            }

                        }
                    }
                } else {
                    avaliacaoService.deleteByPesquisaAndQuestionarioAndAvaliadoAndTipo( pesquisa, questionario, avaliado, null, true, null );
                }
            }

            // AVALIACOES REMOVER
            Set< Funcionario > funcionariosRemover = getFuncionariosRemover( pesquisa, questionario, funcionariosAtuais );
            if ( !funcionariosRemover.isEmpty() ) {
                for ( Funcionario funcionarioRemover : funcionariosRemover ) {
                    avaliacaoService.deleteByPesquisaAndQuestionarioAndFuncionarioAvaliado( pesquisa, questionario, funcionarioRemover );
                }
            }

        }

    }


    private void removerAvaliacoesNaoVinculadasComPesquisa( Pesquisa pesquisa ) {

        List< QuestionarioAvaliacao > avaliacoesDb = avaliacaoDAO.findByPesquisa( pesquisa );

        if ( isNotEmpty( avaliacoesDb ) && isNotEmpty( pesquisa.getQuestionarios() ) ) {
            for ( QuestionarioAvaliacao avaliacao : avaliacoesDb ) {
                boolean found = false;
                for ( Questionario questionario : pesquisa.getQuestionarios() ) {
                    if ( avaliacao.getQuestionario().getId().equals( questionario.getId() ) ) {
                        found = true;
                        break;
                    }
                }
                if ( !found ) {
                    avaliacaoDAO.deleteLogic( avaliacao.getId() );
                }
            }

        }

    }


    private Set< Funcionario > getFuncionariosRemover( Pesquisa pesquisa, Questionario questionario, Set< Funcionario > funcionariosAtuais )
        throws ServiceException {

        Set< Funcionario > funcionariosRemover = new HashSet<>();

        List< Funcionario > funcionariosPesquisa = avaliacaoDAO.findFuncionariosByPesquisaAndQuestionario( pesquisa, questionario );

        if ( isNotEmpty( funcionariosPesquisa ) ) {
            for ( Funcionario funcionarioPesquisa : funcionariosPesquisa ) {
                boolean funcionarioFound = false;
                if ( isNotEmpty( funcionariosAtuais ) ) {
                    for ( Funcionario funcionarioAtual : funcionariosAtuais ) {
                        if ( funcionarioAtual.getId().equals( funcionarioPesquisa.getId() ) ) {
                            funcionarioFound = true;
                            break;
                        }
                    }
                }
                if ( !funcionarioFound ) {
                    funcionariosRemover.add( funcionarioPesquisa );
                }
            }
        }

        return funcionariosRemover;
    }


    private void salvarMetas( Set< Questionario > questionarios, Pesquisa pesquisa, String userName )
        throws ServiceException {

        if ( isNotEmpty( questionarios ) ) {
            for ( Questionario questionario : questionarios ) {
                if ( isNotEmpty( questionario.getGruposQuestoes() ) ) {
                    for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
                        if ( isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {
                            for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                                Questao questao = questaoOrdem.getQuestao();

                                if ( questao != null ) {

                                    List< QuestaoMeta > questoesMetasGruposOcupacionais = questao.getQuestoesMetasGruposOcupacionais();
                                    List< QuestaoMeta > questoesMetasCargos = questao.getQuestoesMetasCargos();
                                    List< QuestaoMeta > questoesMetasSetores = questao.getQuestoesMetasSetores();

                                    if ( isNotEmpty( questoesMetasGruposOcupacionais ) ) {
                                        for ( QuestaoMeta questaoMeta : questoesMetasGruposOcupacionais ) {
                                            if ( questaoMeta.getMeta() != null && questaoMeta.getIdGrupoOcupacional() != null ) {
                                                questaoMetaService
                                                    .save( newQuestaoMeta( questaoMeta, pesquisa, questao, grupoQuestao, questionario, userName ), userName );
                                            }
                                        }
                                    }

                                    if ( isNotEmpty( questoesMetasCargos ) ) {
                                        for ( QuestaoMeta questaoMeta : questoesMetasCargos ) {
                                            if ( questaoMeta.getMeta() != null && questaoMeta.getIdCargo() != null ) {
                                                questaoMetaService
                                                    .save( newQuestaoMeta( questaoMeta, pesquisa, questao, grupoQuestao, questionario, userName ), userName );
                                            }
                                        }
                                    }

                                    if ( isNotEmpty( questoesMetasSetores ) ) {
                                        for ( QuestaoMeta questaoMeta : questoesMetasSetores ) {
                                            if ( questaoMeta.getMeta() != null && questaoMeta.getIdSetor() != null ) {
                                                questaoMetaService
                                                    .save( newQuestaoMeta( questaoMeta, pesquisa, questao, grupoQuestao, questionario, userName ), userName );
                                            }
                                        }
                                    }

                                }
                            }

                        }
                    }
                }
            }

        }
    }


    private QuestaoMeta newQuestaoMeta(
        QuestaoMeta questaoMeta,
        Pesquisa pesquisa,
        Questao questao,
        GrupoQuestao grupoQuestao,
        Questionario questionario,
        String user ) {

        QuestaoMeta newQuestaoMeta = new QuestaoMeta();
        newQuestaoMeta.setIdPesquisa( pesquisa.getId() );
        newQuestaoMeta.setIdQuestao( questao.getId() );
        newQuestaoMeta.setIdGrupoQuestao( grupoQuestao.getId() );
        newQuestaoMeta.setIdQuestionario( questionario.getId() );
        newQuestaoMeta.setAutoCalculada( questaoMeta.getAutoCalculada() );
        newQuestaoMeta.setIdCargo( questaoMeta.getIdCargo() );
        newQuestaoMeta.setIdGrupoOcupacional( questaoMeta.getIdGrupoOcupacional() );
        newQuestaoMeta.setIdSetor( questaoMeta.getIdSetor() );
        newQuestaoMeta.setMeta( questaoMeta.getMeta() );
        newQuestaoMeta.setNegativo( questaoMeta.getNegativo() );
        newQuestaoMeta.setAtivo( true );
        newQuestaoMeta.setExcluido( false );
        newQuestaoMeta.setDataCadastro( new Date() );
        newQuestaoMeta.setUsuarioCadastro( user );
        return newQuestaoMeta;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Pesquisa > findByQuestionario( List< Questionario > questionarios )
        throws ServiceException {

        if ( isEmpty( questionarios ) ) {
            return null;
        }
        return dao.findByQuestionario( questionarios );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Pesquisa > findPesquisasEmAberto( Long idEmpresa )
        throws ServiceException {

        return dao.findPesquisasEmAberto( idEmpresa );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Pesquisa > findPesquisasEmAberto( Long idEmpresa, List< String > fieldsToFetch )
        throws ServiceException {

        return dao.findPesquisasEmAberto( idEmpresa, fieldsToFetch );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Pesquisa > findPesquisasFechadas( Long idEmpresa )
        throws ServiceException {

        return dao.findPesquisasFechadas( idEmpresa );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Pesquisa > findPesquisasFechadasByAvaliado( Long idAvaliado )
        throws ServiceException {

        Assert.notNull( idAvaliado, "Parameter idAvaliado is required." );

        Funcionario funcionarioAvaliado = funcionarioService.findById( idAvaliado );
        Long idEmpresa = funcionarioAvaliado.getIdEmpresa();

        List< Pesquisa > pesquisasFechadasResult = new ArrayList<>();

        List< Pesquisa > pesquisasFechadas = dao.findPesquisasFechadas( idEmpresa );

        if ( isNotEmpty( pesquisasFechadas ) ) {

            List< Resposta > respostas = respostaService.findByAvaliado( idAvaliado, getIds( pesquisasFechadas ) );

            if ( respostas != null ) {
                for ( Resposta resp : respostas ) {
                    if ( resp.getQuestionarioAvaliacao() != null && resp.getQuestionarioAvaliacao().getPesquisa() != null ) {
                        boolean hasAdd = false;
                        for ( Pesquisa pesq : pesquisasFechadasResult ) {
                            if ( pesq.getId().equals( resp.getQuestionarioAvaliacao().getPesquisa().getId() ) ) {
                                hasAdd = true;
                                break;
                            }
                        }
                        if ( !hasAdd ) {
                            pesquisasFechadasResult.add( resp.getQuestionarioAvaliacao().getPesquisa() );
                        }
                    }
                }
            }
        }

        return pesquisasFechadasResult;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Funcionario > findAvaliados( Long idPesquisa )
        throws ServiceException {

        List< Funcionario > avaliados = new ArrayList<>();

        List< QuestionarioAvaliacao > avaliacoes = avaliacaoService.findByPesquisa( idPesquisa );
        if ( avaliacoes != null ) {
            for ( QuestionarioAvaliacao avaliacao : avaliacoes ) {
                boolean hasIncluded = false;
                for ( Funcionario f : avaliados ) {
                    if ( f.getId().equals( avaliacao.getFuncionarioAvaliado().getId() ) ) {
                        hasIncluded = true;
                        break;
                    }
                }
                if ( !hasIncluded ) {
                    avaliados.add( avaliacao.getFuncionarioAvaliado() );
                }
            }
        }

        return avaliados;
    }


    @Transactional( readOnly = true )
    @Override
    public boolean existeAvaliacaoRespondida( Long pesquisaId )
        throws ServiceException {

        Assert.notNull( pesquisaId, "Parâmetro pesquisaId é obrigatório" );

        Pesquisa pesquisa = findById( pesquisaId );

        if ( pesquisa != null ) {
            List< QuestionarioAvaliacao > questionarioAvaliacaos = avaliacaoService.findByPesquisa( pesquisa );
            if ( isNotEmpty( questionarioAvaliacaos ) ) {
                for ( QuestionarioAvaliacao questionarioAvaliacao : questionarioAvaliacaos ) {
                    if ( isNotEmpty( questionarioAvaliacao.getRespostas() ) ) {
                        return true;
                    }
                }

            }
        }

        return false;
    }


    @Override
    @Transactional
    public void removerAvaliacoesEResponstas( Long pesquisaId, String userName )
        throws ServiceException {

        Assert.notNull( pesquisaId, "Parâmetro pesquisaId é obrigatório" );

        Pesquisa pesquisa = findById( pesquisaId );

        if ( pesquisa != null ) {

            List< QuestionarioAvaliacao > avaliacoes = avaliacaoService.findByPesquisa( pesquisa );
            if ( isNotEmpty( avaliacoes ) ) {
                for ( QuestionarioAvaliacao avaliacao : avaliacoes ) {
                    respostaService.deleteByAvaliacao( avaliacao.getId(), userName );
                }
            }

            avaliacaoService.deleteByPesquisa( pesquisa );

        }

    }


    @Override
    @Transactional
    public void removerRespostas( Long pesquisaId, String userName )
        throws ServiceException {

        Assert.notNull( pesquisaId, "Parâmetro pesquisaId é obrigatório" );

        Pesquisa pesquisa = findById( pesquisaId );

        if ( pesquisa != null ) {

            List< QuestionarioAvaliacao > avaliacoes = avaliacaoService.findByPesquisa( pesquisa );
            if ( isNotEmpty( avaliacoes ) ) {
                for ( QuestionarioAvaliacao avaliacao : avaliacoes ) {
                    respostaService.deleteByAvaliacao( avaliacao.getId(), userName );
                }
            }

        }

    }


    @Transactional
    @Override
    public void clonarPesquisa( Long oldIdPesquisa, String user )
        throws ServiceException {

        Assert.notNull( oldIdPesquisa );

        Pesquisa pesquisa = findById( oldIdPesquisa );

        Set< Questionario > oldQuestionarios = pesquisa.getQuestionarios();
        oldQuestionarios = Questionario.copy( oldQuestionarios );

        List< QuestionarioAvaliacao > oldAvaliacoes = avaliacaoDAO.findByPesquisa( pesquisa );

        List< QuestaoMeta > oldMetas = questaoMetaService.findByPesquisa( oldIdPesquisa );

        pesquisa = pesquisa.copy();

        pesquisa.setId( null );
        pesquisa.setQuestionarios( null );
        pesquisa.setNome( pesquisa.getNome() + " - CLONE " + getDate() );
        pesquisa.setUsuarioCadastro( user );
        pesquisa.setDataCadastro( new Date() );
        pesquisa.setDataAtualizacao( null );
        pesquisa.setUsuarioAtualizacao( null );

        dao.getSession().persist( pesquisa );

        pesquisa.setQuestionarios( oldQuestionarios );

        clonarMetas( oldMetas, pesquisa.getId(), user );
        clonarAvaliacoes( oldAvaliacoes, pesquisa, user );

        update( pesquisa, user );

    }


    private void clonarAvaliacoes( List< QuestionarioAvaliacao > oldAvaliacoes, Pesquisa newPesquisa, String user ) {

        if ( isNotEmpty( oldAvaliacoes ) ) {
            oldAvaliacoes = QuestionarioAvaliacao.copy( oldAvaliacoes );
            for ( QuestionarioAvaliacao avaliacao : oldAvaliacoes ) {

                List< Resposta > respostas = avaliacao.getRespostas();

                avaliacao = avaliacao.copy();

                log.info( "Old avaliacao:" + avaliacao.getId() );
                avaliacao.setId( null );
                avaliacao.setPesquisa( newPesquisa );
                avaliacao.setUsuarioCadastro( user );
                avaliacao.setDataCadastro( new Date() );
                avaliacao.setDataAtualizacao( null );
                avaliacao.setUsuarioAtualizacao( null );
                avaliacao.setRespostas( null );

                avaliacaoDAO.getSession().persist( avaliacao );
                log.info( "New avaliacao:" + avaliacao.getId() );

                // respostas = Resposta.copy( respostas );
                // for ( Resposta resposta : respostas ) {
                // resposta.setId( null );
                // resposta.setQuestionarioAvaliacao( avaliacao );
                // resposta.setUsuarioCadastro( user );
                // resposta.setDataCadastro( new Date() );
                // resposta.setDataAtualizacao( null );
                // resposta.setUsuarioAtualizacao( null );
                //
                // respostaDAO.save( resposta );
                // }

            }

        }
    }


    private void clonarMetas( List< QuestaoMeta > oldMetas, Long newIdPesquisa, String user ) {

        if ( isNotEmpty( oldMetas ) ) {
            for ( QuestaoMeta oldMeta : oldMetas ) {

                oldMeta = oldMeta.copy();

                oldMeta.setId( null );
                oldMeta.setIdPesquisa( newIdPesquisa );
                oldMeta.setUsuarioCadastro( user );
                oldMeta.setDataCadastro( new Date() );
                oldMeta.setDataAtualizacao( null );
                oldMeta.setUsuarioAtualizacao( null );
                questaoMetaDAO.getSession().persist( oldMeta );
            }

        }
    }


    private String getDate() {

        SimpleDateFormat f = new SimpleDateFormat( "dd/MM/yyyy" );
        return f.format( new Date() );
    }


    @Override
    public List< Long > getIds( List< Pesquisa > pesquisas ) {

        List< Long > idsPesquisas = new ArrayList<>();
        if ( pesquisas != null ) {
            for ( Pesquisa pes : pesquisas ) {
                idsPesquisas.add( pes.getId() );
            }
        }
        return idsPesquisas;
    }

}
