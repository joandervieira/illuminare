package com.illuminare.service.impl;


import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.QuestaoClassificacaoDAO;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.Questionario;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.QuestaoClassificacaoService;
import com.illuminare.service.UserService;


@Service
public class QuestaoClassificacaoServiceImpl extends GenericServiceImpl< QuestaoClassificacao > implements QuestaoClassificacaoService {

    @Autowired
    QuestaoClassificacaoDAO dao;

    @Autowired
    UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< QuestaoClassificacao > getDao() {

        return dao;
    }


    @Override
    @Transactional( readOnly = true )
    public QuestaoClassificacao findByQuestionarioAndQuestao( Questionario questionario, Questao questao, GrupoQuestao grupoQuestao ) {

        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );

        List< QuestaoClassificacao > list = dao.findByQuestionarioAndQuestao( questionario, questao, grupoQuestao );
        if ( isNotEmpty( list ) ) {
            return list.get( 0 );
        }
        return null;

    }


    @Override
    @Transactional( readOnly = true )
    public List< QuestaoClassificacao > findByQuestionario( Questionario questionario ) {

        Assert.notNull( questionario );
        Assert.notNull( questionario.getId() );

        return dao.findByQuestionarioAndQuestao( questionario, null, null );

    }


    @Override
    @Transactional
    public void salvarAtualizarTodos( List< QuestaoClassificacao > entities, String user )
        throws ServiceException {

        Assert.notEmpty( entities );

        List< QuestaoClassificacao > entitiesSave = new ArrayList<>();

        Long idQuestionario = entities.get( 0 ).getIdQuestionario();

        dao.deleteByQuestionario( new Questionario( idQuestionario ) );

        for ( QuestaoClassificacao entity : entities ) {

            if ( entity != null && entity.getIdQuestionario() != null && entity.getIdQuestao() != null && entity.getIdGrupoQuestao() != null ) {

                entity.setDataCadastro( new Date() );
                entity.setUsuarioCadastro( user );
                dao.save( entity );

            } else {
                throw new ServiceException( "Payload inválido." );
            }
        }

    }

}
