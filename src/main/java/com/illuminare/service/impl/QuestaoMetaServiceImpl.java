package com.illuminare.service.impl;


import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.QuestaoMetaDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoClassificacaoService;
import com.illuminare.service.QuestaoMetaService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.UserService;
import com.illuminare.view.CargoView;
import com.illuminare.view.GrupoOcupacionalView;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.PesquisaView;
import com.illuminare.view.QuestaoMetaView;
import com.illuminare.view.QuestaoOrdemView;
import com.illuminare.view.QuestaoView;
import com.illuminare.view.QuestionarioView;
import com.illuminare.view.SetorView;


@Service
public class QuestaoMetaServiceImpl extends GenericServiceImpl< QuestaoMeta > implements QuestaoMetaService {

    Logger log = Logger.getLogger( QuestaoMetaServiceImpl.class );

    @Autowired
    QuestaoMetaDAO dao;

    @Autowired
    UserService userService;

    @Autowired
    private QuestionarioService questionarioService;

    @Autowired
    private QuestaoClassificacaoService questaoClassificacaoService;

    @Autowired
    private QuestaoService questaoService;

    @Autowired
    private PesquisaService pesquisaService;


    @Override
    public GenericDAO< QuestaoMeta > getDao() {

        return dao;
    }


    @Override
    public UserService getUserService() {

        return userService;
    }


    @Transactional( readOnly = true )
    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndGrupoOcupacional( Long questao, Long grupoQuestao, Long pesquisa, Long questionario, Long grupoOcupacional ) {

        Assert.notNull( questao, "Parâmetro questao é obrigatório" );
        Assert.notNull( pesquisa, "Parâmetro pesquisa é obrigatório" );

        return findByQuestaoAndPesquisaAndGrupoOcupacional(
            new Questao( questao ),
            new GrupoQuestao( grupoQuestao ),
            new Pesquisa( pesquisa ),
            new Questionario( questionario ),
            new GrupoOcupacional( grupoOcupacional ) );

    }


    @Transactional( readOnly = true )
    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndGrupoOcupacional(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        GrupoOcupacional grupoOcupacional ) {

        Assert.notNull( questao, "Parâmetro questao é obrigatório" );
        Assert.notNull( questao.getId(), "Parâmetro questao.id é obrigatório" );
        Assert.notNull( pesquisa, "Parâmetro pesquisa é obrigatório" );
        Assert.notNull( pesquisa.getId(), "Parâmetro pesquisa.id é obrigatório" );

        return dao.findByQuestaoAndPesquisaAndGrupoOcupacional( questao, grupoQuestao, pesquisa, questionario, grupoOcupacional );

    }


    @Transactional( readOnly = true )
    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndCargo( Long questao, Long grupoQuestao, Long pesquisa, Long questionario, Long cargo ) {

        Assert.notNull( questao, "Parâmetro questao é obrigatório" );
        Assert.notNull( pesquisa, "Parâmetro pesquisa é obrigatório" );

        return findByQuestaoAndPesquisaAndCargo(
            new Questao( questao ),
            new GrupoQuestao( grupoQuestao ),
            new Pesquisa( pesquisa ),
            new Questionario( questionario ),
            new Cargo( cargo ) );

    }


    @Transactional( readOnly = true )
    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndCargo(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        Cargo cargo ) {

        Assert.notNull( questao, "Parâmetro questao é obrigatório" );
        Assert.notNull( questao.getId(), "Parâmetro questao.id é obrigatório" );
        Assert.notNull( pesquisa, "Parâmetro pesquisa é obrigatório" );
        Assert.notNull( pesquisa.getId(), "Parâmetro pesquisa.id é obrigatório" );

        return dao.findByQuestaoAndPesquisaAndCargo( questao, grupoQuestao, pesquisa, questionario, cargo );

    }


    @Transactional( readOnly = true )
    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndSetor( Long questao, Long grupoQuestao, Long pesquisa, Long questionario, Long setor ) {

        Assert.notNull( questao, "Parâmetro questao é obrigatório" );
        Assert.notNull( pesquisa, "Parâmetro pesquisa é obrigatório" );

        return findByQuestaoAndPesquisaAndSetor(
            new Questao( questao ),
            new GrupoQuestao( grupoQuestao ),
            new Pesquisa( pesquisa ),
            new Questionario( questionario ),
            new Setor( setor ) );

    }


    @Transactional( readOnly = true )
    @Override
    public QuestaoMeta findByQuestaoAndPesquisaAndSetor(
        Questao questao,
        GrupoQuestao grupoQuestao,
        Pesquisa pesquisa,
        Questionario questionario,
        Setor setor ) {

        Assert.notNull( questao, "Parâmetro questao é obrigatório" );
        Assert.notNull( questao.getId(), "Parâmetro questao.id é obrigatório" );
        Assert.notNull( pesquisa, "Parâmetro pesquisa é obrigatório" );
        Assert.notNull( pesquisa.getId(), "Parâmetro pesquisa.id é obrigatório" );

        return dao.findByQuestaoAndPesquisaAndSetor( questao, grupoQuestao, pesquisa, questionario, setor );

    }


    @Transactional
    @Override
    public void deleteByPesquisa( Pesquisa pesquisa, String user ) {

        dao.deleteByPesquisa( pesquisa, user );
    }


    @Transactional( readOnly = true )
    @Override
    public Questionario fillMetasQuestionario( Questionario questionario, Pesquisa pesquisa )
        throws ServiceException {

        questionario = questionarioService
            .findById( questionario.getId(), Arrays.asList( "gruposOcupacionais", "cargos", "setores", "gruposQuestoes gq", "[gq].questoesOrdem" ) );

        Set< GrupoOcupacional > gruposOcupacionais = questionario.getGruposOcupacionais();
        Set< Cargo > cargos = questionario.getCargos();
        Set< Setor > setores = questionario.getSetores();

        if ( CollectionUtils.isNotEmpty( questionario.getGruposQuestoes() ) ) {
            for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
                if ( CollectionUtils.isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {
                    for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                        Questao questao = questaoOrdem.getQuestao();

                        List< QuestaoMeta > questoesMetasGruposOcupacionais = new ArrayList<>();
                        List< QuestaoMeta > questoesMetasCargos = new ArrayList<>();
                        List< QuestaoMeta > questoesMetasSetores = new ArrayList<>();

                        if ( CollectionUtils.isNotEmpty( gruposOcupacionais ) ) {
                            for ( GrupoOcupacional gruposOcupacional : gruposOcupacionais ) {
                                QuestaoMeta questaoMeta =
                                    findByQuestaoAndPesquisaAndGrupoOcupacional( questao, grupoQuestao, pesquisa, questionario, gruposOcupacional );
                                if ( questaoMeta != null ) {
                                    questoesMetasGruposOcupacionais.add( questaoMeta );
                                }
                            }
                        }

                        if ( CollectionUtils.isNotEmpty( cargos ) ) {
                            for ( Cargo cargo : cargos ) {
                                QuestaoMeta questaoMeta = findByQuestaoAndPesquisaAndCargo( questao, grupoQuestao, pesquisa, questionario, cargo );
                                if ( questaoMeta != null ) {
                                    questoesMetasCargos.add( questaoMeta );
                                }
                            }
                        }

                        if ( CollectionUtils.isNotEmpty( setores ) ) {
                            for ( Setor setor : setores ) {
                                QuestaoMeta questaoMeta = findByQuestaoAndPesquisaAndSetor( questao, grupoQuestao, pesquisa, questionario, setor );
                                if ( questaoMeta != null ) {
                                    questoesMetasSetores.add( questaoMeta );
                                }
                            }
                        }

                        questao.setQuestoesMetasGruposOcupacionais( questoesMetasGruposOcupacionais );
                        questao.setQuestoesMetasCargos( questoesMetasCargos );
                        questao.setQuestoesMetasSetores( questoesMetasSetores );

                        // CLASSIFICACAO
                        QuestaoClassificacao classificacao = questaoClassificacaoService.findByQuestionarioAndQuestao( questionario, questao, grupoQuestao );
                        questao.setQuestaoClassificacao( classificacao );

                    }
                }
            }
        }

        return questionario;

    }


    @Transactional( readOnly = true )
    @Override
    public Questao findMetasQuestao( Long idQuestao, Long idQuestionario, Long idPesquisa, Long idGrupoQuestao )
        throws ServiceException {

        Questionario questionario = questionarioService.findById( idQuestionario );
        Questao questao = questaoService.findById( idQuestao, Arrays.asList( "listaValores" ) );
        Pesquisa pesquisa = new Pesquisa( idPesquisa );

        Set< GrupoOcupacional > gruposOcupacionais = questionario.getGruposOcupacionais();
        Set< Cargo > cargos = questionario.getCargos();
        Set< Setor > setores = questionario.getSetores();

        List< QuestaoMeta > questoesMetasGruposOcupacionais = new ArrayList<>();
        List< QuestaoMeta > questoesMetasCargos = new ArrayList<>();
        List< QuestaoMeta > questoesMetasSetores = new ArrayList<>();

        if ( CollectionUtils.isNotEmpty( gruposOcupacionais ) ) {
            for ( GrupoOcupacional gruposOcupacional : gruposOcupacionais ) {
                QuestaoMeta questaoMeta = null;
                if ( idPesquisa != null ) {
                    questaoMeta =
                        findByQuestaoAndPesquisaAndGrupoOcupacional( questao, new GrupoQuestao( idGrupoQuestao ), pesquisa, questionario, gruposOcupacional );
                }

                if ( questaoMeta != null ) {
                    questoesMetasGruposOcupacionais.add( questaoMeta );
                } else {
                    QuestaoMeta newQuestaoMeta = new QuestaoMeta();
                    newQuestaoMeta.setIdQuestionario( idQuestionario );
                    newQuestaoMeta.setIdGrupoOcupacional( gruposOcupacional.getId() );
                    newQuestaoMeta.setIdPesquisa( idPesquisa );
                    newQuestaoMeta.setIdQuestao( idQuestao );
                    newQuestaoMeta.setIdGrupoQuestao( idGrupoQuestao );
                    questoesMetasGruposOcupacionais.add( newQuestaoMeta );
                }
            }
        }

        if ( CollectionUtils.isNotEmpty( cargos ) ) {
            for ( Cargo cargo : cargos ) {
                QuestaoMeta questaoMeta = null;
                if ( idPesquisa != null ) {
                    questaoMeta = findByQuestaoAndPesquisaAndCargo( questao, new GrupoQuestao( idGrupoQuestao ), pesquisa, questionario, cargo );
                }
                if ( questaoMeta != null ) {
                    questoesMetasCargos.add( questaoMeta );
                } else {
                    QuestaoMeta newQuestaoMeta = new QuestaoMeta();
                    newQuestaoMeta.setIdQuestionario( idQuestionario );
                    newQuestaoMeta.setIdCargo( cargo.getId() );
                    newQuestaoMeta.setIdPesquisa( idPesquisa );
                    newQuestaoMeta.setIdQuestao( idQuestao );
                    newQuestaoMeta.setIdGrupoQuestao( idGrupoQuestao );
                    questoesMetasCargos.add( newQuestaoMeta );
                }
            }
        }

        if ( CollectionUtils.isNotEmpty( setores ) ) {
            for ( Setor setor : setores ) {
                QuestaoMeta questaoMeta = null;
                if ( idPesquisa != null ) {
                    questaoMeta = findByQuestaoAndPesquisaAndSetor( questao, new GrupoQuestao( idGrupoQuestao ), pesquisa, questionario, setor );
                }
                if ( questaoMeta != null ) {
                    questoesMetasSetores.add( questaoMeta );
                } else {
                    QuestaoMeta newQuestaoMeta = new QuestaoMeta();
                    newQuestaoMeta.setIdQuestionario( idQuestionario );
                    newQuestaoMeta.setIdSetor( setor.getId() );
                    newQuestaoMeta.setIdPesquisa( idPesquisa );
                    newQuestaoMeta.setIdQuestao( idQuestao );
                    newQuestaoMeta.setIdGrupoQuestao( idGrupoQuestao );
                    questoesMetasSetores.add( newQuestaoMeta );
                }
            }
        }

        questao.setQuestoesMetasGruposOcupacionais( questoesMetasGruposOcupacionais );
        questao.setQuestoesMetasCargos( questoesMetasCargos );
        questao.setQuestoesMetasSetores( questoesMetasSetores );

        // CLASSIFICACAO
        QuestaoClassificacao classificacao =
            questaoClassificacaoService.findByQuestionarioAndQuestao( questionario, questao, new GrupoQuestao( idGrupoQuestao ) );
        questao.setQuestaoClassificacao( classificacao );

        return questao;

    }


    @Transactional( readOnly = true )
    @Override
    public void fillMetaNasQuestoesTransient( PesquisaView pesquisa )
        throws ServiceException {

        Assert.notNull( pesquisa );

        if ( isNotEmpty( pesquisa.getQuestionarios() ) ) {

            List< QuestaoMeta > questaoMetasDaPesquisa = new ArrayList<>();
            if ( pesquisa.getId() != null ) {
                questaoMetasDaPesquisa = findByPesquisa( pesquisa.getId() );
            }
            List< QuestaoMetaView > questaoMetasDaPesquisaView = QuestaoMeta.formatToWebPage( questaoMetasDaPesquisa );

            for ( QuestionarioView questionario : pesquisa.getQuestionarios() ) {

                List< QuestaoClassificacao > classificacoesDoQuestionario = questaoClassificacaoService.findByQuestionario( questionario.toEntityOnlyId() );

                Set< GrupoOcupacionalView > gruposOcupacionais = questionario.getGruposOcupacionais();
                Set< CargoView > cargos = questionario.getCargos();
                Set< SetorView > setores = questionario.getSetores();

                if ( isNotEmpty( questionario.getGruposQuestoes() ) ) {
                    for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {
                        if ( isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {

                            for ( QuestaoOrdemView questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                                QuestaoView questaoView = questaoOrdem.getQuestao();
                                Questao questao = questaoService.findById( questaoView.getId(), Arrays.asList( "listaValores" ) );
                                questaoView = questao.formatToWebPage().format();

                                List< QuestaoMetaView > questoesMetasGruposOcupacionais = new ArrayList<>();
                                List< QuestaoMetaView > questoesMetasCargos = new ArrayList<>();
                                List< QuestaoMetaView > questoesMetasSetores = new ArrayList<>();

                                if ( isNotEmpty( gruposOcupacionais ) ) {
                                    for ( GrupoOcupacionalView gruposOcupacional : gruposOcupacionais ) {
                                        QuestaoMetaView questaoMeta =
                                            getQuestaoMeta( questaoMetasDaPesquisaView, questaoView, grupoQuestao, questionario, pesquisa, gruposOcupacional );

                                        if ( questaoMeta != null ) {
                                            questoesMetasGruposOcupacionais.add( questaoMeta );
                                        } else {
                                            QuestaoMetaView newQuestaoMeta = new QuestaoMetaView();
                                            newQuestaoMeta.setIdQuestionario( questionario.getId() );
                                            newQuestaoMeta.setIdGrupoOcupacional( gruposOcupacional.getId() );
                                            newQuestaoMeta.setIdPesquisa( pesquisa.getId() );
                                            newQuestaoMeta.setIdQuestao( questaoView.getId() );
                                            newQuestaoMeta.setIdGrupoQuestao( grupoQuestao.getId() );
                                            questoesMetasGruposOcupacionais.add( newQuestaoMeta );
                                        }
                                    }
                                }

                                if ( isNotEmpty( cargos ) ) {
                                    for ( CargoView cargo : cargos ) {
                                        QuestaoMetaView questaoMeta =
                                            getQuestaoMeta( questaoMetasDaPesquisaView, questaoView, grupoQuestao, questionario, pesquisa, cargo );
                                        if ( questaoMeta != null ) {
                                            questoesMetasCargos.add( questaoMeta );
                                        } else {
                                            QuestaoMetaView newQuestaoMeta = new QuestaoMetaView();
                                            newQuestaoMeta.setIdQuestionario( questionario.getId() );
                                            newQuestaoMeta.setIdCargo( cargo.getId() );
                                            newQuestaoMeta.setIdPesquisa( pesquisa.getId() );
                                            newQuestaoMeta.setIdQuestao( questaoView.getId() );
                                            newQuestaoMeta.setIdGrupoQuestao( grupoQuestao.getId() );
                                            questoesMetasCargos.add( newQuestaoMeta );
                                        }
                                    }
                                }

                                if ( isNotEmpty( setores ) ) {
                                    for ( SetorView setor : setores ) {
                                        QuestaoMetaView questaoMeta =
                                            getQuestaoMeta( questaoMetasDaPesquisaView, questaoView, grupoQuestao, questionario, pesquisa, setor );
                                        if ( questaoMeta != null ) {
                                            questoesMetasSetores.add( questaoMeta );
                                        } else {
                                            QuestaoMetaView newQuestaoMeta = new QuestaoMetaView();
                                            newQuestaoMeta.setIdQuestionario( questionario.getId() );
                                            newQuestaoMeta.setIdSetor( setor.getId() );
                                            newQuestaoMeta.setIdPesquisa( pesquisa.getId() );
                                            newQuestaoMeta.setIdQuestao( questaoView.getId() );
                                            newQuestaoMeta.setIdGrupoQuestao( grupoQuestao.getId() );
                                            questoesMetasSetores.add( newQuestaoMeta );
                                        }
                                    }
                                }

                                questaoView.setQuestoesMetasGruposOcupacionais( questoesMetasGruposOcupacionais );
                                questaoView.setQuestoesMetasCargos( questoesMetasCargos );
                                questaoView.setQuestoesMetasSetores( questoesMetasSetores );

                                // CLASSIFICACAO
                                QuestaoClassificacao classificacao = getClassificacao( classificacoesDoQuestionario, questaoView, grupoQuestao );
                                if ( classificacao != null ) {
                                    questaoView.setQuestaoClassificacao( classificacao.formatToWebPage().format() );
                                }

                                // questaoView.unproxy( dao );

                                questaoOrdem.setQuestao( questaoView );

                            }

                        }
                    }

                }
            }

        }

    }


    @Transactional( readOnly = true )
    @Override
    public void clonarMetasTransient( PesquisaView pesquisa )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getCloneMetasPesquisaFromId() );

        PesquisaView pesquisaCloneFrom = pesquisaService.findById( pesquisa.getCloneMetasPesquisaFromId() ).formatToWebPage().format();

        fillMetaNasQuestoesTransient( pesquisaCloneFrom );

        if ( isNotEmpty( pesquisa.getQuestionarios() ) ) {

            List< QuestaoMetaView > allQuestaoMetaCloneFrom = getAllQuestaoMetaCloneFrom( pesquisaCloneFrom );

            Long qtdeQuestionariosClonadosMetas = 0L;
            Long qtdeQuestionariosClonadosMantido = 0L;
            for ( QuestionarioView questionario : pesquisa.getQuestionarios() ) {
                boolean questionarioHasCloned = false;
                boolean questionarioHasKeepted = false;
                Set< GrupoOcupacionalView > gruposOcupacionais = questionario.getGruposOcupacionais();
                Set< CargoView > cargos = questionario.getCargos();
                Set< SetorView > setores = questionario.getSetores();

                if ( isNotEmpty( questionario.getGruposQuestoes() ) ) {
                    for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {
                        if ( isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {

                            for ( QuestaoOrdemView questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                                QuestaoView questaoView = questaoOrdem.getQuestao();

                                List< QuestaoMetaView > questoesMetasGruposOcupacionais = new ArrayList<>();
                                List< QuestaoMetaView > questoesMetasCargos = new ArrayList<>();
                                List< QuestaoMetaView > questoesMetasSetores = new ArrayList<>();

                                if ( isNotEmpty( gruposOcupacionais ) ) {
                                    for ( GrupoOcupacionalView gruposOcupacional : gruposOcupacionais ) {

                                        QuestaoMetaView questaoMetaFrom =
                                            getQuestaoMeta( allQuestaoMetaCloneFrom, questaoView, grupoQuestao, questionario, null, gruposOcupacional );

                                        QuestaoMetaView questaoMetaAtual = getQuestaoMeta(
                                            questaoView.getQuestoesMetasGruposOcupacionais(),
                                            questaoView,
                                            grupoQuestao,
                                            questionario,
                                            null,
                                            gruposOcupacional );

                                        if ( questaoMetaAtual != null && questaoMetaAtual.getMeta() != null ) {
                                            questionarioHasKeepted = true;
                                            questoesMetasGruposOcupacionais.add( questaoMetaAtual );
                                            continue;
                                        }

                                        if ( questaoMetaFrom != null ) {
                                            questoesMetasGruposOcupacionais.add( questaoMetaFrom );
                                            questionarioHasCloned = true;
                                        } else {

                                            if ( questaoMetaAtual != null ) {
                                                questoesMetasGruposOcupacionais.add( questaoMetaAtual );
                                                questionarioHasKeepted = true;
                                            }
                                        }
                                    }
                                }

                                if ( isNotEmpty( cargos ) ) {
                                    for ( CargoView cargo : cargos ) {

                                        QuestaoMetaView questaoMetaFrom =
                                            getQuestaoMeta( allQuestaoMetaCloneFrom, questaoView, grupoQuestao, questionario, null, cargo );

                                        QuestaoMetaView questaoMetaAtual =
                                            getQuestaoMeta( questaoView.getQuestoesMetasCargos(), questaoView, grupoQuestao, questionario, null, cargo );

                                        if ( questaoMetaAtual != null && questaoMetaAtual.getMeta() != null ) {
                                            questionarioHasKeepted = true;
                                            questoesMetasCargos.add( questaoMetaAtual );
                                            continue;
                                        }

                                        if ( questaoMetaFrom != null ) {
                                            questoesMetasCargos.add( questaoMetaFrom );
                                            questionarioHasCloned = true;
                                        } else {

                                            if ( questaoMetaAtual != null ) {
                                                questoesMetasCargos.add( questaoMetaAtual );
                                                questionarioHasKeepted = true;
                                            }
                                        }
                                    }
                                }

                                if ( isNotEmpty( setores ) ) {
                                    for ( SetorView setor : setores ) {
                                        QuestaoMetaView questaoMetaFrom =
                                            getQuestaoMeta( allQuestaoMetaCloneFrom, questaoView, grupoQuestao, questionario, null, setor );

                                        QuestaoMetaView questaoMetaAtual =
                                            getQuestaoMeta( questaoView.getQuestoesMetasSetores(), questaoView, grupoQuestao, questionario, null, setor );

                                        if ( questaoMetaAtual != null && questaoMetaAtual.getMeta() != null ) {
                                            questionarioHasKeepted = true;
                                            questoesMetasSetores.add( questaoMetaAtual );
                                            continue;
                                        }

                                        if ( questaoMetaFrom != null ) {
                                            questoesMetasSetores.add( questaoMetaFrom );
                                            questionarioHasCloned = true;
                                        } else {

                                            if ( questaoMetaAtual != null ) {
                                                questoesMetasSetores.add( questaoMetaAtual );
                                                questionarioHasKeepted = true;
                                            }
                                        }
                                    }
                                }

                                questaoView.setQuestoesMetasGruposOcupacionais( questoesMetasGruposOcupacionais );
                                questaoView.setQuestoesMetasCargos( questoesMetasCargos );
                                questaoView.setQuestoesMetasSetores( questoesMetasSetores );

                                questaoOrdem.setQuestao( questaoView );

                            }

                        }
                    }

                }

                if ( questionarioHasCloned ) {
                    qtdeQuestionariosClonadosMetas++;
                }
                if ( questionarioHasKeepted ) {
                    qtdeQuestionariosClonadosMantido++;
                }
            }
            pesquisa.setQtdeQuestionariosClonadosMetas( qtdeQuestionariosClonadosMetas );
            pesquisa.setQtdeQuestionariosClonadosMantido( qtdeQuestionariosClonadosMantido );

        }

    }


    private List< QuestaoMetaView > getAllQuestaoMetaCloneFrom( PesquisaView pesquisaFrom ) {

        List< QuestaoMetaView > allQuestaoMetas = new ArrayList<>();
        if ( pesquisaFrom != null ) {
            if ( isNotEmpty( pesquisaFrom.getQuestionarios() ) ) {
                for ( QuestionarioView questionarioFrom : pesquisaFrom.getQuestionarios() ) {
                    if ( isNotEmpty( questionarioFrom.getGruposQuestoes() ) ) {
                        for ( GrupoQuestaoView grupoQuestaoFrom : questionarioFrom.getGruposQuestoes() ) {
                            if ( isNotEmpty( grupoQuestaoFrom.getQuestoesOrdem() ) ) {
                                for ( QuestaoOrdemView questaoOrdemFrom : grupoQuestaoFrom.getQuestoesOrdem() ) {
                                    QuestaoView questaoFrom = questaoOrdemFrom.getQuestao();
                                    if ( isNotEmpty( questaoFrom.getQuestoesMetasGruposOcupacionais() ) ) {
                                        for ( QuestaoMetaView questaoMetaFrom : questaoFrom.getQuestoesMetasGruposOcupacionais() ) {
                                            allQuestaoMetas.add( questaoMetaFrom );
                                        }
                                    }
                                    if ( isNotEmpty( questaoFrom.getQuestoesMetasCargos() ) ) {
                                        for ( QuestaoMetaView questaoMetaFrom : questaoFrom.getQuestoesMetasCargos() ) {
                                            allQuestaoMetas.add( questaoMetaFrom );
                                        }
                                    }
                                    if ( isNotEmpty( questaoFrom.getQuestoesMetasSetores() ) ) {
                                        for ( QuestaoMetaView questaoMetaFrom : questaoFrom.getQuestoesMetasSetores() ) {
                                            allQuestaoMetas.add( questaoMetaFrom );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return allQuestaoMetas;
    }


    private boolean isTheQuestaoMetaBase(
        QuestaoMetaView questaoMeta,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa ) {

        boolean sameQuestao = false, sameGrupoQuestao = false, samePesquisa = false, sameQuestionario = false;

        if ( questaoMeta != null ) {
            sameQuestao = questao.getId().equals( questaoMeta.getIdQuestao() );
            sameGrupoQuestao = grupoQuestao.getId().equals( questaoMeta.getIdGrupoQuestao() );
            sameQuestionario = questionario.getId().equals( questaoMeta.getIdQuestionario() );
            if ( pesquisa != null && pesquisa.getId() != null ) {
                samePesquisa = pesquisa.getId().equals( questaoMeta.getIdPesquisa() );
            } else {
                samePesquisa = true;
            }
        }

        return sameQuestao && sameGrupoQuestao && sameQuestionario && samePesquisa;

    }


    @Override
    public QuestaoMetaView getQuestaoMeta(
        List< QuestaoMetaView > questaoMetas,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa,
        GrupoOcupacionalView grupoOcupacionalView ) {

        if ( isNotEmpty( questaoMetas ) ) {
            for ( QuestaoMetaView qm : questaoMetas ) {
                boolean sameBase = isTheQuestaoMetaBase( qm, questao, grupoQuestao, questionario, pesquisa );

                if ( sameBase && grupoOcupacionalView.getId().equals( qm.getIdGrupoOcupacional() ) ) {
                    return qm;
                }
            }
        }
        return null;

    }


    @Override
    public QuestaoMetaView getQuestaoMeta(
        List< QuestaoMetaView > questaoMetas,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa,
        SetorView setor ) {

        if ( isNotEmpty( questaoMetas ) ) {
            for ( QuestaoMetaView qm : questaoMetas ) {
                boolean sameBase = isTheQuestaoMetaBase( qm, questao, grupoQuestao, questionario, pesquisa );

                if ( sameBase && setor.getId().equals( qm.getIdSetor() ) ) {
                    return qm;
                }
            }
        }
        return null;

    }


    @Override
    public QuestaoMetaView getQuestaoMeta(
        List< QuestaoMetaView > questaoMetas,
        QuestaoView questao,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaView pesquisa,
        CargoView cargo ) {

        if ( isNotEmpty( questaoMetas ) ) {
            for ( QuestaoMetaView qm : questaoMetas ) {

                boolean sameBase = isTheQuestaoMetaBase( qm, questao, grupoQuestao, questionario, pesquisa );

                if ( sameBase && cargo.getId().equals( qm.getIdCargo() ) ) {
                    return qm;
                }
            }
        }

        return null;

    }


    private QuestaoClassificacao getClassificacao( List< QuestaoClassificacao > questaoMetas, QuestaoView questao, GrupoQuestaoView grupoQuestao ) {

        if ( isNotEmpty( questaoMetas ) ) {
            for ( QuestaoClassificacao questaoClassificacao : questaoMetas ) {
                boolean sameQuestao, sameGrupoQuestao;

                sameQuestao = questao.getId().equals( questaoClassificacao.getIdQuestao() );
                sameGrupoQuestao = grupoQuestao.getId().equals( questaoClassificacao.getIdGrupoQuestao() );

                if ( sameQuestao && sameGrupoQuestao ) {
                    return questaoClassificacao;
                }
            }

        }

        return null;
    }


    @Override
    @Transactional( readOnly = true )
    public List< QuestaoMeta > findByQuestionario( Long idQuestionario ) {

        Assert.notNull( idQuestionario );

        return dao.findByQuestionario( idQuestionario );

    }


    @Override
    @Transactional( readOnly = true )
    public List< QuestaoMeta > findByPesquisa( Long idPesquisa ) {

        Assert.notNull( idPesquisa );

        return dao.findByPesquisa( idPesquisa );

    }
}
