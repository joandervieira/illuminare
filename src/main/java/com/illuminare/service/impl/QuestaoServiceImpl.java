package com.illuminare.service.impl;


import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.ListaValoresDAO;
import com.illuminare.dao.QuestaoDAO;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.ValorLista;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.ListaValoresService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;
import com.illuminare.view.QuestaoView;


@Service
public class QuestaoServiceImpl extends GenericServiceImpl< Questao > implements QuestaoService {

    @Autowired
    QuestaoDAO dao;
    @Autowired
    ListaValoresDAO listaValoresDAO;
    @Autowired
    ListaValoresService listaValoresService;
    @Autowired
    QuestionarioService questionarioService;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    private UserService userService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< Questao > getDao() {

        return dao;
    }


    // @Override
    // public Set<Questao> listAllOrderBy() {
    // // return super.listAllOrderBy();
    // return ordenarListAll(super.listAllOrderBy());
    // }
    //
    // @Override
    // public Set<Questao> listAllOrderBy(String orderBy) {
    // return ordenarListAll(super.listAllOrderBy(orderBy));
    // }
    //
    // @Override
    // public Set<Questao> listAllOrderBy(Set<String> fieldsToFetch) {
    // return ordenarListAll(super.listAllOrderBy(fieldsToFetch));
    // }
    //
    // @Override
    // public Set<Questao> listAllOrderBy(Set<String> fieldsToFetch, String
    // orderBy) {
    // return ordenarListAll(super.listAllOrderBy(fieldsToFetch, orderBy));
    // }

    private Set< Questao > ordenarListAll( Set< Questao > questoes ) {

        if ( questoes != null ) {
            Set< Questao > listOrdered = new LinkedHashSet< Questao >();
            for ( Questao questao : questoes ) {
                // questao = ordernarValores(questao);
                listOrdered.add( questao );
            }
            return listOrdered;
        } else {
            return null;
        }
    }


    // @Transactional( readOnly = true )
    // @Override
    // public Boolean temClassificacao( Long idQuestao ) {
    //
    // Questao questao = findById( idQuestao );
    //
    // if ( questao.getClassificacao() != null ) {
    // return questao.getClassificacao() <= 2L;
    // }
    // if ( questao.getQuestaoClassificacao() != null &&
    // questao.getQuestaoClassificacao().getClassificacao() != null ) {
    // return questao.getQuestaoClassificacao().getClassificacao() <= 2L;
    // }
    //
    // return true;
    // }

    @Transactional( readOnly = true )
    @Override
    public Boolean temClassificacao( QuestaoView questao ) {

        if ( questao.getClassificacao() != null ) {
            return questao.getClassificacao() <= 2L;
        }
        if ( questao.getQuestaoClassificacao() != null && questao.getQuestaoClassificacao().getClassificacao() != null ) {
            return questao.getQuestaoClassificacao().getClassificacao() <= 2L;
        }

        return true;
    }


    @Transactional( readOnly = true )
    @Override
    public Questao findById( Long id ) {

        // return super.findById(id);
        // return ordernarValores(super.findById(id));
        return super.findById( id );
    }


    @Transactional( readOnly = true )
    @Override
    public Questao findById( Long id, List< String > fieldsToFetch ) {

        // return super.findById(id, fieldsToFetch);
        // return ordernarValores(super.findById(id, fieldsToFetch));
        return super.findById( id, fieldsToFetch );
    }


    // private Questao ordernarValores(Questao questao) {
    // if (questao != null) {
    // if (questao.getListaValores() != null &&
    // questao.getListaValores().getValoresLista() != null) {
    // questao.getListaValores().setValoresLista(new
    // TreeList<ValorLista>(questao.getListaValores().getValoresLista()));
    // }
    // return questao;
    // } else {
    // return null;
    // }
    // }

    @Override
    @Transactional( readOnly = true )
    public Paginator< Questao > pesquisar( Paginator< Questao > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Questao >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( Questao entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( Questao entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNomeQuestao() != null ) {
                fields.put( "nomeQuestao", entity.getNomeQuestao() );
            }
            if ( entity.getPerguntaQuestao() != null ) {
                fields.put( "perguntaQuestao", entity.getPerguntaQuestao() );
            }
            if ( entity.getIsLista() ) {
                fields.put( "isLista", entity.getIsLista() );
            }
            if ( entity.getIsTexto() ) {
                fields.put( "isTexto", entity.getIsTexto() );
            }
            // if (entity.getGrupoQuestao() != null &&
            // entity.getGrupoQuestao().getId() != null) {
            // fields.put("grupoQuestao.id", entity.getGrupoQuestao().getId());
            // }
        }

        return fields;
    }


    @Override
    @Transactional
    public Questao save( Questao entity, String usuarioCadastro )
        throws ServiceException {

        if ( entity != null ) {

            entity = validarSaveUpdate( entity );
            entity.setDataCadastro( new Date() );
            entity.setUsuarioCadastro( usuarioCadastro );
            return dao.save( entity );

        } else {
            throw new ServiceException( "Campos obrigatórios não preenchidos" );
        }
    }


    @Override
    @Transactional
    public void update( Questao entity, String usuarioAtualizacao )
        throws ServiceException {

        // validarEstaAssociadoQuestionario(entity);
        entity = validarSaveUpdate( entity );
        entity.setDataAtualizacao( new Date() );
        entity.setUsuarioAtualizacao( usuarioAtualizacao );
        dao.update( entity );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        // validarEstaAssociadoQuestionario(id);
        super.deleteLogic( id, usuarioAtualizacao );
    }


    @Transactional
    private Questao validarSaveUpdate( Questao entity )
        throws ServiceException {

        if ( ( entity.getIsLista() && entity.getListaValores() == null ) ) {
            throw new ServiceException( "Campos obrigatórios não preenchidos" );
        }

        if ( entity.getIsLista() ) {
            if ( StringUtils.isBlank( entity.getListaValores().getNome() ) || CollectionUtils.isEmpty( entity.getListaValores().getValoresLista() ) ) {
                throw new ServiceException( "Campos obrigatórios não preenchidos" );
            }
            entity = configurarOrdem( entity );
            if ( entity.getListaValores().getId() == null ) {
                entity.getListaValores().setIdEmpresa( entity.getIdEmpresa() );
                entity.setListaValores( listaValoresDAO.save( entity.getListaValores() ) );
            } else {
                // listaValoresDAO.update(entity.getListaValores());
            }
        }

        // if ( entity.getListaValores() != null ) {
        // entity.getListaValores().setIdEmpresa( entity.getIdEmpresa() );
        // }

        return entity;
    }


    private Questao configurarOrdem( Questao entity ) {

        int i = 0;
        Set< ValorLista > listaValoresOrdenada = new LinkedHashSet< ValorLista >();

        if ( entity.getListaValores() != null && entity.getListaValores().getValoresLista() != null ) {
            for ( ValorLista valorLista : entity.getListaValores().getValoresLista() ) {
                if ( valorLista.getOrdem() == null ) {
                    valorLista.setOrdem( i );
                }
                i++;
                listaValoresOrdenada.add( valorLista );
            }
            entity.getListaValores().setValoresLista( listaValoresOrdenada );
        }

        return entity;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Questao > findByListaValores( Long idListaValores )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap<>();
        attrs.put( "listaValores.id", idListaValores );
        return findByAttr( attrs );
    };


    private void validarEstaAssociadoQuestionario( Questao entity )
        throws ServiceException {

        if ( entity == null || entity.getId() == null ) {
            throw new ServiceException( "Parâmetros nulos. [idQuestao]" );
        }
        validarEstaAssociadoQuestionario( entity.getId() );
    }


    private void validarEstaAssociadoQuestionario( Long idQuestao )
        throws ServiceException {

        if ( idQuestao == null ) {
            throw new ServiceException( "Parâmetros nulos. [idQuestao]" );
        }

        Questao questao = findById( idQuestao );
        if ( questao == null ) {
            throw new ServiceException( "Questão id=" + idQuestao + ", não encontrada" );
        }

        List< Questionario > questionarios = questionarioService.findByQuestao( Arrays.asList( questao ) );
        if ( CollectionUtils.isNotEmpty( questionarios ) ) {
            String questionariosNomes = "";
            int c = 0;
            for ( Questionario q : questionarios ) {
                if ( c == 3 ) {
                    break;
                }
                if ( c > 0 ) {
                    questionariosNomes += ", ";
                }
                questionariosNomes += q.getNome();
                c++;
            }
            String aoAos = questionarios.size() > 1 ? "aos questionários: " : "ao questionário: ";
            String entreOutros = questionarios.size() > 3 ? ", entre outros ..." : "";
            throw new ServiceException( "Questão está associada " + aoAos + questionariosNomes + entreOutros );
        }

    }

}
