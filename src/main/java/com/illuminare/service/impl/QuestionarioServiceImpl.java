package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.QuestionarioDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.ListaValores;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.ListaValoresService;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoClassificacaoService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class QuestionarioServiceImpl extends GenericServiceImpl< Questionario > implements QuestionarioService {

    @Autowired
    QuestionarioDAO dao;
    @Autowired
    ListaValoresService listaValoresService;
    @Autowired
    QuestaoService questaoService;
    @Autowired
    PesquisaService pesquisaService;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    QuestaoClassificacaoService questaoClassificacaoService;
    @Autowired
    private UserService userService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< Questionario > getDao() {

        return dao;
    }


    @Override
    public Questionario findById( Long id, List< String > fieldsToFetch ) {

        if ( fieldsToFetch == null ) {
            fieldsToFetch = new ArrayList<>();
        }

        Set< String > fetchQuestionarioFields = new LinkedHashSet<>( fieldsToFetch.size() );

        fetchQuestionarioFields.addAll( fieldsToFetch );

        return findById( id, fetchQuestionarioFields );
    }


    @Transactional( readOnly = true )
    @Override
    public Questionario findById( Long id, Set< String > fieldsToFetch ) {

        Set< String > newFieldsToFetch = new LinkedHashSet<>();

        boolean fetchClassificacao = false;
        if ( fieldsToFetch != null ) {
            for ( String fetch : fieldsToFetch ) {
                if ( "classificacao".equals( fetch ) ) {
                    fetchClassificacao = true;
                } else {
                    newFieldsToFetch.add( fetch );
                }
            }

        }

        Questionario questionario = super.findById( id, newFieldsToFetch );

        if ( questionario != null ) {
            if ( fetchClassificacao ) {

                if ( CollectionUtils.isNotEmpty( questionario.getGruposQuestoes() ) ) {
                    for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
                        if ( CollectionUtils.isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {
                            for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {
                                QuestaoClassificacao classificacao =
                                    questaoClassificacaoService.findByQuestionarioAndQuestao( questionario, questaoOrdem.getQuestao(), grupoQuestao );
                                questaoOrdem.getQuestao().unproxy( dao );
                                questaoOrdem.getQuestao().setQuestaoClassificacao( classificacao );
                                Questao newQuestao = new Questao();
                                try {
                                    BeanUtils.copyProperties( newQuestao, questaoOrdem.getQuestao() );
                                } catch ( Exception e ) {
                                    e.printStackTrace();
                                }
                                questaoOrdem.setQuestao( newQuestao );
                            }

                        }
                    }

                }

            }
        }

        return questionario;
    }


    @Transactional
    @Override
    public Questionario save( Questionario questionario, String usuarioCadastro )
        throws ServiceException {

        saveValidate( questionario );

        return super.save( questionario, usuarioCadastro );
    }


    private void saveValidate( Questionario questionario )
        throws ServiceException {

        boolean cargoOK, grupoOcupacionalOk, setorOk;

        if ( CollectionUtils.isNotEmpty( questionario.getCargos() ) ) {
            Set< Cargo > newCargos = new HashSet<>();
            for ( Cargo cargo : questionario.getCargos() ) {
                if ( cargo != null && cargo.getId() != null ) {
                    newCargos.add( cargo );
                }
            }
            questionario.setCargos( newCargos );
        }
        cargoOK = CollectionUtils.isNotEmpty( questionario.getCargos() );

        if ( CollectionUtils.isNotEmpty( questionario.getSetores() ) ) {
            Set< Setor > newSetores = new HashSet<>();
            for ( Setor setor : questionario.getSetores() ) {
                if ( setor != null && setor.getId() != null ) {
                    newSetores.add( setor );
                }
            }
            questionario.setSetores( newSetores );
        }
        setorOk = CollectionUtils.isNotEmpty( questionario.getSetores() );

        if ( CollectionUtils.isNotEmpty( questionario.getGruposOcupacionais() ) ) {
            Set< GrupoOcupacional > newGrupoOcupacionais = new HashSet<>();
            for ( GrupoOcupacional grupoOcupacional : questionario.getGruposOcupacionais() ) {
                if ( grupoOcupacional != null && grupoOcupacional.getId() != null ) {
                    newGrupoOcupacionais.add( grupoOcupacional );
                }
            }
            questionario.setGruposOcupacionais( newGrupoOcupacionais );
        }
        grupoOcupacionalOk = CollectionUtils.isNotEmpty( questionario.getGruposOcupacionais() );

        if ( !grupoOcupacionalOk && !setorOk && !cargoOK ) {
            throw new ServiceException( "Por favor, selecionar pelo menos um Grupo Ocupacional ou Cargo ou Setor" );
        }
    }


    @Override
    @Transactional( readOnly = true )
    public Paginator< Questionario > pesquisar( Paginator< Questionario > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Questionario >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFieldsToSerch( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( Questionario entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFieldsToSerch( Questionario entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getObs() != null ) {
                fields.put( "obs", entity.getObs() );
            }
            if ( CollectionUtils.isNotEmpty( entity.getGruposOcupacionais() ) ) {
                List< Long > ids = new ArrayList<>();
                for ( GrupoOcupacional g : entity.getGruposOcupacionais() ) {
                    if ( g != null && g.getId() != null ) {
                        ids.add( g.getId() );
                    }
                }
                if ( ids.size() > 0 ) {
                    fields.put( "[g].id in ", ids );
                }
            }
        }

        return fields;
    }


    @Override
    @Transactional
    public void update( Questionario entity, String usuarioAtualizacao )
        throws ServiceException {

        saveValidate( entity );

        validarEstaAssociadoPesquisa( entity );
        super.update( entity, usuarioAtualizacao );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        validarEstaAssociadoPesquisa( id );
        super.deleteLogic( id, usuarioAtualizacao );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Questionario > findByQuestao( List< Questao > questoes )
        throws ServiceException {

        if ( CollectionUtils.isEmpty( questoes ) ) {
            return null;
        }
        return dao.findByQuestao( questoes );
    }


    @Override
    @Transactional( readOnly = true )
    public List< Questionario > findByListaValores( Long idListaValores )
        throws ServiceException {

        ListaValores listaValores = listaValoresService.findById( idListaValores );
        if ( listaValores != null ) {
            List< Questao > questoes = questaoService.findByListaValores( listaValores.getId() );
            return findByQuestao( questoes );
        }
        return null;
    }


    private void validarEstaAssociadoPesquisa( Questionario entity )
        throws ServiceException {

        if ( entity == null || entity.getId() == null ) {
            throw new ServiceException( "Parâmetros nulos. [idQuestionario]" );
        }
        validarEstaAssociadoPesquisa( entity.getId() );
    }


    private void validarEstaAssociadoPesquisa( Long idQuestionario )
        throws ServiceException {

        if ( idQuestionario == null ) {
            throw new ServiceException( "Parâmetros nulos. [idQuestionario]" );
        }

        Questionario questionario = findById( idQuestionario );
        if ( questionario == null ) {
            throw new ServiceException( "Questionário id=" + idQuestionario + ", não encontrado" );
        }

        List< Pesquisa > pesquisas = pesquisaService.findByQuestionario( Arrays.asList( questionario ) );
        if ( CollectionUtils.isNotEmpty( pesquisas ) ) {
            for ( Pesquisa e : pesquisas ) {
                if ( e.getExcluido() == null || !e.getExcluido() ) {
                    String pesquisasNomes = "";
                    int c = 0;
                    for ( Pesquisa p : pesquisas ) {
                        if ( c == 3 ) {
                            break;
                        }
                        if ( c > 0 ) {
                            pesquisasNomes += ", ";
                        }
                        pesquisasNomes += p.getNome();
                        c++;
                    }
                    String aoAos = pesquisas.size() > 1 ? "as pesquisas: " : "a pesquisa: ";
                    String entreOutras = pesquisas.size() > 3 ? ", entre outras ..." : "";
                    throw new ServiceException( "Questionário está associado " + aoAos + pesquisasNomes + entreOutras );
                }
            }
        }

    }

}
