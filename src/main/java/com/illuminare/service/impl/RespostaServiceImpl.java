package com.illuminare.service.impl;


import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.QuestaoOrdemView;
import com.illuminare.view.QuestionarioView;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.RespostaDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.Setor;
import com.illuminare.exceptions.DAOException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.AvaliacaoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.QuestaoClassificacaoService;
import com.illuminare.service.RespostaService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;
import com.illuminare.view.QuestionarioRespondido;


@Service
public class RespostaServiceImpl extends GenericServiceImpl< Resposta > implements RespostaService {

    @Autowired
    RespostaDAO dao;

    @Autowired
    UserService userService;

    @Autowired
    FuncionarioService funcionarioService;

    @Autowired
    AvaliacaoService avaliacaoService;

    @Autowired
    EmpresaService empresaService;

    @Autowired
    QuestaoClassificacaoService questaoClassificacaoService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< Resposta > getDao() {

        return dao;
    }


    @Override
    public Paginator< Resposta > pesquisar( Paginator< Resposta > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Resposta >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }
        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( Resposta entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( Resposta entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
        }

        return fields;
    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByAvaliado( Long idAvaliado )
        throws ServiceException {

        Assert.notNull( idAvaliado );
        return dao.findByAvaliado( idAvaliado );
    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByAvaliado( Long idAvaliado, List< Long > idsPesquisas )
        throws ServiceException {

        Assert.notNull( idAvaliado );
        return dao.findByAvaliado( idAvaliado, idsPesquisas );
    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByAvaliado( Funcionario funcionarioAvaliado, Pesquisa pesquisa )
        throws ServiceException {

        Assert.notNull( funcionarioAvaliado );
        Assert.notNull( funcionarioAvaliado.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        return dao.findByAvaliado( funcionarioAvaliado, pesquisa );

    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByGrupoOcupacional( Pesquisa pesquisa, GrupoOcupacional grupoOcupacional )
        throws ServiceException {

        Assert.notNull( grupoOcupacional );
        Assert.notNull( grupoOcupacional.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        List< Funcionario > funcionarios = funcionarioService.findByGrupoOcupacional( grupoOcupacional );

        return dao.findByAvaliado( funcionarios, pesquisa );

    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByCargo( Pesquisa pesquisa, Cargo cargo )
        throws ServiceException {

        Assert.notNull( cargo );
        Assert.notNull( cargo.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        List< Funcionario > funcionarios = funcionarioService.findByCargo( cargo );

        return dao.findByAvaliado( funcionarios, pesquisa );

    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findBySetor( Pesquisa pesquisa, Setor setor )
        throws ServiceException {

        Assert.notNull( setor );
        Assert.notNull( setor.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        List< Funcionario > funcionarios = funcionarioService.findBySetor( setor );

        return dao.findByAvaliado( funcionarios, pesquisa );

    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByEmpresa( Pesquisa pesquisa, Empresa empresa )
        throws ServiceException {

        Assert.notNull( empresa );
        Assert.notNull( empresa.getId() );
        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        List< Funcionario > funcionarios = funcionarioService.findByEmpresa( empresa );

        return dao.findByAvaliado( funcionarios, pesquisa );

    }


    @Transactional( readOnly = true )
    @Override
    public List< Resposta > findByAvaliacao( Long idQuestionarioAvaliacao )
        throws ServiceException {

        return dao.findByAvaliacao( idQuestionarioAvaliacao );
    }


    @Transactional( readOnly = true )
    @Override
    public Boolean checkIsRespnodidoByAvaliacao( Long idQuestionarioAvaliacao ) {

        return dao.checkIsRespnodidoByAvaliacao( idQuestionarioAvaliacao );
    }


    @Transactional( readOnly = true )
    @Override
    public Resposta findByAvaliacaoAndQuestao( Long idQuestionarioAvaliacao, Long idQuestao, Long idGrupoQuestao )
        throws ServiceException {

        List< Resposta > result = dao.findByAvaliacaoAndQuestao( idQuestionarioAvaliacao, idQuestao, idGrupoQuestao );
        if ( isNotEmpty( result ) ) {
            return result.get( 0 );
        }
        return null;
    }


    @Transactional( readOnly = true )
    @Override
    public List< QuestionarioView > findByAvaliacaoAgrupado( Long idQuestionarioAvaliacao )
        throws ServiceException {

        List< QuestionarioView > questionariosResult = new ArrayList<>();

        QuestionarioAvaliacao avaliacao = avaliacaoService.findByIdCurrent( idQuestionarioAvaliacao );

        Set< Questionario > questionarios = avaliacao.getPesquisa().getQuestionarios();

        for ( Questionario q : questionarios ) {

            QuestionarioView questionarioView = q.formatToWebPage().format();

            QuestionarioAvaliacao a = avaliacaoService.findByFuncionarioAvaliadoAndAvaliadorAndPesquisaAndQuestionario(
                avaliacao.getFuncionarioAvaliado(),
                avaliacao.getFuncionarioAvaliador(),
                avaliacao.getPesquisa(),
                q );

            if ( a != null ) {

                Boolean temRespostas = false;
                if ( questionarioView.getGruposQuestoes() != null ) {
                    for ( GrupoQuestaoView gq : questionarioView.getGruposQuestoes() ) {
                        if ( gq.getQuestoesOrdem() != null ) {
                            for ( QuestaoOrdemView qo : gq.getQuestoesOrdem() ) {
                                if ( qo.getQuestao() != null ) {
                                    Resposta resposta = findByAvaliacaoAndQuestao( a.getId(), qo.getQuestao().getId(), gq.getId() );
                                    // removerReferenciasCirculares( resposta );
                                    if ( resposta != null ) {
                                        qo.getQuestao().setResposta( resposta.formatToWebPage().format() );
                                        temRespostas = true;
                                    }
                                }
                            }
                        }
                    }
                }
                questionarioView.setPesquisaId( avaliacao.getPesquisa().getId() );
                questionarioView.setPesquisaNome( avaliacao.getPesquisa().getNome() );
                questionarioView.setAvaliadoId( avaliacao.getFuncionarioAvaliado().getId() );
                questionarioView.setAvaliadoNome( avaliacao.getFuncionarioAvaliado().getPessoa().getNome() );
                questionarioView.setAvaliadorId( avaliacao.getFuncionarioAvaliador().getId() );
                questionarioView.setAvaliadorNome( avaliacao.getFuncionarioAvaliador().getPessoa().getNome() );

                if ( temRespostas ) {
                    questionariosResult.add( questionarioView );
                }

            }
        }

        if ( CollectionUtils.isEmpty( questionariosResult ) ) {
            throw new ServiceException( "Avaliação não encontrada" );
        }

        return questionariosResult;

    }


    @Transactional
    @Override
    public void deleteByAvaliacao( Long idAvaliacao, String user )
        throws ServiceException {

        Assert.notNull( idAvaliacao, "Parameter idAvaliacao is required." );
        Assert.notNull( user, "Parameter user is required." );

        try {
            dao.deleteByAvaliacao( idAvaliacao );
        } catch ( DAOException e ) {
            throw new ServiceException( e );
        }
    }


    @Transactional( readOnly = true )
    @Override
    public QuestionarioRespondido questionarioRespondido( Pesquisa pesquisa, Questionario questionario, Funcionario avaliado, Funcionario avaliador )
        throws ServiceException {

        Assert.notNull( pesquisa, "Parameter pesquisa is required." );
        Assert.notNull( questionario, "Parameter questionario is required." );
        Assert.notNull( avaliado, "Parameter avaliado is required." );
        Assert.notNull( avaliador, "Parameter avaliador is required." );

        QuestionarioRespondido questionarioRespondido = new QuestionarioRespondido();

        questionarioRespondido.setDescricao( "Pendente" );
        questionarioRespondido.setNaoRespondido( true );

        List< QuestionarioAvaliacao > questionarioAvaliacoes =
            avaliacaoService.findByPesquisaAndQuestionarioAndAvaliadoAndAvaliador( pesquisa, questionario, avaliado, avaliador );

        if ( isNotEmpty( questionarioAvaliacoes ) ) {
            boolean respondidoParcial = false;
            boolean respondidoTotal = true;

            QuestionarioAvaliacao avaliacao = questionarioAvaliacoes.get( 0 );

            int totalRespotas = 0;
            if ( isNotEmpty( avaliacao.getRespostas() ) ) {
                totalRespotas = avaliacao.getRespostas().size();
                for ( Resposta resposta : avaliacao.getRespostas() ) {
                    if ( resposta.getRespostaNumerico() != null ) {
                        respondidoParcial = true;
                        continue;
                    }
                    if ( StringUtils.isNotBlank( resposta.getRespostaTexto() ) ) {
                        respondidoParcial = true;
                        continue;
                    }
                    if ( resposta.getRespostaValorLista() != null && resposta.getRespostaValorLista().getId() != null ) {
                        respondidoParcial = true;
                        continue;
                    }

                    respondidoTotal = false;
                }

            } else {
                respondidoTotal = false;
            }

            int totalPerguntas = getTotalPerguntas( avaliacao.getQuestionario() );

            if ( respondidoTotal && totalRespotas >= totalPerguntas ) {

                questionarioRespondido.setDescricao( "Respondido" );
                questionarioRespondido.setRespondidoTotal( true );
                questionarioRespondido.setNaoRespondido( false );

            } else if ( respondidoParcial ) {

                questionarioRespondido.setDescricao( "Respondido Parcial" );
                questionarioRespondido.setRespondidoParcial( true );
                questionarioRespondido.setNaoRespondido( false );
            }

        }

        return questionarioRespondido;

    }


    private int getTotalPerguntas( Questionario questionario ) {

        int total = 0;

        if ( isNotEmpty( questionario.getGruposQuestoes() ) ) {
            for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
                if ( isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {

                    if ( isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {
                        for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                            QuestaoClassificacao questaoClassificacao =
                                questaoClassificacaoService.findByQuestionarioAndQuestao( questionario, questaoOrdem.getQuestao(), grupoQuestao );
                            if ( questaoClassificacao != null ) {
                                if ( questaoClassificacao.getClassificacao() <= 2 ) {
                                    total++;
                                }
                            } else {
                                total++;
                            }
                        }

                    }

                    // total += grupoQuestao.getQuestoesOrdem().size();
                }
            }

        }

        return total;
    }


    private void removerReferenciasCirculares( List< Resposta > respostas ) {

        if ( respostas != null ) {
            for ( Resposta r : respostas ) {
                removerReferenciasCirculares( r );
            }
        }
    }


    private void removerReferenciasCirculares( Resposta resposta ) {

        if ( resposta != null ) {
            resposta.setQuestionarioAvaliacao( null );
            resposta.setQuestao( null );
        }
    }


    @Override
    @Transactional
    public int removerDuplicadas( Long idEmpresa )
        throws ServiceException {

        List< Map< String, Object > > all = dao.find( null, null, null );
        int totalDuplicates = 0;
        if ( isNotEmpty( all ) ) {
            for ( Map< String, Object > resposta : all ) {

                Object idQuestionarioAvaliacao = resposta.get( "id_questionario_avaliacao" );
                Object idQuestao = resposta.get( "id_questao" );
                Object idGrupoQuestao = resposta.get( "id_grupo_questao" );

                if ( idQuestionarioAvaliacao == null || idQuestao == null || idGrupoQuestao == null ) {
                    continue;
                }

                List< Map< String, Object > > duplicadas = dao.find( (Long) idQuestionarioAvaliacao, (Long) idQuestao, (Long) idGrupoQuestao );
                if ( duplicadas.size() > 1 ) {
                    for ( int c = 0; c < duplicadas.size(); c++ ) {
                        if ( c > 0 ) {
                            dao.deleteById( (Long) resposta.get( "id" ) );
                            totalDuplicates++;
                        }
                    }
                }
            }

        }
        return totalDuplicates;
    }

}
