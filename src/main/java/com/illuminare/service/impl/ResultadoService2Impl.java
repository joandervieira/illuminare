package com.illuminare.service.impl;


import static com.illuminare.util.NumberUtil.getDouble;
import static com.illuminare.util.NumberUtil.isNull;
import static com.illuminare.util.NumberUtil.notNull;
import static com.illuminare.util.NumberUtil.notNullNumber;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.FuncionarioDAO;
import com.illuminare.dao.GrupoQuestaoDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.QuestionarioAvaliacao;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.Setor;
import com.illuminare.entity.User;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.resultado.Resultado;
import com.illuminare.service.AvaliacaoService;
import com.illuminare.service.CargoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoMetaService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.RespostaService;
import com.illuminare.service.ResultadoService2;
import com.illuminare.service.SetorService;
import com.illuminare.service.UserService;
import com.illuminare.util.DateUtil;
import com.illuminare.view.CargoView;
import com.illuminare.view.GrupoOcupacionaisCargosSetoresView;
import com.illuminare.view.GrupoOcupacionalView;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.PesquisaResultView;
import com.illuminare.view.PesquisaView;
import com.illuminare.view.QuestaoMetaView;
import com.illuminare.view.QuestaoOrdemView;
import com.illuminare.view.QuestaoView;
import com.illuminare.view.QuestionarioView;
import com.illuminare.view.SetorView;
import com.illuminare.view.ValorListaView;


@Service
public class ResultadoService2Impl implements ResultadoService2 {

    // Logger log = Logger.getLogger( ResultadoService2Impl.class );

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Autowired
    private RespostaService respostaService;

    @Autowired
    private PesquisaService pesquisaService;

    @Autowired
    private FuncionarioService funcionarioService;

    @Autowired
    private GrupoQuestaoDAO grupoQuestaoDAO;

    @Autowired
    private GrupoOcupacionalService grupoOcupacionalService;

    @Autowired
    private CargoService cargoService;

    @Autowired
    private QuestionarioService questionarioService;

    @Autowired
    private SetorService setorService;

    @Autowired
    private UserService userService;

    @Autowired
    private QuestaoMetaService questaoMetaService;

    @Autowired
    private QuestaoService questaoService;

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private FuncionarioDAO funcionarioDAO;


    @Override
    @Transactional( readOnly = true )
    public Resultado resultadoPorFuncionarioAvaliado( Long idPesquisa, Long idAvaliado, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );
        PesquisaResultView pesquisaView = new PesquisaResultView( pesquisa.getId(), pesquisa.getNome() );
        pesquisaView.setEntity( pesquisa );

        Funcionario avaliado = funcionarioService.findById( idAvaliado, Arrays.asList( "cargo c", "[c].grupoOcupacional" ) );
        if ( avaliado == null ) {
            throw new ServiceException( "Funcionário não encontrado" );
        }

        List< Funcionario > avaliados = Collections.singletonList( avaliado );

        List< QuestionarioView > questionariosResult = new ArrayList<>();

        buscarRespostasDoAvaliadoColocarNoQuestionario( idPesquisa, avaliados, questionariosResult );

        somarGerarMedias( questionariosResult, pesquisaView, avaliados );

        Resultado resultado = new Resultado();
        resultado.setQuestionarios( questionariosResult );
        resultado.setPesquisa( pesquisaView );
        resultado.setAvaliado( avaliado.formatToWebPage().format() );
        resultado.cleanResult();

        return resultado;
    }


    @Transactional( readOnly = true )
    @Override
    public Resultado resultadoPorGrupoOcupacional( Long idPesquisa, Long idGrupoOcupacional, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );
        PesquisaResultView pesquisaView = new PesquisaResultView( pesquisa.getId(), pesquisa.getNome() );
        pesquisaView.setEntity( pesquisa );

        GrupoOcupacional grupoOcupacional = grupoOcupacionalService.findById( idGrupoOcupacional );
        if ( grupoOcupacional == null ) {
            throw new ServiceException( "Grupo Ocupacional não encontrado" );
        }

        List< Funcionario > avaliados = funcionarioService.findByGrupoOcupacional( grupoOcupacional );

        List< QuestionarioView > questionariosResult = new ArrayList<>();

        buscarRespostasDoAvaliadoColocarNoQuestionario( idPesquisa, avaliados, questionariosResult );

        somarGerarMedias( questionariosResult, pesquisaView, avaliados );

        Resultado resultado = new Resultado();
        resultado.setQuestionarios( questionariosResult );
        resultado.setPesquisa( pesquisaView );
        resultado.setGrupoOcupacional( grupoOcupacional.formatToWebPage().format() );
        resultado.cleanResult();

        return resultado;

    }


    @Transactional( readOnly = true )
    @Override
    public Resultado resultadoPorCargo( Long idPesquisa, Long idCargo, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );
        PesquisaResultView pesquisaView = new PesquisaResultView( pesquisa.getId(), pesquisa.getNome() );
        pesquisaView.setEntity( pesquisa );

        Cargo cargo = cargoService.findById( idCargo );
        if ( cargo == null ) {
            throw new ServiceException( "Cargo não encontrado" );
        }

        List< Funcionario > avaliados = funcionarioService.findByCargo( cargo );

        List< QuestionarioView > questionariosResult = new ArrayList<>();

        buscarRespostasDoAvaliadoColocarNoQuestionario( idPesquisa, avaliados, questionariosResult );

        somarGerarMedias( questionariosResult, pesquisaView, avaliados );

        Resultado resultado = new Resultado();
        resultado.setQuestionarios( questionariosResult );
        resultado.setPesquisa( pesquisaView );
        resultado.setCargo( cargo.formatToWebPage().format() );
        resultado.cleanResult();

        return resultado;
    }

    @Transactional( readOnly = true )
    @Override
    public Resultado resultadoPorSetor( Long idPesquisa, Long idSetor, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );
        PesquisaResultView pesquisaView = new PesquisaResultView( pesquisa.getId(), pesquisa.getNome() );
        pesquisaView.setEntity( pesquisa );

        Setor setor = setorService.findById( idSetor );
        if ( setor == null ) {
            throw new ServiceException( "setor não encontrado" );
        }

        List< Funcionario > avaliados = funcionarioService.findBySetor( setor );

        List< QuestionarioView > questionariosResult = new ArrayList<>();

        buscarRespostasDoAvaliadoColocarNoQuestionario( idPesquisa, avaliados, questionariosResult );

        somarGerarMedias( questionariosResult, pesquisaView, avaliados );

        Resultado resultado = new Resultado();
        resultado.setQuestionarios( questionariosResult );
        resultado.setPesquisa( pesquisaView );
        resultado.setSetor( setor.formatToWebPage().format() );
        resultado.cleanResult();

        return resultado;
    }

    @Transactional( readOnly = true )
    @Override
    public Resultado resultadoPorEmpresa( Long idPesquisa, Long idEmpresa, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );
        PesquisaResultView pesquisaView = new PesquisaResultView( pesquisa.getId(), pesquisa.getNome() );
        pesquisaView.setEntity( pesquisa );

        Empresa empresa = empresaService.findById( idEmpresa );
        if ( empresa == null ) {
            throw new ServiceException( "empresa não encontrado" );
        }

        List< Funcionario > avaliados = funcionarioService.findByEmpresa( empresa );

        List< QuestionarioView > questionariosResult = new ArrayList<>();

        buscarRespostasDoAvaliadoColocarNoQuestionario( idPesquisa, avaliados, questionariosResult );

        somarGerarMedias( questionariosResult, pesquisaView, avaliados );

        Resultado resultado = new Resultado();
        resultado.setQuestionarios( questionariosResult );
        resultado.setPesquisa( pesquisaView );
        resultado.setEmpresa( empresa );
        resultado.cleanResult();

        return resultado;
    }


    private void somarGerarMedias( List< QuestionarioView > questionariosResult, PesquisaResultView pesquisa, List< Funcionario > avaliados ) {

        if ( isNotEmpty( questionariosResult ) ) {
            // cache
            List< QuestaoMeta > questaoMetasDoQuestionario = questaoMetaService.findByPesquisa( pesquisa.getId() );
            List< QuestaoMetaView > questaoMetasDoQuestionarioView = QuestaoMeta.formatToWebPage( questaoMetasDoQuestionario );

            for ( QuestionarioView questionario : questionariosResult ) {
                if ( isNotEmpty( questionario.getGruposQuestoes() ) ) {
                    for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {
                        Double maiorNotaGrupo = getMaiorNotaGrupo( grupoQuestao );
                        for ( QuestaoOrdemView questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {
                            QuestaoView questao = questaoOrdem.getQuestao();

                            questao.setRespostasProprias( getRespostas( questionario.getRespostasProprias(), grupoQuestao, questao ) );
                            questao.setRespostasLider( getRespostas( questionario.getRespostasLider(), grupoQuestao, questao ) );
                            questao.setRespostasSubordinados( getRespostas( questionario.getRespostasSubordinados(), grupoQuestao, questao ) );
                            questao.setRespostasOutros( getRespostas( questionario.getRespostasOutros(), grupoQuestao, questao ) );

                            calcularMediaDaQuestao( questao );
                            fillMetaNaQuestao( questao, avaliados, grupoQuestao, questionario, pesquisa, questaoMetasDoQuestionarioView );
                            calcularValorObtidoDaQuestao( questao );
                            calcularPercentualMetaDaQuestao( questao );

                            ajustarNotasRegrasPesquisa( pesquisa, questao, maiorNotaGrupo );

                            somarNotasGrupo( questao, grupoQuestao );

                        }
                        gerarMediaGrupo( grupoQuestao );
                        somarNotasQuestionario( grupoQuestao, questionario );
                    }
                    gerarMediaQuestionario( questionario );
                }
                somarNotasPesquisa( questionario, pesquisa );
            }
            gerarMediaPesquisa( pesquisa );
        }

    }


    private void gerarMediaQuestionario( QuestionarioView questionario ) {

        if ( notNull( questionario.getMediaPropria() ) && questionario.qtdeTotalPropria > 0 ) {
            questionario.setMediaPropria( questionario.getMediaPropria() / questionario.qtdeTotalPropria );
        }

        if ( notNull( questionario.getMediaLideres() ) && questionario.qtdeTotalLider > 0 ) {
            questionario.setMediaLideres( questionario.getMediaLideres() / questionario.qtdeTotalLider );
        }

        if ( notNull( questionario.getMediaSubordinados() ) && questionario.qtdeTotalSubordinados > 0 ) {
            questionario.setMediaSubordinados( questionario.getMediaSubordinados() / questionario.qtdeTotalSubordinados );
        }

        if ( notNull( questionario.getMediaOutros() ) && questionario.qtdeTotalOutros > 0 ) {
            questionario.setMediaOutros( questionario.getMediaOutros() / questionario.qtdeTotalOutros );
        }

        if ( notNull( questionario.getMeta() ) && questionario.qtdeTotalMeta > 0 ) {
            questionario.setMeta( questionario.getMeta() / questionario.qtdeTotalMeta );
        }

        if ( notNull( questionario.getValorObtido() ) && questionario.qtdeTotalValorObtido > 0 ) {
            questionario.setValorObtido( questionario.getValorObtido() / questionario.qtdeTotalValorObtido );
        }

        if ( notNull( questionario.getPercentualMeta() ) && questionario.qtdeTotalPercentualMeta > 0 ) {
            questionario.setPercentualMeta( questionario.getPercentualMeta() / questionario.qtdeTotalPercentualMeta );
        }
    }


    private void gerarMediaPesquisa( PesquisaResultView pesquisa ) {

        if ( notNull( pesquisa.getMediaPropria() ) && pesquisa.qtdeTotalPropria > 0 ) {
            pesquisa.setMediaPropria( pesquisa.getMediaPropria() / pesquisa.qtdeTotalPropria );
        }

        if ( notNull( pesquisa.getMediaLideres() ) && pesquisa.qtdeTotalLider > 0 ) {
            pesquisa.setMediaLideres( pesquisa.getMediaLideres() / pesquisa.qtdeTotalLider );
        }

        if ( notNull( pesquisa.getMediaSubordinados() ) && pesquisa.qtdeTotalSubordinados > 0 ) {
            pesquisa.setMediaSubordinados( pesquisa.getMediaSubordinados() / pesquisa.qtdeTotalSubordinados );
        }

        if ( notNull( pesquisa.getMediaOutros() ) && pesquisa.qtdeTotalOutros > 0 ) {
            pesquisa.setMediaOutros( pesquisa.getMediaOutros() / pesquisa.qtdeTotalOutros );
        }

        if ( notNull( pesquisa.getMeta() ) && pesquisa.qtdeTotalMeta > 0 ) {
            pesquisa.setMeta( pesquisa.getMeta() / pesquisa.qtdeTotalMeta );
        }

        if ( notNull( pesquisa.getValorObtido() ) && pesquisa.qtdeTotalValorObtido > 0 ) {
            pesquisa.setValorObtido( pesquisa.getValorObtido() / pesquisa.qtdeTotalValorObtido );
        }

        if ( notNull( pesquisa.getPercentualMeta() ) && pesquisa.qtdeTotalPercentualMeta > 0 ) {
            pesquisa.setPercentualMeta( pesquisa.getPercentualMeta() / pesquisa.qtdeTotalPercentualMeta );
        }
    }


    private void somarNotasPesquisa( QuestionarioView questionario, PesquisaResultView pesquisa ) {

        if ( notNull( questionario.getMediaPropria() ) ) {
            if ( pesquisa.getMediaPropria() == null ) {
                pesquisa.setMediaPropria( 0d );
            }
            pesquisa.setMediaPropria( pesquisa.getMediaPropria() + questionario.getMediaPropria() );
            pesquisa.qtdeTotalPropria++;
        }

        if ( notNull( questionario.getMediaLideres() ) ) {
            if ( pesquisa.getMediaLideres() == null ) {
                pesquisa.setMediaLideres( 0d );
            }
            pesquisa.setMediaLideres( pesquisa.getMediaLideres() + questionario.getMediaLideres() );
            pesquisa.qtdeTotalLider++;
        }

        if ( notNull( questionario.getMediaSubordinados() ) ) {
            if ( pesquisa.getMediaSubordinados() == null ) {
                pesquisa.setMediaSubordinados( 0d );
            }
            pesquisa.setMediaSubordinados( pesquisa.getMediaSubordinados() + questionario.getMediaSubordinados() );
            pesquisa.qtdeTotalSubordinados++;
        }

        if ( notNull( questionario.getMediaOutros() ) ) {
            if ( pesquisa.getMediaOutros() == null ) {
                pesquisa.setMediaOutros( 0d );
            }
            pesquisa.setMediaOutros( pesquisa.getMediaOutros() + questionario.getMediaOutros() );
            pesquisa.qtdeTotalOutros++;
        }

        if ( notNull( questionario.getMeta() ) ) {
            if ( pesquisa.getMeta() == null ) {
                pesquisa.setMeta( 0d );
            }
            pesquisa.setMeta( pesquisa.getMeta() + questionario.getMeta() );
            pesquisa.qtdeTotalMeta++;
        }

        if ( notNull( questionario.getValorObtido() ) ) {
            if ( pesquisa.getValorObtido() == null ) {
                pesquisa.setValorObtido( 0d );
            }
            pesquisa.setValorObtido( pesquisa.getValorObtido() + questionario.getValorObtido() );
            pesquisa.qtdeTotalValorObtido++;
        }

        if ( notNull( questionario.getPercentualMeta() ) ) {
            if ( pesquisa.getPercentualMeta() == null ) {
                pesquisa.setPercentualMeta( 0d );
            }
            pesquisa.setPercentualMeta( pesquisa.getPercentualMeta() + questionario.getPercentualMeta() );
            pesquisa.qtdeTotalPercentualMeta++;
        }

    }


    private void somarNotasQuestionario( GrupoQuestaoView grupo, QuestionarioView questionario ) {

        if ( notNull( grupo.getMediaPropria() ) ) {
            if ( questionario.getMediaPropria() == null ) {
                questionario.setMediaPropria( 0d );
            }
            questionario.setMediaPropria( questionario.getMediaPropria() + grupo.getMediaPropria() );
            questionario.qtdeTotalPropria++;
        }

        if ( notNull( grupo.getMediaLideres() ) ) {
            if ( questionario.getMediaLideres() == null ) {
                questionario.setMediaLideres( 0d );
            }
            questionario.setMediaLideres( questionario.getMediaLideres() + grupo.getMediaLideres() );
            questionario.qtdeTotalLider++;
        }

        if ( notNull( grupo.getMediaSubordinados() ) ) {
            if ( questionario.getMediaSubordinados() == null ) {
                questionario.setMediaSubordinados( 0d );
            }
            questionario.setMediaSubordinados( questionario.getMediaSubordinados() + grupo.getMediaSubordinados() );
            questionario.qtdeTotalSubordinados++;
        }

        if ( notNull( grupo.getMediaOutros() ) ) {
            if ( questionario.getMediaOutros() == null ) {
                questionario.setMediaOutros( 0d );
            }
            questionario.setMediaOutros( questionario.getMediaOutros() + grupo.getMediaOutros() );
            questionario.qtdeTotalOutros++;
        }

        if ( notNull( grupo.getMeta() ) ) {
            if ( questionario.getMeta() == null ) {
                questionario.setMeta( 0d );
            }
            questionario.setMeta( questionario.getMeta() + grupo.getMeta() );
            questionario.qtdeTotalMeta++;
        }

        if ( notNull( grupo.getValorObtido() ) ) {
            if ( questionario.getValorObtido() == null ) {
                questionario.setValorObtido( 0d );
            }
            questionario.setValorObtido( questionario.getValorObtido() + grupo.getValorObtido() );
            questionario.qtdeTotalValorObtido++;
        }

        if ( notNull( grupo.getPercentualMeta() ) ) {
            if ( questionario.getPercentualMeta() == null ) {
                questionario.setPercentualMeta( 0d );
            }
            questionario.setPercentualMeta( questionario.getPercentualMeta() + grupo.getPercentualMeta() );
            questionario.qtdeTotalPercentualMeta++;
        }

    }


    private void gerarMediaGrupo( GrupoQuestaoView grupoQuestao ) {

        if ( notNull( grupoQuestao.getMediaPropria() ) && grupoQuestao.totalQuestoesPropria > 0 ) {
            grupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() / grupoQuestao.totalQuestoesPropria );
        }

        if ( notNull( grupoQuestao.getMediaLideres() ) && grupoQuestao.totalQuestoesLider > 0 ) {
            grupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() / grupoQuestao.totalQuestoesLider );
        }

        if ( notNull( grupoQuestao.getMediaSubordinados() ) && grupoQuestao.totalQuestoesSubordinados > 0 ) {
            grupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() / grupoQuestao.totalQuestoesSubordinados );
        }

        if ( notNull( grupoQuestao.getMediaOutros() ) && grupoQuestao.totalQuestoesOutros > 0 ) {
            grupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() / grupoQuestao.totalQuestoesOutros );
        }

        if ( notNull( grupoQuestao.getMeta() ) && grupoQuestao.totalQuestoesMeta > 0 ) {
            grupoQuestao.setMeta( grupoQuestao.getMeta() / grupoQuestao.totalQuestoesMeta );
        }

        if ( notNull( grupoQuestao.getValorObtido() ) && grupoQuestao.totalQuestoesValorObtido > 0 ) {
            grupoQuestao.setValorObtido( grupoQuestao.getValorObtido() / grupoQuestao.totalQuestoesValorObtido );
        }

        if ( notNull( grupoQuestao.getPercentualMeta() ) && grupoQuestao.totalQuestoesPercentualMeta > 0 ) {
            grupoQuestao.setPercentualMeta( grupoQuestao.getPercentualMeta() / grupoQuestao.totalQuestoesPercentualMeta );
        }
    }


    private void somarNotasGrupo( QuestaoView questao, GrupoQuestaoView grupoQuestao ) {

        if ( notNull( questao.getMediaPropria() ) ) {
            if ( grupoQuestao.getMediaPropria() == null ) {
                grupoQuestao.setMediaPropria( 0d );
            }
            grupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() + questao.getMediaPropria() );
            grupoQuestao.totalQuestoesPropria++;
        }

        if ( notNull( questao.getMediaLider() ) ) {
            if ( grupoQuestao.getMediaLideres() == null ) {
                grupoQuestao.setMediaLideres( 0d );
            }
            grupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() + questao.getMediaLider() );
            grupoQuestao.totalQuestoesLider++;
        }

        if ( notNull( questao.getMediaSubordinados() ) ) {
            if ( grupoQuestao.getMediaSubordinados() == null ) {
                grupoQuestao.setMediaSubordinados( 0d );
            }
            grupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() + questao.getMediaSubordinados() );
            grupoQuestao.totalQuestoesSubordinados++;
        }

        if ( notNull( questao.getMediaOutros() ) ) {
            if ( grupoQuestao.getMediaOutros() == null ) {
                grupoQuestao.setMediaOutros( 0d );
            }
            grupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() + questao.getMediaOutros() );
            grupoQuestao.totalQuestoesOutros++;
        }

        if ( notNull( questao.getMeta() ) ) {
            if ( grupoQuestao.getMeta() == null ) {
                grupoQuestao.setMeta( 0d );
            }
            grupoQuestao.setMeta( grupoQuestao.getMeta() + questao.getMeta() );
            grupoQuestao.totalQuestoesMeta++;
        }

        if ( notNull( questao.getValorObtido() ) ) {
            if ( grupoQuestao.getValorObtido() == null ) {
                grupoQuestao.setValorObtido( 0d );
            }
            grupoQuestao.setValorObtido( grupoQuestao.getValorObtido() + questao.getValorObtido() );
            grupoQuestao.totalQuestoesValorObtido++;
        }

        if ( notNull( questao.getPercentualMeta() ) ) {
            if ( grupoQuestao.getPercentualMeta() == null ) {
                grupoQuestao.setPercentualMeta( 0d );
            }
            grupoQuestao.setPercentualMeta( grupoQuestao.getPercentualMeta() + questao.getPercentualMeta() );
            grupoQuestao.totalQuestoesPercentualMeta++;
        }

    }


    private void calcularPercentualMetaDaQuestao( QuestaoView questao ) {

        if ( notNull( questao.getMeta() ) && questao.getMeta() > 0d && notNull( questao.getValorObtido() ) && questao.getValorObtido() > 0d ) {

            if ( questao.getNegativo() ) {
                questao.setPercentualMeta( questao.getMeta() / questao.getValorObtido() );
            } else {
                questao.setPercentualMeta( questao.getValorObtido() / questao.getMeta() );
            }

        }

    }


    private void calcularValorObtidoDaQuestao( QuestaoView questao ) {

        Double soma = 0d;
        int qtde = 0;

        if ( notNull( questao.getMediaPropria() ) ) {
            soma += questao.getMediaPropria();
            qtde++;
        }

        if ( notNull( questao.getMediaLider() ) ) {
            soma += questao.getMediaLider();
            qtde++;
        }

        if ( notNull( questao.getMediaSubordinados() ) ) {
            soma += questao.getMediaSubordinados();
            qtde++;
        }

        if ( notNull( questao.getMediaOutros() ) ) {
            soma += questao.getMediaOutros();
            qtde++;
        }

        if ( qtde > 0 ) {
            questao.setValorObtido( soma / qtde );
        }

    }


    private void fillMetaNaQuestao(
        QuestaoView questao,
        List< Funcionario > avaliados,
        GrupoQuestaoView grupoQuestao,
        QuestionarioView questionario,
        PesquisaResultView pesquisa,
        List< QuestaoMetaView > questaoMetasDoQuestionario ) {

        if ( isEmpty( questaoMetasDoQuestionario ) ) {
            return;
        }

        // Funcionario avaliado = avaliados.get( 0 );
        // avaliado.preencherGrupoOcupacional();
        GrupoOcupacionaisCargosSetoresView grupoOcupacionaisCargosSetores = getGruposOcupacionaisFuncionarios( avaliados );
        Double metaGrupoOcupacional = null;
        Double metaCargo = null;
        Double metaSetor = null;

        // GRUPO OCUPACIONAL
        int qtde = 0;
        for ( GrupoOcupacionalView grupoOcupacional : grupoOcupacionaisCargosSetores.getGruposOcupacionais() ) {
            QuestaoMetaView questaoMeta = questaoMetaService
                .getQuestaoMeta( questaoMetasDoQuestionario, questao, grupoQuestao, questionario, new PesquisaView( pesquisa.getId() ), grupoOcupacional );
            if ( questaoMeta != null ) {
                // questao.setQuestaoMeta( questaoMeta );
                if ( isNull( questao.getMeta() ) ) {
                    questao.setMeta( 0d );
                }
                questao.setMeta( questao.getMeta() + questaoMeta.getMeta() );
                questao.setNegativo( questaoMeta.getNegativo() );
                qtde++;
            }
        }
        if ( notNull( questao.getMeta() ) ) {
            metaGrupoOcupacional = questao.getMeta() / qtde;
            questao.setMeta( null );
        }

        // SETOR
        qtde = 0;
        for ( SetorView setor : grupoOcupacionaisCargosSetores.getSetores() ) {
            QuestaoMetaView questaoMeta = questaoMetaService
                .getQuestaoMeta( questaoMetasDoQuestionario, questao, grupoQuestao, questionario, new PesquisaView( pesquisa.getId() ), setor );
            if ( questaoMeta != null ) {
                // questao.setQuestaoMeta( questaoMeta );
                if ( isNull( questao.getMeta() ) ) {
                    questao.setMeta( 0d );
                }
                questao.setMeta( questao.getMeta() + questaoMeta.getMeta() );
                questao.setNegativo( questaoMeta.getNegativo() );
                qtde++;
            }
        }
        if ( notNull( questao.getMeta() ) ) {
            metaSetor = questao.getMeta() / qtde;
            questao.setMeta( null );
        }

        // CARGO
        qtde = 0;
        for ( CargoView cargo : grupoOcupacionaisCargosSetores.getCargos() ) {
            QuestaoMetaView questaoMeta = questaoMetaService
                .getQuestaoMeta( questaoMetasDoQuestionario, questao, grupoQuestao, questionario, new PesquisaView( pesquisa.getId() ), cargo );
            if ( questaoMeta != null ) {
                // questao.setQuestaoMeta( questaoMeta );
                if ( isNull( questao.getMeta() ) ) {
                    questao.setMeta( 0d );
                }
                questao.setMeta( questao.getMeta() + questaoMeta.getMeta() );
                questao.setNegativo( questaoMeta.getNegativo() );
                qtde++;
            }
        }
        if ( notNull( questao.getMeta() ) ) {
            metaCargo = questao.getMeta() / qtde;
            questao.setMeta( null );
        }

        // MEDIA DAS METAS
        qtde = 0;
        Double mediaMeta = null;
        if ( metaGrupoOcupacional != null ) {
            if ( mediaMeta == null ) {
                mediaMeta = 0d;
            }
            mediaMeta += metaGrupoOcupacional;
            qtde++;
        }
        if ( metaSetor != null ) {
            if ( mediaMeta == null ) {
                mediaMeta = 0d;
            }
            mediaMeta += metaSetor;
            qtde++;
        }
        if ( metaCargo != null ) {
            if ( mediaMeta == null ) {
                mediaMeta = 0d;
            }
            mediaMeta += metaCargo;
            qtde++;
        }
        if ( notNull( mediaMeta ) && qtde > 0 ) {
            questao.setMeta( mediaMeta / qtde );
        }

    }


    private GrupoOcupacionaisCargosSetoresView getGruposOcupacionaisFuncionarios( List< Funcionario > avaliados ) {

        GrupoOcupacionaisCargosSetoresView grupoOcupacionaisCargosSetoresView = new GrupoOcupacionaisCargosSetoresView();
        if ( isNotEmpty( avaliados ) ) {
            for ( Funcionario avaliado : avaliados ) {
                avaliado.preencherGrupoOcupacional();
                grupoOcupacionaisCargosSetoresView.addCargo( avaliado.getCargo().formatToWebPage().format() );
                grupoOcupacionaisCargosSetoresView.addSetor( avaliado.getSetor().formatToWebPage().format() );
                grupoOcupacionaisCargosSetoresView.addGrupoOcupacional( avaliado.getGrupoOcupacional().formatToWebPage().format() );
            }
        }
        return grupoOcupacionaisCargosSetoresView;
    }


    private void calcularMediaDaQuestao( QuestaoView questao ) {

        // PROPRIA
        if ( isNotEmpty( questao.getRespostasProprias() ) ) {
            for ( Resposta resp : questao.getRespostasProprias() ) {

                Double valorNumerico = resp.getValorNumerico();
                String valorTexto = resp.getValorTexto();

                if ( valorNumerico != null ) {
                    if ( questao.getMediaPropria() == null ) {
                        questao.setMediaPropria( 0d );
                    }
                    questao.setMediaPropria( questao.getMediaPropria() + valorNumerico );
                    questao.qtdeMediaPropria++;
                }

                if ( valorTexto != null ) {
                    if ( questao.getRespostasTextosProprias() == null ) {
                        questao.setRespostasTextosProprias( new ArrayList< String >() );
                    }
                    questao.getRespostasTextosProprias().add( valorTexto );
                }
            }
        }

        // LIDER
        if ( isNotEmpty( questao.getRespostasLider() ) ) {
            for ( Resposta resp : questao.getRespostasLider() ) {

                Double valorNumerico = resp.getValorNumerico();
                String valorTexto = resp.getValorTexto();

                if ( valorNumerico != null ) {
                    if ( questao.getMediaLider() == null ) {
                        questao.setMediaLider( 0d );
                    }
                    questao.setMediaLider( questao.getMediaLider() + valorNumerico );
                    questao.qtdeMediaLider++;
                }

                if ( valorTexto != null ) {
                    if ( questao.getRespostasTextosLider() == null ) {
                        questao.setRespostasTextosLider( new ArrayList< String >() );
                    }
                    questao.getRespostasTextosLider().add( valorTexto );
                }
            }
        }

        // SUBORDINADOS
        if ( isNotEmpty( questao.getRespostasSubordinados() ) ) {
            for ( Resposta resp : questao.getRespostasSubordinados() ) {

                Double valorNumerico = resp.getValorNumerico();
                String valorTexto = resp.getValorTexto();

                if ( valorNumerico != null ) {
                    if ( questao.getMediaSubordinados() == null ) {
                        questao.setMediaSubordinados( 0d );
                    }
                    questao.setMediaSubordinados( questao.getMediaSubordinados() + valorNumerico );
                    questao.qtdeMediaSubordinados++;
                }

                if ( valorTexto != null ) {
                    if ( questao.getRespostasTextosSubordinados() == null ) {
                        questao.setRespostasTextosSubordinados( new ArrayList< String >() );
                    }
                    questao.getRespostasTextosSubordinados().add( valorTexto );
                }
            }
        }

        // OUTROS
        if ( isNotEmpty( questao.getRespostasOutros() ) ) {
            for ( Resposta resp : questao.getRespostasOutros() ) {

                Double valorNumerico = resp.getValorNumerico();
                String valorTexto = resp.getValorTexto();

                if ( valorNumerico != null ) {
                    if ( questao.getMediaOutros() == null ) {
                        questao.setMediaOutros( 0d );
                    }
                    questao.setMediaOutros( questao.getMediaOutros() + valorNumerico );
                    questao.qtdeMediaOutros++;
                }

                if ( valorTexto != null ) {
                    if ( questao.getRespostasTextosOutros() == null ) {
                        questao.setRespostasTextosOutros( new ArrayList< String >() );
                    }
                    questao.getRespostasTextosOutros().add( valorTexto );
                }
            }
        }

        if ( notNull( questao.getMediaPropria() ) ) {
            questao.setMediaPropria( questao.getMediaPropria() / questao.qtdeMediaPropria );
        }

        if ( notNull( questao.getMediaLider() ) ) {
            questao.setMediaLider( questao.getMediaLider() / questao.qtdeMediaLider );
        }

        if ( notNull( questao.getMediaSubordinados() ) ) {
            questao.setMediaSubordinados( questao.getMediaSubordinados() / questao.qtdeMediaSubordinados );
        }

        if ( notNull( questao.getMediaOutros() ) ) {
            questao.setMediaOutros( questao.getMediaOutros() / questao.qtdeMediaOutros );
        }

    }


    private List< Resposta > getRespostas( List< Resposta > respostas, GrupoQuestaoView grupoQuestao, QuestaoView questao ) {

        List< Resposta > respostasDaQuestao = new ArrayList<>();
        if ( isNotEmpty( respostas ) ) {
            for ( Resposta resposta : respostas ) {
                if ( sameGrupo( resposta, grupoQuestao ) && sameQuestao( resposta, questao ) ) {
                    respostasDaQuestao.add( resposta );
                }
            }
        }
        return respostasDaQuestao;
    }


    private boolean sameQuestao( Resposta resposta, QuestaoView questao ) {

        return resposta.getIdQuestao().equals( questao.getId() );
    }


    private boolean sameGrupo( Resposta resposta, GrupoQuestaoView grupoQuestao ) {

        if ( resposta.getIdGrupoQuestao() != null ) {
            return resposta.getIdGrupoQuestao().equals( grupoQuestao.getId() );
        } else {
            return true;
        }
    }


    private void buscarRespostasDoAvaliadoColocarNoQuestionario( Long idPesquisa, List< Funcionario > avaliados, List< QuestionarioView > questionariosResult )
        throws ServiceException {

        boolean hasResult = false;

        for ( Funcionario avaliado : avaliados ) {

            List< QuestionarioAvaliacao > avaliacoes = avaliacaoService.findByPesquisaAndAvaliado( idPesquisa, avaliado.getId() );
            if ( isNotEmpty( avaliacoes ) ) {
                for ( QuestionarioAvaliacao avaliacao : avaliacoes ) {

                    Funcionario avaliador = avaliacao.getFuncionarioAvaliador();

                    if ( isNotEmpty( avaliacao.getRespostas() ) ) {
                        hasResult = true;
                    }

                    // AVALIACAO PROPRIA
                    if ( avaliador.getId().equals( avaliado.getId() ) ) {
                        QuestionarioView questionario = putQuestionarioDaAvaliacaoNoResult( questionariosResult, avaliacao );
                        questionario.getRespostasProprias().addAll( avaliacao.getRespostas() );
                    }
                    // AVALIACAO LIDER
                    else if ( temLider( avaliado ) && avaliador.getId().equals( avaliado.getIdLider() ) ) {
                        QuestionarioView questionario = putQuestionarioDaAvaliacaoNoResult( questionariosResult, avaliacao );
                        questionario.getRespostasLider().addAll( avaliacao.getRespostas() );
                    }
                    // AVALIACAO SUBORDINADOS
                    else if ( temLider( avaliador ) && avaliador.getIdLider().equals( avaliado.getId() ) ) {
                        QuestionarioView questionario = putQuestionarioDaAvaliacaoNoResult( questionariosResult, avaliacao );
                        questionario.getRespostasSubordinados().addAll( avaliacao.getRespostas() );
                    }
                    // AVALIACAO OUTROS
                    else {
                        QuestionarioView questionario = putQuestionarioDaAvaliacaoNoResult( questionariosResult, avaliacao );
                        questionario.getRespostasOutros().addAll( avaliacao.getRespostas() );
                    }

                }

            }

        }

        if ( !hasResult ) {
            throw new ServiceException( "Resultado não encontrado" );
        }

    }


    private boolean temLider( Funcionario funcionario ) {

        if ( funcionario != null ) {
            return funcionario.getIdLider() != null;
        }
        return false;
    }


    private QuestionarioView putQuestionarioDaAvaliacaoNoResult( List< QuestionarioView > questionariosResult, QuestionarioAvaliacao avaliacao ) {

        for ( QuestionarioView quest : questionariosResult ) {
            if ( quest.getId().equals( avaliacao.getIdQuestionario() ) ) {
                return quest;
            }
        }

        QuestionarioView questionario = avaliacao.getQuestionario().formatToWebPage().format();
        questionariosResult.add( questionario );
        return questionario;

    }


    private Pesquisa validarPesquisa( Long idPesquisa, String userName )
        throws ServiceException {

        Pesquisa pesquisa = pesquisaService.findById( idPesquisa, Collections.singletonList( "questionarios" ) );

        if ( pesquisa == null ) {
            throw new ServiceException( "Pesquisa não encontrada" );
        }

        User user = userService.findByName( userName );
        if ( !userService.isAdmin( user ) && !userService.isGestor( user ) ) {
            if ( DateUtil.isAfterToday( pesquisa.getDtaEntrega() ) ) {
                throw new ServiceException( "Pesquisa ainda não foi finalizada" );
            }
        }

        return pesquisa;
    }


    private void ajustarNotasRegrasPesquisa( PesquisaResultView pesquisa, QuestaoView questao, Double maiorNotaGrupo ) {

        if ( !questaoService.temClassificacao( questao ) ) {
            return;
        }

        Double somaNotasValorObtido = 0D;
        int totalAvaliadoresValorObtido = 0;

        Double notaPropria = questao.getMediaPropria();
        Double notaLideres = questao.getMediaLider();
        Double notaSubordinados = questao.getMediaSubordinados();
        Double notaOutros = questao.getMediaOutros();

        Double metaQuestao = questao.getMeta();

        if ( notNull( notaPropria ) ) {
            if ( metaQuestao != null && pesquisa.getEntity().getCalculoPorMeta() ) {
                notaPropria = ( metaQuestao / maiorNotaGrupo ) * notaPropria;
            }
            if ( pesquisa.getEntity().getCalculoValorObtidoPropria() ) {
                somaNotasValorObtido += notaPropria;
                totalAvaliadoresValorObtido++;
            }
        }

        if ( notNull( notaLideres ) ) {
            if ( metaQuestao != null && pesquisa.getEntity().getCalculoPorMeta() ) {
                notaLideres = ( metaQuestao / maiorNotaGrupo ) * notaLideres;
            }
            if ( pesquisa.getEntity().getCalculoValorObtidoSuperior() ) {
                somaNotasValorObtido += notaLideres;
                totalAvaliadoresValorObtido++;
            }
        }

        if ( notNull( notaSubordinados ) ) {
            if ( metaQuestao != null && pesquisa.getEntity().getCalculoPorMeta() ) {
                notaSubordinados = ( metaQuestao / maiorNotaGrupo ) * notaSubordinados;
            }
            if ( pesquisa.getEntity().getCalculoValorObtidoSubordinados() ) {
                somaNotasValorObtido += notaSubordinados;
                totalAvaliadoresValorObtido++;
            }
        }

        if ( notNull( notaOutros ) ) {
            if ( metaQuestao != null && pesquisa.getEntity().getCalculoPorMeta() ) {
                notaOutros = ( metaQuestao / maiorNotaGrupo ) * notaOutros;
            }
            if ( pesquisa.getEntity().getCalculoValorObtidoOutros() ) {
                somaNotasValorObtido += notaOutros;
                totalAvaliadoresValorObtido++;
            }
        }

        questao.setMediaPropria( notaPropria );
        questao.setMediaLider( notaLideres );
        questao.setMediaSubordinados( notaSubordinados );
        questao.setMediaOutros( notaOutros );

        if ( totalAvaliadoresValorObtido > 0 ) {
            questao.setValorObtido( somaNotasValorObtido / totalAvaliadoresValorObtido );
        }

        if ( notNull( metaQuestao ) && metaQuestao > 0d && notNull( questao.getValorObtido() ) && questao.getValorObtido() > 0d ) {
            if ( questao.getNegativo() ) {
                questao.setPercentualMeta( metaQuestao / questao.getValorObtido() );
            } else {
                questao.setPercentualMeta( questao.getValorObtido() / metaQuestao );
            }
        }

    }


    private Double getMaiorNotaGrupo( GrupoQuestaoView grupoQuestao ) {

        Double maiorNota = 0D;

        if ( isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {
            for ( QuestaoOrdemView qo : grupoQuestao.getQuestoesOrdem() ) {

                QuestaoView questao = qo.getQuestao();

                if ( !questaoService.temClassificacao( questao ) ) {
                    continue;
                }

                if ( questao.getListaValores() != null && isNotEmpty( questao.getListaValores().getValoresLista() ) ) {
                    for ( ValorListaView valorLista : questao.getListaValores().getValoresLista() ) {

                        if ( notNullNumber( valorLista.getValor() ) ) {
                            Double valor = 0d;

                            try {

                                valor = getDouble( valorLista.getValor() );

                            } catch ( Exception e ) {
                                // do nothing
                            }

                            if ( valor > maiorNota ) {
                                maiorNota = valor;
                            }

                        }

                    }
                }

            }

        }

        return maiorNota;
    }

}
