package com.illuminare.service.impl;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.FuncionarioDAO;
import com.illuminare.dao.GrupoQuestaoDAO;
import com.illuminare.entity.Cargo;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoClassificacao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Resposta;
import com.illuminare.entity.Setor;
import com.illuminare.entity.User;
import com.illuminare.exceptions.QuestionarioSemRespostasException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.resultado.Nota;
import com.illuminare.resultado.QuestaoRespostas;
import com.illuminare.resultado.QuestaoRespostasAgrupadasPorAvaliadores;
import com.illuminare.resultado.RespostasAgrupadasPorAvaliadores;
import com.illuminare.resultado.Resultado;
import com.illuminare.service.AvaliacaoService;
import com.illuminare.service.CargoService;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.GrupoOcupacionalService;
import com.illuminare.service.PesquisaService;
import com.illuminare.service.QuestaoMetaService;
import com.illuminare.service.QuestaoService;
import com.illuminare.service.QuestionarioService;
import com.illuminare.service.RespostaService;
import com.illuminare.service.ResultadoService;
import com.illuminare.service.SetorService;
import com.illuminare.service.UserService;
import com.illuminare.util.DateUtil;
import com.illuminare.view.GrupoQuestaoView;
import com.illuminare.view.PesquisaResultView;
import com.illuminare.view.QuestaoOrdemView;
import com.illuminare.view.QuestaoView;
import com.illuminare.view.QuestionarioView;
import com.illuminare.view.ValorListaView;


@Service
public class ResultadoServiceImpl implements ResultadoService {

    Logger log = Logger.getLogger( ResultadoServiceImpl.class );

    @Autowired
    AvaliacaoService avaliacaoService;

    @Autowired
    RespostaService respostaService;

    @Autowired
    PesquisaService pesquisaService;

    @Autowired
    FuncionarioService funcionarioService;

    @Autowired
    GrupoQuestaoDAO grupoQuestaoDAO;

    @Autowired
    GrupoOcupacionalService grupoOcupacionalService;

    @Autowired
    CargoService cargoService;

    @Autowired
    QuestionarioService questionarioService;

    @Autowired
    SetorService setorService;

    @Autowired
    UserService userService;

    @Autowired
    QuestaoMetaService questaoMetaService;

    @Autowired
    QuestaoService questaoService;

    @Autowired
    EmpresaService empresaService;

    @Autowired
    FuncionarioDAO funcionarioDAO;


    @Override
    @Transactional( readOnly = true )
    public Resultado resultadoPorFuncionarioAvaliado( Long idPesquisa, Long idAvaliado, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );

        Funcionario avaliado = funcionarioService.findById( idAvaliado, Arrays.asList( "cargo c", "[c].grupoOcupacional" ) );
        if ( avaliado == null ) {
            throw new ServiceException( "Funcionário não encontrado" );
        }
        avaliado.preencherGrupoOcupacional();

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = findRespostas( pesquisa, avaliado );

        Resultado resultado = gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, avaliado );

        return resultado;
    }


    @Override
    @Transactional( readOnly = true )
    public Resultado resultadoPorGrupoOcupacional( Long idPesquisa, Long idGrupoOcupacional, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );

        GrupoOcupacional grupoOcupacional = grupoOcupacionalService.findById( idGrupoOcupacional );
        if ( grupoOcupacional == null ) {
            throw new ServiceException( "Grupo Ocupacional não encontrado" );
        }

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = findRespostas( pesquisa, grupoOcupacional );

        Resultado resultado = gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, grupoOcupacional );

        return resultado;
    }


    @Override
    @Transactional( readOnly = true )
    public Resultado resultadoPorCargo( Long idPesquisa, Long idCargo, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );

        Cargo cargo = cargoService.findById( idCargo );
        if ( cargo == null ) {
            throw new ServiceException( "Cargo não encontrado" );
        }

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = findRespostas( pesquisa, cargo );

        Resultado resultado = gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, cargo );

        return resultado;
    }


    @Override
    @Transactional( readOnly = true )
    public Resultado resultadoPorSetor( Long idPesquisa, Long idSetor, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );

        Setor setor = setorService.findById( idSetor );
        if ( setor == null ) {
            throw new ServiceException( "Setor não encontrado" );
        }

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = findRespostas( pesquisa, setor );

        Resultado resultado = gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, setor );

        return resultado;
    }


    @Override
    @Transactional( readOnly = true )
    public Resultado resultadoPorEmpresa( Long idPesquisa, Long idEmpresa, String user )
        throws ServiceException {

        Pesquisa pesquisa = validarPesquisa( idPesquisa, user );

        Empresa empresa = empresaService.findById( idEmpresa );
        if ( empresa == null ) {
            throw new ServiceException( "Empresa não encontrado" );
        }

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = findRespostas( pesquisa, empresa );

        Resultado resultado = gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, empresa );

        return resultado;
    }


    private Pesquisa validarPesquisa( Long idPesquisa, String userName )
        throws ServiceException {

        Pesquisa pesquisa = pesquisaService.findById( idPesquisa, Arrays.asList( "questionarios" ) );

        if ( pesquisa == null ) {
            throw new ServiceException( "Pesquisa não encontrada" );
        }

        User user = userService.findByName( userName );
        if ( !userService.isAdmin( user ) && !userService.isGestor( user ) ) {
            if ( DateUtil.isAfterToday( pesquisa.getDtaEntrega() ) ) {
                throw new ServiceException( "Pesquisa ainda não foi finalizada" );
            }
        }

        return pesquisa;
    }


    private Setor copySetor( Setor obj )
        throws ServiceException {

        if ( obj != null ) {
            Setor newObj = new Setor();
            try {
                org.apache.commons.beanutils.BeanUtils.copyProperties( newObj, obj );
            } catch (
                IllegalAccessException
                | InvocationTargetException e ) {
                throw new ServiceException( "Erro ao copiar objeto. resultadoServiceImpl.copySetor() " + e.getMessage() );
            }
            return newObj;
        }
        return null;
    }


    private GrupoOcupacional copyGrupoOcupacional( GrupoOcupacional obj )
        throws ServiceException {

        if ( obj != null ) {
            GrupoOcupacional newObj = new GrupoOcupacional();
            try {
                org.apache.commons.beanutils.BeanUtils.copyProperties( newObj, obj );
            } catch (
                IllegalAccessException
                | InvocationTargetException e ) {
                throw new ServiceException( "Erro ao copiar objeto. resultadoServiceImpl.copyGrupoOcupacional() " + e.getMessage() );
            }
            return newObj;
        }
        return null;
    }


    private Cargo copyCargo( Cargo obj )
        throws ServiceException {

        if ( obj != null ) {
            Cargo newObj = new Cargo();
            try {
                org.apache.commons.beanutils.BeanUtils.copyProperties( newObj, obj );
            } catch (
                IllegalAccessException
                | InvocationTargetException e ) {
                throw new ServiceException( "Erro ao copiar objeto. resultadoServiceImpl.copyCargo() " + e.getMessage() );
            }
            return newObj;
        }
        return null;
    }


    private Resultado gerarResultado( Pesquisa pesquisa, RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores, Funcionario avaliado )
        throws ServiceException {

        return gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, avaliado, null, null, null, null );
    }


    private Resultado gerarResultado( Pesquisa pesquisa, RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores, GrupoOcupacional grupoOcupacional )
        throws ServiceException {

        return gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, null, grupoOcupacional, null, null, null );
    }


    private Resultado gerarResultado( Pesquisa pesquisa, RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores, Cargo cargo )
        throws ServiceException {

        return gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, null, null, cargo, null, null );
    }


    private Resultado gerarResultado( Pesquisa pesquisa, RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores, Setor setor )
        throws ServiceException {

        return gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, null, null, null, setor, null );
    }


    private Resultado gerarResultado( Pesquisa pesquisa, RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores, Empresa empresa )
        throws ServiceException {

        return gerarResultado( pesquisa, respostasAgrupadasPorAvaliadores, null, null, null, null, empresa );
    }


    private Resultado gerarResultado(
        Pesquisa pesquisa,
        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores,
        Funcionario avaliado,
        GrupoOcupacional grupoOcupacional,
        Cargo cargo,
        Setor setor,
        Empresa empresa )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        boolean semRespostas = true;
        if ( respostasAgrupadasPorAvaliadores != null ) {
            semRespostas = CollectionUtils.isEmpty( respostasAgrupadasPorAvaliadores.getRespostasPropria() )
                && CollectionUtils.isEmpty( respostasAgrupadasPorAvaliadores.getRespostasLideres() )
                && CollectionUtils.isEmpty( respostasAgrupadasPorAvaliadores.getRespostasSubordinados() )
                && CollectionUtils.isEmpty( respostasAgrupadasPorAvaliadores.getRespostasOutros() );
        }

        if ( semRespostas ) {
            throw new ServiceException( "Avaliação não respondida" );
        }

        Resultado resultado = new Resultado();

        QuestaoRespostasAgrupadasPorAvaliadores questaoRespostasAgrupadasPorAvaliadores =
            gerarQuestaoRespostasAgrupadas( pesquisa, respostasAgrupadasPorAvaliadores );

        resultado.setPesquisa( new PesquisaResultView( pesquisa.getId(), pesquisa.getNome() ) );
        if ( avaliado != null ) {
            resultado.setAvaliado( avaliado.formatToWebPage().format() );
        }
        if ( grupoOcupacional != null ) {
            resultado.setGrupoOcupacional( grupoOcupacional.formatToWebPage().format() );
        }
        if ( cargo != null ) {
            resultado.setCargo( cargo.formatToWebPage().format() );
        }
        if ( setor != null ) {
            resultado.setSetor( setor.formatToWebPage().format() );
        }
        resultado.setEmpresa( empresa );

        // String[] pesquisaFetch = { "questionarios questionario",
        // "[questionario].gruposQuestoes grupoQuestao",
        // "[questionario].gruposOcupacionais" };
        // pesquisa = pesquisaService.findById(pesquisa.getId(),
        // Arrays.asList(pesquisaFetch));

        List< QuestionarioView > questionariosResultado = new ArrayList<>();

        for ( Questionario questionario : pesquisa.getQuestionarios() ) {

            questionario = questionarioService.findById( questionario.getId(), Arrays.asList( "gruposQuestoes g", "[g].questoesOrdem qo", "classificacao" ) );

            boolean temRespostas;

            removeReferencesQuestao( questionario );

            // RESPOSTAS PROPRIA
            temRespostas = setarRespostasNasQuestoesDoQuestionario( questionario, questaoRespostasAgrupadasPorAvaliadores );

            if ( temRespostas ) {
                questionariosResultado.add( questionario.formatToWebPage().format() );
            }
        }

        // questionariosResultado = gerarMediaTotal(questionariosResultado);
        gerarMediaTotal2( pesquisa, questionariosResultado );

        // resultado.setQuestionarios(questionariosResultado);
        resultado.setQuestionarios( questionariosResultado );

        return resultado;

    }


    private RespostasAgrupadasPorAvaliadores findRespostas( Pesquisa pesquisa, Funcionario avaliado )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( avaliado );
        Assert.notNull( avaliado.getId() );

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = new RespostasAgrupadasPorAvaliadores();

        List< Resposta > respostas = respostaService.findByAvaliado( avaliado, pesquisa );

        respostasAgrupadasPorAvaliadores = agruparRespostasPorGruposDeAvaliadores( avaliado, respostas );

        return respostasAgrupadasPorAvaliadores;
    }


    private RespostasAgrupadasPorAvaliadores findRespostas( Pesquisa pesquisa, GrupoOcupacional grupoOcupacional )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( grupoOcupacional );
        Assert.notNull( grupoOcupacional.getId() );

        List< Resposta > respostas = respostaService.findByGrupoOcupacional( pesquisa, grupoOcupacional );

        return agruparRespotasPorAvaliado( respostas );

    }


    private RespostasAgrupadasPorAvaliadores findRespostas( Pesquisa pesquisa, Cargo cargo )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( cargo );
        Assert.notNull( cargo.getId() );

        List< Resposta > respostas = respostaService.findByCargo( pesquisa, cargo );

        return agruparRespotasPorAvaliado( respostas );

    }


    private RespostasAgrupadasPorAvaliadores findRespostas( Pesquisa pesquisa, Setor setor )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( setor );
        Assert.notNull( setor.getId() );

        List< Resposta > respostas = respostaService.findBySetor( pesquisa, setor );

        return agruparRespotasPorAvaliado( respostas );

    }


    private RespostasAgrupadasPorAvaliadores findRespostas( Pesquisa pesquisa, Empresa empresa )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );
        Assert.notNull( empresa );
        Assert.notNull( empresa.getId() );

        List< Resposta > respostas = respostaService.findByEmpresa( pesquisa, empresa );

        return agruparRespotasPorAvaliado( respostas );

    }


    private RespostasAgrupadasPorAvaliadores agruparRespotasPorAvaliado( List< Resposta > respostas )
        throws ServiceException {

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = new RespostasAgrupadasPorAvaliadores();

        Map< Funcionario, List< Resposta > > respostasPorAvaliados = new HashMap< Funcionario, List< Resposta > >();

        // separando todos os avaliados
        for ( Resposta resp : respostas ) {
            Funcionario avaliado = resp.getQuestionarioAvaliacao().getFuncionarioAvaliado();
            if ( !respostasPorAvaliados.containsKey( avaliado ) ) {
                // respostasPorAvaliados.put(avaliado, new
                // ArrayList<Resposta>());
                respostasPorAvaliados.put( avaliado, new ArrayList< Resposta >() );
            }
        }
        // agrupando as respotas dos avaliados
        for ( Resposta resp : respostas ) {
            Funcionario avaliado = resp.getQuestionarioAvaliacao().getFuncionarioAvaliado();
            respostasPorAvaliados.get( avaliado ).add( resp );
        }

        // agrupando respotas
        for ( Map.Entry< Funcionario, List< Resposta > > respEntry : respostasPorAvaliados.entrySet() ) {
            Funcionario avaliado = respEntry.getKey();
            List< Resposta > respostasAvaliado = respEntry.getValue();
            RespostasAgrupadasPorAvaliadores agrupadas = agruparRespostasPorGruposDeAvaliadores( avaliado, respostasAvaliado );
            respostasAgrupadasPorAvaliadores.getRespostasLideres().addAll( agrupadas.getRespostasLideres() );
            respostasAgrupadasPorAvaliadores.getRespostasOutros().addAll( agrupadas.getRespostasOutros() );
            respostasAgrupadasPorAvaliadores.getRespostasPropria().addAll( agrupadas.getRespostasPropria() );
            respostasAgrupadasPorAvaliadores.getRespostasSubordinados().addAll( agrupadas.getRespostasSubordinados() );
        }

        return respostasAgrupadasPorAvaliadores;
    }


    private RespostasAgrupadasPorAvaliadores agruparRespostasPorGruposDeAvaliadores( Funcionario avaliado, List< Resposta > respostas )
        throws ServiceException {

        Assert.notNull( avaliado );
        Assert.notNull( avaliado.getId() );

        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores = new RespostasAgrupadasPorAvaliadores();

        List< Resposta > respostasPropria = new ArrayList<>();
        List< Resposta > respostasLideres = new ArrayList<>();
        List< Resposta > respostasSubordinados = new ArrayList<>();
        List< Resposta > respostasOutros = new ArrayList<>();

        avaliado = funcionarioService.findById( avaliado.getId(), Arrays.asList( "lider" ) );

        // respostasAgrupadasPorAvaliadores.setAvaliado(avaliado);

        if ( respostas != null ) {

            for ( Resposta resp : respostas ) {

                Funcionario avaliadorResposta = resp.getQuestionarioAvaliacao().getFuncionarioAvaliador();

                // SELF
                if ( avaliadorResposta.getId().equals( avaliado.getId() ) ) {

                    // tratar *problema respostas proprias duplicadas (aconteceu
                    // devido um cache no browser que mandou o json
                    // incorretamente
                    boolean hasAdd = true;
                    for ( Resposta r : respostasPropria ) {

                        boolean sameQuestao = r.getQuestao().getId().equals( resp.getQuestao().getId() );
                        boolean sameQuestionario = r.getQuestionarioAvaliacao().getId().equals( resp.getQuestionarioAvaliacao().getId() );
                        boolean sameGrupo = true;
                        if ( r.getIdGrupoQuestao() != null ) {
                            sameGrupo = r.getIdGrupoQuestao().equals( resp.getIdGrupoQuestao() );
                        }

                        if ( sameQuestao && sameQuestionario && sameGrupo ) {
                            hasAdd = false;
                            // pegar a ultima
                            if ( resp.getDataCadastro() != null && DateUtil.isAfter( resp.getDataCadastro(), r.getDataCadastro() ) ) {
                                r.setRespostaTexto( resp.getRespostaTexto() );
                                r.setRespostaValorLista( resp.getRespostaValorLista() );
                                r.setRespostaNumerico( resp.getRespostaNumerico() );
                            }
                        }

                    }
                    if ( hasAdd ) {
                        respostasPropria.add( resp );
                    }

                } else {

                    // LIDER
                    if ( avaliado.getLider() != null && avaliado.getLider().getId() != null //
                        && avaliado.getLider().getId().equals( avaliadorResposta.getId() ) ) {

                        respostasLideres.add( resp );

                    } else {
                        // SUBORDINADOS
                        avaliadorResposta = funcionarioService.findById( avaliadorResposta.getId(), Arrays.asList( "lider" ) );
                        boolean isSubordinado = false;
                        if ( avaliadorResposta.getLider() != null && avaliadorResposta.getLider().getId() != null ) {

                            if ( avaliado.getId().equals( avaliadorResposta.getLider().getId() ) ) {

                                isSubordinado = true;
                                respostasSubordinados.add( resp );

                            }

                        }

                        // OUTROS
                        if ( !isSubordinado ) {
                            respostasOutros.add( resp );
                        }

                    }

                }
            } // for
        }

        respostasAgrupadasPorAvaliadores.setRespostasPropria( respostasPropria );
        respostasAgrupadasPorAvaliadores.setRespostasLideres( respostasLideres );
        respostasAgrupadasPorAvaliadores.setRespostasSubordinados( respostasSubordinados );
        respostasAgrupadasPorAvaliadores.setRespostasOutros( respostasOutros );

        return respostasAgrupadasPorAvaliadores;
    }


    private void gerarMediaTotal( Pesquisa pesquisa, List< QuestionarioView > questionariosResultado ) {

        ajustarNotas( pesquisa, questionariosResultado );

        for ( QuestionarioView questionario : questionariosResultado ) {

            for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {

                int totalQuestoesMeta = 0;
                Double totalPercentualMetaQuestoes = 0D;
                boolean temPropria = false;
                boolean temLider = false;
                boolean temSubordinados = false;
                boolean temOutras = false;

                // SOMANDO AS MEDIAS DAS NOTAS
                // LISTA QUESTOES
                for ( QuestaoOrdemView questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                    if ( !questaoService.temClassificacao( questaoOrdem.getQuestao() ) ) {
                        continue;
                    }

                    boolean somarQuestoesMeta = false;

                    // SOMANDO MEDIA PROPRIA
                    if ( questaoOrdem.getQuestao().getNotaPropria() != null ) {

                        if ( notNull( questaoOrdem.getQuestao().getNotaPropria().getMedia() ) ) {
                            // if ( questaoService.temClassificacao( questaoOrdem.getQuestao() ) ) {
                            if ( grupoQuestao.getMediaPropria() == null ) {
                                grupoQuestao.setMediaPropria( 0D );
                            }
                            Double mediaQuestao = questaoOrdem.getQuestao().getNotaPropria().getMedia();
                            grupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() + mediaQuestao );
                            // somarQuestoesMeta = true;
                            // totalAvaliadores++;
                            temPropria = true;
                            // }
                        }
                        if ( "0".equals( questaoOrdem.getQuestao().getNotaPropria().getMediaExibicao() ) ) {
                            temPropria = true;
                        }
                    }

                    // SOMANDO MEDIA LIDERES
                    if ( questaoOrdem.getQuestao().getNotaLideres() != null ) {
                        if ( notNull( questaoOrdem.getQuestao().getNotaLideres().getMedia() ) ) {
                            // if ( questaoService.temClassificacao( questaoOrdem.getQuestao() ) ) {
                            if ( grupoQuestao.getMediaLideres() == null ) {
                                grupoQuestao.setMediaLideres( 0D );
                            }
                            Double mediaQuestao = questaoOrdem.getQuestao().getNotaLideres().getMedia();
                            grupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() + mediaQuestao );
                            // somarQuestoesMeta = true;
                            // totalAvaliadores++;
                            temLider = true;
                            // }
                        }
                        if ( "0".equals( questaoOrdem.getQuestao().getNotaLideres().getMediaExibicao() ) ) {
                            temLider = true;
                        }
                    }

                    // SOMANDO MEDIA SUBORDINADOS
                    if ( questaoOrdem.getQuestao().getNotaSubordinados() != null ) {
                        if ( questaoOrdem.getQuestao().getNotaSubordinados().getMedia() != null ) {
                            // if ( questaoService.temClassificacao( questaoOrdem.getQuestao() ) ) {
                            if ( grupoQuestao.getMediaSubordinados() == null ) {
                                grupoQuestao.setMediaSubordinados( 0D );
                            }
                            Double mediaQuestao = questaoOrdem.getQuestao().getNotaSubordinados().getMedia();
                            grupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() + mediaQuestao );
                            // somarQuestoesMeta = true;
                            // totalAvaliadores++;
                            temSubordinados = true;
                            // }
                        }
                        if ( "0".equals( questaoOrdem.getQuestao().getNotaSubordinados().getMediaExibicao() ) ) {
                            temSubordinados = true;
                        }
                    }

                    // SOMANDO MEDIA OUTROS
                    if ( questaoOrdem.getQuestao().getNotaOutros() != null ) {
                        if ( questaoOrdem.getQuestao().getNotaOutros().getMedia() != null ) {
                            // if ( questaoService.temClassificacao( questaoOrdem.getQuestao() ) ) {
                            if ( grupoQuestao.getMediaOutros() == null ) {
                                grupoQuestao.setMediaOutros( 0D );
                            }
                            Double mediaQuestao = questaoOrdem.getQuestao().getNotaOutros().getMedia();
                            grupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() + mediaQuestao );
                            // somarQuestoesMeta = true;
                            // totalAvaliadores++;
                            temOutras = true;
                            // }
                        }
                        if ( "0".equals( questaoOrdem.getQuestao().getNotaOutros().getMediaExibicao() ) ) {
                            temOutras = true;
                        }
                    }

                    // SOMANDO META GRUPO
                    Double metaQuestao = questaoOrdem.getQuestao().getMeta();
                    if ( metaQuestao != null && !metaQuestao.equals( 0D ) ) {
                        // if ( questaoService.temClassificacao( questaoOrdem.getQuestao() ) ) {
                        if ( grupoQuestao.getMeta() == null ) {
                            grupoQuestao.setMeta( 0D );
                        }
                        grupoQuestao.setMeta( grupoQuestao.getMeta() + metaQuestao );
                        somarQuestoesMeta = true;
                        // }
                    }

                    if ( somarQuestoesMeta ) {

                        totalQuestoesMeta++;

                    }

                    if ( notNull( questaoOrdem.getQuestao().getPercentualMeta() ) ) {
                        totalPercentualMetaQuestoes += questaoOrdem.getQuestao().getPercentualMeta();
                    }

                } // for questoes

                // META DO GRUPO
                if ( notNull( grupoQuestao.getMeta() ) ) {
                    grupoQuestao.setMeta( grupoQuestao.getMeta() / totalQuestoesMeta );
                }

                // VALOR OBTIDO GRUPO
                /*
                 * Double totalMediasGrupo = 0D; if ( grupoQuestao.getMediaPropria() ) ) {
                 * totalMediasGrupo += grupoQuestao.getMediaPropria(); } if (
                 * grupoQuestao.getMediaLideres() ) ) { totalMediasGrupo +=
                 * grupoQuestao.getMediaLideres(); } if ( grupoQuestao.getMediaSubordinados() !=
                 * null ) { totalMediasGrupo += grupoQuestao.getMediaSubordinados(); } if (
                 * grupoQuestao.getMediaOutros() ) ) { totalMediasGrupo +=
                 * grupoQuestao.getMediaOutros(); }
                 */

                // PERCENTUAL DA META DO GRUPO
                if ( totalQuestoesMeta > 0 ) {
                    grupoQuestao.setPercentualMeta( totalPercentualMetaQuestoes / totalQuestoesMeta );
                }

                // CALCULANDO MEDIA DO GRUPO
                // int qtdeQuestoes = grupoQuestao.getQuestoesOrdem().size();
                boolean isPropria = false, isLideres = false, isSubordinados = false, isOutros = false;

                Double totalMediasGrupo = 0D;
                Double totalMediasGrupoValorObtido = 0D;

                // PROPRIA
                if ( notNull( grupoQuestao.getMediaPropria() ) ) {
                    isPropria = true;
                    int qtdeQuestoes = getTotalQuestoesTipoEscalaOuNumericoQuePossuiNota(
                        grupoQuestao.getQuestoesOrdem(),
                        isPropria,
                        isLideres,
                        isSubordinados,
                        isOutros,
                        false );
                    grupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() / qtdeQuestoes );
                    totalMediasGrupo += grupoQuestao.getMediaPropria();
                    if ( pesquisa.getCalculoValorObtidoPropria() ) {
                        totalMediasGrupoValorObtido += grupoQuestao.getMediaPropria();
                    }
                }
                // LIDERES
                if ( notNull( grupoQuestao.getMediaLideres() ) ) {
                    isPropria = false;
                    isLideres = true;
                    int qtdeQuestoes = getTotalQuestoesTipoEscalaOuNumericoQuePossuiNota(
                        grupoQuestao.getQuestoesOrdem(),
                        isPropria,
                        isLideres,
                        isSubordinados,
                        isOutros,
                        false );
                    grupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() / qtdeQuestoes );
                    totalMediasGrupo += grupoQuestao.getMediaLideres();
                    if ( pesquisa.getCalculoValorObtidoSuperior() ) {
                        totalMediasGrupoValorObtido += grupoQuestao.getMediaLideres();
                    }
                }
                // SUBORDINADOS
                if ( notNull( grupoQuestao.getMediaSubordinados() ) ) {
                    isPropria = false;
                    isLideres = false;
                    isSubordinados = true;
                    int qtdeQuestoes = getTotalQuestoesTipoEscalaOuNumericoQuePossuiNota(
                        grupoQuestao.getQuestoesOrdem(),
                        isPropria,
                        isLideres,
                        isSubordinados,
                        isOutros,
                        false );
                    grupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() / qtdeQuestoes );
                    totalMediasGrupo += grupoQuestao.getMediaSubordinados();
                    if ( pesquisa.getCalculoValorObtidoSubordinados() ) {
                        totalMediasGrupoValorObtido += grupoQuestao.getMediaSubordinados();
                    }
                }
                // OUTROS
                if ( notNull( grupoQuestao.getMediaOutros() ) ) {
                    isPropria = false;
                    isLideres = false;
                    isSubordinados = false;
                    isOutros = true;
                    int qtdeQuestoes = getTotalQuestoesTipoEscalaOuNumericoQuePossuiNota(
                        grupoQuestao.getQuestoesOrdem(),
                        isPropria,
                        isLideres,
                        isSubordinados,
                        isOutros,
                        false );
                    grupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() / qtdeQuestoes );
                    totalMediasGrupo += grupoQuestao.getMediaOutros();
                    if ( pesquisa.getCalculoValorObtidoOutros() ) {
                        totalMediasGrupoValorObtido += grupoQuestao.getMediaOutros();
                    }
                }

                // VALOR OBTIDO
                int totalAvaliadores = 0;
                int totalAvaliadoresValorObtido = 0;
                if ( temPropria ) {
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoPropria() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }
                if ( temLider ) {
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoSuperior() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }
                if ( temSubordinados ) {
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoSubordinados() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }
                if ( temOutras ) {
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoOutros() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }

                grupoQuestao.setValorObtido( totalMediasGrupoValorObtido / totalAvaliadoresValorObtido );

            } // for grupo

            // CALCULANDO MEDIA TOTAL DO QUESTIONARIO

            int totalGrupos = 0;
            Double totalPercentualMetaGrupos = 0D;
            int totalAvaliadores = 0;
            int totalAvaliadoresValorObtido = 0;

            // SOMANDO AS MEDIAS DO GRUPO
            for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {

                boolean somarGrupo = false;

                // PROPRIA
                if ( notNull( grupoQuestao.getMediaPropria() ) ) {
                    if ( questionario.getMediaPropria() == null ) {
                        questionario.setMediaPropria( 0D );
                    }
                    Double mediaGrupo = grupoQuestao.getMediaPropria();
                    questionario.setMediaPropria( questionario.getMediaPropria() + mediaGrupo );
                    somarGrupo = true;
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoPropria() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }
                // LIDERES
                if ( notNull( grupoQuestao.getMediaLideres() ) ) {
                    if ( questionario.getMediaLideres() == null ) {
                        questionario.setMediaLideres( 0D );
                    }
                    Double mediaGrupo = grupoQuestao.getMediaLideres();
                    questionario.setMediaLideres( questionario.getMediaLideres() + mediaGrupo );
                    somarGrupo = true;
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoSuperior() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }
                // SUBORDINADOS
                if ( notNull( grupoQuestao.getMediaSubordinados() ) ) {
                    if ( questionario.getMediaSubordinados() == null ) {
                        questionario.setMediaSubordinados( 0D );
                    }
                    Double mediaGrupo = grupoQuestao.getMediaSubordinados();
                    questionario.setMediaSubordinados( questionario.getMediaSubordinados() + mediaGrupo );
                    somarGrupo = true;
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoSubordinados() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }
                // OUTROS
                if ( notNull( grupoQuestao.getMediaOutros() ) ) {
                    if ( questionario.getMediaOutros() == null ) {
                        questionario.setMediaOutros( 0D );
                    }
                    Double mediaGrupo = grupoQuestao.getMediaOutros();
                    questionario.setMediaOutros( questionario.getMediaOutros() + mediaGrupo );
                    somarGrupo = true;
                    totalAvaliadores++;
                    if ( pesquisa.getCalculoValorObtidoOutros() ) {
                        totalAvaliadoresValorObtido++;
                    }
                }

                // SOMANDO META DO QUESTIONARIO
                Double metaGrupo = grupoQuestao.getMeta();
                if ( metaGrupo != null ) {
                    if ( questionario.getMeta() == null ) {
                        questionario.setMeta( 0D );
                    }
                    questionario.setMeta( questionario.getMeta() + metaGrupo );
                    somarGrupo = true;
                }

                if ( somarGrupo ) {
                    totalGrupos++;
                }

                if ( notNull( grupoQuestao.getPercentualMeta() ) ) {
                    totalPercentualMetaGrupos += grupoQuestao.getPercentualMeta();
                }

            } // for grupos

            // META DO QUESTIONARIO
            if ( notNull( questionario.getMeta() ) ) {
                questionario.setMeta( questionario.getMeta() / totalGrupos );
            }

            // VALOR OBTIDO QUESTIONARIO
            Double totalMediasQuestionarioValorObtido = 0D;
            if ( notNull( questionario.getMediaPropria() ) ) {
                if ( pesquisa.getCalculoValorObtidoPropria() ) {
                    totalMediasQuestionarioValorObtido += questionario.getMediaPropria();
                }
            }
            if ( notNull( questionario.getMediaLideres() ) ) {
                if ( pesquisa.getCalculoValorObtidoSuperior() ) {
                    totalMediasQuestionarioValorObtido += questionario.getMediaLideres();
                }
            }
            if ( notNull( questionario.getMediaSubordinados() ) ) {
                if ( pesquisa.getCalculoValorObtidoSubordinados() ) {
                    totalMediasQuestionarioValorObtido += questionario.getMediaSubordinados();
                }
            }
            if ( notNull( questionario.getMediaOutros() ) ) {
                if ( pesquisa.getCalculoValorObtidoOutros() ) {
                    totalMediasQuestionarioValorObtido += questionario.getMediaOutros();
                }
            }

            questionario.setValorObtido( totalMediasQuestionarioValorObtido / totalAvaliadoresValorObtido );

            // PERCENTUAL DA META DO QUESTIONARIO
            questionario.setPercentualMeta( totalPercentualMetaGrupos / totalGrupos );

            // CALCULANDO MEDIA TOTAL DO QUESTIONARIO

            // PROPRIA
            if ( notNull( questionario.getMediaPropria() ) ) {
                questionario.setMediaPropria( questionario.getMediaPropria() / totalGrupos );
            }
            // LIDERES
            if ( notNull( questionario.getMediaLideres() ) ) {
                questionario.setMediaLideres( questionario.getMediaLideres() / totalGrupos );
            }
            // SUBORDINADOS
            if ( notNull( questionario.getMediaSubordinados() ) ) {
                questionario.setMediaSubordinados( questionario.getMediaSubordinados() / totalGrupos );
            }
            // OUTROS
            if ( notNull( questionario.getMediaOutros() ) ) {
                questionario.setMediaOutros( questionario.getMediaOutros() / totalGrupos );
            }

        }

        // return questionariosResultado;

    }


    private void gerarMediaTotal2( Pesquisa pesquisa, List< QuestionarioView > questionariosResultado ) {

        ajustarNotas( pesquisa, questionariosResultado );

        for ( QuestionarioView questionario : questionariosResultado ) {

            for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {
                somarNotasDasQuestoesDoGrupo( grupoQuestao );
                calcularMediaDasNotasDasQuestoesDoGrupo( grupoQuestao );
            }

            somarNotasDosGruposDoQuestionario( questionario );
            calcularMediaDasNotasDosGruposDoQuestionario( questionario );

        }

    }


    private void somarNotasDosGruposDoQuestionario( QuestionarioView questionario ) {

        for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {

            if ( notNull( grupoQuestao.getMediaPropria() ) ) {
                if ( questionario.getMediaPropria() == null ) {
                    questionario.setMediaPropria( 0D );
                }
                questionario.setMediaPropria( questionario.getMediaPropria() + grupoQuestao.getMediaPropria() );
                questionario.qtdeTotalPropria++;
            }

            if ( notNull( grupoQuestao.getMediaLideres() ) ) {
                if ( questionario.getMediaLideres() == null ) {
                    questionario.setMediaLideres( 0D );
                }
                questionario.setMediaLideres( questionario.getMediaLideres() + grupoQuestao.getMediaLideres() );
                questionario.qtdeTotalLider++;
            }

            if ( notNull( grupoQuestao.getMediaSubordinados() ) ) {
                if ( questionario.getMediaSubordinados() == null ) {
                    questionario.setMediaSubordinados( 0D );
                }
                questionario.setMediaSubordinados( questionario.getMediaSubordinados() + grupoQuestao.getMediaSubordinados() );
                questionario.qtdeTotalSubordinados++;
            }

            if ( notNull( grupoQuestao.getMediaOutros() ) ) {
                if ( questionario.getMediaOutros() == null ) {
                    questionario.setMediaOutros( 0D );
                }
                questionario.setMediaOutros( questionario.getMediaOutros() + grupoQuestao.getMediaOutros() );
                questionario.qtdeTotalOutros++;
            }

            if ( notNull( grupoQuestao.getMeta() ) ) {
                if ( questionario.getMeta() == null ) {
                    questionario.setMeta( 0D );
                }
                questionario.setMeta( questionario.getMeta() + grupoQuestao.getMeta() );
                questionario.qtdeTotalMeta++;
            }

            if ( notNull( grupoQuestao.getValorObtido() ) ) {
                if ( questionario.getValorObtido() == null ) {
                    questionario.setValorObtido( 0D );
                }
                questionario.setValorObtido( questionario.getValorObtido() + grupoQuestao.getValorObtido() );
                questionario.qtdeTotalValorObtido++;
            }

            if ( notNull( grupoQuestao.getPercentualMeta() ) ) {
                if ( questionario.getPercentualMeta() == null ) {
                    questionario.setPercentualMeta( 0D );
                }
                questionario.setPercentualMeta( questionario.getPercentualMeta() + grupoQuestao.getPercentualMeta() );
                questionario.qtdeTotalPercentualMeta++;
            }

        }

    }


    private void calcularMediaDasNotasDosGruposDoQuestionario( QuestionarioView questionario ) {

        if ( notNull( questionario.getMediaPropria() ) ) {
            questionario.setMediaPropria( questionario.getMediaPropria() / questionario.qtdeTotalPropria );
        }

        if ( notNull( questionario.getMediaLideres() ) ) {
            questionario.setMediaLideres( questionario.getMediaLideres() / questionario.qtdeTotalLider );
        }

        if ( notNull( questionario.getMediaSubordinados() ) ) {
            questionario.setMediaSubordinados( questionario.getMediaSubordinados() / questionario.qtdeTotalSubordinados );
        }

        if ( notNull( questionario.getMediaOutros() ) ) {
            questionario.setMediaOutros( questionario.getMediaOutros() / questionario.qtdeTotalOutros );
        }

        if ( notNull( questionario.getMeta() ) ) {
            questionario.setMeta( questionario.getMeta() / questionario.qtdeTotalMeta );
        }

        if ( notNull( questionario.getValorObtido() ) ) {
            questionario.setValorObtido( questionario.getValorObtido() / questionario.qtdeTotalValorObtido );
        }

        if ( notNull( questionario.getPercentualMeta() ) ) {
            questionario.setPercentualMeta( questionario.getPercentualMeta() / questionario.qtdeTotalPercentualMeta );
        }

    }


    private void somarNotasDasQuestoesDoGrupo( GrupoQuestaoView grupoQuestao ) {

        for ( QuestaoOrdemView questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

            QuestaoView questao = questaoOrdem.getQuestao();

            if ( !questaoService.temClassificacao( questao ) ) {
                continue;
            }

            // SOMANDO MEDIA PROPRIA
            if ( questao.getNotaPropria() != null ) {

                if ( questao.getNotaPropria().getMedia() != null ) {
                    if ( grupoQuestao.getMediaPropria() == null ) {
                        grupoQuestao.setMediaPropria( 0D );
                    }
                    Double mediaQuestao = questao.getNotaPropria().getMedia();
                    grupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() + mediaQuestao );
                    grupoQuestao.totalQuestoesPropria++;
                }

            }

            // SOMANDO MEDIA LIDERES
            if ( questao.getNotaLideres() != null ) {
                if ( questao.getNotaLideres().getMedia() != null ) {
                    if ( grupoQuestao.getMediaLideres() == null ) {
                        grupoQuestao.setMediaLideres( 0D );
                    }
                    Double mediaQuestao = questao.getNotaLideres().getMedia();
                    grupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() + mediaQuestao );
                    grupoQuestao.totalQuestoesLider++;
                }
            }

            // SOMANDO MEDIA SUBORDINADOS
            if ( questao.getNotaSubordinados() != null ) {
                if ( questao.getNotaSubordinados().getMedia() != null ) {
                    if ( grupoQuestao.getMediaSubordinados() == null ) {
                        grupoQuestao.setMediaSubordinados( 0D );
                    }
                    Double mediaQuestao = questao.getNotaSubordinados().getMedia();
                    grupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() + mediaQuestao );
                    grupoQuestao.totalQuestoesSubordinados++;
                }
            }

            // SOMANDO MEDIA OUTROS
            if ( questao.getNotaOutros() != null ) {
                if ( questao.getNotaOutros().getMedia() != null ) {
                    if ( grupoQuestao.getMediaOutros() == null ) {
                        grupoQuestao.setMediaOutros( 0D );
                    }
                    Double mediaQuestao = questao.getNotaOutros().getMedia();
                    grupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() + mediaQuestao );
                    grupoQuestao.totalQuestoesOutros++;
                }
            }

            // SOMANDO META GRUPO
            Double metaQuestao = questao.getMeta();
            if ( metaQuestao != null && !metaQuestao.equals( 0D ) ) {
                if ( grupoQuestao.getMeta() == null ) {
                    grupoQuestao.setMeta( 0D );
                }
                grupoQuestao.setMeta( grupoQuestao.getMeta() + metaQuestao );
                grupoQuestao.totalQuestoesMeta++;
            }

            // SOMANDO VALOR OBTIDO
            Double valorObtido = questao.getValorObtido();
            if ( valorObtido != null && !valorObtido.equals( 0D ) ) {
                if ( grupoQuestao.getValorObtido() == null ) {
                    grupoQuestao.setValorObtido( 0D );
                }
                grupoQuestao.setValorObtido( grupoQuestao.getValorObtido() + valorObtido );
                grupoQuestao.totalQuestoesValorObtido++;
            }

            // SOMANDO PERCENTUAL META
            Double percentualMeta = questao.getPercentualMeta();
            if ( percentualMeta != null && !percentualMeta.equals( 0D ) ) {
                if ( grupoQuestao.getPercentualMeta() == null ) {
                    grupoQuestao.setPercentualMeta( 0D );
                }
                grupoQuestao.setPercentualMeta( grupoQuestao.getPercentualMeta() + percentualMeta );
                grupoQuestao.totalQuestoesPercentualMeta++;
            }

        } // loop questoes
    }


    private void calcularMediaDasNotasDasQuestoesDoGrupo( GrupoQuestaoView grupoQuestao ) {

        if ( notNull( grupoQuestao.getMediaPropria() ) ) {
            grupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() / grupoQuestao.totalQuestoesPropria );
        }

        if ( notNull( grupoQuestao.getMediaLideres() ) ) {
            grupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() / grupoQuestao.totalQuestoesLider );
        }

        if ( notNull( grupoQuestao.getMediaSubordinados() ) ) {
            grupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() / grupoQuestao.totalQuestoesSubordinados );
        }

        if ( notNull( grupoQuestao.getMediaOutros() ) ) {
            grupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() / grupoQuestao.totalQuestoesOutros );
        }

        if ( notNull( grupoQuestao.getMeta() ) ) {
            grupoQuestao.setMeta( grupoQuestao.getMeta() / grupoQuestao.totalQuestoesMeta );
        }

        if ( notNull( grupoQuestao.getValorObtido() ) ) {
            grupoQuestao.setValorObtido( grupoQuestao.getValorObtido() / grupoQuestao.totalQuestoesValorObtido );
        }

        if ( notNull( grupoQuestao.getPercentualMeta() ) ) {
            grupoQuestao.setPercentualMeta( grupoQuestao.getPercentualMeta() / grupoQuestao.totalQuestoesPercentualMeta );
        }

    }


    private int getTotalGruposComMedia( Questionario questionario, boolean isPropria, boolean isLideres, boolean isSubordinados ) {

        int total = 0;
        if ( CollectionUtils.isNotEmpty( questionario.getGruposQuestoes() ) ) {
            for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
                if ( notNull( grupoQuestao.getMediaPropria() ) ) {

                }
            }

        }
        return total;
    }


    private void ajustarNotas( Pesquisa pesquisa, List< QuestionarioView > questionariosResultado ) {

        if ( questionariosResultado != null ) {

            for ( QuestionarioView questionario : questionariosResultado ) {

                // int qtdeQuestoes = 0;

                if ( questionario.getGruposQuestoes() != null ) {
                    for ( GrupoQuestaoView grupoQuestao : questionario.getGruposQuestoes() ) {

                        if ( grupoQuestao.getQuestoesOrdem() != null ) {

                            Double maiorNotaGrupo = getMaiorNotaGrupo( grupoQuestao );

                            // qtdeQuestoes = grupoQuestao.getQuestoesOrdem().size();

                            for ( QuestaoOrdemView questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                                QuestaoView q = questaoOrdem.getQuestao();

                                if ( !questaoService.temClassificacao( q ) ) {
                                    continue;
                                }

                                Double somaNotas = 0D;
                                Double somaNotasValorObtido = 0D;
                                int totalAvaliadores = 0;
                                int totalAvaliadoresValorObtido = 0;

                                Nota notaPropria = null;
                                Nota notaLideres = null;
                                Nota notaSubordinados = null;
                                Nota notaOutros = null;

                                if ( q.getNotaPropria() != null ) {
                                    notaPropria = q.getNotaPropria();
                                }
                                if ( q.getNotaLideres() != null ) {
                                    notaLideres = q.getNotaLideres();
                                }
                                if ( q.getNotaSubordinados() != null ) {
                                    notaSubordinados = q.getNotaSubordinados();
                                }
                                if ( q.getNotaOutros() != null ) {
                                    notaOutros = q.getNotaOutros();
                                }

                                Double metaQuestao = q.getMeta();

                                if ( notaPropria != null && notaPropria.getMedia() != null ) {
                                    if ( metaQuestao != null && pesquisa.getCalculoPorMeta() ) {
                                        notaPropria.setMedia( ( metaQuestao / maiorNotaGrupo ) * notaPropria.getMedia() );
                                    }
                                    somaNotas += notaPropria.getMedia();
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoPropria() ) {
                                        somaNotasValorObtido += notaPropria.getMedia();
                                        totalAvaliadoresValorObtido++;
                                    }
                                }
                                if ( notaPropria != null && "0".equals( notaPropria.getMediaExibicao() ) ) {
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoPropria() ) {
                                        totalAvaliadoresValorObtido++;
                                    }
                                }

                                if ( notaLideres != null && notaLideres.getMedia() != null ) {
                                    if ( metaQuestao != null && pesquisa.getCalculoPorMeta() ) {
                                        notaLideres.setMedia( ( metaQuestao / maiorNotaGrupo ) * notaLideres.getMedia() );
                                    }
                                    somaNotas += notaLideres.getMedia();
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoSuperior() ) {
                                        somaNotasValorObtido += notaLideres.getMedia();
                                        totalAvaliadoresValorObtido++;
                                    }
                                }
                                if ( notaLideres != null && "0".equals( notaLideres.getMediaExibicao() ) ) {
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoSuperior() ) {
                                        totalAvaliadoresValorObtido++;
                                    }
                                }

                                if ( notaSubordinados != null && notaSubordinados.getMedia() != null ) {
                                    if ( metaQuestao != null && pesquisa.getCalculoPorMeta() ) {
                                        notaSubordinados.setMedia( ( metaQuestao / maiorNotaGrupo ) * notaSubordinados.getMedia() );
                                    }
                                    somaNotas += notaSubordinados.getMedia();
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoSubordinados() ) {
                                        somaNotasValorObtido += notaSubordinados.getMedia();
                                        totalAvaliadoresValorObtido++;
                                    }
                                }
                                if ( notaSubordinados != null && "0".equals( notaSubordinados.getMediaExibicao() ) ) {
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoSubordinados() ) {
                                        totalAvaliadoresValorObtido++;
                                    }
                                }

                                if ( notaOutros != null && notaOutros.getMedia() != null ) {
                                    if ( metaQuestao != null && pesquisa.getCalculoPorMeta() ) {
                                        notaOutros.setMedia( ( metaQuestao / maiorNotaGrupo ) * notaOutros.getMedia() );
                                    }
                                    somaNotas += notaOutros.getMedia();
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoOutros() ) {
                                        somaNotasValorObtido += notaOutros.getMedia();
                                        totalAvaliadoresValorObtido++;
                                    }
                                }
                                if ( notaOutros != null && "0".equals( notaOutros.getMediaExibicao() ) ) {
                                    totalAvaliadores++;
                                    if ( pesquisa.getCalculoValorObtidoOutros() ) {
                                        totalAvaliadoresValorObtido++;
                                    }
                                }

                                q.setNotaPropria( notaPropria );
                                q.setNotaLideres( notaLideres );
                                q.setNotaSubordinados( notaSubordinados );
                                q.setNotaOutros( notaOutros );

                                q.setValorObtido( somaNotasValorObtido / totalAvaliadoresValorObtido );

                                if ( metaQuestao != null ) {
                                    if ( q.getNegativo() ) {
                                        q.setPercentualMeta( metaQuestao / q.getValorObtido() );
                                    } else {
                                        q.setPercentualMeta( q.getValorObtido() / metaQuestao );
                                    }
                                }

                            }

                        }

                    }

                }

            }

        }
    }


    private Double getMaiorNotaGrupo( GrupoQuestaoView grupoQuestao ) {

        Double maiorNota = 0D;

        if ( grupoQuestao != null && CollectionUtils.isNotEmpty( grupoQuestao.getQuestoesOrdem() ) ) {
            for ( QuestaoOrdemView qo : grupoQuestao.getQuestoesOrdem() ) {

                if ( !questaoService.temClassificacao( qo.getQuestao() ) ) {
                    continue;
                }

                if ( qo.getQuestao().getListaValores() != null && CollectionUtils.isNotEmpty( qo.getQuestao().getListaValores().getValoresLista() ) ) {
                    for ( ValorListaView valorLista : qo.getQuestao().getListaValores().getValoresLista() ) {

                        if ( valorLista.getValor() != null ) {
                            Double valor = 0D;

                            try {

                                valor = Double.valueOf( valorLista.getValor() );

                            } catch ( Exception e ) {
                                // do nothing
                            }

                            if ( valor > maiorNota ) {
                                maiorNota = valor;
                            }

                        }

                    }
                }

            }

        }

        return maiorNota;
    }


    private int getTotalQuestoesTipoEscalaOuNumericoQuePossuiNota(
        Set< QuestaoOrdemView > questoesOrdem,
        boolean isPropria,
        boolean isLideres,
        boolean isSubordinados,
        boolean isOutros,
        boolean somarQuestoesNaoClassificadas ) {

        int qtde = 0;
        if ( questoesOrdem != null ) {
            for ( QuestaoOrdemView qo : questoesOrdem ) {
                if ( qo.getQuestao() != null ) {

                    if ( somarQuestoesNaoClassificadas || questaoService.temClassificacao( qo.getQuestao() ) ) {
                        if ( qo.getQuestao().getIsLista() || qo.getQuestao().getIsNumerico() ) {
                            boolean somar = false;

                            if ( somarQuestoesNaoClassificadas ) {
                                somar = true;
                            } else {

                                if ( isPropria ) {
                                    if ( qo.getQuestao().getNotaPropria() != null ) {
                                        if ( qo.getQuestao().getNotaPropria().getMedia() != null
                                            || "0".equals( qo.getQuestao().getNotaPropria().getMediaExibicao() ) ) {
                                            somar = true;
                                        }
                                    }
                                } else if ( isLideres ) {
                                    if ( qo.getQuestao().getNotaLideres() != null ) {
                                        if ( qo.getQuestao().getNotaLideres().getMedia() != null
                                            || "0".equals( qo.getQuestao().getNotaLideres().getMediaExibicao() ) ) {
                                            somar = true;
                                        }
                                    }
                                } else if ( isSubordinados ) {
                                    if ( qo.getQuestao().getNotaSubordinados() != null ) {
                                        if ( qo.getQuestao().getNotaSubordinados().getMedia() != null
                                            || "0".equals( qo.getQuestao().getNotaSubordinados().getMediaExibicao() ) ) {
                                            somar = true;
                                        }
                                    }
                                } else if ( isOutros ) {
                                    if ( qo.getQuestao().getNotaOutros() != null ) {
                                        if ( qo.getQuestao().getNotaOutros().getMedia() != null
                                            || "0".equals( qo.getQuestao().getNotaOutros().getMediaExibicao() ) ) {
                                            somar = true;
                                        }
                                    }
                                }

                            }

                            if ( somar ) {
                                qtde++;
                            }
                        }
                    }

                }
            }
        }
        return qtde;
    }


    private void atualizarQuestoesDoQuestionario(
        Questionario questionario,
        List< QuestaoRespostas > questoesRespostas,
        boolean isPropria,
        boolean isLideres,
        boolean isSubordinados,
        boolean isOutros )
        throws IllegalAccessException,
        InvocationTargetException,
        QuestionarioSemRespostasException {

        boolean questionarioSemRespostas = true;
        for ( QuestaoRespostas questaoRespostas : questoesRespostas ) {
            if ( questaoRespostas.getQuestionario().getId().equals( questionario.getId() ) ) {
                Set< GrupoQuestao > newGruposQuestoes = copyGruposQuestoes( questionario.getGruposQuestoes() );
                questionario.setGruposQuestoes( newGruposQuestoes );
                for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
                    Set< QuestaoOrdem > newQuestoesOrdem = copyQuestoesOrdem( grupoQuestao.getQuestoesOrdem() );
                    grupoQuestao.setQuestoesOrdem( newQuestoesOrdem );
                    for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                        if ( questaoOrdem.getQuestao().getId().equals( questaoRespostas.getQuestao().getId() )
                            && grupoQuestao.getId().equals( questaoRespostas.getIdGrupoQuestao() ) ) {
                            questionarioSemRespostas = false;

                            QuestaoClassificacao questaoClassificacao = questaoOrdem.getQuestao().getQuestaoClassificacao();

                            // ATUALIZANDO QUESTAO
                            questaoOrdem.setQuestao( questaoRespostas.getQuestao() );

                            if ( questaoClassificacao != null ) {
                                questaoOrdem.getQuestao().setQuestaoClassificacao( questaoClassificacao );
                                questaoOrdem.getQuestao().setClassificacao( questaoClassificacao.getClassificacao() );
                            }

                        }
                    }
                }
            }
        }

        if ( questionarioSemRespostas ) {
            throw new QuestionarioSemRespostasException();
        }

    }


    private boolean setarRespostasNasQuestoesDoQuestionario(
        Questionario questionario,
        QuestaoRespostasAgrupadasPorAvaliadores questaoRespostasAgrupadasPorAvaliadores ) {

        boolean questionarioComRespostas = false;

        for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
            for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {

                Questao questao = questaoOrdem.getQuestao();

                // RESPOSTAS PROPRIA
                if ( questaoRespostasAgrupadasPorAvaliadores.getRespostasPropria() != null ) {
                    for ( QuestaoRespostas questaoRespostasPropria : questaoRespostasAgrupadasPorAvaliadores.getRespostasPropria() ) {

                        boolean sameGrupo = grupoQuestao.getId().equals( questaoRespostasPropria.getIdGrupoQuestao() );
                        boolean sameQuestao = questao.getId().equals( questaoRespostasPropria.getQuestao().getId() );
                        boolean sameQuestionario = questionario.getId().equals( questaoRespostasPropria.getQuestionario().getId() );

                        if ( !sameGrupo && questaoRespostasPropria.getIdGrupoQuestao() == null ) {
                            sameGrupo = true;
                        }

                        if ( sameGrupo && sameQuestao && sameQuestionario ) {

                            questionarioComRespostas = true;
                            QuestaoClassificacao questaoClassificacao = questaoOrdem.getQuestao().getQuestaoClassificacao();

                            // ATUALIZAR QUESTAO
                            questaoOrdem.getQuestao().setNotaPropria( questaoRespostasPropria.getQuestao().getNotaPropria() );
                            // questaoOrdem.getQuestao().setNotaLideres(
                            // questaoRespostasPropria.getQuestao().getNotaLideres() );
                            // questaoOrdem.getQuestao().setNotaSubordinados(
                            // questaoRespostasPropria.getQuestao().getNotaSubordinados() );
                            // questaoOrdem.getQuestao().setNotaOutros(
                            // questaoRespostasPropria.getQuestao().getNotaOutros() );
                            questaoOrdem.getQuestao().setResposta( questaoRespostasPropria.getQuestao().getResposta() );
                            questaoOrdem.getQuestao().setRespostaTexto( questaoRespostasPropria.getQuestao().getRespostaTexto() );
                            questaoOrdem.getQuestao().setRespostaNumerico( questaoRespostasPropria.getQuestao().getRespostaNumerico() );
                            questaoOrdem.getQuestao().setRespostaValorLista( questaoRespostasPropria.getQuestao().getRespostaValorLista() );
                            questaoOrdem.getQuestao().setRespostaId( questaoRespostasPropria.getQuestao().getRespostaId() );
                            questaoOrdem.getQuestao()
                                .setQuestoesMetasGruposOcupacionais( questaoRespostasPropria.getQuestao().getQuestoesMetasGruposOcupacionais() );
                            questaoOrdem.getQuestao().setQuestoesMetasCargos( questaoRespostasPropria.getQuestao().getQuestoesMetasCargos() );
                            questaoOrdem.getQuestao().setQuestoesMetasSetores( questaoRespostasPropria.getQuestao().getQuestoesMetasSetores() );
                            questaoOrdem.getQuestao().setClassificacao( questaoRespostasPropria.getQuestao().getClassificacao() );
                            questaoOrdem.getQuestao().setQuestaoClassificacao( questaoRespostasPropria.getQuestao().getQuestaoClassificacao() );
                            questaoOrdem.getQuestao().setMeta( questaoRespostasPropria.getQuestao().getMeta() );
                            questaoOrdem.getQuestao().setNegativo( questaoRespostasPropria.getQuestao().getNegativo() );
                            questaoOrdem.getQuestao().setValorObtido( questaoRespostasPropria.getQuestao().getValorObtido() );
                            questaoOrdem.getQuestao().setPercentualMeta( questaoRespostasPropria.getQuestao().getPercentualMeta() );
                            questaoOrdem.getQuestao().setMedia( questaoRespostasPropria.getQuestao().getMedia() );

                            if ( questaoClassificacao != null ) {
                                questaoOrdem.getQuestao().setQuestaoClassificacao( questaoClassificacao );
                                questaoOrdem.getQuestao().setClassificacao( questaoClassificacao.getClassificacao() );
                            }

                            break;

                        }
                    }
                }

                // RESPOSTAS LIDERES
                if ( questaoRespostasAgrupadasPorAvaliadores.getRespostasLideres() != null ) {
                    for ( QuestaoRespostas questaoRespostasLideres : questaoRespostasAgrupadasPorAvaliadores.getRespostasLideres() ) {

                        boolean sameGrupo = grupoQuestao.getId().equals( questaoRespostasLideres.getIdGrupoQuestao() );
                        boolean sameQuestao = questao.getId().equals( questaoRespostasLideres.getQuestao().getId() );
                        boolean sameQuestionario = questionario.getId().equals( questaoRespostasLideres.getQuestionario().getId() );

                        if ( !sameGrupo && questaoRespostasLideres.getIdGrupoQuestao() == null ) {
                            sameGrupo = true;
                        }

                        if ( sameGrupo && sameQuestao && sameQuestionario ) {

                            questionarioComRespostas = true;
                            QuestaoClassificacao questaoClassificacao = questaoOrdem.getQuestao().getQuestaoClassificacao();

                            // ATUALIZAR QUESTAO
                            // questaoOrdem.getQuestao().setNotaPropria(
                            // questaoRespostasLideres.getQuestao().getNotaPropria() );
                            questaoOrdem.getQuestao().setNotaLideres( questaoRespostasLideres.getQuestao().getNotaLideres() );
                            // questaoOrdem.getQuestao().setNotaSubordinados(
                            // questaoRespostasLideres.getQuestao().getNotaSubordinados() );
                            // questaoOrdem.getQuestao().setNotaOutros(
                            // questaoRespostasLideres.getQuestao().getNotaOutros() );
                            questaoOrdem.getQuestao().setResposta( questaoRespostasLideres.getQuestao().getResposta() );
                            questaoOrdem.getQuestao().setRespostaTexto( questaoRespostasLideres.getQuestao().getRespostaTexto() );
                            questaoOrdem.getQuestao().setRespostaNumerico( questaoRespostasLideres.getQuestao().getRespostaNumerico() );
                            questaoOrdem.getQuestao().setRespostaValorLista( questaoRespostasLideres.getQuestao().getRespostaValorLista() );
                            questaoOrdem.getQuestao().setRespostaId( questaoRespostasLideres.getQuestao().getRespostaId() );
                            questaoOrdem.getQuestao()
                                .setQuestoesMetasGruposOcupacionais( questaoRespostasLideres.getQuestao().getQuestoesMetasGruposOcupacionais() );
                            questaoOrdem.getQuestao().setQuestoesMetasCargos( questaoRespostasLideres.getQuestao().getQuestoesMetasCargos() );
                            questaoOrdem.getQuestao().setQuestoesMetasSetores( questaoRespostasLideres.getQuestao().getQuestoesMetasSetores() );
                            questaoOrdem.getQuestao().setClassificacao( questaoRespostasLideres.getQuestao().getClassificacao() );
                            questaoOrdem.getQuestao().setQuestaoClassificacao( questaoRespostasLideres.getQuestao().getQuestaoClassificacao() );
                            questaoOrdem.getQuestao().setMeta( questaoRespostasLideres.getQuestao().getMeta() );
                            questaoOrdem.getQuestao().setNegativo( questaoRespostasLideres.getQuestao().getNegativo() );
                            questaoOrdem.getQuestao().setValorObtido( questaoRespostasLideres.getQuestao().getValorObtido() );
                            questaoOrdem.getQuestao().setPercentualMeta( questaoRespostasLideres.getQuestao().getPercentualMeta() );
                            questaoOrdem.getQuestao().setMedia( questaoRespostasLideres.getQuestao().getMedia() );

                            if ( questaoClassificacao != null ) {
                                questaoOrdem.getQuestao().setQuestaoClassificacao( questaoClassificacao );
                                questaoOrdem.getQuestao().setClassificacao( questaoClassificacao.getClassificacao() );
                            }

                            break;

                        }
                    }
                }

                // RESPOSTAS SUBORDINADOS
                if ( questaoRespostasAgrupadasPorAvaliadores.getRespostasSubordinados() != null ) {
                    for ( QuestaoRespostas questaoRespostasSubordinados : questaoRespostasAgrupadasPorAvaliadores.getRespostasSubordinados() ) {

                        boolean sameGrupo = grupoQuestao.getId().equals( questaoRespostasSubordinados.getIdGrupoQuestao() );
                        boolean sameQuestao = questao.getId().equals( questaoRespostasSubordinados.getQuestao().getId() );
                        boolean sameQuestionario = questionario.getId().equals( questaoRespostasSubordinados.getQuestionario().getId() );

                        if ( !sameGrupo && questaoRespostasSubordinados.getIdGrupoQuestao() == null ) {
                            sameGrupo = true;
                        }

                        if ( sameGrupo && sameQuestao && sameQuestionario ) {

                            questionarioComRespostas = true;
                            QuestaoClassificacao questaoClassificacao = questaoOrdem.getQuestao().getQuestaoClassificacao();

                            // ATUALIZAR QUESTAO
                            // questaoOrdem.getQuestao().setNotaPropria(
                            // questaoRespostasSubordinados.getQuestao().getNotaPropria() );
                            // questaoOrdem.getQuestao().setNotaLideres(
                            // questaoRespostasSubordinados.getQuestao().getNotaLideres() );
                            questaoOrdem.getQuestao().setNotaSubordinados( questaoRespostasSubordinados.getQuestao().getNotaSubordinados() );
                            // questaoOrdem.getQuestao().setNotaOutros(
                            // questaoRespostasSubordinados.getQuestao().getNotaOutros() );
                            questaoOrdem.getQuestao().setResposta( questaoRespostasSubordinados.getQuestao().getResposta() );
                            questaoOrdem.getQuestao().setRespostaTexto( questaoRespostasSubordinados.getQuestao().getRespostaTexto() );
                            questaoOrdem.getQuestao().setRespostaNumerico( questaoRespostasSubordinados.getQuestao().getRespostaNumerico() );
                            questaoOrdem.getQuestao().setRespostaValorLista( questaoRespostasSubordinados.getQuestao().getRespostaValorLista() );
                            questaoOrdem.getQuestao().setRespostaId( questaoRespostasSubordinados.getQuestao().getRespostaId() );
                            questaoOrdem.getQuestao()
                                .setQuestoesMetasGruposOcupacionais( questaoRespostasSubordinados.getQuestao().getQuestoesMetasGruposOcupacionais() );
                            questaoOrdem.getQuestao().setQuestoesMetasCargos( questaoRespostasSubordinados.getQuestao().getQuestoesMetasCargos() );
                            questaoOrdem.getQuestao().setQuestoesMetasSetores( questaoRespostasSubordinados.getQuestao().getQuestoesMetasSetores() );
                            questaoOrdem.getQuestao().setClassificacao( questaoRespostasSubordinados.getQuestao().getClassificacao() );
                            questaoOrdem.getQuestao().setQuestaoClassificacao( questaoRespostasSubordinados.getQuestao().getQuestaoClassificacao() );
                            questaoOrdem.getQuestao().setMeta( questaoRespostasSubordinados.getQuestao().getMeta() );
                            questaoOrdem.getQuestao().setNegativo( questaoRespostasSubordinados.getQuestao().getNegativo() );
                            questaoOrdem.getQuestao().setValorObtido( questaoRespostasSubordinados.getQuestao().getValorObtido() );
                            questaoOrdem.getQuestao().setPercentualMeta( questaoRespostasSubordinados.getQuestao().getPercentualMeta() );
                            questaoOrdem.getQuestao().setMedia( questaoRespostasSubordinados.getQuestao().getMedia() );

                            if ( questaoClassificacao != null ) {
                                questaoOrdem.getQuestao().setQuestaoClassificacao( questaoClassificacao );
                                questaoOrdem.getQuestao().setClassificacao( questaoClassificacao.getClassificacao() );
                            }

                            break;

                        }
                    }
                }

                // RESPOSTAS OUTROS
                if ( questaoRespostasAgrupadasPorAvaliadores.getRespostasOutros() != null ) {
                    for ( QuestaoRespostas questaoRespostasOutros : questaoRespostasAgrupadasPorAvaliadores.getRespostasOutros() ) {

                        boolean sameGrupo = grupoQuestao.getId().equals( questaoRespostasOutros.getIdGrupoQuestao() );
                        boolean sameQuestao = questao.getId().equals( questaoRespostasOutros.getQuestao().getId() );
                        boolean sameQuestionario = questionario.getId().equals( questaoRespostasOutros.getQuestionario().getId() );

                        if ( !sameGrupo && questaoRespostasOutros.getIdGrupoQuestao() == null ) {
                            sameGrupo = true;
                        }

                        if ( sameGrupo && sameQuestao && sameQuestionario ) {

                            questionarioComRespostas = true;
                            QuestaoClassificacao questaoClassificacao = questaoOrdem.getQuestao().getQuestaoClassificacao();

                            // ATUALIZAR QUESTAO
                            // questaoOrdem.getQuestao().setNotaPropria(
                            // questaoRespostasOutros.getQuestao().getNotaPropria() );
                            // questaoOrdem.getQuestao().setNotaLideres(
                            // questaoRespostasOutros.getQuestao().getNotaLideres() );
                            // questaoOrdem.getQuestao().setNotaSubordinados(
                            // questaoRespostasOutros.getQuestao().getNotaSubordinados() );
                            questaoOrdem.getQuestao().setNotaOutros( questaoRespostasOutros.getQuestao().getNotaOutros() );
                            questaoOrdem.getQuestao().setResposta( questaoRespostasOutros.getQuestao().getResposta() );
                            questaoOrdem.getQuestao().setRespostaTexto( questaoRespostasOutros.getQuestao().getRespostaTexto() );
                            questaoOrdem.getQuestao().setRespostaNumerico( questaoRespostasOutros.getQuestao().getRespostaNumerico() );
                            questaoOrdem.getQuestao().setRespostaValorLista( questaoRespostasOutros.getQuestao().getRespostaValorLista() );
                            questaoOrdem.getQuestao().setRespostaId( questaoRespostasOutros.getQuestao().getRespostaId() );
                            questaoOrdem.getQuestao()
                                .setQuestoesMetasGruposOcupacionais( questaoRespostasOutros.getQuestao().getQuestoesMetasGruposOcupacionais() );
                            questaoOrdem.getQuestao().setQuestoesMetasCargos( questaoRespostasOutros.getQuestao().getQuestoesMetasCargos() );
                            questaoOrdem.getQuestao().setQuestoesMetasSetores( questaoRespostasOutros.getQuestao().getQuestoesMetasSetores() );
                            questaoOrdem.getQuestao().setClassificacao( questaoRespostasOutros.getQuestao().getClassificacao() );
                            questaoOrdem.getQuestao().setQuestaoClassificacao( questaoRespostasOutros.getQuestao().getQuestaoClassificacao() );
                            questaoOrdem.getQuestao().setMeta( questaoRespostasOutros.getQuestao().getMeta() );
                            questaoOrdem.getQuestao().setNegativo( questaoRespostasOutros.getQuestao().getNegativo() );
                            questaoOrdem.getQuestao().setValorObtido( questaoRespostasOutros.getQuestao().getValorObtido() );
                            questaoOrdem.getQuestao().setPercentualMeta( questaoRespostasOutros.getQuestao().getPercentualMeta() );
                            questaoOrdem.getQuestao().setMedia( questaoRespostasOutros.getQuestao().getMedia() );

                            if ( questaoClassificacao != null ) {
                                questaoOrdem.getQuestao().setQuestaoClassificacao( questaoClassificacao );
                                questaoOrdem.getQuestao().setClassificacao( questaoClassificacao.getClassificacao() );
                            }

                            break;

                        }
                    }
                }

            }
        }

        return questionarioComRespostas;

    }


    private void updateRespostasNaQuestao( Questao questao, Questao questaoComRespostas ) {

        questao.setNotaPropria( questaoComRespostas.getNotaPropria() );
        questao.setNotaLideres( questaoComRespostas.getNotaLideres() );
        questao.setNotaSubordinados( questaoComRespostas.getNotaSubordinados() );
        questao.setNotaOutros( questaoComRespostas.getNotaOutros() );
        questao.setResposta( questaoComRespostas.getResposta() );
        questao.setRespostaTexto( questaoComRespostas.getRespostaTexto() );
        questao.setRespostaNumerico( questaoComRespostas.getRespostaNumerico() );
        questao.setRespostaValorLista( questaoComRespostas.getRespostaValorLista() );
        questao.setRespostaId( questaoComRespostas.getRespostaId() );
        questao.setQuestoesMetasGruposOcupacionais( questaoComRespostas.getQuestoesMetasGruposOcupacionais() );
        questao.setQuestoesMetasCargos( questaoComRespostas.getQuestoesMetasCargos() );
        questao.setQuestoesMetasSetores( questaoComRespostas.getQuestoesMetasSetores() );
        questao.setClassificacao( questaoComRespostas.getClassificacao() );
        questao.setQuestaoClassificacao( questaoComRespostas.getQuestaoClassificacao() );
        questao.setMeta( questaoComRespostas.getMeta() );
        questao.setNegativo( questaoComRespostas.getNegativo() );
        questao.setValorObtido( questaoComRespostas.getValorObtido() );
        questao.setPercentualMeta( questaoComRespostas.getPercentualMeta() );
        questao.setMedia( questaoComRespostas.getMedia() );

    }


    private void removeReferencesQuestao( Questionario questionario )
        throws ServiceException {

        questionario.unproxy( funcionarioDAO );
        Set< GrupoQuestao > newGruposQuestoes = copyGruposQuestoes( questionario.getGruposQuestoes() );
        questionario.setGruposQuestoes( newGruposQuestoes );
        for ( GrupoQuestao grupoQuestao : questionario.getGruposQuestoes() ) {
            Set< QuestaoOrdem > newQuestoesOrdem = copyQuestoesOrdem( grupoQuestao.getQuestoesOrdem() );
            grupoQuestao.setQuestoesOrdem( newQuestoesOrdem );
            for ( QuestaoOrdem questaoOrdem : grupoQuestao.getQuestoesOrdem() ) {
                Questao newQuestao = new Questao();
                try {
                    org.apache.commons.beanutils.BeanUtils.copyProperties( newQuestao, questaoOrdem.getQuestao() );
                    questaoOrdem.setQuestao( newQuestao );
                } catch (
                    IllegalAccessException
                    | InvocationTargetException e ) {
                    throw new ServiceException( "Erro ao copiar objeto Questao removeReferencesQuestao(). " + e.getMessage() );
                }

            }
        }

    }


    private Set< GrupoQuestao > copyGruposQuestoes( Set< GrupoQuestao > entities ) {

        Set< GrupoQuestao > newEntities = new LinkedHashSet<>();

        if ( entities != null ) {
            for ( GrupoQuestao entity : entities ) {
                entity.unproxy( funcionarioDAO );
                GrupoQuestao newEntity = new GrupoQuestao();
                try {
                    org.apache.commons.beanutils.BeanUtils.copyProperties( newEntity, entity );
                    newEntities.add( newEntity );
                } catch ( Exception e ) {
                    log.error( e.getMessage(), e );
                }
            }

        }

        return newEntities;

    }


    private Set< QuestaoOrdem > copyQuestoesOrdem( Set< QuestaoOrdem > entities ) {

        Set< QuestaoOrdem > newEntities = new LinkedHashSet<>();

        if ( entities != null ) {
            for ( QuestaoOrdem entity : entities ) {
                entity.unproxy( funcionarioDAO );
                QuestaoOrdem newEntity = new QuestaoOrdem();
                try {
                    org.apache.commons.beanutils.BeanUtils.copyProperties( newEntity, entity );
                    newEntities.add( newEntity );
                } catch ( Exception e ) {
                    log.error( e.getMessage(), e );
                }
            }

        }

        return newEntities;

    }


    private void gerarNota(
        QuestaoRespostas questaoRespostas,
        Questao questao,
        boolean isPropria,
        boolean isLideres,
        boolean isSubordinados,
        boolean isOutros ) {

        if ( !questaoService.temClassificacao( questao.formatToWebPage().format() ) ) {
            return;
        }

        Nota nota = new Nota();

        // GERANDO NOTA
        // Questao esacala de valores
        if ( questaoRespostas.getQuestao().getIsLista() || questaoRespostas.getQuestao().getIsNumerico() ) {

            nota.setMedia( questaoRespostas.getMedia() );

            if ( questaoRespostas.getValorListaMedia() != null ) {
                nota.setMediaExibicao( questaoRespostas.getValorListaMedia().getNome() );
            } else {
                String m = questaoRespostas.getMedia() != null ? "" + questaoRespostas.getMedia() : "0";
                nota.setMediaExibicao( m );
            }

        } else {
            // Questao de texto
            List< String > textosRespostas = new ArrayList<>();
            if ( questaoRespostas.getRespostas() != null ) {
                for ( Resposta resposta : questaoRespostas.getRespostas() ) {
                    if ( resposta.getRespostaTexto() != null ) {
                        textosRespostas.add( resposta.getRespostaTexto() );
                    }
                }
            }
            nota.setRespostasTextos( textosRespostas );
        }

        // Questao questao = new Questao();
        // if (questao == null) {
        // questao = new Questao();
        // }

        // if (questao.getNomeQuestao() == null) {
        // questao.setNomeQuestao(questaoRespostas.getQuestao().getPerguntaQuestao());
        // }
        if ( questaoRespostas.getQuestao().getPerguntaQuestao() != null ) {
            questao.setNomeQuestao( questaoRespostas.getQuestao().getPerguntaQuestao() );
        }

        // if (questao.getId() == null) {
        // questao.setId(questaoRespostas.getQuestao().getId());
        // }
        // questao.setIsLista(questaoRespostas.getQuestao().getIsLista());
        // questao.setIsTexto(questaoRespostas.getQuestao().getIsTexto());

        // if ( questaoRespostas.getTotalQuestoesDoGrupo() != null && nota.getMedia() !=
        // null ) {
        // nota.setMedia( nota.getMedia() / Double.valueOf(
        // questaoRespostas.getTotalQuestoesDoGrupo() ) );
        // }

        if ( isPropria ) {
            questao.setNotaPropria( nota );
        } else if ( isLideres ) {
            questao.setNotaLideres( nota );
        } else if ( isSubordinados ) {
            questao.setNotaSubordinados( nota );
        } else if ( isOutros ) {
            questao.setNotaOutros( nota );
        }

        // return questao;

    }


    private QuestaoRespostasAgrupadasPorAvaliadores gerarQuestaoRespostasAgrupadas(
        Pesquisa pesquisa,
        RespostasAgrupadasPorAvaliadores respostasAgrupadasPorAvaliadores )
        throws ServiceException {

        Assert.notNull( pesquisa );
        Assert.notNull( pesquisa.getId() );

        QuestaoRespostasAgrupadasPorAvaliadores questaoRespostasAgrupadasPorAvaliadores = new QuestaoRespostasAgrupadasPorAvaliadores();

        questaoRespostasAgrupadasPorAvaliadores
            .setRespostasSubordinados( gerarQuestoesRespostas( respostasAgrupadasPorAvaliadores.getRespostasSubordinados() ) );
        questaoRespostasAgrupadasPorAvaliadores.setRespostasLideres( gerarQuestoesRespostas( respostasAgrupadasPorAvaliadores.getRespostasLideres() ) );
        questaoRespostasAgrupadasPorAvaliadores.setRespostasOutros( gerarQuestoesRespostas( respostasAgrupadasPorAvaliadores.getRespostasOutros() ) );
        questaoRespostasAgrupadasPorAvaliadores.setRespostasPropria( gerarQuestoesRespostas( respostasAgrupadasPorAvaliadores.getRespostasPropria() ) );

        questaoRespostasAgrupadasPorAvaliadores.setPesquisa( pesquisa );

        // RESPOSTAS PROPRIA
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasPropria() ) {
            // Questao questao = questaoRespostas.getQuestao();

            boolean isPropria = false, isLideres = false, isSubordinados = false, isOutros = false;
            isPropria = true;
            gerarNota( questaoRespostas, questaoRespostas.getQuestao(), isPropria, isLideres, isSubordinados, isOutros );
            calcularMetaQuestao(
                questaoRespostasAgrupadasPorAvaliadores,
                questaoRespostas.getQuestao(),
                pesquisa,
                questaoRespostas.getQuestionario(),
                questaoRespostas.getFuncionarioAvaliado() );

            // questaoRespostas.setQuestao(questao);
        }
        // RESPOSTAS LIDERES
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasLideres() ) {
            // Questao questao = questaoRespostas.getQuestao();

            boolean isPropria = false, isLideres = false, isSubordinados = false, isOutros = false;
            isLideres = true;
            gerarNota( questaoRespostas, questaoRespostas.getQuestao(), isPropria, isLideres, isSubordinados, isOutros );
            calcularMetaQuestao(
                questaoRespostasAgrupadasPorAvaliadores,
                questaoRespostas.getQuestao(),
                pesquisa,
                questaoRespostas.getQuestionario(),
                questaoRespostas.getFuncionarioAvaliado() );

            // questaoRespostas.setQuestao(questao);
        }
        // RESPOSTAS SUBORDINADOS
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasSubordinados() ) {
            // Questao questao = questaoRespostas.getQuestao();

            boolean isPropria = false, isLideres = false, isSubordinados = false, isOutros = false;
            isSubordinados = true;
            gerarNota( questaoRespostas, questaoRespostas.getQuestao(), isPropria, isLideres, isSubordinados, isOutros );
            calcularMetaQuestao(
                questaoRespostasAgrupadasPorAvaliadores,
                questaoRespostas.getQuestao(),
                pesquisa,
                questaoRespostas.getQuestionario(),
                questaoRespostas.getFuncionarioAvaliado() );

            // questaoRespostas.setQuestao(questao);
        }
        // RESPOSTAS OUTROS
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasOutros() ) {
            // Questao questao = questaoRespostas.getQuestao();

            boolean isPropria = false, isLideres = false, isSubordinados = false, isOutros = false;
            isOutros = true;
            gerarNota( questaoRespostas, questaoRespostas.getQuestao(), isPropria, isLideres, isSubordinados, isOutros );
            calcularMetaQuestao(
                questaoRespostasAgrupadasPorAvaliadores,
                questaoRespostas.getQuestao(),
                pesquisa,
                questaoRespostas.getQuestionario(),
                questaoRespostas.getFuncionarioAvaliado() );

            // questaoRespostas.setQuestao(questao);
        }

        calcularMediaDaQuestao( questaoRespostasAgrupadasPorAvaliadores );

        return questaoRespostasAgrupadasPorAvaliadores;
    }


    private void calcularMediaDaQuestao( QuestaoRespostasAgrupadasPorAvaliadores questaoRespostasAgrupadasPorAvaliadores ) {

        // RESPOSTAS PROPRIA
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasPropria() ) {
            makeCalcularMediaDaQuestao( questaoRespostasAgrupadasPorAvaliadores, questaoRespostas.getQuestao() );
        }
        // RESPOSTAS LIDERES
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasLideres() ) {
            makeCalcularMediaDaQuestao( questaoRespostasAgrupadasPorAvaliadores, questaoRespostas.getQuestao() );
        }
        // RESPOSTAS SUBORDINADOS
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasSubordinados() ) {
            makeCalcularMediaDaQuestao( questaoRespostasAgrupadasPorAvaliadores, questaoRespostas.getQuestao() );
        }
        // RESPOSTAS OUTROS
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasOutros() ) {
            makeCalcularMediaDaQuestao( questaoRespostasAgrupadasPorAvaliadores, questaoRespostas.getQuestao() );
        }

    }


    private void makeCalcularMediaDaQuestao( QuestaoRespostasAgrupadasPorAvaliadores questaoRespostasAgrupadasPorAvaliadores, Questao questao ) {

        Double media = null;
        int totalAvaliadores = 0;

        // RESPOSTAS PROPRIA
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasPropria() ) {
            if ( questaoRespostas.getQuestao().getId().equals( questao.getId() ) ) {
                if ( questaoRespostas.getMedia() != null ) {
                    if ( media == null ) {
                        media = 0D;
                    }
                    // A MEDIA AQUI NA VERDADE É A NOTA
                    media += questaoRespostas.getMedia();
                    totalAvaliadores++;
                }
                break;
            }
        }
        // RESPOSTAS LIDERES
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasLideres() ) {
            if ( questaoRespostas.getQuestao().getId().equals( questao.getId() ) ) {
                if ( questaoRespostas.getMedia() != null ) {
                    if ( media == null ) {
                        media = 0D;
                    }
                    // A MEDIA AQUI NA VERDADE É A NOTA
                    media += questaoRespostas.getMedia();
                    totalAvaliadores++;
                }
                break;
            }
        }
        // RESPOSTAS SUBORDINADOS
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasSubordinados() ) {
            if ( questaoRespostas.getQuestao().getId().equals( questao.getId() ) ) {
                if ( questaoRespostas.getMedia() != null ) {
                    if ( media == null ) {
                        media = 0D;
                    }
                    // A MEDIA AQUI NA VERDADE É A NOTA
                    media += questaoRespostas.getMedia();
                    totalAvaliadores++;
                }
                break;
            }
        }
        // RESPOSTAS OUTROS
        for ( QuestaoRespostas questaoRespostas : questaoRespostasAgrupadasPorAvaliadores.getRespostasOutros() ) {
            if ( questaoRespostas.getQuestao().getId().equals( questao.getId() ) ) {
                if ( questaoRespostas.getMedia() != null ) {
                    if ( media == null ) {
                        media = 0D;
                    }
                    // A MEDIA AQUI NA VERDADE É A NOTA
                    media += questaoRespostas.getMedia();
                    totalAvaliadores++;
                }
                break;
            }
        }

        if ( media != null ) {
            // VERIFICAR, POIS ESTE questao.setMedia NAO ESTA SENDO USADO, AQUI O VALOR
            // PREENCHDIDO É O MESMO DO VALOR OBTIDO
            questao.setMedia( media / totalAvaliadores );
            questao.setValorObtido( questao.getMedia() );
            if ( notNull( questao.getMeta() ) ) {

                if ( questao.getNegativo() ) {
                    questao.setPercentualMeta( questao.getMeta() / questao.getValorObtido() );
                } else {
                    questao.setPercentualMeta( questao.getValorObtido() / questao.getMeta() );
                }

            }
        }

    }


    private void calcularMetaQuestao(
        QuestaoRespostasAgrupadasPorAvaliadores questaoRespostasAgrupadasPorAvaliadores,
        Questao questao,
        Pesquisa pesquisa,
        Questionario questionario,
        Funcionario funcionarioAvaliado ) {

        if ( questaoRespostasAgrupadasPorAvaliadores != null && questao != null && pesquisa != null && questionario != null && funcionarioAvaliado != null ) {

            List< QuestaoMeta > metasQuestao = new ArrayList<>();

            // GRUPO OCUPACIONAL
            if ( funcionarioAvaliado.getCargo() != null && funcionarioAvaliado.getCargo().getGrupoOcupacional() != null
                && CollectionUtils.isNotEmpty( questao.getQuestoesMetasGruposOcupacionais() ) ) {
                for ( QuestaoMeta questaoMeta : questao.getQuestoesMetasGruposOcupacionais() ) {
                    if ( questaoMeta.getIdGrupoOcupacional().equals( funcionarioAvaliado.getCargo().getGrupoOcupacional().getId() ) ) {
                        metasQuestao.add( questaoMeta );
                        break;
                    }
                }
            }

            // CARGO
            if ( funcionarioAvaliado.getCargo() != null && CollectionUtils.isNotEmpty( questao.getQuestoesMetasCargos() ) ) {
                for ( QuestaoMeta questaoMeta : questao.getQuestoesMetasCargos() ) {
                    if ( questaoMeta.getIdCargo().equals( funcionarioAvaliado.getCargo().getId() ) ) {
                        metasQuestao.add( questaoMeta );
                        break;
                    }
                }
            }
            // SETOR
            if ( funcionarioAvaliado.getSetor() != null && CollectionUtils.isNotEmpty( questao.getQuestoesMetasSetores() ) ) {
                for ( QuestaoMeta questaoMeta : questao.getQuestoesMetasSetores() ) {
                    if ( questaoMeta.getIdSetor().equals( funcionarioAvaliado.getSetor().getId() ) ) {
                        metasQuestao.add( questaoMeta );
                        break;
                    }
                }
            }

            if ( !metasQuestao.isEmpty() ) {
                Double meta = 0D;
                boolean negativo = false;
                for ( QuestaoMeta questaoMeta : metasQuestao ) {
                    if ( questaoMeta != null && notNull( questaoMeta.getMeta() ) ) {
                        meta += questaoMeta.getMeta();
                        negativo = questaoMeta.getNegativo();
                    }
                }
                if ( !meta.equals( 0D ) ) {
                    questao.setMeta( meta / metasQuestao.size() );
                    questao.setNegativo( negativo );
                }

            }

        }

    }


    /**
     * Agrupa as respostas por questoes (QuestaoRespostas) e realiza o calculo de
     * media das questoes do tipo Lista de Valores (escala)
     *
     * @param respostas
     * @return lista de questões, cada uma com suas respostas
     */
    private List< QuestaoRespostas > gerarQuestoesRespostas( List< Resposta > respostas )
        throws ServiceException {

        List< QuestaoRespostas > questoesRespostas = new ArrayList<>();

        if ( respostas != null ) {

            for ( Resposta resposta : respostas ) {

                boolean hasIncluded = false;
                for ( QuestaoRespostas questResp : questoesRespostas ) {

                    boolean sameQuestao = questResp.getQuestao().getId().equals( resposta.getQuestao().getId() );
                    boolean sameQuestionario = questResp.getQuestionario().getId().equals( resposta.getQuestionarioAvaliacao().getQuestionario().getId() );
                    boolean sameGrupo = true;
                    if ( resposta.getIdGrupoQuestao() != null ) {
                        sameGrupo = resposta.getIdGrupoQuestao().equals( questResp.getIdGrupoQuestao() );
                    }

                    if ( sameQuestao && sameQuestionario && sameGrupo ) {

                        // ADICIONANDO AS RESPOSTAS
                        questResp.getRespostas().add( resposta );
                        // resposta.setQuestao(null);
                        // resposta.setQuestionarioAvaliacao(null);
                        hasIncluded = true;
                        break;

                    }
                }

                if ( !hasIncluded ) {

                    QuestaoRespostas questaoRespostas = new QuestaoRespostas();

                    Long idQuestao = resposta.getQuestao().getId();
                    Long idQuestionario = resposta.getQuestionarioAvaliacao().getQuestionario().getId();
                    Long idPesquisa = resposta.getQuestionarioAvaliacao().getPesquisa().getId();

                    Questao questao = questaoMetaService.findMetasQuestao( idQuestao, idQuestionario, idPesquisa, resposta.getIdGrupoQuestao() );
                    Questao newQuestao = new Questao();
                    BeanUtils.copyProperties( questao, newQuestao );

                    questaoRespostas.setQuestao( newQuestao );
                    questaoRespostas.setIdGrupoQuestao( resposta.getIdGrupoQuestao() );
                    questaoRespostas.getRespostas().add( resposta );
                    questaoRespostas.setQuestionario( resposta.getQuestionarioAvaliacao().getQuestionario() );
                    questaoRespostas.setFuncionarioAvaliado( resposta.getQuestionarioAvaliacao().getFuncionarioAvaliado() );

                    // questaoRespostas.setTotalQuestoesDoGrupo(
                    // grupoQuestaoDAO.totalQuestoesDoGrupo( questaoRespostas.getIdGrupoQuestao() )
                    // );

                    questoesRespostas.add( questaoRespostas );
                }
            }

        }

        // return questoesRespostas;
        return calcularMediaQuestoesTipoListaValores( questoesRespostas );
    }


    /**
     * Calcula a média nas questões do tipo lista de valores. Atributo media.
     *
     * @param questoesRespostas
     * @return a mesma lista passada, porém com a média calculada para cada questão
     *         do tipo lista valores.
     */
    private List< QuestaoRespostas > calcularMediaQuestoesTipoListaValores( List< QuestaoRespostas > questoesRespostas ) {
        // List<QuestaoRespostas> questoesRespostasCalculada = new
        // ArrayList<>();

        if ( questoesRespostas != null ) {
            for ( QuestaoRespostas questaoRespostas : questoesRespostas ) {

                if ( questaoRespostas.getQuestao() != null
                    && ( questaoRespostas.getQuestao().getIsLista() || questaoRespostas.getQuestao().getIsNumerico() ) ) {

                    Double somaNotas = 0D;
                    Double qtdeNotas = 0D;

                    // Set<ValorLista> valoresLista = new HashSet<>();
                    if ( questaoRespostas.getRespostas() != null ) {
                        for ( Resposta resposta : questaoRespostas.getRespostas() ) {

                            try {

                                if ( resposta.getRespostaValorLista() != null && resposta.getRespostaValorLista().getValor() != null ) {

                                    String valor = resposta.getRespostaValorLista().getValor();
                                    Double nota = Double.valueOf( valor );
                                    somaNotas = somaNotas + nota;
                                    qtdeNotas++;
                                    // valoresLista.add(resposta.getRespostaValorLista());

                                } else if ( resposta.getRespostaNumerico() != null ) {
                                    Double nota = resposta.getRespostaNumerico();
                                    somaNotas = somaNotas + nota;
                                    qtdeNotas++;
                                }

                            } catch ( NumberFormatException e ) {
                                log.error( "NumberFormatException ERRO AO CALCULAR MEDIA: :" + resposta );
                                continue;
                            }
                        }
                    }

                    if ( ( somaNotas != null && somaNotas != 0D ) && ( qtdeNotas != null && qtdeNotas != 0D ) ) {
                        questaoRespostas.setMedia( somaNotas / qtdeNotas );
                    } else {
                        questaoRespostas.setMedia( null );
                    }

                }

                // questoesRespostasCalculada.add(questaoRespostas);

            }
        }

        // return questoesRespostasCalculada;
        return questoesRespostas;
    }


    private boolean notNull( Double number ) {

        if ( number != null && !Double.isNaN( number ) ) {
            return true;
        }
        return false;
    }

}
