package com.illuminare.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.RoleDAO;
import com.illuminare.entity.Role;
import com.illuminare.service.RoleService;
import com.illuminare.service.UserService;


@Service
public class RoleServiceImpl extends GenericServiceImpl< Role > implements RoleService {

    @Autowired
    RoleDAO dao;

    @Autowired
    UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    @Transactional( readOnly = true )
    public Role findByName( String name ) {

        return dao.findByName( name );
    }


    @Override
    public GenericDAO< Role > getDao() {

        return dao;
    }

}
