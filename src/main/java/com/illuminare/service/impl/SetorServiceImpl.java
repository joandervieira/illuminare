package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.SetorDAO;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Setor;
import com.illuminare.entity.User;
import com.illuminare.enums.RoleEnum;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.FuncionarioService;
import com.illuminare.service.SetorService;
import com.illuminare.service.UserService;
import com.illuminare.view.Paginator;


@Service
public class SetorServiceImpl extends GenericServiceImpl< Setor > implements SetorService {

    @Autowired
    SetorDAO dao;
    @Autowired
    FuncionarioService funcionarioService;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    private UserService userService;

    public UserService getUserService() {

        return this.userService;
    }

    @Override
    public GenericDAO< Setor > getDao() {

        return dao;
    }


    @Transactional( readOnly = true )
    @Override
    public Paginator< Setor > pesquisar( Paginator< Setor > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< Setor >();
        }
        if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }

        User user = userService.findByName( usuarioLogado );
        Funcionario userFuncionario = user.getFuncionario();
        if ( userFuncionario != null ) {
            user.setFuncionario( dao.unproxyInitialize( user.getFuncionario() ) );
            userFuncionario = user.getFuncionario();
        }
        if ( !userService.isAdmin( user ) && ( paginator.getEntity() == null || paginator.getEntity().getIdEmpresa() == null ) ) {
            throw new ServiceException( "Empresa é obrigatório" );
        }

        if ( user.containsRole( RoleEnum.GESTOR_SETOR.getName() ) ) {
            // pegar setor do usuario logado (do objeto funcionario)
            if ( userFuncionario == null ) {
                throw new ServiceException( "Usuário não possuí funcionário associado." );
            }
            if ( userFuncionario.getSetor() == null ) {
                throw new ServiceException( "Usuário não possuí setor associado." );
            }
            paginator.getEntity().setId( userFuncionario.getSetor().getId() );
        }

        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {

            List< Setor > newResult = new ArrayList<>();

            for ( Setor entity : paginator.getResult() ) {
                if ( entity.getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getIdEmpresa() );
                    entity.setEmpresa( empresa );
                }
                newResult.add( dao.unproxyInitialize( entity ) );
            }

            paginator.setResult( newResult );

        }

        return paginator;
    }


    private Map< String, Object > mapFields( Setor entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNome() != null ) {
                fields.put( "nome", entity.getNome() );
            }
            if ( entity.getDescricao() != null ) {
                fields.put( "descricao", entity.getDescricao() );
            }
            if ( entity.getObs() != null ) {
                fields.put( "obs", entity.getObs() );
            }
        }

        return fields;
    }


    @Override
    @Transactional( readOnly = true )
    public List< Setor > findByNome( Long idEmpresa, String nome )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "nome", nome );
        if ( idEmpresa != null ) {
            attrs.put( "idEmpresa", idEmpresa );
        }
        return dao.findByAttr( attrs );
    }


    @Override
    @Transactional
    public void deleteLogic( Long id, String usuarioAtualizacao )
        throws ServiceException {

        Map< String, Object > attrs = new HashMap<>();
        Setor entity = new Setor();
        entity.setId( id );
        attrs.put( "setor", entity );
        List< Funcionario > entities = funcionarioService.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( entities ) ) {
            for ( Funcionario e : entities ) {
                if ( e.getExcluido() == null || !e.getExcluido() ) {
                    throw new ServiceException( "Impossível Excluir. Setor está associado a um funcionário" );
                }
            }
        }
        super.deleteLogic( id, usuarioAtualizacao );
    }

}
