package com.illuminare.service.impl;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.TipoEnderecoEletronicoDAO;
import com.illuminare.entity.TipoEnderecoEletronico;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.TipoEnderecoEletronicoService;
import com.illuminare.service.UserService;
import com.illuminare.util.Constantes;


@Service
public class TipoEnderecoEletronicoServiceImpl extends GenericServiceImpl< TipoEnderecoEletronico > implements TipoEnderecoEletronicoService {

    @Autowired
    TipoEnderecoEletronicoDAO dao;

    @Autowired
    private UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< TipoEnderecoEletronico > getDao() {

        return dao;
    }


    @Override
    @Transactional
    public TipoEnderecoEletronico getTypeEmail()
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "tipo", "E-mail" );
        List< TipoEnderecoEletronico > types = dao.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( types ) ) {
            return types.get( 0 );
        } else {
            TipoEnderecoEletronico email = new TipoEnderecoEletronico();
            email.setTipo( "E-mail" );
            email.setAtivo( true );
            email.setDataCadastro( new Date() );
            email.setUsuarioCadastro( Constantes.USER_SYSTEM );
            return dao.save( email );
        }
    }
}
