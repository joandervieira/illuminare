package com.illuminare.service.impl;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.TipoEnderecoDAO;
import com.illuminare.entity.TipoEndereco;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.TipoEnderecoService;
import com.illuminare.service.UserService;
import com.illuminare.util.Constantes;


@Service
public class TipoEnderecoServiceImpl extends GenericServiceImpl< TipoEndereco > implements TipoEnderecoService {

    @Autowired
    TipoEnderecoDAO dao;

    @Autowired
    UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< TipoEndereco > getDao() {

        return dao;
    }


    @Override
    @Transactional
    public TipoEndereco getResidencial()
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "tipo", "Residencial" );
        List< TipoEndereco > types = dao.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( types ) ) {
            return types.get( 0 );
        } else {
            TipoEndereco tipo = new TipoEndereco();
            tipo.setTipo( "Residencial" );
            tipo.setAtivo( true );
            tipo.setDataCadastro( new Date() );
            tipo.setUsuarioCadastro( Constantes.USER_SYSTEM );
            return dao.save( tipo );
        }
    }

}
