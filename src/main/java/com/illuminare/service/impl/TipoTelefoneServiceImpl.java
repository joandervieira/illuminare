package com.illuminare.service.impl;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.TipoTelefoneDAO;
import com.illuminare.entity.TipoTelefone;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.service.TipoTelefoneService;
import com.illuminare.service.UserService;
import com.illuminare.util.Constantes;


@Service
public class TipoTelefoneServiceImpl extends GenericServiceImpl< TipoTelefone > implements TipoTelefoneService {

    @Autowired
    TipoTelefoneDAO dao;

    @Autowired
    private UserService userService;


    public UserService getUserService() {

        return this.userService;
    }


    @Override
    public GenericDAO< TipoTelefone > getDao() {

        return dao;
    }


    @Override
    @Transactional
    public TipoTelefone getResidencial()
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "tipo", "Residencial" );
        List< TipoTelefone > types = dao.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( types ) ) {
            return types.get( 0 );
        } else {
            TipoTelefone tipo = new TipoTelefone();
            tipo.setTipo( "Residencial" );
            tipo.setAtivo( true );
            tipo.setDataCadastro( new Date() );
            tipo.setUsuarioCadastro( Constantes.USER_SYSTEM );
            return dao.save( tipo );
        }
    }


    @Override
    @Transactional
    public TipoTelefone getPessoal()
        throws ServiceException {

        Map< String, Object > attrs = new HashMap< String, Object >();
        attrs.put( "tipo", "Pessoal" );
        List< TipoTelefone > types = dao.findByAttr( attrs );
        if ( CollectionUtils.isNotEmpty( types ) ) {
            return types.get( 0 );
        } else {
            TipoTelefone tipo = new TipoTelefone();
            tipo.setTipo( "Pessoal" );
            tipo.setAtivo( true );
            tipo.setDataCadastro( new Date() );
            tipo.setUsuarioCadastro( Constantes.USER_SYSTEM );
            return dao.save( tipo );
        }
    }

}
