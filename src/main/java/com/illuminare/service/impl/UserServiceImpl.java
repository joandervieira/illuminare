package com.illuminare.service.impl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.illuminare.dao.GenericDAO;
import com.illuminare.dao.UserDAO;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.EnderecoEletronico;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.Role;
import com.illuminare.entity.User;
import com.illuminare.enums.RoleEnum;
import com.illuminare.enums.TipoEnderecoEletronico;
import com.illuminare.enums.TipoUsuario;
import com.illuminare.exceptions.EmailException;
import com.illuminare.exceptions.EntityNotFoundException;
import com.illuminare.exceptions.ServiceException;
import com.illuminare.mail.EmailUtil;
import com.illuminare.service.EmpresaService;
import com.illuminare.service.RoleService;
import com.illuminare.service.UserService;
import com.illuminare.util.Constantes;
import com.illuminare.util.PasswordUtils;
import com.illuminare.view.Paginator;


@Service
public class UserServiceImpl extends GenericServiceImpl< User > implements UserService {

    Logger log = Logger.getLogger( UserServiceImpl.class );

    @Autowired
    UserDAO dao;
    @Autowired
    RoleService roleService;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    @Qualifier( "passwordEncoder" )
    private PasswordEncoder passwordEncoder;

    public UserService getUserService() {

        return this;
    }

    @Override
    public GenericDAO< User > getDao() {

        return dao;
    }


    @Override
    public Paginator< User > pesquisar( Paginator< User > paginator, String usuarioLogado )
        throws ServiceException {

        if ( paginator == null ) {
            paginator = new Paginator< User >();
        }
        // if ( !userService.isAdmin( usuarioLogado ) && ( paginator.getEntity()
        // == null || paginator.getEntity().getIdEmpresa() == null ) ) {
        // throw new ServiceException( "Empresa é obrigatório" );
        // }

        paginator.setFieldsToSearch( mapFields( paginator.getEntity() ) );

        if ( paginator.getFieldsToFetch() == null ) {
            paginator.setFieldsToFetch( new ArrayList< String >() );
        }

        paginator.getFieldsToFetch().add( "roles role" );

        paginator = dao.pesquisar( paginator );

        if ( paginator.getResult() != null ) {
            for ( User entity : paginator.getResult() ) {
                if ( entity.getFuncionario() != null && entity.getFuncionario().getIdEmpresa() != null ) {
                    Empresa empresa = empresaService.findById( entity.getFuncionario().getIdEmpresa() );
                    entity.getFuncionario().setEmpresa( empresa );
                }
            }

        }

        return paginator;
    }


    private Map< String, Object > mapFields( User entity ) {

        Map< String, Object > fields = new HashMap< String, Object >();

        if ( entity != null ) {
            if ( entity.getId() != null ) {
                fields.put( "id", entity.getId() );
            }
            if ( entity.getNomeExibir() != null ) {
                fields.put( "nomeExibir", entity.getNomeExibir() );
            }
            if ( entity.getName() != null ) {
                fields.put( "name", entity.getName() );
            }
            if ( entity.getFuncionario() != null ) {
                if ( entity.getFuncionario().getMatricula() != null ) {
                    fields.put( "[f].matricula", entity.getFuncionario().getMatricula() );
                }
            }
            if ( entity.getTipoUser() != null ) {

                String role = null;
                if ( entity.getTipoUser().equals( TipoUsuario.ADMIN.getTipo() ) ) {
                    role = Constantes.ROLE_ADMIN;

                } else if ( entity.getTipoUser().equals( TipoUsuario.GESTOR_RH.getTipo() ) ) {
                    role = Constantes.ROLE_GESTOR;

                } else if ( entity.getTipoUser().equals( TipoUsuario.GESTOR_SETOR.getTipo() ) ) {
                    role = RoleEnum.GESTOR_SETOR.getName();

                } else {
                    role = Constantes.ROLE_FUNCIONARIO;
                }

                fields.put( "[role].name in ", Arrays.asList( role ) );

            }
            if ( entity.getFuncionario() != null && entity.getFuncionario().getIdEmpresa() != null ) {
                fields.put( "[f].idEmpresa", entity.getFuncionario().getIdEmpresa() );
            }
        }

        return fields;
    }


    @Override
    @Transactional( readOnly = true )
    public UserDetails loadUserByUsername( String username )
        throws UsernameNotFoundException {

        return dao.loadUserByUsername( username );
    }


    @Override
    @Transactional( readOnly = true )
    public User findByName( String name )
        throws EntityNotFoundException {

        return findByName( name, null );
    }


    @Override
    @Transactional( readOnly = true )
    public User findByName( String name, List< String > fieldsToFetch )
        throws EntityNotFoundException {

        User user = dao.findByName( name, fieldsToFetch );
        if ( user == null ) {
            throw new EntityNotFoundException( "Usuário não encontrado" );
        }

        return user;
    }


    @Override
    @Transactional( readOnly = true )
    public List< User > listAll( List< String > fieldsToFetch ) {

        return dao.listAll( fieldsToFetch );
    }


    @Override
    @Transactional
    public User save( User entity, String usuarioCadastro )
        throws ServiceException {

        entity = validateSaveUpdate( entity, null );

        if ( dao.usuarioJaExiste( entity.getName() ) ) {
            throw new ServiceException( "Nome de usuário já existe" );
        }

        entity.setPassword( passwordEncoder.encode( entity.getPassword() ) );

        entity.setDataCadastro( new Date() );
        entity.setUsuarioCadastro( usuarioCadastro );
        entity.setAtivo( true );

        return dao.save( entity );

    }


    @Override
    @Transactional
    public void update( User entity, String usuarioAtualizacao )
        throws ServiceException {

        if ( entity != null && entity.getId() != null ) {

            if ( entity == null || entity.getId() == null ) {
                throw new ServiceException( "parametros nulo" );
            }

            User userBase = dao.findById( entity.getId() );

            entity = validateSaveUpdate( entity, userBase );

            if ( !userBase.getName().toUpperCase().equals( entity.getName().toUpperCase() ) ) {
                if ( dao.usuarioJaExiste( entity.getName() ) ) {
                    throw new ServiceException( "Nome de usuário já existe" );
                }
            }

            String pwDb = dao.getSenhaUsuario( entity.getId() );
            String newPw = entity.getPassword();

            if ( !pwDb.equals( newPw ) ) {
                entity.setPassword( passwordEncoder.encode( newPw ) );
            }

            entity.setDataAtualizacao( new Date() );
            entity.setUsuarioAtualizacao( usuarioAtualizacao );
            dao.update( entity );

        } else {
            throw new ServiceException( "parametros nulo" );
        }
    }


    private User validateSaveUpdate( User entity, User userDb )
        throws ServiceException {

        if ( entity == null ) {
            throw new ServiceException( "parametros nulo" );
        }

        if ( entity == null || StringUtils.isBlank( entity.getName() ) || StringUtils.isBlank( entity.getNomeExibir() )
            || StringUtils.isBlank( entity.getPassword() ) ) {
            throw new ServiceException( "Campos obrigatórios não preenchidos" );
        }

        if ( StringUtils.isBlank( entity.getTipoUser() ) ) {
            throw new ServiceException( "Tipo de usuário obrigatório" );
        }

        Role roleUser = roleService.findByName( Constantes.ROLE_USER );
        if ( entity.getTipoUser().equals( TipoUsuario.ADMIN.getTipo() ) ) {
            entity.setRoles( new HashSet<>( Arrays.asList( roleService.findByName( Constantes.ROLE_ADMIN ), roleUser ) ) );
            entity.setFuncionario( null );

        } else {
            // FUNCIONARIO
            if ( entity.getTipoUser().equals( TipoUsuario.FUNCIONARIO.getTipo() ) ) {
                if ( entity.getFuncionario() == null || entity.getFuncionario().getId() == null ) {
                    throw new ServiceException( "Campo Funcionário é obrigatório" );
                }
                User user = findByFuncionario( entity.getFuncionario().getId() );
                if ( user != null ) {
                    if ( entity.getId() != null ) {
                        if ( userDb.getFuncionario() == null || userDb.getFuncionario().getId() == null ) {
                            throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                        }
                        if ( userDb != null && userDb.getFuncionario() != null && !entity.getFuncionario().getId().equals( userDb.getFuncionario().getId() ) ) {
                            throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                        }
                    } else {
                        throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                    }
                }

                entity.setRoles( new HashSet<>( Arrays.asList( roleService.findByName( Constantes.ROLE_FUNCIONARIO ), roleUser ) ) );

            }
            // GESTOR
            else if ( entity.getTipoUser().equals( TipoUsuario.GESTOR_RH.getTipo() ) ) {
                if ( entity.getFuncionario() == null || entity.getFuncionario().getId() == null ) {
                    throw new ServiceException( "Campo Funcionário é obrigatório" );
                }
                User user = findByFuncionario( entity.getFuncionario().getId() );
                if ( user != null ) {
                    if ( entity.getId() != null ) {
                        if ( userDb.getFuncionario() == null || userDb.getFuncionario().getId() == null ) {
                            throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                        }
                        if ( userDb != null && userDb.getFuncionario() != null && !entity.getFuncionario().getId().equals( userDb.getFuncionario().getId() ) ) {
                            throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                        }
                    } else {
                        throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                    }
                }

                entity.setRoles( new HashSet<>( Arrays.asList( roleService.findByName( Constantes.ROLE_GESTOR ), roleUser ) ) );

            }
            // GESTOR SETOR
            else if ( entity.getTipoUser().equals( TipoUsuario.GESTOR_SETOR.getTipo() ) ) {
                if ( entity.getFuncionario() == null || entity.getFuncionario().getId() == null ) {
                    throw new ServiceException( "Campo Funcionário é obrigatório" );
                }
                User user = findByFuncionario( entity.getFuncionario().getId() );
                if ( user != null ) {
                    if ( entity.getId() != null ) {
                        if ( userDb.getFuncionario() == null || userDb.getFuncionario().getId() == null ) {
                            throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                        }
                        if ( userDb != null && userDb.getFuncionario() != null && !entity.getFuncionario().getId().equals( userDb.getFuncionario().getId() ) ) {
                            throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                        }
                    } else {
                        throw new ServiceException( "Este funcionário já possui um usuário. Login=" + user.getName() );
                    }
                }

                entity.setRoles( new HashSet<>( Arrays.asList( roleService.findByName( RoleEnum.GESTOR_SETOR.getName() ), roleUser ) ) );

            }
        }

        return entity;
    }


    @Override
    @Transactional
    public void atualizarSenha( User entity, String usuarioAtualizacao )
        throws ServiceException {

        if ( entity != null && entity.getId() != null && StringUtils.isNotBlank( entity.getPassword() ) ) {
            // String pwDb = dao.getSenhaUsuario(entity.getId());
            User userDb = dao.findById( entity.getId() );
            String pwDb = userDb.getPassword();
            String newPw = entity.getPassword();

            if ( !pwDb.equals( newPw ) ) {
                userDb.setPassword( passwordEncoder.encode( newPw ) );
            }
            if ( entity.getEmail() != null ) {
                userDb.setEmail( entity.getEmail() );
            }

            userDb.setSenhaAtualizada( true );

            userDb.setDataAtualizacao( new Date() );
            userDb.setUsuarioAtualizacao( usuarioAtualizacao );
            dao.update( userDb );

        } else {
            throw new ServiceException( "senha não pode ser vazia" );
        }
    }


    @Override
    @Transactional( readOnly = true )
    public List< User > listarAdministradores() {

        return dao.listarAdministradores();
    }


    @Override
    @Transactional
    public void salvarUltimoAcesso( String userName )
        throws ServiceException {

        User user = findByName( userName, null );
        user.setUltimoAcesso( new Date() );
        dao.update( user );
    }


    @Override
    // @Transactional(rollbackFor = Exception.class)
    @Transactional
    public void enviarEmailRecuperacao( User user )
        throws ServiceException {

        if ( user != null && ( StringUtils.isNotBlank( user.getName() ) || StringUtils.isNotBlank( user.getEmail() ) ) ) {
            if ( StringUtils.isNoneBlank( user.getName() ) ) {
                String userName = user.getUsername();
                user = findByName( userName, null );
                if ( user == null ) {
                    throw new ServiceException( "Usuário \"" + userName + "\" não cadastrado." );
                }
            } else {
                Map< String, Object > attrs = new HashMap< String, Object >();
                attrs.put( "email", user.getEmail() );
                List< User > list = findByAttr( attrs );
                if ( CollectionUtils.isNotEmpty( list ) ) {
                    user = list.get( 0 );
                } else {
                    throw new ServiceException( "Nenhum usuário cadastrado com o email: " + user.getEmail() );
                }
            }

            String senha = PasswordUtils.getRandomPassword( 4 );
            user.setPassword( passwordEncoder.encode( senha ) );
            user.setSenhaAtualizada( false );
            dao.update( user );

            if ( StringUtils.isBlank( user.getEmail() ) ) {
                throw new ServiceException( "Usuário \"" + user.getName()
                    + "\" não possuí email cadastrado. Por favor utilizar temporariamente a senha: <strong>" + senha + "</strong>" );
            }

            String html = "";
            html += "Olá, você solicitou a recuperação de senha de acesso ao sistema Illuminare <br><br>";
            html += "Segue sua nova senha temporária: <br>";
            html += "Senha: <strong>" + senha + "</strong><br>";
            html += "Usuário: <strong>" + user.getName() + "</strong><br>";
            html += "E-mail: <strong>" + user.getEmail() + "</strong><br>";
            EmailUtil emailUtil = new EmailUtil();
            try {
                emailUtil.enviarEmail( "Recuperação de Senha - Illuminare", html, Arrays.asList( user.getEmail() ) );
            } catch ( EmailException e ) {
                log.error( "Erro ao enviar email de recuperacao de senha" );
                log.error( "usuario:" + user );
                log.error( "Erro:" + e.getMessage() );
                log.error( e );
                throw new ServiceException( "Erro ao enviar email, por favor utilizar temporariamente a senha: <strong>" + senha + "</strong>" );
            }

        } else {
            throw new ServiceException( "Usuário ou email obrigatório" );
        }

    }


    @Override
    @Transactional
    public void cadastrarUsuarioFuncionario( Funcionario funcionario, String usuarioCadastro )
        throws ServiceException {

        Assert.notNull( funcionario );
        Assert.notNull( funcionario.getId() );
        Assert.notNull( funcionario.getPessoa() );
        Assert.notNull( funcionario.getPessoa().getDocumento() );

        User userFunc = new User();
        userFunc.setFuncionario( funcionario );
        userFunc.setName( funcionario.getPessoa().getDocumento() );
        userFunc.setNomeExibir( funcionario.getPessoa().getNome() );
        userFunc.setExcluido( false );
        if ( funcionario.getPessoa().getEnderecosEletronicos() != null ) {
            for ( EnderecoEletronico endE : funcionario.getPessoa().getEnderecosEletronicos() ) {
                if ( endE.getTipoEnderecoEletronico() != null && endE.getTipoEnderecoEletronico().getTipo() != null
                    && endE.getTipoEnderecoEletronico().getTipo().equals( TipoEnderecoEletronico.EMAIL.getTipo() ) ) {
                    userFunc.setEmail( endE.getEndereco() );
                }
            }
        }

        userFunc.setTipoUser( TipoUsuario.FUNCIONARIO.getTipo() );
        userFunc.setSenhaAtualizada( false );
        userFunc.setPassword( funcionario.getPessoa().getDocumento() );

        save( userFunc, usuarioCadastro );
    }


    @Override
    @Transactional( readOnly = true )
    public User findByFuncionario( Long idFuncionario )
        throws ServiceException {

        Assert.notNull( idFuncionario );
        return dao.findByFuncionario( idFuncionario );
    }


    @Override
    public boolean isAdmin( String userName )
        throws EntityNotFoundException {

        Assert.notNull( userName );
        boolean isAdmin = false;
        User user = findByName( userName, Arrays.asList( "roles" ) );

        isAdmin = user.containsRole( Constantes.ROLE_ADMIN ) || user.containsRole( Constantes.ROLE_ROOT ) || userName.equals( Constantes.USER_SYSTEM );

        return isAdmin;
    }


    @Override
    public boolean isAdmin( User user ) {

        Assert.notNull( user );
        Assert.notNull( user.getRoles() );
        boolean isAdmin = false;

        isAdmin = user.containsRole( Constantes.ROLE_ADMIN ) || user.containsRole( Constantes.ROLE_ROOT ) || user.getName().equals( Constantes.USER_SYSTEM );

        return isAdmin;
    }


    @Override
    public boolean isGestor( String userName )
        throws EntityNotFoundException {

        Assert.notNull( userName );
        boolean haveRole = false;
        User user = findByName( userName, Arrays.asList( "roles" ) );

        haveRole = user.containsRole( RoleEnum.GESTOR_RH.getName() ) || user.containsRole( RoleEnum.GESTOR_SETOR.getName() );

        return haveRole;
    }


    @Override
    public boolean isGestor( User user ) {

        Assert.notNull( user );
        Assert.notNull( user.getRoles() );
        boolean haveRole = false;

        haveRole = user.containsRole( RoleEnum.GESTOR_RH.getName() ) || user.containsRole( RoleEnum.GESTOR_SETOR.getName() );

        return haveRole;
    }

}
