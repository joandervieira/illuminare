package com.illuminare.util;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;


public class AppStringUtils {

    public static void main( String[] args ) {

        String n = "MEU     Nome teSte     nº      TESTANDO                ";
        System.out.println( capitalize( n ) );
        System.out.println( capitalizeFully( n ) );
    }


    public static String capitalize( String st ) {

        if ( st != null ) {
            return StringUtils.capitalize( StringUtils.normalizeSpace( st.toLowerCase() ) );
        } else {
            return null;
        }
    }


    public static String capitalizeFully( String st ) {

        if ( st != null ) {
            return WordUtils.capitalizeFully( StringUtils.normalizeSpace( st ) );
        } else {
            return null;
        }
    }


    public static String normalizeSpace( String st ) {

        if ( st != null ) {
            return StringUtils.normalizeSpace( st );
        } else {
            return null;
        }
    }


    public static String somenteNumeros( String st ) {

        if ( st != null ) {
            return st.replaceAll( "[^0-9]", "" ).replaceAll( "[^a-zA-Z0-9.-]", "" );
        } else {
            return null;
        }
    }

}
