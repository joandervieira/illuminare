package com.illuminare.util;


public class Constantes {

    public static final String USER_ROOT = "root";

    public static final String USER_USER = "user";

    public static final String USER_SYSTEM = "system";

    public static final String TIPO_USER_ADMIN = "admin";

    public static final String TIPO_USER_FUNCIONARIO = "funcionario";

    public static final String ROLE_ROOT = "role_root";

    public static final String ROLE_USER = "role_user";

    public static final String ROLE_ADMIN = "role_admin";

    public static final String ROLE_FUNCIONARIO = "role_funcionario";

    public static final String ROLE_GESTOR = "role_gestor";

    public static final String EMAIL_SISTEMA = "sistemailluminare@gmail.com";

    public static final String SENHA_EMAIL_SISTEMA = "root@2015";

    public static final String EMAIL_FROM_TITLE = "Sistema Illuminare";

    public static final String APP_PROPERTY_INIT_DATA_BASE = "INIT_DATA_BASE";

    public static final String TIPO_PESSOA_PF = "PF";

    public static final String TIPO_PESSOA_PJ = "PJ";
}
