package com.illuminare.util;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.GrupoQuestao;
import com.illuminare.entity.Pesquisa;
import com.illuminare.entity.Pessoa;
import com.illuminare.entity.Questao;
import com.illuminare.entity.QuestaoMeta;
import com.illuminare.entity.QuestaoOrdem;
import com.illuminare.entity.Questionario;
import com.illuminare.entity.Setor;
import com.illuminare.view.PesquisaResultView;


public class CopyEntityUtil {

    public static PesquisaResultView copyPesquisa( PesquisaResultView pesquisa ) {

        if ( pesquisa != null ) {
            PesquisaResultView newPesquisa = new PesquisaResultView();

            newPesquisa.setId( pesquisa.getId() );
            newPesquisa.setNome( pesquisa.getNome() );

            return newPesquisa;
        }

        return null;
    }


    public static Pesquisa copyPesquisa( Pesquisa pesquisa ) {

        if ( pesquisa != null ) {
            Pesquisa newPesquisa = new Pesquisa();

            newPesquisa.setId( pesquisa.getId() );
            newPesquisa.setNome( pesquisa.getNome() );

            return newPesquisa;
        }

        return null;
    }


    public static Funcionario copyAvaliado( Funcionario avaliado ) {

        Funcionario newAvaliado = new Funcionario();

        if ( avaliado != null ) {
            newAvaliado.setId( avaliado.getId() );
            newAvaliado.setCargo( copyCargo( avaliado.getCargo() ) );
            newAvaliado.setSetor( copySetor( avaliado.getSetor() ) );
            newAvaliado.setGrupoOcupacional( copyGrupoOcupacional( avaliado.getGrupoOcupacional() ) );
            newAvaliado.setMatricula( avaliado.getMatricula() );
            newAvaliado.setPessoa( copyPessoa( avaliado.getPessoa() ) );
        }

        return newAvaliado;
    }


    public static Set< Setor > copySetor( Set< Setor > entities ) {

        Set< Setor > newGrupoQuestao = new LinkedHashSet<>();

        for ( Setor entity : entities ) {
            newGrupoQuestao.add( copySetor( entity ) );
        }

        return newGrupoQuestao;
    }


    public static Setor copySetor( Setor obj ) {

        if ( obj != null ) {
            Setor newObj = new Setor();
            try {
                BeanUtils.copyProperties( newObj, obj );
            } catch (
                IllegalAccessException
                | InvocationTargetException e ) {
                e.printStackTrace();
            }
            return newObj;
        }
        return null;
    }


    public static Set< GrupoOcupacional > copyGrupoOcupacional( Set< GrupoOcupacional > entities ) {

        Set< GrupoOcupacional > newGrupoQuestao = new LinkedHashSet<>();

        for ( GrupoOcupacional entity : entities ) {
            newGrupoQuestao.add( copyGrupoOcupacional( entity ) );
        }

        return newGrupoQuestao;
    }


    public static GrupoOcupacional copyGrupoOcupacional( GrupoOcupacional obj ) {

        if ( obj != null ) {
            GrupoOcupacional newObj = new GrupoOcupacional();
            try {
                BeanUtils.copyProperties( newObj, obj );
            } catch (
                IllegalAccessException
                | InvocationTargetException e ) {
                e.printStackTrace();
            }
            return newObj;
        }
        return null;
    }


    public static Set< Cargo > copyCargo( Set< Cargo > entities ) {

        Set< Cargo > newGrupoQuestao = new LinkedHashSet<>();

        for ( Cargo entity : entities ) {
            newGrupoQuestao.add( copyCargo( entity ) );
        }

        return newGrupoQuestao;
    }


    public static Cargo copyCargo( Cargo obj ) {

        if ( obj != null ) {
            Cargo newObj = new Cargo();
            try {
                BeanUtils.copyProperties( newObj, obj );
                newObj.setGrupoOcupacional( null );
            } catch (
                IllegalAccessException
                | InvocationTargetException e ) {
                e.printStackTrace();
            }
            return newObj;
        }
        return null;
    }


    public static Pessoa copyPessoa( Pessoa pessoa ) {

        Pessoa newPessoa = new Pessoa();

        if ( pessoa != null ) {
            newPessoa.setId( pessoa.getId() );
            newPessoa.setDocumento( pessoa.getDocumento() );
            newPessoa.setNome( pessoa.getNome() );
            newPessoa.setSexo( pessoa.getSexo() );
            newPessoa.setTipoPessoa( pessoa.getTipoPessoa() );
            return newPessoa;
        }
        return null;

    }


    public static List< Questionario > copyQuestionario( List< Questionario > questionarios ) {

        List< Questionario > newQuestionarios = new ArrayList<>();

        for ( Questionario questionario : questionarios ) {
            newQuestionarios.add( copyQuestionario( questionario ) );
        }

        return newQuestionarios;
    }


    public static Set< Questionario > copyQuestionario( Set< Questionario > questionarios ) {

        Set< Questionario > newQuestionarios = new LinkedHashSet<>();

        for ( Questionario questionario : questionarios ) {
            newQuestionarios.add( copyQuestionario( questionario ) );
        }

        return newQuestionarios;
    }


    public static Questionario copyQuestionario( Questionario questionario ) {

        Questionario newQuestionario = new Questionario();

        newQuestionario.setId( questionario.getId() );
        newQuestionario.setNome( questionario.getNome() );
        newQuestionario.setObs( questionario.getObs() );
        newQuestionario.setGruposQuestoes( copyGrupoQuestao( questionario.getGruposQuestoes() ) );
        newQuestionario.setGruposOcupacionais( copyGrupoOcupacional( questionario.getGruposOcupacionais() ) );
        newQuestionario.setCargos( copyCargo( questionario.getCargos() ) );
        newQuestionario.setSetores( copySetor( questionario.getSetores() ) );
        newQuestionario.setMediaPropria( questionario.getMediaPropria() );
        newQuestionario.setMediaLideres( questionario.getMediaLideres() );
        newQuestionario.setMediaSubordinados( questionario.getMediaSubordinados() );
        newQuestionario.setMediaOutros( questionario.getMediaOutros() );
        newQuestionario.setPesquisaNome( questionario.getPesquisaNome() );
        newQuestionario.setPesquisaId( questionario.getPesquisaId() );
        newQuestionario.setAvaliadoNome( questionario.getAvaliadoNome() );
        newQuestionario.setAvaliadoId( questionario.getAvaliadoId() );
        newQuestionario.setAvaliadorNome( questionario.getAvaliadorNome() );
        newQuestionario.setAvaliadorId( questionario.getAvaliadorId() );
        newQuestionario.setMeta( questionario.getMeta() );
        newQuestionario.setValorObtido( questionario.getValorObtido() );
        newQuestionario.setPercentualMeta( questionario.getPercentualMeta() );

        return newQuestionario;
    }


    public static Questao copyQuestao( Questao questao ) {

        Questao newQuestao = new Questao();

        newQuestao.setId( questao.getId() );
        newQuestao.setNomeQuestao( questao.getNomeQuestao() );
        newQuestao.setPerguntaQuestao( questao.getPerguntaQuestao() );
        newQuestao.setIsTexto( questao.getIsTexto() );
        newQuestao.setIsLista( questao.getIsLista() );
        newQuestao.setIsNumerico( questao.getIsNumerico() );
        newQuestao.setListaValores( questao.getListaValores() );
        newQuestao.setNotaPropria( questao.getNotaPropria() );
        newQuestao.setNotaLideres( questao.getNotaLideres() );
        newQuestao.setNotaSubordinados( questao.getNotaSubordinados() );
        newQuestao.setNotaOutros( questao.getNotaOutros() );
        newQuestao.setResposta( questao.getResposta() );
        newQuestao.setRespostaTexto( questao.getRespostaTexto() );
        newQuestao.setRespostaNumerico( questao.getRespostaNumerico() );
        newQuestao.setRespostaValorLista( questao.getRespostaValorLista() );
        newQuestao.setRespostaId( questao.getRespostaId() );
        newQuestao.setQuestoesMetasGruposOcupacionais( copyQuestoesMetas( questao.getQuestoesMetasGruposOcupacionais() ) );
        newQuestao.setQuestoesMetasCargos( copyQuestoesMetas( questao.getQuestoesMetasCargos() ) );
        newQuestao.setQuestoesMetasSetores( copyQuestoesMetas( questao.getQuestoesMetasSetores() ) );
        newQuestao.setClassificacao( questao.getClassificacao() );
        newQuestao.setQuestaoClassificacao( questao.getQuestaoClassificacao() );
        newQuestao.setMeta( questao.getMeta() );
        newQuestao.setNegativo( questao.getNegativo() );
        newQuestao.setValorObtido( questao.getValorObtido() );
        newQuestao.setPercentualMeta( questao.getPercentualMeta() );
        newQuestao.setMedia( questao.getMedia() );

        return newQuestao;
    }


    public static List< QuestaoMeta > copyQuestoesMetas( List< QuestaoMeta > entities ) {

        List< QuestaoMeta > newGrupoQuestao = new ArrayList<>();

        for ( QuestaoMeta entity : entities ) {
            newGrupoQuestao.add( copyQuestoesMeta( entity ) );
        }

        return newGrupoQuestao;
    }


    public static QuestaoMeta copyQuestoesMeta( QuestaoMeta questaoMeta ) {

        QuestaoMeta newQuestaoMeta = new QuestaoMeta();

        try {
            BeanUtils.copyProperties( newQuestaoMeta, questaoMeta );
        } catch (
            IllegalAccessException
            | InvocationTargetException e ) {
            e.printStackTrace();
        }

        return newQuestaoMeta;
    }


    public static Set< GrupoQuestao > copyGrupoQuestao( Set< GrupoQuestao > entities ) {

        Set< GrupoQuestao > newGrupoQuestao = new LinkedHashSet<>();

        for ( GrupoQuestao entity : entities ) {
            newGrupoQuestao.add( copyGrupoQuestao( entity ) );
        }

        return newGrupoQuestao;
    }


    public static Set< QuestaoOrdem > copyQuestoesOrdem( Set< QuestaoOrdem > entities ) {

        Set< QuestaoOrdem > newGrupoQuestao = new LinkedHashSet<>();

        for ( QuestaoOrdem entity : entities ) {
            newGrupoQuestao.add( copyQuestaoOrdem( entity ) );
        }

        return newGrupoQuestao;
    }


    public static GrupoQuestao copyGrupoQuestao( GrupoQuestao grupoQuestao ) {

        GrupoQuestao newGrupoQuestao = new GrupoQuestao();

        newGrupoQuestao.setId( grupoQuestao.getId() );
        newGrupoQuestao.setNome( grupoQuestao.getNome() );
        newGrupoQuestao.setDescricao( grupoQuestao.getDescricao() );
        newGrupoQuestao.setQuestoesOrdem( copyQuestoesOrdem( grupoQuestao.getQuestoesOrdem() ) );
        newGrupoQuestao.setObs( grupoQuestao.getObs() );
        newGrupoQuestao.setMediaPropria( grupoQuestao.getMediaPropria() );
        newGrupoQuestao.setMediaLideres( grupoQuestao.getMediaLideres() );
        newGrupoQuestao.setMediaSubordinados( grupoQuestao.getMediaSubordinados() );
        newGrupoQuestao.setMediaOutros( grupoQuestao.getMediaOutros() );
        newGrupoQuestao.setMeta( grupoQuestao.getMeta() );
        newGrupoQuestao.setValorObtido( grupoQuestao.getValorObtido() );
        newGrupoQuestao.setPercentualMeta( grupoQuestao.getPercentualMeta() );

        return newGrupoQuestao;
    }


    public static QuestaoOrdem copyQuestaoOrdem( QuestaoOrdem questaoOrdem ) {

        QuestaoOrdem newQuestaoOrdem = new QuestaoOrdem();

        newQuestaoOrdem.setQuestao( copyQuestao( questaoOrdem.getQuestao() ) );
        newQuestaoOrdem.setOrdem( questaoOrdem.getOrdem() );

        return newQuestaoOrdem;
    }

}
