package com.illuminare.util;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.util.Assert;


public class DateUtil {

    private static Map< Integer, String > meses;

    static {
        meses = new HashMap< Integer, String >();
        meses.put( 1, "Janeiro" );
        meses.put( 2, "Fevereiro" );
        meses.put( 3, "Março" );
        meses.put( 4, "Abril" );
        meses.put( 5, "Maio" );
        meses.put( 6, "Junho" );
        meses.put( 7, "Julho" );
        meses.put( 8, "Agosto" );
        meses.put( 9, "Setembro" );
        meses.put( 10, "Outubro" );
        meses.put( 11, "Novembro" );
        meses.put( 12, "Dezembro" );
    }


    public static String getMesNome( int num ) {

        return meses.get( num + 1 );
    }


    public static Date getStartOfToday() {

        return new DateTime().withTimeAtStartOfDay().toDate();
    }


    public static boolean isBeforeOrEqualsToday( Date dateCompare ) {

        Assert.notNull( dateCompare );
        DateTime dta = new DateTime( dateCompare );
        return dta.isBefore( getStartOfToday().getTime() ) || dta.isEqual( getStartOfToday().getTime() );
    }


    public static boolean isAfterOrEqualsToday( Date dateCompare ) {

        Assert.notNull( dateCompare );
        DateTime dta = new DateTime( dateCompare );
        return dta.isAfter( getStartOfToday().getTime() ) || dta.isEqual( getStartOfToday().getTime() );
    }


    public static boolean isAfterToday( Date dateCompare ) {

        Assert.notNull( dateCompare );
        DateTime dta = new DateTime( dateCompare );
        return dta.isAfter( getStartOfToday().getTime() );
    }


    public static boolean isAfter( Date d1, Date d2 ) {

        Assert.notNull( d1, "Parameter D1 is required" );
        if ( d2 != null ) {
            return d1.after( d2 );
        } else {
            return true;
        }
    }


    public static void main( String[] args ) {

        Date d1 = new Date();
        Date d2 = null;
        System.out.println( isAfter( d1, d2 ) );
    }

}
