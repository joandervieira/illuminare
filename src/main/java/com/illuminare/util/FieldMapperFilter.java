package com.illuminare.util;


import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;


public class FieldMapperFilter implements ContainerResponseFilter {

    // private static final Logger LOG =
    // Logger.getLogger(FieldMapperFilter.class);

    public ContainerResponse filter( ContainerRequest request, ContainerResponse response ) {
        // After request processed

        // LOG.debug("##################################################################");
        // LOG.debug("BEFORE MAPPER ");
        // Set<Relatorio> relatorios =
        // ((Paginator)response.getEntity()).getResult();
        // for(Relatorio rel : relatorios){
        // System.out.println("#############: id:" + rel.getId() + " - data:" +
        // rel.getDataCelula());
        // }
        // LOG.debug("##################################################################");

        response.setEntity( ServiceMapper.map( response.getEntity() ) );

        // LOG.debug("##################################################################");
        // LOG.debug("AFTER MAPPER ");
        // Set<Relatorio> relatorios2 =
        // ((Paginator)response.getEntity()).getResult();
        // for(Relatorio rel : relatorios2){
        // System.out.println("#############: id:" + rel.getId() + " - data:" +
        // rel.getDataCelula());
        // }
        // LOG.debug("##################################################################");

        return response;
    }

}