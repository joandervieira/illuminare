package com.illuminare.util;


public class FormatUtil {

    public static String formatFone( String fone ) {

        String ddd, number, foneFormatado;
        fone = fone.replace( "(", "" ).replace( ")", "" ).replace( "-", "" ).replace( " ", "" );
        switch ( fone.length() ) {
            case 11:
                ddd = "(" + fone.substring( 0, 2 ) + ") ";
                number = ( fone.substring( 2, 7 ) + "-" + fone.substring( 7, 11 ) );
                foneFormatado = ddd + number;
                break;
            case 10:
                ddd = "(" + fone.substring( 0, 2 ) + ") ";
                number = ( fone.substring( 2, 6 ) + "-" + fone.substring( 6, 10 ) );
                foneFormatado = ddd + number;
                break;
            case 9:
                number = ( fone.substring( 0, 5 ) + "-" + fone.substring( 5, 9 ) );
                foneFormatado = number;
                break;
            case 8:
                number = ( fone.substring( 0, 4 ) + "-" + fone.substring( 5, 8 ) );
                foneFormatado = number;
                break;

            default:
                foneFormatado = fone;
                break;
        }

        return foneFormatado;
    }
}
