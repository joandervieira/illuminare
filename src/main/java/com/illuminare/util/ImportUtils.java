package com.illuminare.util;


import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.illuminare.entity.Cargo;
import com.illuminare.entity.Endereco;
import com.illuminare.entity.EnderecoEletronico;
import com.illuminare.entity.Funcionario;
import com.illuminare.entity.GrupoOcupacional;
import com.illuminare.entity.Lider;
import com.illuminare.entity.Pessoa;
import com.illuminare.entity.Setor;
import com.illuminare.entity.Telefone;
import com.illuminare.exceptions.ConvertFromXlsException;
import com.illuminare.file.CampoArquivoImport;
import com.illuminare.view.ImportFuncionarioView;


public class ImportUtils {

    private List< Funcionario > funcionarios;

    private List< String > msgsErro;

    private List< String > errosLinha;

    private Map< String, Integer > indexMatriculasLinhas;


    public ImportUtils() {

        msgsErro = new ArrayList<>();
        funcionarios = new ArrayList<>();
        indexMatriculasLinhas = new HashMap< String, Integer >();
    }


    public String getCNPJEmpresa( Workbook wb )
        throws ConvertFromXlsException {

        Sheet sheetSobre = wb.getSheetAt( 1 );
        if ( sheetSobre != null ) {

            Cell cellCNPJ = sheetSobre.getRow( 1 ).getCell( 0 );

            if ( cellCNPJ != null ) {
                String cnpj = cellCNPJ.getCellType() == Cell.CELL_TYPE_STRING ? cellCNPJ.getStringCellValue()
                    : BigDecimal.valueOf( cellCNPJ.getNumericCellValue() ).toString();
                if ( cnpj != null ) {
                    cnpj = StringUtils.deleteWhitespace( cnpj );
                    cnpj = cnpj.replace( ".", "" ).replace( "/", "" ).replace( "-", "" );
                    cnpj = cnpj.trim();
                    if ( cnpj.equals( "0" ) || cnpj.equals( "0.0" ) ) {
                        cnpj = null;
                    }
                }

                return cnpj;

            } else {
                msgsErro.add( "Formato do documento inválido" );
            }

        } else {
            msgsErro.add( "Formato do documento inválido" );
        }

        return null;

    }


    public ImportFuncionarioView xlxToFuncionario( InputStream inputStream )
        throws ConvertFromXlsException {

        try {

            ImportFuncionarioView importFuncionarioView = new ImportFuncionarioView();

            if ( inputStream == null ) {

                msgsErro.add( "Arquivo null" );

            } else {
                Workbook wb = null;
                try {
                    wb = WorkbookFactory.create( inputStream );
                } catch (
                    EncryptedDocumentException
                    | InvalidFormatException
                    | IOException e ) {
                    msgsErro.add( "Formato do documento inválido" );
                }

                if ( wb != null ) {
                    String cnpj = getCNPJEmpresa( wb );
                    importFuncionarioView.setCnpj( cnpj );
                    Sheet sheet = wb.getSheetAt( 1 );
                    if ( sheet != null ) {
                        for ( Row row : sheet ) {
                            String matricula, nome, cpf, email, sexo, fone, endLogradouro, endNumero, endCep, endBairro, endComplemento, endSiglaEstado,
                                endNomeCidade, nomeCargo, nomeGrupoOcupacional, nomeSetor, matriculaGestor;
                            if ( row.getRowNum() == 0 ) {
                                continue;
                            }

                            errosLinha = new ArrayList<>();
                            // matricula = getCampo( row, row.getRowNum(), CampoArquivoImport.MATRICULA,
                            // true );
                            // if ( matricula != null ) {
                            // matricula = NumberUtils.isNumber( matricula ) ? Double.valueOf( matricula
                            // ).intValue() + "" : matricula;
                            // }
                            matricula = getCellNumericAsString( row, CampoArquivoImport.MATRICULA );
                            nome = getCampo( row, row.getRowNum(), CampoArquivoImport.NOME, true );

                            // Considerado linha em branco
                            if ( StringUtils.isBlank( matricula ) && StringUtils.isBlank( nome ) ) {
                                errosLinha = new ArrayList<>();
                                continue;
                            }

                            cpf = getCampo( row, row.getRowNum(), CampoArquivoImport.CPF, true );
                            if ( cpf != null ) {
                                cpf = cpf.replace( ".", "" ).replace( "/", "" ).replace( "-", "" );
                            }
                            email = getCampo( row, row.getRowNum(), CampoArquivoImport.EMAIL, false );
                            sexo = getCampo( row, row.getRowNum(), CampoArquivoImport.SEXO, false );
                            fone = getCampo( row, row.getRowNum(), CampoArquivoImport.FONE, false );
                            endLogradouro = getCampo( row, row.getRowNum(), CampoArquivoImport.END_LOGRADOURO, false );
                            endNumero = getCampo( row, row.getRowNum(), CampoArquivoImport.END_NUMERO, false );
                            endCep = getCampo( row, row.getRowNum(), CampoArquivoImport.END_CEP, false );
                            endBairro = getCampo( row, row.getRowNum(), CampoArquivoImport.END_BAIRRO, false );
                            endComplemento = getCampo( row, row.getRowNum(), CampoArquivoImport.END_COMPLEMENTO, false );
                            endSiglaEstado = getCampo( row, row.getRowNum(), CampoArquivoImport.END_SIGLA_ESTADO, false );
                            endNomeCidade = getCampo( row, row.getRowNum(), CampoArquivoImport.END_NOME_CIDADE, false );
                            nomeCargo = getCampo( row, row.getRowNum(), CampoArquivoImport.NOME_CARGO, true );
                            nomeGrupoOcupacional = getCampo( row, row.getRowNum(), CampoArquivoImport.NOME_GRUPO_OCUPACIONAL, true );
                            nomeSetor = getCampo( row, row.getRowNum(), CampoArquivoImport.NOME_SETOR, false );

                            // matriculaGestor = getCampo( row, row.getRowNum(),
                            // CampoArquivoImport.MATRICULA_LIDER, false );
                            matriculaGestor = getCellNumericAsString( row, CampoArquivoImport.MATRICULA_LIDER );

                            // if ( matriculaGestor != null ) {
                            // matriculaGestor = NumberUtils.isNumber( matriculaGestor ) ? Double.valueOf(
                            // matriculaGestor ).intValue() + "" : matriculaGestor;
                            // }

                            if ( sexo != null ) {
                                if ( !sexo.equals( "M" ) && !sexo.equals( "F" ) ) {
                                    errosLinha.add( "Linha " + ( row.getRowNum() + 1 ) + " - formato inválido do campo sexo, utilizar \"F\" ou \"M\"" );
                                }
                            }

                            if ( matricula == null && nome == null && cpf == null && email == null && fone == null && endLogradouro == null && endNumero == null
                                && endCep == null && endBairro == null && endComplemento == null && endSiglaEstado == null && endNomeCidade == null ) {
                                continue;
                            }

                            if ( !errosLinha.isEmpty() ) {
                                msgsErro.addAll( errosLinha );
                            }

                            // CAMPOS OBRIGATORIOS, SO CRIA O FUNCIONARIO SE OS
                            // CAMPOS OBRIGATORIOS ESTIVEREM PREENCHIDOS
                            if ( matricula != null && cpf != null && nome != null && nomeCargo != null && nomeGrupoOcupacional != null ) {

                                criarFuncionario(
                                    row,
                                    matricula,
                                    nome,
                                    cpf,
                                    email,
                                    sexo,
                                    fone,
                                    endLogradouro,
                                    endNumero,
                                    endCep,
                                    endBairro,
                                    endComplemento,
                                    endSiglaEstado,
                                    endNomeCidade,
                                    nomeCargo,
                                    nomeGrupoOcupacional,
                                    nomeSetor,
                                    matriculaGestor );

                            }

                        }
                    } else {
                        msgsErro.add( "Formato do documento inválido" );
                    }

                }
            }

            importFuncionarioView.setFuncionarios( funcionarios );
            return importFuncionarioView;

        } catch ( Exception e ) {
            throw new ConvertFromXlsException( e.getMessage(), e );
        }

    }


    private String getCellNumericAsString( Row row, CampoArquivoImport campoArquivoImport ) {

        Cell cell = row.getCell( campoArquivoImport.getIndex() );
        if ( cell != null ) {

            if ( cell.getCellType() == Cell.CELL_TYPE_STRING ) {
                String value = cell.getStringCellValue();
                if ( StringUtils.isBlank( value ) || value.equals( "0" ) || value.equals( "0.0" ) ) {
                    return null;
                }
                try {
                    return String.valueOf( Double.valueOf( value ).intValue() );
                } catch ( Exception e ) {
                }
                return value;
            } else {

                BigDecimal value = BigDecimal.valueOf( cell.getNumericCellValue() );

                try {
                    if ( Double.valueOf( value.doubleValue() ).equals( 0D ) ) {
                        return null;
                    }
                } catch ( Exception e ) {
                }

                try {
                    return String.valueOf( value.intValue() );
                } catch ( Exception e ) {
                }

                return value.toString();
            }

        }

        return null;

    }


    private void criarFuncionario(
        Row row,
        String matricula,
        String nome,
        String cpf,
        String email,
        String sexo,
        String fone,
        String endLogradouro,
        String endNumero,
        String endCep,
        String endBairro,
        String endComplemento,
        String endSiglaEstado,
        String endNomeCidade,
        String nomeCargo,
        String nomeGrupoOcupacional,
        String nomeSetor,
        String matriculaGestor ) {

        indexMatriculasLinhas.put( matricula, row.getRowNum() );
        Funcionario func = new Funcionario();
        func.setAtivo( true );
        func.setExcluido( false );
        func = new Funcionario();
        Pessoa pes = new Pessoa();

        if ( matricula != null ) {
            matricula = matricula.equals( "0" ) ? null : matricula;
            func.setMatricula( matricula );
        }
        pes.setDocumento( cpf );
        pes.setNome( nome );
        pes.setSexo( sexo );

        if ( email != null ) {
            EnderecoEletronico endEletronico = new EnderecoEletronico();
            endEletronico.setEndereco( email );
            pes.setEnderecosEletronicos( new HashSet<>( Arrays.asList( endEletronico ) ) );
        }

        if ( endLogradouro != null ) {
            Endereco end = new Endereco();
            end.setLogradouro( endLogradouro );
            end.setNumero( endNumero );
            end.setCep( endCep );
            end.setBairro( endBairro );
            end.setComplemento( endComplemento );
            end.setSiglaEstado( endSiglaEstado );
            end.setNomeCidade( endNomeCidade );
            pes.setEnderecos( new HashSet<>( Arrays.asList( end ) ) );
        }

        if ( fone != null ) {
            Telefone tel = new Telefone();
            tel.setNumero( fone );
            pes.setTelefones( new HashSet<>( Arrays.asList( tel ) ) );
        }

        if ( nomeCargo != null ) {
            Cargo cargo = new Cargo();
            cargo.setNome( nomeCargo );
            func.setCargo( cargo );
        }

        if ( nomeGrupoOcupacional != null ) {
            GrupoOcupacional go = new GrupoOcupacional();
            go.setNome( nomeGrupoOcupacional );
            if ( func.getCargo() == null ) {
                func.setCargo( new Cargo() );
            }
            func.getCargo().setGrupoOcupacional( go );
        }

        if ( nomeSetor != null ) {
            Setor setor = new Setor();
            setor.setNome( nomeSetor );
            func.setSetor( setor );
        }

        if ( matriculaGestor != null ) {
            matriculaGestor = matriculaGestor.equals( "0" ) ? null : matriculaGestor;
        }
        if ( matriculaGestor != null ) {
            Lider lider = new Lider();
            lider.setMatricula( matriculaGestor );
            func.setLider( lider );
        }

        func.setPessoa( pes );

        funcionarios.add( func );

    }


    private String getCampo( Row row, Integer numRow, CampoArquivoImport campoArquivoImport, boolean obrigatorio ) {

        String campo = null;
        try {
            Cell cell = row.getCell( campoArquivoImport.getIndex() );
            if ( cell != null ) {
                campo = cell.getCellType() == Cell.CELL_TYPE_STRING ? cell.getStringCellValue() : String.valueOf( cell.getNumericCellValue() );
                // campo = StringUtils.deleteWhitespace(campo);
                if ( campo != null ) {
                    campo = campo.trim();
                    if ( campo.equals( "0" ) || campo.equals( "0.0" ) ) {
                        campo = null;
                    }
                }
                if ( StringUtils.isBlank( campo ) ) {
                    campo = null;
                }
            }
        } catch ( IllegalStateException e ) {
            errosLinha.add( "Linha " + ( numRow + 1 ) + " - formato inválido do campo " + campoArquivoImport.getNome() );
        }
        if ( obrigatorio && campo == null ) {
            errosLinha.add( "Linha " + ( numRow + 1 ) + " - Campo " + campoArquivoImport.getNome() + " não informado" );
        }

        return campo;
    }


    public List< Funcionario > getFuncionarios() {

        return funcionarios;
    }


    public void setFuncionarios( List< Funcionario > funcionarios ) {

        this.funcionarios = funcionarios;
    }


    public List< String > getMsgsErro() {

        return msgsErro;
    }


    public void setMsgsErro( List< String > msgsErro ) {

        this.msgsErro = msgsErro;
    }


    public Map< String, Integer > getIndexMatriculasLinhas() {

        return indexMatriculasLinhas;
    }


    public void setIndexMatriculasLinhas( Map< String, Integer > indexMatriculasLinhas ) {

        this.indexMatriculasLinhas = indexMatriculasLinhas;
    }

}
