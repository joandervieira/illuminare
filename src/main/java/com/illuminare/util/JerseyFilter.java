package com.illuminare.util;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.sun.jersey.api.container.ContainerException;
import com.sun.jersey.core.util.ReaderWriter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;


public class JerseyFilter implements ContainerRequestFilter {

    @Override
    public ContainerRequest filter( ContainerRequest request ) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = request.getEntityInputStream();
        final StringBuilder b = new StringBuilder();
        try {
            if ( in.available() > 0 ) {
                ReaderWriter.writeTo( in, out );

                byte[] requestEntity = out.toByteArray();
                printEntity( b, requestEntity );

                request.setEntityInputStream( new ByteArrayInputStream( requestEntity ) );
            }
            return request;
        } catch ( IOException ex ) {
            throw new ContainerException( ex );
        }

    }


    private void printEntity( StringBuilder b, byte[] entity )
        throws IOException {

        if ( entity.length == 0 )
            return;
        b.append( new String( entity ) ).append( "\n" );
        // System.out.println();
        // System.out.println();
        // System.out.println("################### INTERCEPTED ENTITY
        // #######################");
        // System.out.println(b.toString());
        // System.out.println("################### INTERCEPTED ENTITY FIM
        // #######################");
        // System.out.println();
        // System.out.println();
    }

}