package com.illuminare.util;


import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class NumberUtil {

    public static boolean notNull( Double number ) {

        if ( number != null && !Double.isNaN( number ) ) {
            return true;
        }
        return false;
    }

    public static boolean isNull( Double number ) {

        if ( number == null || Double.isNaN( number ) ) {
            return true;
        }
        return false;
    }


    public static boolean notNullNumber( String number ) {

        if ( number != null ) {
            Double d = getDouble( number );
            return notNull( d );
        }
        return false;
    }


    public static Double getDouble( String value ) {

        if ( isNotBlank( value ) ) {
            return Double.valueOf( value.replace( ",", "." ) );
        }
        return null;
    }

}
