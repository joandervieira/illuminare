package com.illuminare.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PasswordUtils {

    public static String getRandomPassword( int digits ) {

        if ( digits == 0 ) {
            digits = 4;
        }
        if ( digits > 9 ) {
            digits = 9;
        }
        List< Integer > numbers = new ArrayList< Integer >();
        for ( int i = 0; i < 10; i++ ) {
            numbers.add( i );
        }

        Collections.shuffle( numbers );

        String result = "";
        for ( int i = 0; i < digits; i++ ) {
            result += numbers.get( i ).toString();
        }

        return result;

    }

}
