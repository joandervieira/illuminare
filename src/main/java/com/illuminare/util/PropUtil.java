package com.illuminare.util;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;


public class PropUtil {

    public PropUtil() {

    }


    public static PropUtil getInstance() {

        return new PropUtil();
    }


    public String getProp( String key ) {

        Properties prop = new Properties();
        InputStream input = null;

        try {

            if ( StringUtils.isNotBlank( key ) ) {

                input = getClass().getClassLoader().getResourceAsStream( "app.properties" );

                // load a properties file
                prop.load( input );

                return prop.getProperty( key );

            }

        } catch ( IOException ex ) {
            ex.printStackTrace();
        } finally {
            if ( input != null ) {
                try {
                    input.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

}
