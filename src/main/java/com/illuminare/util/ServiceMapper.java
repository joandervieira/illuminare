package com.illuminare.util;


import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.text.WordUtils;
import org.hibernate.collection.internal.PersistentBag;
import org.hibernate.collection.internal.PersistentList;
import org.hibernate.collection.internal.PersistentSet;
import org.hibernate.proxy.HibernateProxy;

import javassist.util.proxy.Proxy;


public class ServiceMapper {

    // private static final Logger log = Logger.getLogger(ServiceMapper.class);

    @SuppressWarnings( "unchecked" )
    public static < T > T map( T obj ) {

        return (T) map( obj, new HashMap< String, Object >() );
    }


    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public static Object map( Object obj, Map< String, Object > mapp ) {

        if ( obj != null ) {

            if ( isListInstance( obj ) ) {

                List list = (List) obj;
                List returnList = new ArrayList();

                for ( Object o : list ) {

                    Object value = mapp.get( createKey( o ) );

                    if ( value == null ) {
                        Object b = map( o, mapp );
                        if ( b != null ) {
                            returnList.add( b );
                        }
                    } else {
                        returnList.add( value );
                    }
                }
                return returnList;

            } else if ( isSetInstance( obj ) ) {

                Set setList = (Set) obj;
                Set returnSet = new LinkedHashSet();

                for ( Object o : setList ) {

                    Object value = mapp.get( createKey( o ) );

                    if ( value == null ) {
                        Object b = map( o, mapp );
                        if ( b != null ) {
                            returnSet.add( b );
                        }
                    } else {
                        returnSet.add( value );
                    }

                }
                return returnSet;

            } else {

                if ( mapp.get( createKey( obj ) ) == null ) {

                    mapp.put( createKey( obj ), obj );

                    for ( Field field : getInheritedPrivateFields( obj.getClass() ) ) {

                        field.setAccessible( true );

                        try {
                            if ( obj.getClass().getMethod( createGetMethodName( field ), new Class[] {} ) == null ) {
                                continue;
                            }
                        } catch ( NoSuchMethodException ex ) {
                            continue;
                        }

                        try {

                            Object value = field.get( obj );
                            // value.getClass().getSuperclass()
                            // PersistentBag
                            if ( ( value != null ) && !isBasicType( value ) ) {

                                // log.debug( "field: " + field.getName() );

                                if ( value instanceof org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer ) {
                                    field.set( obj, null );
                                } else if ( ( value instanceof Proxy ) || ( value instanceof HibernateProxy ) ) {
                                    field.set( obj, null );
                                } else if ( value instanceof PersistentSet ) {

                                    value = map( obj, value, mapp );
                                    mapp.put( createKey( obj, value ), value );
                                    field.set( obj, value );

                                } else if ( ( value instanceof PersistentBag ) && ( !( (PersistentBag) value ).wasInitialized() ) ) {

                                    field.set( obj, null );

                                } else {

                                    Object value2 = mapp.get( createKey( obj, value ) );

                                    if ( value2 == null ) {
                                        value = map( obj, value, mapp );
                                        mapp.put( createKey( obj, value ), value );
                                        field.set( obj, value );
                                    } else {
                                        field.set( obj, value2 );
                                    }

                                }
                            }

                        } catch ( Exception e ) {
                            try {
                                field.set( obj, null );
                            } catch ( IllegalArgumentException e1 ) {
                                // do nothing
                            } catch ( IllegalAccessException e2 ) {
                                // do nothing
                            }
                        }

                    }
                    return obj;
                } else {
                    return mapp.get( createKey( obj ) );
                }

            }
        } else {
            return null;

        }

    }


    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public static Object map( Object parent, Object obj, Map< String, Object > mapp ) {

        if ( isListInstance( obj ) ) {

            List list = (List) obj;
            List returnList = new ArrayList();

            for ( Object o : list ) {

                Object value2 = mapp.get( createKey( parent, o ) );

                if ( value2 == null ) {

                    Object b = map( parent, o, mapp );
                    if ( b != null ) {
                        returnList.add( b );
                    }

                } else {
                    returnList.add( value2 );
                }

            }
            return returnList;

        } else if ( isSetInstance( obj ) ) {

            Set set = (Set) obj;
            Set returnSet = new LinkedHashSet();

            for ( Object o : set ) {

                Object value2 = mapp.get( createKey( parent, o ) );

                if ( value2 == null ) {

                    Object b = map( parent, o, mapp );
                    if ( b != null ) {
                        returnSet.add( b );
                    }

                } else {
                    returnSet.add( value2 );
                }

            }
            return returnSet;

        } else {

            Object obj1 = mapp.get( createKey( parent, obj ) );

            if ( parent != null && obj1 != null ) {

                return obj1;

            } else if ( parent != null && obj1 == null ) {

                mapp.put( createKey( parent, obj ), obj );

                for ( Field field : getInheritedPrivateFields( obj.getClass() ) ) {

                    field.setAccessible( true );

                    try {
                        if ( obj.getClass().getMethod( createGetMethodName( field ), new Class[] {} ) == null ) {
                            continue;
                        }
                    } catch ( NoSuchMethodException ex ) {
                        continue;
                    }

                    try {

                        Object value = field.get( obj );

                        if ( ( value != null ) && !isBasicType( value ) ) {

                            // log.debug( "field: " + field.getName() );

                            if ( ( value instanceof org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer )
                                || ( "org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer".equals( value.getClass().getName() ) ) ) {
                                field.set( obj, null );
                            } else if ( value instanceof PersistentSet ) {
                                value = map( value, mapp );
                                mapp.put( createKey( value ), value );
                                field.set( obj, value );

                            } else if ( ( value instanceof Proxy ) || ( value instanceof HibernateProxy ) ) {
                                field.set( obj, null );
                            } else {

                                Object value2 = mapp.get( createKey( obj, value ) );

                                if ( value2 == null ) {

                                    value = map( obj, value, mapp );
                                    mapp.put( createKey( obj, value ), value );
                                    field.set( obj, value );

                                } else {
                                    field.set( obj, value2 );
                                }

                            }
                        }
                    } catch ( Exception e ) {
                        try {
                            field.set( obj, null );
                        } catch ( IllegalArgumentException e1 ) {
                            // do nothing
                        } catch ( IllegalAccessException e2 ) {
                            // do nothing
                        }
                    }

                }
                return obj;

            } else if ( mapp.get( createKey( obj ) ) == null ) {

                mapp.put( createKey( obj ), obj );

                for ( Field field : getInheritedPrivateFields( obj.getClass() ) ) {

                    field.setAccessible( true );

                    try {
                        if ( obj.getClass().getMethod( createGetMethodName( field ), new Class[] {} ) == null ) {
                            continue;
                        }
                    } catch ( NoSuchMethodException ex ) {
                        continue;
                    }

                    try {

                        Object value = field.get( obj );

                        if ( ( value != null ) && !isBasicType( value ) ) {

                            // log.debug( "field: " + field.getName() );

                            if ( ( value instanceof org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer )
                                || ( "org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer".equals( value.getClass().getName() ) ) ) {
                                field.set( obj, null );
                            } else if ( value instanceof PersistentSet ) {
                                value = map( value, mapp );
                                mapp.put( createKey( value ), value );
                                field.set( obj, value );
                            } else if ( ( value instanceof Proxy ) || ( value instanceof HibernateProxy ) ) {
                                field.set( obj, null );
                            } else if ( mapp.get( createKey( value ) ) == null ) {
                                value = map( value, mapp );
                                mapp.put( createKey( value ), value );
                                field.set( obj, value );
                            } else {
                                field.set( obj, null );
                            }
                        }
                    } catch ( Exception e ) {
                        try {
                            field.set( obj, null );
                        } catch ( IllegalArgumentException e1 ) {
                            ; // do nothing
                        } catch ( IllegalAccessException e2 ) {
                            ; // do nothing
                        }
                    }

                }
                return obj;
            } else {
                return mapp.get( createKey( obj ) );
            }

        }

    }


    public static boolean isBasicType( Object value ) {

        return value instanceof String || value instanceof Long || value instanceof Boolean || value instanceof Short || value instanceof Integer
            || value instanceof BigDecimal || value instanceof BigInteger || value instanceof Byte || value instanceof Character || value instanceof Date
            || value instanceof Timestamp || value instanceof Float || value instanceof Double;
    }


    private static String createGetMethodName( Field field ) {

        return "get" + WordUtils.capitalize( field.getName() );
    }


    public static boolean isSetInstance( Object obj ) {

        return obj instanceof Set || obj instanceof LinkedHashSet || obj instanceof HashSet || obj instanceof PersistentSet;
    }


    public static boolean isListInstance( Object obj ) {

        return obj instanceof List || obj instanceof ArrayList || obj instanceof PersistentList;
    }


    private static String createKey( Object o ) {

        return o.hashCode() + "-" + o.getClass();
    }


    private static String createKey( Object parent, Object o ) {

        return parent.hashCode() + "-" + parent.getClass() + "-" + o.hashCode() + "-" + o.getClass();
    }


    public static List< Field > getInheritedPrivateFields( Class< ? > type ) {

        List< Field > result = new ArrayList< Field >();

        Class< ? > i = type;
        while ( i != null && i != Object.class ) {
            for ( Field field : i.getDeclaredFields() ) {
                if ( !field.isSynthetic() ) {
                    result.add( field );
                }
            }
            i = i.getSuperclass();
        }

        return result;
    }

}
