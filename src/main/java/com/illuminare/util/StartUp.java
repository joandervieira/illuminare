package com.illuminare.util;


import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.illuminare.dao.UserDAO;
import com.illuminare.entity.User;
import com.illuminare.exceptions.EntityNotFoundException;
import com.illuminare.service.UserService;


@Component
public class StartUp {

    Logger log = Logger.getLogger( StartUp.class );
    @Autowired
    UserDAO userDAO;
    @Autowired
    @Qualifier( "passwordEncoder" )
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;

    @Transactional
    public void init() {

        log.info( "init StartUp()" );

        try {

            String resetRootPassword = PropUtil.getInstance().getProp( "reset_root_password" );

            if ( "true".equals( resetRootPassword ) ) {

                log.info( "updating root password to root" );
                User root = userService.findByName( "root" );
                root.setPassword( passwordEncoder.encode( "root" ) );
                log.info( "pw=" + root.getPassword() );
                root.setDataAtualizacao( new Date() );
                userDAO.update( root );

            }

        } catch ( EntityNotFoundException e ) {
            log.error( e.getMessage(), e );
        }

        log.info( "end StartUp()" );
    }

}
