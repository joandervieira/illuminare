package com.illuminare.view;


public class AppError {

    private String msg = "";


    public AppError() {

    }


    public AppError( String msg ) {

        this.msg = msg;
    }


    public String getMsg() {

        if ( msg == null ) {
            msg = "";
        }
        return msg;
    }


    public void setMsg( String msg ) {

        this.msg = msg;
    }


    @Override
    public String toString() {

        return "Error [msg=" + msg + "]";
    }

}
