package com.illuminare.view;


import java.util.Date;
import java.util.Map;


public class CacheEntry {

    private Date date;

    private Map[] data;


    public CacheEntry( Map[] data ) {

        setDate( new Date() );
        setData( data );
    }


    public Date getDate() {

        return date;
    }


    public void setDate( Date date ) {

        this.date = date;
    }


    public Map[] getData() {

        return data;
    }


    public void setData( Map[] data ) {

        this.data = data;
    }
}
