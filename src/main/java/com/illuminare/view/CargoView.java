package com.illuminare.view;


public class CargoView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String descricaoSumaria;

    private String descricaoDetalhada;

    private Long cargaHoraria;

    private String obs;

    private GrupoOcupacionalView grupoOcupacional;


    public CargoView() {

    }


    public CargoView( String nome ) {

        setNome( nome );
    }


    public CargoView( Long id ) {

        setId( id );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDescricaoSumaria() {

        return descricaoSumaria;
    }


    public void setDescricaoSumaria( String descricaoSumaria ) {

        this.descricaoSumaria = descricaoSumaria;
    }


    public String getDescricaoDetalhada() {

        return descricaoDetalhada;
    }


    public void setDescricaoDetalhada( String descricaoDetalhada ) {

        this.descricaoDetalhada = descricaoDetalhada;
    }


    public Long getCargaHoraria() {

        return cargaHoraria;
    }


    public void setCargaHoraria( Long cargaHoraria ) {

        this.cargaHoraria = cargaHoraria;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public GrupoOcupacionalView getGrupoOcupacional() {

        return grupoOcupacional;
    }


    public void setGrupoOcupacional( GrupoOcupacionalView grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }


    @Override
    public String toString() {

        return "Cargo [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
