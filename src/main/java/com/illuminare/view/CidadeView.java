package com.illuminare.view;


public class CidadeView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private EstadoView estado;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public EstadoView getEstado() {

        return estado;
    }


    public void setEstado( EstadoView estado ) {

        this.estado = estado;
    }


    @Override
    public String toString() {

        return "Cidade [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
