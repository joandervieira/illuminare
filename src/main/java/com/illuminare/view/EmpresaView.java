package com.illuminare.view;


public class EmpresaView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String cnpj;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getCnpj() {

        return cnpj;
    }


    public void setCnpj( String cnpj ) {

        this.cnpj = cnpj;
    }
}
