package com.illuminare.view;


public class EnderecoEletronico {

    private Long id;

    private Long idEmpresa;

    private String endereco;

    private String obs;

    private TipoEnderecoEletronico tipoEnderecoEletronico;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getEndereco() {

        return endereco;
    }


    public void setEndereco( String endereco ) {

        this.endereco = endereco;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public TipoEnderecoEletronico getTipoEnderecoEletronico() {

        return tipoEnderecoEletronico;
    }


    public void setTipoEnderecoEletronico( TipoEnderecoEletronico tipoEnderecoEletronico ) {

        this.tipoEnderecoEletronico = tipoEnderecoEletronico;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( endereco == null ) ? 0 : endereco.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        EnderecoEletronico other = (EnderecoEletronico) obj;
        if ( endereco == null ) {
            if ( other.endereco != null )
                return false;
        } else if ( !endereco.equals( other.endereco ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "EnderecoEletronico [endereco=" + endereco + ", getId()=" + getId() + "]";
    }

}
