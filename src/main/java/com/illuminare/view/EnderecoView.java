package com.illuminare.view;


public class EnderecoView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String logradouro;

    private String numero;

    private String cep;

    private String bairro;

    private String complemento;

    private String referencia;

    private Boolean isPrincipal;

    private String obs;

    private CidadeView cidade;

    private String nomeCidade;

    private String siglaEstado;

    private TipoEnderecoView tipoEndereco;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getLogradouro() {

        return logradouro;
    }


    public void setLogradouro( String logradouro ) {

        this.logradouro = logradouro;
    }


    public String getNumero() {

        return numero;
    }


    public void setNumero( String numero ) {

        this.numero = numero;
    }


    public String getCep() {

        return cep;
    }


    public void setCep( String cep ) {

        this.cep = cep;
    }


    public String getBairro() {

        return bairro;
    }


    public void setBairro( String bairro ) {

        this.bairro = bairro;
    }


    public String getComplemento() {

        return complemento;
    }


    public void setComplemento( String complemento ) {

        this.complemento = complemento;
    }


    public String getReferencia() {

        return referencia;
    }


    public void setReferencia( String referencia ) {

        this.referencia = referencia;
    }


    public Boolean getIsPrincipal() {

        return isPrincipal;
    }


    public void setIsPrincipal( Boolean isPrincipal ) {

        this.isPrincipal = isPrincipal;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public CidadeView getCidade() {

        return cidade;
    }


    public void setCidade( CidadeView cidade ) {

        this.cidade = cidade;
    }


    public TipoEnderecoView getTipoEndereco() {

        return tipoEndereco;
    }


    public void setTipoEndereco( TipoEnderecoView tipoEndereco ) {

        this.tipoEndereco = tipoEndereco;
    }


    public String getNomeCidade() {

        return nomeCidade;
    }


    public void setNomeCidade( String nomeCidade ) {

        this.nomeCidade = nomeCidade;
    }


    public String getSiglaEstado() {

        return siglaEstado;
    }


    public void setSiglaEstado( String siglaEstado ) {

        this.siglaEstado = siglaEstado;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( numero == null ) ? 0 : numero.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        EnderecoView other = (EnderecoView) obj;
        if ( numero == null ) {
            if ( other.numero != null )
                return false;
        } else if ( !numero.equals( other.numero ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Endereco [logradouro=" + logradouro + ", getId()=" + getId() + "]";
    }

}
