package com.illuminare.view;


import com.illuminare.util.AppStringUtils;


public class EstadoCivilView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;


    public EstadoCivilView() {

    }


    public EstadoCivilView( String nome ) {

        setNome( nome );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = AppStringUtils.capitalizeFully( nome );
    }

}
