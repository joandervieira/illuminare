package com.illuminare.view;


public class EstadoView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String uf;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getUf() {

        return uf;
    }


    public void setUf( String uf ) {

        this.uf = uf;
    }


    @Override
    public String toString() {

        return "Estado [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
