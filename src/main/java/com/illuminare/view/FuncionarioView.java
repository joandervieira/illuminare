package com.illuminare.view;


public class FuncionarioView extends GenericEntityView {

    GrupoOcupacionalView grupoOcupacional;

    private Long id;

    private Long idEmpresa;

    private PessoaView pessoa;

    private CargoView cargo;

    private SetorView setor;

    private LiderView lider;

    private String matricula;


    public FuncionarioView() {

    }


    public FuncionarioView( Long id ) {

        setId( id );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public PessoaView getPessoa() {

        return pessoa;
    }


    public void setPessoa( PessoaView pessoa ) {

        this.pessoa = pessoa;
    }


    public CargoView getCargo() {

        return cargo;
    }


    public void setCargo( CargoView cargo ) {

        this.cargo = cargo;
    }


    public SetorView getSetor() {

        return setor;
    }


    public void setSetor( SetorView setor ) {

        this.setor = setor;
    }


    public GrupoOcupacionalView getGrupoOcupacional() {

        return grupoOcupacional;
    }


    public void setGrupoOcupacional( GrupoOcupacionalView grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }


    public LiderView getLider() {

        return lider;
    }


    public void setLider( LiderView lider ) {

        this.lider = lider;
    }


    public String getMatricula() {

        return matricula;
    }


    public void setMatricula( String matricula ) {

        this.matricula = matricula;
    }


    public void preencherGrupoOcupacional() {

        if ( getCargo() != null ) {
            setGrupoOcupacional( getCargo().getGrupoOcupacional() );
        }
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( matricula == null ) ? 0 : matricula.hashCode() );
        result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        FuncionarioView other = (FuncionarioView) obj;
        if ( matricula == null ) {
            if ( other.matricula != null )
                return false;
        } else if ( !matricula.equals( other.matricula ) )
            return false;
        if ( getId() == null ) {
            if ( other.getId() != null )
                return false;
        } else if ( !getId().equals( other.getId() ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Funcionario [matricula=" + matricula + ", getId()=" + getId() + "]";
    }

}
