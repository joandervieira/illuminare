package com.illuminare.view;


import java.util.ArrayList;
import java.util.List;


public class GrupoOcupacionaisCargosSetoresView {

    private List< GrupoOcupacionalView > gruposOcupacionais = new ArrayList<>();

    private List< CargoView > cargos = new ArrayList<>();

    private List< SetorView > setores = new ArrayList<>();


    public void addGrupoOcupacional( GrupoOcupacionalView grupoOcupacionalView ) {

        if ( grupoOcupacionalView != null && grupoOcupacionalView.getId() != null ) {
            boolean hasAdd = false;
            for ( GrupoOcupacionalView go : gruposOcupacionais ) {
                if ( go.getId().equals( grupoOcupacionalView.getId() ) ) {
                    hasAdd = true;
                    break;
                }
            }
            if ( !hasAdd ) {
                gruposOcupacionais.add( grupoOcupacionalView );
            }
        }
    }


    public void addSetor( SetorView setorView ) {

        if ( setorView != null && setorView.getId() != null ) {
            boolean hasAdd = false;
            for ( SetorView s : setores ) {
                if ( s.getId().equals( setorView.getId() ) ) {
                    hasAdd = true;
                    break;
                }
            }
            if ( !hasAdd ) {
                setores.add( setorView );
            }
        }
    }


    public void addCargo( CargoView cargoView ) {

        if ( cargoView != null && cargoView.getId() != null ) {
            boolean hasAdd = false;
            for ( CargoView c : cargos ) {
                if ( c.getId().equals( cargoView.getId() ) ) {
                    hasAdd = true;
                    break;
                }
            }
            if ( !hasAdd ) {
                cargos.add( cargoView );
            }
        }
    }


    public List< GrupoOcupacionalView > getGruposOcupacionais() {

        return gruposOcupacionais;
    }


    public void setGruposOcupacionais( List< GrupoOcupacionalView > gruposOcupacionais ) {

        this.gruposOcupacionais = gruposOcupacionais;
    }


    public List< CargoView > getCargos() {

        return cargos;
    }


    public void setCargos( List< CargoView > cargos ) {

        this.cargos = cargos;
    }


    public List< SetorView > getSetores() {

        return setores;
    }


    public void setSetores( List< SetorView > setores ) {

        this.setores = setores;
    }
}
