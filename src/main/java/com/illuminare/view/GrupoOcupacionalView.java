package com.illuminare.view;


public class GrupoOcupacionalView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String descricao;

    private String obs;


    public GrupoOcupacionalView() {

    }


    public GrupoOcupacionalView( String nome ) {

        setNome( nome );
    }


    public GrupoOcupacionalView( Long id ) {

        setId( id );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDescricao() {

        return descricao;
    }


    public void setDescricao( String descricao ) {

        this.descricao = descricao;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
        result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        GrupoOcupacionalView other = (GrupoOcupacionalView) obj;
        if ( getId() == null ) {
            if ( other.getId() != null )
                return false;
        } else if ( !getId().equals( other.getId() ) )
            return false;
        if ( nome == null ) {
            if ( other.nome != null )
                return false;
        } else if ( !nome.equals( other.nome ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "GrupoOcupacional [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
