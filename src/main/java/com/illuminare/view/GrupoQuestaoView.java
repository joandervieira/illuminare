package com.illuminare.view;


import java.util.Set;

import com.illuminare.entity.GrupoQuestao;


public class GrupoQuestaoView extends GenericEntityView {

    public int totalQuestoesPropria;

    public int totalQuestoesLider;

    public int totalQuestoesSubordinados;

    public int totalQuestoesOutros;

    public int totalQuestoesMeta;

    public int totalQuestoesValorObtido;

    public int totalQuestoesPercentualMeta;

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String descricao;

    private Set< QuestaoOrdemView > questoesOrdem;

    private String obs;

    private Double mediaPropria;

    private Double mediaLideres;

    private Double mediaSubordinados;

    private Double mediaOutros;

    private Double meta;

    private Double valorObtido;

    private Double percentualMeta;


    public GrupoQuestaoView() {

    }


    public GrupoQuestaoView( Long idGrupoQuestao ) {

        setId( idGrupoQuestao );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDescricao() {

        return descricao;
    }


    public void setDescricao( String descricao ) {

        this.descricao = descricao;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public Set< QuestaoOrdemView > getQuestoesOrdem() {

        return questoesOrdem;
    }


    public void setQuestoesOrdem( Set< QuestaoOrdemView > questoesOrdem ) {

        this.questoesOrdem = questoesOrdem;
    }


    public Double getMediaPropria() {

        return mediaPropria;
    }


    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }


    public Double getMediaLideres() {

        return mediaLideres;
    }


    public void setMediaLideres( Double mediaLideres ) {

        this.mediaLideres = mediaLideres;
    }


    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }


    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }


    public Double getMediaOutros() {

        return mediaOutros;
    }


    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }


    @Override
    public String toString() {

        return "GrupoQuestao [nome=" + nome + ", getId()=" + getId() + "]";
    }


    public GrupoQuestao toEntityOnlyId() {

        if ( getId() != null ) {
            return new GrupoQuestao( getId() );
        }
        return null;
    }
}
