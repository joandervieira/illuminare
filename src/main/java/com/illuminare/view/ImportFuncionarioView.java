package com.illuminare.view;


import java.util.List;

import com.illuminare.entity.Funcionario;


/**
 * Created by joander on 12/07/2016.
 */
public class ImportFuncionarioView extends GenericEntityView {

    private List< Funcionario > funcionarios;

    private String cnpj;


    public List< Funcionario > getFuncionarios() {

        return funcionarios;
    }


    public void setFuncionarios( List< Funcionario > funcionarios ) {

        this.funcionarios = funcionarios;
    }


    public String getCnpj() {

        return cnpj;
    }


    public void setCnpj( String cnpj ) {

        this.cnpj = cnpj;
    }
}
