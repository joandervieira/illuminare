package com.illuminare.view;


public class LiderView extends GenericEntityView {

    PessoaView pessoa;

    CargoView cargo;

    SetorView setor;

    GrupoOcupacionalView grupoOcupacional;

    private Long id;

    private Long idEmpresa;

    private String matricula;


    public LiderView() {

    }


    public LiderView( Long id, String matricula ) {

        setId( id );
        this.matricula = matricula;
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public PessoaView getPessoa() {

        return pessoa;
    }


    public void setPessoa( PessoaView pessoa ) {

        this.pessoa = pessoa;
    }


    public CargoView getCargo() {

        return cargo;
    }


    public void setCargo( CargoView cargo ) {

        this.cargo = cargo;
    }


    public SetorView getSetor() {

        return setor;
    }


    public void setSetor( SetorView setor ) {

        this.setor = setor;
    }


    public GrupoOcupacionalView getGrupoOcupacional() {

        return grupoOcupacional;
    }


    public void setGrupoOcupacional( GrupoOcupacionalView grupoOcupacional ) {

        this.grupoOcupacional = grupoOcupacional;
    }


    public String getMatricula() {

        return matricula;
    }


    public void setMatricula( String matricula ) {

        this.matricula = matricula;
    }


    @Override
    public String toString() {

        return "Funcionario [getId()=" + getId() + "]";
    }

}
