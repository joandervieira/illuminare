package com.illuminare.view;


import java.util.LinkedHashSet;
import java.util.Set;


public class ListaValoresView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String tipoLista;

    private Set< ValorListaView > valoresLista = new LinkedHashSet< ValorListaView >();


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public Set< ValorListaView > getValoresLista() {

        return valoresLista;
    }


    public void setValoresLista( Set< ValorListaView > valoresLista ) {

        this.valoresLista = valoresLista;
    }


    public String getTipoLista() {

        return tipoLista;
    }


    public void setTipoLista( String tipoLista ) {

        this.tipoLista = tipoLista;
    }


    @Override
    public String toString() {

        return "ListaValores{" + "nome='" + nome + '\'' + ", tipoLista='" + tipoLista + '\'' + ", valoresLista=" + valoresLista + '}';
    }

}
