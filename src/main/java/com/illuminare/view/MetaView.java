package com.illuminare.view;


public class MetaView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private FuncionarioView funcionario;

    private PesquisaView pesquisa;

    private String meta;


    public MetaView() {

    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public FuncionarioView getFuncionario() {

        return funcionario;
    }


    public void setFuncionario( FuncionarioView funcionario ) {

        this.funcionario = funcionario;
    }


    public PesquisaView getPesquisa() {

        return pesquisa;
    }


    public void setPesquisa( PesquisaView pesquisa ) {

        this.pesquisa = pesquisa;
    }


    public String getMeta() {

        return meta;
    }


    public void setMeta( String meta ) {

        this.meta = meta;
    }


    @Override
    public String toString() {

        return "Meta [funcionario=" + funcionario + ", pesquisa=" + pesquisa + ", meta=" + meta + "]";
    }

}
