package com.illuminare.view;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Paginator< T > {

    private T entity;

    private Long total;

    private Long pageNumber;

    private Long pageSize;

    private List< T > result;

    private List< String > fieldsToFetch;

    private Map< String, Object > fieldsToSearch;

    private String orderBy;

    private Boolean pesquisaGeral = false;

    private Boolean hasNext = true;


    public Long getTotal() {

        return total;
    }


    public void setTotal( Long total ) {

        this.total = total;
    }


    public Long getPageNumber() {

        if ( pageNumber != null && pageNumber < 1L ) {
            pageNumber = 1L;
        }
        return pageNumber;
    }


    public void setPageNumber( Long pageNumber ) {

        this.pageNumber = pageNumber;
    }


    public Long getPageSize() {

        return pageSize;
    }


    public void setPageSize( Long pageSize ) {

        this.pageSize = pageSize;
    }


    public List< T > getResult() {

        return result;
    }


    /*
     * public Set<T> getResultSet() { if (result != null) { return new
     * LinkedHashSet<>(result); } else { return null; } }
     * 
     * public void setResultSet(Set<T> result) { if (result != null) { this.result =
     * new ArrayList<>(result); } else { this.result = null; } }
     */
    public void setResult( List< T > result ) {

        this.result = result;
    }


    public List< String > getFieldsToFetch() {

        return fieldsToFetch;
    }


    public void setFieldsToFetch( List< String > fieldsToFetch ) {

        this.fieldsToFetch = fieldsToFetch;
    }


    public String getOrderBy() {

        return orderBy;
    }


    public void setOrderBy( String orderBy ) {

        this.orderBy = orderBy;
    }


    public T getEntity() {

        return entity;
    }


    public void setEntity( T entity ) {

        this.entity = entity;
    }


    public Boolean getPesquisaGeral() {

        return pesquisaGeral;
    }


    public void setPesquisaGeral( Boolean pesquisaGeral ) {

        this.pesquisaGeral = pesquisaGeral;
    }


    public Map< String, Object > getFieldsToSearch() {

        if ( fieldsToSearch == null ) {
            fieldsToSearch = new HashMap< String, Object >();
        }
        return fieldsToSearch;
    }


    public void setFieldsToSearch( Map< String, Object > fieldsToSearch ) {

        this.fieldsToSearch = fieldsToSearch;
    }


    public Boolean getHasNext() {

        return hasNext;
    }


    public void setHasNext( Boolean hasNext ) {

        this.hasNext = hasNext;
    }

}
