package com.illuminare.view;


import com.illuminare.entity.Pesquisa;


public class PesquisaResultView extends GenericEntityView {

    public int qtdeTotalPropria;

    public int qtdeTotalLider;

    public int qtdeTotalSubordinados;

    public int qtdeTotalOutros;

    public int qtdeTotalMeta;

    public int qtdeTotalValorObtido;

    public int qtdeTotalPercentualMeta;

    private Pesquisa entity;

    private Long id;

    private String nome;

    private Double mediaPropria;

    private Double mediaLideres;

    private Double mediaSubordinados;

    private Double mediaOutros;

    private Double meta;

    private Double valorObtido;

    private Double percentualMeta;


    public PesquisaResultView() {

    }


    public PesquisaResultView( Long id ) {

        setId( id );
    }


    public PesquisaResultView( Long id, String nome ) {

        setId( id );
        setNome( nome );
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public Double getMediaPropria() {

        return mediaPropria;
    }


    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }


    public Double getMediaLideres() {

        return mediaLideres;
    }


    public void setMediaLideres( Double mediaLideres ) {

        this.mediaLideres = mediaLideres;
    }


    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }


    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }


    public Double getMediaOutros() {

        return mediaOutros;
    }


    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public Pesquisa getEntity() {

        return entity;
    }


    public void setEntity( Pesquisa entity ) {

        this.entity = entity;
    }


    public Pesquisa toEntityOnlyId() {

        if ( getId() != null ) {
            return new Pesquisa( getId() );
        }
        return null;
    }
}
