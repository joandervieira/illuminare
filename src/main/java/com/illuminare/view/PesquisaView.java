package com.illuminare.view;


import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.entity.Empresa;
import com.illuminare.entity.Pesquisa;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;


public class PesquisaView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private Boolean ativo;

    private Boolean excluido = false;

    private String usuarioCadastro;

    private String usuarioAtualizacao;

    private Date dataCadastro;

    private Date dataAtualizacao;

    private List< String > fieldsToFetch;

    private Empresa empresa;

    private String nome;

    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaInicio;

    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaFim;

    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaEntrega;

    private String obs;

    private Set< QuestionarioView > questionarios;

    private Boolean autoAvaliacao;

    private Boolean avaliacaoSubordinados;

    private Boolean avaliacaoSuperiores;

    private Boolean porTipo;

    private Boolean calculoPorMeta;

    private Boolean calculoValorObtidoSuperior = true;

    // private Set<Questionario> questionarios = new
    // LinkedHashSet<Questionario>();

    private Boolean calculoValorObtidoPropria = true;

    private Boolean calculoValorObtidoSubordinados = true;

    private Boolean calculoValorObtidoOutros = true;

    private Date dtaInicioMaiorQue;

    private Date dtaInicioMenorQue;

    private Date dtaFimMaiorQue;

    private Date dtaFimMenorQue;

    private Date dtaEntregaMaiorQue;

    private Date dtaEntregaMenorQue;

    private boolean salvarMetas;

    private Long cloneMetasPesquisaFromId;

    private Long qtdeQuestionariosClonadosMetas;

    private Long qtdeQuestionariosClonadosMantido;


    public PesquisaView() {

    }


    public PesquisaView( Long id ) {

        setId( id );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaInicio() {

        return dtaInicio;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaInicio( Date dtaInicio ) {

        this.dtaInicio = dtaInicio;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaFim() {

        return dtaFim;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaFim( Date dtaFim ) {

        this.dtaFim = dtaFim;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaEntrega() {

        return dtaEntrega;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaEntrega( Date dtaEntrega ) {

        this.dtaEntrega = dtaEntrega;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public Set< QuestionarioView > getQuestionarios() {

        return questionarios;
    }


    public void setQuestionarios( Set< QuestionarioView > questionarios ) {

        this.questionarios = questionarios;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaInicioMaiorQue() {

        return dtaInicioMaiorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaInicioMaiorQue( Date dtaInicioMaiorQue ) {

        this.dtaInicioMaiorQue = dtaInicioMaiorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaInicioMenorQue() {

        return dtaInicioMenorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaInicioMenorQue( Date dtaInicioMenorQue ) {

        this.dtaInicioMenorQue = dtaInicioMenorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaFimMaiorQue() {

        return dtaFimMaiorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaFimMaiorQue( Date dtaFimMaiorQue ) {

        this.dtaFimMaiorQue = dtaFimMaiorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaFimMenorQue() {

        return dtaFimMenorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaFimMenorQue( Date dtaFimMenorQue ) {

        this.dtaFimMenorQue = dtaFimMenorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaEntregaMaiorQue() {

        return dtaEntregaMaiorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaEntregaMaiorQue( Date dtaEntregaMaiorQue ) {

        this.dtaEntregaMaiorQue = dtaEntregaMaiorQue;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaEntregaMenorQue() {

        return dtaEntregaMenorQue;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaEntregaMenorQue( Date dtaEntregaMenorQue ) {

        this.dtaEntregaMenorQue = dtaEntregaMenorQue;
    }


    public boolean getAutoAvaliacao() {

        if ( autoAvaliacao == null ) {
            autoAvaliacao = false;
        }

        return autoAvaliacao;
    }


    public void setAutoAvaliacao( boolean autoAvaliacao ) {

        this.autoAvaliacao = autoAvaliacao;
    }


    public Boolean getAvaliacaoSubordinados() {

        if ( avaliacaoSubordinados == null ) {
            avaliacaoSubordinados = false;
        }
        return avaliacaoSubordinados;
    }


    public void setAvaliacaoSubordinados( Boolean avaliacaoSubordinados ) {

        this.avaliacaoSubordinados = avaliacaoSubordinados;
    }


    public Boolean getAvaliacaoSuperiores() {

        if ( avaliacaoSuperiores == null ) {
            avaliacaoSuperiores = false;
        }

        return avaliacaoSuperiores;
    }


    public void setAvaliacaoSuperiores( Boolean avaliacaoSuperiores ) {

        this.avaliacaoSuperiores = avaliacaoSuperiores;
    }


    @Override
    public String toString() {

        return "Pesquisa [nome=" + nome + ", getId()=" + getId() + "]";
    }


    public Boolean getPorTipo() {

        if ( porTipo == null ) {
            porTipo = false;
        }

        return porTipo;
    }


    public void setPorTipo( Boolean porTipo ) {

        this.porTipo = porTipo;
    }


    public boolean getSalvarMetas() {

        return salvarMetas;
    }


    public void setSalvarMetas( boolean salvarMetas ) {

        this.salvarMetas = salvarMetas;
    }


    public Boolean getCalculoPorMeta() {

        if ( calculoPorMeta == null ) {
            return false;
        }

        return calculoPorMeta;
    }


    public void setCalculoPorMeta( Boolean calculoPorMeta ) {

        this.calculoPorMeta = calculoPorMeta;
    }


    public Boolean getCalculoValorObtidoSuperior() {

        if ( calculoValorObtidoSuperior == null ) {
            calculoValorObtidoSuperior = true;
        }

        return calculoValorObtidoSuperior;
    }


    public void setCalculoValorObtidoSuperior( Boolean calculoValorObtidoSuperior ) {

        this.calculoValorObtidoSuperior = calculoValorObtidoSuperior;
    }


    public Boolean getCalculoValorObtidoPropria() {

        if ( calculoValorObtidoPropria == null ) {
            calculoValorObtidoPropria = true;
        }

        return calculoValorObtidoPropria;
    }


    public void setCalculoValorObtidoPropria( Boolean calculoValorObtidoPropria ) {

        this.calculoValorObtidoPropria = calculoValorObtidoPropria;
    }


    public Boolean getCalculoValorObtidoSubordinados() {

        if ( calculoValorObtidoSubordinados == null ) {
            calculoValorObtidoSubordinados = true;
        }

        return calculoValorObtidoSubordinados;
    }


    public void setCalculoValorObtidoSubordinados( Boolean calculoValorObtidoSubordinados ) {

        this.calculoValorObtidoSubordinados = calculoValorObtidoSubordinados;
    }


    public Boolean getCalculoValorObtidoOutros() {

        if ( calculoValorObtidoOutros == null ) {
            calculoValorObtidoOutros = true;
        }

        return calculoValorObtidoOutros;
    }


    public void setCalculoValorObtidoOutros( Boolean calculoValorObtidoOutros ) {

        this.calculoValorObtidoOutros = calculoValorObtidoOutros;
    }


    public Pesquisa toEntityOnlyId() {

        if ( getId() != null ) {
            return new Pesquisa( getId() );
        }
        return null;
    }


    public Long getCloneMetasPesquisaFromId() {

        return cloneMetasPesquisaFromId;
    }


    public void setCloneMetasPesquisaFromId( Long cloneMetasPesquisaFromId ) {

        this.cloneMetasPesquisaFromId = cloneMetasPesquisaFromId;
    }


    public Boolean getAtivo() {

        return ativo;
    }


    public void setAtivo( Boolean ativo ) {

        this.ativo = ativo;
    }


    public Boolean getExcluido() {

        return excluido;
    }


    public void setExcluido( Boolean excluido ) {

        this.excluido = excluido;
    }


    public Empresa getEmpresa() {

        return empresa;
    }


    public void setEmpresa( Empresa empresa ) {

        this.empresa = empresa;
    }


    public List< String > getFieldsToFetch() {

        return fieldsToFetch;
    }


    public void setFieldsToFetch( List< String > fieldsToFetch ) {

        this.fieldsToFetch = fieldsToFetch;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDataCadastro() {

        return dataCadastro;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDataCadastro( Date dataCadastro ) {

        this.dataCadastro = dataCadastro;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDataAtualizacao() {

        return dataAtualizacao;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDataAtualizacao( Date dataAtualizacao ) {

        this.dataAtualizacao = dataAtualizacao;
    }


    public String getUsuarioAtualizacao() {

        return usuarioAtualizacao;
    }


    public void setUsuarioAtualizacao( String usuarioAtualizacao ) {

        this.usuarioAtualizacao = usuarioAtualizacao;
    }


    public String getUsuarioCadastro() {

        return usuarioCadastro;
    }


    public void setUsuarioCadastro( String usuarioCadastro ) {

        this.usuarioCadastro = usuarioCadastro;
    }


    public Long getQtdeQuestionariosClonadosMetas() {

        return qtdeQuestionariosClonadosMetas;
    }


    public void setQtdeQuestionariosClonadosMetas( Long qtdeQuestionariosClonadosMetas ) {

        this.qtdeQuestionariosClonadosMetas = qtdeQuestionariosClonadosMetas;
    }


    public Long getQtdeQuestionariosClonadosMantido() {

        return qtdeQuestionariosClonadosMantido;
    }


    public void setQtdeQuestionariosClonadosMantido( Long qtdeQuestionariosClonadosMantido ) {

        this.qtdeQuestionariosClonadosMantido = qtdeQuestionariosClonadosMantido;
    }
}
