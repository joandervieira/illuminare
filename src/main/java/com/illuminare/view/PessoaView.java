package com.illuminare.view;


import java.util.Date;
import java.util.Set;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;


public class PessoaView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String tipoPessoa;

    private String nome;

    private String documento; // cpf ou cnpj

    private String rg;

    private String sexo;

    @Temporal( TemporalType.TIMESTAMP )
    private Date dtaNascimento;

    private String inscricaoEstadual;

    private String razaoSocial;

    private String obs;

    private EstadoCivilView estadoCivil;

    private Set< TelefoneView > telefones;

    private Set< EnderecoView > enderecos;

    private Set< EnderecoEletronico > enderecosEletronicos;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getTipoPessoa() {

        return tipoPessoa;
    }


    public void setTipoPessoa( String tipoPessoa ) {

        this.tipoPessoa = tipoPessoa;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getDocumento() {

        return documento;
    }


    public void setDocumento( String documento ) {

        this.documento = documento;
    }


    public String getSexo() {

        return sexo;
    }


    public void setSexo( String sexo ) {

        this.sexo = sexo;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getDtaNascimento() {

        return dtaNascimento;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setDtaNascimento( Date dtaNascimento ) {

        this.dtaNascimento = dtaNascimento;
    }


    public String getInscricaoEstadual() {

        return inscricaoEstadual;
    }


    public void setInscricaoEstadual( String inscricaoEstadual ) {

        this.inscricaoEstadual = inscricaoEstadual;
    }


    public String getRazaoSocial() {

        return razaoSocial;
    }


    public void setRazaoSocial( String razaoSocial ) {

        this.razaoSocial = razaoSocial;
    }


    public EstadoCivilView getEstadoCivil() {

        return estadoCivil;
    }


    public void setEstadoCivil( EstadoCivilView estadoCivil ) {

        this.estadoCivil = estadoCivil;
    }


    public Set< TelefoneView > getTelefones() {

        return telefones;
    }


    public void setTelefones( Set< TelefoneView > telefones ) {

        this.telefones = telefones;
    }


    public Set< EnderecoView > getEnderecos() {

        return enderecos;
    }


    public void setEnderecos( Set< EnderecoView > enderecos ) {

        this.enderecos = enderecos;
    }


    public Set< EnderecoEletronico > getEnderecosEletronicos() {

        return enderecosEletronicos;
    }


    public void setEnderecosEletronicos( Set< EnderecoEletronico > enderecosEletronicos ) {

        this.enderecosEletronicos = enderecosEletronicos;
    }


    public String getRg() {

        return rg;
    }


    public void setRg( String rg ) {

        if ( rg != null ) {
            rg = rg.toUpperCase().trim();
        }
        this.rg = rg;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
        result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        PessoaView other = (PessoaView) obj;
        if ( nome == null ) {
            if ( other.nome != null )
                return false;
        } else if ( !nome.equals( other.nome ) )
            return false;
        if ( getId() == null ) {
            if ( other.getId() != null )
                return false;
        } else if ( !getId().equals( other.getId() ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Pessoa [nome=" + nome + ", getId()=" + getId() + "]";
    }

}
