package com.illuminare.view;


import java.util.ArrayList;
import java.util.List;

import com.illuminare.entity.Funcionario;


/**
 * @author Joander Vieira Candido
 * @since 11/07/16.
 */
public class PreviewFuncionariosImportacao {

    private Long idEmpresa;

    private Long id;

    private List< Funcionario > funcionariosImportados = new ArrayList<>();

    private List< Funcionario > funcionariosDesativados = new ArrayList<>();


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public List< Funcionario > getFuncionariosImportados() {

        return funcionariosImportados;
    }


    public void setFuncionariosImportados( List< Funcionario > funcionariosImportados ) {

        this.funcionariosImportados = funcionariosImportados;
    }


    public List< Funcionario > getFuncionariosDesativados() {

        return funcionariosDesativados;
    }


    public void setFuncionariosDesativados( List< Funcionario > funcionariosDesativados ) {

        this.funcionariosDesativados = funcionariosDesativados;
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }

}
