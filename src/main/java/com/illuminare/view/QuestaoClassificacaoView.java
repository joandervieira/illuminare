package com.illuminare.view;


public class QuestaoClassificacaoView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private Long idQuestionario;

    private Long idGrupoQuestao;

    private Long idQuestao;

    private Long classificacao;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Long getClassificacao() {

        return classificacao;
    }


    public void setClassificacao( Long classificacao ) {

        this.classificacao = classificacao;
    }


    public Long getIdQuestao() {

        return idQuestao;
    }


    public void setIdQuestao( Long idQuestao ) {

        this.idQuestao = idQuestao;
    }


    public Long getIdQuestionario() {

        return idQuestionario;
    }


    public void setIdQuestionario( Long idQuestionario ) {

        this.idQuestionario = idQuestionario;
    }


    @Override
    public String toString() {

        return "QuestaoClassificacao{" + "idQuestionario=" + idQuestionario + ", idQuestao=" + idQuestao + ", classificacao=" + classificacao + '}';
    }


    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }


    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }
}
