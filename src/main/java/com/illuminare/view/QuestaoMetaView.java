package com.illuminare.view;


import java.util.Objects;


public class QuestaoMetaView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private Long idQuestao;

    private Long idGrupoQuestao;

    private Long idPesquisa;

    private Double meta;

    private Boolean autoCalculada;

    private Boolean negativo = false;

    private Long idGrupoOcupacional;

    private Long idCargo;

    private Long idSetor;

    private Long idQuestionario;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Long getIdQuestao() {

        return idQuestao;
    }


    public void setIdQuestao( Long idQuestao ) {

        this.idQuestao = idQuestao;
    }


    public Long getIdPesquisa() {

        return idPesquisa;
    }


    public void setIdPesquisa( Long idPesquisa ) {

        this.idPesquisa = idPesquisa;
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Boolean getNegativo() {

        return negativo;
    }


    public void setNegativo( Boolean negativo ) {

        this.negativo = negativo;
    }


    public Long getIdGrupoOcupacional() {

        return idGrupoOcupacional;
    }


    public void setIdGrupoOcupacional( Long idGrupoOcupacional ) {

        this.idGrupoOcupacional = idGrupoOcupacional;
    }


    public Long getIdCargo() {

        return idCargo;
    }


    public void setIdCargo( Long idCargo ) {

        this.idCargo = idCargo;
    }


    public Long getIdSetor() {

        return idSetor;
    }


    public void setIdSetor( Long idSetor ) {

        this.idSetor = idSetor;
    }


    public Long getIdQuestionario() {

        return idQuestionario;
    }


    public void setIdQuestionario( Long idQuestionario ) {

        this.idQuestionario = idQuestionario;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        if ( !super.equals( o ) )
            return false;
        QuestaoMetaView that = (QuestaoMetaView) o;
        return Objects.equals( getIdQuestao(), that.getIdQuestao() ) && Objects.equals( getIdPesquisa(), that.getIdPesquisa() )
            && Objects.equals( getMeta(), that.getMeta() ) && Objects.equals( getNegativo(), that.getNegativo() )
            && Objects.equals( getIdGrupoOcupacional(), that.getIdGrupoOcupacional() ) && Objects.equals( getIdCargo(), that.getIdCargo() )
            && Objects.equals( getIdSetor(), that.getIdSetor() ) && Objects.equals( getIdQuestionario(), that.getIdQuestionario() );
    }


    @Override
    public int hashCode() {

        return Objects.hash(
            super.hashCode(),
            getIdQuestao(),
            getIdPesquisa(),
            getMeta(),
            getNegativo(),
            getIdGrupoOcupacional(),
            getIdCargo(),
            getIdSetor(),
            getIdQuestionario() );
    }


    public Boolean getAutoCalculada() {

        return autoCalculada;
    }


    public void setAutoCalculada( Boolean autoCalculada ) {

        this.autoCalculada = autoCalculada;
    }


    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }


    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }
}
