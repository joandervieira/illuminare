package com.illuminare.view;


import java.util.Objects;


public class QuestaoOrdemView extends GenericEntityView implements Comparable< QuestaoOrdemView > {

    private Long id;

    private Long idEmpresa;

    private QuestaoView questao;

    private Integer ordem;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public QuestaoView getQuestao() {

        return questao;
    }


    public void setQuestao( QuestaoView questao ) {

        this.questao = questao;
    }


    public Integer getOrdem() {

        return ordem;
    }


    public void setOrdem( Integer ordem ) {

        this.ordem = ordem;
    }


    @Override
    public String toString() {

        return "QuestaoOrdem [questao=" + questao + ", ordem=" + ordem + "]";
    }


    @Override
    public int compareTo( QuestaoOrdemView other ) {

        if ( this.ordem == null || other == null ) {
            return -1;
        }
        if ( this.ordem < other.ordem ) {
            return -1;
        }
        if ( this.ordem > other.ordem ) {
            return 1;
        }
        return 0;
    }

    // implementar o equals comparar por id questao


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( !( o instanceof QuestaoOrdemView ) )
            return false;
        if ( !super.equals( o ) )
            return false;
        QuestaoOrdemView that = (QuestaoOrdemView) o;
        return Objects.equals( getQuestao(), that.getQuestao() );
    }


    @Override
    public int hashCode() {

        return Objects.hash( super.hashCode(), getQuestao() );
    }

}
