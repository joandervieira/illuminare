package com.illuminare.view;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.illuminare.entity.Questao;
import com.illuminare.entity.Resposta;
import com.illuminare.resultado.Nota;


public class QuestaoView extends GenericEntityView {

    public int qtdeMediaPropria;

    public int qtdeMediaLider;

    public int qtdeMediaSubordinados;

    public int qtdeMediaOutros;

    private Long id;

    private Long idEmpresa;

    private String nomeQuestao;

    private String perguntaQuestao;

    private Boolean isTexto;

    private Boolean isLista;

    private Boolean isNumerico;

    private ListaValoresView listaValores;

    private Nota notaPropria;

    private Nota notaLideres;

    private Nota notaSubordinados;

    private Nota notaOutros;

    private RespostaView resposta;

    private String respostaTexto;

    private Double respostaNumerico;

    private ValorListaView respostaValorLista;

    private Long respostaId;

    private QuestaoMetaView questaoMeta;

    private List< QuestaoMetaView > questoesMetasGruposOcupacionais = new ArrayList<>();

    private List< QuestaoMetaView > questoesMetasCargos = new ArrayList<>();

    private List< QuestaoMetaView > questoesMetasSetores = new ArrayList<>();

    private Long classificacao;

    private QuestaoClassificacaoView questaoClassificacao;

    private Double meta;

    private Boolean negativo = false;

    private Double valorObtido;

    private Double percentualMeta;

    private Double media;

    private Double mediaPropria;

    private Double mediaLider;

    private Double mediaSubordinados;

    private Double mediaOutros;

    private List< String > respostasTextosProprias;

    private List< String > respostasTextosLider;

    private List< String > respostasTextosSubordinados;

    private List< String > respostasTextosOutros;

    private List< Resposta > respostasProprias = new ArrayList<>();

    private List< Resposta > respostasLider = new ArrayList<>();

    private List< Resposta > respostasSubordinados = new ArrayList<>();

    private List< Resposta > respostasOutros = new ArrayList<>();



    public QuestaoView() {

    }


    public QuestaoView( Long id ) {

        setId( id );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public String getNomeQuestao() {

        return nomeQuestao;
    }


    public void setNomeQuestao( String nomeQuestao ) {

        this.nomeQuestao = nomeQuestao;
    }


    public Boolean getIsTexto() {

        if ( isTexto == null ) {
            isTexto = false;
        }

        return isTexto;
    }


    public void setIsTexto( Boolean isTexto ) {

        this.isTexto = isTexto;
    }


    public Boolean getIsLista() {

        if ( isLista == null ) {
            isLista = false;
        }

        return isLista;
    }


    public void setIsLista( Boolean isLista ) {

        this.isLista = isLista;
    }


    public ListaValoresView getListaValores() {

        return listaValores;
    }


    public void setListaValores( ListaValoresView listaValores ) {

        this.listaValores = listaValores;
    }


    public String getPerguntaQuestao() {

        return perguntaQuestao;
    }


    public void setPerguntaQuestao( String perguntaQuestao ) {

        this.perguntaQuestao = perguntaQuestao;
    }


    public Nota getNotaPropria() {

        return notaPropria;
    }


    public void setNotaPropria( Nota notaPropria ) {

        this.notaPropria = notaPropria;
    }


    public Nota getNotaLideres() {

        return notaLideres;
    }


    public void setNotaLideres( Nota notaLideres ) {

        this.notaLideres = notaLideres;
    }


    public Nota getNotaSubordinados() {

        return notaSubordinados;
    }


    public void setNotaSubordinados( Nota notaSubordinados ) {

        this.notaSubordinados = notaSubordinados;
    }


    public Nota getNotaOutros() {

        return notaOutros;
    }


    public void setNotaOutros( Nota notaOutros ) {

        this.notaOutros = notaOutros;
    }


    public void setTexto( Boolean isTexto ) {

        this.isTexto = isTexto;
    }


    public void setLista( Boolean isLista ) {

        this.isLista = isLista;
    }


    public RespostaView getResposta() {

        return resposta;
    }


    public void setResposta( RespostaView resposta ) {

        this.resposta = resposta;
    }


    public String getRespostaTexto() {

        return respostaTexto;
    }


    public void setRespostaTexto( String respostaTexto ) {

        this.respostaTexto = respostaTexto;
    }


    public ValorListaView getRespostaValorLista() {

        return respostaValorLista;
    }


    public void setRespostaValorLista( ValorListaView respostaValorLista ) {

        this.respostaValorLista = respostaValorLista;
    }


    @Override
    public String toString() {

        return "Questao [nomeQuestao=" + nomeQuestao + ", getId()=" + getId() + "]";
    }


    public Long getRespostaId() {

        return respostaId;
    }


    public void setRespostaId( Long respostaId ) {

        this.respostaId = respostaId;
    }


    public Boolean getIsNumerico() {

        if ( isNumerico == null ) {
            isNumerico = false;
        }

        return isNumerico;
    }


    public void setIsNumerico( Boolean numerico ) {

        isNumerico = numerico;
    }


    public Double getRespostaNumerico() {

        return respostaNumerico;
    }


    public void setRespostaNumerico( Double respostaNumerico ) {

        this.respostaNumerico = respostaNumerico;
    }


    public Long getClassificacao() {

        return classificacao;
    }


    public void setClassificacao( Long classificacao ) {

        this.classificacao = classificacao;
    }


    public QuestaoClassificacaoView getQuestaoClassificacao() {

        return questaoClassificacao;
    }


    public void setQuestaoClassificacao( QuestaoClassificacaoView questaoClassificacao ) {

        if ( questaoClassificacao != null ) {
            this.classificacao = questaoClassificacao.getClassificacao();
        }

        this.questaoClassificacao = questaoClassificacao;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( !( o instanceof QuestaoView ) )
            return false;
        if ( !super.equals( o ) )
            return false;
        QuestaoView questao = (QuestaoView) o;

        return Objects.equals( getNomeQuestao(), questao.getNomeQuestao() ) && Objects.equals( getPerguntaQuestao(), questao.getPerguntaQuestao() )
            && Objects.equals( getId(), questao.getId() );
    }


    @Override
    public int hashCode() {

        return Objects.hash( super.hashCode(), getNomeQuestao(), getPerguntaQuestao(), getId() );
    }


    public Boolean getNegativo() {

        return negativo;
    }


    public void setNegativo( Boolean negativo ) {

        this.negativo = negativo;
    }


    public Double getMedia() {

        return media;
    }


    public void setMedia( Double media ) {

        this.media = media;
    }


    public List< QuestaoMetaView > getQuestoesMetasGruposOcupacionais() {

        return questoesMetasGruposOcupacionais;
    }


    public void setQuestoesMetasGruposOcupacionais( List< QuestaoMetaView > questoesMetasGruposOcupacionais ) {

        this.questoesMetasGruposOcupacionais = questoesMetasGruposOcupacionais;
    }


    public List< QuestaoMetaView > getQuestoesMetasCargos() {

        return questoesMetasCargos;
    }


    public void setQuestoesMetasCargos( List< QuestaoMetaView > questoesMetasCargos ) {

        this.questoesMetasCargos = questoesMetasCargos;
    }


    public List< QuestaoMetaView > getQuestoesMetasSetores() {

        return questoesMetasSetores;
    }


    public void setQuestoesMetasSetores( List< QuestaoMetaView > questoesMetasSetores ) {

        this.questoesMetasSetores = questoesMetasSetores;
    }


    public List< Resposta > getRespostasProprias() {

        return respostasProprias;
    }


    public void setRespostasProprias( List< Resposta > respostasProprias ) {

        this.respostasProprias = respostasProprias;
    }


    public List< Resposta > getRespostasLider() {

        return respostasLider;
    }


    public void setRespostasLider( List< Resposta > respostasLider ) {

        this.respostasLider = respostasLider;
    }


    public List< Resposta > getRespostasSubordinados() {

        return respostasSubordinados;
    }


    public void setRespostasSubordinados( List< Resposta > respostasSubordinados ) {

        this.respostasSubordinados = respostasSubordinados;
    }


    public List< Resposta > getRespostasOutros() {

        return respostasOutros;
    }


    public void setRespostasOutros( List< Resposta > respostasOutros ) {

        this.respostasOutros = respostasOutros;
    }


    public Double getMediaPropria() {

        return mediaPropria;
    }


    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }


    public Double getMediaLider() {

        return mediaLider;
    }


    public void setMediaLider( Double mediaLider ) {

        this.mediaLider = mediaLider;
    }


    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }


    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }


    public Double getMediaOutros() {

        return mediaOutros;
    }


    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }


    public List< String > getRespostasTextosProprias() {

        return respostasTextosProprias;
    }


    public void setRespostasTextosProprias( List< String > respostasTextosProprias ) {

        this.respostasTextosProprias = respostasTextosProprias;
    }


    public List< String > getRespostasTextosLider() {

        return respostasTextosLider;
    }


    public void setRespostasTextosLider( List< String > respostasTextosLider ) {

        this.respostasTextosLider = respostasTextosLider;
    }


    public List< String > getRespostasTextosSubordinados() {

        return respostasTextosSubordinados;
    }


    public void setRespostasTextosSubordinados( List< String > respostasTextosSubordinados ) {

        this.respostasTextosSubordinados = respostasTextosSubordinados;
    }


    public List< String > getRespostasTextosOutros() {

        return respostasTextosOutros;
    }


    public void setRespostasTextosOutros( List< String > respostasTextosOutros ) {

        this.respostasTextosOutros = respostasTextosOutros;
    }


    public QuestaoMetaView getQuestaoMeta() {

        return questaoMeta;
    }


    public void setQuestaoMeta( QuestaoMetaView questaoMeta ) {

        this.questaoMeta = questaoMeta;
    }


    public Questao toEntityOnlyId() {

        if ( getId() != null ) {
            return new Questao( getId() );
        }
        return null;
    }
}
