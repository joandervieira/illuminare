package com.illuminare.view;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


public class QuestionarioAvaliacaoView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private PesquisaView pesquisa;

    private QuestionarioView questionario;

    private FuncionarioView funcionarioAvaliado;

    private FuncionarioView funcionarioAvaliador;

    private Boolean autoAvaliacao;

    private Boolean avaliacaoSubordinado;

    private Boolean avaliacaoSuperior;

    private List< RespostaView > respostas;

    private Boolean respondido;

    private QuestionarioRespondido questionarioRespondido;


    public static Set< QuestionarioAvaliacaoView > formatToWebPage( Set< QuestionarioAvaliacaoView > avaliacoes ) {

        Set< QuestionarioAvaliacaoView > result = new LinkedHashSet<>();

        if ( avaliacoes != null ) {
            for ( QuestionarioAvaliacaoView avaliacao : avaliacoes ) {

            }

        }

        return result;

    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public QuestionarioView getQuestionario() {

        return questionario;
    }


    public void setQuestionario( QuestionarioView questionario ) {

        this.questionario = questionario;
    }


    public FuncionarioView getFuncionarioAvaliado() {

        return funcionarioAvaliado;
    }


    public void setFuncionarioAvaliado( FuncionarioView funcionarioAvaliado ) {

        this.funcionarioAvaliado = funcionarioAvaliado;
    }


    public FuncionarioView getFuncionarioAvaliador() {

        return funcionarioAvaliador;
    }


    public void setFuncionarioAvaliador( FuncionarioView funcionarioAvaliador ) {

        this.funcionarioAvaliador = funcionarioAvaliador;
    }


    /*
     * @Override public int hashCode() { final int prime = 31; int result =
     * super.hashCode(); result = prime * result + ((funcionarioAvaliado == null) ?
     * 0 : funcionarioAvaliado.hashCode()); result = prime * result +
     * ((funcionarioAvaliador == null) ? 0 : funcionarioAvaliador.hashCode());
     * result = prime * result + ((pesquisa == null) ? 0 : pesquisa.hashCode());
     * result = prime * result + ((questionario == null) ? 0 :
     * questionario.hashCode()); return result; }
     * 
     * @Override public boolean equals(Object obj) { if (this == obj) return true;
     * if (!super.equals(obj)) return false; if (getClass() != obj.getClass())
     * return false; QuestionarioAvaliacao other = (QuestionarioAvaliacao) obj; if
     * (funcionarioAvaliado == null) { if (other.funcionarioAvaliado != null) return
     * false; } else if (!funcionarioAvaliado.equals(other.funcionarioAvaliado))
     * return false; if (funcionarioAvaliador == null) { if
     * (other.funcionarioAvaliador != null) return false; } else if
     * (!funcionarioAvaliador.equals(other.funcionarioAvaliador)) return false; if
     * (pesquisa == null) { if (other.pesquisa != null) return false; } else if
     * (!pesquisa.equals(other.pesquisa)) return false; if (questionario == null) {
     * if (other.questionario != null) return false; } else if
     * (!questionario.equals(other.questionario)) return false; return true; }
     */

    public PesquisaView getPesquisa() {

        return pesquisa;
    }


    public void setPesquisa( PesquisaView pesquisa ) {

        this.pesquisa = pesquisa;
    }


    @Override
    public String toString() {

        String idAvaliador = "", nomeAvaliador = "", idAvaliado = "", nomeAvaliado = "", idPesquisa = "", idQuestionario = "";
        if ( getFuncionarioAvaliador() != null ) {
            idAvaliador = "" + getFuncionarioAvaliador().getId();
            if ( getFuncionarioAvaliador().getPessoa() != null ) {
                nomeAvaliador = getFuncionarioAvaliador().getPessoa().getNome();
            }
        }
        if ( getFuncionarioAvaliador() != null ) {
            idAvaliado = "" + getFuncionarioAvaliado().getId();
            if ( getFuncionarioAvaliado().getPessoa() != null ) {
                nomeAvaliado = getFuncionarioAvaliado().getPessoa().getNome();
            }
        }
        if ( getPesquisa() != null ) {
            idPesquisa = "" + getPesquisa().getId();
        }
        if ( getQuestionario() != null ) {
            idQuestionario = "" + getQuestionario().getId();
        }

        return String.format(
            "Funcionário [%s-%s] avaliando [%s-%s] -- [pesquisa:%s, questionario:%s]",
            idAvaliador,
            nomeAvaliador,
            idAvaliado,
            nomeAvaliado,
            idPesquisa,
            idQuestionario );

    }


    public List< RespostaView > getRespostas() {

        return respostas;
    }


    public void setRespostas( List< RespostaView > respostas ) {

        this.respostas = respostas;
    }


    public Boolean getRespondido() {

        return respondido;
    }


    public void setRespondido( Boolean respondido ) {

        this.respondido = respondido;
    }


    public Boolean getAutoAvaliacao() {

        if ( autoAvaliacao == null ) {
            autoAvaliacao = false;
        }

        return autoAvaliacao;
    }


    public void setAutoAvaliacao( Boolean autoAvaliacao ) {

        this.autoAvaliacao = autoAvaliacao;
    }


    public Boolean getAvaliacaoSubordinado() {

        if ( avaliacaoSubordinado == null ) {
            avaliacaoSubordinado = false;
        }

        return avaliacaoSubordinado;
    }


    public void setAvaliacaoSubordinado( Boolean avaliacaoSubordinado ) {

        this.avaliacaoSubordinado = avaliacaoSubordinado;
    }


    public Boolean getAvaliacaoSuperior() {

        if ( avaliacaoSuperior == null ) {
            avaliacaoSuperior = false;
        }

        return avaliacaoSuperior;
    }


    public void setAvaliacaoSuperior( Boolean avaliacaoSuperior ) {

        this.avaliacaoSuperior = avaliacaoSuperior;
    }


    public QuestionarioRespondido getQuestionarioRespondido() {

        return questionarioRespondido;
    }


    public void setQuestionarioRespondido( QuestionarioRespondido questionarioRespondido ) {

        this.questionarioRespondido = questionarioRespondido;
    }

}
