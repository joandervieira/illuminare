package com.illuminare.view;


public class QuestionarioRespondido {

    private String descricao;

    private Boolean respondidoTotal = false;

    private Boolean respondidoParcial = false;

    private Boolean naoRespondido = false;


    public String getDescricao() {

        return descricao;
    }


    public void setDescricao( String descricao ) {

        this.descricao = descricao;
    }


    public Boolean getRespondidoTotal() {

        return respondidoTotal;
    }


    public void setRespondidoTotal( Boolean respondidoTotal ) {

        this.respondidoTotal = respondidoTotal;
    }


    public Boolean getRespondidoParcial() {

        return respondidoParcial;
    }


    public void setRespondidoParcial( Boolean respondidoParcial ) {

        this.respondidoParcial = respondidoParcial;
    }


    public Boolean getNaoRespondido() {

        return naoRespondido;
    }


    public void setNaoRespondido( Boolean naoRespondido ) {

        this.naoRespondido = naoRespondido;
    }
}
