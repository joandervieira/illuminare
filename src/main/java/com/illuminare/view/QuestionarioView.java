package com.illuminare.view;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.illuminare.entity.Questionario;
import com.illuminare.entity.Resposta;


public class QuestionarioView extends GenericEntityView {

    public int qtdeTotalPropria;

    public int qtdeTotalLider;

    public int qtdeTotalSubordinados;

    public int qtdeTotalOutros;

    public int qtdeTotalMeta;

    public int qtdeTotalValorObtido;

    public int qtdeTotalPercentualMeta;

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String obs;

    private Set< GrupoQuestaoView > gruposQuestoes = new LinkedHashSet< GrupoQuestaoView >();

    private Set< GrupoOcupacionalView > gruposOcupacionais = new LinkedHashSet< GrupoOcupacionalView >();

    private Set< CargoView > cargos = new LinkedHashSet<>();

    private Set< SetorView > setores = new LinkedHashSet<>();

    private Double mediaPropria;

    private Double mediaLideres;

    private Double mediaSubordinados;

    private Double mediaOutros;

    private String pesquisaNome;

    private Long pesquisaId;

    private String avaliadoNome;

    private Long avaliadoId;

    private String avaliadorNome;

    private Long avaliadorId;

    private Double meta;

    private Double valorObtido;

    private Double percentualMeta;

    private List< Resposta > respostasProprias = new ArrayList<>();

    private List< Resposta > respostasLider = new ArrayList<>();

    private List< Resposta > respostasSubordinados = new ArrayList<>();

    private List< Resposta > respostasOutros = new ArrayList<>();


    public QuestionarioView() {

    }


    public QuestionarioView( Long id ) {

        setId( id );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Double getMeta() {

        return meta;
    }


    public void setMeta( Double meta ) {

        this.meta = meta;
    }


    public Double getValorObtido() {

        return valorObtido;
    }


    public void setValorObtido( Double valorObtido ) {

        this.valorObtido = valorObtido;
    }


    public Double getPercentualMeta() {

        return percentualMeta;
    }


    public void setPercentualMeta( Double percentualMeta ) {

        this.percentualMeta = percentualMeta;
    }


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public Set< GrupoQuestaoView > getGruposQuestoes() {

        return gruposQuestoes;
    }


    public void setGruposQuestoes( Set< GrupoQuestaoView > gruposQuestoes ) {

        this.gruposQuestoes = gruposQuestoes;
    }


    public Set< GrupoOcupacionalView > getGruposOcupacionais() {

        return gruposOcupacionais;
    }


    public void setGruposOcupacionais( Set< GrupoOcupacionalView > gruposOcupacionais ) {

        this.gruposOcupacionais = gruposOcupacionais;
    }


    public Double getMediaPropria() {

        return mediaPropria;
    }


    public void setMediaPropria( Double mediaPropria ) {

        this.mediaPropria = mediaPropria;
    }


    public Double getMediaLideres() {

        return mediaLideres;
    }


    public void setMediaLideres( Double mediaLideres ) {

        this.mediaLideres = mediaLideres;
    }


    public Double getMediaSubordinados() {

        return mediaSubordinados;
    }


    public void setMediaSubordinados( Double mediaSubordinados ) {

        this.mediaSubordinados = mediaSubordinados;
    }


    public Double getMediaOutros() {

        return mediaOutros;
    }


    public void setMediaOutros( Double mediaOutros ) {

        this.mediaOutros = mediaOutros;
    }


    public String getPesquisaNome() {

        return pesquisaNome;
    }


    public void setPesquisaNome( String pesquisaNome ) {

        this.pesquisaNome = pesquisaNome;
    }


    public Long getPesquisaId() {

        return pesquisaId;
    }


    public void setPesquisaId( Long pesquisaId ) {

        this.pesquisaId = pesquisaId;
    }


    public String getAvaliadoNome() {

        return avaliadoNome;
    }


    public void setAvaliadoNome( String avaliadoNome ) {

        this.avaliadoNome = avaliadoNome;
    }


    public Long getAvaliadoId() {

        return avaliadoId;
    }


    public void setAvaliadoId( Long avaliadoId ) {

        this.avaliadoId = avaliadoId;
    }


    public String getAvaliadorNome() {

        return avaliadorNome;
    }


    public void setAvaliadorNome( String avaliadorNome ) {

        this.avaliadorNome = avaliadorNome;
    }


    public Long getAvaliadorId() {

        return avaliadorId;
    }


    public void setAvaliadorId( Long avaliadorId ) {

        this.avaliadorId = avaliadorId;
    }


    @Override
    public String toString() {

        return "Questionario [nome=" + nome + ", getId()=" + getId() + "]";
    }


    public Set< CargoView > getCargos() {

        return cargos;
    }


    public void setCargos( Set< CargoView > cargos ) {

        this.cargos = cargos;
    }


    public Set< SetorView > getSetores() {

        return setores;
    }


    public void setSetores( Set< SetorView > setores ) {

        this.setores = setores;
    }


    public List< Resposta > getRespostasProprias() {

        return respostasProprias;
    }


    public void setRespostasProprias( List< Resposta > respostasProprias ) {

        this.respostasProprias = respostasProprias;
    }


    public List< Resposta > getRespostasLider() {

        return respostasLider;
    }


    public void setRespostasLider( List< Resposta > respostasLider ) {

        this.respostasLider = respostasLider;
    }


    public List< Resposta > getRespostasSubordinados() {

        return respostasSubordinados;
    }


    public void setRespostasSubordinados( List< Resposta > respostasSubordinados ) {

        this.respostasSubordinados = respostasSubordinados;
    }


    public List< Resposta > getRespostasOutros() {

        return respostasOutros;
    }


    public void setRespostasOutros( List< Resposta > respostasOutros ) {

        this.respostasOutros = respostasOutros;
    }


    public Questionario toEntityOnlyId() {

        if ( getId() != null ) {
            return new Questionario( getId() );
        }
        return null;
    }
}
