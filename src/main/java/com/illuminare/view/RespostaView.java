package com.illuminare.view;


public class RespostaView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String respostaTexto;

    private Double respostaNumerico;

    private ValorListaView respostaValorLista;

    private QuestaoView questao;

    private Long idGrupoQuestao;

    private QuestionarioAvaliacaoView questionarioAvaliacao;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    /*
     *
     *
     *
     */

    public String getRespostaTexto() {

        return respostaTexto;
    }


    public void setRespostaTexto( String respostaTexto ) {

        this.respostaTexto = respostaTexto;
    }


    public ValorListaView getRespostaValorLista() {

        return respostaValorLista;
    }


    public void setRespostaValorLista( ValorListaView respostaValorLista ) {

        this.respostaValorLista = respostaValorLista;
    }


    public QuestaoView getQuestao() {

        return questao;
    }


    public void setQuestao( QuestaoView questao ) {

        this.questao = questao;
    }


    public QuestionarioAvaliacaoView getQuestionarioAvaliacao() {

        return questionarioAvaliacao;
    }


    public void setQuestionarioAvaliacao( QuestionarioAvaliacaoView questionarioAvaliacao ) {

        this.questionarioAvaliacao = questionarioAvaliacao;
    }


    public Double getRespostaNumerico() {

        return respostaNumerico;
    }


    public void setRespostaNumerico( Double respostaNumerico ) {

        this.respostaNumerico = respostaNumerico;
    }


    @Override
    public String toString() {

        return "Resposta{" + "respostaTexto='" + respostaTexto + '\'' + ", respostaNumerico=" + respostaNumerico + ", respostaValorLista=" + respostaValorLista
            + ", questao=" + questao + ", questionarioAvaliacao=" + questionarioAvaliacao + '}';
    }


    public Long getIdGrupoQuestao() {

        return idGrupoQuestao;
    }


    public void setIdGrupoQuestao( Long idGrupoQuestao ) {

        this.idGrupoQuestao = idGrupoQuestao;
    }

}
