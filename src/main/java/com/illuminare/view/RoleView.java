package com.illuminare.view;


public class RoleView extends GenericEntityView {

    public static final RoleView ROLE_ROOT = new RoleView( "role_root" );

    public static final RoleView ROLE_USER = new RoleView( "role_user" );

    public static final RoleView ROLE_ADMIN = new RoleView( "role_admin" );

    public static final RoleView ROLE_FUNCIONARIO = new RoleView( "role_funcionario" );

    private Long id;

    private Long idEmpresa;

    private String name;

    private String nomeExibir;


    public RoleView() {

    }


    public RoleView( String name ) {

        setName( name );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public String getNomeExibir() {

        return nomeExibir;
    }


    public void setNomeExibir( String nomeExibir ) {

        this.nomeExibir = nomeExibir;
    }


    @Override
    public String toString() {

        return "Role [id=" + getId() + ", name=" + name + "]";
    }

}
