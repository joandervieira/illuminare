package com.illuminare.view;


public class TelefoneView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String numero;

    private String ddd;

    private String ramal;

    private String obs;

    private TipoTelefoneView tipoTelefone;


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getNumero() {

        return numero;
    }


    public void setNumero( String numero ) {

        this.numero = numero;
    }


    public String getDdd() {

        return ddd;
    }


    public void setDdd( String ddd ) {

        this.ddd = ddd;
    }


    public String getRamal() {

        return ramal;
    }


    public void setRamal( String ramal ) {

        this.ramal = ramal;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    public TipoTelefoneView getTipoTelefone() {

        return tipoTelefone;
    }


    public void setTipoTelefone( TipoTelefoneView tipoTelefone ) {

        this.tipoTelefone = tipoTelefone;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( ddd == null ) ? 0 : ddd.hashCode() );
        result = prime * result + ( ( numero == null ) ? 0 : numero.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        TelefoneView other = (TelefoneView) obj;
        if ( ddd == null ) {
            if ( other.ddd != null )
                return false;
        } else if ( !ddd.equals( other.ddd ) )
            return false;
        if ( numero == null ) {
            if ( other.numero != null )
                return false;
        } else if ( !numero.equals( other.numero ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "Telefone [numero=" + numero + ", getId()=" + getId() + "]";
    }

}
