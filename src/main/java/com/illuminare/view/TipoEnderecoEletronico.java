package com.illuminare.view;


public class TipoEnderecoEletronico {

    private Long id;

    private Long idEmpresa;

    private String tipo;

    private String obs;


    public TipoEnderecoEletronico() {

    }


    public TipoEnderecoEletronico( String tipo ) {

        setTipo( tipo );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getTipo() {

        return tipo;
    }


    public void setTipo( String tipo ) {

        this.tipo = tipo;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public String toString() {

        return "TipoEnderecoEletronico [tipo=" + tipo + ", getId()=" + getId() + "]";
    }

}
