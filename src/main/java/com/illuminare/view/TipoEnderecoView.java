package com.illuminare.view;


public class TipoEnderecoView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String tipo;

    private String obs;


    public TipoEnderecoView() {

    }


    public TipoEnderecoView( String tipo ) {

        setTipo( tipo );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getTipo() {

        return tipo;
    }


    public void setTipo( String tipo ) {

        this.tipo = tipo;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public String toString() {

        return "TipoEndereco [tipo=" + tipo + ", getId()=" + getId() + "]";
    }

}
