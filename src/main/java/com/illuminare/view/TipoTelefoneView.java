package com.illuminare.view;


public class TipoTelefoneView extends GenericEntityView {

    private Long id;

    private Long idEmpresa;

    private String tipo;

    private String obs;


    public TipoTelefoneView() {

    }


    public TipoTelefoneView( String tipo ) {

        setTipo( tipo );
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public String getTipo() {

        return tipo;
    }


    public void setTipo( String tipo ) {

        this.tipo = tipo;
    }


    public String getObs() {

        return obs;
    }


    public void setObs( String obs ) {

        this.obs = obs;
    }


    @Override
    public String toString() {

        return "TipoTelefone [tipo=" + tipo + ", getId()=" + getId() + "]";
    }

}
