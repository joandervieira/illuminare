package com.illuminare.view;


import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.illuminare.util.AppStringUtils;
import com.illuminare.util.CustomJsonDateDeserializer;
import com.illuminare.util.JsonDateSerializer;


public class UserView extends GenericEntityView implements UserDetails {

    /**
     *
     */
    private static final long serialVersionUID = -1483955829057616212L;

    /*** SPRING SECURITY **/

    private Long id;

    private Long idEmpresa;

    private String name;

    private String password;

    private Set< RoleView > roles;

    /*** FIM SPRING SECURITY **/

    private String nomeExibir;

    private Boolean senhaAtualizada = false;

    private String email;

    private Date ultimoAcesso;

    private FuncionarioView funcionario;

    private String tipoUser;


    public UserView() {

        /* Reflection instantiation */
    }


    public UserView( String name, String passwordHash ) {

        this.name = name;
        this.password = passwordHash;
    }


    /*** SPRING SECURITY **/

    public String getName() {

        return this.name;
    }


    public void setName( String name ) {

        // this.name = AppStringUtils.capitalizeFully(name);
        this.name = name;
    }


    public Set< RoleView > getRoles() {

        return this.roles;
    }


    public void setRoles( Set< RoleView > roles ) {

        this.roles = roles;
    }


    public Boolean getSenhaAtualizada() {

        return senhaAtualizada;
    }


    public void setSenhaAtualizada( Boolean senhaAtualizada ) {

        this.senhaAtualizada = senhaAtualizada;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail( String email ) {

        this.email = StringUtils.deleteWhitespace( StringUtils.lowerCase( email ) );
    }


    @Override
    public String getPassword() {

        return this.password;
    }


    public void setPassword( String password ) {

        if ( password != null ) {
            this.password = AppStringUtils.normalizeSpace( password );
        } else {
            this.password = password;
        }
    }


    @JsonIgnore
    @Override
    public Collection< ? extends GrantedAuthority > getAuthorities() {

        Set< RoleView > roles = this.getRoles();

        if ( roles == null ) {
            return Collections.emptyList();
        }

        Set< GrantedAuthority > authorities = new HashSet< GrantedAuthority >();
        for ( RoleView role : roles ) {
            authorities.add( new SimpleGrantedAuthority( role.getName() ) );
        }

        return authorities;
    }


    @Override
    public String getUsername() {

        return this.name;
    }


    @Override
    public boolean isAccountNonExpired() {

        return true;
    }


    @Override
    public boolean isAccountNonLocked() {

        return true;
    }


    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }


    @Override
    public boolean isEnabled() {

        return true;
    }


    /*** FIM SPRING SECURITY **/

    public String getNomeExibir() {

        return nomeExibir;
    }


    public void setNomeExibir( String nomeExibir ) {

        // this.nomeExibir = nomeExibir;
        this.nomeExibir = AppStringUtils.capitalizeFully( nomeExibir );
    }


    /*** FUNCOES AUXILIARES **/

    public void addRole( RoleView role ) {

        if ( this.roles == null ) {
            this.roles = new HashSet< RoleView >();
        }
        this.roles.add( role );
    }


    public Boolean containsRole( String role ) {

        Set< RoleView > roles = getRoles();
        if ( role != null && roles != null ) {
            for ( RoleView r : roles ) {
                if ( r.getName().equals( role ) ) {
                    return true;
                }
            }
        }
        return false;
    }


    @JsonSerialize( using = JsonDateSerializer.class )
    public Date getUltimoAcesso() {

        return ultimoAcesso;
    }


    @JsonDeserialize( using = CustomJsonDateDeserializer.class )
    public void setUltimoAcesso( Date ultimoAcesso ) {

        this.ultimoAcesso = ultimoAcesso;
    }


    public FuncionarioView getFuncionario() {

        return funcionario;
    }


    public void setFuncionario( FuncionarioView funcionario ) {

        this.funcionario = funcionario;
    }


    public String getTipoUser() {

        return tipoUser;
    }


    public void setTipoUser( String tipoUser ) {

        this.tipoUser = tipoUser;
    }


    @Override
    public String toString() {

        return "User [name=" + name + ", getId()=" + getId() + "]";
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        UserView other = (UserView) obj;
        if ( name == null ) {
            if ( other.name != null )
                return false;
        } else if ( !name.equals( other.name ) )
            return false;
        return true;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }
}