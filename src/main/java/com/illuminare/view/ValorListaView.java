package com.illuminare.view;


public class ValorListaView extends GenericEntityView implements Comparable< ValorListaView > {

    private Long id;

    private Long idEmpresa;

    private String nome;

    private String valor;

    private Integer ordem;


    public String getNome() {

        return nome;
    }


    public void setNome( String nome ) {

        this.nome = nome;
    }


    public String getValor() {

        return valor;
    }


    public void setValor( String valor ) {

        this.valor = valor;
    }


    public Integer getOrdem() {

        return ordem;
    }


    public void setOrdem( Integer ordem ) {

        this.ordem = ordem;
    }


    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
        result = prime * result + ( ( valor == null ) ? 0 : valor.hashCode() );
        return result;
    }


    @Override
    public boolean equals( Object obj ) {

        if ( this == obj )
            return true;
        if ( !super.equals( obj ) )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        ValorListaView other = (ValorListaView) obj;
        if ( nome == null ) {
            if ( other.nome != null )
                return false;
        } else if ( !nome.equals( other.nome ) )
            return false;
        if ( valor == null ) {
            if ( other.valor != null )
                return false;
        } else if ( !valor.equals( other.valor ) )
            return false;
        return true;
    }


    @Override
    public String toString() {

        return "ValorLista [nome=" + nome + ", valor=" + valor + ", ordem=" + ordem + ", getId()=" + getId() + "]";
    }


    @Override
    public int compareTo( ValorListaView other ) {

        if ( this.ordem == null || other == null ) {
            return -1;
        }
        if ( this.ordem < other.ordem ) {
            return -1;
        }
        if ( this.ordem > other.ordem ) {
            return 1;
        }
        return 0;
    }


    public Long getId() {

        return id;
    }


    public void setId( Long id ) {

        this.id = id;
    }


    public Long getIdEmpresa() {

        return idEmpresa;
    }


    public void setIdEmpresa( Long idEmpresa ) {

        this.idEmpresa = idEmpresa;
    }
}
