'use strict';
/* jshint undef: true, unused: true */
/* global moment, google */


/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
  .module('frontendApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap',
    'ngTable',
    'ui.select',
    'ui.utils',
    'ui.utils.masks',
    'checklist-model',
    'ngStorage',
    //'angular.filter',
    'ngFileUpload',
    'googlechart'
  ])
  .config(function($routeProvider, $httpProvider, $locationProvider, datepickerPopupConfig) {

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })

      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })

      .when('/acesso-negado', {
        templateUrl: 'views/acesso-negado.html',
        controller: 'AcessoNegadoCtrl'
      })

      .when('/funcionario/home', {
        templateUrl: 'views/funcionario/home.html',
        controller: 'FuncionarioHomeCtrl'
      })

      .when('/funcionario/cadastro', {
        templateUrl: 'views/funcionario/cadastro.html',
        controller: 'FuncionarioCadastroCtrl'
      })
      .when('/funcionario/pesquisa', {
        templateUrl: 'views/funcionario/pesquisa.html',
        controller: 'FuncionarioPesquisaCtrl'
      })
      .when('/funcionario/:id', {
        templateUrl: 'views/funcionario/cadastro.html',
        controller: 'FuncionarioCadastroCtrl'
      })

      .when('/grupo-ocupacional/cadastro', {
        templateUrl: 'views/grupo-ocupacional/cadastro.html',
        controller: 'GrupoOcupacionalCadastroCtrl'
      })
      .when('/grupo-ocupacional/pesquisa', {
        templateUrl: 'views/grupo-ocupacional/pesquisa.html',
        controller: 'GrupoOcupacionalPesquisaCtrl'
      })
      .when('/grupo-ocupacional/:id', {
        templateUrl: 'views/grupo-ocupacional/cadastro.html',
        controller: 'GrupoOcupacionalCadastroCtrl'
      })

      .when('/cargo/cadastro', {
        templateUrl: 'views/cargo/cadastro.html',
        controller: 'CargoCadastroCtrl'
      })
      .when('/cargo/pesquisa', {
        templateUrl: 'views/cargo/pesquisa.html',
        controller: 'CargoPesquisaCtrl'
      })
      .when('/cargo/:id', {
        templateUrl: 'views/cargo/cadastro.html',
        controller: 'CargoCadastroCtrl'
      })

      .when('/setor/cadastro', {
        templateUrl: 'views/setor/cadastro.html',
        controller: 'SetorCadastroCtrl'
      })
      .when('/setor/pesquisa', {
        templateUrl: 'views/setor/pesquisa.html',
        controller: 'SetorPesquisaCtrl'
      })
      .when('/setor/:id', {
        templateUrl: 'views/setor/cadastro.html',
        controller: 'SetorCadastroCtrl'
      })

      .when('/pesquisa/home', {
        templateUrl: 'views/pesquisa/home.html',
        controller: 'PesquisaHomeCtrl'
      }).when('/pesquisa/cadastro', {
        templateUrl: 'views/pesquisa/cadastro.html',
        controller: 'PesquisaCadastroCtrl'
      })
      .when('/pesquisa/pesquisa', {
        templateUrl: 'views/pesquisa/pesquisa.html',
        controller: 'PesquisaPesquisaCtrl'
      })
      .when('/pesquisa/:id', {
        templateUrl: 'views/pesquisa/cadastro.html',
        controller: 'PesquisaCadastroCtrl'
      })

      .when('/grupo-questao/cadastro', {
        templateUrl: 'views/grupo-questao/cadastro.html',
        controller: 'GrupoQuestaoCadastroCtrl'
      })
      .when('/grupo-questao/pesquisa', {
        templateUrl: 'views/grupo-questao/pesquisa.html',
        controller: 'GrupoQuestaoPesquisaCtrl'
      })
      .when('/grupo-questao/:id', {
        templateUrl: 'views/grupo-questao/cadastro.html',
        controller: 'GrupoQuestaoCadastroCtrl'
      })

      .when('/questao/cadastro', {
        templateUrl: 'views/questao/cadastro.html',
        controller: 'QuestaoCadastroCtrl'
      })
      .when('/questao/pesquisa', {
        templateUrl: 'views/questao/pesquisa.html',
        controller: 'QuestaoPesquisaCtrl'
      })
      .when('/questao/:id', {
        templateUrl: 'views/questao/cadastro.html',
        controller: 'QuestaoCadastroCtrl'
      })

      .when('/questionario/cadastro', {
        templateUrl: 'views/questionario/cadastro.html',
        controller: 'QuestionarioCadastroCtrl'
      })
      .when('/questionario/pesquisa', {
        templateUrl: 'views/questionario/pesquisa.html',
        controller: 'QuestionarioPesquisaCtrl'
      })
      .when('/questionario/:id', {
        templateUrl: 'views/questionario/cadastro.html',
        controller: 'QuestionarioCadastroCtrl'
      })

      .when('/listavalores/cadastro', {
        templateUrl: 'views/listavalores/cadastro.html',
        controller: 'ListaValoresCadastroCtrl'
      })
      .when('/listavalores/pesquisa', {
        templateUrl: 'views/listavalores/pesquisa.html',
        controller: 'ListaValoresPesquisaCtrl'
      })
      .when('/listavalores/:id', {
        templateUrl: 'views/listavalores/cadastro.html',
        controller: 'ListaValoresCadastroCtrl'
      })

      .when('/avaliacao/:idPesquisa/:idAvaliador/:idAvaliado', {
        templateUrl: 'views/avaliacao/avaliacao.html',
        controller: 'AvaliacaoCadastroCtrl'
      })

      .when('/admin/home', {
        templateUrl: 'views/admin/home.html',
        controller: 'AdminHomeCtrl'
      })
      .when('/user/cadastro', {
        templateUrl: 'views/user/cadastro.html',
        controller: 'UserCadastroCtrl'
      })
      .when('/user/pesquisa', {
        templateUrl: 'views/user/pesquisa.html',
        controller: 'UserPesquisaCtrl'
      })
      .when('/user/:id', {
        templateUrl: 'views/user/cadastro.html',
        controller: 'UserCadastroCtrl'
      })

      .when('/resultado/:idAvaliado', {
        templateUrl: 'views/resultado/resultado.html',
        controller: 'ResultadoCtrl'
      })
      .when('/resultado/byGrupoOcupacional/:idGrupoOcupacional', {
        templateUrl: 'views/resultado/resultado.html',
        controller: 'ResultadoCtrl'
      })
      .when('/resultado/byCargo/:idCargo', {
        templateUrl: 'views/resultado/resultado.html',
        controller: 'ResultadoCtrl'
      })
      .when('/resultado/bySetor/:idSetor', {
        templateUrl: 'views/resultado/resultado.html',
        controller: 'ResultadoCtrl'
      })
      .when('/resultado/byEmpresa/:idEmpresa', {
        templateUrl: 'views/resultado/resultado.html',
        controller: 'ResultadoCtrl'
      })

      .when('/associacao-manual/associar', {
        templateUrl: 'views/associacao-manual/associacao.html',
        controller: 'AssociacaoCtrl'
      })

      .when('/import/funcionario', {
        templateUrl: 'views/import/import-funcionario.html',
        controller: 'ImportFuncionarioCtrl'
      })

      .when('/resultado-admin/:idPesquisa', {
        templateUrl: 'views/resultado/resultado.html',
        controller: 'ResultadoAdminCtrl'
      })
      .when('/resultado-admin/funcionarios/:idPesquisa', {
        templateUrl: 'views/resultado-admin/funcionarios.html',
        controller: 'ResultadoAdminCtrl'
      })

      .when('/respostas/:idAvaliacao', {
        templateUrl: 'views/respostas/respostas.html',
        controller: 'RespostasCtrl'
      })

      .when('/empresa/cadastro', {
        templateUrl: 'views/empresa/cadastro.html',
        controller: 'EmpresaCadastroCtrl'
      })
      .when('/empresa/pesquisa', {
        templateUrl: 'views/empresa/pesquisa.html',
        controller: 'EmpresaPesquisaCtrl'
      })
      .when('/empresa/:id', {
        templateUrl: 'views/empresa/cadastro.html',
        controller: 'EmpresaCadastroCtrl'
      })


      .otherwise({
        redirectTo: '/'
      });


    datepickerPopupConfig.currentText = 'Hoje';
    datepickerPopupConfig.clearText = 'Apagar';

    Date.prototype.toISOString = function() {
      return moment(this).format('YYYY-MM-DD');
    };

    /* Register error provider that shows message on failed requests or redirects to login page on
     * unauthenticated requests */
    $httpProvider.interceptors.push(function($q, $rootScope, $location, MsgService) {
      return {
        'responseError': function(rejection) {
          $('.loadingApp').hide();
          var status = rejection.status;
          var config = rejection.config;
          var method = config.method;
          var url = config.url;

          console.log('response error code: ' + status);
          console.log(rejection);

          if (status == 400) {
            var msg = rejection.data.msg ? rejection.data.msg : rejection.data;
            MsgService.addFailed(msg);

          } else if (status == 401) {
            MsgService.addFailed('Sessão expirada.');
            $rootScope.clearCookiesUser();
            $rootScope.previousPage = $location.path();
            $location.path("/login");

          } else if (status == 403) {

            console.log('acesso negado for the url:' + url);
            $location.path("/acesso-negado");

          } else {
            $rootScope.error = method + " on " + url + " failed with status " + status;
            console.log('erro ao processar solicitacao');
            console.log(rejection.data);
            MsgService.addFailed('Erro interno ao processar solicitação');
          }

          return $q.reject(rejection);
        }
      };
    });



    /* Registers auth token interceptor, auth token is either passed by header or by query parameter
     * as soon as there is an authenticated user */
    $httpProvider.interceptors.push(function($q, $rootScope, $location, $sessionStorage, $cookieStore) {
      return {
        'request': function(config) {
          var isRestCall = config.url.indexOf(APP_CONFIG.prefixURLRest) == 0;
          //if (isRestCall && angular.isDefined($rootScope.authToken)) {

          var authToken = $cookieStore.get('authToken');
          if (angular.isDefined(authToken)) {
            //var authToken = $cookieStore.get(authToken);
            if (APP_CONFIG.useAuthTokenHeader) {
              config.headers['X-Auth-Token'] = authToken;
            } else {
              config.url = config.url + "?token=" + authToken;
            }
          }
          return config || $q.when(config);
        }
      };
    });



  }).run(function($rootScope, $location, $cookieStore, UserService, $sessionStorage, $timeout, EmpresaService) {

    /* Reset error when a new view is loaded */
    $rootScope.$on('$viewContentLoaded', function() {
      delete $rootScope.error;
    });


    $rootScope.clearCookiesUser = function() {
      $cookieStore.remove('authToken');
      $cookieStore.remove('user');
      $cookieStore.remove('idEmpresaUser');
      $rootScope.user = undefined;
    };

    $rootScope.hasRole = function(role) {
      var user;
      if ($rootScope.user) {
        user = $rootScope.user;
      } else {
        user = $cookieStore.get('user');
      }
      if (!user) {
        /*if (user) {
          $rootScope.user = user;
        } else {
          return false;
        }*/
        //return false;
        $location.path("/login");
        return;
      }

      if (!role) {
        return false;
      }

      var hasRole = false;
      if (user.roles) {
        for (var i = 0; i < user.roles.length; i++) {
          var roleUser = user.roles[i];
          if (roleUser.name === role) {
            hasRole = true;
            break;
          }
        }
      }

      return hasRole;
    };

    $rootScope.logout = function() {
      delete $rootScope.user;
      delete $rootScope.authToken;
      $cookieStore.remove('authToken');
      $rootScope.limparCookies();
      $rootScope.clearCookiesUser();
      $rootScope.limparRootScope();
      $location.path("/login");
      //location.reload();
      $timeout(function() {
        location.reload();
      });
    };

    $rootScope.limparCookies = function() {
      $cookieStore.remove('user');
      $cookieStore.remove('idEmpresaUser');
    };

    $rootScope.limparRootScope = function() {
      $rootScope.pesquisaResponder = undefined;
      $rootScope.avaliacoesEmAberto = undefined;
      $rootScope.pesquisasEmAberto = undefined;
      $rootScope.pesquisasFechadas = undefined;
    };


    var originalPath = $location.path();

    var user = $cookieStore.get('user');
    var idEmpresaUser = $cookieStore.get('idEmpresaUser');
    var token = $cookieStore.get('authToken');
    if (user !== undefined && token !== undefined) {
      $rootScope.user = user;
      $rootScope.authToken = token;
      $rootScope.idEmpresaUser = idEmpresaUser;

      $location.path(originalPath);

    } else {

      $rootScope.limparCookies();
      $rootScope.clearCookiesUser();
      $location.path("/login");

    }

    $timeout(function() {
      $('body').css('background', 'none');
      $('.containerMain').fadeIn();
    }, 100);

    $rootScope.initialized = true;

    $rootScope.focus = function(fieldName) {
      $timeout(function() {
        $(fieldName).focus();
      }, 300);
    };

    $rootScope.scrollTo = function(fieldName, negativeNumber, timeout) {
      if (!negativeNumber) {
        negativeNumber = 5;
      }
      if (!timeout) {
        timeout = 0;
      }

      $timeout(function() {
        $("body").animate({
          scrollTop: ($(fieldName).offset().top - negativeNumber)
        }, "fast");
      }, timeout);
    };

    $rootScope.click = function(fieldName) {
      $timeout(function() {
        $(fieldName).click();
      }, 300);
    };

    $rootScope.clickCombo = function(fieldName) {
      $rootScope.click(fieldName + ' .ui-select-toggle');
    };

    $rootScope.removeMsgSuccess = function(index) {
      $rootScope.msgsSuccess.splice(index, 1);
    };

    $rootScope.removeMsgFailed = function(index) {
      $rootScope.msgsFailed.splice(index, 1);
    };

    $rootScope.sexos = [{
      label: 'Masculino',
      value: 'M'
    }, {
      label: 'Feminino',
      value: 'F'
    }];

    $rootScope.simNao = [{
      label: 'Não',
      value: false
    }, {
      label: 'Sim',
      value: true
    }];

    $rootScope.label = 'label';
    $rootScope.value = 'value';
    $rootScope.required = true;
    $rootScope.showMsgsError = false;
    $rootScope.activeMenu = 'main';

    //$rootScope.isFuncionario = false;
    //$rootScope.isGestor = false;
    //if ($rootScope.user) {
    //$rootScope.isFuncionario = $rootScope.user.funcionario && $rootScope.user.funcionario.id;
    //}
    $rootScope.hasRoleAdmin = $rootScope.hasRole('role_admin');
    $rootScope.isFuncionario = $rootScope.hasRole('role_funcionario');
    $rootScope.isGestor = $rootScope.hasRole('role_gestor');
    $rootScope.isGestorSetor = $rootScope.hasRole('role_gestor_setor');

    if (!$rootScope.empresas || $rootScope.empresas.length === 0) {
      $rootScope.empresas = EmpresaService.lista();
    }


  });