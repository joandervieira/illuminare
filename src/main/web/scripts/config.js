var APP_CONFIG = {
	useAuthTokenHeader: true,
	prefixURLRest: 'rest',
	urlPrefix: '/illuminare'
};