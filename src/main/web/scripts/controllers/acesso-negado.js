'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AcessoNegadoCtrl
 * @description
 * # AcessoNegadoCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('AcessoNegadoCtrl', function($scope, $interval, $rootScope, $location, $modal, $timeout,
		MsgService, AvaliacaoService, GridUtilsService, ngTableParams) {


		document.title = 'Conecthare';
		$rootScope.activeMenu = 'main';

		MsgService.cleanAll(true);
		$('.loadingApp').hide();



	});