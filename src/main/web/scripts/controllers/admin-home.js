'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AdminHomeCtrl
 * @description
 * # AdminHomeCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('AdminHomeCtrl', function($scope, MsgService, $rootScope) {

		document.title = 'Administração - Conecthare';
		$rootScope.activeMenu =  'admin';


		MsgService.cleanAll(true);


	});