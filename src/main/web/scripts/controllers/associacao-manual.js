'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:Associacao
 * @description
 * # Associacao
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('AssociacaoCtrl', function ($scope, $timeout, $modal, $routeParams, $location,
		MsgService, ngTableParams, GridUtilsService, AvaliacaoService, FuncionarioService, PesquisaService, RespostaService) {

		document.title = 'Pesquisa - Conecthare';
		$scope.activeMenu = 'pesquisa';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};
		$scope.avaliacaoPesquisa = {};

		$scope.screenInit = function () {

			$scope.showMsgsError = false;

			$scope.avaliacao = {};
			$scope.avaliacaoPesquisa = {};
			$scope.avaliacoes = [];
			$scope.inserindo = false;


			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function () {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function () {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.avaliacao.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}

			}

		};


		$scope.salvar = function () {

			AvaliacaoService.associar($scope.avaliacao, function (result) {

				MsgService.addSuccess('Associação salva com sucesso', true);
				$scope.screenInit();
				$('.loadingApp').hide();
				//$scope.focus('input.nome');
				$scope.initialLoad = true;
				$scope.pesquisar();

			});


		};

		$scope.atualizar = function () {

			/*AvaliacaoService.atualizar($scope.avaliacao, function(result) {

				MsgService.addSuccess('Associação atualizada com sucesso', true);
				$('.loadingApp').hide();
				$scope.pesquisar();

			});*/


		};

		$scope.cancelarSalvar = function () {
			//$location.path('/pesquisa/home');
			$scope.inserindo = false;
		};

		$scope.inserirAssociacao = function () {
			$scope.inserindo = true;
		};


		$scope.excluirAssociacao = function (avaliacao) {
			$scope.entityToExclude = angular.copy(avaliacao);
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						var htmlReturn = 'Você deseja excluir esta Associação  ? <br> ';
						htmlReturn += 'Funcionário <strong>' + $scope.entityToExclude.funcionarioAvaliador.pessoa.nome + '</strong> avaliando Funcionário <strong>' + $scope.entityToExclude.funcionarioAvaliado.pessoa.nome + '</strong>';
						return htmlReturn;
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {
					$('.loadingApp').show();
					AvaliacaoService.excluir({
						id: $scope.entityToExclude.id
					}, function (result) {
						MsgService.addSuccess('Associação excluída com sucesso');
						$scope.initialLoad = true;

						$scope.pesquisar();

					});
				}
			});

		};

		$scope.limparFiltro = function () {
			$scope.avaliacaoPesquisa = {};
			$scope.pesquisar();
		};

		$scope.realizarPesquisar = function ($defer, params) {

			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				$scope.paginator.entity = $scope.avaliacaoPesquisa;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = '+[pessoaAvaliador].nome';

				AvaliacaoService.avaliacoesByPesquisasEmAbertoPageable($scope.paginator, function (paginator) {
					$scope.paginator = paginator;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$scope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.deletarRespostas = function (avaliacao) {
			RespostaService.deleteByAvaliacao({
				id: avaliacao.id
			}, function () {
				$scope.dataGridAssociacoes.reload();
			});
		};

		$scope.dataGridAssociacoes = new ngTableParams({
			count: 10, // count per page
		}, {
				total: 0, // length of data
				counts: [10, 50, 100],
				getData: $scope.realizarPesquisar
			});

		$scope.pesquisar = function () {
			$scope.loadDataGrid = true;
			$scope.dataGridAssociacoes.reload();
		};


		$scope.verRespostas = function (avaliacao) {
			var url = '#/respostas/' + avaliacao.id;
			window.open(url, '_blank');
			//var url = '#/resultado/';
			//var pesquisasIds = '?idsPesquisa=' + avaliacao.pesquisa.id;

		};


		//inserindo
		$scope.$watch('avaliacao.idEmpresa', function (newValue) {
			if (newValue) {
				$scope.avaliacaoPesquisa.idEmpresa = newValue;
				$scope.funcionarios = FuncionarioService.lista({
					idEmpresa: newValue,
					fieldsToFetch: ['pessoa']
				});
				$scope.findPesquisasEmAberto(newValue);
				$scope.findTodasPesquisas(newValue);
			}
		});

		//pesquisa
		$scope.$watch('avaliacaoPesquisa.idEmpresa', function (newValue) {
			if (newValue) {
				$scope.avaliacao.idEmpresa = newValue;
				$scope.funcionarios = FuncionarioService.lista({
					idEmpresa: newValue,
					fieldsToFetch: ['pessoa']
				});
				$scope.findPesquisasEmAberto(newValue);
				$scope.findTodasPesquisas(newValue);
			}
		});

		$scope.findPesquisasEmAberto = function (idEmpresa) {
			$scope.pesquisasEmAberto = PesquisaService.abertas({
				idEmpresa: idEmpresa
			});
		}
		$scope.findTodasPesquisas = function (idEmpresa) {
			$scope.todasPesquisas = PesquisaService.lista({
				idEmpresa: idEmpresa
			});
		}



	});