'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AvaliacaoCadastroCtrl
 * @description
 * # AvaliacaoCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('AvaliacaoCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, AvaliacaoService, RespostaService, $window) {

		document.title = 'Avaliação - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.controle = {
			indexQuestionarios: null
		};

		$scope.onscroll = function() {
			var me = {};
			var elements = angular.element('.formAvaliacao .avaliacoes .nomeGrupoQuestao');
			elements.removeClass('scrolling');
			var totalElements = elements.length;
			elements.each(function(i, obj) {

				var jObj = angular.element(obj);
				var offset = jObj.offset();
				var w = angular.element(window);
				//console.log("(x,y): (" + (offset.left - w.scrollLeft()) + "," + (offset.top - w.scrollTop()) + ")");
				var currentOffsetY = (offset.top - w.scrollTop());
				//console.log(currentOffsetY);


				if (currentOffsetY < 0) {
					me.lastNegative = jObj;
				}

				if (currentOffsetY < 250 && currentOffsetY >= 0) {
					elements.removeClass('scrolling');
					return false;
				} else {

					if (totalElements === (i + 1)) {
						//console.log('the last is:'+jObj.text());
						if (!!me.lastNegative) {
							me.lastNegative.addClass('scrolling');
							return false;
						}
					}

				}

			});
		};

		window.onscroll = function() {
			$scope.onscroll();
		};

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.avaliacao = {};
			$scope.avaliacao.ativo = true;
			$scope.respostasAvaliacoes = [];
			$scope.avaliacoes = [];
			$scope.nomeAvaliado = '';
			$scope.sexoAvaliado = 'M';

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function(callBack) {

			//console.log($scope.avaliacoes);

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				$scope.formatarEnvioRespostas();

				if (!!$scope.avaliacao.id || !!$scope.avaliacoes[0].id) {

					$scope.atualizar(callBack);

				} else {

					$scope.salvar(callBack);

				}

			}

		};

		$scope.formatarEnvioRespostas = function() {
			$scope.respostas = [];
			var indexRespostas = 0;

			var avaliacao = $scope.avaliacoes[$scope.controle.indexQuestionarios];

			//for (var i = 0; i < $scope.avaliacoes.length; i++) {

			//var avaliacao = $scope.avaliacoes[i];

			var grupoQuestao = $scope.avaliacoes[$scope.controle.indexQuestionarios].questionario.gruposQuestoes[$scope.controle.indexGrupoQuestao];

			//for (var j = 0; j < avaliacao.questionario.gruposQuestoes.length; j++) {
			//	var grupoQuestao = avaliacao.questionario.gruposQuestoes[j];

			for (var k = 0; k < grupoQuestao.questoesOrdem.length; k++) {
				var questaoOrdem = grupoQuestao.questoesOrdem[k];
				var questao = questaoOrdem.questao;

				var idRespostaValorLista = questao.respostaValorLista ? questao.respostaValorLista.id : undefined;
				var respostaValorLista = idRespostaValorLista ? {
					id: idRespostaValorLista
				} : undefined;

				$scope.respostas[indexRespostas] = {
					id: questao.respostaId,
					respostaNumerico: questao.respostaNumerico,
					respostaTexto: questao.respostaTexto,
					respostaValorLista: respostaValorLista,
					idGrupoQuestao: grupoQuestao.id,
					questao: {
						id: questao.id
					},
					questionarioAvaliacao: {
						id: avaliacao.id
					}
				};
				indexRespostas++;
			}

			//};

			//};
		};

		$scope.salvar = function(callBack) {

			RespostaService.salvarTodos($scope.respostas, function(result) {

				MsgService.addSuccess('Avaliação salva com sucesso', true);
				$scope.screenInit();
				$('.loadingApp').hide();
				//$scope.focus('input.nome');
				//$location.path('/');
				if (callBack) {
					callBack();
				}
			});


		};

		$scope.atualizar = function(callBack) {

			RespostaService.atualizarTodos($scope.respostas, function(result) {

				MsgService.addSuccess('Avaliação atualizada com sucesso', true);
				$('.loadingApp').hide();
				//$location.path('/avaliacao/pesquisa');
				if (callBack) {
					callBack();
				}

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/pesquisa/home');
		};

		$scope.selecionarQuestionario = function(index) {
			$scope.controle.indexQuestionarios = index;
			$scope.controle.indexGrupoQuestao = 0;

			var avaliacao = $scope.avaliacoes[$scope.controle.indexQuestionarios];
			var gruposQuestoes = avaliacao.questionario.gruposQuestoes;

			var todosGruposRespondidos = true;
			for (var j = 0; j < gruposQuestoes.length; j++) {
				var grupo = gruposQuestoes[j];
				var todosRespondidos = true;
				for (var i = 0; i < grupo.questoesOrdem.length; i++) {
					var respostaNumerico = grupo.questoesOrdem[i].questao.respostaNumerico;
					var respostaTexto = grupo.questoesOrdem[i].questao.respostaTexto;
					var respostaValorLista = grupo.questoesOrdem[i].questao.respostaValorLista;

					if (!respostaNumerico && !respostaTexto && (!respostaValorLista || !respostaValorLista.id)) {
						todosRespondidos = false;
						todosGruposRespondidos = false;
					}
				}
				$scope.avaliacoes[$scope.controle.indexQuestionarios].questionario.gruposQuestoes[j].respondido = todosRespondidos;

				if (todosRespondidos) {
					$scope.controle.indexGrupoQuestao = $scope.controle.indexGrupoQuestao + 1;
				}
			}

			if (todosGruposRespondidos) {
				$scope.controle.indexGrupoQuestao = 0;
			}

			$timeout(function() {
				$scope.scrollToQuestionarios(200);
			});

		};

		$scope.callBackProximoGrupoQuestao = function() {

			var lengthGrupoQuestoes = $scope.avaliacoes[$scope.controle.indexQuestionarios].questionario.gruposQuestoes.length;

			var nextIndex = $scope.controle.indexGrupoQuestao + 1;

			$scope.controle.indexGrupoQuestao = nextIndex

			$scope.scrollToQuestionarios();

		};

		$scope.callBackFinalizarQuestionario = function() {

			var avaliacao = $scope.avaliacoes[$scope.controle.indexQuestionarios];

			var index = $scope.controle.indexQuestionarios;
			RespostaService.questionarioRespondido({
				idQuestionario: avaliacao.questionario.id,
				idPesquisa: avaliacao.pesquisa.id,
				idAvaliado: avaliacao.funcionarioAvaliado.id,
				idAvaliador: avaliacao.funcionarioAvaliador.id
			}, function(result) {
				$scope.avaliacoes[index].questionarioRespondido = result;
				//$window.location.reload();
				var avaliacoesTmp = $scope.avaliacoes;
				$scope.avaliacoes = [];
				$timeout(function() {
					$scope.avaliacoes = avaliacoesTmp;
				});

			});

			$scope.controle.indexGrupoQuestao = null;
			$scope.controle.indexQuestionarios = null;
			$scope.scrollToQuestionarios();

		};

		$scope.scrollToQuestionarios = function(time) {
			time = time || 0;
			$('html, body').animate({
				scrollTop: $(".titleQuestionario").offset().top
			}, time);
		};

		$scope.proximoGrupoQuestao = function() {

			if (!$scope.formIsInvalid()) {



				var lengthGrupoQuestoes = $scope.avaliacoes[$scope.controle.indexQuestionarios].questionario.gruposQuestoes.length;

				var nextIndex = $scope.controle.indexGrupoQuestao + 1;

				var callBack;

				if (lengthGrupoQuestoes > nextIndex) {

					callBack = $scope.callBackProximoGrupoQuestao;

				} else {

					callBack = $scope.callBackFinalizarQuestionario;

				}

				$scope.salvarAtualizar(callBack);

			}
		};


		if ($routeParams.idPesquisa && $routeParams.idAvaliador && $routeParams.idAvaliado) {

			$('.loadingApp').show();
			AvaliacaoService.getAvaliacao({
				idPesquisa: $routeParams.idPesquisa,
				idAvaliador: $routeParams.idAvaliador,
				idAvaliado: $routeParams.idAvaliado
			}, function(avaliacoes) {

				$scope.avaliacoes = avaliacoes;

				if (avaliacoes[0].funcionarioAvaliado && avaliacoes[0].funcionarioAvaliado.pessoa) {

					$scope.nomeAvaliado = avaliacoes[0].funcionarioAvaliado.pessoa.nome
					$scope.sexoAvaliado = avaliacoes[0].funcionarioAvaliado.pessoa.sexo === 'F' ? 'F' : 'M';

				} else if ($scope.user.funcionario) {

					$scope.nomeAvaliado = $scope.user.funcionario.pessoa.nome
					$scope.sexoAvaliado = $scope.user.funcionario.pessoa.sexo === 'F' ? 'F' : 'M';
				}


				if ($scope.avaliacoes && $scope.avaliacoes.length > 0) {
					for (var i = 0; i < $scope.avaliacoes.length; i++) {

						var av = $scope.avaliacoes[i];

						$scope.avaliacoes[i].questionarioRespondido = RespostaService.questionarioRespondido({
							idQuestionario: av.questionario.id,
							idPesquisa: av.pesquisa.id,
							idAvaliado: av.funcionarioAvaliado.id,
							idAvaliador: av.funcionarioAvaliador.id
						});

						var newGruposQuestoes = [];
						if (av.questionario.gruposQuestoes && av.questionario.gruposQuestoes.length > 0) {
							for (var j = 0; j < av.questionario.gruposQuestoes.length; j++) {
								var grupoQuestao = av.questionario.gruposQuestoes[j];
								var newQuestoesOrdem = [];
								if (grupoQuestao.questoesOrdem && grupoQuestao.questoesOrdem.length > 0) {
									for (var k = 0; k < grupoQuestao.questoesOrdem.length; k++) {
										var questaoOrdem = grupoQuestao.questoesOrdem[k];
										if (questaoOrdem.questao.classificacao <= 2) {
											newQuestoesOrdem.push(questaoOrdem);
										}
									}
								}
								grupoQuestao.questoesOrdem = newQuestoesOrdem;
								if (grupoQuestao.questoesOrdem.length > 0) {
									newGruposQuestoes.push(grupoQuestao);
								}
							}
						}
						av.questionario.gruposQuestoes = newGruposQuestoes;
					}
				}

				$('.loadingApp').hide();

			});



		}

	});