'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:CargoCadastroCtrl
 * @description
 * # CargoCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('CargoCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, GrupoOcupacionalService, CargoService) {

		document.title = 'Cadastro Cargo - Conecthare';
		$rootScope.activeMenu = 'funcionario';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.cargo = {};
			$scope.cargo.ativo = true;

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.cargo.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}

			}

		};

		$scope.salvar = function() {

			CargoService.salvar($scope.cargo, function(result) {

				MsgService.addSuccess('Cargo salvo com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});


		};

		$scope.atualizar = function() {

			CargoService.atualizar($scope.cargo, function(result) {

				MsgService.addSuccess('Cargo atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/cargo/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/funcionario/home');
		};


		//$scope.gruposOcupacionais = GrupoOcupacionalService.lista();

		$scope.$watch('cargo.idEmpresa', function(newValue) {
			if (newValue) {
				$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
					idEmpresa: newValue
				});
			}
		});


		//EDITANDO
		if ($routeParams.id) {
			$scope.cargo = CargoService.get({
				id: $routeParams.id,
				fieldsToFetch: ['grupoOcupacional']
			});

		}

	});