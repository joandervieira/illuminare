'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:CargoPesquisaCtrl
 * @description
 * # CargoPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('CargoPesquisaCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, CargoService, GrupoOcupacionalService) {

		document.title = 'Pesquisa Cargo - Conecthare';
		$rootScope.activeMenu = 'funcionario';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.cargo = {};

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function() {
			$scope.cargo = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function() {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();
				if ($rootScope.idEmpresaUser) {
					$scope.cargo.idEmpresa = $rootScope.idEmpresaUser;
				}

				$scope.paginator.entity = $scope.cargo;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = ['grupoOcupacional'];

				CargoService.pesquisar($scope.paginator, function(paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: $scope.realizarPesquisar
		});


		$scope.excluir = function(entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function() {
						return 'Você deseja excluir este Cargo ? <br> <strong>' + $scope.entityToExclude.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function(confirmed) {
				if (confirmed) {

					CargoService.excluir({
						id: $scope.entityToExclude.id
					}, function(result) {
						MsgService.addSuccess('Cargo excluído com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};

		$scope.verPesquisasCargo = function(entity) {
			var idEmpresa = entity.idEmpresa;
			if (!idEmpresa) {
				idEmpresa = $rootScope.idEmpresa;
			}
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/pesquisas-resultado.html',
				controller: 'PesquisaPesquisaModalCtrl',
				size: 'lg',
				windowClass: 'modalPesquisasResultado',
				resolve: {
					avaliadoResultado: function() {
						return {
							idEmpresa: idEmpresa
						};
					},
					idGrupoOcupacional: function() {
						return undefined;
					},
					idCargo: function() {
						return entity.id;
					},
					idSetor: function() {
						return undefined;
					},
					idEmpresa: function() {
						return null;
					}
				}

			});

			modalInstance.result.then(function(result) {
				// do nothing
			});

		};

		$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
			idEmpresa: $rootScope.idEmpresaUser
		});

	});