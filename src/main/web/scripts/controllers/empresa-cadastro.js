'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:EmpresaCadastroCtrl
 * @description
 * # EmpresaCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('EmpresaCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, EmpresaService) {

		document.title = 'Cadastro Empresa - Conecthare';
		$rootScope.activeMenu = 'admin';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.empresa = {};
			$scope.empresa.ativo = true;

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.empresa.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}

		};

		$scope.salvar = function() {

			EmpresaService.salvar($scope.empresa, function(result) {

				MsgService.addSuccess('Empresa salva com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$location.path('/empresa/pesquisa');
				//$scope.focus('input.nome');
				$timeout(function() {
					location.reload();
				}, 500);
			});


		};

		$scope.atualizar = function() {

			EmpresaService.atualizar($scope.empresa, function(result) {

				MsgService.addSuccess('Empresa atualizada com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/empresa/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/empresa/pesquisa');
		};



		//EDITANDO
		if ($routeParams.id) {
			$scope.empresa = EmpresaService.get({
				id: $routeParams.id,
				fieldsToFetch: []
			});

		}

	});