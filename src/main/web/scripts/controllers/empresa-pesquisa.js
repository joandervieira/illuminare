'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:EmpresaPesquisaCtrl
 * @description
 * # EmpresaPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('EmpresaPesquisaCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, EmpresaService) {

		document.title = 'Pesquisa Empresa - Conecthare';
		$rootScope.activeMenu = 'admin';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.empresa = {};

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function() {
			$scope.empresa = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function() {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				$scope.paginator.entity = $scope.empresa;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = [];

				EmpresaService.pesquisar($scope.paginator, function(paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: $scope.realizarPesquisar
		});


		$scope.excluir = function(entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function() {
						return 'Você deseja excluir esta Empresa ? <br> <strong>' + $scope.entityToExclude.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function(confirmed) {
				if (confirmed) {

					EmpresaService.excluir({
						id: $scope.entityToExclude.id
					}, function(result) {
						MsgService.addSuccess('Empresa excluída com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();
						$timeout(function() {
							location.reload();
						}, 500);

					});
				}
			});

		};

		$scope.verPesquisas = function(entity) {
			var idEmpresa = entity.id;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/pesquisas-resultado.html',
				controller: 'PesquisaPesquisaModalCtrl',
				size: 'lg',
				windowClass: 'modalPesquisasResultado',
				resolve: {
					avaliadoResultado: function() {
						return {
							idEmpresa: idEmpresa
						};
					},
					idGrupoOcupacional: function() {
						return undefined;
					},
					idCargo: function() {
						return undefined;
					},
					idSetor: function() {
						return undefined;
					},
					idEmpresa: function() {
						return idEmpresa;
					}
				}

			});

			modalInstance.result.then(function(result) {
				// do nothing
			});

		};


	});