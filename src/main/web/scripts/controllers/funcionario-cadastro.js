'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:FuncionarioCadastroCtrl
 * @description
 * # FuncionarioCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('FuncionarioCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, EstadoCivilService, TipoEnderecoEletronicoService, TipoEnderecoService, TipoTelefoneService, EstadoService, CidadeService, FuncionarioService,
		CargoService, SetorService) {

		document.title = 'Cadastro de Funcionário - Conecthare';
		$rootScope.activeMenu = 'funcionario';


		$scope.cidades = [];
		$scope.lideres = [];
		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.funcionario = {};
			$scope.funcionario.ativo = true;
			$scope.funcionario.pessoa = {};
			$scope.funcionario.pessoa.ativo = true;

			$scope.hoje = new Date();
			$scope.ontem = new Date($scope.hoje);
			$scope.ontem.setDate($scope.hoje.getDate() - 1);

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.funcionario.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}

		};

		$scope.salvar = function() {

			FuncionarioService.salvar($scope.funcionario, function(result) {

				MsgService.addSuccess('Funcionário salvo com sucesso');
				$scope.screenInit();
				$scope.funcionario.pessoa.enderecos = [{}];
				$scope.funcionario.pessoa.enderecosEletronicos = [{}];
				$scope.funcionario.pessoa.telefones = [{}];
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});

		};

		$scope.atualizar = function() {

			FuncionarioService.atualizar($scope.funcionario, function(result) {

				MsgService.addSuccess('Funcionário atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/funcionario/pesquisa');

			});

		};

		$scope.cancelarSalvar = function() {
			$location.path('/funcionario/home');
		};

		$scope.carregarCidades = function(estado) {
			$scope.cidades = CidadeService.findByEstado({
				idEstado: estado.id
			});
		};

		$scope.carregarEstadosCidades = function() {
			if (!$scope.estados || $scope.estados.length == 0) {
				$scope.estados = EstadoService.lista({}, function() {
					if ($scope.funcionario.pessoa.enderecos && $scope.funcionario.pessoa.enderecos.length > 0) {
						for (var i = 0; i < $scope.funcionario.pessoa.enderecos.length; i++) {
							var end = $scope.funcionario.pessoa.enderecos[i];
							if (end.cidade && end.cidade.estado) {
								$scope.funcionario.pessoa.enderecos[i].estado = end.cidade.estado;
								$scope.carregarCidades(end.cidade.estado);
							}
						}
					}
				});
			} else {
				if ($scope.funcionario.pessoa.enderecos && $scope.funcionario.pessoa.enderecos.length > 0) {
					for (var i = 0; i < $scope.funcionario.pessoa.enderecos.length; i++) {
						var end = $scope.funcionario.pessoa.enderecos[i];
						if (end.cidade && end.cidade.estado) {
							$scope.funcionario.pessoa.enderecos[i].estado = end.cidade.estado;
							$scope.carregarCidades(end.cidade.estado);
						}
					}
				}
			}
		};


		$scope.estadosCivis = EstadoCivilService.lista();
		$scope.tiposEnderecos = TipoEnderecoService.lista();
		$scope.tiposEnderecosEletronicos = TipoEnderecoEletronicoService.lista();
		$scope.tiposTelefones = TipoTelefoneService.lista();
		$scope.carregarEstadosCidades();


		$scope.$watch('funcionario.idEmpresa', function(newValue) {
			if (newValue) {
				$scope.cargos = CargoService.lista({
					idEmpresa: newValue
				});
				$scope.setores = SetorService.lista({
					idEmpresa: newValue
				});
				$scope.pesquisarLideres(newValue);
			}
		});


		$scope.pesquisarLideres = function(idEmpresa) {

			$scope.paginatorLideres = {
				entity: {
					idEmpresa: idEmpresa
				},
				orderBy: '-pessoa.nome',
				fieldsToFetch: ['pessoa']
			};

			FuncionarioService.pesquisar($scope.paginatorLideres, function(paginatorLideres) {
				$scope.paginatorLideres = paginatorLideres;
				$scope.lideres = $scope.paginatorLideres.result;
			});
		};

		//$scope.pesquisarLideres();



		//EDITANDO
		if ($routeParams.id) {
			//var fieldsToFetch = ['cargo', 'setor', 'pessoa p', 'p.enderecos', 'p.telefones', 'p.enderecosEletronicos', 'p.estadoCivil'];
			//fieldsToFetch.push('p.');
			FuncionarioService.get({
				id: $routeParams.id,
				fieldsToFetch: ['cargo', 'setor', 'pessoa p', '[p].enderecos', '[p].telefones', '[p].enderecosEletronicos', '[p].estadoCivil', 'lider l', '[l].pessoa']
			}, function(funcionario) {
				$scope.funcionario = funcionario;
				$scope.carregarEstadosCidades();
				if ($scope.funcionario.pessoa) {
					if (!$scope.funcionario.pessoa.enderecos || $scope.funcionario.pessoa.enderecos.length === 0) {
						$scope.funcionario.pessoa.enderecos = [{}];
					}
					if (!$scope.funcionario.pessoa.enderecosEletronicos || $scope.funcionario.pessoa.enderecosEletronicos.length === 0) {
						$scope.funcionario.pessoa.enderecosEletronicos = [{}];
					}
					if (!$scope.funcionario.pessoa.telefones || $scope.funcionario.pessoa.telefones.length === 0) {
						$scope.funcionario.pessoa.telefones = [{}];
					}
				}
			});

		} else {
			$scope.funcionario.pessoa.enderecos = [{}];
			$scope.funcionario.pessoa.enderecosEletronicos = [{}];
			$scope.funcionario.pessoa.telefones = [{}];
		}


	});