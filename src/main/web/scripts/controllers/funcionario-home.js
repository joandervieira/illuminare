'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:FuncionarioHomeCtrl
 * @description
 * # FuncionarioHomeCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('FuncionarioHomeCtrl', function($scope, MsgService, $rootScope, $location) {

		document.title = 'Funcionário - Conecthare';
		$rootScope.activeMenu = 'funcionario';


		MsgService.cleanAll(true);

		// if ($rootScope.isGestor) {
		// 	$location.path("/funcionario/pesquisa");
		// }


	});