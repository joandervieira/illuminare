'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:FuncionarioPesquisaCtrl
 * @description
 * # FuncionarioPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('FuncionarioPesquisaCtrl', function($scope, $location, $modal, $cookieStore, $timeout, $rootScope, $routeParams,
		MsgService, ngTableParams, GridUtilsService, FuncionarioService, CargoService, SetorService, GrupoOcupacionalService) {

		document.title = 'Pesquisa Funcionário  - Conecthare';
		$rootScope.activeMenu = 'funcionario';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.funcionario = {};
		$scope.funcionario.ativo = true;

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function() {
			$scope.funcionario = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function() {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();
				if ($rootScope.idEmpresaUser) {
					$scope.funcionario.idEmpresa = $rootScope.idEmpresaUser;
				}

				$scope.paginator.entity = $scope.funcionario;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = ['pessoa', 'cargo c', 'setor', '[c].grupoOcupacional'];

				FuncionarioService.pesquisar($scope.paginator, function(paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: $scope.realizarPesquisar
		});


		$scope.excluir = function(entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function() {
						return 'Você deseja excluir este Funcionário ? <br> <strong>' + $scope.entityToExclude.pessoa.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function(confirmed) {
				if (confirmed) {

					FuncionarioService.excluir({
						id: $scope.entityToExclude.id
					}, function(result) {
						MsgService.addSuccess('Funcionário excluído com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};

		$scope.verPesquisasFuncionario = function(entity) {
			//$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/pesquisas-resultado.html',
				controller: 'PesquisaPesquisaModalCtrl',
				size: 'lg',
				windowClass: 'modalPesquisasResultado',
				resolve: {
					avaliadoResultado: function() {
						return entity;
					},
					idGrupoOcupacional: function() {
						return undefined;
					},
					idCargo: function() {
						return undefined;
					},
					idSetor: function() {
						return entity.id;
					},
					idEmpresa: function() {
						return null;
					}
				}
			});

			modalInstance.result.then(function(result) {
				console.log(result.idPesquisa);
				console.log(result.idAvaliado);
			});

		};

		$scope.cargos = CargoService.lista({
			idEmpresa: $rootScope.idEmpresaUser
		});
		$scope.setores = SetorService.lista({
			idEmpresa: $rootScope.idEmpresaUser
		});
		$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
			idEmpresa: $rootScope.idEmpresaUser
		});

	});