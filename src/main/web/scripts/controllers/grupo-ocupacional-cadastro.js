'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:GrupoOcupacionalCadastroCtrl
 * @description
 * # GrupoOcupacionalCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('GrupoOcupacionalCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, GrupoOcupacionalService) {

		document.title = 'Cadastro Grupo Ocupacional - Conecthare';
		$rootScope.activeMenu =  'funcionario';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.grupoOcupacional = {};
			$scope.grupoOcupacional.ativo = true;

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.grupoOcupacional.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}

			//$scope.formPrincipal.$setDirty();
		};

		$scope.salvar = function() {

			GrupoOcupacionalService.salvar($scope.grupoOcupacional, function(result) {

				MsgService.addSuccess('Grupo Ocupacional salvo com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');
				//$scope.scrollTo('.viewMainWrap .msgs');

			});


		};

		$scope.atualizar = function() {

			GrupoOcupacionalService.atualizar($scope.grupoOcupacional, function(result) {

				MsgService.addSuccess('Grupo Ocupacional atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/grupo-ocupacional/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/funcionario/home');
		};


		//EDITANDO
		if ($routeParams.id) {

			$scope.grupoOcupacional = GrupoOcupacionalService.get({
				id: $routeParams.id
			});

		}

	});