'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:GrupoQuestaoCadastroCtrl
 * @description
 * # GrupoQuestaoCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('GrupoQuestaoCadastroCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope, ngTableParams,
		MsgService, GrupoQuestaoService, QuestaoService, GridUtilsService, $route) {

		document.title = 'Cadastro Grupo Questão - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.screenInit = function () {

			$scope.showMsgsError = false;
			$scope.grupoQuestao = {};
			$scope.grupoQuestao.idEmpresa = undefined;
			$scope.grupoQuestao.ativo = true;

			$scope.questoes = [];

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function () {
			var isInvalid = $scope.formPrincipal.$invalid;

			if (!isInvalid) {
				isInvalid = $scope.validarCampoQuestoes(isInvalid);
			}

			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.validarCampoQuestoes = function (isInvalid) {
			if (!$scope.grupoQuestao.questoesOrdem || $scope.grupoQuestao.questoesOrdem.length === 0) {
				isInvalid = true;
			} else {
				for (var i = 0; i < $scope.grupoQuestao.questoesOrdem.length; i++) {
					var q = $scope.grupoQuestao.questoesOrdem[i];
					if (!q.questao.id) {
						isInvalid = true;
						break;
					}
				}
			}
			$scope.questoesInvalid = isInvalid;
			return isInvalid;
		};

		$scope.ordenarQuestoes = function () {
			var questoesSelecionadas = [];
			for (var i = 0; i < $scope.grupoQuestao.questoesOrdem.length; i++) {
				var questaoOrdem = $scope.grupoQuestao.questoesOrdem[i];
				questaoOrdem.ordem = i;
				questoesSelecionadas.push(questaoOrdem);
			};
			$scope.grupoQuestao.questoesOrdem = questoesSelecionadas;
		};

		$scope.salvarAtualizar = function () {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				$scope.ordenarQuestoes();

				if ($scope.grupoQuestao.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}

		};

		$scope.salvar = function () {

			GrupoQuestaoService.salvar($scope.grupoQuestao, function (result) {

				MsgService.addSuccess('Grupo Questão salvo com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');
				//$route.reload();

			});


		};

		$scope.atualizar = function () {

			GrupoQuestaoService.atualizar($scope.grupoQuestao, function (result) {

				MsgService.addSuccess('Grupo Questão atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/grupo-questao/pesquisa');

			});


		};

		$scope.cancelarSalvar = function () {
			$location.path('/pesquisa/home');
		};

		$scope.selecionarQuestoes = function () {
			$('.loadingApp').show();
			var p = $scope.pesquisarQuestoes($scope.grupoQuestao.idEmpresa, $scope.onEdit);
			p.$promise.then(function () {
				$('.loadingApp').hide();
				$scope.openModalSelecionarQuestoes();
			});
		}

		$scope.openModalSelecionarQuestoes = function () {
			var questoesDisponiveis = [...$scope.questoes];
			var questoesSelecionadas = [...$scope.grupoQuestao.questoesOrdem];
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/questao/selecionar-questoes.html',
				controller: 'SelecionarQuestaoDialogCtrl',
				size: 'lg',
				resolve: {
					questoesDisponiveis: function () {
						return questoesDisponiveis;
					},
					questoesSelecionadas: function () {
						return questoesSelecionadas;
					},
					isQuestaoOrdem: function () {
						return true;
					}
				}
			});

			modalInstance.result.then(function (result) {

				$scope.questoes = [...result.questoesDisponiveis];
				$scope.grupoQuestao.questoesOrdem = [...result.questoesSelecionadas];

				if (!$scope.questoes) {
					$scope.questoes = [];
				}
				if (!$scope.grupoQuestao.questoesOrdem) {
					$scope.grupoQuestao.questoesOrdem = [];
				}

				if ($scope.grupoQuestao.questoesOrdem.length > 0) {
					$scope.questoesInvalid = false;
				} else {
					$scope.questoesInvalid = true;
				}

				//$scope.paramsDataGridQuestoesDisponiveis.reload();
				$scope.paramsDataGridQuestoesSelecionadas.reload();

			}, function () {
				//cancelar
			});
		};

		$scope.removerQuestaoSelecionada = function (questao) {
			for (var i = 0; i < $scope.grupoQuestao.questoesOrdem.length; i++) {
				var q = $scope.grupoQuestao.questoesOrdem[i];
				if (q.questao.id === questao.id) {
					$scope.grupoQuestao.questoesOrdem.splice(i, 1);
					break;
				}
			}
			$scope.questoes.push(questao);
			//$scope.paramsDataGridQuestoesDisponiveis.reload();
			$scope.paramsDataGridQuestoesSelecionadas.reload();
		};

		$scope.adicionarQuestao = function (questao) {
			if (!$scope.grupoQuestao.questoesOrdem) {
				$scope.grupoQuestao.questoesOrdem = [];
			}
			$scope.grupoQuestao.questoesOrdem[$scope.grupoQuestao.questoesOrdem.length] = {
				questao: questao,
				ordem: $scope.grupoQuestao.questoesOrdem.length
			};
			for (var i = 0; i < $scope.questoes.length; i++) {
				var q = $scope.questoes[i];
				if (q.id === questao.id) {
					$scope.questoes.splice(i, 1);
					break;
				}
			}
			//$scope.paramsDataGridQuestoesDisponiveis.reload();
			$scope.paramsDataGridQuestoesSelecionadas.reload();
		};


		$scope.paramsDataGridQuestoesSelecionadas = new ngTableParams({
			count: 10, // count per page
		}, {
				total: 0, // length of data
				counts: [5, 10, 50],
				getData: function ($defer, params) {
					if (!params.$params.filter) {
						params.$params.filter = {};
					}

					if ($scope.grupoQuestaoSelecionadas && $scope.grupoQuestaoSelecionadas.nome) {
						params.$params.filter.grupoQuestao = $scope.grupoQuestaoSelecionadas;
					} else {
						params.$params.filter.grupoQuestao = undefined;
					}
					GridUtilsService.refreshData($defer, params, $scope.grupoQuestao.questoesOrdem);
				}
			});

		$scope.pesquisarQuestoes = function (idEmpresa, onEdit) {

			return QuestaoService.lista({
				idEmpresa: idEmpresa,
				fieldsToFetch: ['listaValores']
			}, function (questoes) {
				$scope.questoes = questoes;
				//$scope.paramsDataGridQuestoesDisponiveis.reload();
				if ($scope.questoes && $scope.questoes.length > 0) {
					if ($scope.grupoQuestao.questoesOrdem && $scope.grupoQuestao.questoesOrdem.length > 0) {
						var questoesDisponiveisAtualizada = [];
						for (var i = 0; i < $scope.questoes.length; i++) { //questoes disponiveis
							var questaoDisponivel = $scope.questoes[i];
							var jaSelecionada = false;
							for (var j = 0; j < $scope.grupoQuestao.questoesOrdem.length; j++) { //questoes selecionadas
								var questaoSelecionada = $scope.grupoQuestao.questoesOrdem[j];
								if (questaoSelecionada.questao.id === questaoDisponivel.id) {
									jaSelecionada = true;
									break;
								}
							}
							if (!jaSelecionada) {
								// questoesDisponiveisAtualizada[questoesDisponiveisAtualizada.length] = {
								// 	questao: questaoDisponivel,
								// 	ordem: questoesDisponiveisAtualizada.length
								// };
								questoesDisponiveisAtualizada.push(questaoDisponivel);
							}
						}
						$scope.questoes = questoesDisponiveisAtualizada;
					}
				}
			});
		};

		$scope.$watch('grupoQuestao.idEmpresa', function (newValue) {
			if (!!newValue) {
				//$scope.pesquisarQuestoes(newValue, $scope.onEdit);
				if ((!!$scope.idEmpresaEditing && $scope.idEmpresaEditing !== newValue) || !$scope.idEmpresaEditing) {
					$scope.grupoQuestao.questoesOrdem = [];
				}
			}
		});

		//EDITANDO
		// if ($routeParams.id) {
		// 	$scope.grupoQuestao = GrupoQuestaoService.get({
		// 		id: $routeParams.id,
		// 		fieldsToFetch: []
		// 	});
		// }

		//EDITANDO
		if ($routeParams.id) {
			GrupoQuestaoService.get({
				id: $routeParams.id,
				fieldsToFetch: ['questoesOrdem qo', '[qo].questao q', '[q].listaValores']
			}, function (grupoQuestao) {
				$scope.onEdit = true;
				$scope.idEmpresaEditing = grupoQuestao.idEmpresa;
				$scope.grupoQuestao = grupoQuestao;

				if (!$scope.grupoQuestao.questoesOrdem) {
					$scope.grupoQuestao.questoesOrdem = [];
				}

				$scope.paramsDataGridQuestoesSelecionadas.reload();

				//$scope.pesquisarQuestoes(true);
			});

		} else {
			//$scope.pesquisarQuestoes();

		}



	});