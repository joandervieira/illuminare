'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ImportFuncionarioCtrl
 * @description
 * # ImportFuncionarioCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('ImportFuncionarioCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope, Upload, ngTableParams,
		MsgService, FuncionarioService, GridUtilsService, EmpresaService) {

		document.title = 'Importação - Conecthare';
		$rootScope.activeMenu = 'funcionario';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.arquivoEnviado = false;
			$scope.funcionariosPreview = [];

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.previewDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $scope.funcionariosPreview);
			}
		});

		$scope.previewDataGridDesabilitados = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $scope.funcionariosDesabilitados);
			}
		});

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.funcionario.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}

			}

		};

		$scope.salvar = function() {

			FuncionarioService.salvar($scope.funcionario, function(result) {

				MsgService.addSuccess('Import salvo com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});


		};



		$scope.prepareUpload = function() {
			MsgService.cleanAll(true);
			$scope.status = undefined;
			$scope.progress = 0;
			$scope.arquivoEnviado = false;
		};

		$scope.upload = function() {

			if ($scope.files && $scope.files.length) {
				$('.loadingApp').show();
				$scope.showPreConfirmarImportacao = false;
				for (var i = 0; i < $scope.files.length; i++) {
					var file = $scope.files[i];
					Upload.upload({
						url: APP_CONFIG.prefixURLRest + '/funcionario/importFromXls',
						// fields: {
						// 	'username': $scope.username
						// },
						file: file
					}).progress(function(evt) {
						//console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
						$timeout(function() {
							$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
							$scope.status = "Enviando... " + $scope.progress + "%";
						});

					}).success(function(data, status, headers, config) {
						MsgService.cleanAll(true);
						//console.log('file ' + config.file.name + 'uploaded. Response: ');
						//console.log(data);
						$('.loadingApp').hide();
						$scope.funcionariosPreview = data.funcionariosImportados;
						$scope.idEmpresaImport = data.idEmpresa;
						$scope.idEmpresaImportCopy = data.idEmpresa;
						$scope.previewDataGrid.reload();
						$scope.arquivoEnviado = true;

						EmpresaService.get({
							id: $scope.idEmpresaImport
						}, function(empresa) {
							if (empresa) {
								$scope.nomeEmpresaImportada = empresa.nome;
							}
						});
					});
				}
			}
		};

		$scope.confirmarImportacao = function() {
			$('.loadingApp').show();
			MsgService.cleanAll(true);
			var previewFuncionariosImportacao = {
				idEmpresa: $scope.idEmpresaImportCopy,
				funcionariosImportados: $scope.funcionariosPreview
			};
			FuncionarioService.confirmarImportacao(previewFuncionariosImportacao, function() {
				MsgService.addSuccess('Importação realizada com sucesso', true);
				$location.path('/funcionario/pesquisa');
				//$scope.prepareUpload();
				//$('.loadingApp').hide();
			});
		};

		$scope.preConfirmarImportacao = function() {
			MsgService.cleanAll(true);
			$('.loadingApp').show();
			var previewFuncionariosImportacao = {
				idEmpresa: $scope.idEmpresaImportCopy,
				funcionariosImportados: $scope.funcionariosPreview
			};
			FuncionarioService.preConfirmarImportacao(previewFuncionariosImportacao, function(previewFuncionariosImportacaoResult) {
				if (previewFuncionariosImportacaoResult && previewFuncionariosImportacaoResult.funcionariosDesativados && previewFuncionariosImportacaoResult.funcionariosDesativados.length > 0) {
					$scope.funcionariosDesabilitados = previewFuncionariosImportacaoResult.funcionariosDesativados;
					$scope.showPreConfirmarImportacao = true;
					$scope.previewDataGridDesabilitados.reload();
					$timeout(function() {
						$('.loadingApp').hide();
					}, 200);
				} else {
					$scope.confirmarImportacao();
				}
			});
		};

		$scope.atualizar = function() {

			FuncionarioService.atualizar($scope.funcionario, function(result) {

				MsgService.addSuccess('Import atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/funcionario/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/funcionario/home');
		};



		//EDITANDO
		if ($routeParams.id) {


		}

	});