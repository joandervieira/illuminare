'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ListaValoresCadastroCtrl
 * @description
 * # ListaValoresCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('ListaValoresCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ListaValoresService, $filter) {

		document.title = 'Cadastro Escala de Valores - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.valoresListaInvalid = false;
			$scope.valoresListaInvalidCamposObrigatorios = false;
			$scope.listaValores = {};
			$scope.listaValores.ativo = true;

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			$scope.valoresListaInvalid = false;

			var isInvalid = $scope.formPrincipal.$invalid;
			isInvalid = $scope.validarCampoValoresLista(isInvalid);
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.validarCampoValoresLista = function(isInvalid) {
			if (!$scope.listaValores.valoresLista || $scope.listaValores.valoresLista.length === 0) {
				isInvalid = true;
				$scope.valoresListaInvalid = true;
			}
			var listaAtualizada = [];
			for (var i = 0; i < $scope.listaValores.valoresLista.length; i++) {
				var valorLista = $scope.listaValores.valoresLista[i];

				if (valorLista.nome || valorLista.valor) {
					listaAtualizada.push(valorLista);
				}

			};
			$scope.listaValores.valoresLista = listaAtualizada;
			if (listaAtualizada.length === 0) {
				isInvalid = true;
				$scope.valoresListaInvalid = true;
				$scope.listaValores.valoresLista = [{}];
			}

			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.listaValores.valoresLista && $scope.listaValores.valoresLista.length > 0) {
					for (var i = 0; i < $scope.listaValores.valoresLista.length; i++) {
						var valorLista = $scope.listaValores.valoresLista[i];
						valorLista.ordem = i;
						//valorLista.valor = (i + 1);
					};
				}

				if ($scope.listaValores.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}

			}

		};

		$scope.salvar = function() {

			ListaValoresService.salvar($scope.listaValores, function(result) {

				MsgService.addSuccess('Escala de Valores salva com sucesso');
				$scope.screenInit();
				$scope.listaValores.valoresLista = [{}];
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});


		};

		$scope.atualizar = function() {

			ListaValoresService.atualizar($scope.listaValores, function(result) {

				MsgService.addSuccess('Escala de Valores atualizada com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/listavalores/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/pesquisa/home');
		};


		$scope.focusField = function(index) {
			$scope.focus('.valorListaNome' + (index + 1));
		}


		//EDITANDO
		if ($routeParams.id) {
			ListaValoresService.get({
				id: $routeParams.id,
				fieldsToFetch: []
			}, function(listaValores) {
				$scope.listaValores = listaValores;
				if (!$scope.listaValores.valoresLista || $scope.listaValores.valoresLista.length === 0) {
					$scope.listaValores.valoresLista = [{}];
				} else {
					$scope.listaValores.valoresLista = $filter('orderBy')($scope.listaValores.valoresLista, '+ordem');
				}
				// for (var i = 0; i < $scope.listaValores.valoresLista.length; i++) {
				// 	var valor = $scope.listaValores.valoresLista[i].valor;
				// 	valor = parseFloat(valor);
				// 	$scope.listaValores.valoresLista[i].valor = valor;
				// };
			});

		} else {
			$scope.listaValores.valoresLista = [{}];
		}

	});