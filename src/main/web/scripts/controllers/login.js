'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('LoginCtrl', function($scope, $rootScope, $cookieStore, $location, UserService, $sessionStorage, MsgService, $window, EmpresaService) {

		document.title = 'Conecthare';

		MsgService.cleanAll(true);

		if ($rootScope.user) {
			$window.history.back();
		}

		$scope.rememberMe = false;
		$scope.inserindoNovaSenha = false;
		$scope.userAtualizacao = {};
		//$scope.senhaAntiga;
		$scope.userRecuperarSenha = {};

		$('.loadingApp').hide();

		// REMOVER
		localStorage.removeItem('username');
		localStorage.removeItem('password');

		if (window.localStorage['username']) {
			$scope.username = window.localStorage['username'];
			$scope.password = window.localStorage['password'];
			$scope.rememberMe = true;
		}

		$scope.removeAlert = function() {
			$scope.msgsErro = [];
			$scope.msgsSucesso = [];
		};

		$scope.login = function() {
			MsgService.cleanAll(true);
			$('.loadingApp').show();

			UserService.authenticate($.param({
				username: $scope.username,
				password: $scope.password

			}), function(authenticationResult) {
				//LOGIN SUCCESS

				$scope.msgsErro = [];
				var authToken = authenticationResult.token;
				$rootScope.authToken = authToken;
				//$sessionStorage.authToken = authToken;
				$cookieStore.put('authToken', authToken);

				UserService.findById({
					id: authenticationResult.id,
					fieldsToFetch: ['funcionario f', '[f].pessoa p', '[f].cargo c', '[c].grupoOcupacional g']
				}, function(user) {

					if (!user.senhaAtualizada) {

						$scope.userAtualizacao = angular.copy(user);
						$scope.userAtualizacao.password = undefined;
						//$scope.senhaAntiga = user.password;
						$scope.inserindoNovaSenha = true;
						$('.loadingApp').hide();

					} else {

						$rootScope.user = {
							id: user.id,
							roles: user.roles,
							name: user.name,
							nomeExibir: user.nomeExibir,
							senhaAtualizada: user.senhaAtualizada,
							email: user.email,
							funcionario: user.funcionario
						};
						//$sessionStorage.user = $rootScope.user;
						$cookieStore.put('user', $rootScope.user);

						if (user.funcionario && user.funcionario.idEmpresa) {
							$rootScope.idEmpresaUser = user.funcionario.idEmpresa;
							$cookieStore.put('idEmpresaUser', user.funcionario.idEmpresa);
						}

						//$rootScope.isFuncionario = $rootScope.user.funcionario && $rootScope.user.funcionario.id;
						$rootScope.hasRoleAdmin = $rootScope.hasRole('role_admin');
						$rootScope.isFuncionario = $rootScope.hasRole('role_funcionario');
						$rootScope.isGestor = $rootScope.hasRole('role_gestor');
						$rootScope.isGestorSetor = $rootScope.hasRole('role_gestor_setor');

						if ($scope.rememberMe) {
							//$cookieStore.put('authToken', authToken);
							//$cookieStore.put('username', $scope.username);
							window.localStorage['username'] = $scope.username;
							window.localStorage['password'] = $scope.password;
						} else {
							localStorage.removeItem('username');
							localStorage.removeItem('password');
						}

						if (!$rootScope.empresas || $rootScope.empresas.length === 0) {
							$rootScope.empresas = EmpresaService.lista();
						}

						$('.loadingApp').hide();
						var pp = $rootScope.previousPage;
						if (pp && pp.indexOf('login') === -1) {
							$location.path($rootScope.previousPage);
						} else {
							$location.path("/");
						}

						//$window.location.href = APP_CONFIG.urlPrefix + '/';
						//$('.loadingApp').hide();
					}

				});
			});
		};


		/*$scope.$watch('rememberMe', function(newValue) {
			if (newValue) {
				window.localStorage['username'] = $scope.username;
				window.localStorage['password'] = $scope.password;
			} else {
				localStorage.removeItem('username');
				localStorage.removeItem('password');
			}
		});*/

		$scope.enviarEmailRecuperacao = function() {
			$('.loadingApp').show();
			MsgService.cleanAll(true);
			UserService.enviarEmailRecuperacao($scope.userRecuperarSenha, function() {

				MsgService.addSuccess('Senha recuperada com sucesso. A senha foi enviada para o email cadastrado');
				$('.loadingApp').hide();

			}, function(httpResponse) {

				$scope.msgsErro = [httpResponse.data];
				$('.loadingApp').hide();

			});
		};


		$scope.formularioNovaSenhaIsValid = function() {
			var valid = $scope.formMudarSenha.$valid;
			if (!$scope.userAtualizacao.password || !$scope.userAtualizacao.email) {
				valid = false;
			}
			return valid;
		};

		$scope.salvarUsuario = function() {
			if ($scope.formularioNovaSenhaIsValid) {

				delete $scope.userAtualizacao.authorities;
				delete $scope.userAtualizacao.accountNonExpired;
				delete $scope.userAtualizacao.accountNonLocked;
				delete $scope.userAtualizacao.enabled;
				delete $scope.userAtualizacao.username;
				delete $scope.userAtualizacao.credentialsNonExpired;

				UserService.atualizarSenha($scope.userAtualizacao, function() {
					$scope.username = undefined;
					$scope.password = undefined;
					$scope.rememberMe = false;
					$scope.userAtualizacao = {};
					//$scope.senhaAntiga = undefined;

					$scope.inserindoNovaSenha = false;

					MsgService.addSuccess('Senha atualizada com sucesso. Por favor efetuar o login novamente com a nova senha.');

				});
			}
		};

	});