'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('MainCtrl', function($scope, $interval, $rootScope, $location, $modal, $timeout,
		MsgService, AvaliacaoService, GridUtilsService, ngTableParams, PesquisaService) {


		document.title = 'Conecthare';
		$rootScope.activeMenu = 'main';

		MsgService.cleanAll(false);
		$('.loadingApp').hide();

		//$rootScope.pesquisasEmAberto = [];
		//$rootScope.funcionariosParaAvaliar = [];
		$scope.showFuncionariosParaAvaliar = true;

		$scope.initialize = function() {

			if ($scope.user.funcionario && $scope.user.funcionario.id) {

				$scope.findAvaliacoesEmAberto();
				$scope.findPesquisasFechadas();
			}

		};

		$scope.findAvaliacoesEmAberto = function() {
			//if (!$rootScope.avaliacoesEmAberto) {
			$('.loadingApp').show();
			AvaliacaoService.avaliacoesEmAberto({
				idAvaliador: $scope.user.funcionario.id
			}, function(result) {
				$rootScope.avaliacoesEmAberto = result;
				$scope.filterPesquisasEmAberto();
				if ($rootScope.pesquisaResponder) {
					$scope.filterFuncionariosParaAvaliar();
				};
				$('.loadingApp').hide();
			});
			//}
		};

		$scope.findPesquisasFechadas = function() {

			if (!$rootScope.pesquisasFechadas) {
				PesquisaService.fechadas({
					idAvaliado: $scope.user.funcionario.id
				}, function(pesquisasFechadas) {
					$rootScope.pesquisasFechadas = pesquisasFechadas;
					$scope.dataGridPesquisasFechadas.reload();
				});
			}

		};


		$scope.filterPesquisasEmAberto = function() {
			$rootScope.pesquisasEmAberto = [];
			if ($rootScope.avaliacoesEmAberto && $rootScope.avaliacoesEmAberto.length > 0) {
				for (var i = 0; i < $rootScope.avaliacoesEmAberto.length; i++) {
					var avaliacao = $rootScope.avaliacoesEmAberto[i];
					var wasAdd = false;
					for (var j = 0; j < $rootScope.pesquisasEmAberto.length; j++) {
						var pesquisa = $rootScope.pesquisasEmAberto[j];
						if (pesquisa && pesquisa.id === avaliacao.pesquisa.id) {
							wasAdd = true;
							break;
						}
					};
					if (!wasAdd) {
						$rootScope.pesquisasEmAberto.push(avaliacao.pesquisa);
					}
				};
				$scope.dataGridPesquisasEmAberto.reload();
			}
		};

		$scope.filterFuncionariosParaAvaliar = function() {
			$rootScope.funcionariosParaAvaliar = [];
			if ($rootScope.pesquisaResponder) {
				if ($rootScope.avaliacoesEmAberto && $rootScope.avaliacoesEmAberto.length > 0) {
					for (var i = 0; i < $rootScope.avaliacoesEmAberto.length; i++) {
						var avaliacao = $rootScope.avaliacoesEmAberto[i];
						if (avaliacao.pesquisa.id === $rootScope.pesquisaResponder.id) {
							var wasAdd = false;
							for (var j = 0; j < $rootScope.funcionariosParaAvaliar.length; j++) {
								var avaliacaoAvaliado = $rootScope.funcionariosParaAvaliar[j];
								if (avaliacaoAvaliado.funcionarioAvaliado.id === avaliacao.funcionarioAvaliado.id) {
									wasAdd = true;
									break;
								}
							};
							if (!wasAdd) {
								$rootScope.funcionariosParaAvaliar.push(avaliacao);
							}
						}
					};
				}
			}
			if ($rootScope.funcionariosParaAvaliar.length === 0) {
				$rootScope.pesquisaResponder = undefined;
			}
			$scope.dataGridFuncionariosParaAvaliar.reload();
			//$('.loadingApp').hide();
		};


		$scope.responderPesquisa = function(pesquisa) {
			//$('.loadingApp').show();
			$rootScope.pesquisaResponder = pesquisa;
			$scope.filterFuncionariosParaAvaliar();
		};

		$scope.cancelarResponderPesquisa = function() {
			$rootScope.pesquisaResponder = undefined;
			$rootScope.funcionariosParaAvaliar = [];
			$scope.dataGridFuncionariosParaAvaliar.reload();
		};

		$scope.avaliarFuncionario = function(funcionario) {
			console.log(funcionario);
		};


		$scope.dataGridPesquisasEmAberto = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $rootScope.pesquisasEmAberto);
			}
		});

		$scope.dataGridFuncionariosParaAvaliar = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $rootScope.funcionariosParaAvaliar);
			}
		});

		$scope.dataGridPesquisasFechadas = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			sorting: {
				dtaEntrega: 'desc'
			},
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $rootScope.pesquisasFechadas);
			}
		});

		$scope.dataGridPesquisasFechadas.sorting({
			'dtaEntrega': 'desc'
		})


		$scope.initialize();

	});