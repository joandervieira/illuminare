'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PesquisaCadastroCtrl
 * @description
 * # PesquisaCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('PesquisaCadastroCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope, ngTableParams,
		MsgService, QuestionarioService, PesquisaService, GridUtilsService, GrupoOcupacionalService) {

		document.title = 'Cadastro Pesquisa - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.questionarios = [];

		$scope.screenInit = function () {

			$scope.showMsgsError = false;
			$scope.questionariosInvalid = false;
			$scope.pesquisa = {};
			$scope.pesquisa.calculoPorMeta = true;
			$scope.pesquisa.ativo = true;
			$scope.pesquisa.questionarios = [];

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function () {
			var isInvalid = $scope.formPrincipal.$invalid;

			if (!isInvalid) {
				isInvalid = $scope.validarCampoQuestionarios(isInvalid);
			}

			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};


		$scope.validarCampoQuestionarios = function (isInvalid) {
			if (!$scope.pesquisa.questionarios || $scope.pesquisa.questionarios.length === 0) {
				isInvalid = true;
			} else {
				for (var i = 0; i < $scope.pesquisa.questionarios.length; i++) {
					var q = $scope.pesquisa.questionarios[i];
					if (!q.id) {
						isInvalid = true;
						break;
					}
				}
			}

			$scope.questionariosInvalid = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function () {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.pesquisa.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}

		};

		$scope.salvar = function () {

			PesquisaService.salvar($scope.pesquisa, function (result) {

				MsgService.addSuccess('Pesquisa salva com sucesso');
				$scope.screenInit();
				$scope.questionarios = angular.copy($scope.questionariosCache);
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});


		};

		$scope.atualizar = function () {

			PesquisaService.atualizar($scope.pesquisa, function (result) {

				MsgService.addSuccess('Pesquisa atualizada com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/pesquisa/pesquisa');

			});


		};

		$scope.cancelarSalvar = function () {
			$location.path('/pesquisa/home');
		};


		$scope.paramsDataGridQuestionariosSelecionados = new ngTableParams({
			count: 10, // count per page
		}, {
				total: 0, // length of data
				counts: [5, 10, 50],
				getData: function ($defer, params) {
					GridUtilsService.refreshData($defer, params, $scope.pesquisa.questionarios);
				}
			});

		$scope.selecionarQuestionarios = function () {
			var questionariosDisponiveis = angular.copy($scope.questionarios);
			var questionariosSelecionados = angular.copy($scope.pesquisa.questionarios);
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/cadastro-selecionar-questionarios.html',
				controller: 'PesquisaSelecionarQuestionarioDialogCtrl',
				size: 'lg',
				resolve: {
					questionariosDisponiveis: function () {
						return questionariosDisponiveis;
					},
					questionariosSelecionados: function () {
						return questionariosSelecionados;
					},
					gruposOcupacionais: function () {
						return $scope.gruposOcupacionais;
					}
				}
			});

			modalInstance.result.then(function (result) {

				$scope.questionarios = result.questionariosDisponiveis;
				$scope.pesquisa.questionarios = result.questionariosSelecionados;

				if (!$scope.questionarios) {
					$scope.questionarios = [];
				}
				if (!$scope.pesquisa.questionarios) {
					$scope.pesquisa.questionarios = [];
				}

				if ($scope.pesquisa.questionarios.length > 0) {
					$scope.questionariosInvalid = false;
				} else {
					$scope.questionariosInvalid = true;
				}

				$scope.paramsDataGridQuestionariosSelecionados.reload();

			}, function () {
				//cancelar
			});
		};

		$scope.removerQuestionarioSelecionado = function (questao) {
			for (var i = 0; i < $scope.pesquisa.questionarios.length; i++) {
				var q = $scope.pesquisa.questionarios[i];
				if (q.id === questao.id) {
					$scope.pesquisa.questionarios.splice(i, 1);
					break;
				}
			}
			$scope.questionarios.push(questao);
			//$scope.paramsDataGridQuestionariosDisponiveis.reload();
			$scope.paramsDataGridQuestionariosSelecionados.reload();
		};



		$scope.pesquisarQuestionarios = function (idEmpresa, onEditPesquisa) {
			$('.loadingApp').show();
			QuestionarioService.pesquisar({
				entity: {
					idEmpresa: idEmpresa
				},
				fieldsToFetch: ['gruposOcupacionais g', 'cargos c', 'setores s', 'gruposQuestoes gq', '[gq].questoesOrdem qo'],
				orderBy: '+nome'
			}, function (paginator) {
				var questionarios = paginator.result;
				$scope.questionarios = questionarios;
				$scope.questionariosCache = angular.copy(questionarios);
				if (onEditPesquisa && $scope.questionarios && $scope.questionarios.length > 0) {
					if ($scope.pesquisa.questionarios && $scope.pesquisa.questionarios.length > 0) {
						var questionariosDisponiveisAtualizado = [];
						var jaSelecionada = false;
						for (var i = 0; i < $scope.questionarios.length; i++) { //questionarios disponiveis
							var questionarioDisponivel = $scope.questionarios[i];
							jaSelecionada = false;
							for (var j = 0; j < $scope.pesquisa.questionarios.length; j++) { //questionarios selecionadas
								var questionarioSelecionado = $scope.pesquisa.questionarios[j];
								if (questionarioSelecionado.id === questionarioDisponivel.id) {
									jaSelecionada = true;
									break;
								}
							}
							if (!jaSelecionada) {
								questionariosDisponiveisAtualizado.push(questionarioDisponivel);
							}
						}
						$scope.questionarios = questionariosDisponiveisAtualizado;
					}
				}
				$('.loadingApp').hide();
			});
		};

		$scope.metasfetchData = true;


		$scope.informarMetas = function () {

			if ($scope.metasfetchData) {
				$('.loadingApp').show();
				$scope.pesquisa = PesquisaService.buscarMetas($scope.pesquisa, function () {
					$('.loadingApp').hide();
					$scope.abrirModalInformarMetas();
				});
			} else {
				$scope.abrirModalInformarMetas();
			}

		};

		$scope.abrirModalInformarMetas = function () {

			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/informar-metas.html',
				controller: 'PesquisaInformarMetasDialogCtrl',
				size: 'lg',
				resolve: {
					pesquisa: function () {
						return $scope.pesquisa;
					},
					idPesquisa: function () {
						return $scope.pesquisa.id;
					},
					fetchData: function () {
						return $scope.metasfetchData;
					}
				}
			});

			modalInstance.result.then(function (result) {

				//ok
				$scope.pesquisa.questionarios = result;
				$scope.metasfetchData = false;
				$scope.pesquisa.salvarMetas = true;

			}, function () {
				//cancelar
			});

		};


		$scope.$watch('pesquisa.idEmpresa', function (newValue) {
			if (newValue) {
				$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
					idEmpresa: newValue
				});
				$scope.pesquisarQuestionarios(newValue, $scope.onEditPesquisa);
			}
		});

		//EDITANDO
		if ($routeParams.id) {
			PesquisaService.get({
				id: $routeParams.id,
				fieldsToFetch: ['questionarios q', '[q].gruposOcupacionais', '[q].cargos', '[q].setores', '[q].gruposQuestoes gq', '[gq].questoesOrdem'],
				findMetas: true
			}, function (pesquisa) {
				$scope.onEditPesquisa = true;
				$scope.pesquisa = pesquisa;
				$scope.paramsDataGridQuestionariosSelecionados.reload();
				//$scope.pesquisarQuestionarios(true);
			});

		} else {
			//$scope.pesquisarQuestionarios();
		}

	})
	.controller('PesquisaInformarMetasDialogCtrl', function ($scope, $modal, $modalInstance, $timeout, $q,
		GridUtilsService, ngTableParams, QuestaoService, PesquisaService, MsgService, pesquisa, idPesquisa, fetchData) {

		$scope.questionarios = pesquisa.questionarios;
		$scope.idPesquisa = idPesquisa;
		$scope.pesquisa = pesquisa;

		$scope.autoCalcularMetasParaTodos = function () {
			if ($scope.questionarios) {
				$scope.questionarios.forEach(function (questionario) {
					if (questionario.gruposQuestoes) {
						questionario.gruposQuestoes.forEach(function (grupoQuestao) {
							grupoQuestao.metaAutoCalculada = true;
							$scope.calcularMeta(true, grupoQuestao, questionario);
						});
					}
				});
			}
		};


		$scope.calcularMeta = function (checked, grupoQuestao, questionario) {

			if (checked) {
				var qtdeQuestoesForteMtoForte = 0;
				var valorMaximoEscala = 0;
				var qtdeQuestoes = 0;

				for (var indexQuestaoOrdem = 0; indexQuestaoOrdem < grupoQuestao.questoesOrdem.length; indexQuestaoOrdem++) {
					var questaoOrdem = grupoQuestao.questoesOrdem[indexQuestaoOrdem];
					qtdeQuestoes++;
					if (!questaoOrdem.questao.classificacao || questaoOrdem.questao.classificacao <= 2) {
						qtdeQuestoesForteMtoForte++;

						if (questaoOrdem.questao.listaValores && questaoOrdem.questao.listaValores.valoresLista) {
							if (questaoOrdem.questao.listaValores.valoresLista.length > 0) {
								for (var i = 0; i < questaoOrdem.questao.listaValores.valoresLista.length; i++) {
									var valorLista = questaoOrdem.questao.listaValores.valoresLista[i];
									if (valorMaximoEscala < valorLista.valor) {
										valorMaximoEscala = valorLista.valor;
									}
								}
							}
						}
					}

				}

				var meta = qtdeQuestoesForteMtoForte * valorMaximoEscala / qtdeQuestoes;
				meta = Math.round(meta * 10) / 10;

				$scope.setMeta(meta, grupoQuestao, questionario);


			} else {

				$scope.setMeta(0, grupoQuestao, questionario);

			}
		};

		$scope.setMeta = function (meta, grupoQuestao, questionario) {
			for (var indexQuestaoOrdem = 0; indexQuestaoOrdem < grupoQuestao.questoesOrdem.length; indexQuestaoOrdem++) {
				var questaoOrdem = grupoQuestao.questoesOrdem[indexQuestaoOrdem];

				if (!questaoOrdem.questao.questoesMetasGruposOcupacionais) {
					questaoOrdem.questao.questoesMetasGruposOcupacionais = [];
				}
				if (!questaoOrdem.questao.questoesMetasCargos) {
					questaoOrdem.questao.questoesMetasCargos = [];
				}
				if (!questaoOrdem.questao.questoesMetasSetores) {
					questaoOrdem.questao.questoesMetasSetores = [];
				}

				if (questionario.gruposOcupacionais && questionario.gruposOcupacionais.length > 0) {
					for (var i = 0; i < questionario.gruposOcupacionais.length; i++) {
						//var go = questionario.gruposOcupacionais[i];
						if (!questaoOrdem.questao.questoesMetasGruposOcupacionais[i]) {
							questaoOrdem.questao.questoesMetasGruposOcupacionais[i] = {};
						}
						questaoOrdem.questao.questoesMetasGruposOcupacionais[i].meta = meta;
						questaoOrdem.questao.questoesMetasGruposOcupacionais[i].autoCalculada = grupoQuestao.metaAutoCalculada;
						questaoOrdem.questao.questoesMetasGruposOcupacionais[i].negativo = false;
					}
				}
				if (questionario.cargos && questionario.cargos.length > 0) {
					for (var i = 0; i < questionario.cargos.length; i++) {
						//var c = questionario.cargos[i];
						if (!questaoOrdem.questao.questoesMetasCargos[i]) {
							questaoOrdem.questao.questoesMetasCargos[i] = {};
						}
						questaoOrdem.questao.questoesMetasCargos[i].meta = meta;
						questaoOrdem.questao.questoesMetasCargos[i].autoCalculada = grupoQuestao.metaAutoCalculada;
						questaoOrdem.questao.questoesMetasCargos[i].negativo = false;
					}
				}
				if (questionario.setores && questionario.setores.length > 0) {
					for (var i = 0; i < questionario.setores.length; i++) {
						//var s = questionario.setores[i];
						if (!questaoOrdem.questao.questoesMetasSetores[i]) {
							questaoOrdem.questao.questoesMetasSetores[i] = {};
						}
						questaoOrdem.questao.questoesMetasSetores[i].meta = meta;
						questaoOrdem.questao.questoesMetasSetores[i].autoCalculada = grupoQuestao.metaAutoCalculada;
						questaoOrdem.questao.questoesMetasSetores[i].negativo = false;
					}
				}
			}
		};

		$scope.isAllListaEscala = function (grupoQuestao) {

			if (grupoQuestao) {
				for (var indexQuestaoOrdem = 0; indexQuestaoOrdem < grupoQuestao.questoesOrdem.length; indexQuestaoOrdem++) {
					var questaoOrdem = grupoQuestao.questoesOrdem[indexQuestaoOrdem];

					if (!questaoOrdem.questao.isLista) {
						return false;
					}

				}
				return true;
			}
			return false;
		};

		$scope.showGrupoQuestao = function (grupoQuestao) {

			if (grupoQuestao) {
				var allHide = true;
				for (var indexQuestaoOrdem = 0; indexQuestaoOrdem < grupoQuestao.questoesOrdem.length; indexQuestaoOrdem++) {
					var questaoOrdem = grupoQuestao.questoesOrdem[indexQuestaoOrdem];

					if (questaoOrdem.questao.classificacao <= 2 || !questaoOrdem.questao.classificacao) {
						allHide = false;
					}

				}
				return !allHide;
			}
			return false;
		};

		$scope.showClonarMetas = function () {
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/pesquisas-lista-generico.html',
				controller: 'PesquisaListaGenericoCtrl',
				size: 'lg',
				windowClass: 'modalPesquisasResultado',
				resolve: {
					idEmpresa: function () {
						return $scope.pesquisa.idEmpresa;
					},
					opts: function () {
						return {
							msgConfirmacaoSelecaoPrefix: 'Deseja clonar as metas da pesquisa: ',
							actionSelecaoName: 'Clonar metas',
							actionSelecaoIcon: 'glyphicon glyphicon-duplicate'
						}
					}
				}

			});

			modalInstance.result.then(function (result) {

				if (!!result && !!result.pesquisa) {
					$('.loadingApp').show();
					pesquisa.cloneMetasPesquisaFromId = result.pesquisa.id;
					pesquisa.questionarios = $scope.questionarios;
					PesquisaService.clonarMetas(pesquisa, function (pesquisaCloned) {
						$scope.questionarios = [];
						$scope.questionarios = pesquisaCloned.questionarios;
						$('.loadingApp').hide();
						var mantidos = 'Quantidade de questionários mantidos: ' + pesquisaCloned.qtdeQuestionariosClonadosMantido;
						var clonados = 'Quantidade de questionários clonados: ' + pesquisaCloned.qtdeQuestionariosClonadosMetas;
						MsgService.addSuccess(clonados + '<br>' + mantidos);
					});
				}

			});

		};



		$scope.ok = function () {
			$modalInstance.close($scope.questionarios);
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};

	});