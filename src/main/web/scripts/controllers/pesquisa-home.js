'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PesquisaHomeCtrl
 * @description
 * # PesquisaHomeCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('PesquisaHomeCtrl', function($scope, MsgService, $rootScope) {

		document.title = 'Pesquisa - Conecthare';
		$rootScope.activeMenu =  'pesquisa';


		MsgService.cleanAll(true);


	});