'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PesquisaPesquisaCtrl
 * @description
 * # PesquisaPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('PesquisaListaGenericoCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope, $modalInstance,
		MsgService, ngTableParams, GridUtilsService, PesquisaService, GrupoOcupacionalService, idEmpresa, opts) {

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.multiSelect = false;
		$scope.pesquisa = {};
		$scope.idEmpresa = idEmpresa;
		$scope.opts = opts || {};

		$scope.actionSelecaoName = opts.actionSelecaoName || 'Selecionar';
		$scope.actionSelecaoIcon = opts.actionSelecaoIcon || 'glyphicon glyphicon-ok';

		//$scope.timeout = (!!$scope.avaliadoResultado ? 1000 : 0);
		$scope.timeout = 1000;

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 5
		};


		$scope.limparFiltro = function () {
			$scope.pesquisa = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function () {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function ($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				if ($rootScope.idEmpresaUser) {
					$scope.pesquisa.idEmpresa = $rootScope.idEmpresaUser;
				}
				if ($scope.idEmpresa) {
					$scope.pesquisa.idEmpresa = $scope.idEmpresa;
				}
				if ($scope.idEmpresaPesquisa) {
					$scope.pesquisa.idEmpresa = $scope.idEmpresaPesquisa;
				}

				$scope.paginator.entity = $scope.pesquisa;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				var paramOrder = params.orderBy()[0];
				$scope.paginator.orderBy = paramOrder;
				if (!paramOrder) {
					$scope.paginator.orderBy = '+nome';
				}
				$scope.paginator.fieldsToFetch = [];

				PesquisaService.pesquisar($scope.paginator, function (paginator) {
					$scope.paginator = paginator;
					$scope.paginator.entity = $scope.pesquisa;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$timeout(function () {
			$scope.paramsDataGrid = new ngTableParams({
				count: 5, // count per page
			}, {
					total: 0, // length of data
					counts: [5, 50, 100],
					sorting: {
						id: 'asc'
					},
					getData: $scope.realizarPesquisar
				});
		}, $scope.timeout);

		$scope.selecionarPesquisaConfirmacao = function (pesquisa) {
			var suffix = 'id: ' + pesquisa.id + ' - nome: ' + pesquisa.nome;
			var msg = 'Deseja selecionar esta pesquisa ? ' + suffix;
			if ($scope.opts.msgConfirmacaoSelecaoPrefix) {
				msg = $scope.opts.msgConfirmacaoSelecaoPrefix + suffix;
			}

			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return msg;
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {
					$scope.selecionarPesquisa(pesquisa);
				}
			});

		};

		$scope.selecionarPesquisa = function (pesquisa) {
			$modalInstance.close({
				pesquisa: pesquisa
			});
		};

		//MODAL RESULT
		$scope.ok = function (pesquisa) {
			$scope.cancel();
		};

		//MODAL RESULT
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};

		$scope.pesquisasSelected = [];

		$scope.pesquisaCheck = function (pesquisa) {

			if (pesquisa.checked) {
				$scope.pesquisasSelected.push(pesquisa.id);
			} else {
				for (var i = 0; i < $scope.pesquisasSelected.length; i++) {
					var p = $scope.pesquisasSelected[i];
					if (p === pesquisa.id) {
						$scope.pesquisasSelected.splice(i, 1);
						break;
					}
				}
			}

		};

	});