'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PesquisaPesquisaCtrl
 * @description
 * # PesquisaPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('PesquisaPesquisaModalCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope, $modalInstance,
		MsgService, ngTableParams, GridUtilsService, PesquisaService, GrupoOcupacionalService, avaliadoResultado, idGrupoOcupacional, idCargo, idSetor, idEmpresa) {

		document.title = 'Pesquisa Pesquisa - Conecthare';
		$rootScope.activeMenu = 'funcionario';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.pesquisa = {};

		//MODAL VARIAVEIS
		if (avaliadoResultado) {
			$scope.avaliadoResultado = avaliadoResultado;
			$scope.idEmpresa = avaliadoResultado.idEmpresa;
		}
		$scope.idGrupoOcupacional = idGrupoOcupacional;
		$scope.idCargo = idCargo;
		$scope.idSetor = idSetor;
		if (idEmpresa) {
			$scope.idEmpresa = idEmpresa;
			$scope.idEmpresaPesquisa = idEmpresa;
		}
		$scope.verResultado = true;

		//$scope.timeout = (!!$scope.avaliadoResultado ? 1000 : 0);
		$scope.timeout = 1000;

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 5
		};


		$scope.limparFiltro = function() {
			$scope.pesquisa = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function() {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				if ($rootScope.idEmpresaUser) {
					$scope.pesquisa.idEmpresa = $rootScope.idEmpresaUser;
				}
				if ($scope.idEmpresa) {
					$scope.pesquisa.idEmpresa = $scope.idEmpresa;
				}
				if ($scope.idEmpresaPesquisa) {
					$scope.pesquisa.idEmpresa = $scope.idEmpresaPesquisa;
				}

				$scope.paginator.entity = $scope.pesquisa;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = [];

				PesquisaService.pesquisar($scope.paginator, function(paginator) {
					$scope.paginator = paginator;
					$scope.paginator.entity = $scope.pesquisa;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$timeout(function() {
			$scope.paramsDataGrid = new ngTableParams({
				count: 5, // count per page
			}, {
				total: 0, // length of data
				counts: [5, 50, 100],
				getData: $scope.realizarPesquisar
			});
		}, $scope.timeout);


		//MODAL RESULT
		$scope.ok = function(pesquisa) {
			$modalInstance.close({
				idPesquisa: pesquisa.id,
				idAvaliado: $scope.avaliadoResultado.id
			});
		};

		//MODAL RESULT
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};

		$scope.pesquisasSelected = [];

		$scope.pesquisaCheck = function(pesquisa) {

			if (pesquisa.checked) {
				$scope.pesquisasSelected.push(pesquisa.id);
			} else {
				for (var i = 0; i < $scope.pesquisasSelected.length; i++) {
					var p = $scope.pesquisasSelected[i];
					if (p === pesquisa.id) {
						$scope.pesquisasSelected.splice(i, 1);
						break;
					}
				}
			}

		};

		$scope.selecionarPesquisaResultado = function(pesquisa) {
			//$scope.ok(pesquisa);
			var url = '#/resultado/';

			var pesquisasIds;
			if (pesquisa) {
				pesquisasIds = pesquisa.id;
			} else {
				pesquisasIds = $scope.pesquisasSelected.toString();
			}

			pesquisasIds = '?idsPesquisa=' + pesquisasIds;


			if (!!$scope.avaliadoResultado && !!$scope.avaliadoResultado.id) {

				url += $scope.avaliadoResultado.id + '';

			} else if (!!$scope.idGrupoOcupacional) {

				url += 'byGrupoOcupacional/' + $scope.idGrupoOcupacional + '';

			} else if (!!$scope.idCargo) {

				url += 'byCargo/' + $scope.idCargo + '';

			} else if (!!$scope.idSetor) {

				url += 'bySetor/' + $scope.idSetor + '';

			} else if (!!$scope.idEmpresa) {

				url += 'byEmpresa/' + $scope.idEmpresa + '';

			}

			url = url + pesquisasIds;

			window.open(url, '_blank');

			//$scope.cancel();
		};

		$timeout(function() {
			$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
				idEmpresa: $rootScope.idEmpresaUser
			});
		}, $scope.timeout);

	});