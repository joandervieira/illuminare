'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PesquisaPesquisaCtrl
 * @description
 * # PesquisaPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('PesquisaPesquisaCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, PesquisaService, GrupoOcupacionalService) {

		document.title = 'Pesquisa Pesquisa - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.pesquisa = {};

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function () {
			$scope.pesquisa = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function () {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function ($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				$scope.paginator.entity = $scope.pesquisa;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = [];

				PesquisaService.pesquisar($scope.paginator, function (paginator) {
					$scope.paginator = paginator;
					$scope.paginator.entity = $scope.pesquisa;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
				total: 0, // length of data
				counts: [10, 50, 100],
				getData: $scope.realizarPesquisar
			});


		$scope.clonar = function (pesquisa) {
			$('.loadingApp').show();
			PesquisaService.clonar({ idPesquisa: pesquisa.id }, function () {
				$('.loadingApp').hide();
				$scope.pesquisar();
			});
		}

		$scope.excluir = function (entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return 'Você deseja excluir esta Pesquisa ? <br> <strong>' + $scope.entityToExclude.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {

					PesquisaService.excluir({
						id: $scope.entityToExclude.id
					}, function (result) {
						MsgService.addSuccess('Pesquisa excluída com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};


		$scope.removerAvaliacoes = function (entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return 'Você deseja remover todas as avaliações ?';
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {

					PesquisaService.temAvaliacaoRespondida({
						idPesquisa: $scope.entityToExclude.id
					}, function (result) {

						if (result.temAvaliacaoRespondida) {

							$scope.confirmarRemoverAvaliacoes($scope.entityToExclude);

						} else {
							PesquisaService.removerAvaliacoesEResponstas({
								idPesquisa: $scope.entityToExclude.id
							}, function () {
								MsgService.addSuccess('Avaliações removidas com sucesso');
								$scope.initialLoad = true;
								$scope.pesquisar();
							});
						}

					});
				}
			});

		};

		$scope.confirmarRemoverAvaliacoes = function (entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return 'Existem avaliações respondidas ! Confirma a remoção de todas as avaliações desta pesquisa ?';
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {

					PesquisaService.removerAvaliacoesEResponstas({
						idPesquisa: $scope.entityToExclude.id
					}, function () {
						MsgService.addSuccess('Avaliações removidas com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();
					});
				}
			});

		};



		$scope.removerRespostas = function (entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return 'Você deseja remover todas as respostas ?';
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {

					PesquisaService.removerRespostas({
						idPesquisa: $scope.entityToExclude.id
					}, function () {
						MsgService.addSuccess('Respostas removidas com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();
					});
				}
			});

		};


		$scope.gruposOcupacionais = GrupoOcupacionalService.lista();

	});