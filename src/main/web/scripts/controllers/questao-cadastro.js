'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:QuestaoCadastroCtrl
 * @description
 * # QuestaoCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('QuestaoCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope, ngTableParams,
		MsgService, GrupoQuestaoService, QuestaoService, GridUtilsService, ListaValoresService) {

		document.title = 'Cadastro Questao - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.listaValoresInvalid = false;
			$scope.questao = {};
			$scope.questao.ativo = true;
			$scope.listasValores = [];
			$scope.tiposQuestoes = [{
				nome: 'Texto Livre',
				value: 'isTexto'
			}, {
				nome: 'Escala de Valores',
				value: 'isLista'
			}, {
				nome: 'Numérico',
				value: 'isNumerico'
			}];
			$scope.questao.tipoQuestao = $scope.tiposQuestoes[0];

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			$scope.listaValoresInvalid = false;

			var isInvalid = $scope.formPrincipal.$invalid;

			if (!$scope.questao.tipoQuestao) {
				isInvalid = true;
			}

			isInvalid = $scope.validarCampoListaValores(isInvalid);

			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.validarCampoListaValores = function(isInvalid) {
			if ($scope.questao.tipoQuestao) {
				if ($scope.questao.tipoQuestao.value === 'isLista' && !$scope.questao.listaValores) {
					isInvalid = true;
					$scope.listaValoresInvalid = true;
				}
			}
			return isInvalid;
		};

		$scope.paramsDataGridListaValores = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $scope.listasValores);
			}
		});



		$scope.cadastrarNovaListaValores = function(listaValores, indexListaValores) {
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/questao/cadastro-lista-valores.html',
				controller: 'QuestaoCadastroListaValoresDialogCtrl',
				size: 'lg',
				resolve: {
					listaValores: function() {
						return listaValores;
					},
					indexListaValores: function() {
						return indexListaValores
					}
				}
			});

			modalInstance.result.then(function(novaListaValores, indexListaValores) {
				if (novaListaValores) {
					$scope.questao.listaValores = novaListaValores;
					if (indexListaValores) {
						$scope.listasValores[indexListaValores] = novaListaValores;
					}
				}
			}, function() {
				//cancelar
			});
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.questao.tipoQuestao.value === 'isLista') {
					$scope.questao.texto = undefined;
					$scope.questao.isLista = true;
					$scope.questao.isTexto = false;
					$scope.questao.isNumerico = false;
				} else if ($scope.questao.tipoQuestao.value === 'isNumerico') {
					$scope.questao.texto = undefined;
					$scope.questao.isLista = false;
					$scope.questao.isTexto = false;
					$scope.questao.isNumerico = true;
				} else {
					$scope.questao.listaValores = undefined;
					$scope.questao.isLista = false;
					$scope.questao.isTexto = true;
					$scope.questao.isNumerico = false;
				}


				if ($scope.questao.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}

			}

		};

		$scope.salvar = function() {

			QuestaoService.salvar($scope.questao, function(result) {

				MsgService.addSuccess('Questão salva com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');
				$scope.pesquisarListaValores();

			});


		};

		$scope.atualizar = function() {

			QuestaoService.atualizar($scope.questao, function(result) {

				MsgService.addSuccess('Questão atualizada com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/questao/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/pesquisa/home');
		};


		$scope.editarListaValores = function(listaValores, indexListaValores) {
			$scope.cadastrarNovaListaValores(listaValores, indexListaValores);
		};

		$scope.excluirListaValores = function(listaValores) {
			$scope.entityToExclude = listaValores;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function() {
						return 'Você deseja excluir esta Escala de Avaliação ? <br> <strong>' + $scope.entityToExclude.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function(confirmed) {
				if (confirmed) {

					ListaValoresService.excluir({
						id: $scope.entityToExclude.id
					}, function(result) {
						MsgService.addSuccess('Escala de Avaliação excluída com sucesso');
						$scope.pesquisarListaValores();

					});
				}
			});


		};



		//$scope.gruposQuestoes = GrupoQuestaoService.lista();

		$scope.pesquisarListaValores = function(idEmpresa) {
			$scope.listasValores = ListaValoresService.lista({
				idEmpresa: idEmpresa
			}, function() {
				$scope.paramsDataGridListaValores.reload();
			});
		};

		$scope.$watch('questao.idEmpresa', function(newValue) {
			if (newValue) {
				$scope.pesquisarListaValores(newValue);
			}
		});

		//$scope.pesquisarListaValores();


		//EDITANDO
		if ($routeParams.id) {
			QuestaoService.get({
				id: $routeParams.id,
				fieldsToFetch: ['listaValores']
			}, function(questao) {
				if (questao.isLista) {
					questao.tipoQuestao = $scope.tiposQuestoes[1];
				} else if (questao.isNumerico) {
					questao.tipoQuestao = $scope.tiposQuestoes[2];
				} else {
					questao.tipoQuestao = $scope.tiposQuestoes[0];
				}
				$scope.questao = questao;
			});

		}

	});