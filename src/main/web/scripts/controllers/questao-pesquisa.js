'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:QuestaoPesquisaCtrl
 * @description
 * # QuestaoPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('QuestaoPesquisaCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, QuestaoService, GrupoQuestaoService) {

		document.title = 'Pesquisa Questão - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.questao = {};
		$scope.tiposQuestoes = [{
			nome: 'Escala',
			value: 'isLista'
		}, {
			nome: 'Numérico',
			value: 'isNumerico'
		}, {
			nome: 'Texto',
			value: 'isTexto'
		}];

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function () {
			$scope.questao = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function () {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function ($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				if ($scope.questao.tipoQuestao) {
					if ($scope.questao.tipoQuestao.value === 'isLista') {
						$scope.questao.texto = undefined;
						$scope.questao.isLista = true;
						$scope.questao.isTexto = false;
					} else {
						$scope.questao.listaValores = undefined;
						$scope.questao.isLista = false;
						$scope.questao.isTexto = true;
					}
				} else {
					$scope.questao.isLista = undefined;
					$scope.questao.isTexto = undefined;
				}

				$scope.paginator.entity = $scope.questao;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = ['listaValores'];

				QuestaoService.pesquisar($scope.paginator, function (paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
				total: 0, // length of data
				counts: [10, 50, 100],
				getData: $scope.realizarPesquisar
			});


		$scope.excluir = function (entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return 'Você deseja excluir este Questão ? <br> <strong>' + $scope.entityToExclude.nomeQuestao + '</strong>';
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {

					QuestaoService.excluir({
						id: $scope.entityToExclude.id
					}, function (result) {
						MsgService.addSuccess('Questão excluída com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};

		$scope.gruposQuestoes = GrupoQuestaoService.lista();

	});