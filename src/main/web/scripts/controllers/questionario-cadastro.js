'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:QuestionarioCadastroCtrl
 * @description
 * # QuestionarioCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('QuestionarioCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope, ngTableParams,
		MsgService, GrupoOcupacionalService, QuestionarioService, GridUtilsService, GrupoQuestaoService, CargoService, SetorService) {

		document.title = 'Cadastro Questionário - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.gruposQuestoesInvalid = false;
			$scope.questionario = {};
			$scope.questionario.gruposQuestoes = [];
			$scope.questionario.ativo = true;
			$scope.gruposQuestoes = [];

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;

			if (!isInvalid) {
				isInvalid = $scope.validarCampoGruposOcupacionais(isInvalid);
				isInvalid = $scope.validarCampoGrupoQuestoes(isInvalid);
			}

			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};


		$scope.$watch('showMsgsError', function(newValue) {
			if (newValue) {
				$scope.validarCampoGruposOcupacionais(false);
			}
		});

		$scope.validarCampoGruposOcupacionais = function(isInvalid) {
			var listaAtualizada = [];
			if ($scope.questionario.gruposOcupacionais && $scope.questionario.gruposOcupacionais.length > 0) {
				for (var i = 0; i < $scope.questionario.gruposOcupacionais.length; i++) {
					var object = $scope.questionario.gruposOcupacionais[i];
					if (object && object.id) {
						var duplicated = false;
						for (var j = 0; j < listaAtualizada.length; j++) {
							if (listaAtualizada[j].id === object.id) {
								duplicated = true;
								break;
							}
						};
						if (!duplicated) {
							listaAtualizada.push(object);
						}
					}
				};
			}

			if (listaAtualizada.length === 0) {
				//isInvalid = true;
				listaAtualizada[0] = undefined;
			}

			$scope.questionario.gruposOcupacionais = angular.copy(listaAtualizada);

			// if ($scope.formPrincipal.innerForm) {
			// 	$scope.formPrincipal.innerForm.grupoOcupacional.$error.required = isInvalid;
			// 	$scope.formPrincipal.innerForm.grupoOcupacional.$invalid = isInvalid;
			// 	$scope.formPrincipal.innerForm.grupoOcupacional.$valid = !isInvalid;
			// }
			// if ($scope.formPrincipal.innerFormCargo) {
			// 	$scope.formPrincipal.innerFormCargo.cargo.$error.required = isInvalid;
			// 	$scope.formPrincipal.innerFormCargo.cargo.$invalid = isInvalid;
			// 	$scope.formPrincipal.innerFormCargo.cargo.$valid = !isInvalid;
			// }
			// if ($scope.formPrincipal.innerFormSetor) {
			// 	$scope.formPrincipal.innerFormSetor.setor.$error.required = isInvalid;
			// 	$scope.formPrincipal.innerFormSetor.setor.$invalid = isInvalid;
			// 	$scope.formPrincipal.innerFormSetor.setor.$valid = !isInvalid;
			// }

			return isInvalid;
		};

		$scope.validarCampoGrupoQuestoes = function(isInvalid) {
			if (!$scope.questionario.gruposQuestoes || $scope.questionario.gruposQuestoes.length === 0) {
				isInvalid = true;
			} else {
				for (var i = 0; i < $scope.questionario.gruposQuestoes.length; i++) {
					var q = $scope.questionario.gruposQuestoes[i];
					if (!q.id) {
						isInvalid = true;
						break;
					}
				}
			}
			$scope.gruposQuestoesInvalid = isInvalid;
			return isInvalid;
		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.paramsDataGridGruposQuestoesSelecionadas = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				GridUtilsService.refreshData($defer, params, $scope.questionario.gruposQuestoes);
			}
		});

		$scope.selecionarGruposQuestoes = function() {
			var gruposQuestoesDisponiveis = $scope.gruposQuestoes;
			var gruposQuestoesSelecionadas = $scope.questionario.gruposQuestoes;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/grupo-questao/selecionar-grupos-questoes.html',
				controller: 'SelecionarGrupoQuestaoDialogCtrl',
				size: 'lg',
				resolve: {
					gruposQuestoesDisponiveis: function() {
						return gruposQuestoesDisponiveis;
					},
					gruposQuestoesSelecionadas: function() {
						return gruposQuestoesSelecionadas;
					}
				}
			});

			modalInstance.result.then(function(result) {

				$scope.gruposQuestoes = result.gruposQuestoesDisponiveis;
				$scope.questionario.gruposQuestoes = result.gruposQuestoesSelecionadas;

				if (!$scope.gruposQuestoes) {
					$scope.gruposQuestoes = [];
				}
				if (!$scope.questionario.gruposQuestoes) {
					$scope.questionario.gruposQuestoes = [];
				}

				if ($scope.questionario.gruposQuestoes.length > 0) {
					$scope.gruposQuestoesInvalid = false;
				} else {
					$scope.gruposQuestoesInvalid = true;
				}

				$scope.paramsDataGridGruposQuestoesSelecionadas.reload();

			}, function() {
				//cancelar
			});
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.questionario.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}
		};

		$scope.salvar = function() {

			QuestionarioService.salvar($scope.questionario, function(result) {

				MsgService.addSuccess('Questionário salvo com sucesso');
				$scope.screenInit();
				$scope.questionario.gruposOcupacionais = [{}];
				$scope.gruposQuestoes = angular.copy($scope.gruposQuestoesCache);
				$('.loadingApp').hide();
				$scope.focus('input.nome');

				$scope.questionario.gruposOcupacionais = [{}];
				$scope.questionario.cargos = [{}];
				$scope.questionario.setores = [{}];

			});


		};

		$scope.atualizar = function() {

			QuestionarioService.atualizar($scope.questionario, function(result) {

				MsgService.addSuccess('Questionário atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/questionario/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/pesquisa/home');
		};

		$scope.removerGrupoQuestaoSelecionada = function(grupoQuestao) {
			for (var i = 0; i < $scope.questionario.gruposQuestoes.length; i++) {
				var gq = $scope.questionario.gruposQuestoes[i];
				if (gq.id === grupoQuestao.id) {
					$scope.questionario.gruposQuestoes.splice(i, 1);
					break;
				}
			}
			$scope.gruposQuestoes.push(grupoQuestao);
			$scope.paramsDataGridGruposQuestoesSelecionadas.reload();
		};

		$scope.adicionarGrupoQuestao = function(grupoQuestao) {
			$scope.questionario.gruposQuestoes.push(grupoQuestao);
			for (var i = 0; i < $scope.gruposQuestoes.length; i++) {
				var gq = $scope.gruposQuestoes[i];
				if (gq.id === gruposQuestoes.id) {
					$scope.gruposQuestoes.splice(i, 1);
					break;
				}
			}
			$scope.paramsDataGridGruposQuestoesSelecionadas.reload();
		};


		$scope.pesquisarGruposQuestoes = function(idEmpresa, onEditQuestionario) {
			$('.loadingApp').show();
			//fieldsToFetch: ['questoesOrdem']
			GrupoQuestaoService.lista({
				idEmpresa: idEmpresa,
				fieldsToFetch: []
			}, function(gruposQuestoes) {
				$scope.gruposQuestoes = gruposQuestoes;
				$scope.gruposQuestoesCache = angular.copy(gruposQuestoes);
				//$scope.paramsDataGridQuestoesDisponiveis.reload();
				if (onEditQuestionario && $scope.gruposQuestoes && $scope.gruposQuestoes.length > 0) {
					if ($scope.questionario.gruposQuestoes && $scope.questionario.gruposQuestoes.length > 0) {
						var gruposQuestoesDisponiveisAtualizada = [];
						var jaSelecionada = false;
						for (var i = 0; i < $scope.gruposQuestoes.length; i++) { //grupos questoes disponiveis
							var grupoQuestaoDisponivel = $scope.gruposQuestoes[i];
							jaSelecionada = false;
							for (var j = 0; j < $scope.questionario.gruposQuestoes.length; j++) { //grupos questoes selecionadas
								var grupoQuestaoSelecionada = $scope.questionario.gruposQuestoes[j];
								if (grupoQuestaoSelecionada.id === grupoQuestaoDisponivel.id) {
									jaSelecionada = true;
									break;
								}
							}
							if (!jaSelecionada) {
								gruposQuestoesDisponiveisAtualizada.push(grupoQuestaoDisponivel);
							}
						}
						$scope.gruposQuestoes = gruposQuestoesDisponiveisAtualizada;
					}
				}
				$('.loadingApp').hide();
			});
		};



		$scope.$watch('questionario.idEmpresa', function(newValue) {
			if (newValue) {
				$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
					idEmpresa: newValue,
					orderBy: '+nome'
				});
				$scope.cargos = CargoService.lista({
					idEmpresa: newValue,
					orderBy: '+nome'
				});
				$scope.setores = SetorService.lista({
					idEmpresa: newValue,
					orderBy: '+nome'
				});

				$scope.gruposOcupacionaisBck = $scope.gruposOcupacionais;
				$scope.cargosBck = $scope.cargos;
				$scope.setoresBck = $scope.setores;

				$scope.pesquisarGruposQuestoes(newValue, $scope.onEditQuestionario);
			}
		});


		// $scope.gruposQuestoes = GrupoQuestaoService.lista({
		// 	fieldsToFetch: ['questoesOrdem']
		// });

		//EDITANDO
		//fieldsToFetch: ['gruposOcupacionais', 'gruposQuestoes gq', '[gq].questoesOrdem']
		if ($routeParams.id) {
			QuestionarioService.get({
				id: $routeParams.id,
				fieldsToFetch: ['gruposOcupacionais', 'cargos', 'setores', 'gruposQuestoes gq']
			}, function(questionario) {
				$scope.onEditQuestionario = true;
				if (!questionario.gruposOcupacionais || questionario.gruposOcupacionais.length === 0) {
					questionario.gruposOcupacionais = [{}];
				}
				if (!questionario.cargos || questionario.cargos.length === 0) {
					questionario.cargos = [{}];
				}
				if (!questionario.setores || questionario.setores.length === 0) {
					questionario.setores = [{}];
				}
				if (!questionario.gruposQuestoes) {
					questionario.gruposQuestoes = [];
				}
				$scope.questionario = questionario;
				$scope.paramsDataGridGruposQuestoesSelecionadas.reload();
				//$scope.pesquisarGruposQuestoes(true);


			});

		} else {
			$scope.questionario.gruposOcupacionais = [{}];
			$scope.questionario.cargos = [{}];
			$scope.questionario.setores = [{}];
			//$scope.pesquisarGruposQuestoes();
		}

	});