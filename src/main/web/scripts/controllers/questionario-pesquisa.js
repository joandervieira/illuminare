'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:QuestionarioPesquisaCtrl
 * @description
 * # QuestionarioPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('QuestionarioPesquisaCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, QuestionarioService, GrupoOcupacionalService) {

		document.title = 'Pesquisa Questionário - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.questionario = {};
		$scope.questionario.gruposOcupacionais = [];

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function() {
			$scope.questionario = {};
			$scope.questionario.gruposOcupacionais = [];
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function() {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();
				if ($rootScope.idEmpresaUser) {
					$scope.questionario.idEmpresa = $rootScope.idEmpresaUser;
				}

				$scope.paginator.entity = $scope.questionario;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = ['gruposOcupacionais g'];

				QuestionarioService.pesquisar($scope.paginator, function(paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: $scope.realizarPesquisar
		});


		$scope.excluir = function(entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function() {
						return 'Você deseja excluir este Questionário ? <br> <strong>' + $scope.entityToExclude.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function(confirmed) {
				if (confirmed) {

					QuestionarioService.excluir({
						id: $scope.entityToExclude.id
					}, function(result) {
						MsgService.addSuccess('Questionário excluído com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};

		$scope.classificarQuestoes = function(questionario) {

			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/questionario/classificar-questoes.html',
				controller: 'ClassificarQuestoesDialogCtrl',
				size: 'lg',
				resolve: {
					questionario: function() {
						return questionario;
					}
				}
			});

			modalInstance.result.then(function(result) {
				//ok
			}, function() {
				//cancelar
			});

		};

		$scope.gruposOcupacionais = GrupoOcupacionalService.lista({
			idEmpresa: $rootScope.idEmpresaUser
		});

	}).controller('ClassificarQuestoesDialogCtrl', function($scope, $modalInstance, $timeout, $q,
		GridUtilsService, ngTableParams, MsgService, QuestionarioService, QuestaoClassificacaoService, questionario) {

		$scope.questionario = questionario;

		$('.loadingApp').show();
		QuestionarioService.get({
			id: questionario.id,
			fieldsToFetch: ['gruposQuestoes gq', '[gq].questoesOrdem qo', 'classificacao']
		}, function(result) {
			$('.loadingApp').hide();
			$scope.questionario = result;

			if (result) {
				for (var i = 0; i < $scope.questionario.gruposQuestoes.length; i++) {
					var gruposQuestao = $scope.questionario.gruposQuestoes[i];

					for (var j = 0; j < gruposQuestao.questoesOrdem.length; j++) {
						var questaoOrdem = gruposQuestao.questoesOrdem[j];
						var questao = questaoOrdem.questao;

						if (questao.questaoClassificacao) {
							questao.classificacao = questao.questaoClassificacao.classificacao;
						} else {
							questao.classificacao = 1;
						}
					}
				}
			}
		});


		$scope.ok = function() {
			$('.loadingApp').show();

			$scope.salvar();
		};

		$scope.salvar = function() {
			var payload = [];

			for (var i = 0; i < $scope.questionario.gruposQuestoes.length; i++) {
				var gruposQuestao = $scope.questionario.gruposQuestoes[i];

				for (var j = 0; j < gruposQuestao.questoesOrdem.length; j++) {
					var questaoOrdem = gruposQuestao.questoesOrdem[j];
					var questao = questaoOrdem.questao;
					var id = null;
					if (questao.questaoClassificacao && questao.questaoClassificacao.id) {
						id = questao.questaoClassificacao.id;
					}

					var questaoClassificacao = {
						id: id,
						idQuestionario: $scope.questionario.id,
						idGrupoQuestao: gruposQuestao.id,
						idQuestao: questaoOrdem.questao.id,
						classificacao: questaoOrdem.questao.classificacao
					};

					payload.push(questaoClassificacao);
				}
			}


			QuestaoClassificacaoService.salvarTodos(payload, function() {
				$('.loadingApp').hide();
				MsgService.addSuccess('Classificação atualizada com sucesso!', true);
				$modalInstance.close();
			});
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};

	});