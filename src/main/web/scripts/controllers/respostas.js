'use strict';

angular.module('frontendApp')
	.controller('RespostasCtrl', function($scope, $routeParams,
		MsgService, RespostaService) {

		document.title = 'Pesquisa - Conecthare';
		$scope.activeMenu = 'pesquisa';


		MsgService.cleanAll(true);

		$scope.pesquisaNome = '';
		$scope.avaliadoNome = '';
		$scope.avaliadorNome = '';
		$scope.avaliadoId = '';
		$scope.avaliadorId = '';

		$scope.questionariosRespostas = RespostaService.findByAvaliacaoAgrupado({
			id: $routeParams.idAvaliacao
		}, function(resp) {
			console.log('teste');
			if (resp && resp.length > 0) {
				$scope.pesquisaNome = resp[0].pesquisaNome;
				$scope.avaliadoNome = resp[0].avaliadoNome;
				$scope.avaliadorNome = resp[0].avaliadorNome;
				$scope.avaliadoId = resp[0].avaliadoId;
				$scope.avaliadorId = resp[0].avaliadorId;

				$scope.datacadastro = resp[0].gruposQuestoes[0].questoesOrdem[0].questao.resposta.dataCadastro;
			}
		});

	});