'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ResultadoAdminCtrl
 * @description
 * # ResultadoAdminCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('ResultadoAdminCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, PesquisaService) {

		document.title = 'Resultado - Conecthare';
		$rootScope.activeMenu = 'pesquisa';

		$scope.screenInit = function() {
			$scope.resultado = {};
			$scope.avaliados = [];
			$scope.funcionario = {};
			$scope.loadDataGrid = true;
			$scope.initialLoad = true;
		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.paginatorAvaliados = {
			pageNumber: 1,
			pageSize: 10
		};

		

		$scope.dataGridAvaliados = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: $scope.realizarPesquisarAvaliados
		});



		//EDITANDO
		if ($routeParams.idPesquisa) {
			$('.loadingApp').show();
			$scope.idPesquisa = $routeParams.idPesquisa;
			$scope.resultado = PesquisaService.avaliados({
				id: $routeParams.idPesquisa
			}, function(avaliados) {
				$scope.avaliados = avaliados;
				$scope.pesquisarAvaliados();
				$('.loadingApp').hide();

			});

		}

	});