'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ResultadoCtrl
 * @description
 * # ResultadoCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('ResultadoCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope, $filter,
		MsgService, ResultadoService, FuncionarioService, MetaService, GrupoOcupacionalService, CargoService, SetorService, EmpresaService) {

		document.title = 'Resultado - Conecthare';
		$rootScope.activeMenu = 'main';


		$scope.resumaoQuestionario = {
			mediaPropria: 9999,
			mediaLideres: 9999,
			mediaSubordinados: 9999,
			mediaOutros: 9999,
			meta: 9999,
			valorObtido: 99999,
			percentualMeta: 99999
		};


		var drawChart = function (idQuestionario, dataArray) {
			$timeout(function () {
				makeDrawChart(idQuestionario, dataArray);
			}, 500);
		}

		var makeDrawChart = function (idQuestionario, dataArray) {
			google.charts.load('current', {
				'packages': ['corechart', 'bar']
			});
			google.charts.setOnLoadCallback(drawStuff);

			function drawStuff() {

				//var button = document.getElementById('change-chart');
				var chartDiv = document.getElementById('chart_div_questionario_' + idQuestionario);

				// var data = google.visualization.arrayToDataTable([
				// 	['Grupos', 'Auto Avaliação', 'Avaliação Superior'],
				// 	['Grupo teste2 (na + teste numerico)', 8000, 23.3],
				// 	['Grupo teste1 faturamento + funcionarios', 24000, 4.5]
				// ]);


				var data = google.visualization.arrayToDataTable(dataArray);

				var classicOptions = {
					width: 1001,
					chart: {},
					series: {
						0: {
							targetAxisIndex: 0
						},
						1: {
							targetAxisIndex: 0
						},
						2: {
							targetAxisIndex: 0
						},
						3: {
							targetAxisIndex: 0
						},
						4: {
							targetAxisIndex: 1
						}
					},
					vAxes: {
						0: {
							title: 'Média',
							minValue: 0,
							maxValue: 5,
							gridlines: {
								count: 6
							}

						},
						1: {
							title: '% Meta',
							minValue: 0,
							maxValue: 100,
							gridlines: {
								count: 6
							}
						}
					},
					hAxis: {
						slantedText: true,
						slantedTextAngle: 25,
						showTextEvery: 1
					}
				};

				// var classicOptions = {
				// 	width: 900,
				// 	series: {
				// 		0: {
				// 			targetAxisIndex: 0
				// 		},
				// 		1: {
				// 			targetAxisIndex: 1
				// 		}
				// 	},
				// 	title: 'Nearby galaxies - distance on the left, brightness on the right',
				// 	vAxes: {
				// 		// Adds titles to each axis.
				// 		0: {
				// 			title: 'parsecs'
				// 		},
				// 		1: {
				// 			title: 'apparent magnitude'
				// 		}
				// 	}
				// };

				function drawMaterialChart() {
					var materialChart = new google.charts.Bar(chartDiv);
					materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
					// button.innerText = 'Change to Classic';
					// button.onclick = drawClassicChart;
				}

				function drawClassicChart() {
					var classicChart = new google.visualization.ColumnChart(chartDiv);
					classicChart.draw(data, classicOptions);
					// button.innerText = 'Change to Material';
					// button.onclick = drawMaterialChart;
				}

				//drawMaterialChart();
				drawClassicChart();
			};
		};



		$scope.fillDataGrafico = function (grupoQuestao) {

			var propria = grupoQuestao.mediaPropria || 0;
			var lider = grupoQuestao.mediaLideres || 0;
			var subordinados = grupoQuestao.mediaSubordinados || 0;
			var outros = grupoQuestao.mediaOutros || 0;
			var valorObtido = grupoQuestao.valorObtido || 0;
			var percentualMeta = $filter('number')(grupoQuestao.percentualMeta * 100, 1) || 0;
			if (percentualMeta) {
				percentualMeta = percentualMeta.replace(",", ".");
				percentualMeta = Number(percentualMeta);
			}

			$scope.dataArray.push([grupoQuestao.nome, propria, lider, subordinados, outros, percentualMeta]);

		};



		// ******************************************************************************************

		$scope.showResult = false;
		$scope.resultSuccess = false;
		$scope.chartObject = {
			'type': 'ColumnChart',
			'displayed': true,
			'data': {
				'cols': [{
					'id': 'grupo',
					'label': 'Grupo',
					'type': 'string',
					'p': {}
				}, {
					'id': 'autoAvaliacao',
					'label': 'Auto Avaliação',
					'type': 'number',
					'p': {}
				}, {
					'id': 'superior',
					'label': 'Superior',
					'type': 'number',
					'p': {}
				}, {
					'id': 'subordinados',
					'label': 'Subordinados',
					'type': 'number',
					'p': {}
				}, {
					'id': 'outros',
					'label': 'Outros',
					'type': 'number',
					'p': {}
				}],
				'rows': []
			},
			'options': {
				'legend': {
					'position': 'bottom'
				},
				height: 500,
				chartArea: {
					left: 35,
					top: 10,
					bottom: 100,
					width: '100%',
					height: '70%'
				},
				vAxes: {
					"0": {
						title: "Temperature (C)"
					},
					"1": {
						title: "Sap Collected (L)"
					}
				},
			}
		};

		$scope.chartObject.data.rows = [];

		$scope.screenInit = function () {
			$scope.resultado = {};
		};

		MsgService.cleanAll(true);
		$scope.screenInit();


		$scope.scrollTo = function (id) {
			$rootScope.scrollTo(id);
		}


		$scope.montarGrafico = function (grupoQuestao) {


			$scope.chartObject.data.rows.push({
				'c': [{
					'v': grupoQuestao.nome
				}, {
					'v': grupoQuestao.mediaPropria,
					'f': $filter('number')(grupoQuestao.mediaPropria, 1)
				}, {
					'v': grupoQuestao.mediaLideres,
					'f': $filter('number')(grupoQuestao.mediaLideres, 1)
				}, {
					'v': grupoQuestao.mediaSubordinados,
					'f': $filter('number')(grupoQuestao.mediaSubordinados, 1)
				}, {
					'v': grupoQuestao.mediaOutros,
					'f': $filter('number')(grupoQuestao.mediaOutros, 1)
				}]
			});

		};

		$scope.prepararResultados = function (resultados, opts) {

			if (!resultados || resultados.length === 0) {
				MsgService.addFailed('Resultado não encontrado');
				$('.loadingApp').hide();
				return;
			}

			$scope.resultados = resultados;

			var showMediaPropria = false;
			var showMediaLideres = false;
			var showMediaSubordinados = false;
			var showMediaOutros = false;

			var mediaPropria = 0;
			var mediaLideres = 0;
			var mediaSubordinados = 0;
			var mediaOutros = 0;
			var meta = 0;
			var valorObtido = 0;
			var percentualMeta = 0;

			var qtdQuestionarioMedia = 0;
			var qtdQuestionarioMeta = 0;
			var qtdQuestionarioValorObtido = 0;
			var qtdQuestionarioPercentualMeta = 0;

			for (var indexResultados = 0; indexResultados < resultados.length; indexResultados++) {
				var resultado = resultados[indexResultados];

				$scope.nomePesquisa = resultado.pesquisa.nome;

				$scope.prepararResultado(resultado, opts);

				if (!resultado.resumaoQuestionario) {
					continue;
				}

				var hasMedia = false;

				if (!resultado.resumaoQuestionario.hidePropria) {
					showMediaPropria = true;
				}

				if (!resultado.resumaoQuestionario.hideLideres) {
					showMediaLideres = true;
				}

				if (!resultado.resumaoQuestionario.hideSubordinados) {
					showMediaSubordinados = true;
				}

				if (!resultado.resumaoQuestionario.hideOutros) {
					showMediaOutros = true;
				}

				if (resultado.resumaoQuestionario.mediaPropria) {
					mediaPropria += resultado.resumaoQuestionario.mediaPropria;
					hasMedia = true;
				}

				if (resultado.resumaoQuestionario.mediaLideres) {
					mediaLideres += resultado.resumaoQuestionario.mediaLideres;
					hasMedia = true;
				}

				if (resultado.resumaoQuestionario.mediaSubordinados) {
					mediaSubordinados += resultado.resumaoQuestionario.mediaSubordinados;
					hasMedia = true;
				}

				if (resultado.resumaoQuestionario.mediaOutros) {
					mediaOutros += resultado.resumaoQuestionario.mediaOutros;
					hasMedia = true;
				}

				if (hasMedia) {
					qtdQuestionarioMedia++;
				}

				if (resultado.resumaoQuestionario.meta) {
					meta += resultado.resumaoQuestionario.meta;
					qtdQuestionarioMeta++;
				}

				if (resultado.resumaoQuestionario.valorObtido) {
					valorObtido += resultado.resumaoQuestionario.valorObtido;
					qtdQuestionarioValorObtido++;
				}

				if (resultado.resumaoQuestionario.percentualMeta) {
					percentualMeta += resultado.resumaoQuestionario.percentualMeta;
					qtdQuestionarioPercentualMeta++;
				}


			} //FOR PESQUISAS


			$scope.resumaoPesquisas = {
				mediaPropria: mediaPropria / qtdQuestionarioMedia,
				mediaLideres: mediaLideres / qtdQuestionarioMedia,
				mediaSubordinados: mediaSubordinados / qtdQuestionarioMedia,
				mediaOutros: mediaOutros / qtdQuestionarioMedia,
				meta: meta / qtdQuestionarioMeta,
				valorObtido: valorObtido / qtdQuestionarioValorObtido,
				percentualMeta: percentualMeta / qtdQuestionarioPercentualMeta
			};

			$scope.resumaoPesquisas.hidePropria = !showMediaPropria;
			$scope.resumaoPesquisas.hideLideres = !showMediaLideres;
			$scope.resumaoPesquisas.hideSubordinados = !showMediaSubordinados;
			$scope.resumaoPesquisas.hideOutros = !showMediaOutros;


			$('.loadingApp').hide();
			$scope.showResult = true;

		};

		$scope.prepararResultado = function (resultado, opts) {

			if (!resultado.questionarios || resultado.questionarios.length === 0) {
				MsgService.addFailed('Resultado não encontrado');
			} else {

				var showMediaPropria = false;
				var showMediaLideres = false;
				var showMediaSubordinados = false;
				var showMediaOutros = false;

				var mediaPropria = 0;
				var mediaLideres = 0;
				var mediaSubordinados = 0;
				var mediaOutros = 0;
				var meta = 0;
				var valorObtido = 0;
				var percentualMeta = 0;

				var qtdQuestionarioMedia = 0;
				var qtdQuestionarioMeta = 0;
				var qtdQuestionarioValorObtido = 0;
				var qtdQuestionarioPercentualMeta = 0;

				for (var i = 0; i < resultado.questionarios.length; i++) {
					var questionario = resultado.questionarios[i];

					var hasMedia = false;
					if (!questionario.mediaPropria) {
						questionario.hidePropria = true;
					} else {
						showMediaPropria = true;
						mediaPropria += questionario.mediaPropria;
						hasMedia = true;
					}
					if (!questionario.mediaLideres) {
						questionario.hideLideres = true;
					} else {
						showMediaLideres = true;
						mediaLideres += questionario.mediaLideres;
						hasMedia = true;
					}
					if (!questionario.mediaSubordinados) {
						questionario.hideSubordinados = true;
					} else {
						showMediaSubordinados = true;
						mediaSubordinados += questionario.mediaSubordinados;
						hasMedia = true;
					}
					if (!questionario.mediaOutros) {
						questionario.hideOutros = true;
					} else {
						showMediaOutros = true;
						mediaOutros += questionario.mediaOutros;
						hasMedia = true;
					}

					if (hasMedia) {
						qtdQuestionarioMedia++;
					}

					if (questionario.meta) {
						meta += questionario.meta;
						qtdQuestionarioMeta++;
					}

					if (questionario.valorObtido) {
						valorObtido += questionario.valorObtido;
						qtdQuestionarioValorObtido++;
					}

					if (questionario.percentualMeta) {
						percentualMeta += questionario.percentualMeta;
						qtdQuestionarioPercentualMeta++;
					}


					if (!opts.idEmpresa) {
						var columnsChart = ['Grupos', 'Auto Avaliação', 'Superior', 'Subordinados', 'Outros', '% Meta'];

						$scope.dataArray = [
							columnsChart
						];

						for (var j = 0; j < questionario.gruposQuestoes.length; j++) {
							var grupoQuestao = questionario.gruposQuestoes[j];
							$scope.fillDataGrafico(grupoQuestao);
						};

						var idQuestionario = questionario.id;
						drawChart(idQuestionario, $scope.dataArray);
					}

				}; //for questionarios

				var qtdQuestionarios = resultado.questionarios.length;

				resultado.resumaoQuestionario = {
					mediaPropria: mediaPropria / qtdQuestionarioMedia,
					mediaLideres: mediaLideres / qtdQuestionarioMedia,
					mediaSubordinados: mediaSubordinados / qtdQuestionarioMedia,
					mediaOutros: mediaOutros / qtdQuestionarioMedia,
					meta: meta / qtdQuestionarioMeta,
					valorObtido: valorObtido / qtdQuestionarioValorObtido,
					percentualMeta: percentualMeta / qtdQuestionarioPercentualMeta
				};

				resultado.resumaoQuestionario.hidePropria = !showMediaPropria;
				resultado.resumaoQuestionario.hideLideres = !showMediaLideres;
				resultado.resumaoQuestionario.hideSubordinados = !showMediaSubordinados;
				resultado.resumaoQuestionario.hideOutros = !showMediaOutros;



				if (opts.idAvaliado) {
					$scope.prepararAvaliado(opts.idAvaliado);
					if ($scope.resultados.length === 1) {
						$scope.prepararMeta(resultado.pesquisa.id, opts.idAvaliado);
					}
				}

				$scope.resultSuccess = true;

				if (opts.idEmpresa) {
					sumarizarResumaoGrupoQuestoes(resultado);

					var columnsChart = ['Grupos', 'Auto Avaliação', 'Superior', 'Subordinados', 'Outros', '% Meta'];

					$scope.dataArray = [
						columnsChart
					];

					resultado.resumaoGrupoQuestoes.gruposQuestoes.forEach(grupo => {
						$scope.fillDataGrafico(grupo);
					})

					drawChart('resumao_grupo_questoes_pesquisa_' + resultado.pesquisa.id, $scope.dataArray);
				}

			} //success

		};

		var sumarizarResumaoGrupoQuestoes = function (resultado) {
			resultado.questionarios.forEach(questionario => {

				if (questionario.gruposQuestoes && questionario.gruposQuestoes.length > 0) {
					questionario.gruposQuestoes.forEach(grupoQuestao => {

						if (!resultado.resumaoGrupoQuestoes) {
							resultado.resumaoGrupoQuestoes = { gruposQuestoes: [] };
						}

						var found = resultado.resumaoGrupoQuestoes.gruposQuestoes.filter(function (grupo) {
							return grupo.id === grupoQuestao.id;
						});

						if (!found[0]) {
							grupoQuestao.nroGruposSumarizados = 1;

							if (grupoQuestao.mediaPropria) {
								grupoQuestao.nroGruposSumarizadosPropria = 1;
							}
							if (grupoQuestao.mediaLideres) {
								grupoQuestao.nroGruposSumarizadosLideres = 1;
							}
							if (grupoQuestao.mediaSubordinados) {
								grupoQuestao.nroGruposSumarizadosSubordinados = 1;
							}
							if (grupoQuestao.mediaOutros) {
								grupoQuestao.nroGruposSumarizadosOutros = 1;
							}
							if (grupoQuestao.meta) {
								grupoQuestao.nroGruposSumarizadosMeta = 1;
							}
							if (grupoQuestao.valorObtido) {
								grupoQuestao.nroGruposSumarizadosValorObtido = 1;
							}
							if (grupoQuestao.percentualMeta) {
								grupoQuestao.nroGruposSumarizadosPercentualMeta = 1;
							}

							resultado.resumaoGrupoQuestoes.gruposQuestoes.push(grupoQuestao);

						} else {
							resultado.resumaoGrupoQuestoes.gruposQuestoes.forEach(function (grupo) {
								if (grupo.id === grupoQuestao.id) {

									if (grupoQuestao.mediaPropria) {
										grupo.mediaPropria += grupoQuestao.mediaPropria;
										grupo.nroGruposSumarizadosPropria = !!grupo.nroGruposSumarizadosPropria ? grupo.nroGruposSumarizadosPropria += 1 : 1;
									}
									if (grupoQuestao.mediaLideres) {
										grupo.mediaLideres += grupoQuestao.mediaLideres;
										grupo.nroGruposSumarizadosLideres = !!grupo.nroGruposSumarizadosLideres ? grupo.nroGruposSumarizadosLideres += 1 : 1;
									}
									if (grupoQuestao.mediaSubordinados) {
										grupo.mediaSubordinados += grupoQuestao.mediaSubordinados;
										grupo.nroGruposSumarizadosSubordinados = !!grupo.nroGruposSumarizadosSubordinados ? grupo.nroGruposSumarizadosSubordinados += 1 : 1;
									}
									if (grupoQuestao.mediaOutros) {
										grupo.mediaOutros += grupoQuestao.mediaOutros;
										grupo.nroGruposSumarizadosOutros = !!grupo.nroGruposSumarizadosOutros ? grupo.nroGruposSumarizadosOutros += 1 : 1;
									}
									if (grupoQuestao.meta) {
										grupo.meta += grupoQuestao.meta;
										grupo.nroGruposSumarizadosMeta = !!grupo.nroGruposSumarizadosMeta ? grupo.nroGruposSumarizadosMeta += 1 : 1;
									}
									if (grupoQuestao.valorObtido) {
										grupo.valorObtido += grupoQuestao.valorObtido;
										grupo.nroGruposSumarizadosValorObtido = !!grupo.nroGruposSumarizadosValorObtido ? grupo.nroGruposSumarizadosValorObtido += 1 : 1;
									}
									if (grupoQuestao.percentualMeta) {
										grupo.percentualMeta += grupoQuestao.percentualMeta;
										grupo.nroGruposSumarizadosPercentualMeta = !!grupo.nroGruposSumarizadosPercentualMeta ? grupo.nroGruposSumarizadosPercentualMeta += 1 : 1;
									}

								}
							});
						}

					});
				}
			})

			resultado.resumaoGrupoQuestoes.gruposQuestoes.forEach(grupo => {

				if (grupo.mediaPropria) {
					grupo.mediaPropria = grupo.mediaPropria / grupo.nroGruposSumarizadosPropria;
				}
				if (grupo.mediaLideres) {
					grupo.mediaLideres = grupo.mediaLideres / grupo.nroGruposSumarizadosLideres;
				}
				if (grupo.mediaSubordinados) {
					grupo.mediaSubordinados = grupo.mediaSubordinados / grupo.nroGruposSumarizadosSubordinados;
				}
				if (grupo.mediaOutros) {
					grupo.mediaOutros = grupo.mediaOutros / grupo.nroGruposSumarizadosOutros;
				}
				if (grupo.meta) {
					grupo.meta = grupo.meta / grupo.nroGruposSumarizadosMeta;
				}
				if (grupo.valorObtido) {
					grupo.valorObtido = grupo.valorObtido / grupo.nroGruposSumarizadosValorObtido;
				}
				if (grupo.percentualMeta) {
					grupo.percentualMeta = grupo.percentualMeta / grupo.nroGruposSumarizadosPercentualMeta;
				}

			})

			var resultadoGeralDoResumaoDoGrupo = {};
			resultado.resumaoGrupoQuestoes.gruposQuestoes.forEach(grupo => {

				if (grupo.mediaPropria) {
					if (!resultadoGeralDoResumaoDoGrupo.mediaPropria) {
						resultadoGeralDoResumaoDoGrupo.mediaPropria = grupo.mediaPropria;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaPropria = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.mediaPropria += grupo.mediaPropria;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaPropria++;
					}
				}

				if (grupo.mediaLideres) {
					if (!resultadoGeralDoResumaoDoGrupo.mediaLideres) {
						resultadoGeralDoResumaoDoGrupo.mediaLideres = grupo.mediaLideres;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaLideres = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.mediaLideres += grupo.mediaLideres;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaLideres++;
					}
				}

				if (grupo.mediaSubordinados) {
					if (!resultadoGeralDoResumaoDoGrupo.mediaSubordinados) {
						resultadoGeralDoResumaoDoGrupo.mediaSubordinados = grupo.mediaSubordinados;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaSubordinados = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.mediaSubordinados += grupo.mediaSubordinados;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaSubordinados++;
					}
				}

				if (grupo.mediaOutros) {
					if (!resultadoGeralDoResumaoDoGrupo.mediaOutros) {
						resultadoGeralDoResumaoDoGrupo.mediaOutros = grupo.mediaOutros;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaOutros = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.mediaOutros += grupo.mediaOutros;
						resultadoGeralDoResumaoDoGrupo.qtde_mediaOutros++;
					}
				}

				if (grupo.meta) {
					if (!resultadoGeralDoResumaoDoGrupo.meta) {
						resultadoGeralDoResumaoDoGrupo.meta = grupo.meta;
						resultadoGeralDoResumaoDoGrupo.qtde_meta = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.meta += grupo.meta;
						resultadoGeralDoResumaoDoGrupo.qtde_meta++;
					}
				}

				if (grupo.valorObtido) {
					if (!resultadoGeralDoResumaoDoGrupo.valorObtido) {
						resultadoGeralDoResumaoDoGrupo.valorObtido = grupo.valorObtido;
						resultadoGeralDoResumaoDoGrupo.qtde_valorObtido = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.valorObtido += grupo.valorObtido;
						resultadoGeralDoResumaoDoGrupo.qtde_valorObtido++;
					}
				}

				if (grupo.percentualMeta) {
					if (!resultadoGeralDoResumaoDoGrupo.percentualMeta) {
						resultadoGeralDoResumaoDoGrupo.percentualMeta = grupo.percentualMeta;
						resultadoGeralDoResumaoDoGrupo.qtde_percentualMeta = 1;
					} else {
						resultadoGeralDoResumaoDoGrupo.percentualMeta += grupo.percentualMeta;
						resultadoGeralDoResumaoDoGrupo.qtde_percentualMeta++;
					}
				}

			})

			if (resultadoGeralDoResumaoDoGrupo.mediaPropria) {
				resultadoGeralDoResumaoDoGrupo.mediaPropria = resultadoGeralDoResumaoDoGrupo.mediaPropria / resultadoGeralDoResumaoDoGrupo.qtde_mediaPropria;
			}
			if (resultadoGeralDoResumaoDoGrupo.mediaLideres) {
				resultadoGeralDoResumaoDoGrupo.mediaLideres = resultadoGeralDoResumaoDoGrupo.mediaLideres / resultadoGeralDoResumaoDoGrupo.qtde_mediaLideres;
			}
			if (resultadoGeralDoResumaoDoGrupo.mediaSubordinados) {
				resultadoGeralDoResumaoDoGrupo.mediaSubordinados = resultadoGeralDoResumaoDoGrupo.mediaSubordinados / resultadoGeralDoResumaoDoGrupo.qtde_mediaSubordinados;
			}
			if (resultadoGeralDoResumaoDoGrupo.mediaOutros) {
				resultadoGeralDoResumaoDoGrupo.mediaOutros = resultadoGeralDoResumaoDoGrupo.mediaOutros / resultadoGeralDoResumaoDoGrupo.qtde_mediaOutros;
			}
			if (resultadoGeralDoResumaoDoGrupo.meta) {
				resultadoGeralDoResumaoDoGrupo.meta = resultadoGeralDoResumaoDoGrupo.meta / resultadoGeralDoResumaoDoGrupo.qtde_meta;
			}
			if (resultadoGeralDoResumaoDoGrupo.valorObtido) {
				resultadoGeralDoResumaoDoGrupo.valorObtido = resultadoGeralDoResumaoDoGrupo.valorObtido / resultadoGeralDoResumaoDoGrupo.qtde_valorObtido;
			}
			if (resultadoGeralDoResumaoDoGrupo.percentualMeta) {
				resultadoGeralDoResumaoDoGrupo.percentualMeta = resultadoGeralDoResumaoDoGrupo.percentualMeta / resultadoGeralDoResumaoDoGrupo.qtde_percentualMeta;
			}

			resultado.resumaoGrupoQuestoes.total = resultadoGeralDoResumaoDoGrupo;

		};

		$scope.prepararAvaliado = function (idAvaliado) {
			$scope.avaliado = FuncionarioService.get({
				id: idAvaliado,
				fieldsToFetch: ['pessoa', 'cargo cargo', 'setor', '[cargo].grupoOcupacional']
			}, function (avaliado) {
				if (!!$scope.resultado && !!$scope.resultado.avaliado) {
					if (avaliado.id !== $scope.resultado.avaliado.id) {
						MsgService.addFailed('ID Avaliado divergente, contatar Administrador');
					}
				}
			});
		};

		$scope.prepararMeta = function (idPesquisa, idAvaliado) {
			//find metas
			MetaService.findByPesquisaAndFuncionario({
				idPesquisa: idPesquisa,
				idAvaliado: idAvaliado
			}, function (metaObj) {
				$scope.meta = metaObj.meta;
			});
		};

		$scope.salvarMeta = function () {

			$('.loadingApp').show();

			var idPesquisa = $scope.resultados[0].pesquisa.id;

			if (!!$scope.meta && $scope.formPrincipal.$valid) {
				var metaObj = {};
				metaObj.meta = $scope.meta;
				metaObj.pesquisa = {
					id: idPesquisa
				};
				metaObj.funcionario = {
					id: $scope.avaliado.id
				};
				MetaService.salvar(metaObj, function () {
					MsgService.addSuccess('Meta salva com sucesso!');
					$('.loadingApp').hide();
				});
			}
		};

		$scope.hidePlanoMetas = function () {
			//resultados.length > 1
			//(!hasRoleAdmin && !isGestor) && !meta || (!avaliado || !avaliado.id)

			if ($scope.resultados && $scope.resultados.length > 1) {
				return true;
			}

			if (!$scope.hasRoleAdmin && !$scope.isGestor) {
				return true;
			}
			if (!$scope.avaliado || !$scope.avaliado.id) {
				return true;
			}

		};

		$scope.isNotaNa = function (nota) {
			if (nota) {
				return !nota.media && nota.mediaExibicao !== '0';
			}
			return true;
		};

		$scope.hideGrupo = function (questoesOrdem) {

			if (questoesOrdem && questoesOrdem.length > 0) {

				var result = questoesOrdem.map(function (questaoOrdem) {

					if (questaoOrdem.questao.classificacao <= 2) {

						var naPropria = $scope.isNotaNa(questaoOrdem.questao.notaPropria);
						var naLider = $scope.isNotaNa(questaoOrdem.questao.notaLideres);
						var naSubordinados = $scope.isNotaNa(questaoOrdem.questao.notaSubordinados);
						var naOutros = $scope.isNotaNa(questaoOrdem.questao.notaOutros);

						if (!naPropria || !naLider || !naSubordinados || !naOutros) {
							return questaoOrdem;
						}

					}

				});

				if (result.length === 0) {
					return true;
				}

				return false;

			}

			return true;
		};

		//EDITANDO
		if ($routeParams.idsPesquisa) {

			$('.loadingApp').show();

			if ($routeParams.idAvaliado) {

				ResultadoService.get({
					idsPesquisa: $routeParams.idsPesquisa,
					idAvaliado: $routeParams.idAvaliado
				}, function (resultadoGeral) {
					$scope.resultadoGeral = resultadoGeral;
					var opts = {
						idsPesquisa: $routeParams.idsPesquisa,
						idAvaliado: $routeParams.idAvaliado
					};
					$scope.prepararResultados($scope.resultadoGeral.resultadoPesquisas, opts);

				});

			} else if ($routeParams.idGrupoOcupacional) {

				$scope.grupoOcupacional = GrupoOcupacionalService.get({
					id: $routeParams.idGrupoOcupacional
				});

				ResultadoService.byGrupoOcupacional({
					idsPesquisa: $routeParams.idsPesquisa,
					idGrupoOcupacional: $routeParams.idGrupoOcupacional
				}, function (resultadoGeral) {
					$scope.resultadoGeral = resultadoGeral;
					var opts = {
						idsPesquisa: $routeParams.idsPesquisa,
						idGrupoOcupacional: $routeParams.idGrupoOcupacional
					};
					$scope.prepararResultados($scope.resultadoGeral.resultadoPesquisas, opts);

				});

			} else if ($routeParams.idCargo) {

				$scope.cargo = CargoService.get({
					id: $routeParams.idCargo
				});

				ResultadoService.byCargo({
					idsPesquisa: $routeParams.idsPesquisa,
					idCargo: $routeParams.idCargo
				}, function (resultadoGeral) {
					$scope.resultadoGeral = resultadoGeral;
					var opts = {
						idsPesquisa: $routeParams.idsPesquisa,
						idCargo: $routeParams.idCargo
					};
					$scope.prepararResultados($scope.resultadoGeral.resultadoPesquisas, opts);

				});

			} else if ($routeParams.idSetor) {

				$scope.setor = SetorService.get({
					id: $routeParams.idSetor
				});

				ResultadoService.bySetor({
					idsPesquisa: $routeParams.idsPesquisa,
					idSetor: $routeParams.idSetor
				}, function (resultadoGeral) {
					$scope.resultadoGeral = resultadoGeral;
					var opts = {
						idsPesquisa: $routeParams.idsPesquisa,
						idSetor: $routeParams.idSetor
					};
					$scope.prepararResultados($scope.resultadoGeral.resultadoPesquisas, opts);

				});

			} else if ($routeParams.idEmpresa) {

				$scope.empresa = EmpresaService.get({
					id: $routeParams.idEmpresa
				});

				ResultadoService.byEmpresa({
					idsPesquisa: $routeParams.idsPesquisa,
					idEmpresa: $routeParams.idEmpresa
				}, function (resultadoGeral) {
					$scope.resultadoGeral = resultadoGeral;
					var opts = {
						idsPesquisa: $routeParams.idsPesquisa,
						idEmpresa: $routeParams.idEmpresa
					};
					$scope.prepararResultados($scope.resultadoGeral.resultadoPesquisas, opts);

				});

			}



		} //routeParams

	});