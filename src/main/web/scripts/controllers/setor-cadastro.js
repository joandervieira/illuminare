'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:SetorCadastroCtrl
 * @description
 * # SetorCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('SetorCadastroCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, SetorService) {

		document.title = 'Cadastro Setor - Conecthare';
		$rootScope.activeMenu =  'funcionario';

		$scope.screenInit = function() {

			$scope.showMsgsError = false;
			$scope.setor = {};
			$scope.setor.ativo = true;

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();

		$scope.formIsInvalid = function() {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function() {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();

				if ($scope.setor.id != null) {

					$scope.atualizar();

				} else {

					$scope.salvar();

				}
			}

		};

		$scope.salvar = function() {

			SetorService.salvar($scope.setor, function(result) {

				MsgService.addSuccess('Setor salvo com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});


		};

		$scope.atualizar = function() {

			SetorService.atualizar($scope.setor, function(result) {

				MsgService.addSuccess('Setor atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/setor/pesquisa');

			});


		};

		$scope.cancelarSalvar = function() {
			$location.path('/funcionario/home');
		};



		//EDITANDO
		if ($routeParams.id) {
			$scope.setor = SetorService.get({
				id: $routeParams.id,
				fieldsToFetch: []
			});

		}

	});