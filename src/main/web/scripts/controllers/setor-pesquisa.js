'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:SetorPesquisaCtrl
 * @description
 * # SetorPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('SetorPesquisaCtrl', function($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, SetorService) {

		document.title = 'Pesquisa Setor - Conecthare';
		$rootScope.activeMenu = 'funcionario';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.setor = {};

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 10
		};


		$scope.limparFiltro = function() {
			$scope.setor = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function() {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();
				if ($rootScope.idEmpresaUser) {
					$scope.setor.idEmpresa = $rootScope.idEmpresaUser;
				}

				$scope.paginator.entity = $scope.setor;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = [];

				SetorService.pesquisar($scope.paginator, function(paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 10, // count per page
		}, {
			total: 0, // length of data
			counts: [10, 50, 100],
			getData: $scope.realizarPesquisar
		});


		$scope.excluir = function(entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function() {
						return 'Você deseja excluir este Setor ? <br> <strong>' + $scope.entityToExclude.nome + '</strong>';
					}
				}
			});

			modalInstance.result.then(function(confirmed) {
				if (confirmed) {

					SetorService.excluir({
						id: $scope.entityToExclude.id
					}, function(result) {
						MsgService.addSuccess('Setor excluído com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};

		$scope.verPesquisasSetor = function(entity) {
			var idEmpresa = entity.idEmpresa;
			if (!idEmpresa) {
				idEmpresa = $rootScope.idEmpresa;
			}
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/pesquisa/pesquisas-resultado.html',
				controller: 'PesquisaPesquisaModalCtrl',
				size: 'lg',
				windowClass: 'modalPesquisasResultado',
				resolve: {
					avaliadoResultado: function() {
						return {
							idEmpresa: idEmpresa
						};
					},
					idGrupoOcupacional: function() {
						return undefined;
					},
					idCargo: function() {
						return undefined;
					},
					idSetor: function() {
						return entity.id;
					},
					idEmpresa: function() {
						return null;
					}
				}

			});

			modalInstance.result.then(function(result) {
				// do nothing
			});

		};


	});