'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:UserCadastroCtrl
 * @description
 * # UserCadastroCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('UserCadastroCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, UserService, RoleService, FuncionarioService, EmpresaService) {

		document.title = 'Cadastro Usuário - Conecthare';
		$rootScope.activeMenu = 'admin';

		$scope.tiposUsuarios = [{
			label: 'Administrador',
			value: 'admin'
		}, {
			label: 'Funcionário',
			value: 'funcionario'
		}, {
			label: 'Gestor RH',
			value: 'gestor'
		}, {
			label: 'Gestor Setor',
			value: 'gestor_setor'
		}];

		$scope.screenInit = function () {

			$scope.showMsgsError = false;
			$scope.user = {};
			$scope.user.ativo = true;
			$scope.user.senhaAtualizada = false;
			$scope.user.tipoUser = $scope.tiposUsuarios[1];

			if ($scope.formPrincipal) {
				$scope.formPrincipal.$setPristine();
			}

		};

		MsgService.cleanAll(true);
		$scope.screenInit();



		$scope.formIsInvalid = function () {
			var isInvalid = $scope.formPrincipal.$invalid;
			$scope.showMsgsError = isInvalid;
			return isInvalid;
		};

		$scope.salvarAtualizar = function () {

			if (!$scope.formIsInvalid()) {
				$('.loadingApp').show();


				var user = angular.copy($scope.user);
				if (user.tipoUser) {
					user.tipoUser = user.tipoUser.value;
				}


				if (user.id != null) {

					$scope.atualizar(user);

				} else {

					$scope.salvar(user);

				}

			}

		};

		$scope.salvar = function (user) {

			UserService.salvar(user, function (result) {

				MsgService.addSuccess('Usuário salvo com sucesso');
				$scope.screenInit();
				$('.loadingApp').hide();
				$scope.focus('input.nome');

			});


		};

		$scope.atualizar = function (user) {

			UserService.atualizar(user, function (result) {

				MsgService.addSuccess('Usuário atualizado com sucesso', true);
				$('.loadingApp').hide();
				$location.path('/user/pesquisa');

			});


		};

		$scope.cancelarSalvar = function () {
			$location.path('/admin/home');
		};

		//$scope.onload = true;

		$scope.$watch('user.funcionario', function (newValue) {
			if (newValue && newValue.pessoa) {
				$scope.user.nomeExibir = newValue.pessoa.nome;

				if (!$scope.user.id) {
					$scope.user.name = newValue.pessoa.documento;
				}

				if (newValue.pessoa.enderecosEletronicos && newValue.pessoa.enderecosEletronicos.length > 0) {
					for (var i = 0; i < newValue.pessoa.enderecosEletronicos.length; i++) {
						var endEletronico = newValue.pessoa.enderecosEletronicos[i];
						if (endEletronico.tipoEnderecoEletronico) {
							if (endEletronico.tipoEnderecoEletronico.tipo === 'E-mail') {
								$scope.user.email = endEletronico.endereco;
								break;
							}
						}
					};
				}
			}
		});

		$scope.$watch('user.name', function (newValue) {
			if (newValue) {
				$scope.user.name = newValue.replace(/ /g, '').toLowerCase();
			}
		});

		/*RoleService.lista({}, function(result) {
			$scope.roles = result;
			$scope.paramsDataGridRoles.reload();
		});*/

		$scope.funcionarios = FuncionarioService.lista({
			fieldsToFetch: ['pessoa p', '[p].enderecosEletronicos'],
			orderBy: '-[p].nome'
		});

		//$scope.empresas = EmpresaService.lista();


		//EDITANDO
		if ($routeParams.id) {
			UserService.get({
				id: $routeParams.id,
				fieldsToFetch: ['funcionario f', '[f].pessoa']
			}, function (user) {

				$scope.user = user;

				if (user.roles) {
					for (var i = 0; i < user.roles.length; i++) {
						var roleUser = user.roles[i];
						if (roleUser.name === 'role_funcionario') {
							$scope.user.tipoUser = $scope.tiposUsuarios[1];
							break;
						} else if (roleUser.name === 'role_admin') {
							$scope.user.tipoUser = $scope.tiposUsuarios[0];

						} else if (roleUser.name === 'role_gestor') {
							$scope.user.tipoUser = $scope.tiposUsuarios[2];

						} else if (roleUser.name === 'role_gestor_setor') {
							$scope.user.tipoUser = $scope.tiposUsuarios[3];
						}
					}
				}

			});

		}

	});