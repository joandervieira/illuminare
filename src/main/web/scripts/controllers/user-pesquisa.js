'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:UserPesquisaCtrl
 * @description
 * # UserPesquisaCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
	.controller('UserPesquisaCtrl', function ($scope, $timeout, $modal, $routeParams, $location, $rootScope,
		MsgService, ngTableParams, GridUtilsService, UserService) {

		document.title = 'Pesquisa Usuário - Conecthare';
		$rootScope.activeMenu = 'admin';

		$scope.loadDataGrid = true;
		$scope.initialLoad = true;
		$scope.user = {};
		$scope.user.funcionario = {};
		$scope.user.funcionario.pessoa = {};

		MsgService.cleanAll();

		$scope.paginator = {
			pageNumber: 1,
			pageSize: 50
		};

		$scope.tiposUsuarios = [{
			label: 'Administrador',
			value: 'admin'
		}, {
			label: 'Funcionário',
			value: 'funcionario'
		}, {
			label: 'Gestor RH',
			value: 'gestor'
		}, {
			label: 'Gestor Setor',
			value: 'gestor_setor'
		}];


		$scope.limparFiltro = function () {
			$scope.user = {};
			$scope.user.funcionario = {};
			$scope.user.funcionario.pessoa = {};
			$scope.paramsDataGrid.reload();
		};

		$scope.pesquisar = function () {
			$scope.loadDataGrid = true;
			$scope.paramsDataGrid.reload();
		};

		$scope.realizarPesquisar = function ($defer, params) {
			params.hasNext = true;
			if ($scope.loadDataGrid) {
				$('.loadingApp').show();

				var user = angular.copy($scope.user);
				if (user.tipoUser) {
					user.tipoUser = user.tipoUser.value;
				}

				$scope.paginator.entity = user;
				$scope.paginator.pageNumber = params.page();
				$scope.paginator.pageSize = params.count();
				$scope.paginator.orderBy = params.orderBy()[0];
				$scope.paginator.fieldsToFetch = ['funcionario f', '[f].pessoa'];

				UserService.pesquisar($scope.paginator, function (paginator) {
					$scope.paginator = paginator;
					params.hasNext = paginator.hasNext;
					$scope.loadDataGrid = true;

					GridUtilsService.refreshData($defer, params, $scope.paginator, true);

					$('.loadingApp').hide();
					if (!$scope.initialLoad) {
						$rootScope.scrollTo('.gridContainer');
					}
					$scope.initialLoad = false;

				});

			}

		};

		$scope.paramsDataGrid = new ngTableParams({
			count: 50, // count per page
		}, {
				total: 0, // length of data
				counts: [10, 50, 100],
				getData: $scope.realizarPesquisar
			});


		$scope.excluir = function (entity) {
			$scope.entityToExclude = entity;
			var modalInstance = $modal.open({
				templateUrl: 'views/templates/modals/dialog-confirm-directive.html',
				controller: 'ConfirmDialogCtrl',
				resolve: {
					msg: function () {
						return 'Você deseja excluir este Usuário ? <br> <strong>' + $scope.entityToExclude.nomeExibir + '</strong>';
					}
				}
			});

			modalInstance.result.then(function (confirmed) {
				if (confirmed) {

					UserService.excluir({
						id: $scope.entityToExclude.id
					}, function (result) {
						MsgService.addSuccess('Usuário excluído com sucesso');
						$scope.initialLoad = true;
						$scope.pesquisar();

					});
				}
			});

		};


	});