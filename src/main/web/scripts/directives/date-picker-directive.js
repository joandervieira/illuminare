'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:datePickerDirective
 * @description
 * # datePickerDirective
 */
angular.module('frontendApp')
	.directive('datePickerDirective', function() {
		return {
			template: function() { //$element, $attrs
				return '' +
					' <div class="input-group">' +
					'    <input type="text" class="form-control" datepicker-popup="{{format}}" ng-model="dateValue" is-open="opened" min-date="minDate" max-date="maxDate"  ' +
					' 		datepicker-options="dateOptions" date-disabled="dateDisabled" close-text="X" />' +
					'    <span class="input-group-btn">' +
					'      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>' +
					'    </span>' +
					' </div>';
			},
			scope: {
				dateValue: '=ngModel',
				dateDisabled: '=dateDisabled',
				dpChange: '=dpChange',
				minDate: '=minDate',
				max: '=maxDate'
			},
			restrict: 'EA',
			link: function postLink(scope) { //element, attrs
				//element.text('this is the datePickerDirective directive');
				scope.today = function() {
					scope.dt = new Date();
				};
				scope.today();

				scope.clear = function() {
					scope.dt = null;
				};

				scope.open = function($event) {
					$event.preventDefault();
					$event.stopPropagation();

					scope.opened = true;
				};

				scope.dateOptions = {
					formatYear: 'yy',
					startingDay: 1
				};

				scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
				scope.format = scope.formats[0];

			}
		};
	});