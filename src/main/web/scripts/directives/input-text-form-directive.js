'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:inputTextFormDirective
 * @description
 * # inputTextFormDirective
 */
angular.module('frontendApp')
	.directive('inputTextFormDirective', function(EstadoService, $timeout) {
		return {
			//templateUrl: 'views/directives/input-text-form-directive.html',
			restrict: 'EA',
			scope: {
				form: '=form',
				fieldName: '=fieldName',
				label: '=label',
				type: '=type',
				holder: '=holder',
				model: '=model',
				othersAttrs: '=othersAttrs',
				class: '=class',
				required: '=required',
				req: '=req',
				minLength: '=minLength',
				maxLength: '=maxLength',
				autofocus: '=autofocus',
				cnpj: '=cnpj',
				cpf: '=cpf',
				ie: '=ie',
				cep: '=cep',
				disabled: '=disabled',
				fone: '=fone',
				mask: '=mask',
				numero: '=numero',
				min: '=?min',
				max: '=?max',
				date: '=date',
				autocompleteoff: '=autocompleteoff'

			},
			link: function postLink(scope, element, attrs, ngModel) {

				scope.dtaMinPadrao = moment().add(-100, 'y').toDate();
				scope.dtaMaxPadrao = moment().add(100, 'y').toDate();
				/*if (scope.date) {
					if (!scope.min) {
						scope.min = moment().add(-100, 'y').toDate();
					}
					if (!scope.max) {
						scope.max = moment().add(100, 'y').toDate();
					}
				}*/

				scope.noControlLabel = "nocontrollabel" in attrs;


				if (attrs.ie) {
					scope.ufs = [];
					//scope.selectedUF = '';
					EstadoService.lista({}, function(result) {
						//scope.ufs = result;
						//scope.selectedUF = scope.ufs[0];
						for (var i = 0; i < result.length; i++) {
							scope.ufs.push(result[i].sigla);
						}
						//scope.ie = scope.ie || 'MG';
					});
				}


				scope.$watch('model', function(newValue, oldValue) {
					if (attrs.cnpj || attrs.ie) {
						scope.modelDirective = newValue && newValue.toString() || newValue;
					} else {
						/*if (newValue && newValue === '') {
							scope.modelDirective = undefined;
						} else {
							scope.modelDirective = newValue;
						}*/
						scope.modelDirective = newValue;
					}

					/*if (newValue !== oldValue) {
						if (newValue) {
							var field = scope.form[attrs.fieldName];
							if (field && field.$invalid === true && scope.form.$invalid === true && field.$error.required === true) {

							}
						}
					}*/

				});

				scope.firstValidationDate = true;
				scope.$watch('modelDirective', function(newValue, oldValue) {

					//if (newValue !== oldValue) {
					if (scope.date && //
						((scope.form[scope.fieldName].$viewValue && //
							scope.form[scope.fieldName].$viewValue.length === 10) || scope.firstValidationDate)) {

						scope.model = scope.validarData();
						scope.firstValidationDate = false;

					} else {
						scope.model = newValue;
					}
					//}

				});

				scope.validarData = function(value) {
					if (scope.date) {

						var inputValue = undefined;
						if (value) {
							inputValue = value;
						} else {
							inputValue = scope.modelDirective;
						}

						var dtaMin = scope.min ? scope.min : scope.dtaMinPadrao;
						var dtaMax = scope.max ? scope.max : scope.dtaMaxPadrao;


						if (dtaMin && inputValue) {
							var min = moment(dtaMin).startOf('day');
							var input = moment(inputValue).startOf('day');
							var diff = input.diff(min, 'days');
							if (diff < 0) {
								//console.log('min invalid');
								scope.modelDirective = undefined;
								//scope.setInvalidationDate(true);
								return undefined;
							}
						}
						if (dtaMax && inputValue) {
							var max = moment(dtaMax).startOf('day');
							var input = moment(inputValue).startOf('day');
							var diff = input.diff(max, 'days');
							if (diff > 0) {
								//console.log('max invalid');
								//scope.setInvalidationDate(true);
								scope.modelDirective = undefined;
								return undefined;
							}
						}
						return inputValue;
					}
				};

				scope.setInvalidationDate = function(isInvalid) {
					scope.form[scope.fieldName].$error.required = isInvalid;
					scope.form[scope.fieldName].$invalid = isInvalid;
					scope.form[scope.fieldName].$valid = !isInvalid;
				};

				var fieldName;
				if (attrs.fieldName) {
					fieldName = attrs.fieldName.replace(/'/g, '');
				}
				if (attrs.mask) {
					$timeout(function() {
						$('.' + fieldName + ' input').mask('' + attrs.mask);
					}, 100);
				}
				if (attrs.date) {
					$timeout(function() {
						$('.' + fieldName + ' input').mask('99/99/9999');
					}, 100);
				}



				/*** DATEPICKER ***/
				scope.today = function() {
					scope.dt = new Date();
				};
				scope.today();

				scope.clear = function() {
					scope.dt = null;
				};



				scope.open = function($event) {
					$event.preventDefault();
					$event.stopPropagation();

					scope.opened = true;
				};

				scope.dateOptions = {
					formatYear: 'yyyy',
					startingDay: 1
				};

				scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
				scope.format = scope.formats[0];
				/*** FIM DATEPICKER ***/



			},
			template: function($element, $attrs) {
				var html = '';
				var htmlDatePicker = undefined;
				var fieldName;
				if ($attrs.fieldName) {
					fieldName = $attrs.fieldName.replace(/'/g, '');
				}

				if (!$attrs.maxLength && !$attrs.date) {
					$attrs.maxLength = 254;
				}

				//html += '<ng-form name="innerfForm">';

				if ($attrs.ie) {
					html += '<div class="row">';
				}

				if ($attrs.ie) {
					html += '<div class="form-group col-xs-2" >';
					html += '	<label>Estado I.E</label>';
					html += '	<select class="form-control" ng-model="ie" ng-options="uf for uf in ufs"></select> ';
					html += '</div>';
				}


				html += '<div class="' + fieldName + ' form-group ' + ($attrs.ie ? 'col-xs-10' : '') + ' has-feedback {{form[\'' + fieldName + '\'].$invalid && form[\'' + fieldName + '\'].$dirty ? \'has-error\' : form[\'' + fieldName + '\'].$pristine ? \'\' : \'has-success\'}}">';

				//html += '	<label class="control-label" for="{{fieldName}}">' + ($attrs.required ? '<span class="campoObrigatorio">* </span>' : '') + '<span>{{label}}</span></label>';
				html += '	<label class="control-label" for="{{fieldName}}"> <span class="campoObrigatorio" ng-show="req && !noControlLabel">* </span> <span>{{label}}</span> </label>';


				if ($attrs.date) {
					htmlDatePicker += ' datepicker-popup="{{format}}" is-open="opened" ';
					//htmlDatePicker += ' 		datepicker-options="dateOptions" close-text="X"  ';
					htmlDatePicker += ' close-text="X"  ';
					htmlDatePicker += ' starting-day="1"  ';
					htmlDatePicker += ' min-date="min" max-date="max" ';
					htmlDatePicker += ' ng-blur="validarData()" ';

					// html += 'modelDirective:<span style="border: 1px solid red">{{modelDirective | json}}</span><br>';
					// html += 'model:<span style="border: 1px solid red">{{model | json}}</span><br>';
					// html += 'viewValue:<span style="border: 1px solid red">{{form[\''+fieldName+'\'].$viewValue | json}}</span><br>';
					// html += 'modelValue:<span style="border: 1px solid red">{{form[\''+fieldName+'\'].$modelValue | json}}</span><br>';
					// html += 'error:<span style="border: 1px solid red">{{form[\''+fieldName+'\'].$error | json}}</span><br>';
				}

				if (htmlDatePicker) {
					html += ' <div class="input-group">';
				}



				html += '	<input type="' + ($attrs.type ? $attrs.type : 'text') + '" class="{{class}} form-control ' + fieldName + ' "  ';
				html += '		placeholder="{{holder}}"  ng-model="modelDirective" name="' + fieldName + '"  ';
				//html += ' ' + ($attrs.required ? 'required' : '');
				//html += ' {{req ? (\'required\') : (\'\')}} ';
				html += ' ng-required="req" ';
				html += ' ' + ($attrs.autofocus ? 'autofocus' : '');
				html += ' ' + ($attrs.autocompleteoff ? 'autocomplete="off"' : '');
				html += ' ' + ($attrs.minLength ? 'ng-minlength="' + $attrs.minLength + '" minlength="' + $attrs.minLength + '" ' : '');
				html += ' ' + ($attrs.maxLength ? 'ng-maxlength="' + $attrs.maxLength + '" maxlength="' + $attrs.maxLength + '" ' : '');
				html += ' ' + ($attrs.cnpj ? 'ui-br-cnpj-mask' : '');
				html += ' ' + ($attrs.cpf ? 'ui-br-cpf-mask' : '');
				html += ' ' + ($attrs.ie ? 'ui-br-ie-mask="ie"' : '');
				html += ' ' + ($attrs.cep ? 'ui-br-cep-mask' : '');
				html += ' ' + ($attrs.fone ? 'ui-br-phone-number' : '');
				html += ' ' + ($attrs.numero ? 'ui-number-mask="numero" min="min" max="max" ' : '');
				html += ' ' + (htmlDatePicker ? htmlDatePicker : '');
				//html += ' ' + ($attrs.mask ? 'ui-mask="' + $attrs.mask + '" ' : '');
				html += '	ng-disabled="disabled" ';
				html += ' >';

				if (htmlDatePicker) {
					html += '    <span class="input-group-btn">';
					html += '      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>';
					html += '    </span>';
					html += ' </div>'; //input-group
				}

				html += '	<span class="glyphicon glyphicon-{{form[\'' + fieldName + '\'].$invalid && form[\'' + fieldName + '\'].$dirty ? \'remove\' : form[\'' + fieldName + '\'].$pristine ? \'\' : \'ok\'}} form-control-feedback" ';
				html += ' 		style=" ' + (htmlDatePicker ? 'margin-right: 38px' : '') + ' "  ></span>';

				html += '	' + ($attrs.helper ? ' <p class="help-block">' + $attrs.helper + '</p>' : '') + ' ';
				html += '	';

				html += '</div>';



				if ($attrs.ie) {
					html += '</div>'; //row
				}

				//html += '</ng-form>';

				return html;
			}

		};
	});


/**
PLUGINS UTILIZADOS: 
https://github.com/assisrafael/angular-input-masks



**/