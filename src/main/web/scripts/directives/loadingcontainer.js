'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:loadingContainer
 * @description
 * # loadingContainer
 */
angular.module('frontendApp')
	.directive('loadingContainer', function() {
		return {
			/*template: '<div></div>',*/
			restrict: 'EA',
			scope: false,
			link: function postLink(scope, element, attrs) {
				var loadingLayer = angular.element('<div class="loading"></div>');
				element.append(loadingLayer);
				element.addClass('loading-container');
				scope.$watch(attrs.loadingContainer, function(value) {
					loadingLayer.toggleClass('ng-hide', !value);
				});
			}
		};
	});