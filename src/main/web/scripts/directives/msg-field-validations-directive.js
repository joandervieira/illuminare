'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:MsgFieldValidationsDirective
 * @description
 * # MsgFieldValidationsDirective
 */
angular.module('frontendApp')
	.directive('msgFieldValidationsDirective', function() {
		return {
			restrict: 'EA',
			templateUrl: 'views/directives/msg-field-validations-directive.html',
			scope: {
				form: '=form',
				fieldName: '=fieldName',
				msg: '=msg',
				validateType: '=validateType',
				mostrarSempre: '=mostrarSempre',
				show: '=?show'

			},
			//link: function postLink(scope, element, attrs) {
			link: function postLink(scope) {
				if (scope.form[scope.fieldName] && scope.form[scope.fieldName].$modelValue) {
					scope.form[scope.fieldName].$dirty = true;
					scope.form[scope.fieldName].$pristine = true;
				}

				scope.showMsgResult;
				scope.showMsg = function() {
					if (scope.mostrarSempre) {
						scope.showMsgResult = true;
						return true;
					}
					if (scope.form) {
						if (scope.form[scope.fieldName]) {
							var invalid = scope.form[scope.fieldName].$invalid;
							if (invalid && scope.form[scope.fieldName].$dirty) {
								scope.showMsgResult = true;
								return true;
							}
							if (invalid && scope.show) {
								scope.showMsgResult = true;
								return true;
							}
						}
					}

					scope.showMsgResult = false;
					return false;
				};
			}

		};
	});