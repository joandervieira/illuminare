'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:onenterDirective
 * @description
 * # onenterDirective
 */
angular.module('frontendApp')
	.directive('onenterDirective', function() {
		return {
			restrict: 'A',
			transclude: true,
			link: function postLink(scope, element, attrs) {
				element.bind("keydown keypress", function(event) {
					if (event.which === 13) {
						scope.$apply(function() {
							scope.$eval(attrs.onenterDirective);
						});
						event.preventDefault();
					}
				});
			}

		};
	});