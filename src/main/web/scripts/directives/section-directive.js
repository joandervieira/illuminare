'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:sectionDirective
 * @description
 * # sectionDirective
 */
angular.module('frontendApp')
	.directive('sectionDirective', function() {
		return {
			restrict: 'EA',
			templateUrl: 'views/directives/section.html',
			scope: {
				sectionName: '@sectionDirective',
				collapsed: '=collapsed',
			},
			transclude: true,
			link: function postLink(scope, element, attrs) {
				if (scope.collapsed === true || scope.collapsed === false) {
					scope.isCollapsed = scope.collapsed;
				}
			}

		};
	});