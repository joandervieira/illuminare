'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:selectDirective
 * @description
 * # selectDirective
 */
angular.module('frontendApp')
	.directive('selectDirective', function($timeout, $window) {
		return {
			//templateUrl: 'views/directives/input-text-form-directive.html',
			restrict: 'EA',
			scope: {
				model: '=model',
				placeholder: '=?',
				options: '=options',
				label: '=label',
				customClass: '=customClass',
				class: '=class',
				form: '=form',
				fieldName: '=fieldName',
				required: '=required',
				req: '=req',
				autofocus: '=autofocus',
				optionLabel: '=optionLabel',
				optionValue: '=optionValue',
				async: '=async',
				disabled: '=disabled',
				funcOnSelect: '=funcOnSelect'
			},
			link: function postLink(scope, element, attrs) {
				scope.modelDirective = {};
				scope.placeholder = scope.placeholder || '';
				//scope.options = scope.options || [];
				////console.log(scope.model);

				scope.fillMapOptions = function() {
					if (!!scope.optionValue) {
						scope.mapOptions = [];
						for (var i = 0; i < scope.options.length; i++) {
							//scope.mapOptions.push(scope.options[i][scope.optionValue], scope.options[i]);
							scope.mapOptions[scope.options[i][scope.optionValue]] = scope.options[i];
						}
						if (scope.model) {
							$timeout(function() {
								scope.modelDirective.selected = scope.mapOptions[scope.model];
							});
						}
					}
				};

				//scope.fillMapOptions();

				scope.$watch('options', function(options) {
					if (options && options.length > 0) {

						scope.fillMapOptions();

					} else {
						if (options && options.$promise) {
							options.$promise.then(function() {
								scope.fillMapOptions();
							});
						}
					}
				});

				scope.clear = function(event) {
					scope.modelDirective.selected = undefined;
					scope.model = undefined;
					scope.form[attrs.fieldName].$dirty = true;
					scope.form[attrs.fieldName].$pristine = false;
				};

				scope.onSelect = function(item, model) {
					if (!!scope.optionValue) {
						scope.model = item[scope.optionValue];
					} else {
						scope.model = item;
					}
					if (scope.funcOnSelect) {
						scope.funcOnSelect(scope.model);
					}
				};

				scope.$watch('model', function(newValue, oldValue) {
					if (newValue) {
						if (!!scope.optionValue) {
							if (scope.mapOptions && scope.mapOptions.length >= 0) {
								$timeout(function() {
									scope.modelDirective.selected = scope.mapOptions[newValue];
								});
							}
						} else {
							scope.modelDirective.selected = newValue;
						}
					} else {
						scope.modelDirective.selected = undefined;
					}
				});

				scope.teste = function(item, model) {
					//console.log("teste");
				};

				if (attrs.autofocus) {
					$timeout(function() {
						$('.' + attrs.fieldName + ' .ui-select-match').focus();
					}, 300);
				}


				// var w = angular.element($window);

				// w.bind('resize', function() {
				// 	scope.width = (($('.selectDirectiveContainer').width()) - 40);
				// 	scope.$apply();
				// });


				if (scope.options === 'trueFalse') {


					scope.options = [true, false];
				}

			},
			template: function($element, $attrs) {
				var html = '';
				var isValid = true;

				var fieldName;
				if ($attrs.fieldName) {
					fieldName = $attrs.fieldName.replace(/'/g, '');
				}

				if (!fieldName) {
					html += '<div><strong style="background-color: red">Atributte "fieldName" is required for this directive: "selectDirective" </strong></div>';
					isValid = false;
				}



				if (isValid) {
					html += '<div class="{{class}} ' + fieldName + ' selectDirectiveContainer form-group has-feedback {{form[\'' + fieldName + '\'].$invalid && form[\'' + fieldName + '\'].$dirty ? \'has-error\' : form[\'' + fieldName + '\'].$pristine ? \'\' : \'has-success\'}}">';
					//html += '		<label class="control-label" for="{{fieldName}}">' + ($attrs.required ? '<span class="campoObrigatorio">* </span>' : '') + '<span>{{label}}</span></label>';
					if ($attrs.label) {
						html += '		<label class="control-label" for="{{fieldName}}"><span class="campoObrigatorio" ng-show="!!req">* </span><span>{{label}}</span></label>';
					}
					html += '	<div class="input-group">';
					html += '		<ui-select name="' + fieldName + '" ng-model="modelDirective.selected" theme="bootstrap" reset-search-input="true"  ';
					html += '				on-select="onSelect($item, $model)"    ';
					//html += ' 				' + ($attrs.required ? 'required' : '');
					html += ' 				ng-required="!!req" ';
					html += ' 				' + ($attrs.autofocus ? 'autofocus' : '');
					html += ' 				ng-disabled="disabled || (undefined === async ? (undefined === options || options.length <= 0)  : false) " ';
					html += ' 				class="{{disabled ? \'disabled\' : \'\'}}" ';
					html += ' 		>';
					html += '			<ui-select-match  placeholder="{{placeholder}}">{{$select.selected.' + $attrs.optionLabel + '}}</ui-select-match>';
					html += '		      <ui-select-choices ' + ($attrs.async ? ' refresh="async($select.search)" refresh-delay="300" ' : '') + ' repeat="item in options | filter: $select.search" >';
					html += '		        <div ng-bind-html=" ' + ($attrs.optionLabel ? 'item.' + $attrs.optionLabel : 'item') + ' | highlight: $select.search "></div>';
					html += '		      </ui-select-choices>';
					html += '		</ui-select>';
					html += '        <span class="input-group-btn">';
					html += '           <button ';
					html += '             ng-disabled="disabled || (undefined === async ? (undefined === options || options.length <= 0)  : false) "  ';
					html += '           	type="button" ng-click="clear($event)"  ';
					html += '           	class="btn btn-default btnClearSelect {{disabled ? \'disabled\' : \'\'}}" popover-trigger="mouseenter" popover-append-to-body="true" popover="Limpar Campo" >';
					html += '             <span class="glyphicon icon-broom"></span>';
					html += '           </button>';
					html += '        </span>';
					html += '	</div>';
					html += '	' + ($attrs.helper ? ' <p class="help-block">' + $attrs.helper + '</p>' : '') + ' ';
					html += '</div>';
				}

				return html;
			}

		};
	});