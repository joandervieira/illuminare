'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:textareaFormDirective
 * @description
 * # textareaFormDirective
 */
angular.module('frontendApp')
	.directive('textareaFormDirective', function($timeout) {
		return {
			restrict: 'EA',
			scope: {
				form: '=form',
				fieldName: '=fieldName',
				label: '=label',
				rows: '=rows',
				model: '=model',
				class: '=class',
				required: '=required',
				req: '=req',
				minLength: '=minLength',
				maxLength: '=maxLength',
				disabled: '=disabled'
			},
			link: function postLink(scope, element, attrs, ngModel) {

				if (!scope.maxLength) {
					scope.maxLengthDirective = 254;
				} else {
					scope.maxLengthDirective = scope.maxLength;
				}

				if (!scope.minLength) {
					scope.minLengthDirective = 0;
				} else {
					scope.minLengthDirective = scope.minLength;
				}

			},
			template: function($element, $attrs) {
				var html = '';

				if (!$attrs.maxLength) {
					$attrs.maxLength = 254;
				}

				var fieldName;
				if ($attrs.fieldName) {
					fieldName = $attrs.fieldName.replace(/'/g, '');
				}

				html += '<div class="' + fieldName + ' form-group has-feedback {{form[\'' + fieldName + '\'].$invalid && form[\'' + fieldName + '\'].$dirty ? \'has-error\' : form[\'' + fieldName + '\'].$pristine ? \'\' : \'has-success\'}}">';
				//html += 'scope:{{form | json}}<br>';
				if ($attrs.label) {
					html += '	<label class="control-label" for="{{fieldName}}"> <span class="campoObrigatorio" ng-show="!!req">* </span> <span>{{label}}</span> </label>';
				}

				html += '';
				html += '';
				html += '	' + ($attrs.helper ? ' <p class="help-block">' + $attrs.helper + '</p>' : '') + ' ';
				html += '<textarea ';
				html += ' name="' + fieldName + '" ';
				html += ' class="{{fieldName}} {{class}} form-control " ';
				html += ' ng-model="model" ';
				html += ' rows="{{rows}}" ';
				html += ' ng-maxlength="maxLengthDirective" maxlength="{{maxLengthDirective}}" ';
				html += ' ng-minlength="minLengthDirective" minlength="{{minLengthDirective}}" ';
				html += ' placeholder="{{placeholder}}" ';
				html += ' ng-required="!!req" ';
				html += '	ng-disabled="disabled" ';
				html += ' ' + ($attrs.autofocus ? 'autofocus' : '');
				html += ' ></textarea>';

				html += '	<span class="glyphicon glyphicon-{{form[\'' + fieldName + '\'].$invalid && form[\'' + fieldName + '\'].$dirty ? \'remove\' : form[\'' + fieldName + '\'].$pristine ? \'\' : \'ok\'}} form-control-feedback" ';
				html += ' 		style=""  ></span>';

				//html += '	' + ($attrs.helper ? ' <p class="help-block">' + $attrs.helper + '</p>' : '') + ' ';
				html += '	';

				html += '</div>';


				return html;
			}

		};
	});