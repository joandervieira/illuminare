'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:typeaheadDirective
 * @description
 * # typeaheadDirective
 */
angular.module('frontendApp')
	.directive('typeaheadDirective', function() {
		return {
			//templateUrl: 'views/directives/input-text-form-directive.html',
			restrict: 'EA',
			scope: {
				form: '=form',
				fieldName: '=fieldName',
				label: '=label',
				placeholder: '=placeholder',
				class: '=class',
				//model: '=model',
				required: '=required',
				minLength: '=minLength',
				maxLength: '=maxLength',
				autofocus: '=autofocus',
				disabled: '=disabled',
				typeaheadUrl: '=typeaheadUrl',
				functionAsync: '=functionAsync',
				typeaheadLabel: '=typeaheadLabel',
				typeaheadModel: '=typeaheadModel'

			},
			link: function postLink(scope, element, attrs, ngModel) {

				scope.$watch('typeaheadModel', function(newValue) {
					if (newValue) {
						scope.model = newValue[scope.typeaheadLabel];
					}
				});

				scope.onSelect = function($item, $model, $label) {
					scope.typeaheadModel = $item;
				};

				var fieldName = attrs.fieldName;
				scope.onBlur = function() {
					/*if (!scope.model) {
						$('.' + fieldName).val('');
					}*/
				};

			},
			template: function($element, $attrs) {
				var html = '';


				html += '<div class="typeahead  ' + $attrs.fieldName + ' form-group has-feedback {{form[\'' + $attrs.fieldName + '\'].$invalid && form[\'' + $attrs.fieldName + '\'].$dirty ? \'has-error\' : form[\'' + $attrs.fieldName + '\'].$pristine ? \'\' : \'has-success\'}}">';

				html += '	<label class="control-label" for="{{fieldName}}">' + ($attrs.required ? '<span class="campoObrigatorio">* </span>' : '') + '<span>{{label}}</span></label>';

				//html += '	<pre>ModelDirective: {{model | json}}</pre> ';

				html += '	<input type="text" class="{{class}} form-control ' + $attrs.fieldName + ' " id="' + $attrs.fieldName + '" ';
				html += '		placeholder="{{placeholder || \'\'}}"  ng-model="model" name="' + $attrs.fieldName + '"  ';
				html += ' ' + ($attrs.required ? 'required' : '');
				html += ' ' + ($attrs.autofocus ? 'autofocus' : '');
				html += ' ' + ($attrs.minLength ? 'ng-minlength="' + $attrs.minLength + '" minlength="' + $attrs.minLength + '" ' : '');
				html += ' ' + ($attrs.maxLength ? 'ng-maxlength="' + $attrs.maxLength + '" maxlength="' + $attrs.maxLength + '" ' : '');

				//html += ' ' + ($attrs.mask ? 'ui-mask="' + $attrs.mask + '" ' : '');
				html += '	ng-disabled="disabled" ';
				html += '	typeahead-loading="typeAheadLoading' + $attrs.fieldName + '"  ';
				html += '	typeahead-editable="false"  ';
				html += '	typeahead-min-length="minLength"  ';
				html += '	typeahead-on-select="onSelect($item, $model, $label)" ';
				html += '	typeahead="model[' + $attrs.typeaheadLabel + '] as model[' + $attrs.typeaheadLabel + '] for model in functionAsync($viewValue)" ';
				html += '	typeahead-template-url="' + $attrs.typeaheadUrl + '" ';
				html += ' ng-blur="onBlur()" ';

				html += ' >';


				html += '	<span class="glyphicon glyphicon-{{form[\'' + $attrs.fieldName + '\'].$invalid && form[\'' + $attrs.fieldName + '\'].$dirty ? \'remove\' : form[\'' + $attrs.fieldName + '\'].$pristine ? \'\' : \'ok\'}} form-control-feedback" ';
				html += ' 		style="top: 16px;"  ></span>';

				html += '	' + ($attrs.helper ? ' <p class="help-block">' + $attrs.helper + '</p>' : '') + ' ';
				html += '	';
				html += '<i ng-show="typeAheadLoading' + $attrs.fieldName + '" class="glyphicon glyphicon-refresh"></i>	';

				html += '</div>';



				return html;
			}

		};
	});