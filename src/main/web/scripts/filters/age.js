'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:age
 * @function
 * @description
 * # age
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('age', function($filter) {

		return function(input, withSuffix) {
			////console.log(withSuffix);
			/*if (input) {
				if (input instanceof Date) {
					return getAge(input, withSuffix);
				} else {
					return getAge(moment(input, 'YYYY-MM-DD').toDate(), withSuffix);
				}
			} else {
				return '';
			}*/
			return getAge2(input, withSuffix);
		};

	});

function getAge2(date, withSuffix) {
	if (date) {
		if (withSuffix) {
			withSuffix = false;
		} else {
			withSuffix = true;
		}
		if (date instanceof Date) {
			return moment(date).fromNow(withSuffix);
		} else {
			return moment(date, 'YYYY-MM-DD').fromNow(withSuffix);
		}
	} else {
		return '';
	}
}


function getAge(date, withSuffix) {
	if (date) {
		var now = new Date();
		var today = new Date(now.getYear(), now.getMonth(), now.getDate());

		var yearNow = now.getYear();
		var monthNow = now.getMonth();
		var dateNow = now.getDate();

		var dob = date;

		var yearDob = dob.getYear();
		var monthDob = dob.getMonth();
		var dateDob = dob.getDate();
		var age = {};
		var ageString = "";
		var yearString = "";
		var monthString = "";
		var dayString = "";


		var yearAge = yearNow - yearDob;

		if (monthNow >= monthDob)
			var monthAge = monthNow - monthDob;
		else {
			yearAge--;
			var monthAge = 12 + monthNow - monthDob;
		}

		if (dateNow >= dateDob)
			var dateAge = dateNow - dateDob;
		else {
			monthAge--;
			var dateAge = 31 + dateNow - dateDob;

			if (monthAge < 0) {
				monthAge = 11;
				yearAge--;
			}
		}

		age = {
			years: yearAge,
			months: monthAge,
			days: dateAge
		};

		if (age.years > 1) {
			yearString = " anos";
		} else {
			yearString = " ano";
		}

		if (age.months > 1) {
			monthString = " meses";
		} else {
			monthString = " mês";
		}

		if (age.days > 1) {
			dayString = " dias";
		} else {
			dayString = " dia";
		}

		if ((age.years > 0) && (age.months > 0) && (age.days > 0)) {
			ageString = age.years + yearString + ", " + age.months + monthString + " e " + age.days + dayString + " "; //atrás.
			if (withSuffix) {
				ageString += 'atrás.';
			}
		} else if ((age.years == 0) && (age.months == 0) && (age.days > 0)) {
			ageString = " " + age.days + dayString + " "; //Apenas [DATA] atrás!
			if (withSuffix) {
				ageString += 'atrás.';
			}
		} else if ((age.years > 0) && (age.months == 0) && (age.days == 0)) {
			ageString = age.years + yearString + " "; //atrás. Feliz Aniversário!!
			if (withSuffix) {
				ageString += 'atrás.';
			}
		} else if ((age.years > 0) && (age.months > 0) && (age.days == 0)) {
			ageString = age.years + yearString + " e " + age.months + monthString + " "; //atrás.
			if (withSuffix) {
				ageString += 'atrás.';
			}
		} else if ((age.years == 0) && (age.months > 0) && (age.days > 0)) {
			ageString = age.months + monthString + " e " + age.days + dayString + " "; //atrás.
			if (withSuffix) {
				ageString += 'atrás.';
			}
		} else if ((age.years > 0) && (age.months == 0) && (age.days > 0)) {
			ageString = age.years + yearString + " e " + age.days + dayString + " "; //atrás.
			if (withSuffix) {
				ageString += 'atrás.';
			}
		} else if ((age.years == 0) && (age.months > 0) && (age.days == 0)) {
			ageString = age.months + monthString + " "; //" e.";
			if (withSuffix) {
				ageString += '';
			}
		} else if ((yearDob === yearNow) && (monthDob === monthNow) && (dateDob === dateNow)) {
			if (withSuffix) {
				ageString += 'hoje.';
			}
		} else {
			ageString = "" //"Oops! Could not calculate age!";
		}

		return ageString;
	} else {
		return '';
	}
}