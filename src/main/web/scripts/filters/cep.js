'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:cep
 * @function
 * @description
 * # cep
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('cep', function() {

		return function(text, length, end) {
			var cFormated = text;
			if (text && text.length == 8) {
				cFormated = text.substring(0, 5) + '-' + text.substring(5, 9);
			}
			return cFormated;
		}


	});