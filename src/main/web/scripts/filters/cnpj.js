'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:cep
 * @function
 * @description
 * # cep
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('cnpj', function() {

		return function(text) {
			if (text && text.length === 14) {
				text = text.replace(/\D/g, '').replace(/^(\d{2})(\d{3})?(\d{3})?(\d{4})?(\d{2})?/, "$1.$2.$3/$4-$5");
				return text;
			}
			return text;
		}


	});