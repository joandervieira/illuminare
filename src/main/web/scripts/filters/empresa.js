'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:cep
 * @function
 * @description
 * # cep
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('empresa', function() {

		return function(empresa) {
			if (empresa) {
				return empresa.nome;
			}
			return '-';
		}


	});