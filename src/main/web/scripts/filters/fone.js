'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:fone
 * @function
 * @description
 * # fone
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('fone', function() {

		return function(input) {
			if (input) {
				input = input.toString();
				var ddd, number, foneFormatado;
				input = input.replace('(', '').replace(')', '').replace('-', '').replace(' ', '')
				switch (input.length) {
					case 11:
						ddd = '(' + input.slice(0, 2) + ') ';
						number = (input.slice(2, 7) + '-' + input.slice(7, 11));
						foneFormatado = ddd + number;
						break;
					case 10:
						ddd = '(' + input.slice(0, 2) + ') ';
						number = (input.slice(2, 6) + '-' + input.slice(6, 10));
						foneFormatado = ddd + number;
						break;
					case 9:
						number = (input.slice(0, 5) + '-' + input.slice(5, 9));
						foneFormatado = number;
						break;
					case 8:
						number = (input.slice(0, 4) + '-' + input.slice(5, 8));
						foneFormatado = number;
						break;

					default:
						foneFormatado = input;
						break;
				}

				return foneFormatado;

			}
		};


	});