'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:viewdata
 * @function
 * @description
 * # viewdata
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('numberRound', function ($filter) {
		return function (input, places) {
			if (input) {
				if (isNaN(input)) {
					return input;
				} else {
					return input.toFixed(1);
				}
			}
		};
	});