'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:cep
 * @function
 * @description
 * # cep
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('tipouser', function() {

		return function(user) {
			if (user) {
				if (user.roles) {
					for (var i = 0; i < user.roles.length; i++) {
						var roleUser = user.roles[i];
						if (roleUser.name === 'role_funcionario') {
							return 'Funcionário';
							break;
						} else if (roleUser.name === 'role_admin') {
							return 'Administrador';

						} else if (roleUser.name === 'role_gestor') {
							return 'Gestor RH';
						
						} else if (roleUser.name === 'role_gestor_setor') {
							return 'Gestor Setor';
						}
					}
				}
			}
			return '-';
		}


	});