'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:viewdata
 * @function
 * @description
 * # viewdata
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('viewdata', function($filter) {
		return function(input, format) {
			if (input) {
				//DD/MM/YY HH:mm
				format = format ? format : 'DD/MM/YY';
				if (input instanceof Date) {
					return moment(input).format(format);
				} else {
					return moment(input, 'YYYY-MM-DD').format(format);
				}
			} else {
				return '';
			}
		};
	});