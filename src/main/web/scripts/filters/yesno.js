'use strict';

/**
 * @ngdoc filter
 * @name frontendApp.filter:yesNo
 * @function
 * @description
 * # yesNo
 * Filter in the frontendApp.
 */
angular.module('frontendApp')
	.filter('yesno', function() {

		return function(text, length, end) {
			if (text) {
				return 'Sim';
			}
			return 'Não';
		}


	});