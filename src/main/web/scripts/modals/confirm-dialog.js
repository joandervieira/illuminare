'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ConfirmDialogCtrl
 * @description
 * # ConfirmDialogCtrl
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
	.controller('ConfirmDialogCtrl', function($scope, $modalInstance, msg) {

		$scope.msg = msg;

		$scope.ok = function() {
			$modalInstance.close(true);
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};

		/*$timeout(function() {
			$('.modal-dialog').focus();
			$(".modal-dialog").on("keypress", function(e) {
				var code = e.keyCode;
				if (code == 13) { //Enter key
					//$('.btnOkModal', '.modal-dialog').focus();
					$modalInstance.close(true);
					e.preventDefault();
				}
			});
		}, 200);*/


	});