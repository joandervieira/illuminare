'use strict';

/**
 * @ngdoc service
 * @name frontendApp.PesquisaSelecionarQuestionarioDialogCtrl
 * @description
 * # PesquisaSelecionarQuestionarioDialogCtrl
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
	.controller('PesquisaSelecionarQuestionarioDialogCtrl', function($scope, $modalInstance, $timeout, $q,
		GridUtilsService, ngTableParams, questionariosDisponiveis, questionariosSelecionados, gruposOcupacionais) {

		$scope.onLoadQuestionariosDisponiveis = true;
		$scope.onLoadQuestionariosSelecionadas = true;


		$scope.questionariosDisponiveis = questionariosDisponiveis;
		$scope.questionariosSelecionados = questionariosSelecionados;
		$scope.gruposOcupacionais = gruposOcupacionais;
		$scope.filtroQuestionariosDisponiveis = {};
		$scope.filtroQuestionariosDisponiveis.gruposOcupacionais = {};

		/*$scope.gruposOcupacionaisCombo = function() {
			var def = $q.defer();
			var goCombo = [];
			angular.forEach($scope.gruposOcupacionaisList, function(item) {
				goCombo.push({
					'id': item,
					'title': item.nome
				});
			});
			def.resolve(goCombo);
			return def;
		};*/

		//$scope.gruposOcupacionais = $scope.gruposOcupacionaisCombo();


		$scope.ok = function() {
			$modalInstance.close({
				questionariosDisponiveis: angular.copy($scope.questionariosDisponiveis),
				questionariosSelecionados: angular.copy($scope.questionariosSelecionados)
			});
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};


		$scope.paramsDataGridQuestionariosDisponiveis = new ngTableParams({
			count: 5, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				// if (!params.$params.filter) {
				// 	params.$params.filter = {};
				// }
				// params.$params.filter.gruposOcupacionais = {};
				// if ($scope.filtroQuestionariosDisponiveis.gruposOcupacionais && $scope.filtroQuestionariosDisponiveis.gruposOcupacionais.id) {
				// 	params.$params.filter.gruposOcupacionais = {
				// 		id: $scope.filtroQuestionariosDisponiveis.gruposOcupacionais.id
				// 	};
				// }
				// var time = $scope.onLoadQuestionariosDisponiveis ? 500 : 0;
				// $timeout(function() {
				// 	GridUtilsService.refreshData($defer, params, $scope.questionariosDisponiveis);
				// 	$scope.onLoadQuestionariosDisponiveis = false;
				// }, time);

				GridUtilsService.refreshData($defer, params, $scope.questionariosDisponiveis);
			}
		});

		$scope.paramsDataGridQuestionariosSelecionados = new ngTableParams({
			count: 5, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				if (!params.$params.filter) {
					params.$params.filter = {};
				}

				var time = $scope.onLoadQuestionariosSelecionadas ? 500 : 0;
				$timeout(function() {
					GridUtilsService.refreshData($defer, params, $scope.questionariosSelecionados);
					$scope.onLoadQuestionariosSelecionadas = false;
				}, time);
			}
		});


		$scope.$watch('filtroQuestionariosDisponiveis.gruposOcupacionais', function(newValue) {
			$scope.paramsDataGridQuestionariosDisponiveis.reload();
		});


		$scope.removerQuestionarioSelecionado = function(questionario) {
			for (var i = 0; i < $scope.questionariosSelecionados.length; i++) {
				var q = $scope.questionariosSelecionados[i];
				if (q.id === questionario.id) {
					$scope.questionariosSelecionados.splice(i, 1);
					break;
				}
			}
			$scope.questionariosDisponiveis.push(questionario);
			$scope.paramsDataGridQuestionariosDisponiveis.reload();
			$scope.paramsDataGridQuestionariosSelecionados.reload();
		};

		$scope.adicionarQuestionario = function(questionario) {
			$scope.questionariosSelecionados.push(questionario);
			for (var i = 0; i < $scope.questionariosDisponiveis.length; i++) {
				var q = $scope.questionariosDisponiveis[i];
				if (q.id === questionario.id) {
					$scope.questionariosDisponiveis.splice(i, 1);
					break;
				}
			}
			$scope.paramsDataGridQuestionariosDisponiveis.reload();
			$scope.paramsDataGridQuestionariosSelecionados.reload();
		};



	});