'use strict';

/**
 * @ngdoc service
 * @name frontendApp.QuestaoCadastroListaValoresDialogCtrl
 * @description
 * # QuestaoCadastroListaValoresDialogCtrl
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
	.controller('QuestaoCadastroListaValoresDialogCtrl', function($scope, $modalInstance, $timeout, listaValores, indexListaValores, ListaValoresService, MsgService, $filter) {

		$scope.listaValoresNova = {};

		if (listaValores) {
			//$scope.listaValoresBck = angular.copy(listaValores);
			$scope.listaValoresNova = angular.copy(listaValores);
		}

		if (!$scope.listaValoresNova.valoresLista || $scope.listaValoresNova.valoresLista.length === 0) {
			$scope.listaValoresNova.valoresLista = [{}];
		} else {
			$scope.listaValoresNova.valoresLista = $filter('orderBy')($scope.listaValoresNova.valoresLista, '+ordem');
		}

		$scope.ok = function() {
			if ($scope.listaValoresNova.valoresLista && $scope.listaValoresNova.valoresLista.length > 0) {
				for (var i = 0; i < $scope.listaValoresNova.valoresLista.length; i++) {
					var valorLista = $scope.listaValoresNova.valoresLista[i];
					valorLista.ordem = i;
					//valorLista.valor = (i + 1);
				};
			}


			if ($scope.listaValoresNova.id) {
				$('.loadingApp').show();
				ListaValoresService.atualizar($scope.listaValoresNova, function(result) {
					MsgService.addSuccess('Escala de Valores atualizada com sucesso', true);
					$('.loadingApp').hide();
					$modalInstance.close($scope.listaValoresNova, indexListaValores);
				}, function() {
					$('.loadingApp').hide();
					$modalInstance.close(undefined, indexListaValores);
				});
			} else {
				$modalInstance.close($scope.listaValoresNova, indexListaValores);
			}
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};


		$scope.focusField = function(index) {
			$scope.focus('.valorListaNome' + (index + 1));
		}

		$scope.focus('.nomeListaValores');

	});