'use strict';

/**
 * @ngdoc service
 * @name frontendApp.SelecionarGrupoQuestaoDialogCtrl
 * @description
 * # SelecionarGrupoQuestaoDialogCtrl
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
	.controller('SelecionarGrupoQuestaoDialogCtrl', function($scope, $modalInstance, $timeout, GridUtilsService, ngTableParams, gruposQuestoesDisponiveis, gruposQuestoesSelecionadas) {

		$scope.onLoadDisponiveis = true;
		$scope.onLoadSelecionadas = true;

		$scope.gruposQuestoesDisponiveis = gruposQuestoesDisponiveis;
		$scope.gruposQuestoesSelecionadas = gruposQuestoesSelecionadas;

		$scope.ok = function() {
			$modalInstance.close({
				gruposQuestoesDisponiveis: $scope.gruposQuestoesDisponiveis,
				gruposQuestoesSelecionadas: $scope.gruposQuestoesSelecionadas
			});
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};


		$scope.paramsDataGridGruposQuestoesDisponiveis = new ngTableParams({
			count: 5, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				var time = $scope.onLoadDisponiveis ? 500 : 0;
				$timeout(function() {
					GridUtilsService.refreshData($defer, params, $scope.gruposQuestoesDisponiveis);
					$scope.onLoadDisponiveis = false;
				}, time);
			}
		});

		$scope.paramsDataGridGruposQuestoesSelecionadas = new ngTableParams({
			count: 5, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				var time = $scope.onLoadSelecionadas ? 500 : 0;
				$timeout(function() {
					GridUtilsService.refreshData($defer, params, $scope.gruposQuestoesSelecionadas);
					$scope.onLoadSelecionadas = false;
				}, time);
			}
		});


		$scope.removerGrupoQuestaoSelecionada = function(grupoQuestao) {
			for (var i = 0; i < $scope.gruposQuestoesSelecionadas.length; i++) {
				var q = $scope.gruposQuestoesSelecionadas[i];
				if (q.id === grupoQuestao.id) {
					$scope.gruposQuestoesSelecionadas.splice(i, 1);
					break;
				}
			}
			$scope.gruposQuestoesDisponiveis.push(grupoQuestao);
			$scope.paramsDataGridGruposQuestoesDisponiveis.reload();
			$scope.paramsDataGridGruposQuestoesSelecionadas.reload();
		};

		$scope.adicionarGrupoQuestao = function(grupoQuestao) {
			if (!$scope.gruposQuestoesSelecionadas) {
				$scope.gruposQuestoesSelecionadas = [];
			}
			$scope.gruposQuestoesSelecionadas.push(grupoQuestao);

			for (var i = 0; i < $scope.gruposQuestoesDisponiveis.length; i++) {
				var q = $scope.gruposQuestoesDisponiveis[i];
				if (q.id === grupoQuestao.id) {
					$scope.gruposQuestoesDisponiveis.splice(i, 1);
					break;
				}
			}
			$scope.paramsDataGridGruposQuestoesDisponiveis.reload();
			$scope.paramsDataGridGruposQuestoesSelecionadas.reload();
		};

	});