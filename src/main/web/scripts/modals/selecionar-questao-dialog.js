'use strict';

/**
 * @ngdoc service
 * @name frontendApp.SelecionarQuestaoDialogCtrl
 * @description
 * # SelecionarQuestaoDialogCtrl
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
	.controller('SelecionarQuestaoDialogCtrl', function($scope, $modalInstance, $timeout, GridUtilsService, ngTableParams, questoesDisponiveis, questoesSelecionadas, isQuestaoOrdem) {

		$scope.filtroQuestoesDisponiveis = {};
		$scope.onLoadQuestoesDisponiveis = true;
		$scope.onLoadQuestoesSelecionadas = true;

		$scope.questoesDisponiveis = questoesDisponiveis;
		$scope.questoesSelecionadas = questoesSelecionadas;
		$scope.isQuestaoOrdem = isQuestaoOrdem;

		$scope.ok = function() {
			var questoesSelecionadas = [];
			for (var i = 0; i < $scope.questoesSelecionadas.length; i++) {
				var questaoOrdem = $scope.questoesSelecionadas[i];
				questaoOrdem.ordem = i;
				questoesSelecionadas.push(questaoOrdem);
			};
			$modalInstance.close({
				questoesDisponiveis: $scope.questoesDisponiveis,
				questoesSelecionadas: questoesSelecionadas
			});
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};


		$scope.paramsDataGridQuestoesDisponiveis = new ngTableParams({
			count: 5, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				var time = $scope.onLoadQuestoesDisponiveis ? 500 : 0;
				$timeout(function() {
					GridUtilsService.refreshData($defer, params, $scope.questoesDisponiveis);
					$scope.onLoadQuestoesDisponiveis = false;
				}, time);
			}
		});

		$scope.paramsDataGridQuestoesSelecionadas = new ngTableParams({
			count: 5, // count per page
		}, {
			total: 0, // length of data
			counts: [5, 10, 50],
			getData: function($defer, params) {
				var time = $scope.onLoadQuestoesSelecionadas ? 500 : 0;
				$timeout(function() {
					GridUtilsService.refreshData($defer, params, $scope.questoesSelecionadas);
					$scope.onLoadQuestoesSelecionadas = false;
				}, time);
			}
		});


		$scope.removerQuestaoSelecionada = function(questao) {
			for (var i = 0; i < $scope.questoesSelecionadas.length; i++) {
				var q = $scope.questoesSelecionadas[i];
				if ($scope.isQuestaoOrdem) {
					if (q.questao.id === questao.id) {
						$scope.questoesSelecionadas.splice(i, 1);
						break;
					}
				} else {
					if (q.id === questao.id) {
						$scope.questoesSelecionadas.splice(i, 1);
						break;
					}
				}
			}
			$scope.questoesDisponiveis.push(questao);
			$scope.paramsDataGridQuestoesDisponiveis.reload();
			$scope.paramsDataGridQuestoesSelecionadas.reload();
		};

		$scope.adicionarQuestao = function(questao) {
			if (!$scope.questoesSelecionadas) {
				$scope.questoesSelecionadas = [];
			}
			if ($scope.isQuestaoOrdem) {
				$scope.questoesSelecionadas[$scope.questoesSelecionadas.length] = {
					questao: questao,
					ordem: $scope.questoesSelecionadas.length
				};
			} else {
				$scope.questoesSelecionadas.push(questao);
			}

			for (var i = 0; i < $scope.questoesDisponiveis.length; i++) {
				var q = $scope.questoesDisponiveis[i];
				if (q.id === questao.id) {
					$scope.questoesDisponiveis.splice(i, 1);
					break;
				}
			}
			$scope.paramsDataGridQuestoesDisponiveis.reload();
			$scope.paramsDataGridQuestoesSelecionadas.reload();
		};

	});