'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AvaliacaoService
 * @description
 * # AvaliacaoService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AvaliacaoService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/avaliacao/:id/:service/:idPesquisa/:idAvaliador/:idAvaliado', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      avaliacoesEmAberto: {
        method: 'GET',
        isArray: true,
        params: {
          service: 'avaliacoesEmAberto'
        }
      },
      avaliacoesByPesquisasEmAberto: {
        method: 'GET',
        isArray: true,
        params: {
          service: 'avaliacoesByPesquisasEmAberto'
        }
      },
      avaliacoesByPesquisasEmAbertoPageable: {
        method: 'POST',
        isArray: false,
        params: {
          service: 'avaliacoesByPesquisasEmAbertoPageable'
        }
      },
      get: {
        method: 'GET',
        isArray: false
      },
      getAvaliacao: {
        method: 'GET',
        isArray: true
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      associar: {
        method: 'POST',
        isArray: true,
        params: {
          service: 'associar'
        }
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      }
    });


  });