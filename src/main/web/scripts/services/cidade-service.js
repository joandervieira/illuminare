'use strict';

/**
 * @ngdoc service
 * @name frontendApp.CidadeService
 * @description
 * # CidadeService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('CidadeService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/cidade/:id/:service/:idEstado', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      findByEstado: {
        method: 'GET',
        isArray: true,
        params: {
          service: 'findByEstado'
        }
      }
    });


  });