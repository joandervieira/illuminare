'use strict';

/**
 * @ngdoc service
 * @name frontendApp.RespostaService
 * @description
 * # RespostaService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('QuestaoClassificacaoService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/questao-classificacao/:service/:id', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      salvarTodos: {
        method: 'POST',
        isArray: true,
        params: {
          service: 'all'
        }
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      atualizarTodos: {
        method: 'PUT',
        isArray: true,
        params: {
          service: 'all'
        }
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      }
    });


  });