'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ConstantsService
 * @description
 * # ConstantsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ConstantsService', function() {
    // Service logic
    // ...



    // Public API here
    return {
      serverURL: function(url) {
        if (url.substring(0, 1) === '/') {
          url = url.substring(1, url.length);
        }
        return '../servermock/' + url;
      }
    };
  });