'use strict';

/**
 * @ngdoc service
 * @name frontendApp.EmpresaService
 * @description
 * # EmpresaService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('EmpresaService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/empresa/:service/:id', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      }
    });


  });