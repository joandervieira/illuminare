'use strict';

/**
 * @ngdoc service
 * @name frontendApp.EnderecoService
 * @description
 * # EnderecoService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('EnderecoService', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
