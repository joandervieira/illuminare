'use strict';

/**
 * @ngdoc service
 * @name frontendApp.TreinamentoService
 * @description
 * # TreinamentoService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('EstadoCivilService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/estado-civil/:id/:service', {
      ////id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
        /*params: {
          service: 'lista'
        },*/
        /*interceptor: {
          response: function(response) {
            return response.data && response.data.dados_acao && response.data.dados_acao.Sindicatos
          }
        }*/
      },
      get: {
        method: 'GET',
        isArray: false,
        /*params: {
          service: 'byId'
        }*/
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      }
    });


  });