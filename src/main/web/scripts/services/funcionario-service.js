'use strict';

/**
 * @ngdoc service
 * @name frontendApp.FuncionarioService
 * @description
 * # FuncionarioService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('FuncionarioService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/funcionario/:id/:service', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      },
      importFromXls: {
        method: 'POST',
        isArray: false,
        params: {
          service: 'importFromXls'
        }
      },
      confirmarImportacao: {
        method: 'POST',
        isArray: false,
        params: {
          service: 'confirmarImportacao'
        }
      },
      preConfirmarImportacao: {
        method: 'POST',
        isArray: false,
        params: {
          service: 'preConfirmarImportacao'
        }
      }
    });


  });