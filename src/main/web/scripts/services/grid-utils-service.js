'use strict';

/**
 * @ngdoc service
 * @name frontendApp.GridUtilsService
 * @description
 * # GridUtilsService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('GridUtilsService', function($filter) {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      sortData: function(data, params) {

        if (params.orderBy() && params.orderBy().length > 0) {
          return $filter('orderBy')(data, params.orderBy());
        } else {
          return data;
        }

      },

      filterData: function(data, params) {
        if (!angular.equals({}, params.$params.filter)) {
          return $filter('filter')(data, params.$params.filter);
        } else {
          return data;
        }
      },

      refreshData: function($defer, params, data, isPageable) {
        var paginator;
        if (isPageable) {
          paginator = data;
          data = paginator.result;
        }

        data = data || [];

        //params.data = data;

        if (data.length > 0) {

          var orderedData = this.sortData(data, params);
          orderedData = this.filterData(orderedData, params);

          //params.total(orderedData.length); // set total for recalc pagination
          if (isPageable) {
            params.total(paginator.total); // set total for recalc pagination
          } else {
            params.total(orderedData.length); // set total for recalc pagination
          }

          //var pagedData = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
          var pagedData;
          if (isPageable) {
            pagedData = orderedData;
          } else {
            pagedData = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
          }

          if (pagedData.length <= 0) {
            params.page(1);
          }

          $defer.resolve(pagedData);

        } else {
          params.page(1);
          params.total(0);
          $defer.resolve([]);
        }

      }


    };


  });