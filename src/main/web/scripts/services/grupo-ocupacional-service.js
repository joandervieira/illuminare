'use strict';

/**
 * @ngdoc service
 * @name frontendApp.GrupoOcupacionalService
 * @description
 * # GrupoOcupacionalService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('GrupoOcupacionalService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/grupo-ocupacional/:id/:service', {
      ////id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      }
    });


  });