'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ListaValoresService
 * @description
 * # ListaValoresService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ListaValoresService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/lista-valores/:id/:service', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      }
    });


  });