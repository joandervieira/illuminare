'use strict';

/**
 * @ngdoc service
 * @name frontendApp.MetaService
 * @description
 * # MetaService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('MetaService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/meta/:id/:service/:idPesquisa/:idAvaliado', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      },
      findByPesquisaAndFuncionario: {
        method: 'GET',
        params: {
          service: 'findByPesquisaAndFuncionario'
        },
        isArray: false
      }
    });


  });