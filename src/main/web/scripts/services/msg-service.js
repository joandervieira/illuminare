'use strict';

/**
 * @ngdoc service
 * @name frontendApp.MsgService
 * @description
 * # MsgService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('MsgService', function($rootScope) {

    return {
      addSuccess: function(msg, persistent) {
        /*if (!$rootScope.msgsSuccess) {
          $rootScope.msgsSuccess = [];
        }*/
        $rootScope.msgsSuccess = [];
        $rootScope.msgsFailed = [];
        if (!persistent) {
          persistent = false;
        }
        $rootScope.msgsSuccess.push({
          msg: msg,
          persistent: persistent
        });
        //$rootScope.focus('.viewMainWrap .msgs .msg');
        $rootScope.scrollTo('.viewMainWrap .msgs');
      },
      addFailed: function(msg, persistent) {
        /*if (!$rootScope.msgsFailed) {
          $rootScope.msgsFailed = [];
        }*/
        $rootScope.msgsFailed = [];
        $rootScope.msgsSuccess = [];
        if (!persistent) {
          persistent = false;
        }
        $rootScope.msgsFailed.push({
          msg: msg,
          persistent: persistent
        });
        //$rootScope.focus('.viewMainWrap .msgs .msg');
        $rootScope.scrollTo('.viewMainWrap .msgs');
      },
      cleanSuccess: function(cleanPersistents) {
        if (!cleanPersistents) {
          cleanPersistents = false;
        }
        $rootScope.newMsgsSuccess = [];
        if ($rootScope.msgsSuccess && $rootScope.msgsSuccess.length > 0) {
          for (var i = 0; i < $rootScope.msgsSuccess.length; i++) {
            if (!cleanPersistents) {
              if ($rootScope.msgsSuccess[i].persistent) {
                $rootScope.newMsgsSuccess.push($rootScope.msgsSuccess[i]);
              }
            }
          }
        }
        $rootScope.msgsSuccess = $rootScope.newMsgsSuccess;
      },
      cleanFailed: function(cleanPersistents) {
        if (!cleanPersistents) {
          cleanPersistents = false;
        }
        $rootScope.newMsgsFailed = [];
        if ($rootScope.msgsFailed && $rootScope.msgsFailed.length > 0) {
          for (var i = 0; i < $rootScope.msgsFailed.length; i++) {
            if (!cleanPersistents) {
              if ($rootScope.msgsFailed[i].persistent) {
                $rootScope.newMsgsFailed.push($rootScope.msgsFailed[i]);
              }
            }
          }
        }
        $rootScope.msgsFailed = $rootScope.newMsgsFailed;
      },
      cleanAll: function(cleanPersistents) {
        this.cleanSuccess(cleanPersistents);
        this.cleanFailed(cleanPersistents);
      }
    };

  });