'use strict';

/**
 * @ngdoc service
 * @name frontendApp.PesquisaService
 * @description
 * # PesquisaService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('PesquisaService', function ($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/pesquisa/:id/:service/:idAvaliado', {
      //id: '@id'
    }, {
        lista: {
          method: 'GET',
          isArray: true,
        },
        get: {
          method: 'GET',
          isArray: false
        },
        getFull: {
          method: 'GET',
          params: {
            service: 'full'
          },
          isArray: false
        },
        salvar: {
          method: 'POST',
          isArray: false
        },
        atualizar: {
          method: 'PUT',
          isArray: false
        },
        excluir: {
          method: 'DELETE',
          isArray: false
        },
        pesquisar: {
          method: 'POST',
          params: {
            service: 'pesquisar'
          },
          isArray: false
        },
        fechadas: {
          method: 'GET',
          params: {
            service: 'fechadas'
          },
          isArray: true
        },
        abertas: {
          method: 'GET',
          params: {
            service: 'abertas'
          },
          isArray: true
        },
        avaliados: {
          method: 'GET',
          params: {
            service: 'avaliados'
          },
          isArray: true
        },
        temAvaliacaoRespondida: {
          method: 'GET',
          params: {
            service: 'temAvaliacaoRespondida'
          },
          isArray: false
        },
        removerAvaliacoesEResponstas: {
          method: 'DELETE',
          params: {
            service: 'removerAvaliacoesEResponstas'
          },
          isArray: false
        },
        removerRespostas: {
          method: 'DELETE',
          params: {
            service: 'removerRespostas'
          },
          isArray: false
        },
        buscarMetas: {
          method: 'POST',
          params: {
            service: 'buscarMetas'
          },
          isArray: false
        },
        clonar: {
          method: 'POST',
          params: {
            service: 'clonar'
          },
          isArray: false
        },
        clonarMetas: {
          method: 'POST',
          params: {
            service: 'clonarMetas'
          },
          isArray: false
        }
      });


  });