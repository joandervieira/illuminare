'use strict';

/**
 * @ngdoc service
 * @name frontendApp.RespostaService
 * @description
 * # RespostaService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('RespostaService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/resposta/:service/:id', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      salvarTodos: {
        method: 'POST',
        isArray: true,
        params: {
          service: 'all'
        }
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      atualizarTodos: {
        method: 'PUT',
        isArray: true,
        params: {
          service: 'all'
        }
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      deleteByAvaliacao: {
        method: 'DELETE',
        isArray: false,
        params: {
          service: 'deleteByAvaliacao'
        }
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      },
      findByAvaliacao: {
        method: 'GET',
        isArray: true,
        params: {
          service: 'findByAvaliacao'
        }
      },
      findByAvaliacaoAgrupado: {
        method: 'GET',
        isArray: true,
        params: {
          service: 'findByAvaliacaoAgrupado'
        }
      },
      questionarioRespondido: {
        method: 'GET',
        isArray: false,
        params: {
          service: 'questionarioRespondido'
        }
      }
    });


  });