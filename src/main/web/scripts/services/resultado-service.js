'use strict';

/**
 * @ngdoc service
 * @name frontendApp.ResultadoService
 * @description
 * # ResultadoService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('ResultadoService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/resultado/:service/:id/:idPesquisa/:idAvaliado/:idGrupoOcupacional/:idCargo/:idSetor/:idEmpresa', {
      //id: '@id'
    }, {
      get: {
        method: 'GET',
        isArray: false
      },
      byGrupoOcupacional: {
        method: 'GET',
        params: {
          service: 'byGrupoOcupacional'
        },
        isArray: false
      },
      byCargo: {
        method: 'GET',
        params: {
          service: 'byCargo'
        },
        isArray: false
      },
      bySetor: {
        method: 'GET',
        params: {
          service: 'bySetor'
        },
        isArray: false
      },
      byEmpresa: {
        method: 'GET',
        params: {
          service: 'byEmpresa'
        },
        isArray: false
      }

    });


  });