'use strict';

/**
 * @ngdoc service
 * @name frontendApp.RoleService
 * @description
 * # RoleService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('RoleService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/role/:id/:service', {
      ////id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true,
        /*params: {
          service: 'lista'
        },*/
        /*interceptor: {
          response: function(response) {
            return response.data && response.data.dados_acao && response.data.dados_acao.Sindicatos
          }
        }*/
      },
      get: {
        method: 'GET',
        isArray: false,
        /*params: {
          service: 'byId'
        }*/
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      }
    });


  });