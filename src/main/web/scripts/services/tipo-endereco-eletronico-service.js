'use strict';

/**
 * @ngdoc service
 * @name frontendApp.TipoEnderecoEletronicoService
 * @description
 * # TipoEnderecoEletronicoService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('TipoEnderecoEletronicoService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/tipo-endereco-eletronico/:id/:service', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      }
    });


  });