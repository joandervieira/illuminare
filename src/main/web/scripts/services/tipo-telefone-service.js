'use strict';

/**
 * @ngdoc service
 * @name frontendApp.TipoTelefoneService
 * @description
 * # TipoTelefoneService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('TipoTelefoneService', function($resource) {
    // Service logic
    // ...

    return $resource(APP_CONFIG.prefixURLRest + '/tipo-telefone/:id/:service', {
      //id: '@id'
    }, {
      lista: {
        method: 'GET',
        isArray: true
      },
      get: {
        method: 'GET',
        isArray: false
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      }
    });


  });