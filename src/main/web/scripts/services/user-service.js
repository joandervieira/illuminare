'use strict';

/**
 * @ngdoc service
 * @name frontendApp.UserService
 * @description
 * # UserService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('UserService', function($resource) {
    // Service logic
    // ...


    return $resource(APP_CONFIG.prefixURLRest + '/user/:id:service', {}, {
      authenticate: {
        method: 'POST',
        params: {
          'service': 'authenticate'
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      },
      todos: {
        method: 'GET',
        isArray: true,
        params: {
          'service': 'todos'
        }
      },
      salvar: {
        method: 'POST',
        isArray: false
      },
      enviarEmailRecuperacao: {
        method: 'POST',
        isArray: false,
        params: {
          'service': 'enviarEmailRecuperacao'
        }
      },
      atualizar: {
        method: 'PUT',
        isArray: false
      },
      atualizarSenha: {
        method: 'PUT',
        params: {
          'service': 'atualizarSenha'
        },
        isArray: false
      },
      excluir: {
        method: 'DELETE',
        isArray: false
      },
      pesquisar: {
        method: 'POST',
        params: {
          service: 'pesquisar'
        },
        isArray: false
      },
      findById: {
        method: 'GET',
        isArray: false
      }
    });


  });