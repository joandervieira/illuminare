package com.illuminare.entity;


import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import com.google.gson.Gson;


public class PesquisaTest {

    @Test
    public void testFormat() {

        Pesquisa pesquisa = new Pesquisa();
        pesquisa.setId( 2l );
        pesquisa.setNome( "Pesquisa teste" );
        Set< Questionario > questionarios = new LinkedHashSet<>();
        Questionario q1 = new Questionario();
        q1.setId( 1l );
        q1.setNome( "Questionario teste 1" );
        questionarios.add( q1 );
        pesquisa.setQuestionarios( questionarios );

        print( pesquisa.formatToWebPage().format() );
    }


    private void print( Object obj ) {

        System.out.println( toJson( obj ) );
    }


    private static String toJson( Object obj ) {

        return new Gson().toJson( obj );
    }
}